
## Configure Wamp Server

#### 1. Download & Install

  Download and install WampServer from [here](http://www.wampserver.com/en/#download-wrapper).
    *If you need to install 64bit WampServer you must install both of vcredist(x64 and x86)*
   
  Once you installed WampServer successfully server should run correctly on your PC. 
    *If you have problem with running the server it is related to vcredist mostly*
   
#### 2. Setting Virtual Hosts   
  Let's change `localhost` into `s85.co` for convenience.
  
  Uncomment `Include conf/extra/httpd-vhosts.conf` in `c:/wamp64/bin/apache/apache2.4.27/conf/httpd.conf`.
  
  Overwrite `c:/wamp64/bin/apache/apache2.4.27/conf/extra/httpd-vhosts.conf` in to the following.
  
  ```
  # Main domain
  <VirtualHost *:80>
      ServerAdmin webmaster@s85.co
      DocumentRoot "C:/wamp64/www/elliot"
      ServerName s85.co
      ServerAlias www.s85.co
      ErrorLog "${INSTALL_DIR}/logs/s85.co-error.log"
      CustomLog "${INSTALL_DIR}/logs/s85.co-access.log" common

      <Directory "C:/wamp64/www/elliot/">
          Order allow,deny
          Allow from all
      </Directory>
  </VirtualHost>

  # Sub domain for elliot user. You need to change *baluev* in to your own elliot username
  <VirtualHost *:80>
      ServerAdmin webmaster@s85.co
      DocumentRoot "C:/wamp64/www/elliot"
      ServerName baluev.s85.co
      ServerAlias www.baluev.s85.co
      ErrorLog "${INSTALL_DIR}/logs/baluev.s85.co-error.log"
      CustomLog "${INSTALL_DIR}/logs/baluev.s85.co-access.log" common

      <Directory "C:/wamp64/www/elliot/">
          Order allow,deny
          Allow from all
      </Directory>
  </VirtualHost>
  ```
  
  Append the following to `c:/Windows/System32/drivers/etc/hosts`.
  
  ```
  127.0.0.1       s85.co
  127.0.0.1       baluev.s85.co   # Change baluev in to your own elliot username
  ```
   
#### 3. Configure SSL

  Refer to this [guide](https://www.proy.info/how-to-enable-localhost-https-on-wamp-server/).
  
  *Pay attention the following changes in `httpd-ssl.conf`*
  ```
  DocumentRoot "c:/wamp64/www/elliot"
  ServerName s85.co:443
  ```
  
  You can check sample configuration files.
  [c:/wamp64/bin/apache/apache2.4.27/conf/httpd.conf](https://drive.google.com/open?id=0B-8Yx0D7qrlzMTVwa3F1cGktSjQ)
  [c:/wamp64/bin/apache/apache2.4.27/conf/extra/httpd-vhosts.conf](https://drive.google.com/open?id=0B-8Yx0D7qrlzZXMtNVlneHBGb0U)
  [c:/wamp64/bin/apache/apache2.4.27/conf/extra/httpd-ssl.conf](https://drive.google.com/open?id=0B-8Yx0D7qrlzX0hDb3lLVWs4VHc)
  [c:/Windows/System32/drivers/etc/hosts](https://drive.google.com/open?id=0B-8Yx0D7qrlzQkpINnhVazg4VVE)
  
## Install MySQL

  We need to have main database([`elli`](https://drive.google.com/open?id=0B-8Yx0D7qrlzeVh0RFR6UUZsbms)) and user database([`<username>_elliot`](https://drive.google.com/open?id=0B-8Yx0D7qrlzdHRmbHVIaGpSbEk)).


## Configure Source

  Rename source folder into `elliot` and copy it to `c:\wamp64\www`.
  
  In order that application run on local server correctly we need to add Yii configuration files since those were ignored on Git repo.
  You can download configuration files [here](https://drive.google.com/open?id=0B-8Yx0D7qrlzVk5aZElZVl91YXM) 
  
  
  
**All configuration is correct you should see local server is running on `https://s85.co`**  

