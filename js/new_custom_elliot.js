var hidden_user_domain = $("#hidden_user_domain").val();

$(document).ready(function () {
    $("body").tooltip({selector: '[data-toggle=tooltip]'});
});

$(function () {

    $("#content_view_table").DataTable({
        "order": [[0, "asc"]],
        "columnDefs": [
            {"orderable": false, "targets": 1}
        ],
        pageLength: 25,
        "processing": true,
        "serverSide": true,
        "oLanguage": {
            sProcessing: function () {
                $(".contentLoader").first().addClass('be-loading-active');
            }
        },
        initComplete: function () {
            $(".contentLoader").first().removeClass('be-loading-active');
        },
        "ajax": "/content/getpages",
        buttons: [
        ],
        dom: "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B><'col-sm-12 text-right'f>>" +
                "<'row be-datatable-body'<'col-sm-12'tr>>" +
                "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"

    });

    $('#order_index_table').DataTable({
        "order": [[5, "desc"]],
        pageLength: 25,
        "columnDefs": [
            {"orderable": false, "targets": 0}
        ],
        "oLanguage": {
            sProcessing: function () {
                $(".orderListTAble").first().addClass('be-loading-active');
            }
        },
        initComplete: function () {
            $(".orderListTAble").first().removeClass('be-loading-active');
        },
        "processing": true,
        "serverSide": true,
        "ajax": "/orders/ordersajax",
        buttons: [
        ],
        dom: "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B><'col-sm-12 text-right'f>>" +
                "<'row be-datatable-body'<'col-sm-12'tr>>" +
                "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"

    });


//    $('#latest_engagement_dashboard').DataTable({
//        //"order": [[ 4, "desc" ]],
//        pageLength: 5,
//        "columnDefs": [
//            {"orderable": false, "targets": [1, 2, 3]}
//            //{"orderable": true, "targets": 1}
//        ],
//        buttons: [
//        ],
//        dom: ""
//    });


    /*For Sfexpress table*/
    $('#tablesf2').DataTable({
        //"order": [[ 4, "desc" ]],
        pageLength: 10,
        "columnDefs": [
            {"orderable": true, "targets": 1}
        ],
        buttons: [
        ],
        dom: "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B><'col-sm-12 text-right'f>>" +
                "<'row be-datatable-body'<'col-sm-12'tr>>" +
                "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"
    });

    $('#tablesf3').DataTable({
        //"order": [[ 4, "desc" ]],
        pageLength: 10,
        "columnDefs": [
            {"orderable": true, "targets": 1}
        ],
        buttons: [
        ],
        dom: "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B><'col-sm-12 text-right'f>>" +
                "<'row be-datatable-body'<'col-sm-12'tr>>" +
                "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"
    });
    $('#tablesf4').DataTable({
        //"order": [[ 4, "desc" ]],
        pageLength: 10,
        "columnDefs": [
            {"orderable": true, "targets": 1}
        ],
        buttons: [
        ],
        dom: "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B><'col-sm-12 text-right'f>>" +
                "<'row be-datatable-body'<'col-sm-12'tr>>" +
                "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"
    });
    $('#tablesf5').DataTable({
        //"order": [[ 4, "desc" ]],
        pageLength: 10,
        "columnDefs": [
            {"orderable": true, "targets": 1}
        ],
        buttons: [
        ],
        dom: "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B><'col-sm-12 text-right'f>>" +
                "<'row be-datatable-body'<'col-sm-12'tr>>" +
                "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"
    });
    $('#tablesf6').DataTable({
        //"order": [[ 4, "desc" ]],
        pageLength: 10,
        "columnDefs": [
            {"orderable": true, "targets": 1}
        ],
        buttons: [
        ],
        dom: "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B><'col-sm-12 text-right'f>>" +
                "<'row be-datatable-body'<'col-sm-12'tr>>" +
                "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"
    });
    $('#tablesf7').DataTable({
        //"order": [[ 4, "desc" ]],
        pageLength: 10,
        "columnDefs": [
            {"orderable": true, "targets": 1}
        ],
        buttons: [
        ],
        dom: "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B><'col-sm-12 text-right'f>>" +
                "<'row be-datatable-body'<'col-sm-12'tr>>" +
                "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"
    });
    $('#tablesf8').DataTable({
        //"order": [[ 4, "desc" ]],
        pageLength: 10,
        "columnDefs": [
            {"orderable": true, "targets": 1}
        ],
        buttons: [
        ],
        dom: "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B><'col-sm-12 text-right'f>>" +
                "<'row be-datatable-body'<'col-sm-12'tr>>" +
                "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"
    });
    /*End For Sfexpress table*/


    /*Starts Connected Channel Or Store Order page */
    /*get current action in hidden field*/
    var action = document.getElementById("current_action").value;
    if (action == 'connected') {
        var connect = document.getElementById("connected").value;
        $('#connect_order_index_table').DataTable({
            "order": [[5, "desc"]],
            pageLength: 25,
            "columnDefs": [
                {"orderable": false, "targets": 0}
            ],
            "oLanguage": {
                sProcessing: function () {
                    $(".connectedOrDERS").first().addClass('be-loading-active');
                }
            },
            initComplete: function () {
                $(".connectedOrDERS").first().removeClass('be-loading-active');
            },
            "processing": true,
            "serverSide": true,
            "ajax": "/channels/connectordersajax?orders=" + connect,
            buttons: [
            ],
            dom: "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B><'col-sm-12 text-right'f>>" +
                    "<'row be-datatable-body'<'col-sm-12'tr>>" +
                    "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"

        });




    }

    /*End Connected Channel Or Store Order page */

    var recent_orders_dashboard1212 = $('#recent_orders_dashboard_tbl1').DataTable({
        //"order": [[ 4, "desc" ]],
        pageLength: 5,
        "columnDefs": [
            {"orderable": false, "targets": 1}
        ],
        buttons: [
        ],
        dom: ""
    });


//

//    $('#recent_orders_dashboard_ajax').DataTable({
//        // "order": [[ 2, "desc" ]],
//        pageLength: 5,
//        "columnDefs": [
//             {"orderable": false, "targets": [1, 2, 3]}
//            //  {"orderable": false, "targets": 1}
//            // {"orderable": false, "targets": 2},
//            //  {"orderable": false, "targets": 3},
//            // {"orderable": false, "targets": 4}
//        ],
//        "order": [],
//        buttons: [
//        ],
//        dom: ""
//    });


    $('#lazada_connect_modal').DataTable({
        // "order": [[ 2, "desc" ]],
//        pageLength: 2,
        "columnDefs": [
            //{"orderable": false, "targets": 1}
            // {"orderable": false, "targets": 2},
            //  {"orderable": false, "targets": 3},
            // {"orderable": false, "targets": 4}
        ],
        "order": [],
        buttons: [
        ],
        dom: ""
    });


    $('#products_table').DataTable({
        buttons: [
        ],
        dom: ""
    });

    $('#user_added').DataTable({
        buttons: [
        ],
        dom: ""
    });


//    recent_orders_dashboard1212.fixedHeader.disable();
//    recent_orders_dashboard1212.buttons().disable();

    /*Adding Active Open Class to Main Menu if any Submenu is open*/
    if (window.location.href.indexOf("products") > -1) {
        $('.be-left-sidebar .left-sidebar-content .sidebar-elements li a').each(function () {
            var a = $(this).find('span').text();
            if (a == 'Products') {
                $(this).parent().addClass('active open');
            }
        });
    }
    /*END*/



    /* Products Listing DataTable */
    $("#products_table1").dataTable({
        responsive: true,
        "order": [[5, "desc"]],
        pageLength: 25,
        "columnDefs": [
            {"orderable": false, "targets": 0},
            {"targets": 2, "orderable": false}
        ],
        "columns": [
            null,
            null,
            {className: "Custom_SCroll"},
            null,
            null,
            null,
            null
        ],
        "oLanguage": {
            sProcessing: function () {
                $(".productTABLE").first().addClass('be-loading-active');

            }
        },
        "initComplete": function () {
            $(".productTABLE").first().removeClass('be-loading-active');

        },
        drawCallback: function (settings) {
            var api = this.api();
            $('[data-toggle="tooltip"]').tooltip();
            $("[data-toggle='popover']").popover();
        },
        "processing": true,
        "serverSide": true,
        "ajax": "/channels/productsajax",
        buttons: [
            'copy', 'excel', 'pdf', 'print'
        ],
        dom: "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B><'col-sm-12 text-right'f>>" +
                "<'row be-datatable-body'<'col-sm-12'tr>>" +
                "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"
    });


    /*connected Products Listing DataTable */
    /*get current action in hidden field*/
    var action = document.getElementById("current_action").value;
    if (action == 'connected-products') {
        var connect = document.getElementById("connected_products").value;

        $("#connectedproducts_table1").dataTable({
            responsive: true,
            "order": [[5, "desc"]],
            pageLength: 25,
            "columnDefs": [
                {"orderable": false, "targets": 0},
                {"targets": 2, "orderable": false}
            ],
            "columns": [
                null,
                null,
                {className: "Custom_SCroll"},
                null,
                null,
                null,
                null
            ],
            "oLanguage": {
                sProcessing: function () {
                    $(".ConnectedProDUCTS").first().addClass('be-loading-active');
                }
            },
            "initComplete": function () {
                $(".ConnectedProDUCTS").first().removeClass('be-loading-active');
            },
            "processing": true,
            "serverSide": true,
            "ajax": "/channels/connected-products?product=" + connect,
            buttons: [
                'copy', 'excel', 'pdf', 'print'
            ],
            dom: "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B><'col-sm-12 text-right'f>>" +
                    "<'row be-datatable-body'<'col-sm-12'tr>>" +
                    "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"
        });
    }

    if (action == 'inactive-products') {

        var connected = document.getElementById("inactive_products").value;
        $("#inactiveproducts_table1").dataTable({
            responsive: true,
            "order": [[5, "desc"]],
            pageLength: 25,
            "columnDefs": [
                {"orderable": false, "targets": 0},
                {"targets": 2, "orderable": false}
            ],
            "columns": [
                null,
                null,
                {className: "Custom_SCroll"},
                null,
                null,
                null,
                null
            ],
            "oLanguage": {
                sProcessing: function () {
                    $(".InactiVeProDUCTS").first().addClass('be-loading-active');
                }
            },
            "initComplete": function () {
                $(".InactiVeProDUCTS").first().removeClass('be-loading-active');
            },
            "processing": true,
            "serverSide": true,
            "ajax": "/channels/inactive-products?product=" + connected,
            buttons: [
                'copy', 'excel', 'pdf', 'print'
            ],
            dom: "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B><'col-sm-12 text-right'f>>" +
                    "<'row be-datatable-body'<'col-sm-12'tr>>" +
                    "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"

        })
    }

    /* END */

    /* Categories DataTable */
    $("#categories_table").dataTable({
        pageLength: 50,
        buttons: [
            'copy', 'excel', 'pdf', 'print'
        ],
        dom: "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B><'col-sm-12 text-right'f>>" +
                "<'row be-datatable-body'<'col-sm-12'tr>>" +
                "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"
    });
    /* END */
    /* Variations DataTable */
    $("#variations_table").dataTable({
        pageLength: 50,
        buttons: [
            'copy', 'excel', 'pdf', 'print'
        ],
        dom: "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B><'col-sm-12 text-right'f>>" +
                "<'row be-datatable-body'<'col-sm-12'tr>>" +
                "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"
    });
    /* END */

    /*Fields Product Image*/
    $('.pimg_lbl').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });
    $('.pimg_alt_tag').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });
    $('.pimg_html_video').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });
    $('.pimg_360_video').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });
    /*Add More Image - Product*/
    $('.addimg-btn').click(function (e) {
        e.preventDefault();
        var previousDivCount = $('#imgDivCount').val();
        var nextDivCount = parseInt(previousDivCount) + 1;
        var html = '';
        html = '<div id="pimgDiv_' + nextDivCount + '" class="col-sm-3 pimg-div' + nextDivCount + ' be-loading draggable-element" >'
        html += '<div class="bs-grid-block product_create_image product-texthover-default">'
        html += '<div class="user-display-bg">'
        html += '<div class="content dropzone pImg_Create_Drop1" id="pImageDrop_' + nextDivCount + '">'
        html += '</div>'
        html += '</div>'
        html += '</div>'
        html += '<input type="hidden" id="upld_img' + nextDivCount + '" class="upld_img" value="">'
        html += '<div class="setimg">'
        html += '<i class="icon icon-left mdi mdi-image"></i> Set as Default Image'
        html += '<div class="be-radio">'
        html += '<input name="pimg_radio" id="pimgRad_' + nextDivCount + '" type="radio">'
        html += '<label for="pimgRad_' + nextDivCount + '"></label>'
        html += '</div>'
        html += '</div>'
        html += '<table id="pimg_tbl' + nextDivCount + '" style="clear: both" class="table table-striped table-borderless">'
        html += '<tbody>'
        html += '<tr>'
        html += '<td width="45%">Image Label</td>'
        html += '<td width="55%"><a id="pimg_lbl' + nextDivCount + '" class="pimg_lbl" href="#" data-type="text" data-title="Please Enter value"></a></td>'
        html += '</tr>'
        html += '<tr>'
        html += '<td width="45%">Alt Tag</td>'
        html += '<td width="55%"><a id="pimg_alt_tag' + nextDivCount + '" class="pimg_alt_tag" href="#" data-type="text" data-title="Please Enter value"></a></td>'
        html += '</tr>'
        html += '<tr>'
        html += '<td width = "45%">HTML Video Link </td>'
        html += '<td width = "55%"><a id="pimg_html_video' + nextDivCount + '" class = "pimg_html_video" href = "#" data - type = "text" data - title = "Please Enter value(Only support Youtube and Vimeo)" > </a></td >'
        html += '</tr>'
        html += '</tbody>'
        html += '</table>'
        html += '<div class="vfile"><input type="file" name="pimg_360_video" id="pimg_360_video' + nextDivCount + '" data-multiple-caption="{count} files selected" multiple class="inputfile" accept="video/*">'
        html += '<label for="pimg_360_video' + nextDivCount + '" class="btn-default"> <i class="mdi mdi-upload"></i>'
        html += '<span> Select 360 - degree video </span>'
        html += '</label>'
        html += '</div>'
        html += '<div class="progress" id="pimg_360_video_progress_wrapper' + nextDivCount + '">'
        html += '<div id="pimg_360_video_progress' + nextDivCount + '" class="progress-bar progress-bar-primary progress-bar-striped"></div>'
        html += '</div>'
        html += '<div class="vupld">'
        html += '<button id="vupldbtn_' + nextDivCount + '" class="btn btn-rounded btn-space btn-default vupld_btn">Upload</button>'
        html += '</div>'
        html += '<div class="pimg_save_btns"> <button id="pimgSaveBtn_' + nextDivCount + '" class="btn btn-space btn-primary btn-sm pimg_save_btn">Save</button></div>'
        html += '<div class="be-spinner"><svg width="40px" height="40px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle fill="none" stroke-width="4" stroke-linecap="round" cx="33" cy="33" r="30" class="circle"/></svg></div>'
        html += '</div>';
        $(html).insertAfter(".pimg-div" + previousDivCount);
        $('#imgDivCount').val(nextDivCount);
        $('.pimg_lbl').editable({
            validate: function (value) {
                if ($.trim(value) == '') {
                    return 'This field is required';
                }
            }
        });
        $('.pimg_alt_tag').editable({
            validate: function (value) {
                if ($.trim(value) == '') {
                    return 'This field is required';
                }
            }
        });
        $('.pimg_html_video').editable({
            validate: function (value) {
                if ($.trim(value) == '') {
                    return 'This field is required';
                }
            }
        });
        $('.pimg_360_video').editable({
            validate: function (value) {
                if ($.trim(value) == '') {
                    return 'This field is required';
                }
            }
        });
        $("#pImageDrop_" + nextDivCount).dropzone({
            url: "product-image-upload",
            maxFilesize: 2, // MB
            maxFiles: 1,
            acceptedFiles: 'image/*',
            dictDefaultMessage: "Add Product Image",
            dictInvalidFileType: "Please upload Image files.",
            dictMaxFilesExceeded: "You can not upload any more files.",
            init: function () {
                var img_div_id = $(this.element).attr("id");
                var id_arr = img_div_id.split('_');
                var imgdivnum = id_arr[1];
                this.on("sending", function (file, xhr, formData) {
                    var productId = $('#pID_created').val();
                    // Will send the Product Id(Temp) along with the file as POST data.
                    formData.append("pid", productId);
                });
                this.on("success", function (file, responseText) {
                    var responsetext = JSON.parse(file.xhr.responseText);
                    if (responsetext.status == 'success') {
                        var imgId = responsetext.imgId;
                        var imgLbl = responsetext.imgLabel;
                        $('#upld_img' + imgdivnum).val(imgId);
                        $('#pimg_lbl' + imgdivnum).html(imgLbl);
                        $('.pimg-div' + imgdivnum + ' .bs-grid-block').css('border', '2px dashed #c3c3c3');
                    }
                });
            }
        });
        $('.draggable-element').arrangeable();
    });
    /*Product Create Wizard 1st Step Validation & Move to Next Step*/
    $(".wizard-next1").click(function (e) {
        var id = $(this).data("wizard");
        //General Step Fields
        var pName = $('#pName_create input').val();
        var pSKU = $('#pSKU_create input').val();
        var pUPC = $('#pUPC_create input').val();
        var pEAN = $('#pEAN_create input').val();
        var pJAN = $('#pJAN_create input').val();
        var pISBN = $('#pISBN_create input').val();
        var pMPN = $('#pMPN_create input').val();
        var pDes = $('#product-create-description').summernote('code');
        var pAdult = $('#pAdult_create select option:selected').text();
        var pAgeGroup = $('#pAgeGroup_create select option:selected').text();
        var pAvail = $('#pAvail_create select option:selected').text();
        var pBrand = $('#pBrand_create input').val();
        var pCond = $('#pCond_create select option:selected').text();
        var pGender = $('#pGend_create select option:selected').text();
        var pWeight = $('#pWght_create input').val();



        //General Step Required Fields Validation
        var isValid = true;
        if (pName == '') {
            $('#pName_create').addClass('has-error');
            isValid = false;
        } else {
            if ($('#pName_create').hasClass('has-error')) {
                $('#pName_create').removeClass('has-error');
            }
        }
        if (pSKU == '') {
            $('#pSKU_create').addClass('has-error');
            isValid = false;
        } else {
            if ($('#pSKU_create').hasClass('has-error')) {
                $('#pSKU_create').removeClass('has-error');
            }
        }
        if (pUPC == '') {
            $('#pUPC_create').addClass('has-error');
            isValid = false;
        } else {
            if ($('#pUPC_create').hasClass('has-error')) {
                $('#pUPC_create').removeClass('has-error');
            }
        }
        if (pEAN == '') {
            $('#pEAN_create').addClass('has-error');
            isValid = false;
        } else {
            if ($('#pEAN_create').hasClass('has-error')) {
                $('#pEAN_create').removeClass('has-error');
            }
        }
        if (pJAN == '') {
            $('#pJAN_create').addClass('has-error');
            isValid = false;
        } else {
            if ($('#pJAN_create').hasClass('has-error')) {
                $('#pJAN_create').removeClass('has-error');
            }
        }
        if (pISBN == '') {
            $('#pISBN_create').addClass('has-error');
            isValid = false;
        } else {
            if ($('#pISBN_create').hasClass('has-error')) {
                $('#pISBN_create').removeClass('has-error');
            }
        }
        if (pMPN == '') {
            $('#pMPN_create').addClass('has-error');
            isValid = false;
        } else {
            if ($('#pMPN_create').hasClass('has-error')) {
                $('#pMPN_create').removeClass('has-error');
            }
        }
        if (pBrand == '') {
            $('#pBrand_create').addClass('has-error');
            isValid = false;
        } else {
            if ($('#pBrand_create').hasClass('has-error')) {
                $('#pBrand_create').removeClass('has-error');
            }
        }
        if (pWeight == '') {
            $('#pWght_create').addClass('has-error');
            isValid = false;
        } else {
            if ($('#pWght_create').hasClass('has-error')) {
                $('#pWght_create').removeClass('has-error');
            }
        }



        if (isValid) {

            $.ajax({
                type: 'post',
                url: 'create-product-temp',
                data: {
                    pName: pName,
                    pSKU: pSKU,
                    pUPC: pUPC,
                    pEAN: pEAN,
                    pJAN: pJAN,
                    pISBN: pISBN,
                    pMPN: pMPN,
                    pDes: pDes,
                    pAdult: pAdult,
                    pAgeGroup: pAgeGroup,
                    pAvail: pAvail,
                    pBrand: pBrand,
                    pCond: pCond,
                    pGender: pGender,
                    pWeight: pWeight,
                },
                success: function (pid) {
                    $('#pID_created').val(pid);
                    $(id).wizard('next');
                },
            });
        }
        e.preventDefault();
    });
    /* END*/

    /*Media Step - Product Create Wizard*/
    $(".pImg_Create_Drop1").dropzone({
        url: "product-image-upload",
        maxFilesize: 2, // MB
        maxFiles: 1,
        acceptedFiles: 'image/*',
        dictDefaultMessage: "Add Product Image",
        dictInvalidFileType: "Please upload Image files.",
        dictMaxFilesExceeded: "You can not upload any more files.",
        init: function () {
            var img_div_id = $(this.element).attr("id");
            var id_arr = img_div_id.split('_');
            var imgdivnum = id_arr[1];
            this.on("sending", function (file, xhr, formData) {
                var productId = $('#pID_created').val();
                // Will send the Product Id(Temp) along with the file as POST data.
                formData.append("pid", productId);
            });
            this.on("success", function (file, responseText) {
                var responsetext = JSON.parse(file.xhr.responseText);
                if (responsetext.status == 'success') {
                    var imgId = responsetext.imgId;
                    var imgLbl = responsetext.imgLabel;
                    $('#upld_img' + imgdivnum).val(imgId);
                    $('#pimg_lbl' + imgdivnum).html(imgLbl);
                    $('.pimg-div' + imgdivnum + ' .bs-grid-block').css('border', '2px dashed #c3c3c3');
                }
            });
        }
    });
    /*Upload 360-degree video*/
    $(document).on("click", ".vupld_btn", function (event) {
        event.preventDefault();
        var btn_id = this.id;
        var id_arr = btn_id.split('_');
        var id_num = id_arr[1];
        //Get Image Id
        var img_Id = $('#upld_img' + id_num).val();
        if (img_Id == '') {
            $('.pimg-div' + id_num + ' .bs-grid-block').css('border', '2px dashed red');
            return false;
        } else {
            var img_360 = $('#pimg_360_video' + id_num)[0];
            var videos_count = img_360.files.length;
            if (videos_count == 0) {
                $('.pimg-div' + id_num + ' .inputfile + label').css('border', '1px solid red');
            } else {
                $('.pimg-div' + id_num + ' .inputfile + label').css('border', '1px solid #dedede');
                var file_type = img_360.files[0].type;
                console.log(file_type);
                if (file_type !== "video/x-quicktime" && file_type !== "video/quicktime" &&
                        file_type !== "video/m4v" && file_type !== "video/x-m4v" && file_type !== "video/avi" &&
                        file_type !== "video/mov" && file_type !== "video/x-msvideo" && file_type !== "video/wmv" &&
                        file_type !== "video/x-ms-wmv" && file_type !== "video/mpg" && file_type !== "video/mpeg" &&
                        file_type !== "video/ogv" && file_type !== "video/ogm" && file_type !== "video/ogx" &&
                        file_type !== "video/mp4" && file_type !== "video/ogg" &&
                        file_type !== "video/flv" && file_type !== "video/rm" && file_type !== "video/rmvb" &&
                        file_type !== "video/webm" && file_type !== "video/wmv" && file_type !== "video/xvid" && file_type !== "video/divx") {
                    $('#mod-360video-alert').modal('show');
                } else
                {
                    var img_360_video;
                    img_360_video = new FormData();
                    img_360_video.append('file', img_360.files[0]);
                    $.ajax({
                        xhr: function ()
                        {
                            var xhr = new window.XMLHttpRequest();
                            var prgs = 0;
                            $('#pimg_360_video_progress_wrapper' + id_num).show();
                            //Upload progress
                            xhr.upload.addEventListener("progress", function (event) {
                                if (event.lengthComputable) {
                                    console.log(event.lengthComputable);
                                    console.log(event.loaded);
                                    console.log(event.total);
                                    var percentComplete = event.loaded / event.total;
                                    if (percentComplete < 1) {
                                        if (prgs < 90) {
                                            prgs = parseInt(prgs) + 10;
                                        }
                                    } else {
                                        prgs = 100;
                                    }
                                    //Do something with upload progress
                                    $("#pimg_360_video_progress" + id_num).width(prgs + '%');
                                }
                            }, false);
                            return xhr;
                        },
                        type: 'post',
                        url: 'upload-product-image-video360?imgId=' + img_Id,
                        data: img_360_video,
                        processData: false,
                        contentType: false,
                        success: function (value) {
                            if (value.video_src != '') {
                                $('#pimgDiv_' + id_num + ' .vupld').hide();
                                console.log(value);
                            }
                        },
                    });
                }
            }
        }
    });
    /*Save The Media Step Uploaded Image with Fields - Product Create*/
    $(document).on("click", ".pimg_save_btn", function (e) {

        e.preventDefault();
        var btn_id = this.id;
        var id_arr = btn_id.split('_');
        var id_num = id_arr[1];
        //Get Image Id

        var img_Id = $('#upld_img' + id_num).val();
        if (img_Id == '') {
            $('.pimg-div' + id_num + ' .bs-grid-block').css('border', '2px dashed red');
            return false;
        } else {
            $('.pimg-div' + id_num).addClass('be-loading-active');
            //Get the Particular Image Attributes
            var img_selected_default = $('input:radio[id=pimg_rad' + id_num + ']:checked').length;
            var img_label = $('#pimg_lbl' + id_num).text();
            var img_alt = $('#pimg_alt_tag' + id_num).text();
            var img_html_video = $('#pimg_html_video' + id_num).text();
            $.ajax({
                type: 'post',
                url: 'save-product-image',
                data: {
                    img_Id: img_Id,
                    img_default: img_selected_default,
                    img_label: img_label,
                    img_alt: img_alt,
                    img_html_video: img_html_video,
                },
                success: function (value) {
                    if (value == 'success') {
                        $('.pimg-div' + id_num).removeClass('be-loading-active');
                    }
                },
            });
        }

    });
    /*END*/
    //Image Divs Draggable 
    $('.draggable-element').arrangeable();
    //Save the Order of Images
    $(".pmedia_create_next").click(function (e) {
        e.preventDefault();
        var id = $(this).data("wizard");
        var img_order_array;
        var i = 0;
        $('.pmedia .draggable-element').each(function (index, value) {
            //  alert(index + ": " + $(value).find('.upld_img').val());
            var div_id = value.id;
            var id_arr = div_id.split('_');
            var id_num = id_arr[1];
            var img_order = parseInt(index) + 1;
            var img_Id = $(value).find('.upld_img').val();
            var img_default = $('#pimgRad_' + id_num + ':checked').length;
            if (img_Id != '') {
                i = parseInt(i) + 1;
                $.ajax({
                    type: 'post',
                    url: 'update-product-image-order',
                    data: {
                        ImgId: img_Id,
                        ImgOrder: i,
                        ImgDefault: img_default,
                    },
                    success: function (value) {
                    },
                });
            }
        });
        $(id).wizard('next');
    });
    /*AutoComplete Google Categories Fields Attribution Step - Product Create*/
    $("#pattr_cat1_create input").autocomplete({
        source: 'google-product-category'
    });
    $("#pattr_cat2_create input").autocomplete({
        source: 'google-product-category'
    });
    $("#pattr_cat3_create input").autocomplete({
        source: 'google-product-category'
    });
    /* END*/


    /* Product Create Wizard Completion Step */
    $(".wizard-complete").click(function (e) {
        var pID = $('#pID_created').val();
        var id = $(this).data("wizard");
        //General Step Fields
        var pName = $('#pName_create input').val();
        var pSKU = $('#pSKU_create input').val();
        var pUPC = $('#pUPC_create input').val();
        var pEAN = $('#pEAN_create input').val();
        var pJAN = $('#pJAN_create input').val();
        var pISBN = $('#pISBN_create input').val();
        var pMPN = $('#pMPN_create input').val();
        var pDes = $('#product-create-description').summernote('code');
        var pAdult = $('#pAdult_create select option:selected').text();
        var pAgeGroup = $('#pAgeGroup_create select option:selected').text();
        var pAvail = $('#pAvail_create select option:selected').text();
        var pBrand = $('#pBrand_create input').val();
        var pCond = $('#pCond_create select option:selected').text();
        var pGender = $('#pGend_create select option:selected').text();
        var pWeight = $('#pWght_create input').val();
        var pConnectMarketPlace = $('#pConnectMarketPlace').val();
        //Attribution Step Fields
//        var pCat1 = $('#pattr_cat1_create input').val();
//        var pCat2 = $('#pattr_cat2_create input').val();
//        var pCat3 = $('#pattr_cat3_create input').val();
        var Google_Cat1 = $('#pattr_cat1_create').text();
        var Google_Cat2 = $('#pattr_cat2_create').text();
        var pOccas = $('#pattr_occas_create .be-checkbox input:checked').map(function () {
            return $(this).next("label").text();
        }).get().join(",");
        var pWthr = $('#pattr_weather_create .be-checkbox input:checked').map(function () {
            return $(this).next("label").text();
        }).get().join(",");
        //Categories Step Fields

        //Channel Manager Step Fields

        //Inventory Management Step Fields
        var pStk_qty = $('#pstk_qty_create input').val();
        var pStk_lvl = $('#pstk_lvl_create select option:selected').text();
        var pStk_status = $('#pstk_sts_create select option:selected').text();
        var plow_stk_ntf = $('#plw_stk_ntf_create input').val();
        //Media Step Fields

        //Performance Step Fields

        //Pricing Step Fields
        var pPrice = $('#pprice_create input').val();
        var pSale_price = $('#psale_price_create input').val();
        var pSchedule_date = $('#psched_date2_create input').val();
        var Google_Cat_id = $('#gcatname').val();
        //Translation Step Fields

        //connect store
        var connect_checkbox = document.getElementsByName("connect_store_chk[]");


        var vals = "";
        var n;
        for (var i = 0, n = connect_checkbox.length; i < n; i++)
        {
            if (connect_checkbox[i].checked)
            {
                vals += "," + connect_checkbox[i].value;
            }
        }

        vals = vals.substring(1);

        $.ajax({
            type: 'post',
            url: 'create-product?pID=' + pID,
            data: {
                pName: pName,
                pSKU: pSKU,
                pUPC: pUPC,
                pEAN: pEAN,
                pJAN: pJAN,
                pISBN: pISBN,
                pMPN: pMPN,
                pDes: pDes,
                pAdult: pAdult,
                pAgeGroup: pAgeGroup,
                pAvail: pAvail,
                pBrand: pBrand,
                pCond: pCond,
                pGender: pGender,
                pWeight: pWeight,
                pConnectMarketPlace: pConnectMarketPlace,
                Google_Cat1: Google_Cat1,
                Google_Cat2: Google_Cat2,
                Google_Cat_id: Google_Cat_id,
                pOccasion: pOccas,
                pWeather: pWthr,
                pStk_qty: pStk_qty,
                pStk_lvl: pStk_lvl,
                pStk_status: pStk_status,
                plow_stk_ntf: plow_stk_ntf,
                pPrice: pPrice,
                pSale_price: pSale_price,
                pSchedule_date: pSchedule_date,
                vals: vals,
            },
            success: function (value) {

            },
        });
        $(id).wizard('next');
        e.preventDefault();
    });
    /* Product Create Wizard END*/

    /* Translation tab text editor*/
    App.textEditors = function () {
        //Summernote Description
        $('#translation-description').summernote({
            height: 300
        });
        //Summernote shortdescription
        $('#trans-shortdescription').summernote({
            height: 300
        });
        //Summernote applicationtips
        $('#trans-applicationtips').summernote({
            height: 300
        });
        //Summernote ingredients
        $('#trans-ingredients').summernote({
            height: 300
        });
    };
    /* Translation tab text editor END*/

    /*Store Connection Setup Wizard*/
    $('.wizard-next-connect-store1').click(function (e) {
        var id = $(this).data("wizard");
        var selected_store = $("input[name='store-radio']:checked").attr('id');
        if (selected_store == 'BigCommerce') {
//            $('#bigcommerce-authorize-form').show();
            $(id).wizard('next');
        } else {
            alert("please select Bigcommerce for now!");
        }
        e.preventDefault();
    });
    //BigCommerce Authorization 
    $('.wizard-next-auth-store-bgcmrc').click(function (e) {
        e.preventDefault();
        var id = $(this).data("wizard");
        var api_path = $('#big_api_path input').val();
        var access_token = $('#big_access_token input').val();
        var client_id = $('#big_client_id input').val();
        var client_secret = $('#big_client_secret input').val();
        //Required Fields Validation
        var isValid = true;
        if (!isValidUrl(api_path) || api_path == '') {
            $('#big_api_path').addClass('has-error');
            isValid = false;
        } else {
            if ($('#big_api_path').hasClass('has-error')) {
                $('#big_api_path').removeClass('has-error');
            }
        }
        if (access_token == '') {
            $('#big_access_token').addClass('has-error');
            isValid = false;
        } else {
            if ($('#big_access_token').hasClass('has-error')) {
                $('#big_access_token').removeClass('has-error');
            }
        }
        if (client_id == '') {
            $('#big_client_id').addClass('has-error');
            isValid = false;
        } else {
            if ($('#big_client_id').hasClass('has-error')) {
                $('#big_client_id').removeClass('has-error');
            }
        }

        if (isValid) {
            $.ajax({
                type: 'post',
                url: 'auth-bigcommerce',
                data: {
                    api_path: api_path,
                    access_token: access_token,
                    client_id: client_id,
                    client_secret: client_secret,
                },
                complete: function (data) {
                    $('.be-wrapper').removeClass('be-loading-active');
                    var response_data = data.responseText;
                    var obj = JSON.parse(response_data);
                    console.log(obj);
                    if (obj["store_hash"] != undefined) {
                        var store_hash = obj["store_hash"];
                        $('#bigcommerce-connect-form').show();
                        $('#big_store_hash input').val(store_hash);
                        $(id).wizard('next');
                    }
                    if (obj["api_error"] != undefined) {
                        var html_data_error = obj['api_error'];
                        $("#bigcommerce_ajax_msg_eror").html(html_data_error);
                        $("#bigcommerce_ajax_header_error_msg").html('Error!');
                        $('#bigcommerce_ajax_error_modal').modal('show');
                    }
                },
            });
//                    .done(function (data) {
////                $('#bigcommerce-connect-form').show();
//                $('#big_store_hash input').val(data);
//                $(id).wizard('next');
//            });
            // e.preventDefault();
        }
    });

    $('.reset-shopify-form-on-cancel').click(function (e) {
        e.preventDefault();
        $('#shopify-authorize-form').find('#shopify_shop input').val("");
        $('#shopify-authorize-form').find('#shopify_api input').val("");
        $('#shopify-authorize-form').find('#shopify_pass input').val("");
        $('#shopify-authorize-form').find('#shopify_shared_secret input').val("");

    });

    $('.empty-the-bigcommerce-form').click(function (e) {
        e.preventDefault();
        $('#bigcommerce-authorize-form').find("#big_api_path input").val("");
        $('#bigcommerce-authorize-form').find("#big_access_token input").val("");
        $('#bigcommerce-authorize-form').find("#big_client_id input").val("");
        $('#bigcommerce-authorize-form').find("#big_client_secret input").val("");


    });

    $('.bigcommerce_error_modal_close').click(function () {
        $('#bigcommerce_ajax_error_modal').modal('hide');
    });

    //BigCommerce Connection
    $('.wizard-next-connect-store-bgcmrc').click(function (e) {
        var id = $(this).data("wizard");
        var uid = $('#user_id').val();
        var store_hash = $('#big_store_hash input').val();
        var api_path = $('#big_api_path input').val();
        var access_token = $('#big_access_token input').val();
        var client_id = $('#big_client_id input').val();
        var client_secret = $('#big_client_secret input').val();
        $('.be-wrapper').addClass('be-loading-active');
        $("#connect-modal").modal("show");
        $.ajax({
            type: 'get',
            url: BASE_URL + 'stores/connect-bigcommerce',
            data: {
                uid: uid,
                api_path: api_path,
                access_token: access_token,
                client_id: client_id,
                client_secret: client_secret,
                store_hash: store_hash,
            },
            complete: function (event, xhr, options) {
                //complete: function () {
                var domain = event.responseText;
                console.log(domain);
                console.log(event);
                console.log(xhr);
                console.log(options);
                window.location = 'https://' + domain + '.' + DOMAIN_NAME;
//                $('.be-wrapper').removeClass('be-loading-active');
            },
        });
        e.preventDefault();
    });

    $('#connect-modal').on('hidden.bs.modal', function () {
        $('.be-wrapper').removeClass('be-loading-active');
        $('#bigcommerce-authorize-form').hide();
        $('#bigcommerce-connect-form').hide();
        $('#bgc_text1').show();
        $('#bgc_text2').show();
    });

    /**
     * Shopify connection and validation begin
     */

    function isValidUrl(text) {
        return /\b(http|https)/.test(text);
    }

    $('.wizard-next-auth-store-shopify').click(function (e) {
        e.preventDefault();
        var id = $(this).data("wizard");
        var shopify_shop = $('#shopify_shop input').val();
        var shopify_api = $('#shopify_api input').val();
        var shopify_pass = $('#shopify_pass input').val();
        var shopify_shared_secret = $('#shopify_shared_secret input').val();
        var shopify_plus = $('#enable_shopify_plus').prop('checked');
        var user_id = $('#user_id').val();
        //Required Fields Validation
        var isValid = true;

        if (isValidUrl(shopify_shop) || shopify_shop == '')
        {
            $('#shopify_shop').addClass('has-error');
            isValid = false;
        } else
        {
            if ($('#shopify_shop').hasClass('has-error')) {
                $('#shopify_shop').removeClass('has-error');
            }
        }

        if (shopify_api == '') {
            $('#shopify_api').addClass('has-error');
            isValid = false;
        } else {
            if ($('#shopify_api').hasClass('has-error')) {
                $('#shopify_api').removeClass('has-error');
            }
        }
        if (shopify_pass == '') {
            $('#shopify_pass').addClass('has-error');
            isValid = false;
        } else {
            if ($('#shopify_pass').hasClass('has-error')) {
                $('#shopify_pass').removeClass('has-error');
            }
        }
        if (shopify_shared_secret == '') {
            $('#shopify_shared_secret').addClass('has-error');
            isValid = false;
        } else {
            if ($('#shopify_shared_secret').hasClass('has-error')) {
                $('#shopify_shared_secret').removeClass('has-error');
            }
        }


        if (isValid) {
            $('.be-wrapper').addClass('be-loading-active');
            $.ajax({
                type: 'post',
                url: 'auth-shopify',
                data: {
                    shopify_shop: shopify_shop,
                    shopify_api: shopify_api,
                    shopify_pass: shopify_pass,
                    shopify_shared_secret: shopify_shared_secret,
                    shopify_plus: shopify_plus,
                    user_id: user_id,
                },
            }).done(function (data) {
                //alert(data);
                var obj = JSON.parse(data);
                //console.log(obj);
                $('.be-wrapper').removeClass('be-loading-active');
                if (obj["api_error"] != undefined)
                {
                    var html_data_error = obj['api_error'];
                    $("#shopify_ajax_msg_eror").html(html_data_error);
                    $("#ajax_header_error_msg").html('Error!');
                    $('.shipify_ajax_request_error').modal('show');
                }
                if (obj["success"] != undefined)
                {
                    var html_data = obj['success'];
                    var store_connection_id = obj['store_connection_id'];
                    var store_name = obj['store_name'];
                    var domain_name = obj['domain_name'];
                    $("#shopify_ajax_msg").html(html_data);
                    $("#ajax_header_msg").html('Success!');
                    $('#shopify-authorize-form').hide();
                    $('#shipify_ajax_request').modal('show');
                    $('#shopify-authorize-form')[0].reset();
                    $.ajax({
                        type: 'get',
                        url: BASE_URL + 'stores/shopify-import',
                        data: {
                            user_id: user_id,
                            store_connection_id: store_connection_id,
                            store_name: store_name,
                            domain_name: domain_name,
                        },
                        complete: function (data) {
                            //console.log(data);
                            //window.location.reload();
                            window.location = '/';
                            //abc
                        }
                    });
                }
                if (obj["error"] != undefined)
                {
                    var html_data = obj['error'];
                    $("#shopify_ajax_msg").html(html_data);
                    $("#shopify_ajax_msg_eror").html(html_data);
                    $("#ajax_header_error_msg").html('Error!');
                    $('#shipify_ajax_request_error').modal('show');
                }


            });
        }
    });

    $('#shipify_ajax_request .mdi-close').click(function () {
        $('#shipify_ajax_request').modal('hide');
        window.location = '/';
    });

    $('#shipify_ajax_request .shipify_modal_close').click(function () {
        $('#shipify_ajax_request').modal('hide');
        window.location = '/';
    });

    $('.shipify_error_modal_close').click(function () {
        $('.shipify_ajax_request_error').modal('hide');
    });


    /**
     * Shopify connection and validation End
     */


    /**
     * Magento connection and validation begin
     */

    $('.wizard-next-auth-store-magento').click(function (e) {
        e.preventDefault();
        //var id = $(this).data("wizard");
        var magento_shop = $('#magento_shop input').val();
        var magento_soap_user = $('#magento_soap_user input').val();
        var magento_soap_api = $('#magento_soap_api input').val();
        var magento_country = $('#magento_country').val();

        var user_id = $('#user_id').val();
        //Required Fields Validation
        var isValid = true;

        if (!isValidUrl(magento_shop) || magento_shop == '')
        {
            $('#magento_shop').addClass('has-error');
            isValid = false;
        } else
        {
            if ($('#magento_shop').hasClass('has-error')) {
                $('#magento_shop').removeClass('has-error');
            }
        }

        if (magento_soap_user == '') {
            $('#magento_soap_user').addClass('has-error');
            isValid = false;
        } else {
            if ($('#magento_soap_user').hasClass('has-error')) {
                $('#magento_soap_user').removeClass('has-error');
            }
        }
        if (magento_soap_api == '') {
            $('#magento_soap_api').addClass('has-error');
            isValid = false;
        } else {
            if ($('#magento_soap_api').hasClass('has-error')) {
                $('#magento_soap_api').removeClass('has-error');
            }
        }
        if (magento_country == '') {
            $('#wizard_magento_country').addClass('has-error');
            isValid = false;
        } else {
            if ($('#wizard_magento_country').hasClass('has-error')) {
                $('#wizard_magento_country').removeClass('has-error');
            }
        }

        if (isValid) {
            $('.be-wrapper').addClass('be-loading-active');
            $.ajax({
                type: 'post',
                url: '/stores/auth-magento',
                data: {
                    magento_shop: magento_shop,
                    magento_soap_user: magento_soap_user,
                    magento_soap_api: magento_soap_api,
                    user_id: user_id,
                    magento_country: magento_country,
                },
            }).done(function (data) {
                var obj = JSON.parse(data);
                console.log(obj);
                $('.be-wrapper').removeClass('be-loading-active');

                if (obj["success"] != undefined)
                {
                    var html_data = obj['success'];
                    var store_connection_id = obj['store_connection_id'];
                    $("#magento_ajax_msg").html(html_data);
                    $("#ajax_header_msg").html('Success!');
                    $('#magento-authorize-form').hide();
                    $('#magento_ajax_request').css({
                        'display': 'block',
                        'background': 'rgba(0,0,0,0.6)',
                    });
                    $('#magento-authorize-form')[0].reset();
                    $.ajax({
                        type: 'get',
                        url: BASE_URL + '/stores/magento-importing',
                        data: {
                            user_id: user_id,
                            store_connection_id: store_connection_id,
                            magento_country_code: magento_country,
                        },
                        complete: function (data) {
                            //window.location.reload();
                            //window.location = '/';
                        }
                    });
                }
                if (obj["api_error"] != undefined)
                {
                    var html_data_error = obj['api_error'];
                    $("#magento_ajax_msg_eror").html(html_data_error);
                    $("#ajax_header_error_msg").html('Error!');
                    $('.magento_ajax_request_error').css({
                        'display': 'block',
                        'background': 'rgba(0,0,0,0.6)',
                    });
                }
                if (obj["error"] != undefined)
                {
                    var html_data = obj['error'];
                    $("#magento_ajax_msg").html(html_data);
                    $("#magento_ajax_msg_eror").html(html_data);
                    $("#ajax_header_error_msg").html('Error!');
                    $('.magento_ajax_request_error').css({
                        'display': 'block',
                        'background': 'rgba(0,0,0,0.6)',
                    });
                }
            });
            // e.preventDefault();
        }
    });

    $('.magento_error_modal_close').click(function () {
        $('.magento_ajax_request_error').css({
            'display': 'none',
            'background': 'rgba(0,0,0,0.6)',
        });
    });

    $('.magento_modal_close').click(function () {
        $('#magento_ajax_request').css({
            'display': 'none',
            'background': 'rgba(0,0,0,0.6)',
        });
        window.location.reload()
    });


    /**
     * Magento connection and validation End
     */


    /**
     * Magento-2 connection and validation begin
     */

    $('.wizard-next-auth-store-magento_2').click(function (e) {
        $(this).css('pointer-events', 'none');
        e.preventDefault();
        //var id = $(this).data("wizard");
        var magento_2_shop = $('#magento_2_shop input').val();
        var magento_2_access_token = $('#magento_2_access_token input').val();
        var user_id = $('#user_id').val();
        var magento_2_country = $('#magento_2_country').val();
        //Required Fields Validation
        var isValid = true;

        if (!isValidUrl(magento_2_shop) || magento_2_shop == '')
        {
            $('#magento_2_shop').addClass('has-error');
            $(this).css('pointer-events', 'auto');
            isValid = false;
        } else
        {
            if ($('#magento_2_shop').hasClass('has-error')) {
                $('#magento_2_shop').removeClass('has-error');
            }
        }
        if (magento_2_access_token == '') {
            $('#magento_2_access_token').addClass('has-error');
            $(this).css('pointer-events', 'auto');
            isValid = false;
        } else {
            if ($('#magento_2_access_token').hasClass('has-error')) {
                $('#magento_2_access_token').removeClass('has-error');
            }
        }
        if (magento_2_country == '') {
            $('#wizard_magento_2_country').addClass('has-error');
            isValid = false;
        } else {
            if ($('#wizard_magento_2_country').hasClass('has-error')) {
                $('#wizard_magento_2_country').removeClass('has-error');
            }
        }
        if (isValid) {
            $('.wizard-next-auth-store-magento_2').css('pointer-events', 'auto');
            $('.be-wrapper').addClass('be-loading-active');
            $.ajax({
                type: 'post',
                url: '/magento2/auth-magento2',
                data: {
                    magento_2_shop: magento_2_shop,
                    magento_2_access_token: magento_2_access_token,
                    magento_2_country: magento_2_country,
                    user_id: user_id,
                },
            }).done(function (data) {
                var obj = JSON.parse(data);
                console.log(obj);
                $('.be-wrapper').removeClass('be-loading-active');
                if (obj["success"] != undefined) {
                    var html_data = obj['success'];
                    var store_connection_id = obj['store_connection_id'];
                    $("#magento_2_ajax_msg").html(html_data);
                    $("#ajax_header_msg").html('Success!');
                    $('#magento-2-authorize-form').hide();
                    $('#magento_2_ajax_request').css({
                        'display': 'block',
                        'background': 'rgba(0,0,0,0.6)',
                    });
                    $('#magento-2-authorize-form')[0].reset();
                    $.ajax({
                        type: 'get',
                        url: BASE_URL + 'magento2/magento2-importing',
                        data: {
                            user_id: user_id,
                            store_connection_id: store_connection_id,
                            magento_2_country: magento_2_country,
                        },
                    }).done(function (data) {
                        //alert(data);
                        //window.location.reload();
                    });
                }
                if (obj["api_error"] != undefined)
                {
                    var html_data_error = obj['api_error'];
                    $("#magento_2_ajax_msg_eror").html(html_data_error);
                    $("#ajax_header_error_msg").html('Error!');
                    $('.magento_2_ajax_request_error').css({
                        'display': 'block',
                        'background': 'rgba(0,0,0,0.6)',
                    });
                }
                if (obj["error"] != undefined)
                {
                    var html_data = obj['error'];
                    $("#magento_2_ajax_msg_eror").html(html_data);
                    $("#ajax_header_error_msg").html(html_data);
                    $("#ajax_header_error_msg").html('Error!');
                    $('.magento_2_ajax_request_error').css({
                        'display': 'block',
                        'background': 'rgba(0,0,0,0.6)',
                    });
                }
            });
            // e.preventDefault();
        }
    });

    $('.magento_2_error_modal_close').click(function () {
        $('.magento_2_ajax_request_error').css({
            'display': 'none',
            'background': 'rgba(0,0,0,0.6)',
        });
    });

    $('.magento_2_modal_close').click(function () {
        $('#magento_2_ajax_request').css({
            'display': 'none',
            'background': 'rgba(0,0,0,0.6)',
        });
        window.location.reload()
    });


    /**
     * Magento-2 connection and validation End
     */


    /**
     * Vtex connection and validation begin
     */

    $('.vtex_auth_connection').click(function (e) {
        $(this).css('pointer-events', 'none');
        e.preventDefault();
        var vtex_account = $('#vtex_account input').val();
        var vtex_app_key = $('#vtex_app_key input').val();
        var vtex_app_token = $('#vtex_app_token input').val();
        var vtex_country = $('#vtex_country').val();
        var user_id = $('#user_id').val();
        //Required Fields Validation
        var isValid = true;

        if (isValidUrl(vtex_account) || vtex_account == '')
        {
            $('#vtex_account').addClass('has-error');
            $(this).css('pointer-events', 'auto');
            isValid = false;
        } else
        {
            if ($('#vtex_account').hasClass('has-error')) {
                $('#vtex_account').removeClass('has-error');
            }
        }
        if (vtex_app_key == '') {
            $('#vtex_app_key').addClass('has-error');
            $(this).css('pointer-events', 'auto');
            isValid = false;
        } else {
            if ($('#vtex_app_key').hasClass('has-error')) {
                $('#vtex_app_key').removeClass('has-error');
            }
        }
        if (vtex_app_token == '') {
            $('#vtex_app_token').addClass('has-error');
            isValid = false;
        } else {
            if ($('#vtex_app_token').hasClass('has-error')) {
                $('#vtex_app_token').removeClass('has-error');
            }
        }

        if (isValid) {
            $('.vtex_auth_connection').css('pointer-events', 'auto');
            $('.be-wrapper').addClass('be-loading-active');
            $.ajax({
                type: 'post',
                url: '/vtex/auth-vtex',
                data: {
                    vtex_account: vtex_account,
                    vtex_app_key: vtex_app_key,
                    vtex_app_token: vtex_app_token,
                    user_id: user_id,
                    vtex_country: vtex_country
                },
            }).done(function (data) {
                var obj = JSON.parse(data);
                console.log(obj);
                $('.be-wrapper').removeClass('be-loading-active');
                if (obj["success"] != undefined) {
                    var html_data = obj['success'];
                    var store_connection_id = obj['store_connection_id'];
                    $("#vtex_ajax_msg").html(html_data);
                    $("#vtex_ajax_header_msg").html('Success!');
                    $('#vtex-authorize-form').hide();
                    $('#vtex_ajax_request').modal('show');
                    $('#vtex-authorize-form')[0].reset();
                    $.ajax({
                        type: 'get',
                        url: BASE_URL + 'vtex/vtex-importing',
                        data: {
                            user_id: user_id,
                            store_connection_id: store_connection_id,
                            vtex_country: vtex_country
                        },
                    }).done(function (data) {
                        //alert(data);
                        //window.location.reload();
                    });
                }
                if (obj["api_error"] != undefined) {
                    var html_data_error = obj['api_error'];
                    $("#vtex_ajax_msg_eror").html(html_data_error);
                    $("#vtex_ajax_header_error_msg").html('Error!');
                    $('.vtex_ajax_request_error').modal('show');
                }
            });
            // e.preventDefault();
        }
    });

    $('.vtex_error_modal_close').click(function () {
        $('.vtex_ajax_request_error').modal('hide');
    });

    $('.vtex_modal_close').click(function () {
        $('#vtex_ajax_request').modal('hide');
        window.location.reload()
    });


    /**
     * Vtex connection and validation End
     */

    /**
     * Square connection and validation begin
     */

    $('.square-auth-channel').click(function (e) {
        $(this).css('pointer-events', 'none');
        e.preventDefault();
        var square_application_id = $('#square_application_id input').val();
        var square_personal_access_token = $('#square_personal_access_token input').val();
        var user_id = $('#user_id').val();
        //Required Fields Validation
        var isValid = true;

        if (square_application_id == '')
        {
            $('#square_application_id').addClass('has-error');
            $(this).css('pointer-events', 'auto');
            isValid = false;
        } else
        {
            if ($('#square_application_id').hasClass('has-error')) {
                $('#square_application_id').removeClass('has-error');
            }
        }
        if (square_personal_access_token == '') {
            $('#square_personal_access_token').addClass('has-error');
            $(this).css('pointer-events', 'auto');
            isValid = false;
        } else {
            if ($('#square_personal_access_token').hasClass('has-error')) {
                $('#square_personal_access_token').removeClass('has-error');
            }
        }

        if (isValid) {
            $('.square-auth-channel').css('pointer-events', 'auto');
            $('.be-wrapper').addClass('be-loading-active');
            $.ajax({
                type: 'post',
                url: '/square/square-auth',
                data: {
                    square_application_id: square_application_id,
                    square_personal_access_token: square_personal_access_token,
                    user_id: user_id,
                },
            }).done(function (data) {
                var obj = JSON.parse(data);
                console.log(obj);
                $('.be-wrapper').removeClass('be-loading-active');
                if (obj["success"] != undefined)
                {
                    var html_data = obj['success'];
                    $("#square_ajax_msg").html(html_data);
                    $("#square_ajax_header_msg").html('Success!');
                    $('#square-authorize-form').hide();
                    $('#square_ajax_request').css({
                        'display': 'block',
                        'background': 'rgba(0,0,0,0.6)',
                    });
                    $('#square-authorize-form')[0].reset();
                    $.ajax({
                        type: 'get',
                        url: BASE_URL + 'square/square-importing',
                        data: {
                            user_id: user_id,
                        },
                    }).done(function (data) {
                        //alert(data);
                        window.location.reload();
                    });
                }
                if (obj["api_error"] != undefined)
                {
                    var html_data_error = obj['api_error'];
                    $("#square_ajax_msg_eror").html(html_data_error);
                    $("#square_ajax_header_error_msg").html('Error!');
                    $('.square_ajax_request_error').css({
                        'display': 'block',
                        'background': 'rgba(0,0,0,0.6)',
                    });
                }
            });
            // e.preventDefault();
        }
    });

    $('.square_error_modal_close').click(function () {
        $('.square_ajax_request_error').css({
            'display': 'none',
            'background': 'rgba(0,0,0,0.6)',
        });
    });

    $('.square_modal_close').click(function () {
        $('#square_ajax_request').css({
            'display': 'none',
            'background': 'rgba(0,0,0,0.6)',
        });
        window.location.reload()
    });


    /**
     * Square connection and validation End
     */

    /**
     * Jet connection and validation
     */
    /**
     * Square connection and validation begin
     */

    $('.jet-auth-channel').click(function (e) {
        e.preventDefault();
        $(this).css('pointer-events', 'none');
        var jet_api_user = $('#jet_api_user input').val();
        var jet_secret_key = $('#jet_secret_key input').val();
        var jet_merchant_id = $('#jet_merchant_id input').val();
        var user_id = $('#user_id').val();
        //Required Fields Validation
        var isValid = true;

        if (jet_api_user == '')
        {
            $('#jet_api_user').addClass('has-error');
            $(this).css('pointer-events', 'auto');
            isValid = false;
        } else
        {
            if ($('#jet_api_user').hasClass('has-error')) {
                $('#jet_api_user').removeClass('has-error');
            }
        }
        if (jet_secret_key == '') {
            $('#jet_secret_key').addClass('has-error');
            $(this).css('pointer-events', 'auto');
            isValid = false;
        } else {
            if ($('#jet_secret_key').hasClass('has-error')) {
                $('#jet_secret_key').removeClass('has-error');
            }
        }
        if (jet_merchant_id == '') {
            $('#jet_merchant_id').addClass('has-error');
            $(this).css('pointer-events', 'auto');
            isValid = false;
        } else {
            if ($('#jet_merchant_id').hasClass('has-error')) {
                $('#jet_merchant_id').removeClass('has-error');
            }
        }



        if (isValid) {
            $('.jet-auth-channel').css('pointer-events', 'auto');
            $('.be-wrapper').addClass('be-loading-active');
            $.ajax({
                type: 'post',
                url: '/jet/jet-auth',
                data: {
                    jet_api_user: jet_api_user,
                    jet_secret_key: jet_secret_key,
                    jet_merchant_id: jet_merchant_id,
                    user_id: user_id,
                },
                complete: function (data) {
                    var response_data = data.responseText;
                    var obj = JSON.parse(response_data);
                    console.log(obj);
                    if (obj["success"] != undefined) {
                        var html_data = obj['success'];
                        $("#jet_ajax_msg").html(html_data);
                        $("#jet_ajax_header_msg").html('Success!');
                        $('#jet-authorize-form').hide();
                        $('#jet_ajax_request').modal('show');
                        $.ajax({
                            type: 'get',
                            url: BASE_URL + 'jet/jet-importing',
                            data: {
                                user_id: user_id,
                            },
                            complete: function (data) {
                                window.location.reload();
                            }
                        });
                    }
                    if (obj["api_error"] != undefined) {
                        var html_data_error = obj['api_error'];
                        $("#jet_ajax_msg_eror").html(html_data_error);
                        $("#jet_ajax_header_error_msg").html('Error!');
                        $('#jet_ajax_error_modal').modal('show');
                    }
                    $('.be-wrapper').removeClass('be-loading-active');
                },
            });
            // e.preventDefault();
        }
    });

    $('.jet_error_modal_close').click(function () {
        $('#jet_ajax_error_modal').modal('hide');
    });

    $('.jet_modal_close').click(function () {
        $('#jet_ajax_request').modal('hide');
        window.location.reload();
    });

    /**
     * Jet connection and validation end
     */


    /**
     * End For product multiple chekcbox
     */

    $(".product_multiple_check").change(function () {  //"select all" change 
        $(".product_row_check").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
//        if($(".product_multiple_check").prop('checked')==true){
//            $('#dropdown_delete').addClass('open');
//        }
//        else{
//            $('#dropdown_delete').removeClass('open');
//        }
    });

    $("body").on('change', ".product_row_check", function () {
        var atLeastOneIsChecked = $('.product_row_check:checkbox:checked').length;
//        if(atLeastOneIsChecked==0){
//            $('#dropdown_delete').removeClass('open');
//        }
//        else{
//            $('#dropdown_delete').addClass('open');
//        }
        //uncheck "select all", if one of the listed checkbox item is unchecked
        if (false == $(this).prop("checked")) { //if this item is unchecked
            $(".product_multiple_check").prop('checked', false); //change "select all" checked status to false
        }
        //check "select all" if all checkbox items are checked
        if ($('.product_row_check:checked').length == $('.product_row_check').length) {
            $(".product_multiple_check").prop('checked', true);
        }
    });
    $("body").on('click', "#product_delete_button", function () {
        $("#product-delete-modal-warning .close").click();
        $('.productTABLE').addClass('be-loading-active');
        var checkedProductId = $('input[name=product_check]:checked').map(function () {
            return $(this).val();
        }).get();
        $.ajax({
            type: 'POST',
            url: '/products/ajax-product-delete',
            data: {
                productIds: checkedProductId,
            },
        }).done(function (data) {
            setTimeout(function () {
                $('.productTABLE').removeClass('be-loading-active');
                window.location.reload();
            }, 4000);

        });
    });
    $("body").on('click', '#connect_product_delete_button', function () {
        $("#connect-product-delete-modal-warning .close").click();
        $(".ConnectedProDUCTS").addClass('be-loading-active');
        var checkedProductID = $('input[name=inactive_product_check]:checked').map(function () {
            return $(this).val();
        }).get();
        $.ajax({
            type: 'POST',
            url: '/products/ajax-product-delete',
            data: {
                productIds: checkedProductID
            }
        }).done(function (data) {
            setTimeout(function () {
                $('.ConnectedProDUCTS').removeClass('be-loading-active');
                window.location.reload();
            }, 4000);

        });

    })

    $("body").on('click', "#inactive_product_delete_button", function () {
        $("#inactive-product-delete-modal-warning .close").click();
        $('.InactiVeProDUCTS').addClass('be-loading-active');
        var checkedProductID = $('input[name=inactive_product_check]:checked').map(function () {
            return $(this).val();
        }).get();
        $.ajax({
            type: 'POST',
            url: '/products/ajax-inactive-product-delete',
            data: {
                checkedProductID: checkedProductID
            }
        }).done(function (data) {
            setTimeout(function () {
                $('.InactiVeProDUCTS').removeClass('be-loading-active');
                window.location.reload();
            }, 4000);


        });
    });
    /**
     * End For product multiple chekcbox
     */
    /*
     * multiple selection of the data table on order
     */

    $(".order_multiple_check").change(function () {  //"select all" change 
        $(".order_row_check").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
//        if($(".order_multiple_check").prop('checked')==true){
//            $('#dropdown_delete').addClass('open');
//        }
//        else{
//            $('#dropdown_delete').removeClass('open');
//        }
    });

    $("body").on('change', ".order_row_check", function () {
        var atLeastOneIsChecked = $('.order_row_check:checkbox:checked').length;
//        if(atLeastOneIsChecked==0){
//            $('#dropdown_delete').removeClass('open');
//        }
//        else{
//            $('#dropdown_delete').addClass('open');
//        }
        //uncheck "select all", if one of the listed checkbox item is unchecked
        if (false == $(this).prop("checked")) { //if this item is unchecked
            $(".order_multiple_check").prop('checked', false); //change "select all" checked status to false
        }
        //check "select all" if all checkbox items are checked
        if ($('.order_row_check:checked').length == $('.order_row_check').length) {
            $(".order_multiple_check").prop('checked', true);
        }
    });
    $("body").on('click', "#order_delete_button", function () {
        $("#order-delete-modal-warning .close").click();
        $('.orderListTAble ').addClass('be-loading-active');
        var checkedOrderId = $('input[name=order_check]:checked').map(function () {
            return $(this).val();
        }).get();
        $.ajax({
            type: 'POST',
            url: '/orders/ajax-orders-delete',
            data: {
                orderIds: checkedOrderId,
            },
        }).done(function (data) {
            setTimeout(function () {
                $('.orderListTAble').removeClass('be-loading-active');
                window.location.reload();
            }, 4000);

        });
    });

    $("body").on('click', "#connect_order_delete_button", function () {
        $("#connect-order-delete-modal-warning .close").click();
        $('.connectedOrDERS ').addClass('be-loading-active');
        var checkedOrderId = $('input[name=order_check]:checked').map(function () {
            return $(this).val();
        }).get();
        $.ajax({
            type: 'POST',
            url: '/orders/ajax-orders-delete',
            data: {
                orderIds: checkedOrderId,
            },
        }).done(function (data) {
            setTimeout(function () {
                $('.connectedOrDERS').removeClass('be-loading-active');
                window.location.reload();
            }, 4000);

        });
    });
    $("body").on("click", "#inactive_order_delete_button", function () {
        $("#inactive-order-delete-modal-warning .close").click();
        $('.inactiveOrDERS').addClass('be-loading-active');
        var checkedOrder = $('input[name=inactiveorder_check]:checked').map(function () {
            return $(this).val();
        }).get();
        $.ajax({
            type: 'POST',
            url: '/orders/ajax-inactive-orders-deletion',
            data: {
                order_id: checkedOrder
            }
        }).done(function (data) {
            setTimeout(function () {
                $('.inactiveOrDERS').removeClass('be-loading-active');
                window.location.reload();
            }, 4000);

        });
    });


    /**
     * End For order multiple chekcbox
     */
    /*
     * multiple selection of the data table on people
     */
    $(".people_multiple_check").change(function () {  //"select all" change 
        $(".people_row_check").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
//        if($(".order_multiple_check").prop('checked')==true){
//            $('#dropdown_delete').addClass('open');
//        }
//        else{
//            $('#dropdown_delete').removeClass('open');
//        }
    });

    $("body").on('change', ".people_row_check", function () {
        var atLeastOneIsChecked = $('.people_row_check:checkbox:checked').length;
//        if(atLeastOneIsChecked==0){
//            $('#dropdown_delete').removeClass('open');
//        }
//        else{
//            $('#dropdown_delete').addClass('open');
//        }
        //uncheck "select all", if one of the listed checkbox item is unchecked
        if (false == $(this).prop("checked")) { //if this item is unchecked
            $(".people_multiple_check").prop('checked', false); //change "select all" checked status to false
        }
        //check "select all" if all checkbox items are checked
        if ($('.people_row_check:checked').length == $('.people_row_check').length) {
            $(".people_multiple_check").prop('checked', true);
        }
    });
    $("body").on('click', "#people_delete_button", function () {
        $("#people-delete-modal-warning .close").click();
        $('.customerListTAble').addClass('be-loading-active');
        var checkedPeopleId = $('input[name=people_check]:checked').map(function () {
            return $(this).val();
        }).get();
        $.ajax({
            type: 'POST',
            url: '/channels/ajax-people-delete',
            data: {
                peopleIds: checkedPeopleId,
            },
        }).done(function (data) {
            setTimeout(function () {
                $('.customerListTAble').removeClass('be-loading-active');
                window.location.reload();
            }, 4000);

        });
    });
    $("body").on("click", "#connected_people_delete_button", function () {
        $("#connected-people-delete-modal-warning .close").click();
        $('.connectedcustomerLIST').addClass('be-loading-active');
        var checkedPeopleId = $('input[name=people_check]:checked').map(function () {
            return $(this).val();
        }).get();
        $.ajax({
            type: 'POST',
            url: '/channels/ajax-people-delete',
            data: {
                peopleIds: checkedPeopleId,
            },
        }).done(function (data) {
            setTimeout(function () {
                $('.connectedcustomerLIST').removeClass('be-loading-active');
                window.location.reload();
            }, 4000);

        });


    });
    $("body").on('click', '#inactive_people_delete_button', function () {
        $("#inactive-people-delete-modal-warning .close").click();
        $('.inactivecustomerLIST').addClass('be-loading-active');
        var checkedPeopleId = $('input[name=people_check]:checked').map(function () {
            return $(this).val();
        }).get();
        $.ajax({
            type: 'POST',
            url: '/channels/ajax-people-undo-delete',
            data: {
                Inactive_peopleIds: checkedPeopleId,
            },
        }).done(function (data) {
            setTimeout(function () {
                $('.inactivecustomerLIST').removeClass('be-loading-active');
                window.location.reload();
            }, 4000)

        });

    });
    /**
     * End For people multiple chekcbox
     */

    /* Mercadolibre connection and validation begin */

    $('.wizard-next-auth-store-mercadolibre').click(function (e) {
        e.preventDefault();
        //var id = $(this).data("wizard");
        var mercadolibre_channel_email = $('#mercadolibre_channel_email').val();
        var mercadolibre_channel_password = $('#mercadolibre_channel_password').val();
        var mercadolibre_channel_client_id = $('#mercadolibre_channel_client_id').val();
        var mercadolibre_channel_secret_id = $('#mercadolibre_channel_secret_id').val();
        var user_id = $('#mercadolibre_user_id').val();

        //Required Fields Validation
        var chk = true;
        $(".mercado_validate").each(function () {
            var val = $.trim($(this).val());
            if (!val) {
                $(this).addClass("inpt_required");
                chk = false;
            } else {
                $(this).removeClass("inpt_required");
            }
        })


        if (chk == true) {
            $('.be-wrapper').addClass('be-loading-active');
            $.ajax({
                type: 'post',
                url: 'auth-mercadolibre',
                data: {
                    mercadolibre_channel_email: mercadolibre_channel_email,
                    mercadolibre_channel_password: mercadolibre_channel_password,
                    mercadolibre_channel_client_id: mercadolibre_channel_client_id,
                    mercadolibre_channel_secret_id: mercadolibre_channel_secret_id,
                    user_id: user_id,
                },
            }).done(function (data) {
                //alert(data);
                var obj = JSON.parse(data);
                console.log(obj);
                $('.be-wrapper').removeClass('be-loading-active');

                if (obj["success"] != undefined)
                {
                    var html_data = obj['success'];
                    $("#mercadolibre_ajax_msg").html(html_data);
                    $("#ajax_header_msg").html('Success!');
                    $('#mercadolibre-authorize-form').hide();
                    $('#mercadolibre_ajax_request').css({
                        'display': 'block',
                        'background': 'rgba(0,0,0,0.6)',
                    });
                    $('#mercadolibre-authorize-form')[0].reset();


                }
                if (obj["api_error"] != undefined)
                {
                    var html_data_error = obj['api_error'];
                    $("#mercadolibre_ajax_msg_eror").html(html_data_error);
                    $("#ajax_header_error_msg").html('Error!');
                    $('.mercadolibre_ajax_request_error').css({
                        'display': 'block',
                        'background': 'rgba(0,0,0,0.6)',
                    });
                }
                if (obj["error"] != undefined)
                {
                    var html_data = obj['error'];
                    $("#mercadolibre_ajax_msg").html(html_data);
                    $("#mercadolibre_ajax_msg_eror").html(html_data);
                    $("#ajax_header_error_msg").html('Error!');
                    $('.mercadolibre_ajax_request_error').css({
                        'display': 'block',
                        'background': 'rgba(0,0,0,0.6)',
                    });
                }
            });
            e.preventDefault();
        }
    });

    /*Mercadolibre connection and validation End*/

    /**
     * WooCommerce connection and validation begin
     */

    $('.wizard-next-auth-store-woocommerce').click(function (e) {
        e.preventDefault();
        //var id = $(this).data("wizard");
        var woocommerce_store_url = $('#woocommerce_store_url').val();
        var woocommerce_consumer = $('#woocommerce_consumer input').val();
        var woocommerce_secret = $('#woocommerce_secret input').val();
        var woocommerce_country = $('#WooCustomerCountRY').val();
        var user_id = $('#woocommerce_user_id').val();

        //Required Fields Validation
        var chk = true;
        if (!isValidUrl(woocommerce_store_url)) {
            $('#woocommerce_url').addClass('has-error');
            chk = false;
        } else {
            if ($('#woocommerce_url').hasClass('has-error')) {
                $('#woocommerce_url').removeClass('has-error');
            }
        }
        $(".customer_validate").each(function () {
            var val = $.trim($(this).val());
            if (!val) {
                $(this).addClass("inpt_required");
                chk = false;
            } else {
                $(this).removeClass("inpt_required");
            }
        })


        if (chk == true) {
            $('.be-wrapper').addClass('be-loading-active');
            $.ajax({
                type: 'post',
                url: 'auth-woocommerce',
                data: {
                    woocommerce_store_url: woocommerce_store_url,
                    woocommerce_consumer: woocommerce_consumer,
                    woocommerce_secret: woocommerce_secret,
                    woocommerce_country: woocommerce_country,
                    user_id: user_id,
                },
            }).done(function (data) {
                //alert(data);
                var obj = JSON.parse(data);
                console.log(obj);
                $('.be-wrapper').removeClass('be-loading-active');

                if (obj["success"] != undefined) {
                    var html_data = obj['success'];
                    var store_connection_id = obj['store_connection_id'];
                    $("#woocommerce_ajax_msg").html(html_data);
                    $("#ajax_header_msg").html('Success!');
                    $('#woocommerce-authorize-form').hide();
                    $('#woocommerce_ajax_request').css({
                        'display': 'block',
                        'background': 'rgba(0,0,0,0.6)',
                    });
                    $('#woocommerce-authorize-form')[0].reset();

                    $.ajax({
                        type: 'get',
                        url: BASE_URL + 'stores/woocommerce-importing',
                        data: {
                            user_id: user_id,
                            store_connection_id: store_connection_id,
                        },
                        // complete: function (event, xhr, options) {
                        complete: function () {
//                            var domain = event.responseText;
//                            console.log(domain);
//                            console.log(event);
//                            console.log(xhr);
//                            console.log(options);
                            window.location = 'https://' + hidden_user_domain + '.' + DOMAIN_NAME;
                        },
                    });
                    e.preventDefault();
                }
                if (obj["api_error"] != undefined)
                {
                    var html_data_error = obj['api_error'];
                    $("#woocommerce_ajax_msg_eror").html(html_data_error);
                    $("#ajax_header_error_msg").html('Error!');
                    $('.woocommerce_ajax_request_error').css({
                        'display': 'block',
                        'background': 'rgba(0,0,0,0.6)',
                    });
                }
                if (obj["error"] != undefined)
                {
                    var html_data = obj['error'];
                    $("#woocommerce_ajax_msg").html(html_data);
                    $("#woocommerce_ajax_msg_eror").html(html_data);
                    $("#ajax_header_error_msg").html('Error!');
                    $('.woocommerce_ajax_request_error').css({
                        'display': 'block',
                        'background': 'rgba(0,0,0,0.6)',
                    });
                }
            });
            e.preventDefault();
        }
    });

    $('.woocommerce_error_modal_close').click(function () {
        $('.woocommerce_ajax_request_error').css({
            'display': 'none',
            'background': 'rgba(0,0,0,0.6)',
        });
    });

    $('.woocommerce_modal_close').click(function () {
        $('#woocommerce_ajax_request').css({
            'display': 'none',
            'background': 'rgba(0,0,0,0.6)',
        });
        window.location.reload()
    });


    /**
     * WooCommerce connection and validation End
     */

    /*Adding Product Variations*/
    $(document).on("change", "#pvar_create select", function (event) {
        var pVar_Id = $('#pvar_create select option:selected').val();
        var pVar_Name = $('#pvar_create select option:selected').text();
        $("#pvaritems_create select").empty();
        $('#pvaritems_create select').append($('<option>', {
            value: '',
            text: 'Please Select'
        }));
        if (pVar_Name !== 'Please Select') {
            $.ajax({
                type: 'post',
                url: 'get-variant-items',
                datatype: 'json',
                data: {
                    pVar_Id: pVar_Id,
                    pVar_Name: pVar_Name,
                },
                success: function (data) {
                    var jsonObject = JSON.parse(data);
                    var result = $.map(jsonObject, function (val, key) {
                        $('#pvaritems_create select').append($('<option>', {
                            value: key,
                            text: val
                        }));
                    });
                }
            });
        }
    });

    /*Notifications*/
    $(document).on("click", ".elliot_notif .icon.mdi.mdi-notifications", function (event) {
        $('.elliot_notif .indicator').hide();
    });
    $(document).on("mouseover", ".elliot_notif .notification-unread", function (event) {
        $(this).find('.notif_check').show();
    });
    $(document).on("mouseout", ".elliot_notif .notification-unread", function (event) {
        $(this).find('.notif_check').hide();
    });

    //Prevent Notifications Dropdown closes on click
//    $(".be-notifications").on("click", function (e) {
//        e.stopPropagation();
//    });
    /*End*/

    $(document).on("click", ".elliot_notif .notif_check", function (event) {
        var total_notf = $('.badge').text();
        var notif_id = this.id;
        $.ajax({
            type: 'post',
            url: '/site/clear-notification',
            datatype: 'json',
            data: {
                notif_id: notif_id,
            },
            success: function (data) {
                $('#li_' + notif_id).hide("slide", {direction: "right"}, 500);
                total_notf = parseInt(total_notf) - 1;
                $('.badge').text(total_notf);
            }
        });
        event.stopPropagation();
    });

    //Editable Fields
    $('#op_set').editable({
        emptytext: 'Please Select'
    });


//    $('.pvarSKU').editable();
    $('.pvarInventory').editable();
    $('.pvarPrice').editable();
    $('.pvarWeight').editable();
//    $('.pvaritems').editable();

    $('.pvariation_tbl_body').hide();

    /*Variations Step - Product Create*/
    $(document).on("click", ".add_pvar", function (event) {
        var pVar_Id = $('#pvar_create select option:selected').val();
        var pVar_Name = $('#pvar_create select option:selected').text();
        var pVarItem_Id = $('#pvaritems_create select option:selected').val();
        var pVarItem_Name = $('#pvaritems_create select option:selected').text();
        var columns_added_bfr = $('#columns_added').val();
        var columns_count_bfr = $('#columns_count').val();
        var total_rows = $('#rows_count').val();

        if (pVar_Name == 'Please Select') {
            $('#pvar_create .select2-container--default').css('border', '1px solid red');
            $('#pvaritems_create .select2-container--default').css('border', '1px solid #dedede');
        } else if (pVarItem_Name == 'Please Select') {
            $('#pvar_create .select2-container--default').css('border', '1px solid #dedede');
            $('#pvaritems_create .select2-container--default').css('border', '1px solid red');
        } else {
            var varheadersArr = [];
            $('.pvariation_tbl_body').show();
            $('#pvar_create .select2-container--default').css('border', '1px solid #dedede');
            $('#pvaritems_create .select2-container--default').css('border', '1px solid #dedede');
            /*HTML for SKU,Inventory,Price,Weight,Actions Columns*/
            var sku_html = '<a id="pvarSKU_' + pVarItem_Id + '" class="pvarSKU" href="#" data-type="text" data-title="Please Enter value (must be unique)" ></a>';
            var inv_html = '<a id="pvarInv_' + pVarItem_Id + '" class="pvarInventory" href="#" data-type="text" data-title="Please Enter value (must be unique)" ></a>';
            var price_html = '<a id="pvarPrice_' + pVarItem_Id + '" class="pvarPrice" href="#" data-type="text" data-title="Please Enter value (must be unique)" ></a>';
            var weight_html = '<a id="pvarWeight_' + pVarItem_Id + '" class="pvarWeight" href="#" data-type="text" data-title="Please Enter value (must be unique)" ></a>';
            var actions_html = '<a id="pvarActions_' + pVarItem_Id + '" class="pvarActions icon" href="#" title="Remove Variant"><i class="mdi mdi-delete"></i></a>';
            var table_th = $('#pvariation_tbl th').eq(0).text();
            //When 1st variation Item Added
            if (table_th == "Variant Name") {
                $('#pvariation_tbl th').eq(0).text(pVar_Name);
                $('#pvariation_tbl tbody tr:first').html('<td>' + pVarItem_Name + '</td><td>' + sku_html + '</td><td>' + inv_html + '</td><td>' + price_html + '</td><td>' + weight_html + '</td><td>' + actions_html + '</td>');
                $('#pvariation_tbl tbody tr:first').attr('id', 'Elli_' + total_rows);
                $('#rows_count').val(parseInt(total_rows) + 1);
                if (columns_added_bfr == '') {
                    var columns_added_new = pVar_Name;
                } else {
                    var columns_added_new = columns_added_bfr + ',' + pVar_Name;
                }
                $('#columns_added').val(columns_added_new);
                $('#columns_count').val(parseInt(columns_count_bfr) + 1);
            } else {
                var varArr = [];
                $('#pvariation_tbl th').each(function (i) {
                    var th_val = $('#pvariation_tbl th').eq(i).text();
                    varArr.push(th_val);
                });
                //When new variation column added before pre Variation Item
                if ($.inArray(pVar_Name, varArr) == -1) {
                    $('#pvariation_tbl thead th:first-child').before('<th class="' + pVar_Name + '">' + pVar_Name + '</th>');
                    $('#pvariation_tbl tbody tr').each(function () {
                        $(this).find('td:first').before('<td>' + pVarItem_Name + '</td>');
                    });
                    if (columns_added_bfr == '') {
                        var columns_added_new = pVar_Name;
                    } else {
                        var columns_added_new = columns_added_bfr + ',' + pVar_Name;
                    }
                    $('#columns_added').val(columns_added_new);
                    $('#columns_count').val(parseInt(columns_count_bfr) + 1);
                } else {
                    var columnsArr = [];
                    if (columns_added_bfr.indexOf(',') == -1) {
                        columnsArr.push(columns_added_bfr);
                    } else {
                        columnsArr = columns_added_bfr.split(',');
                    }
                    var countArr = columnsArr.length;
                    var html = '';
                    html = '<tr id="Elli_' + total_rows + '">';
                    for (var i = parseInt(countArr) - 1; i >= 0; i--) {
                        if (columnsArr[i] == pVar_Name) {
                            html += '<td>' + pVarItem_Name + '</td>';
                        } else {
                            html += '<td><a id="pvar_' + columnsArr[i] + '_' + pVarItem_Id + '" class="pvaritems" href="javascript:" data-title="Please Select" data-type="select" data-value="" data-pk="1" class="editable editable-click" data-source="get-varitems?var_name=' + columnsArr[i] + '"></a></td>';
                        }
                    }
                    html += '<td>' + sku_html + '</td>';
                    html += '<td>' + inv_html + '</td>';
                    html += '<td>' + price_html + '</td>';
                    html += '<td>' + weight_html + '</td>';
                    html += '<td>' + actions_html + '</td>';
                    html += '</tr>';
                    $('#pvariation_tbl tr:last').after(html);
                    $('#rows_count').val(parseInt(total_rows) + 1);
                }
            }
            //Editable Fields
            $('.pvarSKU').editable();
            $('.pvarInventory').editable();
            $('.pvarPrice').editable();
            $('.pvarWeight').editable();
            $('.pvaritems').editable();
        }
        event.preventDefault();
    });
    /*End*/

    /*Product Variations Save*/
    $(document).on("click", ".save_pvar", function (event) {
        $('.pvariation_tbl_body').addClass('be-loading-active');
        var pID = $('#pID_created').val();
        var varHeaderArr = [];
        $('#pvariation_tbl th:not(:last-child)').each(function (i) {
            var th_val = $('#pvariation_tbl th').eq(i).text();
            varHeaderArr.push(th_val);
        });
        $('#pvariation_tbl tbody tr').each(function (i) {
            var row_id = $(this).attr('id');
            var varRowArr = [];
            $(this).find('td:not(:last-child)').each(function (j) {
                varRowArr.push($(this).text());
            });
            //Save each Table row data to product_variations table
            $.ajax({
                type: 'post',
                url: 'save-product-variations',
                data: {
                    pID: pID,
                    varHeaderArr: varHeaderArr,
                    row_id: row_id,
                    varRowArr: varRowArr,
                },
                success: function (value) {
                    $('.pvariation_tbl_body').removeClass('be-loading-active');
                },
            });



        });
        event.preventDefault();
    });
    /*End*/

    /*Remove the Variant Row - Product Create*/
    $(document).on("click", ".pvarActions", function (event) {
        $('.pvariation_tbl_body').addClass('be-loading-active');
        var pID = $('#pID_created').val();
        var tr_id = $(this).closest('tr').attr('id');
        $('table#pvariation_tbl tr#' + tr_id).remove();
        //Remove Table row data from product_variations table
        $.ajax({
            type: 'post',
            url: 'remove-product-variations',
            data: {
                pID: pID,
                tr_id: tr_id,
            },
            success: function (value) {
                $('.pvariation_tbl_body').removeClass('be-loading-active');
            },
        });

        event.preventDefault();
    });
    /*End*/
    //$("#skus_tbl").dataTable();
    /*Product Variations Save - Product Update*/
    $(document).on("click", ".save_pvar_update", function (event) {

//        $('#variations .skus').addClass('be-loading-active');
        var pID = $('#pID_created').val();
        var varHeaderArr = [];
//        $('#skus_tbl thead th:not(:first-child):not(:last-child)').each(function (i) {
        $('#skus_tbl thead th:not(:first-child)').each(function (i) {
            var th_val = $(this).text();
            varHeaderArr.push(th_val);
        });
        console.log(varHeaderArr);
        $('#skus_tbl tbody tr').each(function (i) {
            var row_id = $(this).attr('id');
            var varRowArr = [];
            var varOptionNamesArr = [];
            var varOptionValuesArr = [];
            var varSKU = $(this).find('.pvarSKU').text();
            $(this).find('.op_div').each(function (k) {
                var op_name = $(this).find('.option_name').text();
                var op_val = $(this).find('.pvaritems').text();
                varOptionNamesArr.push(op_name);
                varOptionValuesArr.push(op_val);
            });
//             console.log(varOptionNamesArr);
//             console.log(varOptionValuesArr);
//            $(this).find('td:not(:first-child):not(:last-child)').each(function (j) {
            $(this).find('td:not(:first-child)').each(function (j) {
                varRowArr.push($(this).text());
            });
//            console.log(varRowArr);
//             alert(varSKU);
//             alert(row_id);
//             alert(pID);
//            return false;
            //Save each Table row data to product_variations table
            $.ajax({
                type: 'post',
                url: 'save-product-variations',
                data: {
                    pID: pID,
                    varHeaderArr: varHeaderArr,
                    row_id: row_id,
                    varSKU: varSKU,
                    varOptionNamesArr: varOptionNamesArr,
                    varOptionValuesArr: varOptionValuesArr,
                    varRowArr: varRowArr,
                },
                success: function (value) {
                    $('#variations .skus').removeClass('be-loading-active');
                },
            });



        });
        event.preventDefault();
    });
    /*End*/

    /*Remove the Variant Row - Product Update*/
    $(document).on("click", ".pvarActions_update", function (event) {
        $('#variations .skus').addClass('be-loading-active');
        var pID = $('#pID_created').val();
        var tr_id = $(this).closest('tr').attr('id');
        $('table#skus_tbl tr#' + tr_id).remove();
        //Remove Table row data from product_variations table
        $.ajax({
            type: 'post',
            url: 'remove-product-variations',
            data: {
                pID: pID,
                tr_id: tr_id,
            },
            success: function (value) {
                $('#variations .skus').removeClass('be-loading-active');
            },
        });

        event.preventDefault();
    });
    /*End*/


    /*Variations Step - Product Update - Add Variation*/
    $(document).on("click", ".addsku-btn", function (event) {
        var var_set = $('#var_set_tbl #op_set').text();
        //Get the Variant Combinations row using Variation Set value
        $.ajax({
            type: 'post',
            url: 'add-product-variations-row',
            data: {
                var_set: var_set,
            },
            success: function (value) {
                var op_arr = JSON.parse(value);
                var op_html = '';
                $.each(op_arr, function (index, val) {
                    op_html += '<div class="op_div">';
                    op_html += '<span class="option_name">' + val + ': </span>';
                    op_html += '<a class="pvaritems" href="javascript:" data-title="Please Select" data-type="select" data-value="" data-pk="1" class="editable editable-click" data-source="get-varitems?var_name=' + val + '"></a>';
                    op_html += '</div>';
                });
                /*HTML for SKU,Inventory,Price,Weight,Actions Columns*/
                var sku_html = '<a class="pvarSKU" href="#" data-type="text" data-title="Please Enter value (must be unique)" ></a>' + op_html + '';
                var inv_html = '<a class="pvarInventory" href="#" data-type="text" data-title="Please Enter value (must be unique)" ></a>';
                var price_html = '<a class="pvarPrice" href="#" data-type="text" data-title="Please Enter value (must be unique)" ></a>';
                var weight_html = '<a class="pvarWeight" href="#" data-type="text" data-title="Please Enter value (must be unique)" ></a>';
                var actions_html = '<a class="pvarActions_update icon" href="#" title="Remove Variant"><i class="mdi mdi-delete"></i></a>';
                var row_id = $.now();
                $('#skus_tbl tbody tr:first').before('<tr id=Elli_' + row_id + '><td>' + sku_html + '</td><td>' + inv_html + '</td><td>' + price_html + '</td><td>' + weight_html + '</td><td>' + actions_html + '</td></tr>');
                //Editable Fields
                $('.pvarSKU').editable();
                $('.pvarInventory').editable();
                $('.pvarPrice').editable();
                $('.pvarWeight').editable();
                $('.pvaritems').editable();
            },
        });
        event.preventDefault();
    });
    /*End*/
//    $('#op_set').on('save', function (e, params) {
//        var opset_id = params.newValue;
//        var var_html = '';
//        var_html += '<table id="var_tbl" style="clear: both" class="table table-striped table-borderless">';
//
//        $.ajax({
//            type: 'post',
//            url: 'get-opset-variations',
//            data: {
//                opset_id: opset_id,
//            },
//            success: function (value) {
//                console.log(value);
//                var op_arr = JSON.parse(value);
//                var op_html = '';
//                $.each(op_arr, function (index, val) {
//                    op_html += '<a class="pvaritems" href="javascript:" data-title="Please Select" data-type="select" data-value="" data-pk="1" class="editable editable-click" data-source="get-varitems?var_name=' + val + '"></a>';
//                });
//            },
//        });
//        var_html += '</table>';
//        $('#variations .table-responsive').append(var_html);
//
//
//    });

    //Attributes & Attributes Type Section
    //Summernote products-text-decription
    $('#attr-desc').summernote();
    $('#update-attr-desc').summernote();
    $('#attr-type-desc').summernote();
    $('#update-attr-type-desc').summernote();
    //Create Attribute Type
    $(document).on("click", "#attr_type_submit", function (event) {
        var attr_type_name = $('#attr-type-name input').val();
        var attr_type_label = $('#attr-type-label input').val();
        var isValid = true;
        if (attr_type_name == '') {
            $('#attr-type-name').addClass('has-error');
            isValid = false;
        } else {
            if ($('#attr-type-name').hasClass('has-error')) {
                $('#attr-type-name').removeClass('has-error');
            }
        }
        if (attr_type_label == '') {
            $('#attr-type-label').addClass('has-error');
            isValid = false;
        } else {
            if ($('#attr-type-label').hasClass('has-error')) {
                $('#attr-type-label').removeClass('has-error');
            }
        }
        var attr_type_desc = $('#attr-type-desc').summernote('code');
        $('#attr_type_desc').val(attr_type_desc);
        if (!isValid) {
            return false;
        }

    });
    //Update Attribute Type
    $('#update-attr-type-name').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        },
        emptytext: 'Please Enter Value'
    });
    $('#update-attr-type-label').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        },
        emptytext: 'Please Enter Value'
    });

    //Create Attribute
    $(document).on("click", "#attr_submit", function (event) {
        var attr_name = $('#attr-name input').val();
        var attr_label = $('#attr-label input').val();
        var attr_type = $('#attr-type select option:selected').text();
        var isValid = true;
        if (attr_name == '') {
            $('#attr-name').addClass('has-error');
            isValid = false;
        } else {
            if ($('#attr-name').hasClass('has-error')) {
                $('#attr-name').removeClass('has-error');
            }
        }
        if (attr_type == 'Please Select') {
            $('#attr-type .select2-container--default').css('border', '1px solid #ea4335');
            isValid = false;
        } else {
            $('#attr-type .select2-container--default').css('border', '1px solid #dedede');
        }
        if (attr_label == '') {
            $('#attr-label').addClass('has-error');
            isValid = false;
        } else {
            if ($('#attr-label').hasClass('has-error')) {
                $('#attr-label').removeClass('has-error');
            }
        }
        var attr_desc = $('#attr-desc').summernote('code');
        $('#attr_desc').val(attr_desc);
        if (!isValid) {
            return false;
        }
    });
    //Update Attributes
    $('#update-attr-name').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        },
        emptytext: 'Please Enter Value'
    });
    $('#update-attr-label').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        },
        emptytext: 'Please Enter Value'
    });
    $('#update-attr-type').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        },
        emptytext: 'Please Enter Value'
    });

    $('.up_context_data').editable({
        emptytext: 'Please Select'
    });

});



/* JS Functions */
function updateattr_type(id) {
    var attr_type_name = $('#update-attr-type-name').text();
    var attr_type_label = $('#update-attr-type-label').text();
    var attr_type_desc = $('#update-attr-type-desc').summernote('code');
    var id = id;
    jQuery.ajax({
        type: 'post',
        url: '/attribute-type/update-attr_type',
        data: {
            id: id,
            attr_type_name: attr_type_name,
            attr_type_label: attr_type_label,
            attr_type_desc: attr_type_desc,
        },
        success: function (status) {

        },
    });
}

function updateattr(id) {
    var attr_name = $('#update-attr-name').text();
    var attr_label = $('#update-attr-label').text();
    var attr_type = $('#update-attr-type').text();
    var attr_desc = $('#update-attr-desc').summernote('code');
    var id = id;
    jQuery.ajax({
        type: 'post',
        url: '/attributes/update-attr',
        data: {
            id: id,
            attr_name: attr_name,
            attr_label: attr_label,
            attr_type: attr_type,
            attr_desc: attr_desc,
        },
        success: function (status) {

        },
    });
}
/*Print functionality Function*/
function printFunction() {
    window.print();
}
/*End Print functionality Function*/

if ($('#customer_grid_multiselect').length > 0)
{
    // add multiple select / deselect functionality
    $("#customer_grid_multiselect").click(function () {
        $('.multi_delete_user').attr('checked', this.checked);
    });

    // if all checkbox are selected, check the selectall checkbox
    // and viceversa
    $(".multi_delete_user").click(function () {

        if ($(".case").length == $(".multi_delete_user:checked").length) {
            $("#customer_grid_multiselect").attr("checked", "checked");
        } else {
            $("#customer_grid_multiselect").removeAttr("checked");
        }

    });
}

$('#channelsmartlingyes').click(function () {
    if ($('#channel_id_translation_data').css('display') == 'block') {
        $('#channel_id_translation_data').css('display', 'none');
        $(".be-wrapper").removeClass("be-loading-active");
    } else {
        $('#channel_id_translation_data').css('display', 'block');
    }
});


if ($("#channelsmartlingyes").length > 0) {
    var atLeastOneIsChecked = $('#channelsmartlingyes:checkbox:checked').length > 0;
    if (atLeastOneIsChecked == true) {
        $("#channel_id_translation_data").css('display', 'block');
    } else {
        $("#channel_id_translation_data").css('display', 'none');
        $(".be-wrapper").removeClass("be-loading-active");
    }
}