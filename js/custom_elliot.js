//Base Url


if ($('.stripe-button-el').hasClass('stripe-button-el')) {
    $('.stripe-button-el').addClass('btn btn-space btn-primary remove-stripe-button-el');
    $('.remove-stripe-button-el').removeClass('stripe-button-el');
//        $('.remove-stripe-button-el').show();

}


$(document).ready(function () {
    // var current_url =  window.location.hostname;
    //         alert(current_url);

    //Customer index Datatables NEED To customer life value show descending order
    $('#customers_view_table').DataTable({
        "order": [[5, "desc"]],
        pageLength: 25,
        "columnDefs": [
            {"orderable": false, "targets": 0}
        ],
        "columns": [
            null,
            null,
            {className: "Custom_SCroll"},
            null,
            null,
            null,
            null
        ],
        "processing": true,
        "serverSide": true,
        "ajax": "/channels/customerajax",
        oLanguage: {
            sProcessing:function () {
                $(".customerListTAble").addClass('be-loading-active');
            },
        },
        initComplete:function () {
            $(".customerListTAble").removeClass('be-loading-active');
            $(".dataTables_processing").remove();
        },
        
//        "columnDefs": [
//            {"orderable": false, "targets": 0}
//        ],
        buttons: [
            'copy', 'excel', 'pdf', 'print'
        ],
        dom: "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B><'col-sm-12 text-right'f>>" +
                "<'row be-datatable-body'<'col-sm-12'tr>>" +
                "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"
    });

    /*Starts Connected Channel Or Store customer page */
    /*get current action in hidden field*/
    var action = document.getElementById("current_action").value;
    if (action == 'connected-customer') {
        var connect_customers = document.getElementById("connected_customer").value;
        $('#connected_customers_view_table').DataTable({
            "order": [[5, "desc"]],
            pageLength: 25,
            "columnDefs": [
                {"orderable": false, "targets": 0}
            ],
            "processing": true,
            "serverSide": true,
            "oLanguage": {
                sProcessing: function () {
                    $(".connectedcustomerLIST").first().addClass('be-loading-active');
                }
            },
            initComplete: function () {

                $(".connectedcustomerLIST").first().removeClass('be-loading-active');
            },
            "ajax": {'url': "/channels/connectedcustomerajax?customers=" + connect_customers
            },
            buttons: [
                'copy', 'excel', 'pdf', 'print'
            ],
            dom: "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B><'col-sm-12 text-right'f>>" +
                    "<'row be-datatable-body'<'col-sm-12'tr>>" +
                    "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"
        });

    }


    if (action == 'inactive-customers') {
        //code for inactive people listing datatable
        var inactive_customers = document.getElementById("inactive_customer").value;
        $("#inactive_people_view_table").DataTable({
            'order': [[5, "desc"]],
            "columnDefs": [
                {"orderable": false, "targets": 0}
            ],
            pagelength: 25,
            "processing": true,
            "serverSide": true,
            "oLanguage": {
                sProcessing: function () {
                    $(".inactivecustomerLIST").first().addClass('be-loading-active');
                }
            },
            initComplete: function () {

                $(".inactivecustomerLIST").first().removeClass('be-loading-active');
            },
            "ajax": {'url': '/channels/inactivecustomerajax?customer=' + inactive_customers},
            buttons: [
                'copy', 'excel', 'pdf', 'print'
            ],
            dom: "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B><'col-sm-12 text-right'f>>" +
                    "<'row be-datatable-body'<'col-sm-12'tr>>" +
                    "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"

        });

    }

    if (action == 'inactive-orders') {
        var inactive_orders_of = document.getElementById("inactive_orderss").value;
        $("#inactive_order_index_table").DataTable({
            "order": [[4, "desc"]],
            "columnDefs": [
                {"orderable": false, "targets": 0}
            ],
            pageLength: 25,
            "oLanguage": {
                sProcessing: function () {
                    $(".inactiveOrDERS").first().addClass('be-loading-active');
                }
            },
            initComplete: function () {
                $(".inactiveOrDERS").first().removeClass('be-loading-active');
            },
            "processing": true,
            "serverSide": true,
            "ajax": "/orders/inactiveordersajax?orders=" + inactive_orders_of,
            buttons: [
            ],
            dom: "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B><'col-sm-12 text-right'f>>" +
                    "<'row be-datatable-body'<'col-sm-12'tr>>" +
                    "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"

        });



    }



    //NEDD TO CHANGE ACCORDING TO THE DOMAIN Make menus/submenu active based on parent from the URL
    var arr = window.location.href.split('/');
    var arr2 = window.location.href.split('co/');
    var page_val = arr[3];
    var page_val2 = arr2[1];

    if ($('.left-sidebar-content .sidebar-elements li a[href="/' + page_val2 + '"]').length <= 0) {
        if (!$('.left-sidebar-content .sidebar-elements li').hasClass("active")) {
            $('.left-sidebar-content .sidebar-elements li a[href="/' + page_val + '"]').parent("li").addClass("active");
        }
    }



    if ($("body").find('.be-color-header').length == 1) {
        if ($("body").find('.be-color-header.be-color-header-success').length == 1) {
            setTimeout(function () {
                $(".be-wrapper.be-fixed-sidebar").removeClass("be-color-header be-color-header-success");
                $(".flash-message.title").removeClass("hide");
                $(".flash-message.title").addClass("show");
                $(".flash-message.msg").removeClass("show");
                $(".flash-message.msg").addClass("hide");
            }, 5000);
        } else if ($("body").find('.be-color-header.be-color-header-danger').length == 1) {
            setTimeout(function () {
                $(".be-wrapper.be-fixed-sidebar").removeClass("be-color-header be-color-header-danger");
                $(".flash-message.title").removeClass("hide");
                $(".flash-message.title").addClass("show");
                $(".flash-message.msg").removeClass("show");
                $(".flash-message.msg").addClass("hide");
            }, 5000);
        } else if ($("body").find('.be-color-header.be-color-header-warning').length == 1) {
            setTimeout(function () {
                $(".be-wrapper.be-fixed-sidebar").removeClass("be-color-header be-color-header-warning");
                $(".flash-message.title").removeClass("hide");
                $(".flash-message.title").addClass("show");
                $(".flash-message.msg").removeClass("show");
                $(".flash-message.msg").addClass("hide");
            }, 5000);
        } else if ($("body").find('.be-color-header.be-color-header-info').length == 1) {
            setTimeout(function () {
                $(".be-wrapper.be-fixed-sidebar").removeClass("be-color-header be-color-header-info");
                $(".flash-message.title").removeClass("hide");
                $(".flash-message.title").addClass("show");
                $(".flash-message.msg").removeClass("show");
                $(".flash-message.msg").addClass("hide");
            }, 5000);
        }
        //Right header Toggle for notifications  
//        $('.notif').click(function () {
//            $('.notif').toggleClass('open');
//        });
//
//        //Right header Toggle for connect Stores 
//        $('.connect').click(function () {
//            $('.connect').toggleClass('open');
//        });
//
//        //Right header Toggle for Account
//        $('.acount-dropdown').click(function () {
//            $('.acount-dropdown').toggleClass('open');
//        });

    }

    //For orders is open connected stores
    $('#see').click(function (e) {

        $('.connect').toggleClass('open');
        e.stopPropagation();

    });
    $('#see-overview').click(function (e) {

        $('.connect').toggleClass('open');
        e.stopPropagation();

    });

    $('.proceedToDocuments').on('click', function () {
        console.log('here');
        $this = $(this);
        window.location.href = '/documents';
    });
    $('.documents_fields').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });
    $('.documents_fields_account_type').editable({
        value: '2',
        source: [
            {value: 'checking', text: 'Checking'},
            {value: 'savings', text: 'Savings'}
        ],
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        },
        success: function (k, val) {
            $('#account_type').attr('data-select-val', val);
        }
    });
    $('.documents_fields_dob').editable({
        format: 'dd-mm-yyyy',
        viewformat: 'dd/mm/yyyy',
        combodate: {
            minYear: 1950,
            maxYear: 2017,
            minuteStep: 1
        }
    });
    $('.payment_account_country').editable({
        typeahead: {
            name: 'country',
            remote: 'get-countries?query=%QUERY',
        },
    });

    //make general setting general_corporate_state editable
    $('.payment_account_state').editable({
        typeahead: {
            name: 'state',
            remote: 'get-states?query=%QUERY',
        }
    });

    //make general setting general_corporate_city editable
    $('.payment_account_city').editable({
        typeahead: {
            name: 'city',
            remote: 'get-cities?query=%QUERY',
        }
    });
    $i = 0;
    $('.addMoreDirectors').on('click', function () {

        $i++;
        $this = $(this);
        director_count = parseInt($i) + parseInt(1);
        $('#directors').children(':first').append('<div class="panel panel-default"><div class="panel-heading"><h4 class="panel-title"><a data-toggle="collapse" class="collapsed" data-parent="#accordionChannelS" href="#director_' + $i + '"><i class="icon mdi mdi-chevron-down"></i>Director ' + director_count + '</a></h4></div><div id="director_' + $i + '" class="panel-collapse collapse"><div class="panel-body"><label> Director </label> <input type="hidden" class="hidden_directors_file" id="hidden_directors_file_id_' + $i + '" data-index="' + $i + '"> <table id="cats_tbl" style="clear: both" class="table table-striped table-borderless"> <tbody> <tr> <td width="35%"> First name</td> <td width="65%"><a id="" class="documents_fields first_name" href="#" data-type="text" data-title="Please Enter value"></a></td> </tr> <tr> <td width="35%"> Last name</td> <td width="65%"><a id="last_name"class="documents_fields last_name" href="#" data-type="text" data-title="Please Enter value"></a></td> </tr> <tr> <td width="35%"> Date of Birth</td> <td width="65%"><a class="documents_fields_dob dob" href="#" data-pk="1" data-template="D / MMM / YYYY" data-viewformat="DD/MM/YYYY" data-format="YYYY-MM-DD"data-type="combodate" data-title="Please Enter value"></a></td> </tr> <tr> <td width="35%"> Address</td> <td width="65%"><a id="" class="documents_fields address" href="#" data-type="text" data-title="Please Enter value"></a></td> </tr> <tr> <td width="35%"> Last 4 of Social</td> <td width="65%"> <input type="text" data-mask="last_4_social" id="last_4_social" placeholder="9999" class="form-control last_4_social"></td> </tr> </tbody> </table> <div class="main-content container-fluid"> <form action="" class="directorsform' + $i + ' dropzone"> <div class="dz-message"> <div class="icon"><span class="mdi mdi-cloud-upload"></span></div> <h2>Drag and Drop files here</h2><span class="note">(This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)</span> </div> </form> </div></div></div></div>');
        $('.documents_fields').editable({
            validate: function (value) {
                if ($.trim(value) == '') {
                    return 'This field is required';
                }
            }
        });
        $('.documents_fields_dob').editable({
            format: 'dd-mm-yyyy',
            viewformat: 'dd/mm/yyyy',
            combodate: {
                minYear: 1950,
                maxYear: 2017,
                minuteStep: 1
            }
        });
        Dropzone.autoDiscover = false;
        var myDropzone3 = new Dropzone(".directorsform" + $i,
                {
                    url: "/documents/upload-documents?type=directors",
                    init: function () {
                        this.on("success", function (file, responseText) {
                            console.log(responseText);
                            data = $.parseJSON(responseText);
                            console.log(data);
                            if (data.id) {

                                console.log($('#hidden_directors_file_id_' + $i));
                                $('#hidden_directors_file_id_' + $i).val(data.id);
                            }
                        });
                    }
                });
    });
    $bankingform = $('#bankingform');
    $businessform = $('#businessform');
    $directorsform = $('.directorsform');
    if ($.contains(document, $bankingform[0])) {

        var myDropzone = new Dropzone("#bankingform", {url: "/documents/upload-documents?type=banking"});
        myDropzone.on("complete", function (file) {
            console.log('done');
        });
    }
    if ($.contains(document, $businessform[0])) {

        var myDropzone1 = new Dropzone("#businessform", {url: "/documents/upload-documents?type=business"});
        myDropzone1.on("complete", function (file) {
            console.log('done');
        });
    }
    if ($.contains(document, $directorsform[0])) {

        var myDropzone2 = new Dropzone(".directorsform",
                {url: "/documents/upload-documents?type=directors",
                    init: function () {
                        this.on("success", function (file, responseText) {
                            data = $.parseJSON(responseText);
                            if (data.id) {
                                $('#hidden_directors_file_id_0').val(data.id);
                            }
                        });
                    }});

    }
	$('#channelfulfillment').click(function(){
		$('#fulfillmentlist').val("");
		$('#select2-fulfillmentlist-container').attr('title','Select Fulfillment');
		$('#select2-fulfillmentlist-container').html('Select Fulfillment');
	});
	if($('#channelfulfillment').prop("checked") == true){
		$('#newfullfill').show();
	}
	
});

$(function () {
    // for the content add new page
    //$("#page_title").editable();
    $("#page_description").summernote();
    $("#page_description_update").summernote();
    
    
//Left Sidebar Menus sublinks 
    $('.be-left-sidebar a').each(function (index) {
        if (this.href.trim() == window.location)
            $(this).parent().addClass("active"); //this will add active class to li containing the anchor tag
    });
    /*Single Product Layout Page Editable Fields*/
    //General Tab
    $('#product_name').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });
    // $('#SKU').editable({
    //     validate: function (value) {
    //         if ($.trim(value) == '') {
    //             return 'This field is required';
    //         }
    //     }
    // });
    $('#UPC').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });
    $('#EAN').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });
    $('#JAN').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });
    $('#ISBN').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });
    $('#MPN').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });
    $('#adult').editable({
        source: [
            {value: 'no', text: 'No'},
            {value: 'yes', text: 'Yes'}
        ],
        display: function (value, sourceData) {
            var colors = {"": "gray", no: "green", yes: "red"},
                    elem = $.grep(sourceData, function (o) {
                        return o.value == value;
                    });
            if (elem.length) {
                $(this).text(elem[0].text).css("color", colors[value]);
            } else {
                $(this).empty();
            }
        }
    });

    $('#age_group').editable({
        source: [
            {value: 'Newborn', text: 'Newborn'},
            {value: 'Infant', text: 'Infant'},
            {value: 'Toddler', text: 'Toddler'},
            {value: 'Kids', text: 'Kids'},
            {value: 'Adult', text: 'Adult'}
        ],
    });

    $('#availability').editable({
        source: [
            {value: 'In Stock', text: 'In Stock'},
            {value: 'Out of Stock', text: 'Out of Stock'}
        ],
        display: function (value, sourceData) {
            var colors = {"": "gray", "In Stock": "green", "Out of Stock": "red"},
                    elem = $.grep(sourceData, function (o) {
                        return o.value == value;
                    });
            if (elem.length) {
                $(this).text(elem[0].text).css("color", colors[value]);
            } else {
                $(this).empty();
            }
        }
    });

    $('#condition').editable({
        source: [
            {value: 'New', text: 'New'},
            {value: 'Used', text: 'Used'},
            {value: 'Refurbished', text: 'Refurbished'},
        ],
    });

    $('#gender').editable({
        source: [
            {value: 'Female', text: 'Female'},
            {value: 'Male', text: 'Male'},
            {value: 'Unisex', text: 'Unisex'}
        ],
        display: function (value, sourceData) {
            var colors = {"": "gray", "Female": "pink", "Male": "blue", "Unisex": "gray"},
                    elem = $.grep(sourceData, function (o) {
                        return o.value == value;
                    });
            if (elem.length) {
                $(this).text(elem[0].text).css("color", colors[value]);
            } else {
                $(this).empty();
            }
        }
    });

    $('#weight').editable();
    /*For Inventory IQ*/
    $('.allocate_inventory').editable({
        type: 'text',
        validate: function (value) {
            if ($.isNumeric(value) == '') {
                return 'Only numbers are allowed';
            }
        }

    });

    /*Starts For inventory iq dynamic*/
    var divison_amount, percent_divison, sum_product_abb, count_product_abb, check_divison = '';
    /*For Enable or disble inventory iq*/
    $(".chnl_dsbl_rv").click(function () {
//      alert('testin git');
        $(".ModalCustomClass").addClass('be-loading-active');
        var sum_product_abb = 0;
        var percent_divison, final_val = '';
        var bb = $(".allocate_inventory").css('display');
        var bb = $(".allocate_priority").css('display');
        if (bb == 'none') {
            setTimeout(function () {
                $(".ModalCustomClass").removeClass('be-loading-active');
            }, 1000);
            $('.allocate_inventory').editable('option', 'disabled', true);
            $(".allocate_inventory").css('display', 'table-row');
            $(".allocate_priority").css('display', 'block');
            $(".allocate_percent").css({'display': 'block', 'color': '#585858'});
            /*Starts For Allocate Inventory*/
            $(".stock_qty_connect").each(function () {
                /*Convert text in integer*/
                var stock_qty_connect_1 = parseInt(($(this).text()));
                if (sum_product_abb == 0) {
                    sum_product_abb = parseInt(stock_qty_connect_1);
                } else {

                    sum_product_abb = parseInt(sum_product_abb) + parseInt(stock_qty_connect_1);
                }
            });
            /*sum_product_abb is total of Stock Quantity dynamicaly */
            /*count_product_abb is total of connected channels */
            count_product_abb = $('#count_product_abb').val();

            percent_divison = sum_product_abb / count_product_abb;
            check_divison = Number.isInteger(percent_divison);
            var inc = 1;
            var count = 1;
            /*For stock Quantity*/
            divison_amount = parseInt(percent_divison);
            var amount = sum_product_abb - (count_product_abb * divison_amount);
            var count_amount = amount;
            $(".stock_qty_connect").each(function () {
                var inv_iq_pecent, final_val = '';
                /*Get attribute Of Stock Quantity for inventory Iq*/
                var data_attr = $(this).attr("data-classname");
                if (count_amount > 0) {
                    final_val = divison_amount + 1;
                    $(this).text(final_val);
                    $(this).attr('data-value', final_val);
                    $(this).editable('setValue', final_val);
                } else {
                    final_val = divison_amount;
                    $(this).text(final_val);
                    $(this).attr('data-value', final_val);
                    $(this).editable('setValue', final_val);
                }
                count_amount--;

                if (count_product_abb == 1) {
                    if (divison_amount == 0) {
                        inv_iq_pecent = 0;
                    } else {
                        inv_iq_pecent = 100;
                    }
                } else {
                    var TwoPlacedfolat = (final_val * 100 / sum_product_abb);

                    if (Number.isInteger(TwoPlacedfolat) == true) {
                        inv_iq_pecent = TwoPlacedfolat;
                    } else {
                        inv_iq_pecent = parseFloat(TwoPlacedfolat).toFixed(2);
                    }
                }
                /*inv_iq_pecent is priprity Percentage*/
                if(isNaN(inv_iq_pecent)) {
                    $('.' + data_attr).text(0);
                } else {
                    $('.' + data_attr).text(inv_iq_pecent);
                }
                
                $('#stk_qty').text(sum_product_abb);
                $('.stock_qty_connect').editable('option', 'disabled', true);
                count++;
            });
        } else {
            setTimeout(function () {
                $(".ModalCustomClass").removeClass('be-loading-active');
            }, 1000);
            $('.stock_qty_connect').editable('option', 'disabled', false);

            $('.allocate_inventory').css('display', 'none');
            $(".allocate_priority").css('display', 'none');
            $(".allocate_percent").css('display', 'none');
        }
    });
    /*End For inventory iq dynamic*/


    //Attribution Tab   
    $('#cat1').editable({
        typeahead: {
            name: 'category',
            remote: '/google-shopping/google-product-category?query=%QUERY'
        },
        success: function (response, newValue) {
            // alert(newValue);
            var request = $.ajax({
                url: "/google-shopping/google-product-category",
                type: "GET",
                data: {querymain: newValue},
                dataType: "html"
            });

            request.done(function (msg) {
                // alert(msg);
                var sources = {
                    1: msg
                };
                $('#cat2').editable('option', 'source', sources[1]);
                $('#cat2').editable('setValue', null);
            });

            //remote: '/google-shopping/google-product-category?querymain='+newValue;
            //$('#cat2').editable('option', 'source', sources[newValue]);  
            //$('#cat2').editable('setValue', null);
        }
    });

    $('#cat2').editable({
        typeahead: {
            name: 'category',
            remote: '/google-shopping/google-product-category?query=%QUERY'
        },
        success: function (response, newValue) {
            // alert(newValue);
            $('#gcatname').remove();
            $('#attr_tbl').append('<input type="hidden" value="' + newValue + '" id="gcatname">');
        }

    });

    $('#cat3').editable({
        typeahead: {
            name: 'category',
            remote: '/google-product-category?query=%QUERY'
        }
    });

    $('#occasion').editable({
        source: [
            {value: 'Athletic', text: 'Athletic'},
            {value: 'Baby Shower', text: 'Baby Shower'},
            {value: 'BBQ', text: 'BBQ'},
            {value: 'Beach', text: 'Beach'},
            {value: 'Date Night', text: 'Date Night'},
            {value: 'Interview', text: 'Interview'},
            {value: 'Outdoors', text: 'Outdoors'},
            {value: 'Wedding', text: 'Wedding'},
            {value: 'Work', text: 'Work'}
        ]
    });

    $('#weather').editable({
        source: [
            {value: 'Breezy', text: 'Breezy'},
            {value: 'Cold', text: 'Cold'},
            {value: 'Dry', text: 'Dry'},
            {value: 'Hot', text: 'Hot'},
            {value: 'Humid', text: 'Humid'},
            {value: 'Rain', text: 'Rain'},
            {value: 'Snow', text: 'Snow'}
        ]
    });

    //Inventory Management Tab
    //$('#stk_qty').editable();
    $('#stk_qty').editable({
        type: 'text',
        validate: function (value) {
            if ($.isNumeric(value) == '') {
                return 'Only numbers are allowed';
            }
        }
    });
    /*For Conected Stock Quantity*/
    //$('.stock_qty_connect').editable();
    $('.stock_qty_connect').editable({
        type: 'text',
        validate: function (value) {
            if ($.isNumeric(value) == '') {
                return 'Only numbers are allowed';
            }
        }
    });

    $('#stk_lvl').editable({
        source: [
            {value: 'In Stock', text: 'In Stock'},
            {value: 'Out of Stock', text: 'Out of Stock'}
        ],
        display: function (value, sourceData) {
            var colors = {"": "gray", "In Stock": "green", "Out of Stock": "red"},
                    elem = $.grep(sourceData, function (o) {
                        return o.value == value;
                    });
            if (elem.length) {
                $(this).text(elem[0].text).css("color", colors[value]);
            } else {
                $(this).empty();
            }
        }
    });

    /*For Conected Stock Level*/
    $('.stock_level_connect').editable({
        source: [
            {value: 'In Stock', text: 'In Stock'},
            {value: 'Out of Stock', text: 'Out of Stock'}
        ],
        display: function (value, sourceData) {
            var colors = {"": "gray", "In Stock": "green", "Out of Stock": "red"},
                    elem = $.grep(sourceData, function (o) {
                        return o.value == value;
                    });
            if (elem.length) {
                $(this).text(elem[0].text).css("color", colors[value]);
            } else {
                $(this).empty();
            }
        }
    });
    /*For End Connected Stock Level*/

    $('#stk_status').editable({
        source: [
            {value: 'Visible', text: 'Visible'},
            {value: 'Hidden', text: 'Hidden'}
        ],
        display: function (value, sourceData) {
            var colors = {"": "gray", "Visible": "green", "Hidden": "red"},
                    elem = $.grep(sourceData, function (o) {
                        return o.value == value;
                    });
            if (elem.length) {
                $(this).text(elem[0].text).css("color", colors[value]);
            } else {
                $(this).empty();
            }
        }
    });
    /*For Start Connected Stock status*/
    $('.stock_status_connect').editable({
        source: [
            {value: 'Visible', text: 'Visible'},
            {value: 'Hidden', text: 'Hidden'}
        ],
        display: function (value, sourceData) {
            var colors = {"": "gray", "Visible": "green", "Hidden": "red"},
                    elem = $.grep(sourceData, function (o) {
                        return o.value == value;
                    });
            if (elem.length) {
                $(this).text(elem[0].text).css("color", colors[value]);
            } else {
                $(this).empty();
            }
        }
    });
    /*For eND Connected Stock status*/

    //$('#low_stk_ntf').editable();
    $('#low_stk_ntf').editable({
        type: 'text',
        validate: function (value) {
            if ($.isNumeric(value) == '') {
                return 'Only numbers are allowed';
            }
        }
    });
    $('.connect_low_stk_ntf').editable({
        type: 'text',
        validate: function (value) {
            if ($.isNumeric(value) == '') {
                return 'Only numbers are allowed';
            }
        }
    });
    //$('.connect_low_stk_ntf').editable();

    //Pricing Tab
    // $('#price_currency').editable();

//    $('#price').editable({success: function (response, newValue) {
////        updateAccount(this.id, newValue);
//            $("#price").text(123);
//        }});

    $('#price').editable();
    $('#sale_price').editable();

    /*Starts For connected pricing*/
    $('.price_connect').editable();
    $('.sale_price_connect').editable();

    $('#schedule_date1').editable({
        format: 'dd-mm-yyyy',
        viewformat: 'dd/mm/yyyy',
        combodate: {
            minYear: 2000,
            maxYear: 2020,
            minuteStep: 1
        }
    });


    $('#schedule_date2').editable({
        format: 'dd-mm-yyyy',
        viewformat: 'dd/mm/yyyy',
        combodate: {
            minYear: 2000,
            maxYear: 2020,
            minuteStep: 1
        }
    });

    $('.schedule_date_connect').editable({
        format: 'dd-mm-yyyy',
        viewformat: 'dd/mm/yyyy',
        combodate: {
            minYear: 2000,
            maxYear: 2020,
            minuteStep: 1
        }
    });

    $('#marketplaces').editable({
        source: [
            {value: 'Breezy', text: 'Breezy'},
            {value: 'Cold', text: 'Cold'},
            {value: 'Dry', text: 'Dry'},
            {value: 'Hot', text: 'Hot'},
            {value: 'Humid', text: 'Humid'},
            {value: 'Rain', text: 'Rain'},
            {value: 'Snow', text: 'Snow'},
        ]
    });

    /*Starts Add categoreis */

    //make Category Name editable
    $('#add_cat_name').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        },
        emptytext: 'Please Enter'
    });
    //make Parent Category Name 
    $('#add_parent_cat_name').editable({
        emptytext: 'Please Select'
    });

    //make Category Name editable
    $('#update_cat_name').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        },
        emptytext: 'Please Enter'
    });
    //make Parent Category Name 
    $('#update_parent_cat_name').editable({
        emptytext: 'Please Select'
    });



    /*Starts Account Profile About Me */

    //make Account firstname editable
    $('#profile_first_name').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });

    //make Account lastname editable
    $('#profile_last_name').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }

        },
    });

    //make Account email editable
    $('#profile_email_add').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
            var profile_email_add = value;
            var atpos = profile_email_add.indexOf("@");
            var dotpos = profile_email_add.lastIndexOf(".");
            if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= profile_email_add.length)
            {
                return 'Email is Not valid';
            }


        }
    });
    //make Gender editable
    $('#profile_gender').editable({
        //prepend:value,
        source: [
            {value: 'Unisex', text: 'Unisex'},
            {value: 'Male', text: 'Male'},
            {value: 'Female', text: 'Female'}
        ],
        display: function (value, sourceData) {
            var colors = {"Female": "pink", "Male": "blue", "Unisex": "gray"},
                    elem = $.grep(sourceData, function (o) {
                        return o.value == value;
                    });
            if (elem.length) {
                $(this).text(elem[0].text).css("color", colors[value]);
            } else {
                $(this).empty();
            }
        },
    });
    
    $("#profile_timezone").editable({
        source: '/integrations/timezones',
        });


    //make Account PhoneNo editable
    $('#profile_Phone_no').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
            if (/^\d{10}$/.test(value)) {
                // value is ok, use it
            } else {
                return 'Invalid number; must be ten digits';

            }

        }
    });

    //make Account corporate_street1 editable
    $('#profile_corporate_street1').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });

    //make Account corporate_street2 editable
    $('#profile_corporate_street2').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });

    //make Account dob editable
    $('#profile_dob').editable({
        format: 'dd-mm-yyyy',
        viewformat: 'dd/mm/yyyy',
        datepicker: {
            weekStart: 1
        }
    });

    //make city editable
    $('#profile_corporate_city').editable({
        typeahead: {
            name: 'city',
            remote: 'get-cities?query=%QUERY',
            // local: ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Dakota", "North Carolina", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"]
        }
    });

    //make state editable
    $('#profile_corporate_state').editable({
        typeahead: {
            name: 'state',
            remote: 'get-states?query=%QUERY',
        },
    });

    //make ZipCode editable
    $('#profile_corporate_zip').editable({
        value: '143001',
        typeahead: {
            name: 'zip code',
            local: ["143001", "0001", "14250", "123", "5725", "65565", "000", "5454", "787887", "253354", "788787", "545454"]
        }
    });

    //make Country editable
    $('#profile_corporate_country').editable({
        typeahead: {
            name: 'country',
            remote: 'get-countries?query=%QUERY',
        },
//            local: ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Dakota", "North Carolina", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"]

    });

    //make Ship_street1 editable
    $('#profile_ship_street1').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });

    //make Ship_street2 editable
    $('#profile_ship_street2').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });

    //make Ship City editable
    $('#profile_ship_city').editable({
        typeahead: {
            name: 'city',
            remote: 'get-cities?query=%QUERY',
            // local: ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Dakota", "North Carolina", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"]
        }
    });

    //make Ship State editable
    $('#profile_ship_state').editable({
        typeahead: {
            name: 'state',
            remote: 'get-states?query=%QUERY',
            // local: ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Dakota", "North Carolina", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"]
        }
    });

    //make Ship Zipcode editable
    $('#profile_ship_zip').editable({
        value: '143001',
        typeahead: {
            name: 'zip code',
            local: ["143001", "0001", "14250", "123", "5725", "65565", "000", "5454", "787887", "253354", "788787", "545454"]
        }
    });

    //make Ship Country editable
    $('#profile_ship_country').editable({
        typeahead: {
            name: 'country',
            remote: 'get-countries?query=%QUERY',
        },
    });

    /*End User Account Profile About Me */


    /*Starts Customer Account Profile About Me */

    //make Customer Account firstname editable
    $('#customer_first_name').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });

    //make Account lastname editable
    $('#customer_last_name').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });

    //make Account email editable
    $('#customer_email_add').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
            var profile_email_add = value;
            var atpos = profile_email_add.indexOf("@");
            var dotpos = profile_email_add.lastIndexOf(".");
            if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= profile_email_add.length)
            {
                return 'Email is Not valid';
            }

        }
    });
    //make Gender editable
    $('#customer_gender').editable({
        //prepend:value,
        source: [
            {value: 'Unisex', text: 'Unisex'},
            {value: 'Male', text: 'Male'},
            {value: 'Female', text: 'Female'}
        ],
        display: function (value, sourceData) {
            var colors = {"Female": "pink", "Male": "blue", "Unisex": "gray"},
                    elem = $.grep(sourceData, function (o) {
                        return o.value == value;
                    });
            if (elem.length) {
                $(this).text(elem[0].text).css("color", colors[value]);
            } else {
                $(this).empty();
            }
        },
    });

    //make Account PhoneNo editable
    $('#customer_Phone_no').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
//            if (/^\d{10}$/.test(value)) {
//                // value is ok, use it
//            } 
//            else {
//                return 'Invalid number; must be ten digits';
//
//            }

        }
    });

    //make Account corporate_street1 editable

    $('.cus_bill_street1').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });

    //make Account corporate_street2 editable
    $('.cus_bill_street2').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });

    //make Account dob editable
    $('#customer_dob').editable({
        format: 'dd-mm-yyyy',
        viewformat: 'dd/mm/yyyy',
        datepicker: {
            weekStart: 1
        }
    });

    //make city editable
    $('.cus_bill_city').editable({
        typeahead: {
            name: 'city',
            remote: '/get-cities?query=%QUERY',
            // local: ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Dakota", "North Carolina", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"]
        }
    });

    //make state editable
    $('.cus_bill_state').editable({
        typeahead: {
            name: 'state',
            remote: '/get-states?query=%QUERY',
        },
    });

    //make ZipCode editable
    $('.cus_bill_zip').editable({
        typeahead: {
            name: 'zip code',
            local: ["143001", "0001", "14250", "123", "5725", "65565", "000", "5454", "787887", "253354", "788787", "545454"]
        }
    });

    //make Country editable
    $('.cus_bill_country').editable({
        typeahead: {
            name: 'country',
            remote: '/get-countries?query=%QUERY',
        },
//            local: ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Dakota", "North Carolina", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"]

    });

    //make Ship_street1 editable
    $('.cus_ship_street1').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });

    //make Ship_street2 editable
    $('.cus_ship_street2').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });

    //make Ship City editable
    $('.cus_ship_city').editable({
        typeahead: {
            name: 'city',
            remote: '/get-cities?query=%QUERY',
            // local: ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Dakota", "North Carolina", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"]
        }
    });

    //make Ship State editable
    $('.cus_ship_state').editable({
        typeahead: {
            name: 'state',
            remote: '/get-states?query=%QUERY',
            // local: ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Dakota", "North Carolina", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"]
        }
    });

    //make Ship Zipcode editable
    $('.cus_ship_zip').editable({
        typeahead: {
            name: 'zip code',
            local: ["143001", "0001", "14250", "123", "5725", "65565", "000", "5454", "787887", "253354", "788787", "545454"]
        }
    });

    //make Ship Country editable
    $('.cus_ship_country').editable({
        typeahead: {
            name: 'country',
            remote: '/get-countries?query=%QUERY',
        },
    });

    // Starts Sub user page

    $("#first_name_sub").editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });

    $("#last_name_sub").editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });

    $("#email_add_sub").editable({
        // alert(window.location.pathname);
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
            var sub_profile_email_add = value;
            var atpos = sub_profile_email_add.indexOf("@");
            var dotpos = sub_profile_email_add.lastIndexOf(".");
            var current_url = window.location.hostname;
            var parts = current_url.split("." + DOMAIN_NAME.slice(0, -1));
            var current_admin = parts[0];
            var entered_domain = sub_profile_email_add.substr(sub_profile_email_add.indexOf("@") + 1);
            if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= sub_profile_email_add.length)
            {
                return 'Email is Not valid';
            }
            if (entered_domain != current_admin + '.com') {

                return 'Email should be of (' + current_admin + '.com) domain only';
            }




        }
    });

    $("#role_sub").editable({
        source: [
            {value: 'merchant', text: 'Store Owner'},
            {value: 'merchant_user', text: 'Store Administrator'}
        ],
        display: function (value, sourceData) {
            var colors = {"merchant": "pink", "merchant_user": "blue"},
                    elem = $.grep(sourceData, function (o) {
                        return o.value == value;
                    });
            if (elem.length) {
                $(this).text(elem[0].text).css("color", colors[value]);
            } else {
                $(this).empty();
            }
        },
    });



    /*Starts General settings page */

    //make AccountOwner editable
    $('#AccountOwner').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
            var AccountOwner = value;
            var atpos = AccountOwner.indexOf("@");
            var dotpos = AccountOwner.lastIndexOf(".");
            if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= AccountOwner.length)
            {
                return 'Email is Not valid';
            }
        }
    });

    //make general_corporate_street1 editable
    $('#general_corporate_street1').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });

    //make general_corporate_street2 editable
    $('#general_corporate_street2').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });
	$('#general_phone_number').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });
	$('#general_company').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });

    //make general setting general_corporate_country editable
    $('#general_corporate_country').editable({
        typeahead: {
            name: 'country',
            remote: 'get-countries?query=%QUERY',
        },
    });

    //make general setting general_corporate_state editable
    $('#general_corporate_state').editable({
        typeahead: {
            name: 'state',
            remote: 'get-states?query=%QUERY',
        }
    });

    //make general setting general_corporate_city editable
    $('#general_corporate_city').editable({
        typeahead: {
            name: 'city',
            remote: 'get-cities?query=%QUERY',
        }
    });

    //make eneral setting general_corporate_Zipcode editable
    $('#general_corporate_zip').editable({
        value: '143001',
        typeahead: {
            name: 'zip code',
            local: ["143001", "0001", "14250", "123", "5725", "65565", "000", "5454", "787887", "253354", "788787", "545454"]
        }
    });

    //make subscription_billing_street1 editable
    $('#subscription_billing_street1').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });

    //make subscription_billing_street2 editable
    $('#subscription_billing_street2').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });

    //make general setting subscription_billing_country editable
    $('#subscription_billing_country').editable({
        typeahead: {
            name: 'country',
            remote: 'get-countries?query=%QUERY',
        },
    });

    //make general setting subscription_billing_state editable
    $('#subscription_billing_state').editable({
        typeahead: {
            name: 'state',
            remote: 'get-states?query=%QUERY',
        }
    });

    //make general setting subscription_billing_city editable
    $('#subscription_billing_city').editable({
        typeahead: {
            name: 'city',
            remote: 'get-cities?query=%QUERY',
        }
    });

    //make eneral setting subscription_billing_zipcode editable
    $('#subscription_billing_zip').editable({
        value: '143001',
        typeahead: {
            name: 'zip code',
            local: ["143001", "0001", "14250", "123", "5725", "65565", "000", "5454", "787887", "253354", "788787", "545454"]
        }
    });

    //make general setting tax rate editable
    $('#general_TaxRate').editable({
        validate: function (value) {
            if ($.trim(value) == '') {
                return 'This field is required';
            }
        }
    });

    //make eneral setting general_Language editable
    $('#general_Language').editable({
        value: 'English',
        typeahead: {
            remote: 'get-languages?query=%QUERY',
        }
    });

    //make general setting WeightPreference editable
    $('#general_WeightPreference').editable({
        source: [
            {value: 'LBs', text: 'LBs'},
            {value: 'OZs', text: 'OZs'},
            {value: 'KGs', text: 'KGs'}
        ],
    });

    //make eneral setting general_Timezone editable
    $('#general_Timezone').editable();
//    $('#general_Timezone').editable({
//            prepend: "not selected",
//            source: function() {
//                $.ajax({
//                    url: 'get-timezone',
//                    dataType: 'json',
//                    success: function(data) {
//                    [{text: "group1", children: [{value: 1, text: "text1"}, {value: 2, text: "text2"}]}];
//
//                    }
//                });
//            },
////            source: [
////                
////            {value: 1, text: 'Male'}
////            ],
//            
//    });



    /* Users Listing DataTable */
    $("#user_table").dataTable({
        pageLength: 50,
        buttons: [
            'copy', 'excel', 'pdf', 'print'
        ],
        dom: "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B>>" +
                "<'row be-datatable-body'<'col-sm-12'tr>>" +
                "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"
    });
    /* END */

    /*Starts Cover image Dropzone*/
    $("#cover_image_dropzone").dropzone({
        url: "save-cover-image",
        maxFilesize: 2, // MB
        maxFiles: 1,
        acceptedFiles: 'image/*',
        dictDefaultMessage: "+",
        dictInvalidFileType: "Please upload Image files.",
        dictMaxFilesExceeded: "You can not upload any more files.",
        init: function () {
            this.on("success", function (file, responseText) {
                var responsetext = JSON.parse(file.xhr.responseText);
                if (responsetext.status == 'success') {
                    window.location = "profile";
                }

            });
        }
    });

    /*Starts Cover image Dropzone 1*/
    $("#cover_image_dropzone1").dropzone({
        url: "save-cover-image",
        maxFilesize: 2, // MB
        maxFiles: 1,
        acceptedFiles: 'image/*',
        dictDefaultMessage: "+",
        dictInvalidFileType: "Please upload Image files.",
        dictMaxFilesExceeded: "You can not upload any more files.",
        init: function () {
            this.on("success", function (file, responseText) {
                var responsetext = JSON.parse(file.xhr.responseText);
                if (responsetext.status == 'success') {
                    window.location = "profile";
                }

            });
        }
    });
    /*End Cover image Dropzone*/

    /*Starts Cover image Default*/
    $("#cover_image_dropzone_default").dropzone({
        url: "save-cover-image",
        maxFilesize: 2, // MB
        maxFiles: 1,
        acceptedFiles: 'image/*',
        dictDefaultMessage: "+",
        dictInvalidFileType: "Please upload Image files.",
        dictMaxFilesExceeded: "You can not upload any more files.",
        init: function () {
            this.on("success", function (file, responseText) {
                var responsetext = JSON.parse(file.xhr.responseText);
                if (responsetext.status == 'success') {
                    window.location = "profile";
                }

            });
        }
    });
    /*End Cover image Default Dropzone*/

    /*Starts Profile image Dropzone*/
    $("#profile_image_dropzone").dropzone({
        url: "save-profile-image",
        maxFilesize: 2, // MB
        maxFiles: 1,
        acceptedFiles: 'image/*',
        dictDefaultMessage: "+",
        dictInvalidFileType: "Please upload Image files.",
        dictMaxFilesExceeded: "You can not upload any more files.",
        init: function () {
            this.on("success", function (file, responseText) {
                var responsetext = JSON.parse(file.xhr.responseText);
                if (responsetext.status == 'success') {
                    window.location = "profile";
                }
            });
        }
    });
    /*End Cover image Dropzone*/

    /*Starts Profile image Dropzone1*/
    $("#profile_image_dropzone1").dropzone({
        url: "save-profile-image",
        maxFilesize: 2, // MB
        maxFiles: 1,
        acceptedFiles: 'image/*',
        dictDefaultMessage: "+",
        dictInvalidFileType: "Please upload Image files.",
        dictMaxFilesExceeded: "You can not upload any more files.",
        init: function () {
            this.on("success", function (file, responseText) {
                var responsetext = JSON.parse(file.xhr.responseText);
                if (responsetext.status == 'success') {
                    window.location = "profile";
                }
            });
        }
    });
    /*End Profile image Dropzone1 */

    /*Starts product_image_dropzone*/
    $("#product_image_dropzone").dropzone({
        url: "save-product-image",
        maxFilesize: 2, // MB
        maxFiles: 1,
        acceptedFiles: 'image/*',
        dictDefaultMessage: "+",
        dictInvalidFileType: "Please upload Image files.",
        dictMaxFilesExceeded: "You can not upload any more files.",
        init: function () {
            this.on("success", function (file, responseText) {
                var responsetext = JSON.parse(file.xhr.responseText);
                if (responsetext.status == 'success') {
                    window.location = "profile";
                }

            });
        }
    });
    /*End product_image_dropzone */

    /*Starts product_image_dropzone 1*/
    $("#product_image_dropzone1").dropzone({
        url: "save-product-image",
        maxFilesize: 2, // MB
        maxFiles: 1,
        acceptedFiles: 'image/*',
        dictDefaultMessage: "+",
        dictInvalidFileType: "Please upload Image files.",
        dictMaxFilesExceeded: "You can not upload any more files.",
        init: function () {
            this.on("success", function (file, responseText) {
                var responsetext = JSON.parse(file.xhr.responseText);
                if (responsetext.status == 'success') {
                    window.location = "profile";
                }

            });
        }
    });
    /*End product_image_dropzone1 */

    /*Starts product_image_dropzone-default*/
    $("#product_image_dropzone-default").dropzone({
        url: "save-product-image",
        maxFilesize: 2, // MB
        maxFiles: 1,
        acceptedFiles: 'image/*',
        dictDefaultMessage: "+",
        dictInvalidFileType: "Please upload Image files.",
        dictMaxFilesExceeded: "You can not upload any more files.",
        init: function () {
            this.on("success", function (file, responseText) {
                var responsetext = JSON.parse(file.xhr.responseText);
                if (responsetext.status == 'success') {
                    window.location = "profile";
                }

            });
        }
    });
    /*Endproduct_image_dropzone-default*/

    /*Starts product_image_dropzone-default1*/
    $("#product_image_dropzone-default1").dropzone({
        url: "save-product-image",
        maxFilesize: 2, // MB
        maxFiles: 1,
        acceptedFiles: 'image/*',
        dictDefaultMessage: "",
        dictInvalidFileType: "Please upload Image files.",
        dictMaxFilesExceeded: "You can not upload any more files.",
        init: function () {
            this.on("success", function (file, responseText) {
                var responsetext = JSON.parse(file.xhr.responseText);
                if (responsetext.status == 'success') {
                    window.location = "profile";
                }

            });
        }
    });
    /*End product_image_dropzone-default1*/



    $(".google_error_modal_close").click(function () {
        $('.googlemod-success').hide();
    });
    $(".google_modal_close").click(function () {
        $('.googlemod-success').hide();
    });
	/* $('#channelfulfillment').click(function(){
		//console.log($(this).prop("checked"));
		if ($(this).prop("checked") == 'true') {
			//alert($('#fulfillmentlist option[value=""]').html());
			//$('#fulfillmentlist option[value=""]').attr('selected','selected');
			$("#fulfillmentlist").val("1");
		}
	}); */
	

    //Product Section//

    /*Starts Customer Cover image Dropzone1*/
    $("#cover_image_dropzone1").dropzone({
        url: "save-cover-image",
        maxFilesize: 2, // MB
        maxFiles: 1,
        acceptedFiles: 'image/*',
        dictDefaultMessage: "+",
        dictInvalidFileType: "Please upload Image files.",
        dictMaxFilesExceeded: "You can not upload any more files.",
        init: function () {
            this.on("success", function (file, responseText) {
                var responsetext = JSON.parse(file.xhr.responseText);
                if (responsetext.status == 'success') {
                    window.location = "profile";
                }

            });
        }
    });
    /*End Customer Cover image Dropzone1*/

    /*Starts customer-cover image Dropzone*/
    $("#customer-cover_image_dropzone").dropzone({
        url: "save-cover-image",
        maxFilesize: 2, // MB
        maxFiles: 1,
        acceptedFiles: 'image/*',
        dictDefaultMessage: "+",
        dictInvalidFileType: "Please upload Image files.",
        dictMaxFilesExceeded: "You can not upload any more files.",
        init: function () {
            this.on("success", function (file, responseText) {
                var responsetext = JSON.parse(file.xhr.responseText);
                if (responsetext.status == 'success') {
                    window.location = "profile";
                }

            });
        }
    });
    /*End customer-cover image Dropzone*/

    /*Starts customer-cover_image_dropzone_default*/
    $("#customer-cover_image_dropzone_default").dropzone({
        url: "save-cover-image",
        maxFilesize: 2, // MB
        maxFiles: 1,
        acceptedFiles: 'image/*',
        dictDefaultMessage: "+",
        dictInvalidFileType: "Please upload Image files.",
        dictMaxFilesExceeded: "You can not upload any more files.",
        init: function () {
            this.on("success", function (file, responseText) {
                var responsetext = JSON.parse(file.xhr.responseText);
                if (responsetext.status == 'success') {
                    window.location = "profile";
                }

            });
        }
    });
    /*End customer-cover_image_dropzone_default*/

    /*Starts Profile image Dropzone*/
    $("#customer-profile_image_dropzone").dropzone({
        url: "save-profile-image",
        maxFilesize: 2, // MB
        maxFiles: 1,
        acceptedFiles: 'image/*',
        dictDefaultMessage: "+",
        dictInvalidFileType: "Please upload Image files.",
        dictMaxFilesExceeded: "You can not upload any more files.",
        init: function () {
            this.on("success", function (file, responseText) {
                var responsetext = JSON.parse(file.xhr.responseText);
                if (responsetext.status == 'success') {
                    window.location = "profile";
                }
            });
        }
    });
    /*End Cover image Dropzone*/

    /*Starts Profile image Dropzone1*/
    $("#customer-profile_image_dropzone1").dropzone({
        url: "save-profile-image",
        maxFilesize: 2, // MB
        maxFiles: 1,
        acceptedFiles: 'image/*',
        dictDefaultMessage: "+",
        dictInvalidFileType: "Please upload Image files.",
        dictMaxFilesExceeded: "You can not upload any more files.",
        init: function () {
            this.on("success", function (file, responseText) {
                var responsetext = JSON.parse(file.xhr.responseText);
                if (responsetext.status == 'success') {
                    window.location = "profile";
                }
            });
        }
    });
    /*End Profile image Dropzone1 */

    //Summernote products-text-decription
    $('#product-update-description').summernote();
//    $('#smartling_editor').summernote();

    //Summernote products-text-decription
    $('#product-create-description').summernote();

    $(".chnl_dsbl").click(function () {
        dt = $(this).attr("data-connid");
        c_name = $(this).attr("data-cname");
        c_type = $(this).attr("data-type");
        cls = $(this).attr("data-cls");

        $('#disable-warning').attr("data-cls", cls);
        $(".modalmsg").text(c_name);
        $(".proceed_todlt").attr("data-type", c_type);
        $(".proceed_todlt").attr("data-connid", dt);
        if (!$(this).hasClass("test")) {
            $('#disable-warning').modal('show');
        }
        $(this).addClass('test');

    })
    $('#disable-warning').on('hidden.bs.modal', function () {
        cls = $(this).attr("data-cls");
        $("." + cls + " .chnl_dsbl").click();
        $("." + cls + " .chnl_dsbl").removeClass('test');
    })

    $(".proceed_todlt").click(function () {
        connection_id = $(this).attr("data-connid");
        type = $(this).attr("data-type");
        $("#channel_disable .be-spinner span").html('Your data is deleting...');
        $("#disable-warning").modal('hide');
        $("#channel_disable").addClass("be-loading-active");
        $.ajax({
            type: 'post',
            url: '/channelsetting/channeldisable',
            data: {
                ch_id: connection_id,
                ch_type: type,
            },
            success: function (value) {
                //window.location.reload('/channels');
                $("#channel_disable").removeClass("be-loading-active");
                window.location = '/';
            },
        });
    })

//console.log($(".runwechatimport"))
//function runwechatimport(user_id){
    if ($('.runwechatimport').length > 0) {
        user_id = $(".runwechatimport").attr("data-id");
//alert("its working fine"+user_id); return false;
        $.ajax({
            type: 'post',
            url: '/channels/wechatimportonlogin',
            data: {
                user_id: user_id,
                task: 'import',
                task_source: 'WeChat'
            },
            beforeSend: function (xhr) {
//                $(".be-wrapper").addClass("be-loading-active");
            },
            success: function (value) {
                if (value != "error") {
                    $.ajax({
                        type: 'get',
                        url: BASE_URL + 'channels/wechatimport/' + user_id,
                        data: {
                            type: value,
                            process: 'backend',
                        },
                        beforeSend: function (xhr) {
                        },
                        success: function (value) {
                        },
                    });
                }

            },
        });
    }

	
	  $(".wechat_sbmt1_already").click(function () {

        storename = $.trim($("#wechat_storename").val());
        password = $.trim($("#wechat_password").val());
        type = $.trim($("#wechat_type").val());
        if (!storename || !password) {
            $("#wechat_storename").addClass("inpt_required");
            $("#wechat_password").addClass("inpt_required");
            return false;
        } else {
            $("#wechat_storename").removeClass("inpt_required");
            $("#wechat_password").removeClass("inpt_required");
        }

        $.ajax({
            type: 'post',
            url: '/channels/wechatconnect',
            data: {
                user: storename,
                password: password,
                type: type,
                already: 'yes',
                email: '',
            },
            beforeSend: function (xhr) {
                $(".be-wrapper").addClass("be-loading-active");
            },
            success: function (value) {
                $(".be-wrapper").removeClass("be-loading-active");
                if (value == 'error') {
                    $('#walkthechat_request_error').modal('show');
                }
                //else if (value) {
                else {
                    $('#walkthechat_request').modal('show');
                    $(".afterwechatrequestmsg_already").show();
                    $(".wechatrequestform").hide();
                    console.log("test");
                    // $('#walkthechat_request').modal('show');
                    // $(".afterwechatrequestmsg").show();
                    // $(".wechatrequestform").hide();
                }

//           location.reload();
            },
        });
    })
	

    $(".sfexpress_sbmt1_already").click(function () {
        username = $.trim($('#SfExpress_channel_email').val());
        password = $.trim($('#SfExpress_channel_password').val());
        if (!username || !password) {
            $("#SfExpress_channel_email").addClass("inpt_required");
            $("#SfExpress_channel_password").addClass("inpt_required");
            return false;
        } else {
            $("#SfExpress_channel_email").removeClass("inpt_required");
            $("#SfExpress_channel_password").removeClass("inpt_required");
        }
        $.ajax({
            type: 'post',
            url: '/sfexpress/save/',
            data: {
                username: username,
                password: password,
            },
            beforeSend: function (xhr) {
                $(".be-wrapper").addClass("be-loading-active");
            },
            success: function (value) {
                $(".be-wrapper").removeClass("be-loading-active");
                if (value == 'error') {
                    $('#SfExpress_ajax_request_error').modal('show');
                } else if (value) {

                    $("#SfExpress_ajax_request").modal('show');

                }

//           location.reload();
            },
        });
    });

    $(".sfexpress_disconnect_already").click(function () {

        $.ajax({
            type: 'post',
            url: '/sfexpress/deletesf/',
            data: {
                keyid: 'SF Express'
            },
            beforeSend: function (xhr) {
                $(".be-wrapper").addClass("be-loading-active");
            },
            success: function (value) {
                $(".be-wrapper").removeClass("be-loading-active");
                location.reload();
                if (value == 'error') {
                    $('#SfExpress_ajax_request_error').modal('show');
                } else if (value) {
                    location.reload();
                    $("#SfExpress_ajax_request").modal('show');
                    window.location.href = '/sfexpress'

                }

//           location.reload();
            },
        });
    });

    $(".Google_sbmt1").click(function () {
        clientID = $.trim($("#Google_clientid").val());
        secretKey = $.trim($("#Google_secretkey").val());
        merchantID = $.trim($("#Google_merchantid").val());
        if (!clientID || !secretKey || !merchantID) {
            $("#Google_clientid").addClass("inpt_required");
            $("#Google_secretkey").addClass("inpt_required");
            $("#Google_merchantid").addClass("inpt_required");
            return false;
        } else {
            $("#Google_clientid").removeClass("inpt_required");
            $("#Google_secretkey").removeClass("inpt_required");
            $("#Google_merchantid").removeClass("inpt_required");
        }

        $.ajax({
            type: 'post',
            url: '/google-shopping/save/',
            data: {
                clientID: clientID,
                secretKey: secretKey,
                merchantID: merchantID,
                // already: 'yes',
                // email: '',
            },
            beforeSend: function (xhr) {
                $(".be-wrapper").addClass("be-loading-active");
            },
            success: function (value) {

                $(".be-wrapper").removeClass("be-loading-active");
                if (value == 'error') {
                    $('#walkthechat_request_error').modal('show');
                } else if (value) {
                    // window.location.href = value;
                    // $('#walkthechat_request').modal('show');
                    //$(".afterwechatrequestmsg_already").show();
                    $(".div23").addClass('active');
                    $(".data2").addClass('active');
                    $(".data1").addClass('complete');
                    $(".div21").removeClass('active');
                    $(".Googlerequestform").hide();
                }

//           location.reload();
            },
        });
    })

});

function addUpdateDocuments() {
    var account_no = $('#account_no').text();
    var routing_no = $('#routing_no').text();
    var bank_code = $('#bank_code').text();
    var bank_name = $('#bank_name').text();
    var bank_address = $('#bank_address').text();
    var swift = $('#swift').text();
//    var account_type = $('#account_type').text();
    var account_type = $('#account_type').attr('data-select-val');
    var tax_id = $('#tax_id').text();
    var alipay_payment_account_no = $('#alipay_payment_account_no').text();
    var alipay_payment_account_id = $('#alipay_payment_account_id').text();
    var alipay_payment_account_email = $('#alipay_payment_account_email').text();
    var alipay_payment_address_1 = $('#alipay_payment_address_1').text();
    var alipay_payment_address_2 = $('#alipay_payment_address_2').text();
    var alipay_payment_account_city = $('#alipay_payment_account_city').text();
    var alipay_payment_account_state = $('#alipay_payment_account_state').text();
    var alipay_payment_account_country = $('#alipay_payment_account_country').text();
    var alipay_payment_account_zip_code = $('#alipay_payment_account_zip_code').text();

    var dinpay_payment_account_no = $('#dinpay_payment_account_no').text();
    var dinpay_payment_account_id = $('#dinpay_payment_account_id').text();
    var dinpay_payment_account_email = $('#dinpay_payment_account_email').text();
    var dinpay_payment_address_1 = $('#dinpay_payment_address_1').text();
    var dinpay_payment_address_2 = $('#dinpay_payment_address_2').text();
    var dinpay_payment_account_city = $('#dinpay_payment_account_city').text();
    var dinpay_payment_account_state = $('#dinpay_payment_account_state').text();
    var dinpay_payment_account_country = $('#dinpay_payment_account_country').text();
    var dinpay_payment_account_zip_code = $('#dinpay_payment_account_zip_code').text();

    var payoneer_payment_account_no = $('#payoneer_payment_account_no').text();
    var payoneer_payment_account_id = $('#payoneer_payment_account_id').text();
    var payoneer_payment_account_email = $('#payoneer_payment_account_email').text();
    var payoneer_payment_address_1 = $('#payoneer_payment_address_1').text();
    var payoneer_payment_address_2 = $('#payoneer_payment_address_2').text();
    var payoneer_payment_account_city = $('#payoneer_payment_account_city').text();
    var payoneer_payment_account_state = $('#payoneer_payment_account_state').text();
    var payoneer_payment_account_country = $('#payoneer_payment_account_country').text();
    var payoneer_payment_account_zip_code = $('#payoneer_payment_account_zip_code').text();

    var worldfirst_payment_account_no = $('#worldfirst_payment_account_no').text();
    var worldfirst_payment_account_id = $('#worldfirst_payment_account_id').text();
    var worldfirst_payment_account_email = $('#worldfirst_payment_account_email').text();
    var worldfirst_payment_address_1 = $('#worldfirst_payment_address_1').text();
    var worldfirst_payment_address_2 = $('#worldfirst_payment_address_2').text();
    var worldfirst_payment_account_city = $('#worldfirst_payment_account_city').text();
    var worldfirst_payment_account_state = $('#worldfirst_payment_account_state').text();
    var worldfirst_payment_account_country = $('#worldfirst_payment_account_country').text();
    var worldfirst_payment_account_zip_code = $('#worldfirst_payment_account_zip_code').text();



    /*Starts For connected pricing*/
    var first_names = [];
    var last_names = [];
    var dobs = [];
    var addresses = [];
    var last_4_socials = [];

    $(".first_name").each(function () {

        var first_name = $(this).text();

        first_names.push(first_name);
    });
    $(".last_name").each(function () {

        var last_name = $(this).text();

        last_names.push(last_name);
    });
    $(".dob").each(function () {

        var dob = $(this).text();

        dobs.push(dob);
    });
    $(".address").each(function () {

        var address = $(this).text();

        addresses.push(address);
    });
    $(".last_4_social").each(function () {

        var last_4_social = $(this).val();

        last_4_socials.push(last_4_social);
    });
    $j = 0;
    var hidden_directors_file_values = {};
    $(".hidden_directors_file").each(function () {

        var val = $(this).val();
        hidden_directors_file_values[$(this).attr('data-index')] = val;
    });

    $.ajax({
        type: 'post',
        url: '/documents/add-update-document',
        data: {
            account_no: account_no,
            routing_no: routing_no,
            bank_code: bank_code,
            bank_name: bank_name,
            bank_address: bank_address,
            swift: swift,
            account_type: account_type,
            tax_id: tax_id,
            alipay_payment_account_no: alipay_payment_account_no,
            alipay_payment_account_id: alipay_payment_account_id,
            alipay_payment_account_email: alipay_payment_account_email,
            alipay_payment_address_1: alipay_payment_address_1,
            alipay_payment_address_2: alipay_payment_address_2,
            alipay_payment_account_city: alipay_payment_account_city,
            alipay_payment_account_state: alipay_payment_account_state,
            alipay_payment_account_country: alipay_payment_account_country,
            alipay_payment_account_zip_code: alipay_payment_account_zip_code,

            dinpay_payment_account_no: dinpay_payment_account_no,
            dinpay_payment_account_id: dinpay_payment_account_id,
            dinpay_payment_account_email: dinpay_payment_account_email,
            dinpay_payment_address_1: dinpay_payment_address_1,
            dinpay_payment_address_2: dinpay_payment_address_2,
            dinpay_payment_account_city: dinpay_payment_account_city,
            dinpay_payment_account_state: dinpay_payment_account_state,
            dinpay_payment_account_country: dinpay_payment_account_country,
            dinpay_payment_account_zip_code: dinpay_payment_account_zip_code,

            payoneer_payment_account_no: payoneer_payment_account_no,
            payoneer_payment_account_id: payoneer_payment_account_id,
            payoneer_payment_account_email: payoneer_payment_account_email,
            payoneer_payment_address_1: payoneer_payment_address_1,
            payoneer_payment_address_2: payoneer_payment_address_2,
            payoneer_payment_account_city: payoneer_payment_account_city,
            payoneer_payment_account_state: payoneer_payment_account_state,
            payoneer_payment_account_country: payoneer_payment_account_country,
            payoneer_payment_account_zip_code: payoneer_payment_account_zip_code,

            worldfirst_payment_account_no: worldfirst_payment_account_no,
            worldfirst_payment_account_id: worldfirst_payment_account_id,
            worldfirst_payment_account_email: worldfirst_payment_account_email,
            worldfirst_payment_address_1: worldfirst_payment_address_1,
            worldfirst_payment_address_2: worldfirst_payment_address_2,
            worldfirst_payment_account_city: worldfirst_payment_account_city,
            worldfirst_payment_account_state: worldfirst_payment_account_state,
            worldfirst_payment_account_country: worldfirst_payment_account_country,
            worldfirst_payment_account_zip_code: worldfirst_payment_account_zip_code,
            directors: {first_names: first_names, last_names: last_names, dobs: dobs, addresses: addresses,
                last_4_socials: last_4_socials},
            hidden_directors_file_values: hidden_directors_file_values
        },
//        dataType: "json",

        success: function (value) {
			$('#documentssaved').modal('show');
            $('.be-wrapper').removeClass('be-loading-active');

        },
    });
}

function updateProduct() {
    var pId = $('#product_id').val();
    //General Tab Fields
    var pName = $('#product_name').text();
    var pSKU = $('#SKU').text();
    var pUPC = $('#UPC').text();
    var pEAN = $('#EAN').text();
    var pJAN = $('#JAN').text();
    var pISBN = $('#ISBN').text();
    var pMPN = $('#MPN').text();
    var pDescription = $('#product-update-description').summernote('code');
    var pAdult = $('#adult').text();
    var pAgeGroup = $('#age_group').text();
    var pAvail = $('#availability').text();
    var pBrand = $('#brand').text();
    var Google_Cat1 = $('#cat1').text();
    var Google_Cat2 = $('#cat2').text();
    var Google_Cat_id = $('#gcatname').val();
    var pCond = $('#condition').text();
    var pGender = $('#gender').text();
    var pWeight = $('#weight').text();
    var elli_allocate_str = $('#elli_allocate_inv').text();
    var elli_allocate_inv = elli_allocate_str.replace(/\,/g, "");
    //Attribution Tab Fields
//    var pAttr_cat1;
//    var pAttr_cat2;
//    var pAttr_cat3;
//    var pOccasion = $('#occasion').text();
//    pOccas = pOccasion;
//    if (pOccasion != 'Empty') {
//        var pOccasion_html = $('#occasion').html();
//        var pOccasion_val = pOccasion_html.split('<br>');
//        var pOccas = '';
//        $.each(pOccasion_val, function () {
//            pOccas = pOccas + this + ',';
//        });
//        pOccas = pOccas.replace(/,$/, '');
//    }

    var pcategories = $('#cats_list').text();


    if (pcategories != 'Empty') {
        var pcategories_html = $('#cats_list').html();
        var pcat = pcategories_html.split('<br>');

    }
    var channel_sale = '';
    var channel_manager_list = $('#channelsonproductview').text();
    if (channel_manager_list != 'Empty') {
        var channel_manager_list = $('#channelsonproductview').html();
        var n = channel_manager_list.indexOf(",");
        if (n != -1) {
            channel_sale = channel_manager_list.split(',');
        } else {
            channel_sale = channel_manager_list.split('<br>');
        }

        // var channel_manager_list_replace = channel_manager_list.replace(",","<br>");



        /*Case of Bigcoomerce if category is empty or null show error product modal*/
        if (channel_sale.indexOf("BigCommerce") != -1) {

            if (pcategories == '' || pcategories == 'Empty')
            {
                $("#product_update_ajax_msg_eror").html('Please select any one of the category');
                $("#product_update_header_error_msg").html('Error!');
                $('.product_update_ajax_request_error').css({
                    'display': 'block',
                    'background': 'rgba(0,0,0,0.6)',
                });
                return false;
            } else
            {
                $('.product_update_ajax_request_error').css({
                    'display': 'none',
                    'background': 'rgba(0,0,0,0.6)',
                });
            }

        }
    }

    /*Starts For connected pricing*/
    var Connected_price = {};
    var Connected_sale_price = {};
    var Connected_schedule_date = {};
    var Connected_stock_qty = {};
    var Connected_stock_level = {};
    var Connected_stock_status = {};
    var Connected_stock_notif = {};
    var Connected_allocate_inv = {};

    $(".price_connect").each(function () {

        var connect_price_str = $(this).text();
        var connect_price = connect_price_str.replace(/\,/g, "");
        var connect_price_channel = $(this).attr('data-connectname');

        Connected_price[connect_price_channel] = connect_price;
    });

    $(".sale_price_connect").each(function () {

        var connect_sale_price_str = $(this).text();
        var connect_sale_price = connect_sale_price_str.replace(/\,/g, "");
        var connect_salePrice_channel = $(this).attr('data-connectname');

        Connected_sale_price[connect_salePrice_channel] = connect_sale_price;
    });

    $(".schedule_date_connect").each(function () {

        var schedule_date_connect = $(this).text();
        var schedule_date_connect_channel = $(this).attr('data-scheduledate');

        Connected_schedule_date[schedule_date_connect_channel] = schedule_date_connect;
    });

    $(".stock_qty_connect").each(function () {

        var stock_qty_connect = $(this).text();
        var stock_qty_connect_channel = $(this).attr('data-stock-qty-connect');

        Connected_stock_qty[stock_qty_connect_channel] = stock_qty_connect;
    });

    $(".stock_level_connect").each(function () {

        var stock_level_connect = $(this).text();
        var stock_level_connect_channel = $(this).attr('data-stock-level-connect');

        Connected_stock_level[stock_level_connect_channel] = stock_level_connect;
    });

    $(".stock_status_connect").each(function () {

        var stock_status_connect = $(this).text();
        var stock_status_connect_channel = $(this).attr('data-stock-status-connect');

        Connected_stock_status[stock_status_connect_channel] = stock_status_connect;
    });

    $(".allocate_inv_connect").each(function () {

        var allocate_inv_connect = $(this).text();
        var allocate_inv_connect_channel = $(this).attr('data-allocate-inv-connect');

        Connected_allocate_inv[allocate_inv_connect_channel] = allocate_inv_connect;
    });





    /*End For connected pricing*/

//    var pWeather = $('#weather').text();
//    pWthr = pWeather;
//    if (pWeather != 'Empty') {
//        var pWeather_html = $('#weather').html();
//        var pWeather_val = pWeather_html.split('<br>');
//        var pWthr = '';
//        $.each(pWeather_val, function () {
//            pWthr = pWthr + this + ',';
//        });
//        pWthr = pWthr.replace(/,$/, '');
//    }

    //Inventory Management Tab Fields
    var pStk_qty = $('#stk_qty').text();
    var pStk_lvl = $('#stk_lvl').text();
    var pStk_status = $('#stk_status').text();
    var plow_stk_ntf = $('#low_stk_ntf').text();

    //Pricing Tab Fields
    var pPrice_str = $('#price').text();
    var pPrice = pPrice_str.replace(/\,/g, "");
    var pSale_price_str = $('#sale_price').text();
    var pSale_price = pSale_price_str.replace(/\,/g, "");
    var pSchedule_date = $('#schedule_date2').html();
    $('.be-wrapper').addClass('be-loading-active');
//    console.log(pPrice+"uu");
//    return false; 
    $.ajax({
        type: 'post',
        url: 'update-product?pId=' + pId,
        data: {
            pId: pId,
            pName: pName,
            pSKU: pSKU,
            pUPC: pUPC,
            pEAN: pEAN,
            pJAN: pJAN,
            Google_Cat1: Google_Cat1,
            Google_Cat2: Google_Cat2,
            Google_Cat_id: Google_Cat_id,
            pISBN: pISBN,
            pMPN: pMPN,
            pDescription: pDescription,
            pAdult: pAdult,
            pAgeGroup: pAgeGroup,
            pAvail: pAvail,
            pBrand: pBrand,
            pCond: pCond,
            pGender: pGender,
            pWeight: pWeight,
//            pOccasion: pOccas,
//            pWeather: pWthr,
            pcat: pcat,
            channel_sale: channel_sale,
            pStk_qty: pStk_qty,
            pStk_lvl: pStk_lvl,
            pStk_status: pStk_status,
            plow_stk_ntf: plow_stk_ntf,
            pPrice: pPrice,
            pSale_price: pSale_price,
            Connected_price: Connected_price,
            Connected_sale_price: Connected_sale_price,
            pSchedule_date: pSchedule_date,
            Connected_schedule_date: Connected_schedule_date,
            Connected_stock_qty: Connected_stock_qty,
            Connected_stock_level: Connected_stock_level,
            Connected_stock_status: Connected_stock_status,
            Connected_stock_notif: Connected_stock_notif,
            elli_allocate_inv: elli_allocate_inv,
            Connected_allocate_inv: Connected_allocate_inv,
        },
//        dataType: "json",

        success: function (value) {
            $('.be-wrapper').removeClass('be-loading-active');

        },
    });
}
/*For product error modal close*/
$('.product_update_error_modal_close').click(function () {
    $('.product_update_ajax_request_error').css({
        'display': 'none',
        'background': 'rgba(0,0,0,0.6)',
    });
});

//save sub user profile

function savesubprofile() {

    var sub_firstname = $("#first_name_sub").text();
    var sub_lastname = $("#last_name_sub").text();
    var sub_email = $("#email_add_sub").text();
    var sub_role = $("#role_sub").attr('data-value');
    var sub_user_id = $("#sub_profile_id").val();

    jQuery.ajax({
        type: 'post',
        url: '/user/savesubprofile',
        data: {
            sub_firstname: sub_firstname,
            sub_lastname: sub_lastname,
            sub_email: sub_email,
            sub_role: sub_role,
            sub_user_id: sub_user_id
        },
        dataType: "json",
        success: function (value) {
            if (value.status == 'success') {
                console.log(value);
            } else {

            }
        },
    })
}


//Save Account Profile Information
function saveprofile() {

    var profile_first_name = $('#profile_first_name').text();
    var profile_last_name = $('#profile_last_name').text();
    var profile_email_add = $('#profile_email_add').text();
    var profile_dob = $('#profile_dob').text();
    var gender = $('#profile_gender').text();
    var profile_Phone_no = $('#profile_Phone_no').text();
    var profile_timezone = $('#profile_timezone').text();
    var corporate_street1 = $('#profile_corporate_street1').text();
    var corporate_street2 = $('#profile_corporate_street2').text();
    var corporate_city = $('#profile_corporate_city').text();
    var corporate_state = $('#profile_corporate_state').text();
    var corporate_zip = $('#profile_corporate_zip').text();
    var corporate_country = $('#profile_corporate_country').text();
    var ship_street1 = $('#profile_ship_street1').text();
    var ship_street2 = $('#profile_ship_street2').text();
    var ship_city = $('#profile_ship_city').text();
    var ship_state = $('#profile_ship_state').text();
    var ship_zip = $('#profile_ship_zip').text();
    var ship_country = $('#profile_ship_country').text();





    jQuery.ajax({
        type: 'post',
        url: 'saveprofile',
        data: {
            profile_first_name: profile_first_name,
            profile_last_name: profile_last_name,
            profile_email_add: profile_email_add,
            profile_dob: profile_dob,
            gender: gender,
            profile_Phone_no: profile_Phone_no,
            profile_timezone: profile_timezone,
            corporate_street1: corporate_street1,
            corporate_street2: corporate_street2,
            corporate_city: corporate_city,
            corporate_state: corporate_state,
            corporate_zip: corporate_zip,
            corporate_country: corporate_country,
            ship_street1: ship_street1,
            ship_street2: ship_street2,
            ship_city: ship_city,
            ship_state: ship_state,
            ship_zip: ship_zip,
            ship_country: ship_country,

        },
        dataType: "json",
        success: function (value) {
            if (value.status == 'success') {
                window.location = "profile";
            } else {

            }
        },
    });
}



//Save Customer Profile Information
function savecustomer() {

    var customer_first_name = $('#customer_first_name').text();
    var customer_last_name = $('#customer_last_name').text();
    var customer_email_add = $('#customer_email_add').text();
    var customer_dob = $('#customer_dob').text();
    var gender = $('#customer_gender').text();
    var customer_Phone_no = $('#customer_Phone_no').text();
    var corporate_street1 = $('#customer_corporate_street1').text();
    var corporate_street2 = $('#customer_corporate_street2').text();
    var corporate_city = $('#customer_corporate_city').text();
    var corporate_state = $('#customer_corporate_state').text();
    var corporate_zip = $('#customer_corporate_zip').text();
    var corporate_country = $('#customer_corporate_country').text();
    var ship_street1 = $('#customer_ship_street1').text();
    var ship_street2 = $('#customer_ship_street2').text();
    var ship_city = $('#customer_ship_city').text();
    var ship_state = $('#customer_ship_state').text();
    var ship_zip = $('#customer_ship_zip').text();
    var ship_country = $('#customer_ship_country').text();
    var customer_id = document.getElementById("customer_id").value;

    var bill_street1 = {};
    var bill_street2 = {};
    var bill_city = {};
    var bill_state = {};
    var bill_zip = {};
    var bill_country = {};

    var ship_street1 = {};
    var ship_street2 = {};
    var ship_city = {};
    var ship_state = {};
    var ship_zip = {};
    var ship_country = {};
    /*For Billing Address*/
    $(".cus_bill_street1").each(function () {

        var cus_bill_street1 = $(this).text();
        var connect_cus_bill_street1 = $(this).attr('data-connectname');

        bill_street1[connect_cus_bill_street1] = cus_bill_street1;
    });

    $(".cus_bill_street2").each(function () {

        var cus_bill_street2 = $(this).text();
        var connect_cus_bill_street2 = $(this).attr('data-connectname');

        bill_street2[connect_cus_bill_street2] = cus_bill_street2;
    });

    $(".cus_bill_city").each(function () {

        var cus_bill_city = $(this).text();
        var connect_cus_bill_city = $(this).attr('data-connectname');

        bill_city[connect_cus_bill_city] = cus_bill_city;
    });

    $(".cus_bill_state").each(function () {

        var cus_bill_state = $(this).text();
        var connect_cus_bill_state = $(this).attr('data-connectname');

        bill_state[connect_cus_bill_state] = cus_bill_state;
    });

    $(".cus_bill_zip").each(function () {

        var cus_bill_zip = $(this).text();
        var connect_cus_bill_zip = $(this).attr('data-connectname');

        bill_zip[connect_cus_bill_zip] = cus_bill_zip;
    });

    $(".cus_bill_country").each(function () {

        var cus_bill_country = $(this).text();
        var connect_cus_bill_country = $(this).attr('data-connectname');

        bill_country[connect_cus_bill_country] = cus_bill_country;
    });

    /*For Shipping Address*/

    $(".cus_ship_street1").each(function () {

        var cus_ship_street1 = $(this).text();
        var connect_cus_ship_street1 = $(this).attr('data-connectname');

        ship_street1[connect_cus_ship_street1] = cus_ship_street1;
    });

    $(".cus_ship_street2").each(function () {

        var cus_ship_street2 = $(this).text();
        var connect_cus_ship_street2 = $(this).attr('data-connectname');

        ship_street2[connect_cus_ship_street2] = cus_ship_street2;
    });

    $(".cus_ship_city").each(function () {

        var cus_ship_city = $(this).text();
        var connect_cus_ship_city = $(this).attr('data-connectname');

        ship_city[connect_cus_ship_city] = cus_ship_city;
    });

    $(".cus_ship_state").each(function () {

        var cus_ship_state = $(this).text();
        var connect_cus_ship_state = $(this).attr('data-connectname');

        ship_state[connect_cus_ship_state] = cus_ship_state;
    });

    $(".cus_ship_zip").each(function () {

        var cus_ship_zip = $(this).text();
        var connect_cus_ship_zip = $(this).attr('data-connectname');

        ship_zip[connect_cus_ship_zip] = cus_ship_zip;
    });

    $(".cus_ship_country").each(function () {

        var cus_ship_country = $(this).text();
        var connect_cus_ship_country = $(this).attr('data-connectname');

        ship_country[connect_cus_ship_country] = cus_ship_country;
    });




    $('.be-wrapper').addClass('be-loading-active');

    jQuery.ajax({
        type: 'post',
        url: 'savecustomer',
        data: {
            customer_first_name: customer_first_name,
            customer_last_name: customer_last_name,
            customer_email_add: customer_email_add,
            customer_dob: customer_dob,
            gender: gender,
            customer_Phone_no: customer_Phone_no,
//            corporate_street1: corporate_street1,
//            corporate_street2: corporate_street2,
//            corporate_city: corporate_city,
//            corporate_state: corporate_state,
//            corporate_zip: corporate_zip,
//            corporate_country: corporate_country,
//            ship_street1: ship_street1,
//            ship_street2: ship_street2,
//            ship_city: ship_city,
//            ship_state: ship_state,
//            ship_zip: ship_zip,
//            ship_country: ship_country,
            customer_id: customer_id,

            bill_street1: bill_street1,
            bill_street2: bill_street2,
            bill_city: bill_city,
            bill_state: bill_state,
            bill_zip: bill_zip,
            bill_country: bill_country,

            ship_street1: ship_street1,
            ship_street2: ship_street2,
            ship_city: ship_city,
            ship_state: ship_state,
            ship_zip: ship_zip,
            ship_country: ship_country,
        },
        dataType: "json",
        success: function (value) {
            if (value.status == 'success') {
                window.location = "view?id=" + customer_id;
            } else {

            }
        },
    });
}



//Add Categories
function addcategories() {

    var add_cat_name = $('#add_cat_name').text();
    var add_parent_cat_name = $('#add_parent_cat_name').text();
    if (add_cat_name == 'Empty') {
        document.getElementById("span_err_add_cat_name").innerHTML = "* Please Enter Category Name";
        return false;
    } else {
        document.getElementById("span_err_add_cat_name").innerHTML = "";
    }

    jQuery.ajax({
        type: 'post',
        url: 'create',
        data: {
            add_cat_name: add_cat_name,
            add_parent_cat_name: add_parent_cat_name,
        },
        dataType: "json",
        success: function (value) {
            if (value.status == 'success') {
                window.location = "profile";
            } else {

            }
        },
    });
}

//Add Categories
function updatecategories(id) {

    var update_cat_name = $('#update_cat_name').text();

    var update_parent_cat_name = $('#update_parent_cat_name').text();
    var id = id;


    if (update_cat_name == 'Empty') {
        document.getElementById("span_err_update_cat_name").innerHTML = "* Please Enter Category Name";
        return false;
    } else {
        document.getElementById("span_err_update_cat_name").innerHTML = "";
    }

    jQuery.ajax({
        type: 'post',
        url: '/categories/updatecat',
        data: {
            id: id,
            update_cat_name: update_cat_name,
            update_parent_cat_name: update_parent_cat_name,
        },
        dataType: "json",
        success: function (value) {
            if (value.status == 'success') {
                window.location = "updatecat";
            } else {

            }
        },
    });
}

//Save General Information
function savegeneralinfo() {

    var AccountOwner = $('#AccountOwner').text();
    var general_corporate_street1 = $('#general_corporate_street1').text();
    var general_corporate_street2 = $('#general_corporate_street2').text();
    var general_phone_number = $('#general_phone_number').text();
    var general_company = $('#general_company').text();
    var general_corporate_country = $('#general_corporate_country').text();
    var general_corporate_state = $('#general_corporate_state').text();
    var general_corporate_city = $('#general_corporate_city').text();
    var general_corporate_zip = $('#general_corporate_zip').text();
    var subscription_billing_street1 = $('#subscription_billing_street1').text();
    var subscription_billing_street2 = $('#subscription_billing_street2').text();
    var subscription_billing_country = $('#subscription_billing_country').text();
    var subscription_billing_state = $('#subscription_billing_state').text();
    var subscription_billing_city = $('#subscription_billing_city').text();
    var subscription_billing_zip = $('#subscription_billing_zip').text();
    var general_TaxRate = $('#general_TaxRate').text();
    var general_Language = $('#general_Language').text();
    var general_WeightPreference = $('#general_WeightPreference').text();
    var general_Timezone = $('#general_Timezone').text();

    jQuery.ajax({
        type: 'post',
        url: 'save-general-info',
        data: {
            AccountOwner: AccountOwner,
            general_corporate_street1: general_corporate_street1,
            general_corporate_street2: general_corporate_street2,
            general_phone_number: general_phone_number,
            general_company: general_company,
            general_corporate_country: general_corporate_country,
            general_corporate_state: general_corporate_state,
            general_corporate_city: general_corporate_city,
            general_corporate_zip: general_corporate_zip,
            subscription_billing_street1: subscription_billing_street1,
            subscription_billing_street2: subscription_billing_street2,
            subscription_billing_country: subscription_billing_country,
            subscription_billing_state: subscription_billing_state,
            subscription_billing_city: subscription_billing_city,
            subscription_billing_zip: subscription_billing_zip,
            general_TaxRate: general_TaxRate,
            general_Language: general_Language,
            general_WeightPreference: general_WeightPreference,
            general_Timezone: general_Timezone,
        },
        dataType: "json",
        success: function (value) {
            if (value.status == 'success') {
                window.location = "general";
            } else {

            }
        },
    });
}

/*Smartling Translation Create Wizard */
$(".smartling-button").click(function (e) {
    //General Step Fields
    var smrt_project_id = $('#smrt_project_id input').val();
    var smrt_user_id = $('#smrt_user_id input').val();
    var smrt_secret_key = $('#smrt_secret_key input').val();
    //General Step Required Fields Validation
    var isValid = true;
    if (smrt_project_id == '') {
        $('#smrt_project_id').addClass('has-error');
        isValid = false;
    } else {
        if ($('#smrt_project_id').hasClass('has-error')) {
            $('#smrt_project_id').removeClass('has-error');
        }
    }

    if (smrt_user_id == '') {
        $('#smrt_user_id').addClass('has-error');
        isValid = false;
    } else {
        if ($('#smrt_user_id').hasClass('has-error')) {
            $('#smrt_user_id').removeClass('has-error');
        }
    }

    if (smrt_secret_key == '') {
        $('#smrt_secret_key').addClass('has-error');
        isValid = false;
    } else {
        if ($('#smrt_secret_key').hasClass('has-error')) {
            $('#smrt_secret_key').removeClass('has-error');
        }
    }

    if (isValid) {
        $.ajax({
            type: 'post',
            url: 'save-smartling-api',
            data: {
                smrt_project_id: smrt_project_id,
                smrt_user_id: smrt_user_id,
                smrt_secret_key: smrt_secret_key,
            },
            success: function () {
                window.location = "integrate-transaltion";
            },
        });
    }
    e.preventDefault();
});
/* END*/


/*Start For Corporate Documents form*/


$('#TaxID').editable();

$("#corporate_form_id").on('submit', (function (e) {
    e.preventDefault();
    $('.be-wrapper').addClass('be-loading-active');
    var tax_id_value = $('#TaxID').text();
    $('#tax_val').val(tax_id_value);
    $.ajax({
        url: "/upload-documents",
        type: "POST",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            var obj_data = JSON.parse(data);
            if (obj_data['error_msg'] != undefined) {
                var html_data_error = obj_data['error_msg'];
                $("#corporate_ajax_msg_eror").html(html_data_error);
                $("#ajax_header_error_msg").html('Error!');
                $('.corporate_ajax_request_error').css({
                    'display': 'block',
                    'background': 'rgba(0,0,0,0.6)',
                });
            }
            $('.be-wrapper').removeClass('be-loading-active');
            if (obj_data["success_msg"] != undefined) {
                var html_data = obj_data['success_msg'];
                $("#corporate_ajax_msg").html(html_data);
                $("#ajax_header_msg").html('Success!');
                $('#corporate_ajax_request').css({
                    'display': 'block',
                    'background': 'rgba(0,0,0,0.6)',
                });
            }
        },
        error: function () {
        }
    });
}));

$('.corporate_error_modal_close').click(function () {
    $('.corporate_ajax_request_error').css({
        'display': 'none',
        'background': 'rgba(0,0,0,0.6)',
    });
});
$('.corporate_modal_close').click(function () {
    $('#corporate_ajax_request').css({
        'display': 'none',
        'background': 'rgba(0,0,0,0.6)',
    });
    window.location.reload()
});

/*End For Corporate Documents form*/

//app ui sticky notifiactions starts here
function stickynotification(remainingdays) {
    if (remainingdays == '7' || remainingdays == '6') {
        $.extend($.gritter.options, {position: 'top-right'});
        $.gritter.add({
            title: 'Success',
            text: remainingdays + " days remains in your trial period.",
            image: BASE_URL + 'img/elliot-logo-small.svg',
            class_name: 'clean color success',
            time: '10000'
        });
        return false;
    } else if (remainingdays == '5' || remainingdays == '4' || remainingdays == '3') {
        $.extend($.gritter.options, {position: 'top-right'});
        $.gritter.add({
            title: 'Warning',
            text: remainingdays + " days remains in your trial period.",
            image: BASE_URL + 'img/elliot-logo-small.svg',
            class_name: 'clean color warning',
            time: '10000'
        });
        return false;
    } else if (remainingdays == '2' || remainingdays == '1') {
        $.extend($.gritter.options, {position: 'top-right'});
        $.gritter.add({
            title: 'Danger',
            text: remainingdays + " day(s) remains in your trial period.",
            image: BASE_URL + 'img/elliot-logo-small.svg',
            class_name: 'clean color danger',
            time: '10000'
        });
        return false;
    }

}
//app ui Success sticky notifiactions starts here
function SuccessStickyNotification(message) {
    $.extend($.gritter.options, {position: 'top-right'});
    $.gritter.add({
        title: 'Success',
        text: message,
        image: BASE_URL + 'img/elliot-logo-small.svg',
        class_name: 'clean color success',
        time: '10000'
    });
    return false;
}

//app ui Danger sticky notifiactions starts here
function DangerStickyNotification(message) {
    $.extend($.gritter.options, {position: 'top-right'});
    $.gritter.add({
        title: 'Danger',
        text: message,
        image: BASE_URL + 'img/elliot-logo-small.svg',
        class_name: 'clean color danger',
        time: '10000'
    });
    return false;
}

//app ui sticky notifiactions starts here
function sticky_ntf_bigcommerce_import(import_done, import_channel) {
    if (import_done == 'true') {
        $.extend($.gritter.options, {position: 'top-right'});
        $.gritter.add({
            title: 'Success',
            text: "Your " + import_channel + " data has been successfully imported.",
            image: BASE_URL + 'img/elliot-logo-small.svg',
            class_name: 'clean color success',
            time: '10000'
        });
        return false;
    } else if (import_done == 'false') {
        $.extend($.gritter.options, {position: 'top-right'});
        $.gritter.add({
            title: 'Danger',
            text: "Your " + import_channel + " data import has been failed.",
            image: BASE_URL + 'img/elliot-logo-small.svg',
            class_name: 'clean color danger',
            time: '10000'
        });
        return false;
    }
}

// Regarding channels 
$("#channels_view_table").dataTable({
    pageLength: 50,
    buttons: [
        'excel', 'pdf',
    ],
    dom: "<'row be-datatable-header'<'col-sm-6'l><'col-sm-6 text-right'B><'col-sm-12 text-right'f>>" +
            "<'row be-datatable-body'<'col-sm-12'tr>>" +
            "<'row be-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>"
});


//function see_store_data() {
// 
//  $(".connect").addClass("open");
//}

// for NestedUi categories

var App = (function () {
    'use strict';

    App.uiNestableLists = function ( ) {

        $('.dd').nestable();
        //Watch for list changes and show serialized output
        function update_out(selector, sel2) {
            var out = $(selector).nestable('serialize');
            $(sel2).html(window.JSON.stringify(out));

        }
        update_out('#list2', "#out2");
        $('#list2').on('change', function () {
            update_out('#list2', "#out2");
            var val = $("#out2").text();
            console.log(val);
            jQuery.ajax({
                type: 'post',
                url: 'categories/update-nested-cat',
                data: {data: val, },
                dataType: "json",
                success: function (value) {

                },
            });

        });

    };
    App.masks = function ( ) {
        $("#last_4_social").mask("9999");

    };
    return App;
})(App || {});

