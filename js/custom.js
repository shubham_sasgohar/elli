$(document).ready(function () {

    $(".swt99").each(function () {

        if ($(this).is(":checked"))
        {
            var conn_id = $(this).attr("data-connectionid");
            $('#smartling_editor'+conn_id).summernote({
                width: 1000, //don't use px
            });
            $(".div_translate_title"+conn_id).show();
        }
    });


//    if ($('.swt99').is(":checked"))
//    {
//
//        // $('#smartling_editor').summernote('enable');
//        $('#smartling_editor').summernote({
//            width: 1500, //don't use px
//        });
//        $(".div_translate_title").show();
//    }

//    $('body').tooltip({
//    selector: '[data-toggle="tooltip"]'
//});

    // if($('.getLiGraph').hasClass('active')) {
    //     alert('yesy');
    //     // $('.custOMPerf').css('display','none');

    // }

    //code for create customer Country on change 
    //code commented hai

    $('#CreateCustomerCountRY').on('change', function () {
        var country_name = $(this).val();
        if (country_name) {
            $.ajax({
                type: 'POST',
                url: 'createcountryajax',
                data: {
                    country_name: country_name
                },
                success: function (response) {
                    console.log(response);
                    // $('#CreateCustomerStatES').html('');
                    $('#CreateCustomerStatES').append(response);
                }
            });

        }
    });

    //code for create customer STATE on change 

    $("#CreateCustomerStatES").on('change', function () {
        var state_ID = $(this).val();
        if (state_ID) {
            $.ajax({
                type: 'POST',
                url: 'createcountryajax',
                data: {
                    state_ID: state_ID
                },
                success: function (response) {
                    console.log(response);
                    // $('#CreateCustomerCitiES').html('');
                    $('#CreateCustomerCitiES').append(response);
                }
            })
        }
    });

    //code for create customer ship country on change

    $("#createShipCountRY").on('change', function () {
        var ship_countryName = $(this).val();
        if (ship_countryName) {
            $.ajax({
                type: "POST",
                url: 'createcountryajax',
                data: {
                    ship_countryName: ship_countryName
                },
                success: function (response) {
                    console.log(response);
                    // $('#createShipStatES').html('');
                    $('#createShipStatES').append(response);
                }
            })
        }
    });

    //code for create customer ship State on change

    $('#createShipStatES').on('change', function () {
        var ship_StateID = $(this).val();
        if (ship_StateID) {
            $.ajax({
                type: "POST",
                url: 'createcountryajax',
                data: {
                    ship_StateID: ship_StateID
                },
                success: function (response) {
                    console.log(response);
                    // $('#createShipCitiES').html('');
                    $('#createShipCitiES').append(response);
                }
            });
        }
    });

    $('.customCC').click(function () {
        $('.custOMPerf').css('display', 'block'); //code for making the publish button show on perfromance tab only more code on line number 721
    });

    $(".wechat_sbmt1").click(function () {

        storename = $.trim($("#wechat_storename").val());
        password = $.trim($("#wechat_password").val());
        type = $.trim($("#wechat_type").val());
        if (!storename || !password) {
            $("#wechat_storename").addClass("inpt_required");
            $("#wechat_password").addClass("inpt_required");
            return false;
        } else {
            $("#wechat_storename").removeClass("inpt_required");
            $("#wechat_password").removeClass("inpt_required");
        }

        $.ajax({
            type: 'post',
            url: '/channels/wechat',
            data: {
                storename: storename,
                password: password,
                wechat_type: type,
            },
            beforeSend: function (xhr) {
                $(".be-wrapper").addClass("be-loading-active");
            },
            success: function (value) {
                $(".be-wrapper").removeClass("be-loading-active");
                if (value) {
                    $('#walkthechat_request').modal('show');
                    $(".afterwechatrequestmsg").show();
                    $(".wechatrequestform").hide();
                }
            },
        });
    })

    $(".wechat_import").click(function () {

        if (!$(this).hasClass("yyy")) {
            $(this).addClass("yyy");
            window.location = "/channels/wechatimport";
//    alert();
        }

        return false;
    })

    $(".channelfulfillment_error_modal_close").click(function () {
        $('.channelfulfillment-error').hide();
    });
    $(".channelfulfillment_modal_close").click(function () {
        $('.channelfulfillment-success').hide();
        $('.channelfulfillment-success-not-found').hide();
        $('.channelfulfillment-success-error-found').hide();
    });
    $(".google_error_modal_close").click(function () {
        $('.googlemod-error').hide();
    });
    $(".google_modal_close").click(function () {
        $('.googlemod-success').hide();
    });
    $('#googlefeedcheck').click(function () {

        if ($('#googlefeedcheck').is(':checked')) {
            var googlefeed = 'yes';
            $('.googlemod-success').show();
            $('.code').show();
            $('.code2').hide();
        } else {
            var googlefeed = 'no';
            $('.googlemod-error').show();
            $('.code').hide();
            $('.code2').hide();
        }


        $.ajax({
            type: 'post',
            url: '/google-shopping/feed/',
            data: {
                feed: googlefeed,
                // already: 'yes',
                // email: '',
            },
            beforeSend: function (xhr) {
                $(".be-wrapper").addClass("be-loading-active");
            },
            success: function (value) {

                $(".be-wrapper").removeClass("be-loading-active");
                if (value == 'error') {
                    $('#walkthechat_request_error').modal('show');
                } else if (value) {
                    // window.location.href = value;
                    // $('#walkthechat_request').modal('show');
                    //$(".afterwechatrequestmsg_already").show();
                    $(".div23").addClass('active');
                    $(".data2").addClass('active');
                    $(".data1").addClass('complete');
                    $(".div21").removeClass('active');
                }

//           location.reload();
            },
        });




    });

    $('.netsuite-success_modal_close').click(function () {
        location.reload();
    });
    $('.netsuitewizardfinish').click(function () {
        var elliotresturl = $('#elliotresturl').val();
        if (elliotresturl == '') {
            $("#elliotresturl").addClass("inpt_required");

            return false;
        } else {
            $("#elliotresturl").removeClass("inpt_required");

        }
        $.ajax({
            type: 'post',
            url: '/integrations/netsuiteupdateurl/',
            data: {
                elliotresturl: elliotresturl,
                // already: 'yes',
                // email: '',
            },
            beforeSend: function (xhr) {
                $(".be-wrapper").addClass("be-loading-active");
                //location.reload();
            },
            success: function (value) {

                $(".be-wrapper").removeClass("be-loading-active");
                $('.netsuite-success').show();
                //location.reload();
            },
        });
    });

    $('.netsuitecredentialupdate').click(function () {
        var netsuitecompanyid = $('#netsuitecompanyid').val();
        var netsuiteempemail = $('#netsuiteempemail').val();
        var netsuitemppass = $('#netsuitemppass').val();
        if (netsuitecompanyid == '' || netsuiteempemail == '' || netsuitemppass == '') {
            $("#netsuitecompanyid").addClass("inpt_required");
            $("#netsuiteempemail").addClass("inpt_required");
            $("#netsuitemppass").addClass("inpt_required");
            return false;
        } else {
            $("#netsuitecompanyid").removeClass("inpt_required");
            $("#netsuiteempemail").removeClass("inpt_required");
            $("#netsuitemppass").removeClass("inpt_required");
        }
        $.ajax({
            type: 'post',
            url: '/integrations/netsuiteupdate/',
            data: {
                netsuitecompanyid: netsuitecompanyid,
                netsuiteempemail: netsuiteempemail,
                netsuitemppass: netsuitemppass,
                // already: 'yes',
                // email: '',
            },
            beforeSend: function (xhr) {
                $(".be-wrapper").addClass("be-loading-active");
                //location.reload();
            },
            success: function (value) {

                $(".be-wrapper").removeClass("be-loading-active");
                $('#netsuiterole').append(value);
                /* $("#smartjob1").removeClass("active");
                 $("#smartjobli1").removeClass("active");
                 $("#smartjob2").addClass("active");
                 $("#smartjobli2").addClass("active"); */

                /* if (value == 'error') {
                 $('#walkthechat_request_error').modal('show');
                 } */

//           location.reload();
            },
        });
    });






    $('.smartling-button-jobs').click(function () {
        var token = $(this).val();
        $('#proidunique').remove();
        $('.proidget').append('<input type="hidden" value="' + token + '" id="proidunique" />');
        /* if (smartlingprojectid == '' || smartlinguserid == '' || smartlingsecretkey == '' ) {
         $("#smartlingprojectid").addClass("inpt_required");
         $("#smartlingsecretkey").addClass("inpt_required");
         $("#smartlinguserid").addClass("inpt_required");
         return false;
         } else {
         $("#smartlingprojectid").removeClass("inpt_required");
         $("#smartlingsecretkey").removeClass("inpt_required");
         $("#smartlinguserid").removeClass("inpt_required");
         } */
        $.ajax({
            type: 'post',
            url: '/channelsetting/jobsetting/',
            data: {
                token: token,
                // already: 'yes',
                // email: '',
            },
            beforeSend: function (xhr) {
                $(".be-wrapper").addClass("be-loading-active");
                //location.reload();
            },
            success: function (value) {

                $(".be-wrapper").removeClass("be-loading-active");
                $("#smartjob1").removeClass("active");
                $("#smartjobli1").removeClass("active");
                $("#smartjobli1").addClass("complete");
                $("#smartjob2").addClass("active");
                $("#smartjobli2").addClass("active");

                /* if (value == 'error') {
                 $('#walkthechat_request_error').modal('show');
                 } */

//           location.reload();
            },
        });
    });

    /*For Smartling Editore in translation Tab*/

    $('.trans_title').editable();
    $('.swt99').change(function () {

        if ($(this).is(":checked"))
        {
            var connection_id = $(this).attr("data-connectionid");
            var channel_acc = $(this).attr("data-channel");
            var product_id = $(this).attr("data-productid");
            $.ajax({
                type: 'post',
                url: '/products/smarting-translation-status-enable',
                data: {
                    connection_id: connection_id,
                    channel_acc: channel_acc,
                    product_id: product_id,
                },

                success: function (value) {
                    if (value == 'success') {
                        //$('#smartling_editor').summernote('enable');
                        $('#smartling_editor'+connection_id).summernote({
                            width: 1000, //don't use px
                        });
                        $(".div_translate_title"+connection_id).show();
                        $('.smartling_editor'+connection_id).next().show();
                        //$(".smartling_editor"+connection_id+" .note-editor.note-frame.panel.panel-default").show();
                    }
                },
            });



            // $("#smartling_editor").css("display", "block");
        } else {
            // $('.note-editor').hide();
            //  $("#smartling_editor").css("display", "none");
            var connection_id = $(this).attr("data-connectionid");
            var channel_acc = $(this).attr("data-channel");
            var product_id = $(this).attr("data-productid");
            $.ajax({
                type: 'post',
                url: '/products/smarting-translation-status-disable',
                data: {
                    connection_id: connection_id,
                    channel_acc: channel_acc,
                    product_id: product_id,
                },

                success: function (value) {
                    if (value == 'success') {
                        $(".div_translate_title"+connection_id).hide();
                        $('.smartling_editor'+connection_id).next().hide();
                        //$(".smartling_editor"+connection_id+" .note-editor.note-frame.panel.panel-default").hide();
                    }
                },
            });
        }
    });
    $('#channelsmartlingyes').change(function () {
        if ($('#channelsmartlingyes').is(':checked')) {
            var channelsmartlingyes = 'yes';
        } else {
            var channelsmartlingyes = 'no';
        }
        if ($('#smartlingyes').is(":checked"))
        {
            var smartlingyes = 'yes';
        } else {
            var smartlingyes = 'no';
        }



        var channel_id_translation = $('#channel_id_translation').val();
        var Main_channel_id_translation = $('#Main_channel_id_translation').val();
        var language = $("#selectTranslationsetting option:selected").text();
        var selectTranslation = $("#selectTranslation option:selected").text();

        if (channelsmartlingyes == 'no') {

            $.ajax({
                type: 'post',
                url: '/channelsetting/translationchannelsettingdisable/',
                data: {
                    enable: channelsmartlingyes,
                    smartlingyes: smartlingyes,
                    channel_id: channel_id_translation,
                    Main_channel_id_translation: Main_channel_id_translation,
                    selectTranslation: selectTranslation,
                    language: language,
                    // already: 'yes',
                    // email: '',
                },
                beforeSend: function (xhr) {
                    $(".be-wrapper").addClass("be-loading-active");
                    //location.reload();
                },
                success: function (value) {

                    $(".be-wrapper").removeClass("be-loading-active");
                    $('.translatoinfulfillment12-success').show();
                    /* if (value == 'error') {
                     $('#walkthechat_request_error').modal('show');
                     } */

//           location.reload();
                },
            });
        }
    });

    $('.translatoinfulfillment_modal_close').click(function () {
        $('.translatoinfulfillment-success').hide();
    });
    $('.translatoinfulfillment12_modal_close').click(function () {

        $('.translatoinfulfillment12-success').hide();
    });
    $('.translatoinfulfillment1_modal_close').click(function () {
        $('.translatoinfulfillment1-success').hide();
    });
    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
    // $('.smartling_enble_controller').click(function () {
    $('body').on('click', '.smartling_enble_controller', function () {

        checkCredit = $('#checkCredit').val();
        if (checkCredit == '1') {
            creditemail = $('#creditemail').val();
            creditcart = $('#creditcart').val();
            creditdate = $('#creditdate').val();
            creditcvc = $('#creditcvc').val();

            if (creditemail == '')
            {
                $('#smartling_creditemail').addClass('has-error');
                //alert('Enter Email Address');
                return false;
            } else
            {
                if ($('#smartling_creditemail').hasClass('has-error')) {
                    $('#smartling_creditemail').removeClass('has-error');
                }
            }
            if (creditcart == '')
            {
                $('#smartling_creditcart').addClass('has-error');
                //alert('Enter Card Number');
                return false;
            } else
            {
                if ($('#smartling_creditcart').hasClass('has-error')) {
                    $('#smartling_creditcart').removeClass('has-error');
                }
            }
            if (creditdate == '')
            {
                $('#smartling_creditdate').addClass('has-error');
                //alert('Enter Valid Exp Date/Month');
                return false;
            } else
            {
                if ($('#smartling_creditdate').hasClass('has-error')) {
                    $('#smartling_creditdate').removeClass('has-error');
                }
            }
            if (creditcvc == '')
            {
                $('#smartling_creditcvc').addClass('has-error');
                //alert('Enter CVC Code');
                return false;
            } else
            {
                if ($('#smartling_creditcvc').hasClass('has-error')) {
                    $('#smartling_creditcvc').removeClass('has-error');
                }
            }


            if ($('#channelsmartlingyes').is(':checked')) {
                var channelsmartlingyes = 'yes';
            } else {
                var channelsmartlingyes = 'no';
            }
            if ($('#smartlingyes').is(":checked"))
            {
                var smartlingyes = 'yes';
            } else {
                var smartlingyes = 'no';
            }

            var credit = 0;

            var channel_id_translation = $('#channel_id_translation').val();
            var Main_channel_id_translation = $('#Main_channel_id_translation').val();
            var language = $("#selectTranslationsetting option:selected").text();
            var selectTranslation = $("#selectTranslation option:selected").val();
            /* alert(channelsmartlingyes);
             alert(checkedsmart); */
            if (channelsmartlingyes == 'yes') {
                $('.smartling_enble_controller').html('Hold For A While');

                $.ajax({
                    type: 'post',
                    url: '/site/subscriptionpaymentcustom',
                    data: {
                        enable: channelsmartlingyes,
                        smartlingyes: smartlingyes,
                        channel_id: channel_id_translation,
                        Main_channel_id_translation: Main_channel_id_translation,
                        selectTranslation: selectTranslation,
                        language: language,
                        credit: credit,
                        creditemail: creditemail,
                        creditcart: creditcart,
                        creditdate: creditdate,
                        creditcvc: creditcvc,
                        // already: 'yes',
                        // email: '',
                    },
                    beforeSend: function (xhr) {
                        $(".be-wrapper").addClass("be-loading-active");

                        //location.reload();
                    },
                    success: function (value) {
                        if (value = 'succeeded') {

                            $.ajax({
                                type: 'post',
                                url: '/channelsetting/translationchannelsetting/',
                                data: {
                                    enable: channelsmartlingyes,
                                    smartlingyes: smartlingyes,
                                    channel_id: channel_id_translation,
                                    Main_channel_id_translation: Main_channel_id_translation,
                                    selectTranslation: selectTranslation,
                                    language: language,
                                    credit: credit,
                                    /* creditemail: creditemail,
                                     creditcart: creditcart,
                                     creditdate: creditdate,
                                     creditcvc: creditcvc, */
                                    // already: 'yes',
                                    // email: '',
                                },
                                beforeSend: function (xhr) {
                                    $(".be-wrapper").addClass("be-loading-active");
                                    //location.reload();
                                },
                                success: function (value) {


                                    $(".be-wrapper").removeClass("be-loading-active");
                                    $('.translatoinfulfillment-success').hide();

                                    $('.alldataadd').html(value);
                                    //alert(value);
                                    $('.translatoinfulfillment1-success').show();
                                    /* if (value == 'error') {
                                     $('#walkthechat_request_error').modal('show');
                                     } */

                                    //           location.reload();
                                },
                            });

                        }


                    },
                    error: function (error) {
                        $(".be-wrapper").removeClass("be-loading-active");
                        alert('Enter Valid Card Details');
                    }
                });

            }
        } else {

            //$('.translatoinfulfillment-success').hide();
            var credit = 1;

            if ($('#channelsmartlingyes').is(':checked')) {
                var channelsmartlingyes = 'yes';
            } else {
                var channelsmartlingyes = 'no';
            }
            if ($('#smartlingyes').is(":checked"))
            {
                var smartlingyes = 'yes';
            } else {
                var smartlingyes = 'no';
            }


            creditemail = $('#creditemail').val();
            creditcart = $('#creditcart').val();
            creditdate = $('#creditdate').val();
            creditcvc = $('#creditcvc').val();
            var channel_id_translation = $('#channel_id_translation').val();
            var Main_channel_id_translation = $('#Main_channel_id_translation').val();
            var language = $("#selectTranslationsetting option:selected").text();
            var selectTranslation = $("#selectTranslation option:selected").val();
            /* alert(channelsmartlingyes);
             alert(checkedsmart); */
            if (channelsmartlingyes == 'yes') {

                $('.smartling_enble_controller').html('Hold On <img src="' + BASE_URL + '/img/Spinner.gif" alt="here!" style="width: 30px; height: 30px;"/>');

                $.ajax({
                    type: 'post',
                    url: '/channelsetting/translationchannelsetting/',
                    data: {
                        enable: channelsmartlingyes,
                        smartlingyes: smartlingyes,
                        channel_id: channel_id_translation,
                        Main_channel_id_translation: Main_channel_id_translation,
                        selectTranslation: selectTranslation,
                        language: language,
                        credit: credit,
                        // already: 'yes',
                        // email: '',
                    },
                    beforeSend: function (xhr) {
                        $(".be-wrapper").addClass("be-loading-active");
                        //location.reload();
                    },
                    success: function (value) {

                        $(".be-wrapper").removeClass("be-loading-active");
                        $('.translatoinfulfillment-success').hide();

                        $('.alldataadd').html(value);
                        //alert(value);
                        $('.translatoinfulfillment1-success').show();

                    },
                });


            }

        }

    });
    $('#translation_corporate_id').click(function () {
        if ($('#channelsmartlingyes').is(':checked')) {
            var channelsmartlingyes = 'yes';
        } else {
            var channelsmartlingyes = 'no';
        }
        if ($('#smartlingyes').is(":checked"))
        {
            var smartlingyes = 'yes';
        } else {
            var smartlingyes = 'no';
        }

        if (smartlingyes == 'no') {
            alert('Please Accept terms and conditions');
            return false;
        }

        var channel_id_translation = $('#channel_id_translation').val();
        var Main_channel_id_translation = $('#Main_channel_id_translation').val();
        var language = $("#selectTranslationsetting option:selected").text();
        var selectTranslation = $("#selectTranslation option:selected").val();
        /* alert(channelsmartlingyes);
         alert(checkedsmart); */
        if (channelsmartlingyes == 'yes') {
            $.ajax({
                type: 'post',
                url: '/channelsetting/translationchannelcost/',
                data: {
                    enable: channelsmartlingyes,
                    smartlingyes: smartlingyes,
                    channel_id: channel_id_translation,
                    Main_channel_id_translation: Main_channel_id_translation,
                    selectTranslation: selectTranslation,
                    language: language,
                    // already: 'yes',
                    // email: '',
                },
                beforeSend: function (xhr) {
                    $(".be-wrapper").addClass("be-loading-active");
                    //location.reload();
                },
                success: function (value) {

                    $(".be-wrapper").removeClass("be-loading-active");
                    $('.translatoinfulfillment-success').show();
                    $('#priceShow').html('Translation Cost - $' + value + '/-');
                    $('#getValuePrice').html(value);
                    /* if (value == 'error') {
                     $('#walkthechat_request_error').modal('show');
                     } */

//           location.reload();
                },
            });


        }
    });

    $('.credit_card_close').click(function () {
        $('.translatoinfulfillment-success').css('display', 'none');
    });

    $('.trans_rate_modal_close').click(function () {
        $('.translationmod-rate').hide();
    });
    $('#sfexpresscheck').click(function () {

        if ($('#sfexpresscheck').is(':checked')) {
            var sfexpresscheck = 'yes';
            $('.googlemod-success').show();
            $('.code').show();
            $('.code2').hide();
        } else {
            var sfexpresscheck = 'no';
            $('.googlemod-error').show();
            $('.code').hide();
            $('.code2').hide();
        }


        $.ajax({
            type: 'post',
            url: '/sfexpress/default/',
            data: {
                feed: sfexpresscheck,
                // already: 'yes',
                // email: '',
            },
            beforeSend: function (xhr) {
                $(".be-wrapper").addClass("be-loading-active");
                location.reload();
            },
            success: function (value) {

                $(".be-wrapper").removeClass("be-loading-active");
                if (value == 'error') {
                    $('#walkthechat_request_error').modal('show');
                }

//           location.reload();
            },
        });




    });






    $('#selectTranslation').on('change', function () {
        if (this.value == 'Google MT') {
            $('#humantran').show();
            $('#machinetran').hide();
            $('#Hybrid').hide();
        } else if (this.value == 'Translation with Edit') {
            $('#humantran').hide();
            $('#machinetran').hide();
            $('#Hybrid').show();
        } else {
            $('#humantran').hide();
            $('#machinetran').show();
            $('#Hybrid').hide();
        }
    });

    $('.trans_rate_modal_close').click(function () {
        $('.translationmod-rate').hide();
    });
    $('.trans_error_modal_close').click(function () {
        $('.translationmod-error').hide();

    });
    $('.trans_modal_close').click(function () {
        $('.translationmod-success').hide();

    });
    $('#getRateTable').click(function () {
        $('.translationmod-rate').show();

    });
    $('.translationwizard').click(function () {
        var translation_type = $('#select2-selectTranslation-container').text();
        if ($('#smartlingyes').is(':checked')) {
            var smartlingyes = 'yes';
            $('.translationmod-success').show();
        } else {
            var smartlingyes = 'no';
            $('.translationmod-error').show();

        }
        var proidval = $('#proidunique').val();
        $('.be-wrapper').addClass('be-loading-active');
        $.ajax({
            type: 'post',
            url: "/channelsetting/translationsave",
            data: {
                translation_type: translation_type,
                smartlingyes: smartlingyes,
                proidval: proidval,
            },
            beforeSend: function (xhr) {
                $(".be-wrapper").addClass("be-loading-active");
            },
            success: function (value) {
                $(".be-wrapper").removeClass("be-loading-active");
            },
        });
    });
    $("#smartling_form_id").on('submit', (function (e) {
        e.preventDefault();
        $('.be-wrapper').addClass('be-loading-active');
        $.ajax({
            url: "/channelsetting/translation",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                //alert(data);
                // var obj_data = JSON.parse(data);
                $('.be-wrapper').removeClass('be-loading-active');
            },
            error: function () {
            }
        });
    }));

    $('#channelfulfillment').click(function () {
        if ($('#channelfulfillment').is(':checked')) {
            var control = 'yes';
            $('#newfullfill').show();
            $('#fulfillmentlist').show();
        } else {
            $('#newfullfill').hide();
            $('#fulfillmentlist').hide();
            $('#disable-warning-fulfilled').addClass('in');
            $('#disable-warning-fulfilled').show();
        }
    });

    $('.proceed_todlt_fulfillment').click(function () {
        var fulfillmentID = $('#fulfillmentlist').find(":selected").val();
        var fulfillmentName = $('#fulfillmentlist').find(":selected").text();
        var channelID = $('#channelId').val();
        $('#disable-warning-fulfilled').removeClass('in');
        $('#disable-warning-fulfilled').hide();
        var control = 'no';
        $('#fulfillmentlist').hide();
        $.ajax({
            type: 'post',
            url: '/channelsetting/save/',
            data: {
                control: control,
                channelID: channelID,
                fulfillmentName: fulfillmentName,
                fulfillmentID: fulfillmentID,
            },
            beforeSend: function (xhr) {
                $(".be-wrapper").addClass("be-loading-active");
            },
            success: function (value) {
                $(".be-wrapper").removeClass("be-loading-active");
            },
        });

    });

    $(".todlt_fulfillment_close").click(function () {
        $("#disable-warning-fulfilled").css('display', 'none');
    });

    $('#fulfillmentlist').change(function () {
        var fulfillmentID = $('#fulfillmentlist').find(":selected").val();
        var fulfillmentName = $('#fulfillmentlist').find(":selected").text();
        var channelID = $('#channelId').val();
        if (!fulfillmentID) {
            $("#fulfillmentlist").addClass("inpt_required");
            return false;
        } else {
            $("#fulfillmentlist").removeClass("inpt_required");
        }
        if ($('#channelfulfillment').is(':checked')) {
            var control = 'yes';

        } else {
            var control = 'no';
            $('.channelfulfillment-error').show();
            $('#fulfillmentlist').removeAttr('selected').find('option:first').attr('selected', 'selected');
        }


        $.ajax({
            type: 'post',
            url: '/channelsetting/save/',
            data: {
                control: control,
                channelID: channelID,
                fulfillmentName: fulfillmentName,
                fulfillmentID: fulfillmentID,
            },
            beforeSend: function (xhr) {
                $(".be-wrapper").addClass("be-loading-active");
            },
            success: function (value) {

                $(".be-wrapper").removeClass("be-loading-active");
                if (value == 'error') {
                    $('#walkthechat_request_error').modal('show');
                } else if (value) {
                    var storeid = $('#channelId').val();
                    var fulfillmentID = $('#fulfillmentlist').find(":selected").val();
                    if (fulfillmentID == 5) {
                        $.ajax({
                            type: 'post',
                            url: '/channelsettingorder/sforders/',
                            data: {storeid: storeid, fulfillmentID: fulfillmentID},
                            success: function (value) {//alert(value);
                                if (value == 'success') {
                                    $('.channelfulfillment-success').show();
                                } else {
                                    $('.channelfulfillment-success-error-found').show();
                                    $('.channelfulfillment-success-error-found #magento_ajax_msg').html(value);
                                }
                            }
                        });
                    } else {
                        $('.channelfulfillment-success-not-found').show();
                        //alert('No Fulfillment Found..!');
                    }
                }

//           location.reload();
            },
        });




    });











    $(".fb_error_modal_close").click(function () {
        $('.fbmod-error').hide();
    });
    $(".fb_modal_close").click(function () {
        $('.fbmod-success').hide();
    });
    $('#facebookfeedcheck').click(function () {

        if ($('#facebookfeedcheck').is(':checked')) {
            var fb_feed = 'yes';
            $('.fbmod-success').show();
            $('.code').show();
            $('.code2').hide();
        } else {
            var fb_feed = 'no';
            $('.fbmod-error').show();
            $('.code').hide();
            $('.code2').hide();
        }

        $.ajax({
            type: 'post',
            url: '/facebook/savechannel/',
            data: {
                feed: fb_feed,
                // already: 'yes',
                // email: '',
            },
            beforeSend: function (xhr) {
                $(".be-wrapper").addClass("be-loading-active");
            },
            success: function (value) {

                $(".be-wrapper").removeClass("be-loading-active");
                if (value == 'error') {
                    $('#walkthechat_request_error').modal('show');
                } else if (value) {
                    // window.location.href = value;
                    // $('#walkthechat_request').modal('show');
                    //$(".afterwechatrequestmsg_already").show();
                    $(".div23").addClass('active');
                    $(".data2").addClass('active');
                    $(".data1").addClass('complete');
                    $(".div21").removeClass('active');
                }

//           location.reload();
            },
        });




    });
    $('.subscribeChannel').on('click', function () {
        $this = $(this);
        if ($this.attr('data-type') == 'channel_facebook') {
            window.location.href = '/facebook/facebook';
        }
    });

    $("#channel_WeChat-Store .btn-primary").click(function () {
        var radioValue = '';
        radioValue = $("#channel_WeChat-Store input[name='rad3']:checked").val();

        if (typeof radioValue != 'undefined') {
            if ($("#channel_WeChat-Store").hasClass("admin_pg")) {
                window.location = "/channels/wechatconnect?type=" + radioValue;
            } else {
                window.location = "/channels/wechat?type=" + radioValue;
            }

            $("#channel_WeChat-Store label").removeClass("checkrequired");
        } else {
            $("#channel_WeChat-Store label").addClass("checkrequired");
        }

    });

    $("#channel_Lazada-Vietnam .btn-primary").click(function () {
        var radioValue = '';
        radioValue = $("#channel_Lazada-Vietnam input[name='rad3']:checked").val();

        if (typeof radioValue != 'undefined') {
            lazada_type = radioValue.split(' ');
            window.location = "/lazada/lazada/?type=" + lazada_type[1];

            $("#channel_Lazada-Vietnam label").removeClass("checkrequired");
        } else {
            $("#channel_Lazada-Vietnam label").addClass("checkrequired");
        }

    });


    $(".wizard-next-auth-store-lazada").click(function () {
        lazada_user_email = $.trim($("#lazada_user_email").val());
        lazada_api_key = $.trim($("#lazada_api_key").val());
        lazada_api_url = $.trim($("#lazada_api_url").val());
        channel_type = $.trim($("#channel_type").val());
        channel_id = $.trim($("#channel_id").val());
        var user_id = $('#loggedin_user_id').val();
        if (!lazada_api_key || !lazada_user_email || !lazada_api_url) {
            $("#lazada_api_key").addClass("inpt_required");
            $("#lazada_user_email").addClass("inpt_required");
            $("#lazada_api_url").addClass("inpt_required");
            return false;
        } else {
            $("#lazada_api_key").removeClass("inpt_required");
            $("#lazada_user_email").removeClass("inpt_required");
        }
//alert('he');return false;
        $.ajax({
            type: 'post',
//            url: '/lazada/lazada/?type=' + channel_type,
            url: '/lazada/auth-lazada-malaysia/?type=' + channel_type,
            data: {
                lazada_user_email: lazada_user_email,
                lazada_api_key: lazada_api_key,
                lazada_api_url: lazada_api_url,
                channel_id: channel_id,
            },
            beforeSend: function (xhr) {
                $(".be-loading").first().addClass("be-loading-active");
            },
            success: function (value) {
                var obj = JSON.parse(value);
                $(".be-loading").first().removeClass("be-loading-active");
                $('.be-wrapper').removeClass('be-loading-active');
                if (obj["api_error"] != undefined)
                {
                    var html_data_error = obj['api_error'];
                    $("#lazada_ajax_msg_eror").html(html_data_error);
                    $("#lazada_ajax_header_error_msg").html('Error!');
                    $('.lazada_ajax_request_error').modal('show');
                }
                if (obj["success"] != undefined)
                {
                    var html_data = obj['success'];
                    $("#lazada_ajax_msg").html(html_data);
                    $("#lazada_ajax_header_msg").html('Success!');
                    $('#lazada-authorize-form').hide();
                    $('#lazada_ajax_request').modal('show');
                    $('#lazada-authorize-form')[0].reset();
                    $.ajax({
                        type: 'get',
                        url: BASE_URL + 'lazada/malaysia-importing/?type=' + channel_type,
                        data: {
                            user_id: user_id,
                        },
                        complete: function (data) {
                            window.location.reload();
                        }
                    });
                }
                if (obj["error"] != undefined)
                {
                    var html_data = obj['error'];
                    $("#lazada_ajax_msg_eror").html(html_data);
                    $("#lazada_ajax_header_error_msg").html('Error!');
                    $('.lazada_ajax_request_error').modal('show');
                }


            },
        });
    });
    $('#lazada_ajax_request .mdi-close').click(function () {
        $('#lazada_ajax_request').modal('hide');
        window.location.reload()
    });

    $('#lazada_ajax_request .lazada_modal_close').click(function () {
        $('#lazada_ajax_request').modal('hide');
        window.location.reload()
    });

    $('.lazada_error_modal_close').click(function () {
        $('.lazada_ajax_request_error').modal('hide');
    });


    $('#channelsonproductview').editable();
    $('#cats_list').editable();
//    var currencies_array=[];
//    $.ajax({
//        type: 'get',
//        dataType: 'json',
//        data: {},
//        url: '/integrations/currencies',
//        success: function (data) {
//          currencies_array=data;
//            console.log(currencies_array);
//        }
//    });

    $('#general_CurrencyPreference').editable({
        source: '/integrations/currencies',
        success: function (response, newValue) {
            var request = $.ajax({
                url: "/site/savecurrency",
                type: "POST",
                data: {currency: newValue},
                dataType: "json"
            });
            request.done(function (msg) {
                $('.currency_symbol').html(msg.currency_symbol);
                $('#annual_revenue').editable('setValue', msg.annual_revenue);
            });
        }
    });
    $('#annual_revenue').editable({

        success: function (response, newValue) {
            var request = $.ajax({
                url: "/site/saverevenue",
                type: "POST",
                data: {revenue: newValue},
//                dataType: "html"
            });

            request.done(function (msg) {
//                alert(msg);
            });
        }
    });
    $('#annual_order_revenue').editable({

        success: function (response, newValue) {
            var request = $.ajax({
                url: "/site/saveorderrevenue",
                type: "POST",
                data: {revenue: newValue},
//                dataType: "html"
            });

            request.done(function (msg) {
//                alert(msg);
            });
        }
    });
    $('#channel_CurrencyPreference').editable({
        source: '/integrations/currencies',
        success: function (response, newValue) {
            var request = $.ajax({
                url: "/site/savechannelcurrency",
                type: "POST",
                data: {currency: newValue},
                dataType: "json"
            });
            request.done(function (msg) {
                $('.currency_symbol').html(msg.currency_symbol);
                $('#annual_revenue').editable('setValue', msg.annual_revenue);
            });
        }
    });
})
//Add Categories
function updatecategory() {

    var add_cat_name = $('#add_cat_name').text();
    var add_parent_cat_name = $('#add_parent_cat_name').text();
    if (add_cat_name == 'Empty') {
        document.getElementById("span_err_add_cat_name").innerHTML = "* Please Enter Category Name";
        return false;
    } else {
        document.getElementById("span_err_add_cat_name").innerHTML = "";
    }


//    jQuery.ajax({
//        type: 'post',
//        url: 'create',
//        data: {
//            add_cat_name: add_cat_name,
//            add_parent_cat_name: add_parent_cat_name,
//           
//        },
//        dataType: "json",
//        success: function (value) {
//            if (value.status == 'success') {
//                window.location = "profile";
//            } else {
//
//            }
//        },
//    });
}


// Save Customer Information



//Create Attribute
/*AutoComplete for country state zip - Customer Create*/
// $("#customer-Country input").autocomplete({
//     source: 'get-country'
// });
// $("#customer-City input").autocomplete({
//     source: 'get-cities'
// });
// $("#customer-State input").autocomplete({
//     source: 'get-states'
// });
// $("#customer-ship-Country input").autocomplete({
//     source: 'get-country'
// });
// $("#customer-ship-City input").autocomplete({
//     source: 'get-cities'
// });
// $("#customer-ship-State input").autocomplete({
//     source: 'get-states'
// });
$(document).on("click", "#customer_submit", function (event) {
    var customer_first_name = $('#customer-first-name input').val();
    var customer_last_name = $('#customer-last-name input').val();
    var customer_email = $('#customer-email input').val();
    var bill_steet1 = $('#customer-Street-1 input').val();
    var bill_steet2 = $('#customer-Street-2 input').val();
    var bill_country = $('#customer-Country select').val();
    var bill_city = $('#customer-City select').val();
    var bill_state = $('#customer-State select').val();
    var ship_country = $('#customer-Country-ship select').val();
    var ship_city = $('#customer-City-ship select').val();
    var ship_state = $('#customer-State-ship select').val();
    var bill_zip = $('#customer-Zip input').val();

    var chk = true;
    $(".customer_validate").each(function () {
        var val = $.trim($(this).val());
        if (!val || val == 'Please Select Value' || val == 'Please Select Country First' || val == 'Please Select State First') {
            $(this).addClass("inpt_required");
            chk = false;
        } else {
            //     if(val == 'Please Select Value') {
            //          $(this).addClass("inpt_required");
            //     chk = false;
            // } else {

            $(this).removeClass("inpt_required");
            // }
        }
    })

    if (!chk) {
        return false;
    }

});

/***************For content Create Form Validations********************/

$(document).on('click', "#Content_submit", function (e) {
    e.preventDefault();
    var page_title = $('#page_title input').val();
    var page_desc = $('#page_description').summernote('code');
    var chk = true;
    $('.content_validate').each(function () {
        var val = $.trim($(this).val());
        if (!val) {
            $(this).addClass("inpt_required");
            chk = false;
        } else {
            $(this).removeClass("inpt_required");
        }
    });
    if (!chk) {
        return false;
    } else {
        $.ajax({
            type: 'post',
            url: '/content/create',
            data: {
                'page_title': page_title,
                'page_desc': page_desc
            },
            success: function (response) {
                console.log(response);
            }
        });


    }

});


/************For update form validations and ajax*******************/

$(document).on('click', '#Content_update_submit', function (e) {
    e.preventDefault();
    var page_title_update = $("#page_title_update input").val();
    var page_desc_update = $("#page_description_update").summernote('code');
    var chk = true;
    $('.content_validate').each(function () {
        var val = $.trim($(this).val());
        if (!val) {
            $(this).addClass("inpt_required");
            chk = false;
        } else {
            $(this).removeClass("inpt_required");
        }
    });
    if (!chk) {
        return false;
    } else {
        var page_id = $("#update_page_ID").val();
        $.ajax({
            type: 'post',
            url: '/content/updatepage',
            data: {
                'page_title': page_title_update,
                'page_desc': page_desc_update,
                'page_id': page_id
            },
            success: function (response) {
                console.log(response);
            }
        });

    }


});


/**********For dashboard Main chart buttons click***************/
/*click on month button put value in hiiden field*/
$("#Month").click(function () {
    $("#week_row_div").css("display", "none");
    $("#today_row_div").css("display", "none");
    $("#Year_row_div").css("display", "none");
    $("#main-chart1").css("display", "none");
    $("#main-chartToday").css("display", "none");
    $("#main-chartYear").css("display", "none");
    $("#month_row_div").css("display", "block");
    $("#main-chartMonthly").css("display", "block");
    $('#hidden_graph').val('Month');
    $('.active-remove').removeClass('active');
    $(this).addClass('active');
    App.dashboard();
});

// click on Year button put value in hiiden field
$("#Year").click(function () {
    $('#hidden_graph').val('Year');
    $("#month_row_div").css("display", "none");
    $("#week_row_div").css("display", "none");
    $("#today_row_div").css("display", "none");
    $("#main-chartToday").css("display", "none");
    $("#main-chart1").css("display", "none");
    $("#main-chartMonthly").css("display", "none");
    $("#main-chartYear").css("display", "block");
    $("#Year_row_div").css("display", "block");
    $('.active-remove').removeClass('active');
    $(this).addClass('active');
    App.dashboard();
});
// click on WEEK button put value in hiiden field
$("#Week").click(function () {
    $("#month_row_div").css("display", "none");
    $("#today_row_div").css("display", "none");
    $("#Year_row_div").css("display", "none");
    $("#main-chartMonthly").css("display", "none");
    $("#main-chartToday").css("display", "none");
    $("#main-chartYear").css("display", "none");
    $("#main-chart1").css("display", "block");
    $("#week_row_div").css("display", "block");
    $('#hidden_graph').val('Week');
    $('.active-remove').removeClass('active');
    $(this).addClass('active');
    App.dashboard();
});

// click on Today button put value in hiiden field
$("#Today").click(function () {

    $("#month_row_div").css("display", "none");
    $("#week_row_div").css("display", "none");
    $("#Year_row_div").css("display", "none");
    $("#main-chart1").css("display", "none");
    $("#main-chartMonthly").css("display", "none");
    $("#main-chartYear").css("display", "none");
    $("#main-chartToday").css("display", "block");
    $("#today_row_div").css("display", "block");
    $('#hidden_graph').val('Today');
    $('.active-remove').removeClass('active');
    $(this).addClass('active');
    App.dashboard();
});
/**********End dashboard Main chart buttons click***************/



/**********For Single Product Perforamnce Main chart buttons click***************/
if ($('#hidden_product_id').length > 0) {

    var hidden_product_id = $('#hidden_product_id').val();
}

// click on Weekely button put value in hiiden field
$("#Weekly").click(function () {
    $("#main-chart_Annually").css("display", "none");
    $("#main-chart_Monthly").css("display", "none");
    $("#main-chart_daily").css("display", "none");
    $("#main-chart_Weekly").css("display", "block");
    $('#hidden_graph').val('Weekly' + hidden_product_id);
    $('.active-remove').removeClass('active');
    $(this).addClass('active');
    App.dashboard();
});

// click on Monthly button put value in hiiden field
$("#Monthly").click(function () {
    $("#main-chart_Annually").css("display", "none");
    $("#main-chart_Weekly").css("display", "none");
    $("#main-chart_daily").css("display", "none");
    $("#main-chart_Monthly").css("display", "block");
    $('#hidden_graph').val('Monthly' + hidden_product_id);
    $('.active-remove').removeClass('active');
    $(this).addClass('active');
    App.dashboard();
});

// click on Annually button put value in hiiden field
$("#Annually").click(function () {
    $("#main-chart_Weekly").css("display", "none");
    $("#main-chart_Monthly").css("display", "none");
    $("#main-chart_daily").css("display", "none");
    $("#main-chart_Annually").css("display", "block");
    $('#hidden_graph').val('Annually' + hidden_product_id);
    $('.active-remove').removeClass('active');
    $(this).addClass('active');
    App.dashboard();
});

// click on Daily button put value in hiiden field
$("#daily").click(function () {
    $("#main-chart_Weekly").css("display", "none");
    $("#main-chart_Monthly").css("display", "none");
    $("#main-chart_Annually").css("display", "none");
    $("#main-chart_daily").css("display", "block");
    $('#hidden_graph').val('Daily' + hidden_product_id);
    $('.active-remove').removeClass('active');
    $(this).addClass('active');
    App.dashboard();
});
if ($('#hidden_product_id').length > 0 && $('.singleProductGraph')[0]) {

    var hidden_product_value = $('#hidden_product_id').val();
    if ($('#single_product_chart').hasClass('single_product_chart') == true) {
        $(document).ready(function () {
//            getSingleProductGraph('day', hidden_product_value, 'yes');
        });

    }
    function getSingleProductGraph(id, hidden_product_value, onload) {

        $('.PerformanceTab').addClass('be-loading-active');

        $.ajax({
            type: 'Post',
            dataType: 'json',
            data: {id: id, product_id: hidden_product_value},
            url: '/site/single-product-graph',
            success: function (value) {
                $('.PerformanceTab').removeClass('be-loading-active');
                $('#chartInfoProduct').html('');
                $('#chartInfoProduct').html(value.showcolorhtml);
                if (onload == 'no') {
                    $('#single_product_chart').html('');
                }
                var resp = JSON.parse(value.data);
                var ykeyslabels = JSON.parse(value.ykeyslabels);
                var linecolors = JSON.parse(value.linecolors);
                var area = new Morris.Area({
                    element: 'single_product_chart',
                    resize: true,
                    data: resp,
                    xkey: 'period',
                    // xLabels: 'month',
                    parseTime: false,
                    //yLabelFormat:'Day 1',
                    ykeys: ykeyslabels,
                    labels: ykeyslabels,
                    lineColors: linecolors,
                    hideHover: 'auto'
                });



                var storesName = value.store_channel_names;
                var i = 0;
                var colorNo = 0;
                var t = 1;
                //foreach acc to coonected store names
                storesName.forEach(function (storeitem, storeindex) {
                    //every time to empty array for store sthe new values
                    var color2 = tinycolor(App.color.primary).lighten(colorNo).toString();
//                    console.log(color2);
                    if (i < storesName.length) {
                        i++;
                        $('[data-color="main-chart-color' + t + '"]').css({'background-color': color2});
                    }
                    t++;
                    colorNo = colorNo + 10;
                });
            }
        });
    }
    $('.singleProductGraph').on('click', function () {
        $this = $(this);
        id = $this.attr('data-id');
        $('.singleProductGraph').removeClass('active');
        $this.addClass('active');
        getSingleProductGraph(id, hidden_product_value, 'no');
    });
    $('.getLiGraph').on('click', function () {

        $('.custOMPerf').css('display', 'none');

        $this = $(this);
        getSingleProductGraph('month', hidden_product_value, 'no');
    });
}

/**********End For Single Product Perforamnce Main chart buttons click***************/

//Top tile widgets
var App = (function () {
    'use strict';
    App.dashboard = function () {
        var hidden_graph_value = $('#hidden_graph').val();
        if ($('#hidden_product_id').length > 0) {

            var hidden_product_value = $('#hidden_product_id').val();
        }
        //Counter
        function counter() {

            $('[data-toggle="counter"]').each(function (i, e) {
                var _el = $(this);
                var prefix = '';
                var suffix = '';
                var start = 0;
                var end = 0;
                var decimals = 0;
                var duration = 2.5;
                if (_el.data('prefix')) {
                    prefix = _el.data('prefix');
                }

                if (_el.data('suffix')) {
                    suffix = _el.data('suffix');
                }

                if (_el.data('start')) {
                    start = _el.data('start');
                }

                if (_el.data('end')) {
                    end = _el.data('end');
                }

                if (_el.data('decimals')) {
                    decimals = _el.data('decimals');
                }

                if (_el.data('duration')) {
                    duration = _el.data('duration');
                }

                var count = new CountUp(_el.get(0), start, end, decimals, duration, {
                    suffix: suffix,
                    prefix: prefix,
                });
                count.start();
            });
        }

        //Show loading class toggle
        function toggleLoader() {
            $('.toggle-loading').on('click', function () {
                var parent = $(this).parents('.widget, .panel');
                if (parent.length) {
                    parent.addClass('be-loading-active');
                    setTimeout(function () {
                        parent.removeClass('be-loading-active');
                    }, 3000);
                }
            });
        }

        //Top tile widgets
        function sparklines() {
            var color1 = App.color.primary;
            var color2 = App.color.warning;
            var color3 = App.color.success;
            var color4 = App.color.danger;
            // For NEw Orders and sales
            $('.orderDaSH').addClass('be-loading-active');
            $('.ProductsDaSH').addClass('be-loading-active');
            $('.avgOrderDaSH').addClass('be-loading-active');






            /*For Customer View File Statistics*/
            /*For NEw Orders and sales*/
            var controller_action = document.getElementById('current_controller_action').value;
            if (controller_action == 'people/view') {

                var customer_view_id = document.getElementById('customer_view_id').value;
                $.ajax({
                    url: "/customer-graph-orders",
                    datatype: 'json',
                    type: 'post',
                    data: {customer_view_id: customer_view_id},
                    success: function (value) {
                        var orders_count = '';
                        var JSonData = JSON.parse(value);
                        var NewOrders = JSonData['data'];
                        //$('#spark1').sparkline([0, 5, 3, 7, 5, 10, 3, 6, 5, 10], {
                        // For NEW ORDERS
                        $('#customer_order_spark1').sparkline(NewOrders, {
                            width: '85',
                            height: '35',
                            lineColor: color1,
                            highlightSpotColor: color1,
                            highlightLineColor: color1,
                            fillColor: false,
                            spotColor: false,
                            minSpotColor: false,
                            maxSpotColor: false,
                            lineWidth: 1.15
                        });
                    }
                });
                /* For Returns */
                $.ajax({
                    url: "/customer-graph-returns",
                    type: 'post',
                    data: {customer_view_id: customer_view_id},
                    datatype: 'json',
                    success: function (value) {
                        var orders_count = '';
                        var JSonData = JSON.parse(value);
                        var NewOrders = JSonData['data'];
                        $("#customer_return_spark2").sparkline(NewOrders, {
                            type: 'bar',
                            width: '85',
                            height: '35',
                            barWidth: 3,
                            barSpacing: 3,
                            chartRangeMin: 0,
                            barColor: color3
                        });
                    }
                });
                /* For item Purchased */
                $.ajax({
                    url: "/customer-graph-item-purchase",
                    datatype: 'json',
                    type: 'post',
                    data: {customer_view_id: customer_view_id},
                    success: function (value) {
                        var orders_count = '';
                        var JSonData = JSON.parse(value);
                        var NewOrders = JSonData['data'];
                        $("#customer_item_purchase_spark3").sparkline(NewOrders, {
                            type: 'bar',
                            width: '85',
                            height: '35',
                            barWidth: 3,
                            barSpacing: 3,
                            chartRangeMin: 0,
                            barColor: color2
                        });
                    }
                });
                /*END Customer View File Statistics*/
            }
        }

        //Main chart
        function mainChart() {

            $('.dashBoardChaRT').addClass('be-loading-active');

        }


        //Top sales chart
        function topSales() {

            var data = [
                {label: "Services", data: 33},
                {label: "Standard Plans", data: 33},
                {label: "Services", data: 33}
            ];
            var color1 = App.color.success;
            var color2 = App.color.warning;
            var color3 = App.color.primary;
            $.plot('#top-sales', data, {
                series: {
                    pie: {
                        radius: 0.75,
                        innerRadius: 0.58,
                        show: true,
                        highlight: {
                            opacity: 0.1
                        },
                        label: {
                            show: false
                        }
                    }
                },
                grid: {
                    hoverable: true,
                },
                legend: {
                    show: false
                },
                colors: [color1, color2, color3]
            });
            //Chart legend color setter
            $('[data-color="top-sales-color1"]').css({'background-color': color1});
            $('[data-color="top-sales-color2"]').css({'background-color': color2});
            $('[data-color="top-sales-color3"]').css({'background-color': color3});
        }

        //Calendar widget
        function calendar() {
            var widget = $("#calendar-widget");
            var now = new Date();
            var year = now.getFullYear();
            var month = now.getMonth();
            var events = [year + '-' + (month + 1) + '-16', year + '-' + (month + 1) + '-20'];
            function checkRows(datepicker) {
                var dp = datepicker.dpDiv;
                var rows = $("tbody tr", dp).length;
                if (rows == 6) {
                    dp.addClass('ui-datepicker-6rows');
                } else {
                    dp.removeClass('ui-datepicker-6rows');
                }
            }

            //Extend default datepicker to support afterShow event
            $.extend($.datepicker, {
                _updateDatepicker_original: $.datepicker._updateDatepicker,
                _updateDatepicker: function (inst) {
                    this._updateDatepicker_original(inst);
                    var afterShow = this._get(inst, 'afterShow');
                    if (afterShow) {
                        afterShow.apply(inst, [inst]);
                    }
                }
            });
            if (typeof jQuery.ui != 'undefined') {
                widget.datepicker({
                    showOtherMonths: true,
                    selectOtherMonths: true,
                    beforeShowDay: function (date) {
                        var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
                        if ($.inArray(y + '-' + (m + 1) + '-' + d, events) != -1) {
                            return [true, 'has-events', 'This day has events!'];
                        } else {
                            return [true, "", ""];
                        }
                    },
                    afterShow: function (o) {
                        //If datepicker has 6 rows add a class to the widget
                        checkRows(o);
                    }
                });
            }
        }

        //Map widget
        function map() {

            var color1 = tinycolor(App.color.primary).lighten(15).toHexString();
            var color2 = tinycolor(App.color.primary).lighten(8).toHexString();
            var color3 = tinycolor(App.color.primary).toHexString();
            //Highlight data
            var data = {
                "ru": "14",
                "us": "14",
                "ca": "10",
                "br": "10",
                "au": "11",
                "uk": "3",
                "cn": "12"
            };
            $('#map-widget').vectorMap({
                map: 'world_en',
                backgroundColor: null,
                color: color1,
                hoverOpacity: 0.7,
                selectedColor: color2,
                enableZoom: true,
                showTooltip: true,
                values: data,
                scaleColors: [color1, color2],
                normalizeFunction: 'polynomial'
            });
        }

        //CounterUp Init
        counter();
        //Loader show
        toggleLoader();
        //Row 1
        sparklines();
        //Row 2
        mainChart();
        //Row 4
        topSales();
        calendar();
        //Row 5
        map();
    };
    return App;
})(App || {});
