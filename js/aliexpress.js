
$('.aliexpress_auth_connection').click(function (e) {
    $(this).css('pointer-events', 'none');
    e.preventDefault();
    var aliexpress_api_key = $('#aliexpress_api_key input').val();
    var aliexpress_tracking_id = $('#aliexpress_tracking_id input').val();
    var aliexpress_digital_signature = $("#aliexpress_digital_signature input").val();

    var user_id = $('#user_id').val();
    //Required Fields Validation
    var isValid = true;

    if (aliexpress_api_key == '') {
        $('#aliexpress_api_key').addClass('has-error');
        $(this).css('pointer-events', 'auto');
        isValid = false;
    } else {
        if ($('#aliexpress_api_key').hasClass('has-error')) {
            $('#aliexpress_api_key').removeClass('has-error');
        }
    }
    if (aliexpress_tracking_id == '') {
        $('#aliexpress_tracking_id').addClass('has-error');
        isValid = false;
    } else {
        if ($('#aliexpress_tracking_id').hasClass('has-error')) {
            $('#aliexpress_tracking_id').removeClass('has-error');
        }
    }
    if (aliexpress_digital_signature == '') {
        $('#aliexpress_digital_signature').addClass('has-error');
        isValid = false;
    } else {
        if ($('#aliexpress_digital_signature').hasClass('has-error')) {
            $('#aliexpress_digital_signature').removeClass('has-error');
        }
    }

    if (isValid) {
        var currentURL = $("#hidURL").val();
        var subdomain = currentURL.split('.');

        $('.aliexpress_auth_connection').css('pointer-events', 'auto');
        $('.be-wrapper').addClass('be-loading-active');
        $.ajax({
            type: 'post',
            url: 'https://' + subdomain[0] + '.s86.co/aliexpress/auth-aliexpress',
            data: {
                aliexpress_api_key: aliexpress_api_key,
                aliexpress_tracking_id: aliexpress_tracking_id,
                aliexpress_digital_signature: aliexpress_digital_signature,
                redirect_url: 'https://' + subdomain[0] + '.s86.co/aliexpress/get-authorizecode',
                user_id: user_id,
            },
        }).done(function (data) {

            var authorizeURL =  "http://gw.api.alibaba.com/auth/authorize.htm?client_id=" + aliexpress_api_key + "&site=aliexpress&redirect_uri=https://" + subdomain[0] + ".s86.co/aliexpress/get-authorizecode&_aop_signature=" + data;
            window.location.href = authorizeURL;

            return;

        });
        // e.preventDefault();
    }
});

$('.aliexpress_error_modal_close').click(function () {
    $('.aliexpress_ajax_request_error').css({
        'display': 'none',
        'background': 'rgba(0,0,0,0.6)',
    });
});

$('.aliexpress_modal_close').click(function () {
    $('#aliexpress_ajax_request').css({
        'display': 'none',
        'background': 'rgba(0,0,0,0.6)',
    });
});

$(document).ready(function(){
    var user_id = $('#user_id').val();
    if($("#hidParam").val() == "aliexpress_success") {
       $('.be-wrapper').addClass('be-loading-active');
        var currentURL = $("#hidURL").val();
        var subdomain = currentURL.split('.');

        $.ajax({
            type: 'get',
            url: 'https://' + subdomain[0] + '.s86.co/aliexpress/aliexpress-importing',
            data: {
                user_id: user_id,
            },
        }).done(function (data) {
            $("#aliexpress_ajax_msg").html("Your AliExpress shop has been connected successfully. Importing is started. Once importing is done you will get a nofiy message.");
            $("#aliexpress_ajax_header_msg").html('Success!');
            $('#aliexpress-authorize-form').hide();
            $('#aliexpress_ajax_request').css({
                'display': 'block',
                'background': 'rgba(0,0,0,0.6)',
            });
            $('.be-wrapper').removeClass('be-loading-active');
            //window.location.reload();
        });

    }else if($("#hidParam").val() == "") {

    }else {
       $("#aliexpress_ajax_msg_eror").html($("#hidParam").val());
       $("#aliexpress_ajax_header_error_msg").html('Error!');
       $('.aliexpress_ajax_request_error').css({
           'display': 'block',
           'background': 'rgba(0,0,0,0.6)',
       });

   }

});
