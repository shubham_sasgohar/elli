var tmallURL = 'https://oauth.tbsandbox.com';

$('.tmall_auth_connection').click(function (e) {
    $(this).css('pointer-events', 'none');
    e.preventDefault();
    var tmall_app_key = $('#tmall_app_key input').val();
    var tmall_app_secret = $('#tmall_app_secret input').val();
    var user_id = $('#user_id').val();
    //Required Fields Validation
    var isValid = true;

    if (tmall_app_key == '') {
        $('#tmall_app_key').addClass('has-error');
        $(this).css('pointer-events', 'auto');
        isValid = false;
    } else {
        if ($('#tmall_app_key').hasClass('has-error')) {
            $('#tmall_app_key').removeClass('has-error');
        }
    }
    if (tmall_app_secret == '') {
        $('#tmall_app_secret').addClass('has-error');
        isValid = false;
    } else {
        if ($('#tmall_app_secret').hasClass('has-error')) {
            $('#tmall_app_secret').removeClass('has-error');
        }
    }

    if (isValid) {
        var currentURL = $("#hidURL").val();
        var subdomain = currentURL.split('.');

        $('.tmall_auth_connection').css('pointer-events', 'auto');
        $('.be-wrapper').addClass('be-loading-active');
        $.ajax({
            type: 'post',
            url: 'https://' + subdomain[0] + '.' + subdomain[1] + '.' + subdomain[2] + '/tmall/auth-tmall',
            data: {
                tmall_app_key: tmall_app_key,
                tmall_app_secret: tmall_app_secret,
                user_id: user_id,
            },
        }).done(function (data) {
            var authorizeURL = tmallURL + "/authorize?response_type=token&client_id=" + tmall_app_key + "&redirect_uri=https://" + subdomain[0] + "." + subdomain[1] + "." + subdomain[2] + "/tmall/get-authorizecode";
            window.location.href = authorizeURL;

            return;

        });
        // e.preventDefault();
    }
});

$('.tmall_error_modal_close').click(function () {
    $('.tmall_ajax_request_error').css({
        'display': 'none',
        'background': 'rgba(0,0,0,0.6)',
    });
});

$('.tmall_modal_close').click(function () {
    $('#tmall_ajax_request').css({
        'display': 'none',
        'background': 'rgba(0,0,0,0.6)',
    });
});

$(document).ready(function(){
    var user_id = $('#user_id').val();

    if($("#hidParam").val() == "tmall_success") {
       $('.be-wrapper').addClass('be-loading-active');

        $("#tmall_ajax_msg").html("Your TMall shop has been connected successfully. Importing is started. Once importing is done you will get a nofiy message.");
        $("#tmall_ajax_header_msg").html('Success!');
        $('#tmall-authorize-form').hide();
        $('#tmall_ajax_request').css({
            'display': 'block',
            'background': 'rgba(0,0,0,0.6)',
        });
        var currentURL = $("#hidURL").val();
        var subdomain = currentURL.split('.');
        $.ajax({
            type: 'get',
            url: 'https://' + subdomain[0] + '.' + subdomain[1] + '.' + subdomain[2] + '/tmall/tmall-importing',
            data: {
                user_id: user_id,
            },
        }).done(function (data) {
            $('.be-wrapper').removeClass('be-loading-active');
            window.location.href = 'https://' + subdomain[0] + '.' + subdomain[1] + '.' + subdomain[2] + '/tmall/';
            // var res = JSON.parse(data);
            // if(res['msg'] != undefined) {
            //     $("#tmall_ajax_msg_eror").html('Importing Error Code : ' + res['code'] + ' : ' + res['msg'] + ' - ' + res['sub_code']);
            //     $("#tmall_ajax_header_error_msg").html('Error!');
            //     $('.tmall_ajax_request_error').css({
            //         'display': 'block',
            //         'background': 'rgba(0,0,0,0.6)',
            //     });
            // }else{
            //     $("#tmall_ajax_msg").html(data);
            //     $("#tmall_ajax_header_msg").html('Success!');
            //     $('#tmall_ajax_request').css({
            //         'display': 'block',
            //         'background': 'rgba(0,0,0,0.6)',
            //     });
            // }
            //window.location.reload();
        }).failure(function(){
            $("#tmall_ajax_msg_eror").html("Request error. Please check your network status.");
            $("#tmall_ajax_header_error_msg").html('Error!');
            $('#tmall-authorize-form').hide();
            $('#tmall_ajax_request').css({
                'display': 'block',
                'background': 'rgba(0,0,0,0.6)',
            });
        });

    }else if($("#hidParam").val() == "") {
        // var currentURL = $("#hidURL").val();
        // var subdomain = currentURL.split('.');
        // $.ajax({
        //     type: 'get',
        //     url: 'https://' + subdomain[0] + '.s86.co/tmall/tmall-importing',
        //     data: {
        //         user_id: user_id,
        //     },
        // }).done(function (data) {
        //     $("#tmall_ajax_msg").html(data);
        //     $("#tmall_ajax_header_msg").html('Success!');
        //     $('#tmall_ajax_request').css({
        //         'display': 'block',
        //         'background': 'rgba(0,0,0,0.6)',
        //     });
        //     //window.location.reload();
        // });

    }else {
       $("#tmall_ajax_msg_eror").html($("#hidParam").val());
       $("#tmall_ajax_header_error_msg").html('Error!');
       $('.tmall_ajax_request_error').css({
           'display': 'block',
           'background': 'rgba(0,0,0,0.6)',
       });

   }

});
