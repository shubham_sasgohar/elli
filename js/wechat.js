$(".wechat_sbmt1_already").click(function () {

    storename = $.trim($("#wechat_storename").val());
    password = $.trim($("#wechat_password").val());
    type = $.trim($("#wechat_type").val());
    if (!storename || !password) {
        $("#wechat_storename").addClass("inpt_required");
        $("#wechat_password").addClass("inpt_required");
        return false;
    } else {
        $("#wechat_storename").removeClass("inpt_required");
        $("#wechat_password").removeClass("inpt_required");
    }

    $.ajax({
        type: 'post',
        url: '/wechat/wechatconnect',
        data: {
            user: storename,
            password: password,
            type: type,
            already: 'yes',
            email: '',
        },
        beforeSend: function (xhr) {
            $(".be-wrapper").addClass("be-loading-active");
        },
        success: function (value) {
            $(".be-wrapper").removeClass("be-loading-active");
            if (value == 'error') {
                $('#walkthechat_request_error').modal('show');
            }
            //else if (value) {
            else {
                $('#walkthechat_request').modal('show');
                $(".afterwechatrequestmsg_already").show();
                $(".wechatrequestform").hide();
                console.log("test");
                // $('#walkthechat_request').modal('show');
                // $(".afterwechatrequestmsg").show();
                // $(".wechatrequestform").hide();
            }

//           location.reload();
        },
    });
})

$(".wechat_sbmt1_quickconnect").click(function () {

    var appid = $.trim($("#wechat_storename").val());
    var appsecret = $.trim($("#wechat_password").val());
    var type = $.trim($("#wechat_type").val());
    if (!appid || !appsecret) {
        $("#wechat_storename").addClass("inpt_required");
        $("#wechat_password").addClass("inpt_required");
        return false;
    } else {
        $("#wechat_storename").removeClass("inpt_required");
        $("#wechat_password").removeClass("inpt_required");
    }

    $.ajax({
        type: 'post',
        url: '/wechat/wechatquickconnect',
        data: {
            user: appid,
            password: appsecret,
            type: type,
            quickconnect: 'yes',
            email: '',
        },
        beforeSend: function (xhr) {
            $(".be-wrapper").addClass("be-loading-active");
        },
        success: function (value) {
            $(".be-wrapper").removeClass("be-loading-active");

            if (value == 'error') {
                $('#walkthechat_request_error').modal('show');
            }
            //else if (value) {
            else {
                $('#walkthechat_request').modal('show');
                $(".afterwechatrequestmsg_quickconnect").show();
                $(".wechatrequestform").hide();
                //console.log("test");
                // $('#walkthechat_request').modal('show');
                // $(".afterwechatrequestmsg").show();
                // $(".wechatrequestform").hide();
            }

//           location.reload();
        },
    });
})

$(".wechat_inti").click(function () {
    var chk = true;
    $(".initi").each(function () {
        var val = $.trim($(this).val());
        if (!val) {
            $(this).addClass("inpt_required");
            chk = false;
        } else {
            $(this).removeClass("inpt_required");
        }
    })

    if (!chk) {
        return false;
    }

    user = $.trim($("#wechat_username").val());
    password = $.trim($("#wechat_password").val());
    email = $.trim($("#elliot_useremail").val());
    userid = $.trim($("#wechat_userid").val());
    type = $.trim($(".wechat_connect_container #acctype").val());

    $.ajax({
        type: 'post',
        url: 'wechatconnect',
        data: {
            user: user,
            password: password,
            email: email,
            type: type,
        },
        beforeSend: function (xhr) {
            $(".be-wrapper").addClass("be-loading-active");
//                $('#walkthechat_request').modal('show');
//                $(".wechat_adminafterform").show();
//                $(".wechat_adminform").hide();
        },
        success: function (value) {
            $(".be-wrapper").removeClass("be-loading-active");
            if (value == "error") {
                $('#walkthechat_request_error').modal('show');
            } else if (value) {
                $('#walkthechat_request').modal('show');
                $(".wechat_adminafterform").show();
                $(".wechat_adminform").hide();

                $.ajax({
                    type: 'get',
                    url: BASE_URL + 'channels/wechatimport/' + userid,
                    data: {
//                            user: user,
//                            password: password,
//                            email: email,
                        type: type,
                    },
                    beforeSend: function (xhr) {
                        //$(".be-wrapper").addClass("be-loading-active");
                        //                $('#walkthechat_request').modal('show');
                        //                $(".wechat_adminafterform").show();
                        //                $(".wechat_adminform").hide();
                    },
                    success: function (value) {
//                            alert("success");
//                            console.log("success");
                    },
                });


            }
            //location.reload();
//            if(value){
//                 $('#walkthechat_request').modal('show');
//            }

//           location.reload();
        },
        complete: function (event, xhr, options) {

            console.log(event);
            console.log(xhr);
            console.log(options);
//                window.location = 'https://' + value + '.s86.co/stores';
//                $('.be-wrapper').removeClass('be-loading-active');
        },
    });
})
