<?php
$order_data_json = array (
  'Order' => 
  array (
    'CneeContactName' => 'Tom Lee',
    'CneeCompany' => 'Sf Express',
    'CneeAddress' => 'ChangAnJie No. 1',
    'CneeCity' => 'XiCheng',
    'CneeProvince' => 'Beijing',
    'CneeCountry' => 'CN',
    'CneePostCode' => '100010',
    'CneePhone' => '13811026548',
    'ReferenceNo1' => 'E434324378248328D90',
    'ReferenceNo2' => 'IZIEF4342324324D23434',
    'ExpressType' => '101',
    'ParcelQuantity' => '1',
    'PayMethod' => '3',
    'TaxPayType' => '2',
    'Currency' => 'USD',
    'Items' => 
    array (
     
      array (
        'Name' => 'Testing Testing',
        'Count' => 2,
        'Unit' => 'pcs',
        'Amount' => '14.29',
        'SourceArea' => 'US',
        'Weight' => '2.0',
      ),
    ),
  ),
  'Gateway' => 'JFK',
  'NetworkCredential' => 
  array (
    'UserName' => '90000001',
    'Password' => 'b9rVZekQqY2bv6ds',
  ),
);
                                                                                    
$data_string = json_encode($order_data_json); 
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_SSL_VERIFYPEER => false,
  CURLOPT_URL => "https://sit.api.sf-express-us.com/api/orderservice/submitorder",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_SSL_VERIFYHOST => FALSE,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => $data_string,
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "charset: utf-8",
    "content-type: application/json"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo '<pre>'; print_r(json_decode($response)); echo '</pre>';
}
