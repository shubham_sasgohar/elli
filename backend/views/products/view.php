<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
use backend\models\ProductImages;
use backend\models\MerchantProducts;
use backend\models\StoresConnection;
use backend\models\ChannelConnection;
use backend\models\Stores;
use backend\models\Channels;
use backend\models\ProductCategories;
use backend\models\Categories;
use backend\models\ProductVariation;
use backend\models\ProductAbbrivation;
use backend\models\Variations;
use backend\models\VariationsItemList;
use backend\models\VariationsSet;
use backend\models\Attributes;
use backend\models\ProductChannel;
use backend\models\Channelsetting;

/* @var $this yii\web\View */
/* @var $product_model app\product_models\Products */
$base_url = Yii::getAlias('@baseurl');
$smartling_files_dir = Yii::getAlias('@smartling_files');


$this->title = 'Product Manager';
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


//echo $l;
/* Product Fields Values */
$user_id = Yii::$app->user->identity->id;
$domain_name = Yii::$app->user->identity->domain_name;
$product_id = isset($product_model->id) ? $product_model->id : "";
$product_des = isset($product_model->description) ? $product_model->description : "";
//General Tab
$product_name = isset($product_model->product_name) ? ucfirst($product_model->product_name) : "";
$SKU = isset($product_model->SKU) ? $product_model->SKU : "";
$UPC = isset($product_model->UPC) ? $product_model->UPC : "";
$EAN = isset($product_model->EAN) ? $product_model->EAN : "";
$JAN = isset($product_model->Jan) ? $product_model->Jan : "";
$ISBN = isset($product_model->ISBN) ? $product_model->ISBN : "";
$MPN = isset($product_model->MPN) ? $product_model->MPN : "";
$adult = isset($product_model->adult) ? $product_model->adult : "no";
$age_group = isset($product_model->age_group) ? $product_model->age_group : "";
$availability = isset($product_model->availability) ? $product_model->availability : "Out of Stock";
$brand = isset($product_model->brand) ? $product_model->brand : "";
$condition = isset($product_model->condition) ? $product_model->condition : "New";
$gender = isset($product_model->gender) ? $product_model->gender : "Unisex";
//echo var_dump($product_model->weight); die;
$weight = isset($product_model->weight) && $product_model->weight ? number_format($product_model->weight, 2) : 0;



//Variations Tab
$var_set_id = isset($product_model->variations_set_id) ? $product_model->variations_set_id : "";
$op_set_object = VariationsSet::find()->where(['variations_set_id' => $var_set_id, 'elliot_user_id' => Yii::$app->user->identity->id])->one();
$op_set = '';
$op_ids_arr = array();
if (!empty($op_set_object)):
    $op_set = $op_set_object->variations_set_name;
    $op_ids_arr = explode(",", $op_set_object->variations_set_options_ids);
endif;

//Attribution Tab
$occasion = isset($product_model->occasion) ? $product_model->occasion : '';
$weather = isset($product_model->weather) ? $product_model->weather : '';

//Categories Tab
//Channel Manager Tab
//Inventory Management Tab
$stk_qty = isset($product_model->stock_quantity) ? $product_model->stock_quantity : 0;
$stk_lvl = isset($product_model->stock_level) ? $product_model->stock_level : 'Out of Stock';
$stk_status = isset($product_model->stock_status) ? $product_model->stock_status : 'Visible';
$low_stk_ntf = isset($product_model->low_stock_notification) ? $product_model->low_stock_notification : '';
//Media Tab
//Performance Tab
//Pricing Tab

$user = Yii::$app->user->identity;
$conversion_rate = 1;
if (isset($user->currency)) {
    
    $conversion_rate = Stores::getDbConversionRate($user->currency);
    
//    $username = Yii::$app->params['xe_account_id'];
//    $password = Yii::$app->params['xe_account_api_key'];
//    $URL = Yii::$app->params['xe_base_url'] . 'convert_from.json/?from=USD&to=' . $user->currency . '&amount=1';
//
//    $ch = curl_init();
//    curl_setopt($ch, CURLOPT_URL, $URL);
//    curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
//    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
//    curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
//    $result = curl_exec($ch);
//    $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
//    curl_close($ch);
////echo'<pre>';
//    $result = json_decode($result, true);
//    if (isset($result) and ! empty($result) and isset($result['to']) and isset($result['to'][0]) and isset($result['to'][0]['quotecurrency'])) {
//        $conversion_rate = $result['to'][0]['mid'];
//        $conversion_rate = number_format((float) $conversion_rate, 2, '.', '');
//    }
}



$selected_currency = \backend\models\CurrencySymbols::find()->where(['name' => strtolower($user->currency)])->select(['id', 'symbol'])->asArray()->one();
if (isset($selected_currency) and ! empty($selected_currency)) {
    $currency_symbol = $selected_currency['symbol'];
}
$price = isset($product_model->price) ? number_format((float) $product_model->price * $conversion_rate, 2) : 0;

//echo var_dump($product_model->sales_price); die;
$sale_price = isset($product_model->sales_price) && $product_model->sales_price ? number_format((float) $product_model->sales_price * $conversion_rate, 2) : 0;
$schedule_date1 = date('Y-m-d', time());
$schedule_date2 = isset($product_model->schedules_sales_date) ? $product_model->schedules_sales_date : '';
//Translation Tab
//product default image
$Product_images = ProductImages::find()->Where(['product_ID' => $product_id])->orderBy(['priority' => SORT_ASC])->all();
$Product_images_count = count($Product_images);

$users_Id = Yii::$app->user->identity->id;
$store_connection = StoresConnection::find()->where(['user_id' => $users_Id])->all();
$channel_connection = ChannelConnection::find()->where(['user_id' => $users_Id])->all();
$connections = array();

foreach ($store_connection as $con) {
    $store = Stores::find()->select(['store_name', 'store_image'])->where(['store_id' => $con->store_id])->one();
    $connections[] = $store->store_name;
}

foreach ($channel_connection as $con1) {
    $channel = Channels::find()->select(['channel_name'])->where(['channel_ID' => $con1->channel_id])->one();
    $connections[] = $channel->channel_name;
}

//User For Categoreis
$product_cat = ProductCategories::find()->Where(['product_Id' => $product_id])->all();

if (!empty($product_cat)) :
    $val = '';
    $name = '';

    foreach ($product_cat as $pcat_data) :
        $category_data = Categories::find()->Where(['category_ID' => $pcat_data->category_ID])->one();

        $cat_name = !empty($category_data) ? $category_data->category_name : '';
        $cat_id = !empty($category_data) ? $category_data->category_ID : '';
        //$arr_data = [];
        $arr_data[] = array('value' => $cat_id, 'name' => $cat_name);
        $val .= $cat_id . ",";
        $name .= $cat_name . ",";

    endforeach;
    $cat_data_source = rtrim($val, ',');
    $cat_name_source1 = trim($name, ",");
    $cat_name_source = str_replace(",", "<br>", $cat_name_source1);
endif;

$users_Id = Yii::$app->user->identity->id;
$store_connection = StoresConnection::find()->where(['user_id' => $users_Id])->all();
$channel_connection = ChannelConnection::find()->where(['user_id' => $users_Id])->all();
$connections = array();
$val_store = '';
foreach ($store_connection as $con) {
    $store = Stores::find()->select(['store_name', 'store_image'])->where(['store_id' => $con->store_id])->one();
//        $arr[] = array("value" => $store->store_name, "text" => $store->store_name);
    $val_store .= $store->store_name . ",";
}
if (!empty($val_store)):
    $val_store_data_source = rtrim($store->store_name, ',');
endif;

foreach ($channel_connection as $con1) {
    $channel = Channels::find()->select(['channel_name', 'channel_image', 'parent_name'])->where(['channel_id' => $con1->channel_id])->one();
    $p_name = explode("_", $channel->parent_name);
    $val_store .= $p_name[1] . ",";
}
if (!empty($val_store)):
    $val_store_data_source = rtrim($val_store, ',');
endif;

/* For Display Coonected Channel in channel manager */
$ProductChannel = ProductChannel::find()->where(['product_id' => $product_model->id, 'status' => 'yes'])->all();


$names = array();
$name_with_country = array();
$check_list_value = array();
$smartling_stores_name = array();
$store_id = $channel_ids = '';
$smartling_enable='no';
$sm_channel_country=$translation_status='';
foreach ($ProductChannel as $pc) {
    if (!empty($pc->store_id)) {
        $store_connection_data = StoresConnection::find()->Where(['stores_connection_id' => $pc->store_id])->with('storesDetails')->one();
        
        $smartling_store_connection_data = StoresConnection::find()->Where(['stores_connection_id' => $pc->store_id,'smartling_status'=>'active'])->with('storesDetails')->one();
        if(!empty($smartling_store_connection_data)){
            $sm_channel_accquired=$smartling_store_connection_data->storesDetails->channel_accquired;
            $sm_channel_country=$smartling_store_connection_data->storesDetails->country;
	    
	    $product_abbrivation_data=ProductAbbrivation::find()->Where(['product_id'=>$product_id,'mul_store_id'=>$pc->store_id,'channel_accquired'=>$sm_channel_accquired])->one();
	    $translation_status=$product_abbrivation_data->translation_status;
	  
	    
           // $smartling_stores_name[]=$sm_channel_accquired.' '.$sm_channel_country;
            $smartling_stores_name[]=['acc_country'=>$sm_channel_country,'connection_id'=>$pc->store_id,'channel_acc'=>$sm_channel_accquired,'translation_status'=>$translation_status];
            $smartling_enable='yes';
        }
	
        
        $store_details_country = $store_connection_data->storesDetails->country;
        $store_connection_id = $store_connection_data->stores_connection_id;
        if (!empty($store_details_country)) {
            $store_country = $store_connection_data->storesDetails->country;
        } else {
            $store_country = '';
        }
        $get_store_id = $store_connection_data->store_id;
        $store_names = Stores::find()->Where(['store_id' => $get_store_id])->one();
        $names[] = $store_names->store_name . $pc->store_id;
        $name_with_country[] = trim($store_names->store_name . ' ' . $store_country);
        $check_list_value[] = $store_connection_id;
    }
    if (!empty($pc->channel_id)) {
        $smartling_channel_connection_data = ChannelConnection::find()->Where(['channel_id' => $pc->channel_id,'smartling_status'=>'active'])->with('storesDetails')->one();
        if(!empty($smartling_channel_connection_data)){
	 
            $sm_channel_accquired=$smartling_channel_connection_data->storesDetails->channel_accquired;
	    $product_abbrivation_data=ProductAbbrivation::find()->Where(['channel_accquired'=>$sm_channel_accquired])->one();
	    $translation_status=$product_abbrivation_data->translation_status;
            //$sm_channel_country=$smartling_channel_connection_data->storesDetails->country;
            //$smartling_stores_name[]=$sm_channel_accquired;
	    $smartling_stores_name[]=['acc_country'=>'','connection_id'=>$pc->channel_id,'channel_acc'=>$sm_channel_accquired,'translation_status'=>$translation_status];

            $smartling_enable='yes';
        }
        
        $channels_name = channels::find()->where(['channel_id' => $pc->channel_id])->one();
        $channel_parent_name = $channels_name->parent_name;

        $channel_name_org = ucwords(str_replace("-", " ", substr($channel_parent_name, 8)));
        if ($channel_name_org == 'Lazada') {
            $channel_name_org = trim($channels_name->channel_name);
        }
        $names[] = trim($channel_name_org);
        $name_with_country[] = trim($channel_name_org);
        $check_list_value[] = trim($channel_name_org);
    }
}

//$value_to_display = isset($names) && $names ? implode(",", $names) : '';
$value_to_display = isset($name_with_country) && $names ? implode(",", $name_with_country) : '';
$value_check_list_display = isset($check_list_value) ? implode(",", $check_list_value) : '';

if (isset($_GET['a']) and ! empty($_GET['a'])) {
    echo "<pre>";
    print_r($names);
    print_r($value_to_display);
    die;
}
/* For Stock Status */
$stock_status_arr = ProductChannel::find()->where(['product_id' => $product_id])->all();
$visible_data_arr = [];
$not_visible_data_arr = [];
if (!empty($stock_status_arr)) {
    foreach ($stock_status_arr as $status_arr) {
        if ($status_arr->status == "yes") {
            $visible_data_arr[] = 'yes';
        } else {
            $not_visible_data_arr[] = 'no';
        }
    }
}
$stock_status_test = "Featured in " . count($visible_data_arr) . " Channels, Hidden in " . count($not_visible_data_arr) . " Channels";


/* For Stock Level */
$stock_levael_arr = ProductAbbrivation::find()->where(['product_id' => $product_id])->all();
$in_data_arr = [];
$out_data_arr = [];
if (!empty($stock_levael_arr)) {
    foreach ($stock_levael_arr as $stock_level_arr) {
        if ($stock_level_arr->stock_level == "In Stock") {
            $in_data_arr[] = 'In';
        } else {
            $out_data_arr[] = 'out';
        }
    }
}
$stock_level_test = "In Stock in " . count($in_data_arr) . " Channels, Out of Stock in " . count($out_data_arr) . " Channels";

/* END For Display Coonected Channel in channel manager */
$users_Id = Yii::$app->user->identity->id;
?>
<div class="row">
    <div class="col-md-12">
        <input type="hidden" id="pID_created" value="<?= $product_id; ?>"/>
        <div class="single_product_main_img">
            <?php
            if (!empty($Product_images)) {
                $default_product_image = ProductImages::find()->Where(['product_ID' => $product_id, 'default_image' => 'Yes'])->one();
                if (empty($default_product_image)):
                    $default_product_image = ProductImages::find()->Where(['product_ID' => $product_id, 'priority' => 1])->one();
                endif;
                $product_image_link = $default_product_image->link;
                ?>
                <div class="bs-grid-block product_image">
                    <div class="content">
                        <!--Starts Change cover image !-->
                        <div class="user-display-bg product-texthover p_default_img" style="background-image:url('<?= $product_image_link ?>');">
                            <!--<img src="<?= $product_image_link ?>"  alt="Profile Background" id="product_image_dropzone1"  class="custom_drozone_css dropzone">-->
                            <div class="product-overlay custom_drozone_css dropzone span-product-image-css" id="product_image_dropzone"><br />
                                <span class="panel-heading profile-panel-heading" ></span>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <div class="bs-grid-block product_image product-texthover-default">
                    <!--Starts Change cover image !-->
                    <div class="user-display-bg">
                        <div class="content dropzone" id="product_image_dropzone-default1"><span class="size span-add-product-text">Add Product Image</span></div>
                        <div class="product-default-overlay custom_drozone_css dropzone span-product-image-css" id="product_image_dropzone-default"><br />
                            <span class="panel-heading profile-panel-heading" ></span>
                        </div>
                    </div>
                </div>

            <?php } ?>
        </div>
        <div class="pdetail_head">
            <div class="page-head product_detail">
                <h2 class="page-head-title"><?= Html::encode($product_name) ?></h2>
                <ol class="breadcrumb page-head-nav">
                    <?php echo Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]); ?>
                </ol>
            </div>


            <div class="pdes">
                <div><?php //echo preg_replace("/<img[^>]+\>/i", "", $product_des);      ?></div>
            </div>
        </div>
        <div class="up_btn_div">
            <button class="btn btn-space btn-primary" onclick="updateProduct()">Publish</button>
        </div>
        <input type="hidden" id="product_id" value="<?php echo $product_model->id; ?>"/>
    </div>
</div>    

<div class="row">
    <!--Default Tabs-->
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="tab-container">
                <ul class="nav nav-tabs">
                    <li class="active customCC"><a href="#general" data-toggle="tab">General Information</a></li>
                    <li class="customCC"><a href="#variations" data-toggle="tab">Variations</a></li>
                    <li class="customCC"><a href="#attribution" data-toggle="tab">Attribution</a></li>
                    <li class="customCC"><a href="#categories" data-toggle="tab">Categories</a></li>
                    <li class="customCC"><a href="#ch_mngr" data-toggle="tab">Channel Manager</a></li>
                    <li class="customCC"><a href="#invt_mngr" data-toggle="tab">Inventory Management</a></li>
                    <li class="customCC"><a href="#media" data-toggle="tab">Media</a></li>
                    <li class="getLiGraph"><a href="#performance" data-toggle="tab">Performance</a></li>
                    <li class="customCC"><a href="#pricing" data-toggle="tab">Pricing</a></li>
                    <?php if($smartling_enable=='yes'){ ?>
                        <li><a href="#translation" data-toggle="tab">Translation</a></li> 
                    <?php } ?>
                </ul>
                <div class="tab-content product_details">
                    <div id="general" class="tab-pane active cont">
                        <div class="table-responsive">
                            <table id="general_tbl" style="clear: both" class="table table-striped table-borderless">
                                <tbody>
                                    <tr>
                                        <td width="35%">Name</td>
                                        <td width="65%"><a id="product_name" href="#" data-type="text" data-title="Please Enter value"><?php echo $product_name; ?></a></td>
                                    </tr>
                                    <tr>
                                        <td width="35%">SKU</td>
                                        <td width="65%"><a id="SKU" href="#" data-type="text" data-title="Please Enter value (must be unique)" ><?php echo $SKU; ?></a></td>
                                    </tr>
                                    <tr>
                                        <td width="35%">UPC</td>
                                        <td width="65%"><a id="UPC" href="#" data-type="text" data-title="Please Enter value (must be unique)" ><?php echo $UPC; ?></a></td>
                                    </tr>
                                    <tr>
                                        <td width="35%">EAN</td>
                                        <td width="65%"><a id="EAN" href="#" data-type="text" data-title="Please Enter value (must be unique)"><?php echo $EAN; ?></a></td>
                                    </tr>
                                    <tr>
                                        <td width="35%">JAN</td>
                                        <td width="65%"><a id="JAN" href="#" data-type="text" data-title="Please Enter value (must be unique)"><?php echo $JAN; ?></a></td>
                                    </tr>
                                    <tr>
                                        <td width="35%">ISBN</td>
                                        <td width="65%"><a id="ISBN" href="#" data-type="text" data-title="Please Enter value (must be unique)"><?php echo $ISBN; ?></a></td>
                                    </tr>
                                    <tr>
                                        <td width="35%">MPN</td>
                                        <td width="65%"><a id="MPN" href="#" data-type="text" data-title="Please Enter value (must be unique)"><?php echo $MPN; ?></a></td>
                                    </tr>
                                    <tr>
                                        <td>Adult</td>
                                        <td><a id="adult" data-title="Please Select" data-value="<?php echo $adult; ?>" data-pk="1" data-type="select" href="#"></a></td>
                                    </tr>
                                    <tr>
                                        <td>Age Group</td>
                                        <td><a id="age_group" data-title="Please Select" data-value="<?php echo $age_group; ?>" data-pk="1" data-type="select" href="#"></a></td>
                                    </tr>
                                    <tr>
                                        <td>Availability</td>
                                        <td><a id="availability" data-title="Please Select" data-value="<?php echo $availability; ?>" data-pk="1" data-type="select" href="#"></a></td>
                                    </tr>
                                    <tr>
                                        <td width="35%">Brand</td>
                                        <td width="65%"><a  id="brand" href="javascript:"><?php echo Yii::$app->user->identity->company_name; ?></a></td>
                                    </tr>
                                    <tr>
                                        <td>Condition</td>
                                        <td><a id="condition" data-title="Please Select" data-value="<?php echo $condition; ?>" data-pk="1" data-type="select" href="#"></a></td>
                                    </tr>
                                    <tr>
                                        <td>Gender</td>
                                        <td><a id="gender" data-title="Please Select" data-value="<?php echo $gender; ?>" data-pk="1" data-type="select" href="#"></a></td>
                                    </tr>
                                    <tr>
                                        <td width="35%">Weight (lbs)</td>
                                        <td width="65%"><a id="weight" href="#" data-type="text" data-title="Please Enter value"><?php echo $weight; ?></a></td>
                                    </tr>
                                    <tr>
                                        <td width="35%">Description</td>
                                        <td width="65%">
                                            <div id="product-update-description"><?= $product_des ?></div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div id="variations" class="tab-pane cont">
                        <div class="table-responsive">
                            <div id="variations" class="tab-pane cont">
                        <div class="table-responsive">

                            <!--table id="var_set_tbl" style="clear: both" class="table table-striped table-borderless">
                                <tbody>
                                    <tr>
                                        <th width="35%">Option Set</th>
                                        <th width="65%"></th>
                                    </tr>
                                    <tr>
                                        <td width="35%">Option Set Name</td>
                                        <td width="65%">
                                            <a id="op_set" data-title="Please Select" data-value="" data-pk="1" data-type="select" href="#" data-source="get-var-set"><?php //echo $op_set; ?></a>
                                        </td>   
                                    </tr>
                                </tbody>
                            </table-->
                            <?php //if (!empty($op_set)) { ?>
                                <!--table id="var_tbl" style="clear: both" class="table table-striped table-borderless">
                                    <tbody>
                                        <tr>
                                            <th width="35%">Variants</th>
                                            <th width="65%"></th>
                                        </tr>
                                <?php
                                // foreach ($op_ids_arr as $key => $val):
                                // $var_obj = Variations::find()->where(['variations_ID' => $val])->one();
                                ?>
                                              <tr>
                                                  <td width="35%">Variant <? //= $key + 1  ?></td>
                                                  <td width="65%">
                                <? //= $var_obj->variation_name ?>
                                                  </td>   
                                              </tr>
                                <?php //endforeach;    ?>

                                        </tbody>
                                    </table-->

                                <div class="col-sm-12 skus be-loading"> 
                                    <table id="skus_tbl" width="100%" class="table table-condensed table-hover table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th colspan="5" class="pskus_th">
                                                    Options & SKUs
                                                    <!--button class="btn btn-space btn-primary addsku-btn"-->
                                                    <!--i class="icon icon-left mdi mdi-plus"></i> 
                                                    Add SKU-->
                                                    <!--/button-->

                                                </th>
                                            </tr>
                                            <tr>
                                                <th>SKU</th>
                                                <th>Inventory</th>
                                                <th>Price</th>
                                                <th>Weight</th>
                                                <!--th>Actions</th-->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $product_variants_rows = ProductVariation::find()->select('store_variation_id')->where(['product_id' => $product_id, 'elliot_user_id' => Yii::$app->user->identity->id])->distinct()->orderBy(['store_variation_id' => SORT_DESC])->all();
                                            if(count($product_variants_rows) == 0){
                                                ?>
                                        <tr class="odd"><td valign="top" colspan="6" style="text-align:center;" class="dataTables_empty">No data available in table</td></tr>
                                        <!--<a href="/variations/create"><button class="btn btn-space btn-primary" >Add Variation</button></a>-->
                                           <?php } else {
                                               foreach ($product_variants_rows as $row):
                                                $product_var_items = ProductVariation::find()->where(['store_variation_id' => $row->store_variation_id, 'elliot_user_id' => Yii::$app->user->identity->id])->all();
                                                $product_var_sku = ProductVariation::find()->where(['item_name' => 'SKU', 'store_variation_id' => $row->store_variation_id, 'elliot_user_id' => Yii::$app->user->identity->id])->one();
                                                $product_var_inventory = ProductVariation::find()->where(['item_name' => 'Inventory', 'store_variation_id' => $row->store_variation_id, 'elliot_user_id' => Yii::$app->user->identity->id])->one();
                                                $product_var_price = ProductVariation::find()->where(['item_name' => 'price', 'store_variation_id' => $row->store_variation_id, 'elliot_user_id' => Yii::$app->user->identity->id])->one();
                                                $product_var_weight = ProductVariation::find()->where(['item_name' => 'weight', 'store_variation_id' => $row->store_variation_id, 'elliot_user_id' => Yii::$app->user->identity->id])->one();
                                                $product_var_options = ProductVariation::find()->where(['store_variation_id' => $row->store_variation_id, 'elliot_user_id' => Yii::$app->user->identity->id])->andWhere(['not', ['variation_id' => NULL]])->all();
                                                ?>
                                                <tr id="<?= $row->store_variation_id; ?>">
                                                    <!--SKU-->
                                                    <?php if (!empty($product_var_sku)){ ?>
                                                        <?php $cls = 'pvar' . $product_var_sku->item_name; ?>
                                                        <td>
                                                            <!--a class="<?= $cls; ?>" href="#" data-type="text" data-title="Please Enter value (must be unique)" ><? //= $product_var_sku->item_value;  ?></a-->
                                                            <div><strong>SKU:</strong> <span class="pvarSKU"><?= $product_var_sku->item_value; ?></span>
                                                                <?php foreach ($product_var_options as $p_var): ?>
                                                                    <div class="op_div">
                                                                        <!--span class="option_name"><? //= $p_var->item_name;  ?>:</span-->
                                                                        <strong class="option_name"><?= $p_var->item_name; ?>:</strong> 
                                                                        <span class="pvaritems"><?= $p_var->item_value; ?></span>
                                                                        <!--a class="pvaritems" href="javascript:" data-title="Please Select" data-type="select" data-value="" data-pk="1" class="editable editable-click" data-source="get-varitems?var_name=<? //= $p_var->item_name;  ?>"><? //= $p_var->item_value;  ?></a-->
                                                                    </div>
                                                                <?php endforeach; ?>
                                                        </td>
                                                    <?php } else {
                                                        echo '<td> - </td>';
                                                    } ?>
                                                    <!--Inventory-->
                                                    <?php if (!empty($product_var_inventory)){ ?>
                                                        <?php $cls = 'pvar' . $product_var_inventory->item_name; ?>
                                                        <td>
                                                            <a class="<?= $cls; ?>" href="#" data-type="text" data-title="Please Enter value (must be unique)" ><?= $product_var_inventory->item_value; ?></a>
                                                        </td>
                                                    <?php } else {
                                                        echo '<td> - </td>';
                                                    } ?>

                                                    <!--Price-->
                                                    <?php if (!empty($product_var_price)) { ?>
                                                        <?php $cls = 'pvar' . $product_var_price->item_name; ?>
                                                        <td>
                                                            <a class="<?= $cls; ?>" href="#" data-type="text" data-title="Please Enter value (must be unique)" ><?= number_format(($product_var_price->item_value) * $conversion_rate, 2); ?></a>
                                                        </td>
                                                    <?php } else {
                                                        echo '<td> - </td>';
                                                    } ?>
                                                    <!--Weight-->
                                                    <?php if (!empty($product_var_weight)){ ?>
                                                        <?php $cls = 'pvar' . $product_var_weight->item_name; ?>
                                                        <td>
                                                            <a class="<?= $cls; ?>" href="#" data-type="text" data-title="Please Enter value (must be unique)" ><?= number_format($product_var_weight->item_value, 2); ?></a>
                                                        </td>
                                                    <?php } else { echo '<td> - </td>'; } ?>
                            <!--td><a class="pvarActions_update icon" href="#" title="Remove Variant"><i class="mdi mdi-delete"></i></a></td-->
                                                </tr>
                                            <?php endforeach; 
                                               
                                           }
                                         ?>   
                                        </tbody>
                                    </table>    

                                    <div class="save_var_btn">
                                        <button class="btn btn-space btn-primary save_pvar_update">
                                            Save 
                                        </button>
                                    </div>
                                    <div class="be-spinner">
                                        <svg width="40px" height="40px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                                        <circle fill="none" stroke-width="4" stroke-linecap="round" cx="33" cy="33" r="30" class="circle"/>
                                        </svg>
                                    </div>
                                </div>
                            <?php //} else { ?>
                                <!--<a href="/variations/create"><button class="btn btn-space btn-primary" >Add Variation</button></a>-->
                            <?php //} ?>
                        </div>
                    </div>
                        </div>
                    </div>

                    <div id="attribution" class="tab-pane cont">
                        <div class="table-responsive">
                            <input type="hidden" id="gcatname" value="" />
                            <table id="attr_tbl" style="clear: both" class="table table-striped table-borderless">
                                <tbody>
                                    <tr>
                                        <th width="35%">Google Shopping</th>
                                        <th width="65%"></th>
                                    </tr>
                                    <tr>
                                        <td>Google Shopping 1</td>
                                        <td><a id="cat1" data-title="Please Enter Value" data-placement="right" data-pk="1" data-type="typeaheadjs" href="#" data-original-title="" title="" class="editable editable-click editable-unsaved"><?= $product_model->Google_cat1; ?></a></td>
                                    </tr>
                                    <tr>
                                        <td>Google Shopping 2</td>
                                        <td><a id="cat2" data-title="Please Enter Value" data-placement="right" data-pk="1" data-type="select" href="#" data-original-title="" title="" class="editable editable-click editable-unsaved"><?= $product_model->Google_cat2; ?></a></td>
                                    </tr>
                                   <!-- <tr>
                                        <td>Google Shopping 3</td>
                                        <td><a id="cat3" data-title="Please Enter Value" data-placement="right" data-pk="1" data-type="typeaheadjs" href="#" data-original-title="" title="" class="editable editable-click"></a></td>
                                    </tr>-->
                                    <tr>
                                        <th width="35%">Contextual Data</th>
                                        <th width="65%">
                                            <?php if (empty($attribute_types)) { ?>
                                                <a class="btn btn-space btn-primary" href="/attribute-type/create"><i class="icon icon-left mdi mdi-plus"></i>Add an Attribute Type</a>
                                            <?php } ?>
                                        </th>
                                    </tr>
                                    <?php if (!empty($attribute_types)) { ?>
                                        <?php
                                        foreach ($attribute_types as $attribute_type) {
                                            $attrs = Attributes::find()->where(['attribute_type' => $attribute_type->attribute_type_id, 'elliot_user_id' => Yii::$app->user->identity->id])->all();
                                            ?>
                                            <tr>
                                                <td><?= ucfirst($attribute_type->attribute_type_name) ?></td>
                                                <?php if (empty($attrs)) { ?>
                                                    <td>       
                                                        <a class="btn btn-space btn-primary" href="/attributes/create"><i class="icon icon-left mdi mdi-plus"></i>Add an Attribute</a>
                                                    </td>
                                                <?php } else { ?>
                                                    <td><a class="up_context_data" data-title="Please Select" data-value="" data-type="checklist" href="#" class="editable editable-click" data-source="get-attributes?attr_type=<?= $attribute_type->attribute_type_id ?>"></a></td>
                                            <?php } ?>
                                            </tr>
                                        <?php }
                                    } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="categories" class="tab-pane">
                        <div class="table-responsive">
                            <a href="/categories/create" target="_blank" class="btn btn-space btn-primary"> 
                                <i class="icon icon-left mdi mdi-plus"></i> 
                                Add New Category 
                            </a>
                            <table id="cats_tbl" style="clear: both" class="table table-striped table-borderless">
                                <tbody>
                                    <tr>
                                        <td width="35%">Categories</td>
                                        <td width="65%"><a id="cats_list" data-title="Please Enter Value" data-value="<?php echo isset($cat_data_source) ? $cat_data_source : ''; ?>" data-type="checklist" href="#" class="editable editable-click" data-source="get-cats"><?php echo isset($cat_name_source) ? $cat_name_source : ''; ?></a></td>    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="ch_mngr" class="tab-pane">
                        <div class="table-responsive">
                            <a href="/channels/create" target="_blank" class="btn btn-space btn-primary"> 
                                <i class="icon icon-left mdi mdi-plus"></i> 
                                Add New Channel 
                            </a>
                            <table id="channel_tbl" style="clear: both" class="table table-striped table-borderless">
                                <tbody>
                                    <tr>
                                        <td width="35%">Select Channel for Sale</td>
                                        <td width="65%"><a id="channelsonproductview" data-title="Please Enter Value" data-value="<?php echo isset($value_check_list_display) ? $value_check_list_display : ''; ?>" data-type="checklist" href="#" class="editable editable-click" data-source="getchannel"><?php echo isset($value_to_display) ? $value_to_display : ''; ?></a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="invt_mngr" class="tab-pane">
                        <div class="table-responsive">
                            <table id="invt_tbl" style="clear: both" class="table table-striped table-borderless">
                                <tbody>
                                    <tr>
                                        <td width="35%">Stock Qty</td>
                                        <!--<td width="65%"><a id="stk_qty" href="#" data-type="text" data-title="Please Enter value"><?php //echo $stk_qty;        ?></a></td>-->
                                        <td width="65%"><a data-toggle="modal" data-target="#form-stock-qty" href="#" data-type="text" data-title="Please Enter value">Click to Modify Allocation by Channel</a></td>
                                    </tr>
                                    <tr>
                                        <td>Stock Level</td>
                                        <!--<td><a id="stk_lvl" data-title="Please Select" data-value="<?php //echo $stk_lvl;        ?>" data-pk="1" data-type="select" href="#"></a></td>-->
                                        <td width="65%"><a data-toggle="modal" data-target="#form-stock-level" href="#"><?= $stock_level_test; ?></a></td>
                                    </tr>
                                    <tr>
                                        <td>Stock Status</td>
                                        <!--<td><a id="stk_status" data-title="Please Select" data-value="<?php //echo $stk_status;        ?>" data-pk="1" data-type="select" href="#"></a></td>-->
                                        <td width="65%"><a data-toggle="modal" data-target="#form-stock-status" href="#"><?= $stock_status_test; ?></a></td>
                                    </tr>
                                    <tr>
                                        <td width="35%">Low Stock Notification</td>
                                        <td width="65%"><a id="low_stk_ntf" href="#" data-type="text" data-title="Please Enter value"><?php echo $low_stk_ntf; ?></a></td>
                                        <!--<td width="65%"><a data-toggle="modal" data-target="#form-stock-notification" href="#">#</a></td>-->
                                    </tr>
                                </tbody>
                            </table>
                        </div> 
                    </div>
                    <div id="media" class="tab-pane">
                        <div class="row pmedia pmedia-update">
                            <input type="hidden" value="<?= $Product_images_count; ?>" id="product_imgDivCount"/>
                            <?php
                            if ($Product_images_count == 0):
                                $more_divs_count = 4;
                            elseif ($Product_images_count == 1):
                                $more_divs_count = 3;
                            elseif ($Product_images_count == 2):
                                $more_divs_count = 2;
                            elseif ($Product_images_count == 3):
                                $more_divs_count = 1;
                            else:
                                $more_divs_count = 0;
                            endif;
                            $i = 0;
                            foreach ($Product_images as $p_image):
                                $i++;
                                $p_image_id = $p_image->image_ID;
                                $p_image_link = $p_image->link;
                                $p_image_label = $p_image->label;
                                $p_image_order = $p_image->priority;
                                $p_image_default = $p_image->default_image;
                                $radio_check = '';
                                if ($p_image_default == 'Yes'):
                                    $radio_check = 'checked';
                                elseif ($p_image_order == 1):
                                    $radio_check = 'checked';
                                endif;
                                ?>
                                <div class="col-sm-3 pimg-div<?= $i ?> be-loading draggable-element" id="pimgDiv_<?= $i ?>">

                                    <div class="bs-grid-block product_update_image product-texthover-default">
                                        <div class="user-display-bg product-texthover">
                                            <img src="<?= $p_image_link ?>"  alt="Profile Background" id="product_image_dropzone1"  height="300" class="custom_drozone_css dropzone">
                                            <div class="product-overlay custom_drozone_css dropzone span-product-image-css pimgUpdate_dropzone" id="pImageDrop_<?php echo $i; ?>"><br />
                                                <span class="panel-heading profile-panel-heading" ></span>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" id="upld_img<?php echo $i; ?>" class="upld_img" value="<?= $p_image_id; ?>">
                                    <div class="setimg">
                                        <i class="icon icon-left mdi mdi-image"></i> 
                                        Set as Default Image
                                        <div class="be-radio">
                                            <input <?= $radio_check ?> name="pimg_radio" id="pimgRad_<?php echo $i; ?>" type="radio">
                                            <label for="pimgRad_<?php echo $i; ?>"></label>
                                        </div>
                                    </div>
                                    <table id="pimg_tbl<?php echo $i; ?>" style="clear: both" class="table table-striped table-borderless">
                                        <tbody>
                                            <tr>
                                                <td width="45%">Image Label</td>
                                                <td width="55%"><a id="pimg_lbl<?php echo $i; ?>" class="pimg_lbl" href="#" data-type="text" data-title="Please Enter value"></a></td>
                                            </tr>
                                            <tr>
                                                <td width="45%">Alt Tag</td>
                                                <td width="55%"><a id="pimg_alt_tag<?php echo $i; ?>" class="pimg_alt_tag" href="#" data-type="text" data-title="Please Enter value"></a></td>
                                            </tr>
                                            <tr>
                                                <td width="45%">HTML Video Link</td>
                                                <td width="55%"><a id="pimg_html_video<?php echo $i; ?>" class="pimg_html_video" href="#" data-type="text" data-title="Please Enter value(Only support Youtube and Vimeo)"></a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="vfile">
                                        <input type="file" name="pimg_360_video" id="pimg_360_video<?php echo $i; ?>" data-multiple-caption="{count} files selected" multiple class="inputfile" accept="video/*" >
                                        <label for="pimg_360_video<?php echo $i; ?>" class="btn-default"> <i class="mdi mdi-upload"></i>
                                            <span>Select 360-degree video</span>
                                        </label>
                                    </div>
                                    <div class="progress" id="pimg_360_video_progress_wrapper<?php echo $i; ?>">
                                        <div id="pimg_360_video_progress<?php echo $i; ?>" class="progress-bar progress-bar-primary progress-bar-striped"></div>
                                    </div>
                                    <div class="vupld">
                                        <button id="vupldbtn_<?php echo $i; ?>" class="btn btn-rounded btn-space btn-default vupld_btn">Upload</button>
                                    </div>
                                    <div class="pimg_save_btns">
                                        <button id="pimgSaveBtn_<?php echo $i; ?>" class="btn btn-space btn-primary btn-sm pimg_save_btn">Save</button>
                                    </div>
                                    <div class="be-spinner">
                                        <svg width="40px" height="40px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                                        <circle fill="none" stroke-width="4" stroke-linecap="round" cx="33" cy="33" r="30" class="circle"/>
                                        </svg>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                            <?php
                            for ($a = 1; $a <= $more_divs_count; $a++):
                                $i++;
                                ?>
                                <div class="col-sm-3 pimg-div<?= $i ?> be-loading draggable-element" id="pimgDiv_<?= $i ?>">
                                    <div class="bs-grid-block product-texthover-default product_create_image">
                                        <div class="user-display-bg">
                                            <div class="content dropzone pImg_Create_Drop1" id="pImageDrop_<?php echo $i; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" id="upld_img<?php echo $i; ?>" class="upld_img" value="">
                                    <div class="setimg">
                                        <i class="icon icon-left mdi mdi-image"></i> 
                                        Set as Default Image
                                        <div class="be-radio">
                                            <input <?= isset($radio_check) ? $radio_check : ''; ?> name="pimg_radio" id="pimgRad_<?php echo $i; ?>" type="radio">
                                            <label for="pimgRad_<?php echo $i; ?>"></label>
                                        </div>
                                    </div>
                                    <table id="pimg_tbl<?php echo $i; ?>" style="clear: both" class="table table-striped table-borderless">
                                        <tbody>
                                            <tr>
                                                <td width="45%">Image Label</td>
                                                <td width="55%"><a id="pimg_lbl<?php echo $i; ?>" class="pimg_lbl" href="#" data-type="text" data-title="Please Enter value"></a></td>
                                            </tr>
                                            <tr>
                                                <td width="45%">Alt Tag</td>
                                                <td width="55%"><a id="pimg_alt_tag<?php echo $i; ?>" class="pimg_alt_tag" href="#" data-type="text" data-title="Please Enter value"></a></td>
                                            </tr>
                                            <tr>
                                                <td width="45%">HTML Video Link</td>
                                                <td width="55%"><a id="pimg_html_video<?php echo $i; ?>" class="pimg_html_video" href="#" data-type="text" data-title="Please Enter value(Only support Youtube and Vimeo)"></a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="vfile">
                                        <input type="file" name="pimg_360_video" id="pimg_360_video<?php echo $i; ?>" data-multiple-caption="{count} files selected" multiple class="inputfile" accept="video/*" >
                                        <label for="pimg_360_video<?php echo $i; ?>" class="btn-default"> <i class="mdi mdi-upload"></i>
                                            <span>Select 360-degree video</span>
                                        </label>
                                    </div>
                                    <div class="progress" id="pimg_360_video_progress_wrapper<?php echo $i; ?>">
                                        <div id="pimg_360_video_progress<?php echo $i; ?>" class="progress-bar progress-bar-primary progress-bar-striped"></div>
                                    </div>
                                    <div class="vupld">
                                        <button id="vupldbtn_<?php echo $i; ?>" class="btn btn-rounded btn-space btn-default vupld_btn">Upload</button>
                                    </div>
                                    <div class="pimg_save_btns">
                                        <button id="pimgSaveBtn_<?php echo $i; ?>" class="btn btn-space btn-primary btn-sm pimg_save_btn">Save</button>
                                    </div>
                                    <div class="be-spinner">
                                        <svg width="40px" height="40px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                                        <circle fill="none" stroke-width="4" stroke-linecap="round" cx="33" cy="33" r="30" class="circle"/>
                                        </svg>
                                    </div>
                                </div>
                            <?php endfor; ?>
                            <input type="hidden" value="<?= $i; ?>" id="imgDivCount"/>

                            <div class="addimg">
                                <button class="btn btn-space btn-primary addimg-btn">
                                    <i class="icon icon-left mdi mdi-plus"></i> 
                                    Add More
                                </button>
                            </div>
                        </div>
                    </div>
                    <div id="performance" class="tab-pane">
                        <!--For Grpah plots!-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="widget widget-fullwidth be-loading PerformanceTab">
                                    <div class="widget-head">
                                        <div class="tools">
                                            <div class="dropdown"><span data-toggle="dropdown" class="icon mdi mdi-more-vert visible-xs-inline-block dropdown-toggle"></span>
                                                <ul role="menu" class="dropdown-menu">
                                                    <li class="singleProductGraph" data-id="day"><a href="javscript:" >Daily</a></li>
                                                    <li  class="singleProductGraph" data-id="week"><a href="javscript:">Weekly</a></li>
                                                    <li class="singleProductGraph" data-id="month"><a href="javscript:" >Monthly</a></li>
                                                    <li class="singleProductGraph" data-id="quarter"><a href="javscript:" >Quarterly</a></li>
                                                    <li class="divider"></li>
                                                    <li class="singleProductGraph" data-id="year"><a href="javscript:" >Annually</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="button-toolbar hidden-xs">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default   singleProductGraph" data-id="day">Daily</button>
                                                <button type="button" class="btn btn-default   singleProductGraph" data-id="week">Weekly</button>
                                                <button type="button" class="btn btn-default  active singleProductGraph"  data-id="month">Monthly</button>
                                                <button type="button" class="btn btn-default  singleProductGraph"  data-id="quarter">Quarterly</button>
                                                <button type="button" class="btn btn-default  singleProductGraph" data-id="year">Yearly</button>

                                                <input type="hidden" name="" value="<?= $product_model->id ?>" id="hidden_product_id">
                                            </div>
                                        </div>
                                        <?php
                                        $store_connection_data = StoresConnection::find()->Where(['user_id' => $user_id])->with('stores')->asArray()->all();
                                        $channels_connection_data = ChannelConnection::find()->Where(['user_id' => $user_id])->with('channels')->asArray()->all();
                                        $connected_data = array_merge($store_connection_data, $channels_connection_data);
                                        ?>
                                        <?php if (empty($connected_data)): ?>
                                            <span class="title">Connect Your Store</span>
                                        <?php else: ?>
                                            <span class="title">Global Performance</span>  
                                        <?php endif; ?>
                                    </div>
                                    <div class="widget-chart-container">
                                        <div class="widget-chart-info">
                                            <ul id="chartInfoProduct" class="chart-legend-horizontal">
                                            </ul>
                                        </div>
                                        <!--STARTS daily Main GRAPH!-->
                                        <div id="single_product_chart" style="height: 260px;" class="single_product_chart"></div>
                                    </div>
                                    <div class="be-spinner" style="width: 100%;text-align:center;right:0;">
                                        <svg width="40px" height="40px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                                        <circle fill="none" stroke-width="4" stroke-linecap="round" cx="33" cy="33" r="30" class="circle"></circle>
                                        </svg>
                                        <span style="display:block; margin-top:30px;">Your data is loading.</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--for recent orders-->

                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default panel-table">
                                    <div class="panel-heading perf"> 
                                        <div class="title">Recent Orders</div>
                                    </div>
                                    <?php if (empty($connected_data)) : ?>
                                        <center><button class="btn btn-space btn-primary" id="see">Connect Your Store to see Data</button></center>
                                    <?php else : ?>
                                        <div class="panel-body table-responsive ">
                                            <table id="recent_orders_dashboard" class="table-borderless table table-striped table-hover table-fw-widget dataTable">
                                                <thead>
                                                    <tr>
                                                        <th>Order ID</th>
                                                        <th>Customer Name</th>
                                                        <th>Amount</th>
                                                        <th>Date</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="no-border-x">
                                                    <?php
                                                    $orders_data_recent = backend\models\OrdersProducts::find()->Where(['elliot_user_id' => Yii::$app->user->identity->id, 'product_id' => $product_id])->select('order_Id')->with(['order.customer'])->orderBy(['order_Id' => SORT_DESC,])->groupBy('order_Id')->limit(5)->all();
//                                        echo"<pre>";                                        print_r($orders_data_recent);die;
                                                    if (empty($orders_data_recent)):
                                                        ?>
                                                                                                                    <tr class="odd"><td valign="top" colspan="7" class="dataTables_empty">No data available in table.</td> </tr>
                                                        <?php
                                                    else :
                                                        foreach ($orders_data_recent as $orders_data_value) :
                                                            $channel_abb_id = isset($orders_data_value->order->channel_abb_id) ? $orders_data_value->order->channel_abb_id : "";
                                                            $firstname = $orders_data_value->order->customer->first_name;
                                                            $lname = $orders_data_value->order->customer->last_name;
                                                            $order_amount = isset($orders_data_value->order->total_amount) ? $orders_data_value->order->total_amount : 0;
                                                            $order_value = number_format((float) $order_amount, 2, '.', '');
                                                            $date_order = date('M-d-Y', strtotime($orders_data_value->order->order_date));
                                                            $order_status = $orders_data_value->order->order_status;
                                                            $label = '';
                                                            if ($order_status == 'Completed') :
                                                                $label = 'label-success';
                                                            endif;

                                                            if ($order_status == 'Returned' || $order_status == 'Refunded' || $order_status == 'Cancel') :
                                                                $label = 'label-danger';
                                                            endif;

                                                            if ($order_status == 'In Transit' || $order_status == 'On Hold'):
                                                                $label = 'label-primary';
                                                            endif;

                                                            if ($order_status == 'Awaiting Fulfillment' || $order_status == 'Incomplete' || $order_status == 'waiting-for-shipment' || $order_status == 'Pending'):
                                                                $label = 'label-warning';
                                                            endif;
                                                            if ($order_status == 'Shipped'):
                                                                $label = 'label-primary';
                                                            endif;


                                                            $conversion_rate = 1;
                                                            $user = Yii::$app->user->identity;
                                                            if (isset($user->currency) and $user->currency != 'USD') {
                                                                $username = Yii::$app->params['xe_account_id'];
                                                                $password = Yii::$app->params['xe_account_api_key'];
                                                                $URL = Yii::$app->params['xe_base_url'] . 'convert_from.json/?from=USD&to=' . $user->currency . '&amount=1';

                                                                $ch = curl_init();
                                                                curl_setopt($ch, CURLOPT_URL, $URL);
                                                                curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
                                                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                                                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                                                                curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
                                                                $result = curl_exec($ch);
                                                                $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
                                                                curl_close($ch);
//echo'<pre>';
                                                                $result = json_decode($result, true);
                                                                if (isset($result) and ! empty($result) and isset($result['to']) and isset($result['to'][0]) and isset($result['to'][0]['quotecurrency'])) {
                                                                    $conversion_rate = $result['to'][0]['mid'];
                                                                    $conversion_rate = number_format((float) $conversion_rate, 2, '.', '');
                                                                    $order_value = $order_value * $conversion_rate;
                                                                    $order_value = number_format((float) $order_value, 2, '.', '');
                                                                }
                                                            }

                                                            $selected_currency = \backend\models\CurrencySymbols::find()->where(['name' => strtolower($user->currency)])->select(['id', 'symbol'])->asArray()->one();
                                                            if (isset($selected_currency) and ! empty($selected_currency)) {
                                                                $currency_symbol = $selected_currency['symbol'];
                                                            }
                                                            ?>
                                                            <tr>
                                                                <td><a href="/orders/view/<?php echo $orders_data_value->order->order_ID; ?>"><?= $channel_abb_id; ?></a></td>
                                                                <td class="captialize"><?= $firstname . ' ' . $lname; ?></td>
                                                                <td class="" style="text-align:left;"><?php echo $currency_symbol ?><?= number_format($order_value, 2); ?></td>
                                                                <td><?= $date_order; ?></td>
                                                                <td><span class="label  <?= $label; ?>"><?= $order_status; ?></span></td>
                                                            </tr>
                                                            <?php
                                                        endforeach;
                                                    endif;
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    <?php
                                    endif;
                                    ?> 

                                </div>
                            </div>
                        </div>

                    </div>
                    <div id="pricing" class="tab-pane">
                        <div class="table-responsive"> 
                            <table id="pricing_tbl" style="clear: both" class="table table-striped table-borderless">
                                <tbody>
                                    <!--Strats For Elliot Price and sale price !-->
                                    <tr>
                                        <td width="35%">Price</td>
                                        <td width="65%"><span class="currency_symbol" style="color:#4285F4"><?php echo $currency_symbol ?></span><a id="price"  href="#" data-type="text" data-title="Please Enter value"><?php echo $price; ?></a></td>
                                    </tr>
                                    <tr>
                                        <td width="35%">Sale Price</td>
                                        <td width="65%"><span class="currency_symbol" style="color:#4285F4"><?php echo $currency_symbol ?></span><a id="sale_price"  href="#" data-type="text" data-title="Please Enter value"><?php echo $sale_price; ?></a></td>
                                    </tr>

                                    <tr>
                                        <td>Schedule Sale Date</td>
                                        <td  width="65%"><a id="schedule_date2" href="#" data-title="Please Select" data-pk="1" data-template="D / MMM / YYYY" data-viewformat="DD/MM/YYYY" data-format="YYYY-MM-DD" data-value="<?php echo $schedule_date1; ?>" data-type="combodate" class="editable editable-click"></a></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div id="accordionChannelS" class="panel-group accordion">

                                <!--END  For Elliot Price and sale price !--> 

                                <!--Strats For  Price and sale price for connected channel and stores !-->
                                <?php if (!empty($names)) { ?>
                                    <?php foreach ($names as $product_Connect_data) { ?>
                                        <?php
                                        $store_country = '';
                                        $magento_2 = substr($product_Connect_data, 0, 8);
                                        $final_product_Connect_data = preg_replace('/[0-9]+/', '', $product_Connect_data);
                                        $store_connection_id = preg_replace("/[^0-9,.]/", "", $product_Connect_data);
                                        if ($magento_2 == 'Magento2') {
                                            $final_product_Connect_data = $magento_2;
                                            $store_connection_id_2 = substr($product_Connect_data, 8);
                                            $store_connection_id = $store_connection_id_2;
                                        }
                                        $check_valid_store = Stores::find()->where(['store_name' => $final_product_Connect_data])->one();
                                        if (!empty($check_valid_store)) {
                                            $prdouct_pricing = ProductAbbrivation::find()->Where(['product_id' => $product_model->id, 'channel_accquired' => trim($final_product_Connect_data), 'mul_store_id' => $store_connection_id])->one();
                                            $store_connection_details_data = StoresConnection::find()->Where(['stores_connection_id' => $store_connection_id])->with('storesDetails')->one();
                                            if (!empty($store_connection_details_data)) {
                                                $store_country = $store_connection_details_data->storesDetails->country;
                                            }
                                        } else {
                                            $prdouct_pricing = ProductAbbrivation::find()->Where(['product_id' => $product_model->id, 'channel_accquired' => trim($final_product_Connect_data)])->one();
                                            $store_country = '';
                                        }

                                        $channel_setting = \backend\models\Channelsetting::find()->where(['channel_name' => $final_product_Connect_data, 'user_id' => Yii::$app->user->identity->id, 'setting_key' => 'currency'])->asArray()->one();
                                        if (isset($channel_setting) and ! empty($channel_setting)) {
                                            $channel_currency = $channel_setting['setting_value'];
                                        }
                                        if (isset($channel_currency) and $channel_currency != 'USD') {
                                            $username = Yii::$app->params['xe_account_id'];
                                            $password = Yii::$app->params['xe_account_api_key'];
                                            $URL = Yii::$app->params['xe_base_url'] . 'convert_from.json/?from=USD&to=' . $channel_currency . '&amount=1';

                                            $ch = curl_init();
                                            curl_setopt($ch, CURLOPT_URL, $URL);
                                            curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
                                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                                            curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
                                            $result = curl_exec($ch);
                                            $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
                                            curl_close($ch);
                                            $result = json_decode($result, true);
                                            if (isset($result) and ! empty($result) and isset($result['to']) and isset($result['to'][0]) and isset($result['to'][0]['quotecurrency'])) {
                                                $conversion_rate = $result['to'][0]['mid'];
                                                $conversion_rate = number_format((float) $conversion_rate, 2, '.', '');
                                            }
                                        }
                                        if (isset($channel_currency)) {
                                            $selected_currency = \backend\models\CurrencySymbols::find()->where(['name' => strtolower($channel_currency)])->select(['id', 'symbol'])->asArray()->one();
                                            if (isset($selected_currency) and ! empty($selected_currency)) {
                                                $currency_symbol_channel = $selected_currency['symbol'];
                                            }
                                        }
                                        $connect_price = number_format(($prdouct_pricing->price) * $conversion_rate, 2);
                                        $connect_sale_price = number_format($prdouct_pricing->salePrice * $conversion_rate, 2);
                                        $connect_schedule_date = $prdouct_pricing->schedules_sales_date;

                                        if (empty($currency_symbol_channel))
                                            $currency_symbol_channel = '$';
                                        ?>
                                        <?php if ($product_Connect_data == 'Google Shopping') {
                                            $href_link = 'Google';
                                        } {
                                            $href_link = str_replace(' ', '', $product_Connect_data);
                                        }
                                        ?>    


                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" class="collapsed" data-parent="#accordionChannelS" href="#<?= $href_link ?>"><i class="icon mdi mdi-chevron-down"></i><?= $final_product_Connect_data . ' ' . $store_country; ?></a></h4>
                                            </div>
                                            <div id="<?= $href_link ?>" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <table class="table table-striped table-borderless">
                                                        <tbody>
                                                            <tr>
                                                                <td width="35%"><?= $final_product_Connect_data; ?> Price</td>
                                                                <td width="65%"><span class="currency_symbol" style="color:#4285F4"><?php echo $currency_symbol_channel ?></span><a id="price1" class="price_connect" data-connectname="<?= $product_Connect_data; ?>" href="#" data-type="text" data-title="Please Enter value"><?php echo $connect_price; ?></a></td>
                                                            </tr>
                                                            <tr>
                                                                <td width="35%"><?= $final_product_Connect_data; ?> Sale Price</td>
                                                                <td width="65%"><span class="currency_symbol" style="color:#4285F4"><?php echo $currency_symbol_channel ?></span><a id="sale_price1" class="sale_price_connect" data-connectname="<?= $product_Connect_data; ?>" href="#" data-type="text" data-title="Please Enter value"><?php echo $connect_sale_price; ?></a></td>
                                                            </tr>
                                                            <tr>
                                                                <td><?= $final_product_Connect_data; ?> Schedule Sale Date</td>
                                                                <td  width="65%">
                                                                    <a id="connect_schedule_date_id" href="#" data-title="Please Select" data-pk="1" data-template="D / MMM / YYYY" data-viewformat="DD/MM/YYYY" data-format="YYYY-MM-DD" data-value="<?php echo $connect_schedule_date; ?>" data-type="combodate" data-scheduledate="<?= $product_Connect_data; ?>" class="editable editable-click schedule_date_connect">
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                            <?php }}?>
                            </div>
                            <!--End For  Price and sale price for connected channel and stores !-->  
                        </div>
                    </div>
					
                   <!--For Translation Tab According ton smartling!-->
                    <div id="translation" class="tab-pane">
			<div class="row"> 
			    <div class="col-sm-12">
				<div id="accordionChannelS1" class="panel-group accordion">
				    <?php if(!empty($smartling_stores_name)) {
					$desc=$data_desc=$data_name='';
					$user_id = Yii::$app->user->identity->id;
					$domain_name = Yii::$app->user->identity->domain_name;
				    ?>

				    <?php foreach ($smartling_stores_name as $sm_store) { 
					$checked;
					$acc_country		=   $sm_store['acc_country'];
					$channel_acc		=   $sm_store['channel_acc'];
					$connection_id		=   $sm_store['connection_id'];
					$sm_channel_country	=   trim($channel_acc.' '.$acc_country);
					$sm_acc_id		=   str_replace(" ","_",trim($sm_channel_country));
					$sm_accordian_id	=   $sm_acc_id.'_translation';
					$sm_panel_id		=   $sm_acc_id.'_panel';
				       // $file_read_data='/var/www/html/img/uploaded_files/smartling_files/shubham4_1952/BigCommerce United States.json';
					$folder_name=$domain_name.'_'.$user_id;
					$file_read_data=$smartling_files_dir.'/'.$folder_name.'/'.$sm_channel_country.'/'.$sm_channel_country.'.json';
                                        
					if($sm_store['translation_status']=='yes'){
					    $checked='checked';
					}else{
					    $checked='';
					}
					if(file_exists($file_read_data)){
					    $file_data=file_get_contents($file_read_data);
					    $file_decode_data= json_decode($file_data,true);
					    if(!empty($file_decode_data)){
						    foreach($file_decode_data as $file_value){
							    if(isset($file_value['id'])){
								$trans_product_id=$file_value['id'];
								$file_value_id= preg_replace('/[^A-Za-z0-9-]/', '', $trans_product_id);
								    if($file_value_id==$product_id){
									    $data_desc=$file_value['description'];
									    $data_name=$file_value['name'];
								    }
							    }
						    }
					    }
					    //$data_desc=$file_decode_data->string1.' '.$file_decode_data->string2;
					}
				    ?>
					
                            
				<div class="panel panel-default">

				    <div class="panel-heading">
					<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordionChannelS1" href="#<?=trim($sm_accordian_id);?>"><i class="icon mdi mdi-chevron-down"></i><?= trim($sm_channel_country);?></a></h4>
				    </div>
				    <div id="<?=trim($sm_accordian_id);?>" class="panel-collapse collapse">
					<div class="panel-body">
					    <div class=" col-sm-4 control-label panel-heading">Override Translation</div>
					    <div class="switch-button switch-button-lg">
						<input type="checkbox" <?=$checked;?> name="<?=trim($sm_panel_id);?>" id="<?=trim($sm_panel_id);?>" class="swt99" data-productid="<?=$product_id;?>" data-connectionid="<?=$connection_id;?>" data-channel="<?=$channel_acc;?>"><span>
						<label for="<?=trim($sm_panel_id);?>"></label></span>
					    </div>
					    <div class="panel-body div_translate_title<?=$connection_id;?>" style="display:none">
						<!--<div class=" col-sm-4 control-label panel-heading custom_Transs">Product Title</div>
						<a id="product_translate_title<?=$connection_id;?>" class="trans_title" href="#" data-type="text" data-title="Please Enter value"><?=$data_name;?></a>-->
						<div class="table-responsive div_translate_title106">
                            <table id="general_tbl" style="clear: both" class="table table-striped table-borderless">
                                <tbody>
                                    <tr>
                                        <td width="35%">Product Title</td>
                                        <td width="65%"><a id="product_translate_title<?=$connection_id;?>" class="trans_title" href="#" data-type="text" data-title="Please Enter value"><?=$data_name;?></a></td>
                                    </tr>
									 <tr>
                                        <td width="35%">Description</td>
                                        <td width="65%"><div id="smartling_editor<?=$connection_id;?>" class="smartling_editor<?=$connection_id;?>" style="display: none;"><?= $data_desc;?></div></td>
                                    </tr>
									
                                 </tbody>
                            </table>
						</div>
					    </div>
					    
					</div>
				    </div>
				</div>
                        <?php } } ?>
			    </div>
			</div>
		    </div>
                    </div>
                </div>
                <div class="tab_up_btn_div custOMPerf">
                    <button class="btn btn-space btn-primary" onclick="updateProduct()">Publish</button>
                </div>
            </div>
        </div>
    </div>
</div> 

<?php
$sum_stock_quantity = ProductAbbrivation::find()->Where(['product_id' => $product_model->id])->sum('stock_quantity');
/* Get count Of Stock Quantity */
$count_stock_quantity = ProductAbbrivation::find()->Where(['product_id' => $product_model->id])->count('stock_quantity');
?>
<!--Starst Product Stock Quantity modal -->   
<div id="form-stock-qty" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
    <div class="modal-dialog custom-width">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><span class="mdi mdi-close"></span></button>
                <h3 class="modal-title">Stock Quantity</h3> 
            </div>
            <input type="hidden" id="sum_product_abb" value="<?= $sum_stock_quantity; ?>"/>
            <input type="hidden" id="count_product_abb" value="<?= $count_stock_quantity; ?>"/>
            <div class="modal-body ModalCustomClass be-loading">
                <div class="table-responsive"> 
                    <div class="be-spinner">
                        <svg width="40px" height="40px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                        <circle fill="none" stroke-width="4" stroke-linecap="round" cx="33" cy="33" r="30" class="circle"></circle>
                        </svg>
                    </div>
                    <table id="pricing_tbl" style="clear: both" class="table table-striped table-borderless">
                        <tbody>
                            <!--Strats For Elliot Stock Qty!--> 
                            <tr>
                                <td width="35%">InventoryIQ</td> 
                                <td>
                                    <div class="switch-button switch-button-lg">
                                        <input type="checkbox" name="qnt" id="qnt">
                                        <span><label for="qnt" class="chnl_dsbl_rv"></label></span>
                                    </div>
                                </td>
                                <td> <div class="allocate_priority">Priority</div></td>
                            </tr>
                            <tr>
                                <td>Stock Qty</td>
                                <td width="20%"><a id="stk_qty" href="#" data-type="text" data-title="Please Enter value"><?= $sum_stock_quantity; ?></a></td>
                                <!--<td><a class="allocate_inventory" id="elli_allocate_inv" href="#" data-type="text" data-title="Please Enter value"><? //= $product_model->allocate_inventory;  ?></a></td>-->
                                <td style="display:inline-flex;"><a class="allocate_inventory" id="elli_allocate_inv" href="#" data-type="text" data-title="Please Enter value">100</a>
                                    <span class="allocate_percent" style="color:#4285F4; display:none;">%</span>
                                </td>
                            </tr> 
                            <!--END  For Elliot Stock Qty!--> 
                            <!--Strats For  Stock Qty for connected channel and stores !-->
                            <?php
                            if (!empty($names)) {
                                $i = 1;
                                foreach ($names as $product_Connect_data) {
                                    $store_country = '';
                                    $store_connection_id = preg_replace("/[^0-9,.]/", "", $product_Connect_data);
                                    $product_Connect_data_final = preg_replace('/[0-9]+/', '', $product_Connect_data);

                                    $magento_2 = substr($product_Connect_data, 0, 8);
                                    if ($magento_2 == 'Magento2') {
                                        $product_Connect_data_final = $magento_2;
                                        $store_connection_id_2 = substr($product_Connect_data, 8);
                                        $store_connection_id = $store_connection_id_2;
                                    }

                                    $check_valid_store = Stores::find()->where(['store_name' => $product_Connect_data_final])->one();
                                    if (!empty($check_valid_store)) {
                                        $prdouct_abb_qty = ProductAbbrivation::find()->Where(['product_id' => $product_model->id, 'channel_accquired' => trim($product_Connect_data_final), 'mul_store_id' => $store_connection_id])->one();
                                        $store_connection_details_data = StoresConnection::find()->Where(['stores_connection_id' => $store_connection_id])->with('storesDetails')->one();
                                        if (!empty($store_connection_details_data)) {
                                            $store_country = @$store_connection_details_data->storesDetails->country;
                                        }
                                    } else {
                                        $prdouct_abb_qty = ProductAbbrivation::find()->Where(['product_id' => $product_model->id, 'channel_accquired' => trim($product_Connect_data)])->one();
                                        $store_country = '';
                                    }
                                    ?>
                                    <tr>
                                        <td><?= $product_Connect_data_final . ' ' . $store_country; ?> Stock Qty</td>
                                        <td width="20%">
                                            <a id="stk_qty" href="#" class="stock_qty_connect" data-classname="qtyiq<?= $i; ?>" data-stock-qty-connect="<?= $product_Connect_data; ?>" data-value="<?= $prdouct_abb_qty->stock_quantity; ?>" data-type="text" data-title="Please Enter value"><?= $prdouct_abb_qty->stock_quantity; ?></a>
                                        </td>
                                        <!--<td><a class="allocate_inventory allocate_inv_connect" href="#" data-allocate-inv-connect="<? //= $product_Connect_data;  ?>" data-type="text" data-title="Please Enter value"><? //= $prdouct_abb_qty->allocate_inventory;  ?></a></td>-->
                                        <td style="display:inline-flex;">
                                            <a class="allocate_inventory allocate_inv_connect qtyiq<?= $i; ?>" href="#" data-value="" data-allocate-inv-connect="<? //= $product_Connect_data; ?>" data-type="text" data-title="Please Enter value">0</a>
                                            <span class="allocate_percent" style="color:#4285F4 ;display:none;">%</span>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?> 
                            <!--End For  Stock Qty for connected channel and stores !-->     
                        </tbody>
                    </table>
                </div> 
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default md-close">Cancel</button>
                <button type="button" data-dismiss="modal" class="btn btn-primary md-close">Save</button>
            </div>
        </div> 
    </div>
</div>

<!--End Product Stock Quantity modal --> 

<!--Starst Product Stock level modal -->   
<div id="form-stock-level" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
    <div class="modal-dialog custom-width">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><span class="mdi mdi-close"></span></button>
                <h3 class="modal-title">Stock Level</h3>
            </div>
            <div class="modal-body">
                <div class="table-responsive"> 
                    <table id="pricing_tbl" style="clear: both" class="table table-striped table-borderless">
                        <tbody>
                            <!--Strats For Elliot Stock Qty!-->
                            <tr>
                                <td width="35%">Stock Level</td>
                                <td><a id="stk_lvl" data-title="Please Select" data-value="<?php echo $stk_lvl; ?>" data-pk="1" data-type="select" href="#"></a></td>
                            </tr>
                            <!--END  For Elliot Stock Qty!--> 

                            <!--Strats For  Stock Qty for connected channel and stores !-->
                            <?php
                            if (!empty($names)) {
                                foreach ($names as $product_Connect_data) {
                                    $store_country = '';
                                    $store_connection_id = preg_replace("/[^0-9,.]/", "", $product_Connect_data);
                                    $product_Connect_data_final = preg_replace('/[0-9]+/', '', $product_Connect_data);

                                    $magento_2 = substr($product_Connect_data, 0, 8);
                                    if ($magento_2 == 'Magento2') {
                                        $product_Connect_data_final = $magento_2;
                                        $store_connection_id_2 = substr($product_Connect_data, 8);
                                        $store_connection_id = $store_connection_id_2;
                                    }

                                    $check_valid_store = Stores::find()->where(['store_name' => $product_Connect_data_final])->one();
                                    if (!empty($check_valid_store)) {

                                        $prdouct_abb_stock = ProductAbbrivation::find()->Where(['product_id' => $product_model->id, 'channel_accquired' => trim($product_Connect_data_final), 'mul_store_id' => $store_connection_id])->one();
                                        $store_connection_details_data = StoresConnection::find()->Where(['stores_connection_id' => $store_connection_id])->with('storesDetails')->one();
                                        if (!empty($store_connection_details_data)) {
                                            $store_country = @$store_connection_details_data->storesDetails->country;
                                        }
                                    } else {
                                        $prdouct_abb_stock = ProductAbbrivation::find()->Where(['product_id' => $product_model->id, 'channel_accquired' => trim($product_Connect_data)])->one();
                                        $store_country = '';
                                    }
                                    ?>
                                    <tr>
                                        <td width="35%"><?= $product_Connect_data_final . ' ' . $store_country; ?> Stock Level</td>
                                        <td><a id="connect_stk_lvl" class="stock_level_connect" data-stock-level-connect="<?= $product_Connect_data; ?>" data-title="Please Select" data-value="<?= $prdouct_abb_stock->stock_level; ?>" data-pk="1" data-type="select" href="#"></a></td>
                                    </tr>
        <?php
    }
}
?>
                            <!--End For  Stock Qty for connected channel and stores !-->    
                        </tbody>
                    </table>
                </div>  
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default md-close">Cancel</button>
                <button type="button" data-dismiss="modal" class="btn btn-primary md-close">Save</button>
            </div>
        </div> 
    </div>
</div>
<!--End Product Stock Level modal -->   

<!--Starst Product Stock Status modal -->   
<div id="form-stock-status" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
    <div class="modal-dialog custom-width">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><span class="mdi mdi-close"></span></button>
                <h3 class="modal-title">Stock Status</h3>
            </div>
            <div class="modal-body">
                <div class="table-responsive"> 
                    <table id="pricing_tbl" style="clear: both" class="table table-striped table-borderless">
                        <tbody>
                            <!--Strats For Elliot Stock Qty!-->
                            <tr>
                                <td width="35%">Stock Status</td>
                                <td><a id="stk_status" data-title="Please Select" data-value="<?php echo $stk_status; ?>" data-pk="1" data-type="select" href="#"></a></td>
                            </tr>
                            <!--END  For Elliot Stock Qty!--> 

                            <!--Strats For  Stock Qty for connected channel and stores !-->
                            <?php
                            if (!empty($names)) {
                                foreach ($names as $product_Connect_data) {
                                    $store_country = '';
                                    $store_connection_id = preg_replace("/[^0-9,.]/", "", $product_Connect_data);
                                    $product_Connect_data_final = preg_replace('/[0-9]+/', '', $product_Connect_data);

                                    $magento_2 = substr($product_Connect_data, 0, 8);
                                    if ($magento_2 == 'Magento2') {
                                        $product_Connect_data_final = $magento_2;
                                        $store_connection_id_2 = substr($product_Connect_data, 8);
                                        $store_connection_id = $store_connection_id_2;
                                    }

                                    $check_valid_store = Stores::find()->where(['store_name' => $product_Connect_data_final])->one();
                                    if (!empty($check_valid_store)) {
                                        $prdouct_abb_stock = ProductAbbrivation::find()->Where(['product_id' => $product_model->id, 'channel_accquired' => trim($product_Connect_data_final), 'mul_store_id' => $store_connection_id])->one();
                                        $prdouct_abb_stk_status = $prdouct_abb_stock->stock_status;
                                        $store_connection_details_data = StoresConnection::find()->Where(['stores_connection_id' => $store_connection_id])->with('storesDetails')->one();
                                        if (!empty($store_connection_details_data)) {
                                            $store_country = @$store_connection_details_data->storesDetails->country;
                                        }
                                    } else {
                                        $prdouct_abb_stock = ProductAbbrivation::find()->Where(['product_id' => $product_model->id, 'channel_accquired' => trim($product_Connect_data)])->one();
                                        $prdouct_abb_stk_status = $prdouct_abb_stock->stock_status;
                                        $store_country = '';
                                    }
                                    ?>
                                    <tr>
                                        <td width="35%"><?= $product_Connect_data_final . ' ' . $store_country; ?> Stock Status</td> 
                                        <td>
                                            <a id="connect_stk_status" data-title="Please Select" class="stock_status_connect" data-stock-status-connect="<?= $product_Connect_data; ?>" data-value="<?= $prdouct_abb_stk_status; ?>" data-pk="1" data-type="select" href="#">
                                            </a>
                                        </td>
                                    </tr>
        <?php
    }
}
?>
                            <!--End For  Stock Qty for connected channel and stores !-->    
                        </tbody>
                    </table> 
                </div>   
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default md-close">Cancel</button>
                <button type="button" data-dismiss="modal" class="btn btn-primary md-close">Save</button>
            </div>
        </div> 
    </div>
</div>
<!--End Product Stock Status modal -->    

<!-- Product error modal -->
<div id="mod-danger" tabindex="-1" role="dialog" class="modal fade in product_update_ajax_request_error" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close product_update_error_modal_close"></span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
                    <h3 id='product_update_header_error_msg'></h3>
                    <p id="product_update_ajax_msg_eror"></p>
                    <div class="xs-mt-50">
                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default product_update_error_modal_close">Close</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div> 
