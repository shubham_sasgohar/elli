<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Products */

$this->title = 'Product Manager';
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

//echo '<Pre>';
//print_r($model);
//die("***");

/* Product Fields Values */
$product_name = isset($model->product_name) ? $model->product_name : "";
$SKU = isset($model->SKU) ? $model->SKU : "";
$EAN = isset($model->EAN) ? $model->EAN : "";
$JAN = isset($model->Jan) ? $model->Jan : "";
$ISBN = isset($model->ISBN) ? $model->ISBN : "";
$MPN = isset($model->MPN) ? $model->MPN : "";
$adult = isset($model->adult) ? $model->adult : "no";
$age_group = isset($model->age_group) ? $model->age_group : "";
$availability = isset($model->availability) ? $model->availability : "Out of Stock";
$brand = isset($model->brand) ? $model->brand : "";
$condition = isset($model->condition) ? $model->condition : "New";
$gender = isset($model->gender) ? $model->gender : "Unisex";
$weight = isset($model->weight) ? $model->weight : 0;
?>
<h1><?= Html::encode($model->product_name) ?></h1>
<p>
    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?=
    Html::a('Delete', ['delete', 'id' => $model->id], [
      'class' => 'btn btn-danger',
      'data' => [
        'confirm' => 'Are you sure you want to delete this item?',
        'method' => 'post',
      ],
    ])
    ?>
</p>
<div class="row">
    <!--Default Tabs-->
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="tab-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#general" data-toggle="tab">General Information</a></li>
                    <li><a href="#attribution" data-toggle="tab">Attribution</a></li>
                    <li><a href="#categories" data-toggle="tab">Categories</a></li>
                    <li><a href="#ch_mngr" data-toggle="tab">Channel Manager</a></li>
                    <li><a href="#invt_mngr" data-toggle="tab">Inventory Management</a></li>
                    <li><a href="#media" data-toggle="tab">Media</a></li>
                    <li><a href="#performance" data-toggle="tab">Performance</a></li>
                    <li><a href="#pricing" data-toggle="tab">Pricing</a></li>
                    <li><a href="#translation" data-toggle="tab">Translation</a></li>
                </ul>
                <div class="tab-content">
                    <div id="general" class="tab-pane active cont">
                        <div class="table-responsive">
                            <table id="user" style="clear: both" class="table table-striped table-borderless">
                                <tbody>
                                    <tr>
                                        <td width="35%">Name</td>
                                        <td width="65%"><a id="product_name" href="#" data-type="text" data-title="Please Enter value"><?php echo $product_name; ?></a></td>
                                    </tr>
                                    <tr>
                                        <td width="35%">SKU</td>
                                        <td width="65%"><a id="SKU" href="#" data-type="text" data-title="Please Enter value (must be unique)" ><?php echo $SKU; ?></a></td>
                                    </tr>
                                    <tr>
                                        <td width="35%">EAN</td>
                                        <td width="65%"><a id="EAN" href="#" data-type="text" data-title="Please Enter value (must be unique)"><?php echo $EAN; ?></a></td>
                                    </tr>
                                    <tr>
                                        <td width="35%">JAN</td>
                                        <td width="65%"><a id="JAN" href="#" data-type="text" data-title="Please Enter value (must be unique)"><?php echo $JAN; ?></a></td>
                                    </tr>
                                    <tr>
                                        <td width="35%">ISBN</td>
                                        <td width="65%"><a id="ISBN" href="#" data-type="text" data-title="Please Enter value (must be unique)"><?php echo $ISBN; ?></a></td>
                                    </tr>
                                    <tr>
                                        <td width="35%">MPN</td>
                                        <td width="65%"><a id="MPN" href="#" data-type="text" data-title="Please Enter value (must be unique)"><?php echo $MPN; ?></a></td>
                                    </tr>
                                    <tr>
                                        <td>Adult</td>
                                        <td><a id="adult" data-title="Please Select" data-value="<?php echo $adult; ?>" data-pk="1" data-type="select" href="#"></a></td>
                                    </tr>
                                    <tr>
                                        <td>Age Group</td>
                                        <td><a id="age_group" data-title="Please Select" data-value="<?php echo $age_group; ?>" data-pk="1" data-type="select" href="#"></a></td>
                                    </tr>
                                    <tr>
                                        <td>Availability</td>
                                        <td><a id="availability" data-title="Please Select" data-value="<?php echo $availability; ?>" data-pk="1" data-type="select" href="#"></a></td>
                                    </tr>
                                    <tr>
                                        <td width="35%">Brand</td>
                                        <td width="65%"><a id="brand" href="#" data-type="text" data-title="Please Enter value"><?php echo $brand; ?></a></td>
                                    </tr>
                                    <tr>
                                        <td>Condition</td>
                                        <td><a id="condition" data-title="Please Select" data-value="<?php echo $condition; ?>" data-pk="1" data-type="select" href="#"></a></td>
                                    </tr>
                                    <tr>
                                        <td>Gender</td>
                                        <td><a id="gender" data-title="Please Select" data-value="<?php echo $gender; ?>" data-pk="1" data-type="select" href="#"></a></td>
                                    </tr>
                                    <tr>
                                        <td width="35%">Weight</td>
                                        <td width="65%"><a id="weight" href="#" data-type="text" data-title="Please Enter value"><?php echo $weight; ?></a></td>
                                    </tr>
                                    <tr>
                                        <td>Twitter typeahead.js</td>
                                        <td><a id="state2" data-title="Start typing State.." data-placement="right" data-pk="1" data-type="typeaheadjs" href="#" data-original-title="" title="" class="editable editable-click">California</a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="products-view">
                            <?=
                            DetailView::widget([
                              'model' => $model,
                              'attributes' => [
                                'id',
                                'product_name',
                                'SKU',
                                'EAN',
                                'Jan',
                                'ISBN',
                                'MPN',
                                'description',
                                'adult',
                                'age_group',
                                'availability',
                                'brand',
                                'condition',
                                'gender',
                                'weight',
                                'stock_quantity',
                                'stock_level',
                                'stock_status',
                                'low_stock_notification',
                                'price',
                                'sales_price',
                                'schedules_sales_date',
                                'created_at',
                                'updated_at',
                                'occasion',
                                'weather',
                              ],
                            ])
                            ?>
                            <p>
                                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                                <?=
                                Html::a('Delete', ['delete', 'id' => $model->id], [
                                  'class' => 'btn btn-danger',
                                  'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                  ],
                                ])
                                ?>
                            </p>

                        </div>

                    </div>
                    <div id="attribution" class="tab-pane cont">
                        <div class="products-form">

                            <?php $form = ActiveForm::begin(); ?>

                            <?= $form->field($model, 'product_name')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'SKU')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'EAN')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'Jan')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'ISBN')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'MPN')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'adult')->dropDownList([ 'no' => 'No', 'yes' => 'Yes',], ['prompt' => '']) ?>

                            <?= $form->field($model, 'age_group')->dropDownList([ 'Newborn' => 'Newborn', 'Infant' => 'Infant', 'Toddler' => 'Toddler', 'Kids' => 'Kids', 'Adult' => 'Adult',], ['prompt' => '']) ?>

                            <?= $form->field($model, 'availability')->dropDownList([ 'In Stock' => 'In Stock', 'Out of Stock' => 'Out of Stock',], ['prompt' => '']) ?>

                            <?= $form->field($model, 'brand')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'condition')->dropDownList([ 'New' => 'New', 'Used' => 'Used', 'Refurbished' => 'Refurbished',], ['prompt' => '']) ?>

                            <?= $form->field($model, 'gender')->dropDownList([ 'Female' => 'Female', 'Male' => 'Male', 'Unisex' => 'Unisex',], ['prompt' => '']) ?>

                            <?= $form->field($model, 'weight')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'stock_quantity')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'stock_level')->dropDownList([ 'In Stock' => 'In Stock', 'Out of Stock' => 'Out of Stock',], ['prompt' => '']) ?>

                            <?= $form->field($model, 'stock_status')->dropDownList([ 'Visible' => 'Visible', 'Hidden' => 'Hidden',], ['prompt' => '']) ?>

                            <?= $form->field($model, 'low_stock_notification')->textInput() ?>

                            <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'sales_price')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'schedules_sales_date')->textInput() ?>

                            <?= $form->field($model, 'created_at')->textInput() ?>

                            <?= $form->field($model, 'updated_at')->textInput() ?>

                            <?= $form->field($model, 'occasion')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'weather')->textInput(['maxlength' => true]) ?>

                            <div class="form-group">
                                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                            </div>

                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                    <div id="categories" class="tab-pane">
                        <p>Consectetur adipisicing elit. Ipsam ut praesentium, voluptate quidem necessitatibus quam nam officia soluta aperiam, recusandae.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos facilis laboriosam, vitae ipsum tenetur atque vel repellendus culpa reiciendis velit quas, unde soluta quidem voluptas ipsam, rerum fuga placeat rem error voluptate eligendi modi. Delectus, iure sit impedit? Facere provident expedita itaque, magni, quas assumenda numquam eum! Sequi deserunt, rerum.</p><a href="#">Read more  </a>
                    </div>
                    <div id="ch_mngr" class="tab-pane">
                        <p>Consectetur adipisicing elit. Ipsam ut praesentium, voluptate quidem necessitatibus quam nam officia soluta aperiam, recusandae.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos facilis laboriosam, vitae ipsum tenetur atque vel repellendus culpa reiciendis velit quas, unde soluta quidem voluptas ipsam, rerum fuga placeat rem error voluptate eligendi modi. Delectus, iure sit impedit? Facere provident expedita itaque, magni, quas assumenda numquam eum! Sequi deserunt, rerum.</p><a href="#">Read more  </a>
                    </div>
                    <div id="invt_mngr" class="tab-pane">
                        <p>Consectetur adipisicing elit. Ipsam ut praesentium, voluptate quidem necessitatibus quam nam officia soluta aperiam, recusandae.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos facilis laboriosam, vitae ipsum tenetur atque vel repellendus culpa reiciendis velit quas, unde soluta quidem voluptas ipsam, rerum fuga placeat rem error voluptate eligendi modi. Delectus, iure sit impedit? Facere provident expedita itaque, magni, quas assumenda numquam eum! Sequi deserunt, rerum.</p><a href="#">Read more  </a>
                    </div>
                    <div id="media" class="tab-pane">
                        <p>Consectetur adipisicing elit. Ipsam ut praesentium, voluptate quidem necessitatibus quam nam officia soluta aperiam, recusandae.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos facilis laboriosam, vitae ipsum tenetur atque vel repellendus culpa reiciendis velit quas, unde soluta quidem voluptas ipsam, rerum fuga placeat rem error voluptate eligendi modi. Delectus, iure sit impedit? Facere provident expedita itaque, magni, quas assumenda numquam eum! Sequi deserunt, rerum.</p><a href="#">Read more  </a>
                    </div>
                    <div id="performance" class="tab-pane">
                        <p>Consectetur adipisicing elit. Ipsam ut praesentium, voluptate quidem necessitatibus quam nam officia soluta aperiam, recusandae.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos facilis laboriosam, vitae ipsum tenetur atque vel repellendus culpa reiciendis velit quas, unde soluta quidem voluptas ipsam, rerum fuga placeat rem error voluptate eligendi modi. Delectus, iure sit impedit? Facere provident expedita itaque, magni, quas assumenda numquam eum! Sequi deserunt, rerum.</p><a href="#">Read more  </a>
                    </div>
                    <div id="pricing" class="tab-pane">
                        <p>Consectetur adipisicing elit. Ipsam ut praesentium, voluptate quidem necessitatibus quam nam officia soluta aperiam, recusandae.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos facilis laboriosam, vitae ipsum tenetur atque vel repellendus culpa reiciendis velit quas, unde soluta quidem voluptas ipsam, rerum fuga placeat rem error voluptate eligendi modi. Delectus, iure sit impedit? Facere provident expedita itaque, magni, quas assumenda numquam eum! Sequi deserunt, rerum.</p><a href="#">Read more  </a>
                    </div>
                    <div id="translation" class="tab-pane">
                        <p>Consectetur adipisicing elit. Ipsam ut praesentium, voluptate quidem necessitatibus quam nam officia soluta aperiam, recusandae.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos facilis laboriosam, vitae ipsum tenetur atque vel repellendus culpa reiciendis velit quas, unde soluta quidem voluptas ipsam, rerum fuga placeat rem error voluptate eligendi modi. Delectus, iure sit impedit? Facere provident expedita itaque, magni, quas assumenda numquam eum! Sequi deserunt, rerum.</p><a href="#">Read more  </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
