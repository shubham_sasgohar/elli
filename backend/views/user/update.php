<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\User */

$this->title = 'Update User: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-profile">
    <div class="row">
<div class="col-md-5">

    <h1><?= Html::encode($this->title) ?></h1>



<div class="page-head">
    <h2 class="page-head-title"><?= Html::encode($this->title) ?></h2>
    <ol class="breadcrumb page-head-nav">
    <?php
        echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
    ?>
    </ol>
</div>

    <div class="subuser-info-list panel panel-default">
                <div class="panel-body">
                <div class="table-responsive">
                <table id="sub_user" style="clear: both" class="table table-striped table-borderless">
                <tbody>
                    <tr>
                        <td width="35%">First Name </td>
                        <td width="65%"><a href="javascript:" data-type="text" class="editable editable-click" data-title="Enter First Name"  id="first_name_sub" ><?php echo $model->first_name; ?></a></td>
                        <input type="hidden" name="sub_profile_id" value="<?php echo $model->id; ?>" id="sub_profile_id" />
                    </tr>
                    <tr>
                        <td width="35%">Last Name</td>
                        <td width="65%"><a href="javascript:" data-type="text" class="editable editable-click" data-title="Enter Last Name" id="last_name_sub"><?php echo $model->last_name; ?></a></td>
                    </tr>
                    <tr>
                    <td width="35%">Email Address</td>
                    <td width="65%"><a id="email_add_sub" href="javascript:" class="editable editable-click" data-type="text" data-title="Enter Email Address"><?php echo $model->email; ?></a></td>
                    </tr>
                    <tr>
                    <td width="35%" >Role</td>
                    <td width="65%"><a id="role_sub" href="javascript:" class="editable editable-click" data-value="<?php echo $model->role; ?>" data-type="select" data-title="Enter Role"><?php echo $model->role; ?></a></td>
                    </tr>
                    <tr>
                       <td><button class="btn btn-space btn-primary" onclick="savesubprofile()">Save</button></td>
                    </tr>
                </tbody>
                </table>
                </div>
                </div>
    </div>
</div>
</div>
</div>