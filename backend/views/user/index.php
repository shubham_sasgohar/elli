<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\User;
use yii\widgets\Breadcrumbs;

/* @var get Baseurl  */
$basepath_backend = Yii::getAlias('@baseurl'). '/backend';
$username=Yii::$app->user->identity->username;
$new_basepath = 'http://' . $username . '.'.Yii::$app->params['DOMAIN_NAME'];
/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => 'javascript: void(0)', 'class' => 'non_link'];
$this->params['breadcrumbs'][] = ['label' => 'User Management', 'url' => ['/user']];
$this->params['breadcrumbs'][] = 'View All';
/* Get user data*/
//$userdata = User::find()->Where(['parent_id' => Yii::$app->user->identity->id])->all();
$userdata = User::find()->all();
$role_merchant='merchant';
$role_merchant_user='merchant_user';
?>



<div class="page-head">
    <h2 class="page-head-title"><?= Html::encode($this->title) ?></h2>
    <ol class="breadcrumb page-head-nav">
    <?php
        echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
    ?>
    </ol>
    <?= Html::a('Create User', ['create'], ['class' => 'btn btn-primary']) ?>
</div>

<?php if(empty($userdata)) {?>
<p>
        <?= Html::a('Add Member', ['create'], ['class' => 'btn btn-space btn-primary']) ?>
</p>
<?php }?>


<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default panel-table">
            <div class="panel-heading">Default
                <div class="tools">

                    <a href="<?php echo $new_basepath ?>export-user"><span class="icon mdi mdi-download"></span></span></a>
                    <span class="icon mdi mdi-more-vert"></span>
                </div>
            </div>
            <div class="panel-body">
                <table id="user_table" class="table table-striped table-hover table-fw-widget">
                    <thead>

                        <tr>
                            <th>Name</th>
                            <th>User Role</th>
                            <th>Date Created</th>
                            <th>Date Last Logged In</th>
                            <th>Manage</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($userdata as $user_value) {
                            ?>
                            <tr class="odd gradeX">
                                <td><?php echo $user_value->first_name . ' ' . $user_value->last_name; ?></td>
                                <td><?php if($user_value->role==$role_merchant){ echo "Store Owner";} else if($user_value->role==$role_merchant_user){ echo "Store Administrator";} ?></td>
                                <td><?php echo $user_value->created_at; ?></td>
                                <td><?php echo $user_value->date_last_login; ?></td>
                                <td class="center">
                                    <?=
                                    Html::a('', ['delete', 'class' => "icon", 'id' => $user_value->id], [
                                        'class' => 'mdi mdi-delete',
                                        'data' => [
                                            'confirm' => 'Are you sure you want to delete this item?',
                                            'method' => 'post',
                                        ],
                                    ]);
                                    ?>
                                    <?php
                                    if($user_value->parent_id == '') {
                                        echo Html::a('', ['/profile'], [
                                        // Url::to(['user/delete', 'id' => $user_value->id]),

                                        'class' => 'mdi mdi-edit',

                                    ]);
                                    } else {
                                    echo Html::a('', ['user/update/', 'id' => $user_value->id], [
                                        // Url::to(['user/delete', 'id' => $user_value->id]),

                                        'class' => 'mdi mdi-edit',

                                    ]);
                                }
                                    ?>
                                    
                                </td>
                            </tr>
                            <?php
                        } 
                        ?>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>










