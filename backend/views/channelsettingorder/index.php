<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use backend\models\Channels;
use backend\models\User;
use backend\models\StoresConnection;
use backend\models\Stores;
use yii\db\Query;
use backend\models\ChannelConnection;
use backend\models\Fulfillment;
use backend\models\FulfillmentList;
use backend\models\Channelsetting;

$this->title = 'Channel Settings';
$this->params['breadcrumbs'][] = 'Channel Settings';
$users_Id = Yii::$app->user->identity->id;
$connectedChannelID = $_GET['id'];
$fulfilled_data = Channelsetting::find()->where(['user_id' => $users_Id, 'channel_id' => $connectedChannelID, 'setting_key' =>'fulfillmentID'])->one();
//echo '<per>'; print_r($fulfilled_data); echo '</pre>';
$currency_setting = Channelsetting::find()->where(['user_id' => $users_Id, 'channel_id' => $connectedChannelID, 'setting_key' => 'currency'])->one();
if (isset($_GET['type']) and !empty($_GET['type']) and $_GET['type'] == 'store') {
    $currency_setting = Channelsetting::find()->where(['user_id' => $users_Id, 'store_id' => $connectedChannelID, 'setting_key' => 'currency'])->one();
}

$arr = array();
$store_connection = StoresConnection::find()->where(['user_id' => $users_Id])->all();
$channel_connection = ChannelConnection::find()->where(['user_id' => $users_Id, 'graph_status' => 'yes'])->all();
$connections = array();
foreach ($store_connection as $con) {
    $store = Stores::find()->select(['store_name', 'store_image'])->where(['store_id' => $con->store_id])->one();
    $arr[] = $con->store_id;
}
foreach ($channel_connection as $con1) {
    $channel = Channels::find()->select(['channel_name', 'channel_image', 'parent_name'])->where(['channel_id' => $con1->channel_id])->one();
    $p_name = explode("_", $channel->parent_name);
    $name = ucwords(str_replace("-", " ", $p_name[1]));
    $arr[] = $con1->channel_id;
}
$connectedChannelID = $_GET['id'];
if (!in_array($connectedChannelID, $arr)) {
    header('Location: /index');
    exit;
}
/* For Checking Google shopping Is enabled or Not */
$user_id = Yii::$app->user->identity->id;
$domain_name = Yii::$app->user->identity->domain_name;
/* Connectivity For Main database */
?>
<div class="page-head">
    <h2 class="page-head-title">Channel Settings</h2>
    <ol class="breadcrumb page-head-nav">
        <?php
        echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
        ?>
    </ol>
</div>

<div class="main-content container-fluid">
    <!--Tabs-->
    <div class="row">
        <!--Default Tabs-->
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">Settings</div>
                <div class="tab-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#home" data-toggle="tab">General</a></li>
<!--                        <li><a href="#additionalnote" data-toggle="tab">Additional Details</a></li>-->
                        <!-- <li><a href="#paymentmethod" data-toggle="tab">Payment Methods</a></li>-->
                        <li><a href="#Fulfillment" data-toggle="tab">Fulfillment</a></li>
                        <li><a href="#Translations" data-toggle="tab">Translations</a></li>
                        <li><a href="#Performance" data-toggle="tab">Performance</a></li>
                        <li><a href="#Currency" data-toggle="tab">Currency Setting</a></li>
                        <!--  <li><a href="#Reports" data-toggle="tab">Reports</a></li>-->
                    </ul>
                    <div class="tab-content">
                        <div id="home" class="tab-pane active cont">
                            <div class="main-content container-fluid">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel panel-default panel-table">
                                            <div class="panel-heading">Disable Channels
                                             <!-- <div class="tools"><span class="icon mdi mdi-download"></span><span class="icon mdi mdi-more-vert"></span></div> -->
                                            </div>
                                            <div class="panel-body">


                                                <?php
                                                $all_conntection = StoresConnection::find()->where(['store_id' => $connectedChannelID])->all();
                                                //
                                                $all_channels = ChannelConnection::find()->where(['channel_id' => $connectedChannelID])->all();

                                                $cnt = 0;
                                                $arr1 = array();
                                                if (!empty($all_conntection)) {
                                                    foreach ($all_conntection as $conn) {
                                                        $arr1[$cnt]['connection_id'] = $conn->stores_connection_id;
                                                        $arr1[$cnt]['big_api_path'] = $conn->big_api_path;
                                                        $arr1[$cnt]['big_access_token'] = $conn->big_access_token;
                                                        $arr1[$cnt]['big_client_id'] = $conn->big_client_id;
                                                        $arr1[$cnt]['big_client_secret'] = $conn->big_client_secret;
                                                        $arr1[$cnt]['big_store_hash'] = $conn->big_store_hash;
                                                        $store = Stores::find()->select(['store_name', 'store_image'])->where(['store_id' => $conn->store_id])->one();

                                                        $arr1[$cnt]['store_name'] = $store->store_name;
                                                        $arr1[$cnt]['store_image'] = $store->store_image;
                                                    }
                                                }

                                                $cnt1 = 0;
                                                $arr2 = array();
                                                if (!empty($all_channels)) {
                                                    foreach ($all_channels as $channel) {
                                                        $arr2[$cnt]['connection_id'] = $channel->channel_connection_id;
                                                        $arr2[$cnt]['username'] = $channel->username;
                                                        $arr2[$cnt]['password'] = $channel->password;
                                                        $chnl = Channels::find()->select(['channel_name', 'channel_image', 'parent_name'])->where(['channel_id' => $channel->channel_id])->one();

                                                        $arr2[$cnt]['channel_name'] = $chnl->channel_name;
                                                        $arr2[$cnt]['channel_image'] = $chnl->channel_image;
                                                        $arr2[$cnt]['parent_name'] = $chnl->parent_name;
                                                    }
                                                }
                                                $arr = array("store" => $arr1, "channel" => $arr2);
                                                /* return $this->render('channeldisable', [
                                                  'connections' => $arr,
                                                  ]); */
                                                //echo '<pre>'; print_r($arr); echo '</pre>'; die('dsf');
                                                if (!empty($arr)) {
                                                    foreach ($arr["store"] as $conn) {
                                                        ?>  


                                                        <label class="col-sm-3 control-label panel-heading"><?php echo $conn['store_name']; ?></label>

                                                        <div class="switch-button switch-button-lg <?php echo str_replace(" ", "", $conn['store_name']); ?>">
                                                            <input type="checkbox" checked="" name="swt4" id="swt4"><span>
                                                                <label for="swt4" class="chnl_dsbl" data-connid="<?php echo $conn['connection_id']; ?>" data-cname="<?php echo $conn['store_name']; ?>" data-type="store" data-cls="<?php echo str_replace(" ", "", $conn['store_name']); ?>"></label></span>
                                                        </div>
                                                        <?php if ($conn['store_name'] == 'BigCommerce') { ?> 
                                                            <div class="col-md-12">
                                                                <label class="col-sm-3 control-label panel-heading"><?php echo 'API Path'; ?></label>
                                                                <input type="text" class="form-control" style="width:60%"  disabled="disabled" value="<?php echo $conn['big_api_path']; ?>" />
                                                            </div>
                                                            <div class="col-md-12">
                                                                <label class="col-sm-3 control-label panel-heading"><?php echo 'Access Token'; ?></label>
                                                                <input type="text" class="form-control" style="width:60%"  disabled="disabled" value="<?php echo $conn['big_access_token']; ?>" />
                                                            </div>
                                                            <div class="col-md-12">
                                                                <label class="col-sm-3 control-label panel-heading"><?php echo 'Client ID'; ?></label>
                                                                <input type="text" class="form-control" style="width:60%"  disabled="disabled" value="<?php echo $conn['big_client_id']; ?>" />
                                                            </div>
                                                            <div class="col-md-12">
                                                                <label class="col-sm-3 control-label panel-heading"><?php echo 'Client Secret'; ?></label>
                                                                <input type="text" class="form-control" style="width:60%"  disabled="disabled" value="<?php echo $conn['big_client_secret']; ?>" />
                                                            </div>
                                                            <div class="col-md-12">
                                                                <label class="col-sm-3 control-label panel-heading"><?php echo 'Store hash'; ?></label>
                                                                <input type="text" class="form-control" style="width:60%"  disabled="disabled" value="<?php echo $conn['big_store_hash']; ?>" />
                                                            </div>
                                                        <?php } ?>




                                                        <?php
                                                    }
                                                    foreach ($arr["channel"] as $conn1) {
                                                        ?>  

                                     <!--td><img src="<?php //echo  $conn1['channel_image'];     ?>" class="ch_img"></td-->
                                                        <label class="col-sm-3 control-label panel-heading"><?php
                                                            $p_name = explode("_", $conn1['parent_name']);
                                                            echo $p_name[1] . " - " . $conn1['channel_name'];
                                                            ?></label>

                                                        <div class="switch-button switch-button-lg <?php echo str_replace(" ", "", $conn1['channel_name']); ?>">
                                                            <input type="checkbox" checked="" name="swt4" id="swt4"><span>
                                                                <label for="swt4" class="chnl_dsbl" data-connid="<?php echo $conn1['connection_id']; ?>" data-cname="<?php echo $p_name[1] . " - " . $conn1['channel_name']; ?>" data-type="channel" data-cls="<?php echo str_replace(" ", "", $conn1['channel_name']); ?>"></label></span>
                                                        </div>
                                                        <?php if ($p_name[1] == 'WeChat') { ?>
                                                            <div class="col-md-12">
                                                                <label class="col-sm-3 control-label panel-heading"><?php echo 'Username'; ?></label>
                                                                <input type="text" class="form-control" style="width:60%"  disabled="disabled" value="<?php echo $conn1['username']; ?>" />
                                                            </div>
                                                            <div class="col-md-12">
                                                                <label class="col-sm-3 control-label panel-heading"><?php echo 'Password'; ?></label>
                                                                <input type="password" class="form-control" style="width:60%"  disabled="disabled" value="<?php echo '***************' ?>" />
                                                            </div>
                                                            <?php
                                                        }
                                                        if ($conn1['channel_name'] == 'Facebook') {
                                                            $checkConnection = ChannelConnection::find()->Where(['user_id' => Yii::$app->user->identity->id, 'channel_id' => $connectedChannelID, 'connected' => 'yes'])->one();
                                                            if (isset($checkConnection) and !empty($checkConnection)) {
                                                                ?>      <div class="col-md-12">
                                                                    <label class="col-sm-3 control-label panel-heading">Feed URL :</label>
                                                                    <input type="text" readonly="readonly"  class="form-control" style="width:60%" disabled="disabled" value="<?php echo Yii::$app->params['BASE_URL'] . 'facebook/feed/?u_id=' . base64_encode(base64_encode(Yii::$app->user->identity->id)) ?>">
                                                                </div>  <?php
                                                            }
                                                        }
                                                    }
                                                }
                                                ?>
                                                <div id="disable-warning" tabindex="-1" role="dialog" class="modal fade">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="text-center">
                                                                    <div class="text-warning"><span class="modal-main-icon mdi mdi-alert-triangle"></span></div>
                                                                    <h3>Warning!</h3>
                                                                    <p>Are you sure, you want to disable your <span class="modalmsg">Store/</span> channel? If you do so, no data will sync until re-enabled.</p>
                                                                    <div class="xs-mt-50">
                                                                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default cacnle_btn">Cancel</button>
                                                                        <button type="button" class="btn btn-space btn-warning proceed_todlt" data-connid="">Proceed</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer"></div>
                                                        </div>
                                                    </div>
                                                </div> 


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
<!--                        <div id="additionalnote" class="tab-pane cont">
                            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima praesentium laudantium ipsa, enim maxime placeat, dolores quos sequi nisi iste velit perspiciatis rerum eveniet voluptate laboriosam perferendis ipsum. Expedita, maiores.</p>
                            <p> Consectetur adipisicing elit. Minima praesentium laudantium ipsa, enim maxime placeat, dolores quos sequi nisi iste velit perspiciatis rerum eveniet voluptate laboriosam perferendis ipsum. Expedita, maiores.</p>
                        </div>-->
                        <!--  <div id="paymentmethod" class="tab-pane">
                            <p>Consectetur adipisicing elit. Ipsam ut praesentium, voluptate quidem necessitatibus quam nam officia soluta aperiam, recusandae.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos facilis laboriosam, vitae ipsum tenetur atque vel repellendus culpa reiciendis velit quas, unde soluta quidem voluptas ipsam, rerum fuga placeat rem error voluptate eligendi modi. Delectus, iure sit impedit? Facere provident expedita itaque, magni, quas assumenda numquam eum! Sequi deserunt, rerum.</p><a href="#">Read more  </a>
                          </div>-->
                        <div id="Fulfillment" class="tab-pane">
                            <div class="panel-body">
                                <form action="#" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label panel-heading">Enable Fulfillment</label>

                                        <div class="col-sm-7 xs-pt-5">
                                            <div class="switch-button switch-button-success">
                                                <input <?php
                                                if (!empty($fulfilled_data)) {
                                                    echo 'checked=""';
                                                }
                                                ?>  type="checkbox" name="channelfulfillment" id="channelfulfillment"><span>
                                                    <label for="channelfulfillment"></label></span>
                                                <input type="hidden" value="<?php echo $_GET['id'] ?>" id="channelId" />
                                            </div>
                                        </div>
                                        <div class="col-sm-7 xs-pt-5">
                                            <select class="form-control input-sm" id="fulfillmentlist" name="fulfillmentlist" <?php
                                            if (empty($fulfilled_data)) {
                                                echo 'style="display:none;"';
                                            }
                                            ?>  >
                                                <option value="">Select Fulfillment</option>
                                                <?php
                                                $FulFillmentList = FulfillmentList::find()->all();
                                                foreach ($FulFillmentList as $fulfillment) {
                                                    ?>
                                                    <option <?php
                                                    if (!empty($fulfilled_data) && $fulfilled_data->setting_value == $fulfillment->id) {
                                                        echo "SELECTED";
                                                    }
                                                    ?> value="<?php echo $fulfillment->id; ?>"><?php echo $fulfillment->fulfillment_name; ?></option>
                                                    <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </form>



                                <div id="disable-warning-fulfilled" tabindex="-1" role="dialog" class="modal fade">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="text-center">
                                                    <div class="text-warning"><span class="modal-main-icon mdi mdi-alert-triangle"></span></div>
                                                    <h3>Warning!</h3>
                                                    <p>Are you sure, you want to disable your <span class="modalmsg">Fulfillment</span> If you do so, no data will sync until re-enabled.</p>
                                                    <div class="xs-mt-50">
                                                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default cacnle_btn">Cancel</button>
                                                        <button type="button" class="btn btn-space btn-warning proceed_todlt_fulfillment" data-connid="">Proceed</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer"></div>
                                        </div>
                                    </div>
                                </div>



                            </div>

                        </div>
                        <?php
                        $users_Id = Yii::$app->user->identity->id;
                        $channelsettingval = Channelsetting::find()->where(['user_id' => $users_Id, 'setting_key' => 'Enable_Smartling'])->one();
                        // echo  '<pre>'; print_r($channelsettingval);
                       // echo $channelsettingval->setting_value;
                        if (!empty($channelsettingval)) {
                            ?>
                            <div id="Translations" class="tab-pane">
                                <!--<form action="#" style="border-radius: 0px;" class="form-horizontal group-border-dashed" id="smartling_form_id">-->

                                <!--<div class="form-group">
                                        <label class="col-sm-3 control-label">Smartling Translation File</label>
                                        <div class="col-sm-6">
                                                <input type="file" name="smart-2" id="smart-2" data-multiple-caption="{count} files selected" multiple="" class="inputfile">
                                                <label for="smart-2" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                                        </div>
                                </div>-->
                                <input type="hidden" value="<?php echo $_GET['id']; ?>" id="channel_id_translation" />
                                <div class="col-sm-12"> 
                                    <div class="panel-body">

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label panel-heading custom_Transs">Enable Smartling Translations</label>
                                            <div class="col-sm-6 xs-pt-5">
                                                <div class="switch-button switch-button-success">
                                                    <input type="checkbox" <?php if ($channelsettingval->setting_value == 'yes') {
                            echo "checked=''";
                        } ?>   name="channelsmartlingyes" id="channelsmartlingyes"><span>
                                                        <label for="channelsmartlingyes"></label></span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <?php
                                $channelsettingvalCat = Channelsetting::find()->where(['user_id' => $users_Id, 'setting_key' => 'Language'])->one();
                                //echo $channelsettingvalCat->setting_value;
                                ?>
                                <div class="col-sm-12">
                                    <div class="panel-body">
                                        <label class="control-label">Choose your translation Language</label>
                                        <p>Translation Services being used by Smartling</p>
                                        <select class="select2" id="selectTranslationsetting">
                                            <optgroup label="Translations Language">
                                                <option <?php if ($channelsettingvalCat->setting_value == 'en-US') {
                                    echo "SELECTED";
                                } ?>  value="en-US">en-US</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'en-CA') {
                                    echo "SELECTED";
                                } ?> value="en-CA">en-CA</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'en-UK') {
                                    echo "SELECTED";
                                } ?> value="en-UK">en-UK</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'fr-CA') {
                                    echo "SELECTED";
                                } ?> value="fr-CA">fr-CA</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'fr-FR') {
                                    echo "SELECTED";
                                } ?> value="fr-FR">fr-FR</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'es-ES') {
                                    echo "SELECTED";
                                } ?> value="es-ES">es-ES</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'es-LA') {
                                    echo "SELECTED";
                                } ?> value="es-LA">es-LA</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'it-IT') {
                                    echo "SELECTED";
                                } ?> value="it-IT">it-IT</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'de-DE') {
                                    echo "SELECTED";
                                } ?> value="de-DE">de-DE</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'nl-NL') {
                                    echo "SELECTED";
                                } ?>  value="nl-NL">nl-NL</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'pl-PL') {
                                echo "SELECTED";
                            } ?> value="pl-PL">pl-PL</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'da-DK') {
                                echo "SELECTED";
                            } ?> value="da-DK">da-DK</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'fi-FI') {
                                echo "SELECTED";
                            } ?> value="fi-FI">fi-FI</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'nb-NO') {
                                echo "SELECTED";
                            } ?> value="nb-NO">nb-NO</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'sv-SE') {
                                echo "SELECTED";
                            } ?> value="sv-SE">sv-SE</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'ja-JP') {
                                echo "SELECTED";
                            } ?> value="ja-JP">ja-JP</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'zh-CN') {
                                echo "SELECTED";
                            } ?> value="zh-CN">zh-CN</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'zh-HK') {
                                echo "SELECTED";
                            } ?> value="zh-HK">zh-HK</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'zh-TW') {
                                echo "SELECTED";
                            } ?> value="zh-TW">zh-TW</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'hi-IN') {
                                echo "SELECTED";
                            } ?> value="hi-IN">hi-IN</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'id-ID') {
                                echo "SELECTED";
                            } ?> value="id-ID">id-ID</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'th-TH') {
                                echo "SELECTED";
                            } ?> value="th-TH">th-TH</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'vi-VN') {
                                echo "SELECTED";
                            } ?> value="vi-VN">vi-VN</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'ko-KR') {
                                echo "SELECTED";
                            } ?> value="ko-KR">ko-KR</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'ar-EG') {
                                echo "SELECTED";
                            } ?> value="ar-EG">ar-EG</option>
                                            </optgroup>
                                        </select>
                                    </div>

                                </div>
    <?php
    $channelsettingvallang = Channelsetting::find()->where(['user_id' => $users_Id, 'setting_key' => 'Enable_Categories'])->one();
    ?>
                                <div class="col-sm-12">
                                    <div class="be-checkbox">

                                        <input type="checkbox"  value="bar" <?php if ($channelsettingvallang->setting_value == 'yes') {
        echo "checked=''";
    } ?> id="channeltranslationcat" data-parsley-multiple="group1" data-parsley-errors-container="#error-container2">
                                        <label class="col-sm-3 control-label panel-heading" for="channeltranslationcat">Category Translation</label>

                                    </div>
                                </div>



                                <button type="submit" class="btn btn-space btn-primary custom_Button_Class" id="translation_corporate_id">Submit</button>
                                <div class="form-group">

                                </div>

                                <!--</form>-->
                            </div>
<?php } else {
    ?>
                            <div id="Translations" class="tab-pane">
                                <!--<form action="#" style="border-radius: 0px;" class="form-horizontal group-border-dashed" id="smartling_form_id">-->

                                <!--<div class="form-group">
                                        <label class="col-sm-3 control-label">Smartling Translation File</label>
                                        <div class="col-sm-6">
                                                <input type="file" name="smart-2" id="smart-2" data-multiple-caption="{count} files selected" multiple="" class="inputfile">
                                                <label for="smart-2" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                                        </div>
                                </div>-->
                                <input type="hidden" value="<?php echo $_GET['id']; ?>" id="channel_id_translation" />
                                <div class="col-sm-12"> 
                                    <div class="panel-body">

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label panel-heading custom_Transs">Enable Smartling Translations</label>
                                            <div class="col-sm-6 xs-pt-5">
                                                <div class="switch-button switch-button-success">
                                                    <input type="checkbox" <?php //if($smartlingData->connected == 'yes') { echo "checked=''"; }  ?>   name="channelsmartlingyes" id="channelsmartlingyes"><span>
                                                        <label for="channelsmartlingyes"></label></span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="panel-body">
                                        <label class="control-label">Choose your translation Language</label>
                                        <p>Translation Services being used by Smartling</p>
                                        <select class="select2" id="selectTranslationsetting">
                                            <optgroup label="Translations Language">
                                                <option  value="en-US">en-US</option>
                                                <option  value="en-CA">en-CA</option>
                                                <option value="en-UK">en-UK</option>
                                                <option value="fr-CA">fr-CA</option>
                                                <option value="fr-FR">fr-FR</option>
                                                <option value="es-ES">es-ES</option>
                                                <option value="es-LA">es-LA</option>
                                                <option value="it-IT">it-IT</option>
                                                <option value="de-DE">de-DE</option>
                                                <option value="nl-NL">nl-NL</option>
                                                <option value="pl-PL">pl-PL</option>
                                                <option value="da-DK">da-DK</option>
                                                <option value="fi-FI">fi-FI</option>
                                                <option value="nb-NO">nb-NO</option>
                                                <option value="sv-SE">sv-SE</option>
                                                <option value="ja-JP">ja-JP</option>
                                                <option value="zh-CN">zh-CN</option>
                                                <option value="zh-HK">zh-HK</option>
                                                <option value="zh-TW">zh-TW</option>
                                                <option value="hi-IN">hi-IN</option>
                                                <option value="id-ID">id-ID</option>
                                                <option value="th-TH">th-TH</option>
                                                <option value="vi-VN">vi-VN</option>
                                                <option value="ko-KR">ko-KR</option>
                                                <option value="ar-EG">ar-EG</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="be-checkbox">

                                        <input type="checkbox" value="bar" id="channeltranslationcat" data-parsley-multiple="group1" data-parsley-errors-container="#error-container2">
                                        <label class="col-sm-3 control-label panel-heading" for="channeltranslationcat">Category Translation</label>

                                    </div>
                                </div>



                                <button type="submit" class="btn btn-space btn-primary custom_Button_Class" id="translation_corporate_id">Submit</button>
                                <div class="form-group">

                                </div>

                                <!--</form>-->
                            </div>
<?php } ?>
                        <div id="Performance" class="tab-pane">
                            <p>Consectetur adipisicing elit. Ipsam ut praesentium, voluptate quidem necessitatibus quam nam officia soluta aperiam, recusandae.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos facilis laboriosam, vitae ipsum tenetur atque vel repellendus culpa reiciendis velit quas, unde soluta quidem voluptas ipsam, rerum fuga placeat rem error voluptate eligendi modi. Delectus, iure sit impedit? Facere provident expedita itaque, magni, quas assumenda numquam eum! Sequi deserunt, rerum.</p><a href="#">Read more  </a>
                        </div>
                        <div id="Currency" class="tab-pane">
                            <p><b> Channel Default Currency</b></p>

                            <span width="65%"><a id="channel_CurrencyPreference" href="javascript:" class="editable editable-click editable-empty" data-type="select" data-value="<?php echo (!empty($currency_setting->setting_value)) ? $currency_setting->setting_value : 'USD' ?>" data-title="Enter Default Currency"><?php echo (!empty($currency_setting->setting_value)) ? $currency_setting->setting_value : 'USD' ?></a></span>
                        </div>
                        <!-- <div id="Reports" class="tab-pane">
      <p>Consectetur adipisicing elit. Ipsam ut praesentium, voluptate quidem necessitatibus quam nam officia soluta aperiam, recusandae.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos facilis laboriosam, vitae ipsum tenetur atque vel repellendus culpa reiciendis velit quas, unde soluta quidem voluptas ipsam, rerum fuga placeat rem error voluptate eligendi modi. Delectus, iure sit impedit? Facere provident expedita itaque, magni, quas assumenda numquam eum! Sequi deserunt, rerum.</p><a href="#">Read more  </a>
    </div>-->
                    </div>
                </div>
            </div>
        </div>
        <!--Success Tabs-->

        <!--Success Tabs-->

    </div>
    <!--Accordions-->

</div>
<div tabindex="-1" role="dialog" class="modal fade in channelfulfillment-success" style="display: none; background: rgba(0,0,0,0.6);">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close channelfulfillment_modal_close"></span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="text-success"><span class="modal-main-icon mdi mdi-check"></span></div>
                    <h3 id="ajax_header_msg">Success!</h3>
                    <p id="magento_ajax_msg">Current Channel/Store order will fulfilled by SF Expres.</p>
                    <div class="xs-mt-50">
                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default channelfulfillment_modal_close">Close</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

<div tabindex="-1" role="dialog" class="modal fade in channelfulfillment-success-not-found" style="display: none; background: rgba(0,0,0,0.6);">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close channelfulfillment_modal_close"></span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
                   <h3 id="ajax_header_msg">Error!</h3>
                    <p id="magento_ajax_msg">No Fullfillment found !</p>
                    <div class="xs-mt-50">
                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default channelfulfillment_modal_close">Close</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>



<div tabindex="-1" role="dialog" class="modal fade in translatoinfulfillment-success" style="display: none; background: rgba(0,0,0,0.6);">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close translatoinfulfillment_modal_close"></span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="text-success"><span class="modal-main-icon mdi mdi-check"></span></div>
                    <h3 id="ajax_header_msg">Success!</h3>
                    <p id="magento_ajax_msg">Your Translations Setting Saved!</p>
                    <div class="xs-mt-50">
                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default translatoinfulfillment_modal_close">Close</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>









<div id="mod-danger" tabindex="-1" role="dialog" class="modal fade in channelfulfillment-error" style="display: none; background: rgba(0,0,0,0.6);">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close channelfulfillment_error_modal_close"></span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
                    <h3 id='ajax_header_error_msg'>Success!</h3>
                    <p id="magento_ajax_msg_eror">Current Channel/Store order will not fulfilled by SF Expres.</p>
                    <div class="xs-mt-50">
                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default channelfulfillment_error_modal_close">Close</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
