<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\Breadcrumbs;
use backend\models\Variations;
use backend\models\VariationsItemList;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Product Variations';
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['/products']];
$this->params['breadcrumbs'][] = ['label' => 'Variations', 'url' => ['/variations']];
$this->params['breadcrumbs'][] = 'View All';
?>
<div class="page-head">
    <h2 class="page-head-title"><?= Html::encode($this->title) ?></h2>
    <ol class="breadcrumb page-head-nav">
        <?php
        echo Breadcrumbs::widget([
          'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
        ?>
    </ol>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default panel-table">
            <div class="panel-heading"><?php echo $this->title; ?>
                <div class="tools"><span class="icon mdi mdi-download"></span><span class="icon mdi mdi-more-vert"></span></div>
            </div>
            <div class="panel-body">
                <table id="variations_table" class="table table-striped table-hover table-fw-widget">
                    <thead>
                        <tr>
                            <th>Variation Name</th>
                            <th>Label</th>
                            <th>Items In Variation</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($variations as $variation): ?>
                          <tr>
                              <td><?= $variation->variation_name ?></td>
                              <td><?= $variation->variation_description ?></td>
                              <?php
                              $variation_items = VariationsItemList::find()->select('item_name')->where(['variation_id' => $variation->variations_ID])->all();
                              $items = '';
                              if (!empty($variation_items)):
                                foreach ($variation_items as $variation_item):
                                  $item_name = $variation_item->item_name;
                                  $items = $items . $item_name . ',';
                                endforeach;
                              endif;
                              ?>
                              <td><?= $items ?></td>
                          </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>






