<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use backend\models\Channels;
use backend\models\User;
use backend\models\StoresConnection;
use backend\models\Stores;
use yii\db\Query;
use backend\models\ChannelConnection;
use backend\models\Fulfillment;
use backend\models\FulfillmentList;
use backend\models\Channelsetting;
use backend\models\StoreDetails;
use backend\models\Content;
use backend\models\Orders;
use backend\models\CustomerUser;
use backend\models\CurrencySymbols;
use backend\models\Smartling;
use backend\models\SmartlingPrice;


$current_store_channel_id = '';
$current_channel_type = '';
$store_detail_id = '';
$store_country_name = '';
$channel_store_name = '';
$import_status='';
$loader_class='';

if(isset($_GET['id'])){
    $current_store_channel_id = $_GET['id'];
}
if(isset($_GET['type'])){
    $current_channel_type = $_GET['type'];
}
if(isset($_GET['mul_store_id'])){
    $store_detail_id = $_GET['mul_store_id'];
    $store_details = StoreDetails::find()->where(['store_connection_id' => $store_detail_id])->with('storeConnection')->one();
    if(!empty($store_details)){
        $import_status=$store_details->storeConnection->import_status;
        $store_country_name = $store_details->country;
    }
    //$store_country_detail = 
}

if($current_channel_type == 'store'){
    $store_details = Stores::find()->where(['store_id' => $current_store_channel_id])->one();
    $channel_store_name = $store_details->store_name;
    $performance_order = Orders::find()->Where(['mul_store_id' => $store_detail_id])->with('customer')->orderBy(['created_at' => SORT_DESC,])->limit(5)->all();
    
}
if($current_channel_type == 'channel'){
    $channel_details = Channels::find()->where(['channel_ID' => $current_store_channel_id])->with('channelconnections')->one();
    $channel_store_name = $channel_details->channel_name;
    if($channel_store_name=='Service Account' || $channel_store_name=='Subscription Account'){
        $channel_store_name = 'WeChat';
    }
    $performance_order = Orders::find()->Where(['channel_accquired' => $channel_store_name])->with('customer')->orderBy(['created_at' => SORT_DESC,])->limit(5)->all();
    $import_status = $channel_details->channelconnections[0]->import_status;
}

    if(strtolower($import_status)==strtolower('Completed-Read') || strtolower($import_status)==strtolower('Completed')){
        $loader_class='';
    }else{
        $loader_class='be-loading be-loading-active';
    } 
//For perfomence tab latest order

$performance_order = Orders::find()->where(['channel_accquired' => $channel_store_name])->orderBy(['created_at' => SORT_DESC])->limit(5)->all();



$this->title = 'Connected Channel - '.$channel_store_name.' '.$store_country_name;
$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => 'javascript: void(0)', 'class' => 'non_link'];
$this->params['breadcrumbs'][] = ['label' => 'Channels', 'url' => ['/index']];
$this->params['breadcrumbs'][] = ['label' => 'Connected Channels', 'url' => 'javascript: void(0)', 'class' => 'non_link'];
$this->params['breadcrumbs'][] = $channel_store_name.' '.$store_country_name;
//$this->params['breadcrumbs'][] = '<span style="color: #4285f4">'.$channel_store_name.' '.$store_country_name.'</span>';
$users_Id = Yii::$app->user->identity->id;
$connectedChannelID = $_GET['id'];
$connectedStoreID = $_GET['mul_store_id'];
$fulfilled_data = Channelsetting::find()->where(['user_id' => $users_Id, 'channel_id' => $connectedChannelID, 'setting_key' =>'fulfillmentID'])->one();
//echo 'Front<pre>'; print_r($fulfilled_data); echo '</pre>';
$currency_setting = Channelsetting::find()->where(['user_id' => $users_Id, 'channel_id' => $connectedChannelID, 'setting_key' => 'currency'])->one();
if (isset($_GET['type']) and !empty($_GET['type']) and $_GET['type'] == 'store') {
    $currency_setting = Channelsetting::find()->where(['user_id' => $users_Id, 'mul_store_id' => $connectedStoreID, 'setting_key' => 'currency'])->one();
}

$arr = array();
$chl = array();
$store_connection = StoresConnection::find()->where(['user_id' => $users_Id])->all();
$channel_connection = ChannelConnection::find()->where(['user_id' => $users_Id, 'graph_status' => 'yes'])->all();
$connections = array();
foreach ($store_connection as $con) {
    $store = StoreDetails::find()->select(['store_name', 'channel_accquired','country','country_code'])->where(['store_connection_id' => $con->stores_connection_id])->one();
    $arr[] = $con->store_id;
}
foreach ($channel_connection as $con1) {
    $channel = Channels::find()->select(['channel_name', 'channel_image', 'parent_name'])->where(['channel_id' => $con1->channel_id])->one();
    $p_name = explode("_", $channel->parent_name);
    $name = ucwords(str_replace("-", " ", $p_name[1]));
    $arr[] = $con1->channel_id;
    $chl[] = $con1->channel_id;
}
$connectedChannelID = $_GET['id'];

if (!in_array($connectedChannelID, $arr)) {
    header('Location: /index');
    exit;
}
/* For Checking Google shopping Is enabled or Not */
$user_id = Yii::$app->user->identity->id;
$domain_name = Yii::$app->user->identity->domain_name;
/* Connectivity For Main database */


//Get Content
 $content_list = Content::find()->where(['elliot_user_id' => $user_id])->all();
// echo "<pre>";
// print_r($content_list);
// echo "</pre>";
?>
<div class="page-head">
    <h2 class="page-head-title"><?php echo 'Connected Channel - '.$channel_store_name.' '.$store_country_name; ?></h2>
    <ol class="breadcrumb page-head-nav">
        <?php
        echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
        ?>
    </ol>
</div>

<div class="main-content container-fluid be-loading" id="channel_disable">
    <!--Tabs-->
    <div class="row">
        <!--Default Tabs-->
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">Settings</div>
                <div class="tab-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#home" data-toggle="tab">General</a></li>
                        <!--li><a href="#additionalnote" data-toggle="tab">Additional Details</a></li-->
                        <!-- <li><a href="#paymentmethod" data-toggle="tab">Payment Methods</a></li>-->
						<?php //if(!empty($chl)){ ?>
                        <li><a href="#Fulfillment" data-toggle="tab">Fulfillment</a></li>
						<?php //} ?>
                        <li><a href="#Translations" data-toggle="tab">Translations</a></li>
                        <li><a href="#Performance" data-toggle="tab">Performance</a></li>
                        <li><a href="#Currency" data-toggle="tab">Currency Setting</a></li>
                        <!--  <li><a href="#Reports" data-toggle="tab">Reports</a></li>-->
                    </ul>
                    <div class="tab-content col-sm-12">
                        <div id="home" class="tab-pane active cont <?=$loader_class?>">
                            <div class="main-content container-fluid">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel panel-default panel-table">
                                            <div class="panel-heading">Disable Channels
                                             <!-- <div class="tools"><span class="icon mdi mdi-download"></span><span class="icon mdi mdi-more-vert"></span></div> -->
                                            </div>
                                            <div class="panel-body">


                                                <?php
                                                $all_conntection = StoresConnection::find()->where(['stores_connection_id' => $connectedStoreID])->all();
                                                //
                                                $all_channels = ChannelConnection::find()->where(['channel_id' => $connectedChannelID])->all();

                                                $cnt = 0;
                                                $arr1 = array();
                                                if (!empty($all_conntection)) {
                                                    foreach ($all_conntection as $conn) {
                                                        $arr1[$cnt]['connection_id'] = $conn->stores_connection_id;
                                                        $arr1[$cnt]['big_api_path'] = $conn->big_api_path;
                                                        $arr1[$cnt]['big_access_token'] = $conn->big_access_token;
                                                        $arr1[$cnt]['big_client_id'] = $conn->big_client_id;
                                                        $arr1[$cnt]['big_client_secret'] = $conn->big_client_secret;
                                                        $arr1[$cnt]['big_store_hash'] = $conn->big_store_hash;
                                                        $store = Stores::find()->select(['store_name', 'store_image'])->where(['store_id' => $conn->store_id])->one();

                                                        $arr1[$cnt]['store_name'] = $store->store_name;
                                                        $arr1[$cnt]['store_image'] = $store->store_image;
                                                    }
                                                }

                                                $cnt1 = 0;
                                                $arr2 = array();
                                                if (!empty($all_channels)) {
                                                    foreach ($all_channels as $channel) {
                                                        $arr2[$cnt]['connection_id'] = $channel->channel_connection_id;
                                                        $arr2[$cnt]['username'] = $channel->username;
                                                        $arr2[$cnt]['password'] = $channel->password;
                                                        $chnl = Channels::find()->select(['channel_name', 'channel_image', 'parent_name'])->where(['channel_id' => $channel->channel_id])->one();

                                                        $arr2[$cnt]['channel_name'] = $chnl->channel_name;
                                                        $arr2[$cnt]['channel_image'] = $chnl->channel_image;
                                                        $arr2[$cnt]['parent_name'] = $chnl->parent_name;
                                                    }
                                                }
                                                $arr = array("store" => $arr1, "channel" => $arr2);
                                                /* return $this->render('channeldisable', [
                                                  'connections' => $arr,
                                                  ]); */
                                                //echo '<pre>'; print_r($arr); echo '</pre>'; die('dsf');
                                                if (!empty($arr)) {
                                                    foreach ($arr["store"] as $conn) {
                                                        ?>  
                                                        <label class="col-sm-3 control-label panel-heading"><?php echo $conn['store_name']; ?></label>

                                                        <div class="TransLable switch-button switch-button-lg <?php echo str_replace(" ", "", $conn['store_name']); ?>">
                                                            <input type="checkbox" checked="" name="swt4" id="swt4"><span>
                                                                <label for="swt4" class="chnl_dsbl" data-connid="<?php echo $conn['connection_id']; ?>" data-cname="<?php echo $conn['store_name']; ?>" data-type="store" data-cls="<?php echo str_replace(" ", "", $conn['store_name']); ?>"></label></span>
                                                        </div>
                                                        <?php if ($conn['store_name'] == 'BigCommerce') { ?> 
                                                            <div class="col-md-12">
                                                                <label class="col-sm-3 control-label panel-heading"><?php echo 'API Path'; ?></label>
                                                                <input type="text" class="form-control" style="width:60%"  disabled="disabled" value="<?php echo $conn['big_api_path']; ?>" />
                                                            </div>
                                                            <div class="col-md-12">
                                                                <label class="col-sm-3 control-label panel-heading"><?php echo 'Access Token'; ?></label>
                                                                <input type="text" class="form-control" style="width:60%"  disabled="disabled" value="<?php echo $conn['big_access_token']; ?>" />
                                                            </div>
                                                            <div class="col-md-12">
                                                                <label class="col-sm-3 control-label panel-heading"><?php echo 'Client ID'; ?></label>
                                                                <input type="text" class="form-control" style="width:60%"  disabled="disabled" value="<?php echo $conn['big_client_id']; ?>" />
                                                            </div>
                                                            <div class="col-md-12">
                                                                <label class="col-sm-3 control-label panel-heading"><?php echo 'Client Secret'; ?></label>
                                                                <input type="text" class="form-control" style="width:60%"  disabled="disabled" value="<?php echo $conn['big_client_secret']; ?>" />
                                                            </div>
                                                            <div class="col-md-12">
                                                                <label class="col-sm-3 control-label panel-heading"><?php echo 'Store hash'; ?></label>
                                                                <input type="text" class="form-control" style="width:60%"  disabled="disabled" value="<?php echo $conn['big_store_hash']; ?>" />
                                                            </div>
                                                        <?php } ?>




                                                        <?php
                                                    }
                                                    foreach ($arr["channel"] as $conn1) {
                                                        ?>  

                                     <!--td><img src="<?php //echo  $conn1['channel_image'];     ?>" class="ch_img"></td-->
                                                        <label class="col-sm-3 control-label panel-heading"><?php
                                                            $p_name = explode("_", $conn1['parent_name']);
                                                            if ($p_name[1] == 'WeChat'){
                                                                echo $p_name[1] . " - " . $conn1['channel_name'];
                                                            }
                                                            else{
                                                                echo $conn1['channel_name'];
                                                            }
                                                            ?></label>

                                                        <div class="TransLable switch-button switch-button-lg <?php echo str_replace(" ", "", $conn1['channel_name']); ?>">
                                                            <input type="checkbox" checked="" name="swt4" id="swt4"><span>
                                                                <label for="swt4" class="chnl_dsbl" data-connid="<?php echo $conn1['connection_id']; ?>" data-cname="<?php echo $p_name[1] . " - " . $conn1['channel_name']; ?>" data-type="channel" data-cls="<?php echo str_replace(" ", "", $conn1['channel_name']); ?>"></label></span>
                                                        </div>
                                                        <?php if ($p_name[1] == 'WeChat') { ?>
                                                            <div class="col-md-12">
                                                                <label class="col-sm-3 control-label panel-heading"><?php echo 'Username'; ?></label>
                                                                <input type="text" class="form-control" style="width:60%"  disabled="disabled" value="<?php echo $conn1['username']; ?>" />
                                                            </div>
                                                            <div class="col-md-12">
                                                                <label class="col-sm-3 control-label panel-heading"><?php echo 'Password'; ?></label>
                                                                <input type="password" class="form-control" style="width:60%"  disabled="disabled" value="<?php echo '***************' ?>" />
                                                            </div>
                                                            <?php
                                                        }
                                                        if ($conn1['channel_name'] == 'Facebook') {
                                                            $checkConnection = ChannelConnection::find()->Where(['user_id' => Yii::$app->user->identity->id, 'channel_id' => $connectedChannelID, 'connected' => 'yes'])->one();
                                                            if (isset($checkConnection) and !empty($checkConnection)) {
                                                                ?>      <div class="col-md-12">
                                                                    <label class="col-sm-3 control-label panel-heading custom_Transs">Feed URL :</label>
                                                                    <input type="text" readonly="readonly"  class="form-control" style="width:60%" disabled="disabled" value="<?php echo Yii::$app->params['BASE_URL'] . 'facebook/feed/?u_id=' . base64_encode(base64_encode(Yii::$app->user->identity->id)) ?>">
                                                                </div>  <?php
                                                            }
                                                        }
                                                    }
                                                }
                                                ?>
                                                <div id="disable-warning" tabindex="-1" role="dialog" class="modal fade">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="text-center">
                                                                    <div class="text-warning"><span class="modal-main-icon mdi mdi-alert-triangle"></span></div>
                                                                    <h3>Warning!</h3>
                                                                    <p>Are you sure, you want to disable your <span class="modalmsg">Store/</span> channel? If you do so, no data will sync until re-enabled.</p>
                                                                    <div class="xs-mt-50">
                                                                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default cacnle_btn">Cancel</button>
                                                                        <button type="button" class="btn btn-space btn-warning proceed_todlt" data-connid="">Proceed</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer"></div>
                                                        </div>
                                                    </div>
                                                </div> 


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--For loader Spinner!-->
                            <div class="be-spinner" style="width:100%; text-align: center; right:auto">
                                <svg width="40px" height="40px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                                <circle fill="none" stroke-width="4" stroke-linecap="round" cx="33" cy="33" r="30" class="circle"></circle>
                                </svg>
                                <span style="display:block; padding-top:30px;">Your data is importing, you will be able to access these settings once importing will be done.</span>                            </div>
                            <!--End Loader Spinner!-->
                            
                        </div>
                        <!--div id="additionalnote" class="tab-pane cont">
                            <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima praesentium laudantium ipsa, enim maxime placeat, dolores quos sequi nisi iste velit perspiciatis rerum eveniet voluptate laboriosam perferendis ipsum. Expedita, maiores.</p>
                            <p> Consectetur adipisicing elit. Minima praesentium laudantium ipsa, enim maxime placeat, dolores quos sequi nisi iste velit perspiciatis rerum eveniet voluptate laboriosam perferendis ipsum. Expedita, maiores.</p>
                            <div class="col-sm-12">
                                    <div class="panel-body">
                                        <!--select class="select2" id="selectTranslationsetting">
                                            <optgroup label="Please Select">
                                                
                                              <?php 
                                             // if(!empty($content_list)){
                                              //foreach($content_list as $content){ 
                                                 
//                                                  echo "<pre>";
//                                                  print_r($content); 
//                                                  echo "</pre>"; ?>
                                                <option  value="<?php //echo $content->page_title; ?>"><?php //echo $content->page_title; ?>11</option>
                                              <?php //}} ?>
                                        </select-->
                                    <!--/div>
                            </div>
                        </div -->
                        <!--  <div id="paymentmethod" class="tab-pane">
                            <p>Consectetur adipisicing elit. Ipsam ut praesentium, voluptate quidem necessitatibus quam nam officia soluta aperiam, recusandae.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos facilis laboriosam, vitae ipsum tenetur atque vel repellendus culpa reiciendis velit quas, unde soluta quidem voluptas ipsam, rerum fuga placeat rem error voluptate eligendi modi. Delectus, iure sit impedit? Facere provident expedita itaque, magni, quas assumenda numquam eum! Sequi deserunt, rerum.</p><a href="#">Read more  </a>
                          </div>-->
						  <?php //if(!empty($chl)){ ?>
                        <div id="Fulfillment" class="tab-pane">
                            <div class="panel-body">
                                <form action="#" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
                                    <div class="form-group">
                                        <div class="col-sm-6">
                                            <label class="col-sm-12 panel-heading">Enable Fulfillment</label>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="switch-button switch-button-success FullLable xs-mt-20">
                                                <input <?php
                                                if (!empty($fulfilled_data)) {
                                                    echo 'checked=""';
                                                }
                                                ?>  type="checkbox" name="channelfulfillment" id="channelfulfillment"><span>
                                                    <label for="channelfulfillment"></label></span>
                                                <input type="hidden" value="<?php echo $_GET['id'] ?>" id="channelId" />
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-sm-6 xs-pt-5" style="display:none;" id="newfullfill">
											<?php //echo '<pre>'; print_r($fulfilled_data); echo '</pre>'; ?>
                                            <select  class="select2" id="fulfillmentlist" name="fulfillmentlist" <?php
                                            if (empty($fulfilled_data)) {
                                                echo 'style="display:none;"';
                                            }
                                            ?> >
                                            <optgroup label="Fulfillments">
											<option value="">Select Fulfillment</option>
                                                <?php
                                                $FulFillmentList = FulfillmentList::find()->all();
												foreach ($FulFillmentList as $fulfillment) {
													if($fulfillment->fulfillment_name == 'SF Express'){
												?>
                                                    <option <?php
                                                    if (!empty($fulfilled_data) && $fulfilled_data->setting_value == $fulfillment->id) {
                                                        echo "SELECTED";
                                                    }
                                                    ?> value="<?php echo $fulfillment->id; ?>"><?php echo $fulfillment->fulfillment_name; ?></option>
												<?php } } ?>
											 </optgroup>
                                        </select>
                                        </div>
                                    </div>
                                </form>



                                <div id="disable-warning-fulfilled" tabindex="-1" role="dialog" class="modal fade">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close todlt_fulfillment_close"></span></button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="text-center">
                                                    <div class="text-warning"><span class="modal-main-icon mdi mdi-alert-triangle"></span></div>
                                                    <h3>Warning!</h3>
                                                    <p>Are you sure, you want to disable your <span class="modalmsg">Fulfillment</span> If you do so, no data will sync until re-enabled.</p>
                                                    <div class="xs-mt-50">
                                                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default cacnle_btn todlt_fulfillment_close">Cancel</button>
                                                        <button type="button" class="btn btn-space btn-warning proceed_todlt_fulfillment" data-connid="">Proceed</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer"></div>
                                        </div>
                                    </div>
                                </div>



                            </div>

                        </div>
						<?php //} ?>
                        <?php
                        $users_Id = Yii::$app->user->identity->id;
						$MainId = $_GET['id'];
						$mul_store_id = $_GET['mul_store_id'];
                        $channelsettingval = Channelsetting::find()->where(['user_id' => $users_Id, 'setting_key' => 'Enable_Smartling','channel_id'=>$MainId])->one();
                        // echo  '<pre>'; print_r($channelsettingval);
                       // echo $channelsettingval->setting_value;
                        if (!empty($channelsettingval)) {
                            ?>
                            <div id="Translations" class="tab-pane">
                                <!--<form action="#" style="border-radius: 0px;" class="form-horizontal group-border-dashed" id="smartling_form_id">-->

                                <!--<div class="form-group">
                                        <label class="col-sm-3 control-label">Smartling Translation File</label>
                                        <div class="col-sm-6">
                                                <input type="file" name="smart-2" id="smart-2" data-multiple-caption="{count} files selected" multiple="" class="inputfile">
                                                <label for="smart-2" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                                        </div>
                                </div>-->
								<?php $type = $_GET['type']; if($type == 'store'){  ?> 
								<input type="hidden" value="<?php echo $_GET['mul_store_id'].'-store'; ?>" id="channel_id_translation" /> 
								<input type="hidden" value="<?php echo $MainId; ?>" id="Main_channel_id_translation" />
								<?php } else { ?>
                                <input type="hidden" value="<?php echo $_GET['id'].'-channel'; ?>" id="channel_id_translation" />
								<input type="hidden" value="<?php echo $MainId; ?>" id="Main_channel_id_translation" />
								<?php } ?>
                                <div class="col-sm-12"> 
                                    <div class="">

                                        <div class="form-group">
                                            <label style="padding:20px 0px 20px !important;" class="col-sm-6 control-label panel-heading custom_Transs">Enable Smartling Translations</label>
                                            <div class="col-sm-6 xs-pt-5">
                                                <div class="switch-button switch-button-success TransLable">
                                                    <input type="checkbox" <?php if ($channelsettingval->setting_value == 'yes') {
                            echo "checked=''";
                        } ?>   name="channelsmartlingyes" id="channelsmartlingyes"><span>
                                                        <label for="channelsmartlingyes"></label></span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <?php
                                $channelsettingvalCat = Channelsetting::find()->where(['user_id' => $users_Id, 'setting_key' => 'Language','channel_id'=>$MainId])->one();
                                echo $channelsettingvalCat->setting_value; die();
                                ?>
                                <div id="channel_id_translation_data">
                                <div class="col-sm-12">
                                    <div class="">
                                        <label class="control-label">Choose your translation Language</label>
                                        <!--p>Translation Services being used by Smartling</p-->
                                        <select class="select2" id="selectTranslationsetting">
                                            <optgroup label="Translations Language">
                                                <option <?php if ($channelsettingvalCat->setting_value == 'ar-EG') {
                                    echo "SELECTED";
                                } ?>  value="ar-EG">ar-EG</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'da-DK') {
                                    echo "SELECTED";
                                } ?> value="da-DK">da-DK</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'de-DE') {
                                    echo "SELECTED";
                                } ?> value="de-DE">de-DE</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'en-CA') {
                                    echo "SELECTED";
                                } ?> value="en-CA">en-CA</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'en-GB') {
                                    echo "SELECTED";
                                } ?> value="en-GB">en-GB</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'en-US') {
                                    echo "SELECTED";
                                } ?> value="en-US">en-US</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'es-ES') {
                                    echo "SELECTED";
                                } ?> value="es-ES">es-ES</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'es-LA') {
                                    echo "SELECTED";
                                } ?> value="es-LA">es-LA</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'fi-FI') {
                                    echo "SELECTED";
                                } ?> value="fi-FI">fi-FI</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'fr-CA') {
                                    echo "SELECTED";
                                } ?>  value="fr-CA">fr-CA</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'fr-FR') {
                                echo "SELECTED";
                            } ?> value="fr-FR">fr-FR</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'hi-IN') {
                                echo "SELECTED";
                            } ?> value="hi-IN">hi-IN</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'id-ID') {
                                echo "SELECTED";
                            } ?> value="id-ID">id-ID</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'it-IT') {
                                echo "SELECTED";
                            } ?> value="it-IT">it-IT</option>
                                               
                                                <option <?php if ($channelsettingvalCat->setting_value == 'ja-JP') {
                                echo "SELECTED";
                            } ?> value="ja-JP">ja-JP</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'ko-KR') {
                                echo "SELECTED";
                            } ?> value="ko-KR">ko-KR</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'nb-NO') {
                                echo "SELECTED";
                            } ?> value="nb-NO">nb-NO</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'nl-NL') {
                                echo "SELECTED";
                            } ?> value="nl-NL">nl-NL</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'pl-PL') {
                                echo "SELECTED";
                            } ?> value="pl-PL">pl-PL</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'sv-SE') {
                                echo "SELECTED";
                            } ?> value="sv-SE">sv-SE</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'th-TH') {
                                echo "SELECTED";
                            } ?> value="th-TH">th-TH</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'vi-VN') {
                                echo "SELECTED";
                            } ?> value="vi-VN">vi-VN</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'zh-CN') {
                                echo "SELECTED";
                            } ?> value="zh-CN">zh-CN</option>
                                                <option <?php if ($channelsettingvalCat->setting_value == 'zh-HK') {
                                echo "SELECTED";
                            } ?> value="zh-HK">zh-HK</option>
							 <option <?php if ($channelsettingvalCat->setting_value == 'zh-TW') {
                                echo "SELECTED";
                            } ?> value="zh-TW">zh-TW</option>
                                            </optgroup>
                                        </select>
                                    </div>

                                </div>
								
								
								
								
								
								
							<?php
                                $channelsettingTranslationtype = Channelsetting::find()->where(['user_id' => $users_Id, 'setting_key' => 'Translationtype','channel_id'=>$MainId])->one();
                                //echo $channelsettingvalCat->setting_value;
                                ?>
								
								
								
								<div class="col-sm-12">
                                    <label class="control-label">Choose your translation</label>
                                    <p>Translation Services being used by Smartling</p>
                                    <select class="select2" id="selectTranslation">
                                        <optgroup label="Translations">
                                            <option <?php if ($channelsettingTranslationtype->setting_value == 'Machine Translations') {echo "SELECTED";} ?> value="Google MT with Edit">Machine Translations</option>
                                            <option <?php if ($channelsettingTranslationtype->setting_value == 'Human Translations') {echo "SELECTED";} ?> value="Google MT">Human Translations</option>
                                            <option value="Translation with Edit" <?php if ($channelsettingTranslationtype->setting_value == 'Hybrid') { echo "SELECTED";} ?> >Hybrid</option>
                                        </optgroup>
                                    </select>
                                    <label <?php if ($channelsettingTranslationtype->setting_value != 'Machine Translations') {echo 'style="display:none;"';} ?>  class="control-label" id="machinetran">Machine Translations ($X.XX, per word)</label>
                                    <label class="control-label" <label <?php if ($channelsettingTranslationtype->setting_value != 'Human Translations') {echo 'style="display:none;"';} ?> id="humantran">Human Translations ($X.XX, per word)</label>
                                    <label class="control-label" <label <?php if ($channelsettingTranslationtype->setting_value != 'Hybrid') { echo 'style="display:none;"';} ?> id="Hybrid">Hybrid($X.XX, per word)</label>
                                </div>
								
								
								
								<?php
                                $channelsettingconnected = Channelsetting::find()->where(['user_id' => $users_Id, 'setting_key' => 'connected','channel_id'=>$MainId])->one();
                                //echo $channelsettingvalCat->setting_value;
                                ?>
								
								<div class="col-sm-12"> 
									<div class="form-group">
                                     <label class="col-sm-6 panel-heading" style="padding:0px !Important;margin:22px 5px !important"><a href="/terms-conditions" target="_blank">You accept our Translations Terms and Conditions.</a></label>
                                            <div class="col-sm-4 xs-pt-20">
                                                <div class="switch-button switch-button-success">
                                                    <input type="checkbox" <?php if ($channelsettingconnected->setting_value == 'yes') {echo "checked=''";} ?> name="smartlingyes" id="smartlingyes"><span>
                                                        <label for="smartlingyes"></label></span>
                                                </div>
                                            </div>
                                        </div>
								</div>
								
								
								
								
								
								
								
								
								
								
  <!--  <?php
    //$channelsettingvallang = Channelsetting::find()->where(['user_id' => $users_Id, 'setting_key' => 'Enable_Categories','channel_id'=>$MainId])->one();
    ?>
                                <div class="col-sm-12" style="display:none;">
                                    <div class="be-checkbox">
                                        <input type="checkbox"  value="bar" <?php //if ($channelsettingvallang->setting_value == 'yes') {
       // echo "checked=''";
  //  } ?> id="channeltranslationcat" data-parsley-multiple="group1" data-parsley-errors-container="#error-container2">
                                        <label class="col-sm-3 control-label panel-heading" for="channeltranslationcat">Category Translation</label>
                                    </div>
                                </div> -->
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label style="margin:8px 5px !important; padding:0px;" class="col-sm-3 control-label panel-heading"><a href="javascript: void(0)" id="getRateTable">View translations rates</a></label>
                                        </div>
                                    </div>
                               <button style="margin:16px 3px 0px 21px;" type="submit" class="btn btn-space btn-primary custom_Button_Class" id="translation_corporate_id">Submit</button>
                            </div>
                                <!--</form>-->
                            </div>
<?php } else {
    ?>
                            <div id="Translations" class="tab-pane">
                                <!--<form action="#" style="border-radius: 0px;" class="form-horizontal group-border-dashed" id="smartling_form_id">-->

                                <!--<div class="form-group">
                                        <label class="col-sm-3 control-label">Smartling Translation File</label>
                                        <div class="col-sm-6">
                                                <input type="file" name="smart-2" id="smart-2" data-multiple-caption="{count} files selected" multiple="" class="inputfile">
                                                <label for="smart-2" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                                        </div>
                                </div>-->
                               <?php $type = $_GET['type']; if($type == 'store'){  ?> 
								<input type="hidden" value="<?php echo $_GET['mul_store_id'].'-store'; ?>" id="channel_id_translation" />
								<input type="hidden" value="<?php echo $MainId; ?>" id="Main_channel_id_translation" />
								<?php } else { ?>
                                <input type="hidden" value="<?php echo $_GET['id'].'-channel'; ?>" id="channel_id_translation" />
								<input type="hidden" value="<?php echo $MainId; ?>" id="Main_channel_id_translation" />
								<?php } ?>
                                <div class="col-sm-12"> 
                                    <div class="panel-body">

                                        <div class="form-group">
                                            <label style="" class="col-sm-6 control-label panel-heading custom_Transs">Enable Smartling Translations</label>
                                            <div class="col-sm-6 xs-pt-5">
                                                <div class="switch-button switch-button-success TransLable">
                                                    <input type="checkbox" <?php //if($smartlingData->connected == 'yes') { echo "checked=''"; }  ?>   name="channelsmartlingyes" id="channelsmartlingyes"><span>
                                                        <label for="channelsmartlingyes"></label></span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div id="channel_id_translation_data">
                                <div class="col-sm-12 test">
                                    <div class="">
                                        <label class="control-label">Choose your translation Language</label>
                                        <p>Translation Services being used by Smartling</p>
                                        <select class="select2" id="selectTranslationsetting">
                                            <optgroup label="Translations Language">
                                                <option  value="ar-EG">ar-EG</option>
                                                <option  value="da-DK">da-DK</option>
                                                <option value="de-DE">de-DE</option>
                                                <option value="en-CA">en-CA</option>
                                                <option value="en-GB">en-GB</option>
                                                <option value="en-US">en-US</option>
                                                <option value="es-ES">es-ES</option>
                                                <option value="es-LA">es-LA</option>
                                                <option value="fi-FI">fi-FI</option>
                                                <option value="fr-CA">fr-CA</option>
                                                <option value="fr-FR">fr-FR</option>
                                                <option value="hi-IN">hi-IN</option>
                                                <option value="id-ID">id-ID</option>
                                                <option value="it-IT">it-IT</option>
                                                <option value="ja-JP">ja-JP</option>
                                                <option value="ko-KR">ko-KR</option>
                                                <option value="nb-NO">nb-NO</option>
                                                <option value="nl-NL">nl-NL</option>
                                                <option value="pl-PL">pl-PL</option>
                                                <option value="sv-SE">sv-SE</option>
                                                <option value="th-TH">th-TH</option>
                                                <option value="vi-VN">vi-VN</option>
                                                <option value="zh-CN">zh-CN</option>
                                                <option value="zh-HK">zh-HK</option>
                                                <option value="zh-TW">zh-TW</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>
								
								
								<?php 
							$user_id = Yii::$app->user->identity->id;
							$smartlingData = Smartling::find()->Where(['elliot_user_id' => $user_id])->one();
								?>
								
								
								<div class="col-sm-12">
                                    <label class="control-label">Choose your translation</label>
                                    <p>Translation Services being used by Smartling</p>
                                    <select class="select2" id="selectTranslation">
                                        <optgroup label="Translations">
                                            <option <?php //if ($smartlingData->translation_type == 'Machine Translations') {echo "SELECTED";} ?> value="Google MT with Edit">Machine Translations</option>
                                            <option <?php //if ($smartlingData->translation_type == 'Human Translations') {echo "SELECTED";} ?> value="Google MT">Human Translations</option>
                                            <option value="Translation with Edit" <?php //if ($smartlingData->translation_type == 'Hybrid') { echo "SELECTED";} ?> >Hybrid</option>
                                        </optgroup>
                                    </select>
                                    <label <?php //if ($smartlingData->translation_type != 'Machine Translations') {echo 'style="display:none;"';} ?>  class="control-label" id="machinetran">Machine Translations ($X.XX, per word)</label>
                                    <label class="control-label" <label <?php //if ($smartlingData->translation_type != 'Human Translations') {echo 'style="display:none;"';} ?> id="humantran">Human Translations ($X.XX, per word)</label>
                                    <label class="control-label" <label <?php //if ($smartlingData->translation_type != 'Hybrid') { echo 'style="display:none;"';} ?> id="Hybrid">Hybrid($X.XX, per word)</label>
                                </div>
								
								
								<div class="col-sm-12"> 
                                    

                                        <div class="form-group">
                                            <label class="col-sm-6 panel-heading" style="padding:0px !Important;margin:22px 5px !important;"><a href="/terms-conditions" target="_blank">You accept our Translations Terms and Conditions.</a></label>
                                            <div class="col-sm-4 xs-pt-20">
                                                <div class="switch-button switch-button-success">
                                                    <input type="checkbox" <?php //if ($smartlingData->connected == 'yes') {echo "checked=''";} ?> name="smartlingyes" id="smartlingyes"><span>
                                                        <label for="smartlingyes"></label></span>
                                                </div>
                                            </div>
                                        </div>

                                   
                                </div>
								
								
								
								
                                <div class="col-sm-12" style="display:none;">
                                    <div class="be-checkbox">

                                        <input type="checkbox" value="bar" id="channeltranslationcat" data-parsley-multiple="group1" data-parsley-errors-container="#error-container2">
                                        <label class="col-sm-3 control-label panel-heading" for="channeltranslationcat">Category Translation</label>

                                    </div>
                                </div>

									<div class="col-sm-12">
                                        <div class="form-group">
                                            <label style="margin:8px 5px !important; padding:0px;" class="col-sm-3 control-label panel-heading"><a href="javascript: void(0)" id="getRateTable">View translations rates</a></label>
                                        </div>
                                    </div>

                                <button style="margin:16px 3px 0px 21px;" type="submit" class="btn btn-space btn-primary custom_Button_Class" id="translation_corporate_id">Submit</button>

                                <!--</form>-->
                            </div>
                            </div>
<?php } ?>
                        <div id="Performance" class="tab-pane">
                            <div class="col-md-12">
                                <div class="panel panel-default panel-table">
                                    <div class="panel-heading"> 
                                        <div class="title">Recent Orders</div>
                                    </div>
                                        <div class="panel-body table-responsive ">
                                            <table id="recent_orders_dashboard" class="table-borderless table table-striped table-hover table-fw-widget dataTable">
                                                <thead>
                                                    <tr>
                                                        <th >Order ID</th>
                                                        <th>Customer Name</th>
                                                        <th class="number12" style="text-align:left;">Amount</th>
                                                        <!-- <th style="width:20%;">Date</th> -->
                                                        <th >Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="no-border-x">
                                                    <?php if (empty($performance_order)): ?>
                                                        <td valign="top" colspan="7" class="dataTables_empty">No data available in table.</td>
                                                        <?php
                                                    else :
                                                        usort($performance_order, function($a, $b) {
                                                            $t1 = strtotime($a->order_date);
                                                            $t2 = strtotime($b->order_date);
                                                            return $t2 - $t1;
                                                        });
                                                        foreach ($performance_order as $orders_data_value) :
                                                            $channel_abb_id = isset($orders_data_value->channel_abb_id) ? $orders_data_value->channel_abb_id : "";
                                                            $firstname = $orders_data_value->customer->first_name;
                                                            $lname = $orders_data_value->customer->last_name;
                                                            $order_amount = isset($orders_data_value->total_amount) ? $orders_data_value->total_amount : 0;
                                                            $order_value = number_format((float) $order_amount, 2, '.', '');
                                                            $date_order = date('M-d-Y', strtotime($orders_data_value->order_date));
                                                            $order_status = $orders_data_value->order_status;
                                                            $label = '';
                                                            if ($order_status == 'Completed') :
                                                                $label = 'label-success';
                                                            endif;

                                                            if ($order_status == 'Returned' || $order_status == 'Refunded' || $order_status == 'Cancel') :
                                                                $label = 'label-danger';
                                                            endif;

                                                            if ($order_status == 'In Transit' || $order_status == 'On Hold'):
                                                                $label = 'label-primary';
                                                            endif;

                                                            if ($order_status == 'Awaiting Fulfillment' || $order_status == 'Incomplete' || $order_status == 'waiting-for-shipment' || $order_status == 'Pending' || $order_status == 'Awaiting Payment' || $order_status == 'On Hold'):
                                                                $label = 'label-warning';
                                                            endif;
                                                            if ($order_status == 'Shipped'):
                                                                $label = 'label-primary';
                                                            endif;


                                                            $conversion_rate = 1;
                                                            $user = Yii::$app->user->identity;
                                                            if (isset($user->currency) and $user->currency != 'USD') {
                                                                $username = Yii::$app->params['xe_account_id'];
                                                                $password = Yii::$app->params['xe_account_api_key'];
                                                                $URL = Yii::$app->params['xe_base_url'] . 'convert_from.json/?from=USD&to=' . $user->currency . '&amount=1';
                                                                $ch = curl_init();
                                                                curl_setopt($ch, CURLOPT_URL, $URL);
                                                                curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
                                                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                                                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                                                                curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
                                                                $result = curl_exec($ch);
                                                                $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE); //get status code
                                                                curl_close($ch);
//echo'<pre>';
                                                                $result = json_decode($result, true);
                                                                if (isset($result) and ! empty($result) and isset($result['to']) and isset($result['to'][0]) and isset($result['to'][0]['quotecurrency'])) {
                                                                    $conversion_rate = $result['to'][0]['mid'];
                                                                    $conversion_rate = number_format((float) $conversion_rate, 2, '.', '');
                                                                    $order_value = $order_value * $conversion_rate;
                                                                    $order_value = number_format((float) $order_value, 2, '.', '');
                                                                }
                                                            }

                                                            $selected_currency = CurrencySymbols::find()->where(['name' => strtolower($user->currency)])->select(['id', 'symbol'])->asArray()->one();
                                                            if (isset($selected_currency) and ! empty($selected_currency)) {
                                                                $currency_symbol = $selected_currency['symbol'];
                                                            }
                                                            ?>
                                                            <tr>
                                                                <td><a href="orders/view/<?php echo $orders_data_value->order_ID; ?>"><?= $channel_abb_id; ?></a></td>
                                                                <td class="captialize"><?= $firstname . ' ' . $lname; ?></td>
                                                                <td class="number12" style="text-align:left;"><?php echo $currency_symbol ?><?= number_format($order_value, 2); ?></td>
                                                                <!-- <td><?= $date_order; ?></td> -->
                                                                <td><span class="label <?= $label; ?>"><?= $order_status; ?></span></td>
                                                            </tr>
                                                            <?php
                                                        endforeach;
                                                    endif;
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                </div>
                            </div>
                        </div>
                        <div id="Currency" class="tab-pane">
                            <p><b> Channel Default Currency</b></p>

                            <span width="65%"><a id="channel_CurrencyPreference" href="javascript:" class="editable editable-click editable-empty" data-type="select" data-value="<?php echo (!empty($currency_setting->setting_value)) ? $currency_setting->setting_value : 'USD' ?>" data-title="Enter Default Currency"><?php echo (!empty($currency_setting->setting_value)) ? $currency_setting->setting_value : 'USD' ?></a></span>
                        </div>
                        <!-- <div id="Reports" class="tab-pane">
      <p>Consectetur adipisicing elit. Ipsam ut praesentium, voluptate quidem necessitatibus quam nam officia soluta aperiam, recusandae.</p>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos facilis laboriosam, vitae ipsum tenetur atque vel repellendus culpa reiciendis velit quas, unde soluta quidem voluptas ipsam, rerum fuga placeat rem error voluptate eligendi modi. Delectus, iure sit impedit? Facere provident expedita itaque, magni, quas assumenda numquam eum! Sequi deserunt, rerum.</p><a href="#">Read more  </a>
    </div>-->
                    </div>
                </div>
            </div>
        </div>
        <!--Success Tabs-->

        <!--Success Tabs-->

    </div>
    <!--Accordions-->

</div>
<div tabindex="-1" role="dialog" class="modal fade in channelfulfillment-success" style="display: none; background: rgba(0,0,0,0.6);">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close channelfulfillment_modal_close"></span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="text-success"><span class="modal-main-icon mdi mdi-check"></span></div>
                    <h3 id="ajax_header_msg">Success!</h3>
                    <p id="magento_ajax_msg">Current Channel/Store order will fulfilled by SF Expres.</p>
                    <div class="xs-mt-50">
                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default channelfulfillment_modal_close">Close</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

<div tabindex="-1" role="dialog" class="modal fade in channelfulfillment-success-not-found" style="display: none; background: rgba(0,0,0,0.6);">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close channelfulfillment_modal_close"></span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
                   <h3 id="ajax_header_msg">Error!</h3>
                    <p id="magento_ajax_msg">No Fullfillment found !</p>
                    <div class="xs-mt-50">
                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default channelfulfillment_modal_close">Close</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<div tabindex="-1" role="dialog" class="modal fade in channelfulfillment-success-error-found" style="display: none; background: rgba(0,0,0,0.6);">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close channelfulfillment_modal_close"></span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
                   <h3 id="ajax_header_msg">Error!</h3>
                    <p id="magento_ajax_msg">No Fullfillment found !</p>
                    <div class="xs-mt-50">
                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default channelfulfillment_modal_close">Close</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>



<div tabindex="-1" role="dialog" class="modal fade in translatoinfulfillment-success" style="display: none; background: rgba(0,0,0,0.6);">
    <div class="modal-dialog">
        <div class="modal-content credit_card_modal" style="overflow: visible;">
			<div class="modal-header" style="padding-top: 50px; padding-bottom: 0;">
				<div class="text-center">
					<p id="ajax_header_msg"><h3 id="">Translation Service Fee</h3> </p>
				</div>
            </div>
			<div class="smartling_logo">
				<img src="/img/elliot-logo-thumbnail-01.jpg" alt="Smartling Logo">
				<button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close credit_card_close"></span></button>
			</div>
            <div class="modal-body">
			<?php
			$userData = User::find()->Where(['id' => $users_Id])->one();
			$credit = $userData->customer_token;
			if(empty($credit)){ ?>
			<input type="hidden" id="checkCredit" value="1" />
			<div class="text-center">
                  <form id="credit-card-authorize-form" action="" data-parsley-namespace="data-parsley-" data-parsley-validate="" novalidate="" class="form-horizontal group-border-dashed">
                    <div class="form-group no-padding main-title">
						<!--label class="col-sm-4 control-label">Email</label-->
						<div id="smartling_creditemail" class="col-sm-12 input_icon">
							<div class="icon">
								<span class="mdi mdi-email"></span>
							</div>
							<input id="creditemail" type="text" placeholder="Email" class="form-control input-padd">
						</div>
                    </div>
					<div class="form-group no-padding main-title">
					  <!--label class="col-sm-4 control-label">Card Number</label-->
					  <div id="smartling_creditcart" class="col-sm-12 input_icon">
							<div class="icon"><span class="mdi mdi-card"></span></div>
							<input id="creditcart" type="text" placeholder="Card Number" class="form-control input-padd">
					  </div>
                    </div>
					<div class="form-group no-padding main-title">
					  <!--label class="col-sm-4 control-label">MM/YY</label-->
						<div id="smartling_creditdate" class="col-sm-6 input_icon">
							<div class="icon"><span class="mdi mdi-calendar-check"></span></div>
							<input id="creditdate" type="text" placeholder="MM/YY" class="form-control input-padd">
						</div>
						<div id="smartling_creditcvc" class="col-sm-6 input_icon">
							<div class="icon"><span class="mdi mdi-lock-outline"></span></div>
							<input id="creditcvc" type="text" placeholder="CVC" class="form-control input-padd">
						</div>
					</div>
                    <div class="col-md-12 xs-mt-20" id="getValuePrice">
                        <button type="button" data-dismiss="modal" class=" smartling_enble_controller btn btn-space btn-default  btn-primary btn-lg">Confirm
                        <img src="img/spinner.gif" alt="" style="width: 30px; height: 30px;"/></button>
                    </div>
					<!--div class="col-md-6 xs-mt-50">
                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default translatoinfulfillment_modal_close btn-primary">Cancel</button>
                    </div-->
					</form>
                </div>
			<?php } else {
			
			?>
				<div class="text-center">
					 <h3>You will be charged from your saved card. Click confirm to Start Translation.</h3>
					 <div class="col-md-12">
					 <div class="button-middle">
						<div class="button-middle-inline xs-mt-50" id="getValuePrice">
                                                    <button type="button" data-dismiss="modal" class=" smartling_enble_controller btn btn-space btn-default  btn-primary">Confirm
                                                        <img src="img/spinner.gif" alt="" style="width: 30px; height: 30px;"/>
                                                        </button>
						</div>
						 <div class="button-middle-inline xs-mt-50">
							<button type="button" data-dismiss="modal" class="btn btn-space btn-default translatoinfulfillment_modal_close btn-primary">Cancel</button>
						</div>
					</div>
                </div>
		<?php	}
			?>
                
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

</div>
<div tabindex="-1" role="dialog" class="modal fade in translatoinfulfillment1-success" style="display: none; background: rgba(0,0,0,0.6);">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
			
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close translatoinfulfillment_modal_close"></span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="text-success"><span class="modal-main-icon mdi mdi-check"></span></div>
                    <h3 id="ajax_header_msg">Success!</h3>
                    <p id="magento_ajax_msg " class="alldataadd">Your Translations Setting Saved!</p>
                    <div class="xs-mt-50">
                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default translatoinfulfillment1_modal_close">Close</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<div tabindex="-1" role="dialog" class="modal fade in translatoinfulfillment12-success" style="display: none; background: rgba(0,0,0,0.6);">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
			
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close translatoinfulfillment_modal_close"></span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="text-success"><span class="modal-main-icon mdi mdi-check"></span></div>
                    <h3 id="ajax_header_msg">Success!</h3>
                    <p id="magento_ajax_msg " class="alldataadd">Your Translations Setting Saved!</p>
                    <div class="xs-mt-50">
                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default translatoinfulfillment12_modal_close">Close</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

<div id="mod-danger" tabindex="-1" role="dialog" class="modal fade in channelfulfillment-error" style="display: none; background: rgba(0,0,0,0.6);">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
			
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close channelfulfillment_error_modal_close"></span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
                    <h3 id='ajax_header_error_msg'>Success!</h3>
                    <p id="magento_ajax_msg_eror">Current Channel/Store order will not fulfilled by SF Express.</p>
                  
					<div class="xs-mt-50">
                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default channelfulfillment_error_modal_close">Close</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<?php  
$SmartlingPrice = SmartlingPrice::find()->all();
//echo '<pre>'; print_r($SmartlingPrice); echo '</pre>';

?>
<div id="mod-danger" tabindex="-1" role="dialog" class="modal fade in translationmod-rate" style="display: none; background: rgba(0,0,0,0.6);">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close trans_rate_modal_close"></span></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="form-group">
																<div class="panel-body table-responsive ">
                                                                    <table id="table1" class="table table-striped table-hover table-fw-widget">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Language(Locale ID)</th>
                                                                                <th>Human Translations Rate/Words</th>
                                                                                <th>Hybrid Rate/Words</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody class="no-border-x">
																		<?php 
																		foreach($SmartlingPrice as $data){
																		?>
                                                                            <tr>
                                                    <td class="captialize"><?php echo $data->target_language.'('.$data->locale_id.')'; ?></td>
                                                                                <td><a href="javascript: void(0)"><?php echo $data->rate2 ?></a></td>
                                                                                <td><a href="javascript: void(0)"><?php echo $data->editing ?></a></td>
                                                                            </tr>
																		<?php } ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer"></div>
                                                </div>
                                            </div>
                                        </div>