<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use backend\models\Channels;
use yii\db\Query;
use backend\models\User;
use backend\models\ChannelConnection;

/* @var $this yii\web\View */
/* @var $model backend\models\Channels */

$this->title = 'Global Commerce Connector';
$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => 'javascript: void(0)', 'class' => 'non_link'];
$this->params['breadcrumbs'][] = ['label' => 'Channels', 'url' => ['/index']];
$this->params['breadcrumbs'][] = 'Add New';
//use for base directory//
$basedir = Yii::getAlias('@basedir');
//include stripe init file//
require $basedir . '/stripe/init.php';
\Stripe\Stripe::setApiKey("sk_test_2GPaBt5mT2k0yyTA9ho3CPIQ");
//get subscription plan through stripe//
$collection_plans = \Stripe\Plan::all(array("limit" => 100));

$stripe_plans = $collection_plans->__toArray(true);
$stripe_plan_data = $stripe_plans['data'];
//echo "<pre>";
//print_r($stripe_plan_data);
//echo "</pre>"; die;


foreach ($stripe_plan_data as $data) {
    $db_stripe_channel_data = Channels::find()->Where(['channel_name' => $data['name']])->one();
    if (empty($db_stripe_channel_data)) {
        $meta = @$data['metadata']['image_url'];
        $id = $data['id'];
        $prefix_channel = substr($id, 0, 7);
        if ($prefix_channel == 'channel') {
            $stripe_Channel = new Channels();
            $stripe_Channel->stripe_Channel_id = stripslashes($data['id']);
            $stripe_Channel->amount = stripslashes($data['amount'] / 100);
            $stripe_Channel->currency = stripslashes($data['currency']);
            $stripe_Channel->channel_image = stripslashes(@$data['metadata']['image_url']);
            $stripe_Channel->channel_name = stripslashes($data['name']);
            $stripe_Channel->parent_name = stripslashes(@$data['metadata']['parent_name']);
            $stripe_Channel->trial_period_days = stripslashes($data['trial_period_days']);
            $stripe_Channel->save(false);
        }
    } else {
        $meta = @$data['metadata']['image_url'];
        $id = $data['id'];
        $prefix_channel = substr($id, 0, 7);
        if ($prefix_channel == 'channel') {
            $stripe_Channel = Channels::find()->Where(['channel_name' => $data['name']])->one();
            $stripe_Channel->stripe_Channel_id = stripslashes($data['id']);
            $stripe_Channel->amount = stripslashes($data['amount'] / 100);
            $stripe_Channel->currency = stripslashes($data['currency']);
            $stripe_Channel->channel_image = stripslashes(@$data['metadata']['image_url']);
            $stripe_Channel->channel_name = stripslashes($data['name']);
            $stripe_Channel->parent_name = stripslashes(@$data['metadata']['parent_name']);
            $stripe_Channel->trial_period_days = stripslashes($data['trial_period_days']);
            $stripe_Channel->save(false);
        }
    }
}
?>

<div class="page-head">
    <h2 class="page-head-title">Add New</h2>
    <ol class="breadcrumb page-head-nav">
        <?php
        echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
        ?>
    </ol>
</div>

<div class="main-content container-fluid">
    <div class="row marketplace-listing">
        <?php
        $connection = \Yii::$app->db;
        //$model1 = $connection->createCommand('SELECT DISTINCT parent_name stripe_Channel_id,channel_image,parent_name FROM channels');
        $model1 = $connection->createCommand('SELECT COUNT(parent_name), parent_name FROM channels GROUP BY parent_name');
        $channels = $model1->queryAll();


        $integrated_channels = array('channel_Facebook', 'channel_Flipkart', 'channel_google-shopping', 'channel_Lazada', 'channel_Square', 'channel_WeChat');

        foreach ($channels as $channels_data) {

            //get wechat Service account id 
            $get_big_id = Channels::find()->select('channel_ID')->where(['parent_name' => $channels_data['parent_name']])->one();
            $channel_id = $get_big_id->channel_ID;



            $channel_connection = ChannelConnection::find()->Where(['channel_id' => $channel_id])->one();
//            echo "<pre>";
//            print_r($channel_connection);
//             echo "</pre>";
            $count_parent_name = $channels_data['COUNT(parent_name)'];
            $parent_name = $channels_data['parent_name'];
            if ($parent_name == 'channel_Lazada') {
                $channels_data = Channels::find()->where(['parent_name' => $parent_name])->with(['channelconnections' => function($query) {
                                $query->andWhere(['user_id' => Yii::$app->user->identity->id]);
                            }])->asArray()->all();
                $multiseller_center_connected_channel_count = 0;
                if (isset($channels_data) and ! empty($channels_data)) {
                    foreach ($channels_data as $single_multiseller_channel) {
                        if (isset($single_multiseller_channel) and ! empty($single_multiseller_channel) and ! empty($single_multiseller_channel['channelconnections'])) {
                            $multiseller_center_connected_channel_count++;
                        }
                    }
                }
            }
            ?>
            <?php
            //if(in_array($parent_name, $integrated_channels) == true){
//			if(in_array($parent_name, $integrated_channels) == true){
            if ($count_parent_name == 1) {

                $marketplace = Channels::find()->Where(['parent_name' => $parent_name])->one();
                if ($marketplace->parent_name == 'channel_Square' || empty($marketplace)) {
                    continue;
                }
                ?>
                <?php //if (!empty($channel_connection) && $channel_connection->wechat_admin_approve == 'yes') { ?>
                <?php
                if ($marketplace->stripe_Channel_id == 'channel_Instagram') {
                    ?>
                    <div class="col-sm-3">
                        <div class="bs-grid-block comingsoon" data-toggle="modal"  data-target="#<?php echo $marketplace->stripe_Channel_id; ?> ">
                            <div class="content">
                                <span class="label label-success custom-span-channels-label span-top-22" style=" position: absolute; right: 30px; ">Coming Soon</span>
                                <img src="<?php echo $marketplace->channel_image; ?>" style="<?php echo $marketplace->stripe_Channel_id == 'channel_WeChat-Store' ? '' : 'width: 100%;' ?>" alt="<?php echo $marketplace->stripe_Channel_id; ?>">
                            </div>
                        </div>
                    </div>
                    <?php
                } else if ($marketplace->stripe_Channel_id == 'channel_google-shopping') {
                    $user_id = Yii::$app->user->identity->id;
                    $user_data = User::find()->where(['id' => $user_id, 'google_feed' => 'yes'])->one();
                    if (!empty($user_data)):
                        $google_enable = 'yes';
                    else:
                        $google_enable = 'no';
                    endif;

                    if ($google_enable == 'yes') {
                        ?>
                        <div class="col-sm-3">
                            <a href="/google-shopping">
                                <div class="bs-grid-block connected">

                                    <div class="content">
                                        <span  class="label label-success span-top-22" style=" position: absolute; right: 30px; ">Connected</span>
                                        <img src="<?php echo $marketplace->channel_image; ?>" alt="" style="width: 100%">
                                    </div>

                                </div>
                            </a>
                        </div>
                    <?php } else {
                        ?>
                        <div class="col-sm-3">
                            <div class="bs-grid-block">
                                <div class="content">
                                    <a href="/google-shopping/"><img src="<?php echo $marketplace->channel_image; ?>" style="width: 100%;" alt="" class="<?php echo $marketplace->stripe_Channel_id == "channel_Xiao_Hong_Shu" ? 'resize_Xiao' : ''; ?>"></a>
                                </div>
                            </div>
                        </div>

                        <?php
                    }
                } else if ($marketplace->stripe_Channel_id == 'channel_facebook') {

                    $facebook = Channels::find()->where(['channel_name' => 'Facebook'])->one();
//echo'<pre>';
//print_r($lazada);die;
                    $channel_id = $facebook->channel_ID;
//echo $channel_id;die;
                    $fb_connected = ChannelConnection::find()->Where(['user_id' => Yii::$app->user->identity->id, 'channel_id' => $channel_id, 'connected' => 'yes'])->one();
                    if (isset($fb_connected) and ! empty($fb_connected)) {
                        ?><div class="col-sm-3">
                            <a href="/integrations/facebook">
                                <div class="bs-grid-block connected">

                                    <div class="content">
                                        <span  class="label label-success span-top-22" style=" position: absolute; right: 30px; ">Connected</span>
                                        <img src="<?php echo $marketplace->channel_image; ?>" alt="" style="width: 100%">
                                    </div>

                                </div>
                            </a>
                        </div><?php
                    } else {
                        ?>
                        <div class="col-sm-3">
                            <div class="bs-grid-block">
                                <div class="content">
                                    <a href="/integrations/facebook"><img src="<?php echo $marketplace->channel_image; ?>" style="width: 100%;" alt="" class="<?php echo $marketplace->stripe_Channel_id == "channel_Xiao_Hong_Shu" ? 'resize_Xiao' : ''; ?>"></a>
                                </div>
                            </div>
                        </div><?php
                    }
                    ?>

                    <?php
                } else if ($marketplace->stripe_Channel_id == 'channel_FlipKart') {

                    $facebook = Channels::find()->where(['channel_name' => 'Flipkart'])->one();
//echo'<pre>';
//print_r($lazada);die;
                    $channel_id = $facebook->channel_ID;
//echo $channel_id;die;
                    $fb_connected = ChannelConnection::find()->Where(['user_id' => Yii::$app->user->identity->id, 'channel_id' => $channel_id, 'connected' => 'yes'])->one();
                    if (isset($fb_connected) and ! empty($fb_connected)) {
                        ?><div class="col-sm-3">
                            <a href="/flipkart">
                                <div class="bs-grid-block connected">

                                    <div class="content">
                                        <span  class="label label-success span-top-22" style=" position: absolute; right: 30px; ">Connected</span>
                                        <img src="<?php echo $marketplace->channel_image; ?>" alt="" style="width: 100%">
                                    </div>

                                </div>
                            </a>
                        </div><?php
                    } else {
                        ?>
                        <div class="col-sm-3">
                            <div class="bs-grid-block">
                                <div class="content">
                                    <a href="/flipkart"><img src="<?php echo $marketplace->channel_image; ?>" style="width: 100%;" alt="" class="<?php echo $marketplace->stripe_Channel_id == "channel_Xiao_Hong_Shu" ? 'resize_Xiao' : ''; ?>"></a>
                                </div>
                            </div>
                        </div><?php
                    }
                    ?>
                    <?php
                } else if ($marketplace->stripe_Channel_id == 'channel_TMall') {

                    $facebook = Channels::find()->where(['channel_name' => 'TMall'])->one();
//echo'<pre>';
//print_r($lazada);die;
                    $channel_id = $facebook->channel_ID;
//echo $channel_id;die;
                    $fb_connected = ChannelConnection::find()->Where(['user_id' => Yii::$app->user->identity->id, 'channel_id' => $channel_id, 'connected' => 'yes'])->one();
                    if (isset($fb_connected) and ! empty($fb_connected)) {
                        ?><div class="col-sm-3">
                            <a href="/tmall">
                                <div class="bs-grid-block connected">

                                    <div class="content">
                                        <span  class="label label-success span-top-22" style=" position: absolute; right: 30px; ">Connected</span>
                                        <img src="<?php echo $marketplace->channel_image; ?>" alt="" style="width: 100%">
                                    </div>

                                </div>
                            </a>
                        </div><?php
                    } else {
                        ?>
                        <div class="col-sm-3">
                            <div class="bs-grid-block">
                                <div class="content">
                                    <a href="/tmall"><img src="<?php echo $marketplace->channel_image; ?>" style="width: 100%;" alt="" class="<?php echo $marketplace->stripe_Channel_id == "channel_Xiao_Hong_Shu" ? 'resize_Xiao' : ''; ?>"></a>
                                </div>
                            </div>
                        </div><?php
                    }
                    ?>

                <?php }
                elseif($marketplace->stripe_Channel_id == 'channel_Jet'){ ?>
                    <div class="col-sm-3">
                        <div class="bs-grid-block">
                            <div class="content">
                                <a href="/jet">
                                    <img src="<?php echo $marketplace->channel_image; ?>" style="width: 100%;" alt="" class="">
                                </a>
                            </div>
                        </div>
                    </div>
                <?php }
                else {
                    ?>
                    <div class="col-sm-3">
                        <div class="bs-grid-block tester">
                            <div class="content">
                                <img src="<?php echo $marketplace->channel_image; ?>" style="width: 100%;" alt="" class="<?php echo $marketplace->stripe_Channel_id == "channel_Xiao_Hong_Shu" ? 'resize_Xiao' : ''; ?>">
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php
            } else if ($count_parent_name != 1) {
                $marketplace = Channels::find()->Where(['parent_name' => $parent_name])->one();
                if (!empty($channel_connection) && $channel_connection->wechat_admin_approve == 'yes') {
                    ?>
                    <div class="col-sm-3">
                        <a href="/channelsetting/?id=<?php echo $channel_id; ?>&type=channel&mul_store_id=">
                            <div class="bs-grid-block connected">
                                <div class="content">
                                    <span  class="label label-success span-top-22" style=" position: absolute; right: 30px; ">Connected</span>
                                    <img src="<?php echo $marketplace->channel_image; ?>" alt="" style="width: 100%">
                                </div>

                            </div>
                        </a>
                    </div>
                    <?php
                } elseif (isset($multiseller_center_connected_channel_count) and ! empty($multiseller_center_connected_channel_count) and $parent_name == 'channel_Lazada') {
                    $lazada_status = $multiseller_center_connected_channel_count . ' of ' . $count_parent_name . ' Seller Centers Connected';
                    ?>

                    <div class="col-sm-3">
                        <div class="bs-grid-block connected" data-toggle="modal"  data-target="#<?php echo $marketplace->stripe_Channel_id; ?> ">
                            <div class="content">
                                <span  class="label label-success span-top-22" style=" position: absolute; right: 30px; "><?php echo $lazada_status; ?></span>

                                <img src="<?php echo $marketplace->channel_image; ?>" style="width: 100%" alt="<?php echo $marketplace->stripe_Channel_id; ?>">
                            </div>
                        </div>
                    </div>
                <?php } else {
                    ?>
                    <div class="col-sm-3">
                        <div class="bs-grid-block testeer12" data-toggle="modal"  data-target="#<?php echo $marketplace->stripe_Channel_id; ?> ">
                            <div class="content">
                                <img src="<?php echo $marketplace->channel_image; ?>" style="width: 100%" alt="<?php echo $marketplace->stripe_Channel_id; ?>">
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
            //}
        }
        ?>
    </div>
</div>
<?php
$connection1 = \Yii::$app->db;

$model2 = $connection->createCommand('SELECT COUNT(parent_name), parent_name FROM channels GROUP BY parent_name');

$channels1 = $model2->queryAll();

foreach ($channels1 as $channels_data1) {
    $count_parent_name1 = $channels_data1['COUNT(parent_name)'];

    $parent_name1 = $channels_data1['parent_name'];

    $marketplace1 = Channels::find()->Where(['parent_name' => $parent_name1])->one();



    if (empty($marketplace1)):
        $name = "test";
    else:
        $name = $marketplace1->channel_name;
    endif;
    ?>
    <?php if ($count_parent_name1 == 1) { ?>
        <div id="<?php echo $marketplace1->stripe_Channel_id; ?>" tabindex="-1" role="dialog" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                    </div>
                    <div class="modal-body">
                        <div class="text-center">
                            <div class="text-primary"><img src="<?php echo $marketplace1->channel_image; ?>" style="width: 50%" alt=""></span></div>
                            <h3>Coming Soon</h3>
                            <p>Subscribe to get updated when <?= $name ?> becomes available in Elliot.</p>
                            <div class="xs-mt-50"> 
                                <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Close</button>
                                <button type="button" data-dismiss="modal" class="btn btn-space btn-primary">Subscribe</button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer"></div>
                </div>
            </div>
        </div>
    <?php } else if ($count_parent_name1 != 1) { ?>  
        <div id="<?php echo $marketplace1->stripe_Channel_id; ?>" tabindex="-1" role="dialog" class="modal fade">
               <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <img src="<?php echo $marketplace1->channel_image; ?>" style="width: 50%" alt="">
                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                    </div>
                    <div class="modal-body">
                        <div class="">
                            <!--<div class="text-primary text-center"></span></div>-->
                            <div class="row">
                                <div class="form-group">
                                    <?php
                                    $channels_group_data = Channels::find()->Where(['parent_name' => $parent_name1])->with(['channelconnections' => function($query) {
                                                    $query->andWhere(['user_id' => Yii::$app->user->identity->id]);
                                                }])->asArray()->all();
                                    $arr = array();
                                    foreach ($channels_group_data as $val) {
                                        $arr[] = $val['channel_name'];
                                    }
                                    sort($arr);
//                                    if ($parent_name1 != 'channel_Lazada') {
//                                        foreach ($arr as $channel_group) {
                                    ?>
                                    <!--                                            <div class="col-sm-3">
                                                                                    <div class="be-radio inline">
                                                                                        <input type="radio" checked="" name="rad3" id="<?php // echo $channel_group; ?>" value="<?php // echo $channel_group; ?>">
                                                                                        <label class="channel-modal-span-group" for="<?php // echo $channel_group; ?>"><br><b><?php // echo $channel_group; ?></b></label>
                                                                                    </div>
                                    
                                                                                </div>-->
                                    <?php
//                                        }
//                                    } else {
                                    ?>           
                                    <div class="panel-body table-responsive ">
                                        <table id="lazada_connect_modal" class="table-borderless table table-striped table-hover table-fw-widget dataTable">
                                            <thead>
                                                <tr>
                                                    <th >Seller Center</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody class="no-border-x">
                                                <?php
                                                if (isset($_GET['a']) and ! empty($_GET['a'])) {
                                                    echo"<pre>";
                                                    print_R($channels_group_data);
                                                    die;
                                                }

                                                foreach ($channels_group_data as $single_multiseller_channel) :
                                                    $status_lazada = 'Get Connected';
                                                    if ($parent_name1 == 'channel_Lazada') {
                                                        $type = str_replace('Lazada ', '', $single_multiseller_channel['channel_name']);
                                                        $link = '/lazada/lazada/?type=' . $type;
                                                    } elseif ($parent_name1 == 'channel_WeChat') {
                                                        $type =  $single_multiseller_channel['channel_name'];
                                                        $link = '/channels/wechat/?type=' . $type;
                                                    }
                                                    else {
                                                      $link = 'javascript:';
                                                    }

                                                    if (isset($single_multiseller_channel['channelconnections']) and ! empty($single_multiseller_channel['channelconnections']) and isset($single_multiseller_channel['channelconnections'][0]) and ! empty($single_multiseller_channel['channelconnections'][0]) and $single_multiseller_channel['channelconnections'][0]['connected'] == 'yes') {
                                                        $status_lazada = 'Connected';
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td class="captialize"><?= $single_multiseller_channel['channel_name']; ?></td>
                                                        <td><a href="<?php echo $link; ?>"><?= $status_lazada; ?></a></td>
                                                    </tr>
                                                    <?php
                                                endforeach;
                                                ?>
                                            </tbody>
                                        </table>
                                    </div><?php
//                                    }
                                    ?>


                                </div>
                            </div>
                            <div class="row">
                                <div class="xs-mt-50 pull-right" style="margin-right: 30px;"> 
                                    <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Close</button>
                                    <!--<button type="button" class="btn btn-space btn-primary">Subscribe</button>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer"></div>
                </div>
            </div>
        </div>

        <?php
    }
}
?>






