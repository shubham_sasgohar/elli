<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\Breadcrumbs;
use backend\models\StoresConnection;
use backend\models\Stores;

$this->title = 'Add Your MercadoLibre Channel';
$this->params['breadcrumbs'][] = $this->title;
$checkConnection = '';
?>
<div class="page-head">
    <h2 class="page-head-title"><?= Html::encode($this->title) ?></h2>
    <ol class="breadcrumb page-head-nav">
        <?php
        echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
        ?>
    </ol>
</div>
<div class="row wizard-row">
    <div class="col-md-12 fuelux">
        <div class="block-wizard panel panel-default">
            <div id="wizard-store-connection" class="wizard wizard-ux">
                <ul class="steps">
                    <li data-step="1" class="active">Authorize<span class="chevron"></span></li>
                    <li data-step="2">Connect<span class="chevron"></span></li>
                </ul>
                <div class="step-content">
                    <div data-step="1" class="step-pane active">
                        <?php if (empty($checkConnection)) : ?>
                            <div id="bgc_text1" class="form-group no-padding main-title" style="display: none">
                                <div class="col-sm-12">
                                    <label class="control-label">Your Mercadolibre Channel is connecting, you will be notified via email when complete.</label>
                                </div>
                            </div>
                            <form id="mercadolibre-authorize-form" action="#" data-parsley-namespace="data-parsley-" data-parsley-validate="" novalidate="" class="form-horizontal group-border-dashed">
                                <div class="form-group no-padding main-title">
                                    <div class="col-sm-12">
                                        <label class="control-label">Please provide your Mercodolibre API details for Authorizing the Integration:</label>
                                        <p>To integrate MercadoLibre Channel with Elliot, </p>                             
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Mercadolibre Email:</label>
                                    <div id="mercadolibre_email" class="col-sm-6">
                                        <input type="text" id="mercadolibre_channel_email" placeholder="Please Enter value" class="form-control mercado_validate">
                                        <em class="notes">Please use full store url ex "http://Mercadolibre.com"</em>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Mercadolibre Password:</label>
                                    <div id="mercadolibre_password" class="col-sm-6">
                                        <input type="text" id="mercadolibre_channel_password" placeholder="Please Enter value" class="form-control mercado_validate">
                                        <em class="notes">Please use full store url ex "http://Mercadolibre.com"</em>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Mercadolibre Client Id:</label>
                                    <div id="mercadolibre_client_id" class="col-sm-6">
                                        <input type="text" id="mercadolibre_channel_client_id" placeholder="Please Enter value" class="form-control mercado_validate">
                                        <em class="notes">Please use full store url ex "http://Mercadolibre.com"</em>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Mercadolibre Client Secret*</label>
                                    <div id="mercadolibre_client_secret_id" class="col-sm-6">
                                        <input type="text" id="mercadolibre_channel_secret_id" placeholder="Please Enter value" class="form-control mercado_validate">
                                        <em class="notes">Example: 2b5b96242b5asdfeqerr8f7c2</em>
                                    </div>
                                </div>

                                <input id="mercadolibre_user_id" type="hidden" value="<?= Yii::$app->user->identity->id; ?>" />
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button class="btn btn-default btn-space">Cancel</button>
                                        <button data-wizard="#wizard-store-connection" class="btn btn-primary btn-space wizard-next-auth-store-mercadolibre">Authorize & Connect</button>
                                    </div> 
                                </div>
                            </form>
                        </div>
                    <?php else: ?>
                        <div id="bgc_conn_text" class="form-group no-padding main-title">
                            <div class="col-sm-12">
                                <label class="control-label">Your Mercadolibre store is connected. When data is import you will get a notify message</label>
                            </div>
                        </div>
                    <?php endif; ?>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<div id="mercadolibre_ajax_request" tabindex="-1" role="dialog" class="modal fade in">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close mercadolibre_modal_close"></span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="text-success"><span class="modal-main-icon mdi mdi-check"></span></div>
                    <h3 id="ajax_header_msg">Success!</h3>
                    <p id="mercadolibre_ajax_msg"></p>
                    <div class="xs-mt-50">
                        <a href="get-access-token"><button type="button" data-dismiss="modal" class="btn btn-space btn-default mercadolibre_modal_close">Next</button></a>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<div id="mod-danger" tabindex="-1" role="dialog" class="modal fade in mercadolibre_ajax_request_error" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close mercadolibre_error_modal_close"></span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
                    <h3 id='ajax_header_error_msg'></h3>
                    <p id="mercadolibre_ajax_msg_eror"></p>
                    <div class="xs-mt-50">
                      <button type="button" data-dismiss="modal" class="btn btn-space btn-default mercadolibre_error_modal_close">Close</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
