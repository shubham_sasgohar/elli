<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use backend\models\CustomerUser;
use backend\models\OrdersProducts;
use backend\models\OrderFullfillment;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CustomerUserSerach */
/* @var $dataProvider yii\data\ActiveDataProvider */
//Company Details Address


$company_street1 = Yii::$app->user->identity->corporate_add_street1;
$company_street2 = Yii::$app->user->identity->corporate_add_street2;
$company_country = Yii::$app->user->identity->corporate_add_country;
$company_state = Yii::$app->user->identity->corporate_add_state;
$company_city = Yii::$app->user->identity->corporate_add_city;
$company_zipcode = Yii::$app->user->identity->corporate_add_zipcode;



$channel_abb_id = $model->channel_abb_id;
$this->title = 'Invoice #' . $channel_abb_id . '';
$this->params['breadcrumbs'][] = $this->title;
$customer_id = $model->customer_id;
$customer_data = CustomerUser::find()->Where(['customer_ID' => $customer_id])->one();
// For amount Invoice;
$marketplace_fees = $model->market_place_fees;
$creditcard_fees = $model->credit_card_fees;
$total_amount = $model->total_amount;
$base_shipping_cost = $model->base_shipping_cost;
$shipping_cost_tax = $model->shipping_cost_tax;
$base_handling_cost = $model->base_handling_cost;
$handling_cost_tax = $model->handling_cost_tax;
$base_wrapping_cost = $model->base_wrapping_cost;
$wrapping_cost_tax = $model->wrapping_cost_tax;
$payment_method = $model->payment_method;
$discount_amount = $model->discount_amount;
$coupon_discount = $model->coupon_discount;

$subtotal = $marketplace_fees + $creditcard_fees + $total_amount;

//For status label
$order_status = $model->order_status;
$label='';
if ($order_status == 'Completed') :
    $label = 'label-success';
endif;

if ($order_status == 'Returned' || $order_status == 'Refunded') :
    $label = 'label-danger';
endif;

if ($order_status == 'In Transit'):
    $label = 'label-primary';
endif;

if ($order_status == 'Awaiting FulFillment' || $order_status == 'Incomplete'):
    $label = 'label-warning';
endif;
if ($order_status == 'Shipped'):
    $label = 'label-primary';
//                           $check='checked';
endif;

//For Products
$order_id = $model->order_ID;
$orders_data = OrdersProducts::find()->Where(['order_Id' => $order_id])->with('product')->all();
$OrderFullfillmentData = OrderFullfillment::find()->Where(['order_Id' => $order_id, 'key_data'=>'1'])->one();
/* echo'<pre>';
print_r($OrderFullfillmentData);
echo '</pre>'; */

// Split it into pieces, with the delimiter being a space. This creates an array.
$billing_country = explode(",", $model->billing_address);
$shipping_country = explode(",", $model->shipping_address);

$bill_to_country = $billing_country[count($billing_country) - 1];
$ship_to_country = $shipping_country[count($shipping_country) - 1];
?>  
<div class="page-head">
    <h2 class="page-head-title"><?= Html::encode($this->title) ?></h2>
    <ol class="breadcrumb page-head-nav">
        <?php
        echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
        ?>
    </ol>
	<?php  if(!empty($OrderFullfillmentData)){ 
			$value_data = json_decode($OrderFullfillmentData->value_data);
			$data =  $value_data->Data;
			$hawbs = $data->Hawbs;
			
			//$hawbs_64encode = base64_encode($hawbs); 
			//echo 'Hawbs '.$hawbs;die;
	?>
	<div class="col-xs-12 invoice-order">
	<h3>Tracking ID : <?php echo $hawbs;?></h3> <span style="display: block;">Click this button to view order tracking status</span>
                    <a target="_blank" href="http://www.sf-express.com/us/en/dynamic_functions/waybill/"><button class="btn btn-lg btn-space btn-primary">Track Order</button></a>
                 
    </div>
	<?php } ?>
</div>    


<div class="row">
    <div class="col-md-12">
        <div class="invoice">
            <div class="row invoice-header">
                <div class="col-xs-7">
                    <div class="invoice-logo"></div>
                    <div class="invoice-person company-details-invoice">
                        <span><?php echo preg_replace('/^([^a-zA-Z0-9])*/', '', $company_street1 . ' ' . $company_street2); ?></span>
                        <span><?php echo preg_replace('/^([^a-zA-Z0-9])*/', '', $company_city . ' ' . $company_state); ?></span>
                        <span><?php echo preg_replace('/^([^a-zA-Z0-9])*/', '', $company_country); ?></span>
                        <span><?php echo preg_replace('/^([^a-zA-Z0-9])*/', '', $company_zipcode); ?></span>
                    </div>
                </div>
                <div class="col-xs-5 invoice-order">
                    <span class="incoice-staus incoice-date"><span class="label  <?php echo $label; ?>"><?php echo $order_status; ?></span></span>
                    <span class="invoice-id">Invoice #<?php echo $model->channel_abb_id; ?></span>
                    <span class="incoice-date"><?php echo date('F j, Y', strtotime($model->created_at)); ?></span>
                </div>
            </div>
            <div class="row invoice-data">
                <div class="col-xs-5 invoice-person">
                    <span class="f-22">Bill To :</span>
                    <span><?php  echo preg_replace('/^([^a-zA-Z0-9])*/', '', $model->bill_street_1.', '.$model->bill_street_2); ?> </span>
                     <span><?php echo preg_replace('/^([^a-zA-Z0-9])*/', '', $model->bill_city.', '.$model->bill_state.', '.$model->bill_zip.', '.$model->bill_country); ?> </span>
                     
                </div>
                <div class="col-xs-2 invoice-payment-direction"><i class="icon mdi mdi-chevron-right"></i></div>
                <div class="col-xs-5 invoice-person">
                    <span class="f-22">Ship To :</span>
                    <span><?php echo preg_replace('/^([^a-zA-Z0-9])*/', '', $model->ship_street_1.', '.$model->ship_street_2)?> </span>
                     <span><?php echo preg_replace('/^([^a-zA-Z0-9])*/', '', $model->ship_city.', '.$model->ship_state.', '.$model->ship_zip.', '.$model->ship_country); ?> </span>
                    
                    <span><?php // echo preg_replace('/^([^a-zA-Z0-9])*/', '', $model->bill_street_1.', '.$model->bill_street_2); ?></span>
                </div>
            </div>

            <!-- for products section !-->
            <div class="row">
                <div class="col-md-12">
                    <table class="invoice-details">
                        <tr>
                            <th>Item Description </th>
                            <th>OTY</th>
                            <th>Price</th>
                            <th>Total</th>
                        </tr>
                        <?php
                        
                        
                                                  $user = Yii::$app->user->identity;
$conversion_rate = 1;
if (isset($user->currency) and $user->currency != 'USD') {
    $username = Yii::$app->params['xe_account_id'];
    $password = Yii::$app->params['xe_account_api_key'];
    $URL = Yii::$app->params['xe_base_url'].'convert_from.json/?from=USD&to=' . $user->currency . '&amount=1';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $URL);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
    $result = curl_exec($ch);
    $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
    curl_close($ch);
//echo'<pre>';
    $result = json_decode($result, true);
    if (isset($result) and ! empty($result) and isset($result['to']) and isset($result['to'][0]) and isset($result['to'][0]['quotecurrency'])) {
        $conversion_rate = $result['to'][0]['mid'];
        $conversion_rate = number_format((float) $conversion_rate, 2, '.', '');
        
    }
}
                        
//                        echo "<pre>";
//                        print_r($orders_data); die;
                        foreach ($orders_data as $order_value) :
                            
//                            echo'<pre>';
//                        print_r($order_value);
//                        echo'<pre>';
//                            
                            $product_name = isset($order_value->product->product_name)?$order_value->product->product_name:'';
                            $price = isset($order_value->product->price)?number_format($order_value->product->price*$conversion_rate, 2):'';
                            $qty = isset($order_value->qty)?$order_value->qty:'';
                            $total = isset($order_value->product->price)?number_format($order_value->product->price *$conversion_rate* $qty, 2):''; 
                              $price = $price * $conversion_rate;
                        $price = number_format((float) $price, 2, '.', '');
                              $total = $total * $conversion_rate;
                        $total = number_format((float) $total, 2, '.', '');
         
                        $selected_currency = \backend\models\CurrencySymbols::find()->where(['name'=> strtolower($user->currency)])->select(['id','symbol'])->asArray()->one();
                                 if (isset($selected_currency) and ! empty($selected_currency)) {
                                    $currency_symbol=$selected_currency['symbol'];
                                   
                                 }
                            
                            ?>
                            <tr>
                                <td class="description"><?php echo $product_name; ?></td>
                                <td class="hours"><?php echo $qty; ?></td>
                                <td class="amount"><?php echo $currency_symbol; ?><?php echo $price; ?></td>
                                <td class="amount"><?php echo $currency_symbol; ?><?php echo $total; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
            <!-- End for products section !-->

            <div class="row invoice-table-2">
                <div class="col-md-12">
                    <table class="invoice-details">
                        <tr>
                            <th style="width:60%">Fees</th>
                            <th style="width:15%" class="amount"></th>
                            <th style="width:17%" class="amount">Amount</th>
                        </tr>
                        <tr>
                            <td class="description">Marketplace Fees</td>
                            <td class="hours"></td>
                            <td class="amount"><?php echo  $currency_symbol;?><?php echo number_format($marketplace_fees*$conversion_rate, 2); ?></td>
                        </tr>
                        <tr>
                            <td class="description">Shipping Cost</td>
                            <td class="hours"></td>
                            <td class="amount"><?php echo  $currency_symbol;?><?php echo number_format($base_shipping_cost*$conversion_rate, 2); ?></td>
                        </tr>
                        <tr>
                            <td class="description">Shipping Cost Tax </td>
                            <td class="hours"></td>
                            <td class="amount"><?php echo  $currency_symbol;?><?php echo number_format($shipping_cost_tax*$conversion_rate, 2); ?></td>
                        </tr>
                        <tr>
                            <td class="description">Handling Cost</td>
                            <td class="hours"></td>
                            <td class="amount"><?php echo  $currency_symbol;?><?php echo number_format($base_handling_cost*$conversion_rate, 2); ?></td>
                        </tr>
                        <tr>
                            <td class="description">Handling Cost Tax </td>
                            <td class="hours"></td>
                            <td class="amount"><?php echo  $currency_symbol;?><?php echo number_format($handling_cost_tax*$conversion_rate, 2); ?></td>
                        </tr>
                        <tr>
                            <td class="description">Wrapping Cost</td>
                            <td class="hours"></td>
                            <td class="amount"><?php echo  $currency_symbol;?><?php echo number_format($base_wrapping_cost*$conversion_rate, 2); ?></td>
                        </tr>
                        <tr>
                            <td class="description">Wrapping Cost Tax</td>
                            <td class="hours"></td>
                            <td class="amount"><?php echo  $currency_symbol;?><?php echo number_format($wrapping_cost_tax*$conversion_rate, 2); ?></td>
                        </tr>
                        <tr>
                            <td class="description">Credit Card Fees</td>
                            <td class="hours"></td>
                            <td class="amount"><?php echo  $currency_symbol;?><?php echo number_format($creditcard_fees*$conversion_rate, 2); ?></td>
                        </tr>
                        <tr>
                            <td class="description">Cost</td>
                            <td class="hours"></td>
                            <td class="amount"><?php echo  $currency_symbol;?><?php echo number_format($total_amount*$conversion_rate, 2); ?></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="summary">Subtotal</td>
                            <td class="amount"><?php echo  $currency_symbol;?><?php echo number_format($subtotal*$conversion_rate, 2); ?></td>
                        </tr>

<!--                        <tr>
     <td></td>
     <td class="summary">Discount (20%)</td>
     <td class="amount">$1,480,00</td>
 </tr>-->
                        <tr>
                            <td></td>
                            <td class="summary total">Total</td>
                            <td class="amount total-value"><?php echo  $currency_symbol;?><?php echo number_format($subtotal*$conversion_rate, 2); ?></td>
                        </tr>
                    </table>
                </div>
            </div>
            <!--            <div class="row">
                            <div class="col-md-12 invoice-payment-method"><span class="title">Payment Method</span><span>Credit card</span><span>Credit card type: mastercard</span><span>Number verification: 4256981387</span></div>
                        </div>-->
            <!--            <div class="row">
                            <div class="col-md-12 invoice-message"><span class="title">Thank you for contacting us</span>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quis massa nisl. Sed fringilla turpis id mi ultrices, et faucibus ipsum aliquam. Sed ut eros placerat, facilisis est eu, congue felis.</p>
                            </div>
                        </div>-->
            <!--            <div class="row invoice-company-info">
                            <div class="col-sm-6 col-md-2 logo"><img src="assets/img/logo-symbol.png" alt="Logo-symbol"></div>
                            <div class="col-sm-6 col-md-4 summary"><span class="title">Beagle Company</span>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                            </div>
                            <div class="col-sm-6 col-md-3 phone">
                                <ul class="list-unstyled">
                                    <li>+1(535)-8999278</li>
                                    <li>+1(656)-3558302</li>
                                </ul>
                            </div>
                            <div class="col-sm-6 col-md-3 email">
                                <ul class="list-unstyled">
                                    <li>beagle@company.co</li>
                                    <li>beagle@support.co</li>
                                </ul>
                            </div>
                        </div>-->
            <div class="row invoice-footer">
                <div class="col-md-12" id="pdf">
                    <a target="_blank" href="/orders/savepdf/<?=$order_id;?>"><button class="btn btn-lg btn-space btn-primary" >Save PDF</button></a>
                    <button class="btn btn-lg btn-space btn-primary" onclick="printFunction()">Print</button>
                </div>
            </div>
        </div>
    </div>
</div>
