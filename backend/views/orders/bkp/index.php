<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use backend\models\Orders;
use backend\models\Stores;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\OrdersSerach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;



?>

<div class="page-head">
    <h2 class="page-head-title"><?= Html::encode($this->title) ?></h2>
    <ol class="breadcrumb page-head-nav">
        <?php
        echo Breadcrumbs::widget([
          'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
        ?>
    </ol>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default panel-table">
            <div class="panel-heading">
                <div class="tools">
                    <a href="<?php //echo $new_basepath  ?>/export-user"><span class="icon mdi mdi-download"></span></span></a>
                    <span class="icon mdi mdi-more-vert"></span>
                </div>
            </div>
            <div class="panel-body">
                <table id="channels_view_table" class="table table-striped table-hover table-fw-widget">
                    <thead>
                        <tr>
                            <th>Order #</th>
                            <th>Customer Name</th>
                            <th>Channel Sold On</th>
                            <th>Order Value</th>
                            <th>Date Ordered</th>
                            <th>Ship to City and Country</th>
                            <th>Status</th>
                            <th>Shipped</th>   
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        /* Get Orders data */
                        $orders_data = Orders::find()->with('customer')->all();
                        
                        if(!empty($orders_data)) :
                            foreach ($orders_data as $orders_data_value) :
                            $check='';
                        $channel_abb_id = isset($orders_data_value->channel_abb_id) ? $orders_data_value->channel_abb_id : "";
                        $channel_explode= explode('_', $channel_abb_id);
                        $channel_name=$channel_explode[0];
                        //get Store image
                        $stores=Stores::find()->Where(['store_name'=>'BigCommerce'])->one();
                        $channel_img=$stores->store_image;
                        $order_amount = isset($orders_data_value->total_amount) ? $orders_data_value->total_amount : "";
                        $order_value= number_format((float)$order_amount, 2, '.', '');  
                        $firstname= ucfirst(strtolower($orders_data_value->customer->first_name));
                        $lname= ucfirst(strtolower($orders_data_value->customer->last_name));
                        $date_order = date('m/d/y h:i A', strtotime($orders_data_value->created_at));
                        $city=$orders_data_value->customer->city ?ucfirst(strtolower($orders_data_value->customer->city)) :"" ;
                        $country=$orders_data_value->customer->country ?ucfirst(strtolower($orders_data_value->customer->country)) :"" ;
                        
                        $order_status=$orders_data_value->order_status;
                        
                       if($order_status=='Completed') :
                           $label='label-success';
                       endif;
                       
                       if($order_status=='Returned' || $order_status=='Refunded') :
                           $label='label-danger';
                       endif;
                       
                       if($order_status=='In Transit'):
                           $label='label-default';
                       endif;
                       
                       if($order_status=='Awaiting FulFillment'):
                           $label='label-Warning';
                       endif;
                       if($order_status=='Shipped'):
                           $label='label-default';
                           $check='checked';
                       endif;
                      
                        ?>
                          <tr class="odd gradeX">
                              <td><?php echo $channel_abb_id;?></td>
                              <td><?php echo $firstname.' '.$lname;?></td>
                              <td><img class="ch_img" src=<?php echo $channel_img; ?> width="50" height="50" alt="Channel Image"></td>
                              <td>$<?php echo $order_value;?></td>
                              <td><?php echo $date_order;?></td>
                              <td><?php echo $city.','.$country;?></td>
                              <td><span class="label label-default <?php echo $label; ?>"><?php echo $order_status;?></span></td>
                              <td>
                                  <div class="be-radio">
                                    <input type="radio" <?php echo $check;?> name="rad2<?php echo $channel_abb_id;?>" id="rad3<?php echo $channel_abb_id;?>">
                                    <label for="rad4<?php echo $channel_abb_id;?>"></label>
                                  </div>
                              </td>
                          </tr>
                          <?php
                        endforeach;
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
