<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ProductAttribution */

$this->title = 'Update Product Attribution: ' . $model->product_attribution_id;
$this->params['breadcrumbs'][] = ['label' => 'Product Attributions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->product_attribution_id, 'url' => ['view', 'id' => $model->product_attribution_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-attribution-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
