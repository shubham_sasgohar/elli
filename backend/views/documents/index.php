<?php

use backend\models\User;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use backend\models\Channels;
use backend\models\CorporateDocuments;

$base_url = Yii::getAlias('@baseurl');

$this->title = 'Corporate Documents';
$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => 'javascript: void(0)', 'class' => 'non_link'];
$this->params['breadcrumbs'][] = $this->title;
//$this->params['breadcrumbs'][] = ['label' => 'Documents', 'url' => ['index']];
?>

<div class="page-head">
    <h2 class="page-head-title"><?= Html::encode($this->title) ?></h2>
    <ol class="breadcrumb page-head-nav">
        <?php
        echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
        ?>
    </ol>
</div>
<div class="row">
    <!--Default Tabs-->
    <div class="col-sm-12">

        <div class="panel panel-default">
            <div class="tab-container">

                <ul class="nav nav-tabs">
                    <li class="active customCC"><a href="#banking" data-toggle="tab">Banking</a></li>
                    <li class="customCC"><a href="#business" data-toggle="tab">Business</a></li>
                    <li class="customCC"><a href="#directors" data-toggle="tab">Directors</a></li>
                    <li class="customCC"><a href="#payments" data-toggle="tab">Payments</a></li>
                </ul>
                <div class="tab-content product_details">
                    <div id="banking" class="tab-pane active cont">
                        <div class="table-responsive">
                            <table id="general_tbl" style="clear: both" class="table table-striped table-borderless">
                                <tbody>
                                    <tr>
                                        <td width="35%">Account No.</td>
                                        <td width="65%"><a class="documents_fields" id="account_no" href="#" data-type="text" data-title="Please Enter value"></a></td>
                                    </tr>
                                    <tr>
                                        <td width="35%">Routing No.</td>
                                        <td width="65%"><a class="documents_fields"  id="routing_no" href="#" data-type="text" data-title="Please Enter value" ></a></td>
                                    </tr>
                                    <tr>
                                        <td width="35%">Bank Code</td>
                                        <td width="65%"><a class="documents_fields" id="bank_code" href="#" data-type="text" data-title="Please Enter value" ></a></td>
                                    </tr>
                                    <tr>
                                        <td width="35%">Bank Name</td>
                                        <td width="65%"><a class="documents_fields" id="bank_name"  href="#" data-type="text" data-title="Please Enter value"></a></td>
                                    </tr>
                                    <tr>
                                        <td width="35%">Bank Address</td>
                                        <td width="65%"><a class="documents_fields"  id="bank_address"  href="#" data-type="textarea" data-title="Please Enter value"></a></td>
                                    </tr>
                                    <tr>
                                        <td width="35%">SWIFT</td>
                                        <td width="65%"><a class="documents_fields"  id="swift"  href="#" data-type="text" data-title="Please Enter value "></a></td>
                                    </tr>
                                    <tr>
                                        <td width="35%">Account Type</td>
                                        <td width="65%"><a class="documents_fields_account_type" id="account_type"  href="#" data-type="select" data-title="Please Enter value (must be unique)"></a></td>
                                    </tr>
                                    <tr>
                                        <td width="35%">Latest Bank Statement</td>
                                        <td width="65%"></td>
                                    </tr>
                                </tbody>
                            </table>



                            <div class="main-content container-fluid">
                                <form id="bankingform" action="" class="dropzone">
                                    <div class="dz-message">
                                        <div class="icon"><span class="mdi mdi-cloud-upload"></span></div>
                                        <h2>Drag and Drop files here</h2>
<!--                                        <span class="note">(This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)</span>-->
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div id="business" class="tab-pane cont">
                        <div class="table-responsive">
                            <input type="hidden" id="gcatname" value="" />
                            <table id="attr_tbl" style="clear: both" class="table table-striped table-borderless">
                                <tbody>
                                    <tr>
                                        <td width="35%">Tax ID</td>
                                        <td width="65%"><a class="documents_fields" id="tax_id"  href="#" data-type="text" data-title="Please Enter value"></a></td>
                                    </tr>
                                    <tr>
                                        <td width="35%">Articles of Incorporation</td>
                                        <td width="65%"></td>
                                    </tr>

                                </tbody>
                            </table>
                            <div class="main-content container-fluid">
                                <form id="businessform" action="" class="dropzone ">
                                    <div class="dz-message">
                                        <div class="icon"><span class="mdi mdi-cloud-upload"></span></div>
                                        <h2>Drag and Drop files here</h2>
<!--                                        <span class="note">(This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)</span>-->
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div id="directors" class="tab-pane">
                        <div id="accordionChannelS" class="panel-group accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" class="collapsed" data-parent="#accordionChannelS" href="#director_0"><i class="icon mdi mdi-chevron-down"></i>Director 1</a></h4>
                                </div>
                                <div id="director_0" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="table-responsive ">
                                            <label> Director </label>
                                            <input type="hidden" class="hidden_directors_file" id="hidden_directors_file_id_0" data-index="0">
                                            <table id="cats_tbl" style="clear: both" class="table table-striped table-borderless">
                                                <tbody>
                                                    <tr>
                                                        <td width="35%"> First name</td>
                                                        <td width="65%"><a id="" class="documents_fields first_name" href="#" data-type="text" data-title="Please Enter value"></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%"> Last  name</td>
                                                        <td width="65%"><a  id="last_name"class="documents_fields last_name" href="#" data-type="text" data-title="Please Enter value"></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%"> Date of Birth</td>
                                                        <td width="65%"><a class="documents_fields_dob dob" href="#"  data-pk="1" data-template="D / MMM / YYYY" data-viewformat="DD/MM/YYYY" data-format="YYYY-MM-DD"data-type="combodate" data-title="Please Enter value"></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%"> Address</td>
                                                        <td width="65%"><a id="" class="documents_fields address" href="#" data-type="text" data-title="Please Enter value"></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%"> Last 4 of Social</td>
                                                        <td width="65%"> 
                                                            <input type="text" data-mask="last_4_social"  id="last_4_social" placeholder="9999" class="form-control last_4_social"></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="main-content container-fluid">
                                                <form  action="" class="dropzone directorsform">
                                                    <div class="dz-message">
                                                        <div class="icon"><span class="mdi mdi-cloud-upload"></span></div>
                                                        <h2>Drag and Drop files here</h2>
<!--                                                        <span class="note">(This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)</span>-->
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button class="btn btn-space btn-primary addMoreDirectors pull-right" >+Add More</button>
                    </div>
                    <div id="payments" class="tab-pane">

                        <div id="accordionChannelPayment" class="panel-group accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" class="collapsed" data-parent="#accordionChannelPayment" href="#alipay"><i class="icon mdi mdi-chevron-down"></i>Ali Pay</a></h4>
                                </div>
                                <div id="alipay" class="panel-collapse collapse">
                                    <div class="panel-body">

                                        <div class="table-responsive">
                                            <table id="" style="clear: both" class="table table-striped table-borderless">
                                                <tbody>
                                                    <tr>
                                                        <td width="35%">Account #</td>
                                                        <td width="65%"><a class="documents_fields" id="alipay_payment_account_no" href="#" data-type="text" data-title="Please Enter value"></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">Account ID</td>
                                                        <td width="65%"><a class="documents_fields"  id="alipay_payment_account_id" href="#" data-type="text" data-title="Please Enter value" ></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">Account Email</td>
                                                        <td width="65%"><a class="documents_fields" id="alipay_payment_account_email" href="#" data-type="email" data-title="Please Enter value" ></a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="panel-heading profile-panel-heading"> 
                                                <div class="title">Account Address</div>
                                            </div>
                                            <table id="general_tbl" style="clear: both" class="table table-striped table-borderless">
                                                <tbody>
                                                    <tr>
                                                        <td width="35%">Address 1</td>
                                                        <td width="65%"><a class="documents_fields" id="alipay_payment_address_1" href="#" data-type="text" data-title="Please Enter value"></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">Address 2</td>
                                                        <td width="65%"><a class="documents_fields"  id="alipay_payment_address_2" href="#" data-type="text" data-title="Please Enter value" ></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">City</td>
                                                        <td width="65%"><a id="alipay_payment_account_city" data-title="Start typing City.." data-placement="right" data-pk="1" data-type="typeaheadjs" href="javascript:"  class="payment_account_city"></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">State</td>
                                                        <td width="65%"><a  id="alipay_payment_account_state" data-title="Start typing State.." data-placement="right" data-pk="1" data-type="typeaheadjs" href="javascript:"  data-type="email" class="payment_account_state" ></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">Country</td>
                                                        <td width="65%"><a  id="alipay_payment_account_country" data-title="Start typing Country.." data-placement="right" data-pk="1" data-type="typeaheadjs" href="javascript:"  class="payment_account_country"></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">Zip Code</td>
                                                        <td width="65%"><a class="documents_fields" id="alipay_payment_account_zip_code" href="#" data-type="text" data-title="Please Enter value" ></a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" class="collapsed" data-parent="#accordionChannelPayment" href="#dinpay"><i class="icon mdi mdi-chevron-down"></i>DinPay</a></h4>
                                </div>
                                <div id="dinpay" class="panel-collapse collapse">
                                    <div class="panel-body">

                                        <div class="table-responsive">
                                            <table id="" style="clear: both" class="table table-striped table-borderless">
                                                <tbody>
                                                    <tr>
                                                        <td width="35%">Account #</td>
                                                        <td width="65%"><a class="documents_fields" id="dinpay_payment_account_no" href="#" data-type="text" data-title="Please Enter value"></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">Account ID</td>
                                                        <td width="65%"><a class="documents_fields"  id="dinpay_payment_account_id" href="#" data-type="text" data-title="Please Enter value" ></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">Account Email</td>
                                                        <td width="65%"><a class="documents_fields" id="dinpay_payment_account_email" href="#" data-type="email" data-title="Please Enter value" ></a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="panel-heading profile-panel-heading"> 
                                                <div class="title">Account Address</div>
                                            </div>
                                            <table id="" style="clear: both" class="table table-striped table-borderless">
                                                <tbody>
                                                    <tr>
                                                        <td width="35%">Address 1</td>
                                                        <td width="65%"><a class="documents_fields" id="dinpay_payment_address_1" href="#" data-type="text" data-title="Please Enter value"></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">Address 2</td>
                                                        <td width="65%"><a class="documents_fields"  id="dinpay_payment_address_2" href="#" data-type="text" data-title="Please Enter value" ></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">City</td>
                                                        <td width="65%"><a id="dinpay_payment_account_city" data-title="Start typing City.." data-placement="right" data-pk="1" data-type="typeaheadjs" href="javascript:"  class="payment_account_city"></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">State</td>
                                                        <td width="65%"><a  id="dinpay_payment_account_state" data-title="Start typing State.." data-placement="right" data-pk="1" data-type="typeaheadjs" href="javascript:"  data-type="email" class="payment_account_state" ></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">Country</td>
                                                        <td width="65%"><a  id="dinpay_payment_account_country" data-title="Start typing Country.." data-placement="right" data-pk="1" data-type="typeaheadjs" href="javascript:"  class="payment_account_country"></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">Zip Code</td>
                                                        <td width="65%"><a class="documents_fields" id="dinpay_payment_account_zip_code" href="#" data-type="text" data-title="Please Enter value" ></a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" class="collapsed" data-parent="#accordionChannelPayment" href="#payoneer"><i class="icon mdi mdi-chevron-down"></i>Payoneer</a></h4>
                                </div>
                                <div id="payoneer" class="panel-collapse collapse">
                                    <div class="panel-body">

                                        <div class="table-responsive">
                                            <table id="" style="clear: both" class="table table-striped table-borderless">
                                                <tbody>
                                                    <tr>
                                                        <td width="35%">Account #</td>
                                                        <td width="65%"><a class="documents_fields" id="payoneer_payment_account_no" href="#" data-type="text" data-title="Please Enter value"></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">Account ID</td>
                                                        <td width="65%"><a class="documents_fields"  id="payoneer_payment_account_id" href="#" data-type="text" data-title="Please Enter value" ></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">Account Email</td>
                                                        <td width="65%"><a class="documents_fields" id="payoneer_payment_account_email" href="#" data-type="email" data-title="Please Enter value" ></a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="panel-heading profile-panel-heading"> 
                                                <div class="title">Account Address</div>
                                            </div>
                                            <table id="general_tbl" style="clear: both" class="table table-striped table-borderless">
                                                <tbody>
                                                    <tr>
                                                        <td width="35%">Address 1</td>
                                                        <td width="65%"><a class="documents_fields" id="payoneer_payment_address_1" href="#" data-type="text" data-title="Please Enter value"></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">Address 2</td>
                                                        <td width="65%"><a class="documents_fields"  id="payoneer_payment_address_2" href="#" data-type="text" data-title="Please Enter value" ></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">City</td>
                                                        <td width="65%"><a id="payoneer_payment_account_city" data-title="Start typing City.." data-placement="right" data-pk="1" data-type="typeaheadjs" href="javascript:"  class="payment_account_city"></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">State</td>
                                                        <td width="65%"><a  id="payoneer_payment_account_state" data-title="Start typing State.." data-placement="right" data-pk="1" data-type="typeaheadjs" href="javascript:"  data-type="email" class="payment_account_state" ></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">Country</td>
                                                        <td width="65%"><a  id="payoneer_payment_account_country" data-title="Start typing Country.." data-placement="right" data-pk="1" data-type="typeaheadjs" href="javascript:"  class="payment_account_country"></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">Zip Code</td>
                                                        <td width="65%"><a class="documents_fields" id="payoneer_payment_account_zip_code" href="#" data-type="text" data-title="Please Enter value" ></a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" class="collapsed" data-parent="#accordionChannelPayment" href="#worldfirst"><i class="icon mdi mdi-chevron-down"></i>WorldFirst</a></h4>
                                </div>
                                <div id="worldfirst" class="panel-collapse collapse">
                                    <div class="panel-body">

                                        <div class="table-responsive">
                                            <table id="" style="clear: both" class="table table-striped table-borderless">
                                                <tbody>
                                                    <tr>
                                                        <td width="35%">Account #</td>
                                                        <td width="65%"><a class="documents_fields" id="worldfirst_payment_account_no" href="#" data-type="text" data-title="Please Enter value"></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">Account ID</td>
                                                        <td width="65%"><a class="documents_fields"  id="worldfirst_payment_account_id" href="#" data-type="text" data-title="Please Enter value" ></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">Account Email</td>
                                                        <td width="65%"><a class="documents_fields" id="worldfirst_payment_account_email" href="#" data-type="email" data-title="Please Enter value" ></a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <div class="panel-heading profile-panel-heading"> 
                                                <div class="title">Account Address</div>
                                            </div>
                                            <table id="general_tbl" style="clear: both" class="table table-striped table-borderless">
                                                <tbody>
                                                    <tr>
                                                        <td width="35%">Address 1</td>
                                                        <td width="65%"><a class="documents_fields" id="worldfirst_payment_address_1" href="#" data-type="text" data-title="Please Enter value"></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">Address 2</td>
                                                        <td width="65%"><a class="documents_fields"  id="worldfirst_payment_address_2" href="#" data-type="text" data-title="Please Enter value" ></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">City</td>
                                                        <td width="65%"><a id="worldfirst_payment_account_city" data-title="Start typing City.." data-placement="right" data-pk="1" data-type="typeaheadjs" href="javascript:"  class="payment_account_city"></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">State</td>
                                                        <td width="65%"><a  id="worldfirst_payment_account_state" data-title="Start typing State.." data-placement="right" data-pk="1" data-type="typeaheadjs" href="javascript:"  data-type="email" class="payment_account_state" ></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">Country</td>
                                                        <td width="65%"><a  id="worldfirst_payment_account_country" data-title="Start typing Country.." data-placement="right" data-pk="1" data-type="typeaheadjs" href="javascript:"  class="payment_account_country"></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="35%">Zip Code</td>
                                                        <td width="65%"><a class="documents_fields" id="worldfirst_payment_account_zip_code" href="#" data-type="text" data-title="Please Enter value" ></a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab_up_btn_div custOMPerf">

                        <button class="btn btn-space btn-primary" onclick="addUpdateDocuments();">Update</button>
                    </div>
					
					<div id="documentssaved" tabindex="-1" role="dialog" style="display: none;" class="modal fade">
						<div class="modal-dialog">
						  <div class="modal-content">
							<div class="modal-header">
							  <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
							</div>
							<div class="modal-body">
							  <div class="text-center">
								<div class="text-success"><span class="modal-main-icon mdi mdi-check"></span></div>
								<h3>Success!</h3>
								<p>Data has been saved successfully.</p>
								<div class="xs-mt-50">
								  <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Close</button>
								</div>
							  </div>
							</div>
							<div class="modal-footer"></div>
						  </div>
						</div>
					</div>
		  
                </div>
            </div>

        </div>
    </div> 
