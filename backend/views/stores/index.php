<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\Breadcrumbs;
use backend\models\Stores;
use backend\models\Channels;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\StoresSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Connected Channels';
$this->params['breadcrumbs'][] = $this->title;

if ($notify == 'true') :
  ?>
  <!--app ui sticky notification check-->
  <script>
    var import_done = '<?php echo $notify; ?>';
    setTimeout(function () {
        jQuery(function () {
            sticky_ntf_bigcommerce_import(import_done)
        });
    }, 3000);
  </script>
<?php endif; ?>

<div class="page-head">
    <h2 class="page-head-title"><?= Html::encode($this->title) ?></h2>
    <ol class="breadcrumb page-head-nav">
        <?php
        echo Breadcrumbs::widget([
          'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
        ?>
    </ol>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default panel-table">
            <div class="panel-heading"><?php echo $this->title; ?>
                <div class="tools"><span class="icon mdi mdi-download"></span><span class="icon mdi mdi-more-vert"></span></div>
            </div>
            <div class="panel-body">
                <table id="products_table" class="table table-striped table-hover table-fw-widget">
                    <thead>
                        <tr>
                            <th>Store Type</th>
                            <th>Store Hash</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($store_connection)) :
                            
                          foreach ($store_connection['store'] as $store) :
                            $st_id = $store->store_id;
                            $store_type = Stores::find()->where(['store_id' => $st_id])->one();
                            ?>
                            <tr class="store_row">
                                <td><a href="stores/channeldisable"><?= $store_type->store_name ?></a></td>
                                <td><a href="stores/channeldisable"><?= $store->big_store_hash ?></a></td>
                                <td>Connected</td>
                            </tr>
                            <?php
                          endforeach;
                          
                          foreach ($store_connection['channel'] as $channel) :
                               
                            $st_id = $channel->channel_id;
                            $channel_type = Channels::find()->where(['channel_id' => $st_id])->one();
                            $parent=explode("_",$channel_type->parent_name);
                       //     echo "<pre>";
                       // print_r($channel_type);
                       // echo "</pre>";
                       // print_r($parent);
                            ?>
                            <tr class="store_row">
                                <td><a href="stores/channeldisable"><?php if($parent[1] == 'Facebook') { echo 'Facebook Product Feed'; } elseif($channel_type->channel_name == 'Google shopping') {echo 'Google Merchant Feed';} else { echo $parent[1]." - ".$channel_type->channel_name; } ?></a></td>
                                <td>--</td>
                                <td>Connected</td>
                            </tr>
                            <?php
                          endforeach;
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>





