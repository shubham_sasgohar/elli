<?php

use backend\models\User;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use backend\models\Channels;
use backend\models\CorporateDocuments;

$base_url = Yii::getAlias('@baseurl');
$this->title = 'Corporate Documents';
$username = Yii::$app->user->identity->username;
$this->params['breadcrumbs'][] = $this->title;

$user_id = Yii::$app->user->identity->id;
$show_channels = array(
    "1" => "MercadoLibre",
    "2" => "Jumia",
    "3" => "WeChat",
    "4" => "Lazada",
    "5" => "Shopee",
    "6" => "Xiao Hong Shu"
);
?>

<div class="page-head">
    <h2 class="page-head-title"><?= Html::encode($this->title) ?></h2>
    <ol class="breadcrumb page-head-nav">
        <?php
        echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
        ?>
    </ol>
</div>

<!--Input File-->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default panel-border-color panel-border-color-primary">
            <div class="panel-heading panel-heading-divider"><span class="panel-subtitle">Upload Your Corporate Documents</span></div>
            <div class="panel-body">
                <form action="#" style="border-radius: 0px;" class="form-horizontal group-border-dashed" id="corporate_form_id">

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Business License</label>
                        <div class="col-sm-6">
                            <input type="file" name="file-2" id="file-2" data-multiple-caption="{count} files selected" multiple class="inputfile">
                            <label for="file-2" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Business Incorporation Papers</label>
                        <div class="col-sm-6">
                            <input type="file" name="file-3" id="file-3" data-multiple-caption="{count} files selected" multiple class="inputfile">
                            <label for="file-3" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tax ID</label>
                        <div class="col-sm-6">
                            <a id="TaxID" href="#" data-type="text" data-title="Enter username" class="editable editable-click" data-original-title="" title=""></a>
                            <input class="form-control" name="tax_id" id="tax_val" value="" type="hidden">
                        </div> 
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Channels</label>
                        <div class="col-sm-2">
                            <select class="form-control" name="channel_name">
                                <option value=""></option>
                                <option value="MercadoLibre">MercadoLibre</option>
                                <option value="Jumia">Jumia</option>
                                <option value="WeChat">WeChat</option>
                                <option value="Lazada">Lazada</option>
                                <option value="Tokopedia">Tokopedia</option>
                                <option value="Shopee">Shopee</option>
                                <option value="Xiao Hong">Xiao Hong</option>
                            </select>
                        </div>

                    </div>
                    <button type="submit" class="btn btn-space btn-primary" id="corporate_id">Submit</button>
                    <div class="form-group">

                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

 
<!--<div class="main-content container-fluid">
    <div class="row marketplace-listing">
        <?php //foreach ($show_channels as $corporate_channel): $channel_name_org = ucwords(str_replace(" ", "-", $corporate_channel)); ?>
            <?php //$marketplace = Channels::find()->Where(['parent_name' => 'channel_' . $channel_name_org])->one(); ?>
            <div class="col-sm-3">
                <div class="bs-grid-block" data-toggle="modal"  data-target="#<? //= $marketplace->stripe_Channel_id ?>">
                    <div class="content">
                        <span class="label label-success custom-span-channels-label span-top-22" style=" position: absolute; right: 30px; "></span>
                        <img src="<? //=$marketplace->channel_image; ?>"style="width: 100% ;" class="<? //= $marketplace->stripe_Channel_id == "channel_Xiao_Hong_Shu" ? 'resize_Xiao' : ''; ?>" alt="<?php //echo $marketplace->stripe_Channel_id; ?>">
                    </div>
                </div>
            </div>
        <?php //endforeach; ?>
    </div>
</div>-->

<!--Channel Model Here!-->
<?php foreach ($show_channels as $corporate_channel1): $channel_name_org1 = ucwords(str_replace(" ", "-", $corporate_channel1)); ?>
    <?php $marketplace1 = Channels::find()->Where(['parent_name' => 'channel_' . $channel_name_org1])->one(); ?>
    <?php $corporate_data = CorporateDocuments::find()->Where(['elliot_user_id' => $user_id, 'channel' => $corporate_channel1])->one(); ?>
    <?php if (empty($corporate_data)): ?>
        <div id="<?= $marketplace1->stripe_Channel_id ?>" tabindex="-1" role="dialog" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                    </div>
                    <div class="modal-body">
                        <div class="text-center">
                            <div class="text-primary"><img src="<?php echo $marketplace1->channel_image; ?>" style="width: 50%" alt=""></span></div>
                        </div>
                    </div>
                    <div class="modal-footer"></div>
                </div>
            </div>
        </div>

    <?php else: ?>
        <div id="<?= $marketplace1->stripe_Channel_id ?>" tabindex="-1" role="dialog" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                    </div>
                    <div class="modal-body">
                        <div class="text-center">
                            <div class="text-primary"><img src="<?php echo $marketplace1->channel_image; ?>" style="width: 50%" alt=""></span></div>
                        </div>
                        <div class="col-md-12">
                            <div class="bs-grid-block">
                                <div class="col-sm-4">
                                    <div class="icon-container">
                                        <span class="icon-class">Business License</span>
                                        <div class="icon"><a href="<?= Yii::$app->params['BASE_URL'].$corporate_data->Business_License;?>" download="Buisness license"><span class="mdi mdi-download"></span></a></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="icon-container">
                                        <span class="icon-class">Business Incorporation</span>
                                        <div class="icon"><a href="<?= Yii::$app->params['BASE_URL'].$corporate_data->Business_Papers;?>" download="Buisness license"><span class="mdi mdi-download"></span></a></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="icon-container">
                                        <span class="icon-class">Tax ID</span>
                                        <div> <span><b><?= $corporate_data->Tax_ID; ?></b></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer"></div>
                </div>
            </div>
        </div>
    <?php endif; ?>

<?php endforeach; ?>

<!-- Modal code here -->
<div id="corporate_ajax_request" tabindex="-1" role="dialog" class="modal fade in">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close corporate_modal_close"></span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="text-success"><span class="modal-main-icon mdi mdi-check"></span></div>
                    <h3 id="ajax_header_msg">Success!</h3>
                    <p id="corporate_ajax_msg"></p>
                    <div class="xs-mt-50">
                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default corporate_modal_close">Close</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<div id="mod-danger" tabindex="-1" role="dialog" class="modal fade in corporate_ajax_request_error" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close corporate_error_modal_close"></span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
                    <h3 id='ajax_header_error_msg'></h3>
                    <p id="corporate_ajax_msg_eror"></p>
                    <div class="xs-mt-50">
                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default corporate_error_modal_close">Close</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>



