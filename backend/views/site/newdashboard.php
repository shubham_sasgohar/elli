<?php
/* @var $this yii\web\View */

use backend\models\Channels;
use yii\db\Query;
use yii\helpers\Html;
use yii\web\Session;
use backend\models\Orders;

$this->title = 'Dashboard | Elliot';
?>
<!--*********************************1st ROW GRAPH line chart********************************************************************************************-->
<div class="row">
    <div class="form-group">
        <div class="col-md-4">
            <input type="hidden" name="daterange" value="01/01/2015 - 01/31/2015" class="form-control daterange">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="tools">
            <div class="dropdown"><span data-toggle="dropdown" class="icon mdi mdi-more-vert visible-xs-inline-block dropdown-toggle" aria-expanded="false"></span>
                <ul role="menu" class="dropdown-menu">
                    <li><a  class="btn btn-default newdashchart "  data="week" style="cursor:pointer">Week</a></li>
                    <li><a  class="btn btn-default newdashchart " data="month" style="cursor:pointer">Month</a></li>
                    <li><a  class="btn btn-default newdashchart " data="Quarter" style="cursor:pointer">Quarter</a></li>
                    <li><a   class="btn btn-default" style="cursor:pointer">Date Range</a></li>
                    <li class="divider"></li>
                    <li><a  class="btn btn-default newdashchart"  data="today" style="cursor:pointer">Today</a></li>
                </ul>
            </div>
        </div>
        <div class="button-toolbar hidden-xs">
            <div class="btn-group">
                <button type="button" class="btn btn-default newdashchart week" data="week">Week</button>
                <button type="button" class="btn btn-default newdashchart month active" data="month">Month</button>
                <button type="button" class="btn btn-default newdashchart Quarter" data="Quarter">Quarter</button>
                <button id="daterange" type="button" class="btn btn-default dateRange">Date Range</button>
                <input name="" value="Week" id="hidden_graph" type="hidden">
            </div>
            <div class="btn-group">
                <button type="button" class="btn btn-default newdashchart today" data="today">Today</button>
            </div>
        </div>
    </div>
</div>
<div class='row'>
    <!--#########################REVENUE CHART START HERE###################################################-->
    <div class="col-sm-6">
        <div class="Loader_Reven be-loading be-loading-active">
            <div class="panel panel-default projection_slider_contaner">
                <div class="panel-heading panel-heading-divider">
                    <div class="widget-head">
                        <span class="title">Revenue <span id="updownrevenue"></span></span> 
                    </div>
                    <div class="be-spinner">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 66 66" height="40px" width="40px">

                        <circle class="circle" r="25" cy="33" cx="33" stroke-linecap="round" stroke-width="4" fill="none"/>
                        </svg>
                    </div>

                    <?php
                    $order_amount_total = Orders::find()->Where(['elliot_user_id' => Yii::$app->user->identity->id])->sum('total_amount');
                    $annual_revenue = Yii::$app->user->identity->annual_revenue;

                    if (!empty($annual_revenue) and Yii::$app->user->identity->annual_revenue != '') {
                        $fraction = $order_amount_total / $annual_revenue;
                        $percentage = $fraction * 100;
                        $percentage = number_format((float) $percentage, 0, ".", '');
                        ?>
                        <div class="progress-widget">
                            <div class="progress-data"><span class="name">Revenue Projection</span><span class="progress-value"><?php echo $percentage; ?>%</span></div>
                            <div class="progress">
                                <div style="width: <?php echo $percentage; ?>%;" class="progress-bar progress-bar-primary"></div>
                            </div>
                        </div>

                    <?php } else { ?>

                        <a href="/general"><button class="btn btn-space btn-default"><i class="icon icon-left mdi mdi-globe"></i> Activate Revenue Targeting</button></a>
                    <?php }
                    ?>
                    <span class="panel-subtitle space"><ul style="text-align:center;" id="revchartInfo" class="chart-legend-horizontal_data">
                        </ul>
                    </span
                </div>
                <div class="panel-body">
                    <div id="line-chart1" class='line-chart1'style="height: 250px;"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--#########################ORDERS CHART START HERE###################################################-->
<div class="col-sm-6">
    <div class="Loader_Orders  be-loading be-loading-active">
        <div class="panel panel-default projection_slider_contaner">
            <div class="panel-heading panel-heading-divider">
                <div class="widget-head">
                    <span class="title">Orders  <span id="updownorders"></span></span>  
                </div>
                <div class="be-spinner">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 66 66" height="40px" width="40px">

                    <circle class="circle" r="25" cy="33" cx="33" stroke-linecap="round" stroke-width="4" fill="none"/>
                    </svg>
                </div>
                <?php
                $order_count_total = Orders::find()->Where(['elliot_user_id' => Yii::$app->user->identity->id])->count('order_ID');
                $annual_order = Yii::$app->user->identity->annual_order_target;

                if (!empty($annual_order) and Yii::$app->user->identity->annual_order_target != '') {
                    $fraction = $order_count_total / $annual_order;
                    $order_percentage = $fraction * 100;
                    $order_percentage = number_format((float) $order_percentage, 0, ".", '');
                    ?>
                    <div class="progress-widget">
                        <div class="progress-data"><span class="name">Orders Projection</span><span class="progress-value"><?php echo $order_percentage; ?>%</span></div>
                        <div class="progress">
                            <div style="width: <?php echo $order_percentage; ?>%;" class="progress-bar progress-bar-primary"></div>
                        </div>
                    </div>

                <?php } else { ?>

                    <a href="/general"><button class="btn btn-space btn-default"><i class="icon icon-left mdi mdi-globe"></i> Activate Orders Targeting</button></a>
                <?php } ?>
                <span class="panel-subtitle space"><ul style="text-align:center;"id="OrderchartInfo" class="chart-legend-horizontal_data">
                    </ul>
                </span
            </div>
            <div class="panel-body">
                <div id="line-chart2" class='line-chart2'style="height: 250px;"></div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!--*********************************2rd ROW GRAPH pie chart********************************************************************************************-->
<div class="row">
    <div class="col-md-6">
        <div class="Loader_Rev_Ch be-loading be-loading-active">
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-divider">
                    <div class="tools"><span class="icon mdi mdi-chevron-down"></span><span class="icon mdi mdi-refresh-sync"></span><span class="icon mdi mdi-close"></span></div><span class="title">Revenue by Channel</span><span class="panel-subtitle">This is a pie chart created with Chart.js</span>
                </div>
                <div class="be-spinner">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 66 66" height="40px" width="40px">

                    <circle class="circle" r="25" cy="33" cx="33" stroke-linecap="round" stroke-width="4" fill="none"/>
                    </svg>
                </div>
                <div class="panel-body" id="custompanel">
                    <canvas id="pie-chart1" class="pie-chart1" height="180"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="Loader_Rev_Region be-loading be-loading-active">
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-divider">
                    <div class="tools"><span class="icon mdi mdi-chevron-down"></span><span class="icon mdi mdi-refresh-sync"></span><span class="icon mdi mdi-close"></span></div><span class="title">Revenue by region</span><span class="panel-subtitle">This is a pie chart created with Chart.js</span>
                </div>
                <div class="be-spinner">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 66 66" height="40px" width="40px">

                    <circle class="circle" r="25" cy="33" cx="33" stroke-linecap="round" stroke-width="4" fill="none"/>
                    </svg>
                </div>
                <div class="panel-body" id="custompanel2">
                    <canvas id="pie-chart2" class="pie-chart2"height="180"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
<!--*********************************3rd ROW GRAPH bar chart********************************************************************************************-->
<div class="row">
    <div class="col-sm-6">
        <div class="Loader_Cust_acq be-loading be-loading-active">
            <div class="widget widget-fullwidth">
                <div class="widget-head">
                    <div class="tools"><span class="icon mdi mdi-chevron-down"></span><span class="icon mdi mdi-refresh-sync"></span><span class="icon mdi mdi-close"></span></div><span class="title">Customer Acquired</span>
                </div>
                <div class="be-spinner">

                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 66 66" height="40px" width="40px">

                    <circle class="circle" r="25" cy="33" cx="33" stroke-linecap="round" stroke-width="4" fill="none"/>
                    </svg>
                </div>
                <div class="widget-chart-container">
                    <div class="counter">
                        <div class="value">80%</div>
                        <div class="desc">More Clients</div>
                    </div>
                    <div id="bar-chart1" class="bar-chart1"style="height: 440px;"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="Loader_Avg_order_val be-loading be-loading-active">
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-divider">
                    <div class="tools"><span class="icon mdi mdi-chevron-down"></span><span class="icon mdi mdi-refresh-sync"></span><span class="icon mdi mdi-close"></span></div>
                    <span class="title">Average Order Value</span><span class="panel-subtitle">By Region</span>
                </div>
                <div class="be-spinner">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 66 66" height="40px" width="40px">

                    <circle class="circle" r="25" cy="33" cx="33" stroke-linecap="round" stroke-width="4" fill="none"/>
                    </svg>
                </div>
                <div  class="panel-body" id="custombargraph2">
                    <canvas id="bar-chart2" class="bar-chart2"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>


<!--*********************************Carousels********************************************************************************************-->


<div class="row wizard-row panel newdashboard_container">
    <div class="col-md-12 fuelux">

        <div id="revenue" class="carouseller"> 
            <div class="widget-head panel-heading-divider">
                <div class="tools">
                    <div class="col-md-8">
                        <select class="form-control customer_validate countrySelection" id="revenueSelect" name="cu-Country" style="color:#989696;">
                            <option value="0">Please Select Value</option>
                            <?php
                            if (isset($countries) and ! empty($countries)) {
                                foreach ($countries as $key => $single) {
                                    ?>
                                    <option name="country" value="<?php echo $key; ?>" class=""><?php echo $single; ?></option>
                                    <?php
                                }
                            }
                            ?>

                        </select>
                    </div>
                    <div class="col-md-4">
                        <a href="/products" class="form-control btn btn-space btn-primary">View All</a>
                    </div>
                </div>
                <span class="title">Top Products</span>
                <span class="description">By Revenue</span>
            </div>
            <?php if (isset($products_by_revenue) and ! empty($products_by_revenue)) { ?>
                <a href="javascript:void(0)" class="carouseller__left">‹</a>
                <div class="carouseller__wrap"> 
                    <div class="carouseller__list revenueProducts"> 
                        <?php
                        foreach ($products_by_revenue as $key => $single) {
                            if (empty($single['product']))
                                continue;
                            ?>
                            <div class="car__2" data-value="<?php echo $single['product_price'] ?>">
                                <?php
                                if (isset($single['product']['productImages']) and ! empty($single['product']['productImages']) and isset($single['product']['productImages'][0]) and ! empty($single['product']['productImages'][0])) {
                                    $src = $single['product']['productImages'][0]['link'];
                                } else {
                                    $src = '/img/elliot-logo.svg';
                                }
                                ?>                                        <img src="<?php echo $src ?>" height="120" width="120">
                                <?php ?>
                                <br>
                                <h5><a href="/products/<?php echo $single['product']['id'] ?>"><?php echo @$single['product']['product_name'] ?></a></h5>

                                <h6><?php echo $user->currency . ' ' . @$single['product']['price'] ?></h6>

                            </div>

                            <?php
                        }
                        ?>




                    </div>
                </div>
                <a href="javascript:void(0)" class="carouseller__right">›</a>
<?php } else {
    ?> <h3 style="text-align: center;"> No data found</h3><?php }
?>
        </div>
    </div>
</div>

<div class="row wizard-row panel newdashboard_container">
    <div class="col-md-12 fuelux">

        <div id="volume" class="carouseller"> 
            <div class="widget-head panel-heading-divider">
                <div class="tools">
                    <div class="col-md-8">
                        <select class="form-control customer_validate countrySelection" id="volumeSelect" name="cu-Country" style="color:#989696;">
                            <option value="0">Please Select Value</option>
<?php
if (isset($countries) and ! empty($countries)) {
    foreach ($countries as $key => $single) {
        ?>
                                    <option name="country" value="<?php echo $key; ?>" class=""><?php echo $single; ?></option>
                                    <?php
                                }
                            }
                            ?>

                        </select>
                    </div>
                    <div class="col-md-4">
                        <a href="/products" class="form-control btn btn-space btn-primary">View All</a>
                    </div>
                </div>
                <span class="title">Top Products</span>
                <span class="description">By Volume</span>
            </div>
<?php if (isset($products_by_volume) and ! empty($products_by_volume)) { ?>
                <a href="javascript:void(0)" class="carouseller__left">‹</a> 
                <div class="carouseller__wrap"> 


                    <div class="carouseller__list volumeProducts"> 
    <?php
    foreach ($products_by_volume as $key => $single) {
        if (empty($single['product']))
            continue;
        ?>
                            <div class="car__2" data-value="<?php echo $single['product_quantity'] ?>">
                            <?php
                            if (isset($single['product']['productImages']) and ! empty($single['product']['productImages']) and isset($single['product']['productImages'][0]) and ! empty($single['product']['productImages'][0])) {
                                $src = $single['product']['productImages'][0]['link'];
                            } else {
                                $src = '/img/elliot-logo.svg';
                            }
                            ?>                                        <img src="<?php echo $src ?>" height="120" width="120">
                                <?php ?>
                                <br>
                                <h5><a href="/products/<?php echo $single['product']['id'] ?>"><?php echo @$single['product']['product_name'] ?></a></h5>

                                <h6><?php echo $user->currency . ' ' . @$single['product']['price'] ?></h6>

                            </div>

        <?php
    }
    ?>




                    </div>
                </div>
                <a href="javascript:void(0)" class="carouseller__right">›</a>
<?php } else {
    ?> <h3 style="text-align: center;"> No data found</h3><?php }
?>
        </div>
    </div>
</div>

<div id="mod-danger" tabindex="-1" role="dialog" class="modal fade in product_ajax_request_error" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close lazada_error_modal_close"></span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
                    <h3 id=''>Error</h3>
                    <p id="">No data Found for the selected Region.</p>
                    <div class="xs-mt-50">
                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default lazada_error_modal_close">Close</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('#revenue').carouseller();
        $('#volume').carouseller();
        $('.countrySelection').on('change', function () {
            $this = $(this);
            country_code = $this.val();
            id = $this.attr('id');
            country_code_volume = '';
            country_code_revenue = '';
            if (id == 'volumeSelect') {
                country_code_volume = country_code;
                divClass = 'volumeProducts';

            }
            if (id == 'revenueSelect') {
                country_code_revenue = country_code;
                divClass = 'revenueProducts';

            }
            $.ajax({
                type: 'Post',
//                dataType: 'json',
                data: {country_code_revenue: country_code_revenue, country_code_volume: country_code_volume},
                url: '/lazada/get-filtered-products',
                success: function (data) {
                    if (data) {
                        $('.' + divClass).html(data);
                    } else {
                        $('#' + id).prop('selectedIndex', 0);
                        $('.product_ajax_request_error').modal('show');
                    }

                }
            });
        });
    });
</script>
