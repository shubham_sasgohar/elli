<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */
use backend\models\User;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;

$this->title = $name;
?>
<!-- <div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>
        The above error occurred while the Web server was processing your request.
    </p>
    <p>
        Please contact us if you think this is a server error. Thank you.
    </p>

</div> -->
<div class="be-error be-error-404">

        <div class="main-content container-fluid">
          <div class="error-container">
            <div class="error-number">404</div>
            <div class="error-description">The page you are looking for might have been removed.</div>
            <div class="error-goback-text">Would you like to go home?</div>
            <div class="error-goback-button"><a href="/" class="btn btn-xl btn-primary">Let's go home</a></div>
            <div class="footer">&copy; 2016 Your Company</div>
          </div>
        </div>
    
      </div>
