<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$basepath = Yii::getAlias('@baseurl');

$baseurl = Yii::$app->getUrlManager()->getBaseUrl();

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;

$actual_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$match_link=Yii::$app->params['BASE_URL'].'login';
?>
<?php
if (Yii::$app->session->hasFlash('success')):
    $header_cls = 'panel panel-full-success';
elseif (Yii::$app->session->hasFlash('warning')):
    $header_cls = 'panel panel-full-warning';
elseif (Yii::$app->session->hasFlash('danger')):
    $header_cls = 'panel panel-full-danger';
elseif (Yii::$app->session->hasFlash('info')):
    $header_cls = 'panel panel-full-info';
else:
    $header_cls = '';
endif;

//    $header_cls = 'be-wrapper be-fixed-sidebar';
?>
<div class="be_loader_modal_text">We are updating and loading your data.</div>
<div class="be-spinner" style="display: none; transform: translateY(-50%);">
    <svg width="40px" height="40px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
    <circle fill="none" stroke-width="4" stroke-linecap="round" cx="33" cy="33" r="30" class="circle"></circle>
    </svg>
</div>
<div class="be-wrapper be-login">
    <div class="be-content be-loading">
        <div class="main-content container-fluid">
            <div class="splash-container">
                <div class="panel panel-default panel-border-color panel-border-color-primary">
                    <div class="<?php echo $header_cls ?>">
                        <span class="panel-heading" ><?= Yii::$app->session->getFlash('success'); ?></span>
                    </div>
                    <div class="panel-heading"><img src="<?php echo $basepath ?>/img/elliot-logo-thumbnail.svg" alt="logo" width="150" height="160" class="logo-img">
			    <!--<span class="splash-description">Global Commerce Unified</span>-->
		    </div>
                    <div class="panel-body">

                        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                        <div class="form-group">
                            <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label('Email') ?>
                        </div>
                        <div class="form-group">
                            <?= $form->field($model, 'password')->passwordInput() ?>
                        </div>
                        <div class="form-group row login-tools">
                            <div class="col-xs-6 login-remember">
                                <div class="be-checkbox">
                                    <input type="checkbox" id="remember">
                                    <label for="remember">Remember Me</label>
                                </div>
                            </div>
                            <div class="col-xs-6 login-forgot-password"><a href="<?php echo $baseurl; ?>/request-password-reset">Forgot Password?</a></div>
                        </div>
                        <div class="form-group login-submit">
                            <?= Html::submitButton('Log in', ['class' => 'btn btn-primary btn-xl LoginCustomClass', 'name' => 'login-button']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
                <?php if($actual_link==$match_link and strstr($match_link,'elliot.store')) {?>
                <div class="splash-footer"><span>Don't have an account? <a href="<?php //echo $baseurl; ?>/signup">Sign Up</a></span></div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript">
    jQuery(document).ready(function(){
      jQuery(".LoginCustomClass").click(function(event){
        //event.preventDefault();
        jQuery('body').addClass('be-loading-active');
        jQuery('body').addClass('be-loading');
        jQuery('.be_loader_modal_text').css('display', 'block');
        jQuery('.be-spinner').css('display', 'block');
        // setTimeout(function(){
        //   jQuery('.be-loading').first().removeClass('be-loading-active');
        //   jQuery('.SignUpCustomClass').unbind('click').click();

        // }, );
      });
    });
    </script>
