<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\User;
use yii\widgets\Breadcrumbs;

$this->title = ' Translations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-head">
    <h2 class="page-head-title"><?= Html::encode($this->title) ?></h2>
    <ol class="breadcrumb page-head-nav">
        <?php
        echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
        ?>
    </ol>
</div>

<!--Swtich Component-->
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default panel-border-color panel-border-color-primary">
            <div class="panel-heading panel-heading-divider">Integrations Translations</div>
            <div class="panel-body">
                <form action="#" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
                    <div class="form-group">
                        <label class="col-sm-3 control-label panel-heading">Integrate translation</label>
                        <div class="col-sm-6 xs-pt-5">
                            <div class="switch-button switch-button-success">
                                <input type="checkbox" checked="" name="swt5" id="swt5"><span>
                                    <label for="swt5"></label></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>