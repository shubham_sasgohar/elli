<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$basepath=Yii::getAlias('@baseurl');
$baseurl=Yii::$app->getUrlManager()->getBaseUrl().'/index.php';

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-reset-password">
<div class="be-wrapper be-login">
      <div class="be-content">
        <div class="main-content container-fluid">
          <div class="splash-container forgot-password">
                  <h1><?= Html::encode($this->title) ?></h1>
            <div class="panel panel-default panel-border-color panel-border-color-primary">
              <div class="panel-heading"><img src="<?php echo $basepath?>/img/elliot-logo.svg" alt="logo" width="102" height="27" class="logo-img"><span class="splash-description">Omnichannel Syndication for Global Merchants</span></div>
              <div class="panel-body">
                  <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
                  
                  <p>Please choose your new password:</p>
                  <div class="form-group xs-pt-20">
                    <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>

                  </div>
                  <div class="form-group xs-pt-5">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-block btn-primary btn-xl']) ?>
                  </div>
                <?php ActiveForm::end(); ?>
              </div>
            </div>
            <div class="splash-footer">&copy; 2017 Elliot</div>
          </div>
        </div>
      </div>
    </div>
</div>
