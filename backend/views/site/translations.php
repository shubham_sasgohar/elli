<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\User;
use yii\widgets\Breadcrumbs;

$this->title = 'Translations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-head">
    <h2 class="page-head-title">Add Smartling API</h2>
    <ol class="breadcrumb page-head-nav">
        <?php
        echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
        ?>
    </ol>
</div>

<!--Swtich Component-->
<div class="row wizard-row">
    <div class="col-md-12 fuelux">
        <div class="block-wizard panel panel-default">
            <div id="wizard-store-connection" class="wizard wizard-ux">
                <ul class="steps">
                    <li data-step="1" class="active">Authorize<span class="chevron"></span></li>
                    <!--<li data-step="3">Finish<span class="chevron"></span></li>-->
                </ul>
                <div class="step-content">
                    <div data-step="1" class="step-pane active">
                        <form id="smartling-authorize-form" action="#" data-parsley-namespace="data-parsley-" data-parsley-validate="" novalidate="" class="form-horizontal group-border-dashed">
                            <div class="form-group no-padding main-title">
                                <div class="col-sm-12">
                                    <label class="control-label">Please provide your Smartling API Account details for Authorizing the Integration :</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Smartling Project Id</label>
                                <div id="smrt_project_id" class="col-sm-6">
                                    <input type="text" placeholder="Please Enter value" class="form-control">

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Smartling User ID</label>
                                <div id="smrt_user_id" class="col-sm-6">
                                    <input type="text" placeholder="Please Enter value" class="form-control">

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Smartling Secret Key</label>
                                <div id="smrt_secret_key" class="col-sm-6">
                                    <input type="text" placeholder="Please Enter value" class="form-control">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button data-wizard="#smartling-button" class="btn btn-primary btn-space smartling-button">Connect</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
