<?php
use backend\models\OrderChannel;
use backend\models\OrdersProducts;  
use backend\models\ProductChannel;
use backend\models\Stores;
use backend\models\CustomerAbbrivation;
use backend\controllers\ChannelsController;
use backend\models\StoreDetails;


if (empty($lates_engagements)) : ?>
<tr class="odd"><td valign="top" colspan="7" class="dataTables_empty">No data available in table.</td> </tr>
<?php else : ?> 
    
    <?php foreach ($lates_engagements as $latest) :
        ?>
        <!-- For Orders!--->
        <?php if (array_key_exists("order_ID", $latest)): ?>
            <?php
           // $order_channel_store = OrderChannel::find()->where(['order_id' => $latest['order_ID']])->with('stores')->one();
            $order_channel_store = OrderChannel::find()->where(['order_id' => $latest['order_ID']])->with('order')->one();
            $channel_name=$order_channel_store->order->channel_accquired;
            $store_img = ChannelsController::getStoreChannelImageByName($channel_name);
            ?>
            <tr>
                <td style="width:37%;"><a href="orders/view/<?php echo $latest['order_ID']; ?>"><?= $latest['channel_abb_id']; ?></a></td>
                <td style="width:36%;"><?= "Order"; ?></td>
                <td><?php echo $store_img; ?></td>

            </tr>
        <?php endif; ?>
        <!-- For Products!--->
        <?php if (array_key_exists("id", $latest)): ?>
            <?php
            $product_channel_store = ProductChannel::find()->where(['product_id' => $latest['id']])->one();
            
            if(empty($product_channel_store)){
                continue;
            }
            if ($product_channel_store['store_id'] == NULL):
             
                $product_channel = ProductChannel::find()->where(['product_id' => $latest['id']])->with('channel')->one();
                $channel_name=$product_channel->channel['channel_name'];
                if($channel_name=='Service Account' || $channel_name=='Subscription Account'):
                    $channel_name = "WeChat";
               endif;
            else:
               
               $store_connection_id=$product_channel_store->store_id;
               $store_connection_data=StoreDetails::find()->Where(['store_connection_id'=>$store_connection_id])->one();
               $channel_name=$store_connection_data->channel_accquired;
                 //   $store_img = $product_channel_store->stores['store_image'];
            endif;
            $store_img = ChannelsController::getStoreChannelImageByName($channel_name);
            ?>
            <tr>
                <td style="width:37%;" class="captialize"><a href="/products/<?php echo $latest['id']; ?>"><?= $latest['product_name']; ?></td>
                <td style="width:36%;"><?= "Product"; ?></td>
                <td><?php echo $store_img; ?></td>

            </tr>
        <?php endif; ?>
        <!-- For Customers!--->
        <?php if (array_key_exists("customer_ID", $latest)): ?>
            <?php
            $ch_acc = $latest['channel_acquired'];
            $store_img = ChannelsController::getStoreChannelImageByName($ch_acc);
            ?>
            <tr>
                <td style="width:37%;" class="captialize"><a href="/people/view?id=<?php echo $latest['customer_ID']; ?>"><?= $latest['first_name'] . ' ' . $latest['last_name']; ?></a></td>
                <td style="width:36%;"><?= "Customer"; ?></td>
                <td><?php echo $store_img; ?></td>
            </tr>
        <?php endif; ?>

    <?php endforeach; ?>
<?php endif; ?>