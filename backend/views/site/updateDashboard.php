<?php
/* @var $this yii\web\View */

use backend\models\Channels;
use yii\db\Query;
use backend\models\Orders;
use backend\models\OrderChannel;
use backend\models\OrdersProducts;
use backend\models\StoresConnection;
use backend\models\Stores;
use backend\models\ChannelConnection;
use backend\models\CronTasks;
use backend\models\CustomerUser;
use backend\models\Products;
use backend\models\ProductChannel;
use yii\web\Session;

$this->title = 'Dashboard | Elliot';
$basepath = Yii::getAlias('@baseurl');
$user_id = Yii::$app->user->identity->id;

$store_img = '';

/* GET ALL PRODUCTS COUNT FOR SALES */
$products_count = count(Products::find()->all());
/* END ALL PRODUCTS COUNT FOR SALES */
// For recent orders 
$orders_data_REcent = Orders::find()->Where(['elliot_user_id' => Yii::$app->user->identity->id])->with('customer')->orderBy(['order_ID' => SORT_DESC,])->limit(5)->all();


$StoresConnection = StoresConnection::find()->all();
$ChannelConnection = ChannelConnection::find()->all();

if (Yii::$app->user->identity->role == 'superadmin') {
    echo "<p>Admin Dashboards</p>";
    //use for base directory//
    $basedir = Yii::getAlias('@basedir');
    //include stripe init file//
    require $basedir . '/stripe/init.php';
    \Stripe\Stripe::setApiKey("sk_test_2GPaBt5mT2k0yyTA9ho3CPIQ");
    //get subscription plan through stripe//
    $collection_plans = \Stripe\Plan::all(array("limit" => 100));
    $stripe_plans = $collection_plans->__toArray(true);
    $stripe_plan_data = $stripe_plans['data'];
    foreach ($stripe_plan_data as $data) {
        $db_stripe_channel_data = Channels::find()->Where(['channel_name' => $data['name']])->one();
        if (empty($db_stripe_channel_data)) {
            $meta = @$data['metadata']['image_url'];
            $id = $data['id'];
            $prefix_channel = substr($id, 0, 7);
            if ($prefix_channel == 'channel') {
                $stripe_Channel = new Channels();
                $stripe_Channel->stripe_Channel_id = stripslashes($data['id']);
                $stripe_Channel->amount = stripslashes($data['amount'] / 100);
                $stripe_Channel->currency = stripslashes($data['currency']);
                $stripe_Channel->channel_image = stripslashes(@$data['metadata']['image_url']);
                $stripe_Channel->channel_name = stripslashes($data['name']);
                $stripe_Channel->parent_name = stripslashes(@$data['metadata']['parent_name']);
                $stripe_Channel->trial_period_days = stripslashes($data['trial_period_days']);
                $stripe_Channel->save(false);
            }
        } else {
            $meta = @$data['metadata']['image_url'];
            $id = $data['id'];
            $prefix_channel = substr($id, 0, 7);
            if ($prefix_channel == 'channel') {
                $stripe_Channel = Channels::find()->Where(['channel_name' => $data['name']])->one();
                $stripe_Channel->stripe_Channel_id = stripslashes($data['id']);
                $stripe_Channel->amount = stripslashes($data['amount'] / 100);
                $stripe_Channel->currency = stripslashes($data['currency']);
                $stripe_Channel->channel_image = stripslashes(@$data['metadata']['image_url']);
                $stripe_Channel->channel_name = stripslashes($data['name']);
                $stripe_Channel->parent_name = stripslashes(@$data['metadata']['parent_name']);
                $stripe_Channel->trial_period_days = stripslashes($data['trial_period_days']);
                $stripe_Channel->save(false);
            }
        }
    }
    ?>
    <div class="main-content container-fluid">

        <div class="row marketplace-listing">
            <?php
            $connection = \Yii::$app->db;
            //$model1 = $connection->createCommand('SELECT DISTINCT parent_name stripe_Channel_id,channel_image,parent_name FROM channels');
            $model1 = $connection->createCommand('SELECT COUNT(parent_name), parent_name FROM channels GROUP BY parent_name');
            $channels = $model1->queryAll();
            foreach ($channels as $channels_data) {
                $count_parent_name = $channels_data['COUNT(parent_name)'];
                $parent_name = $channels_data['parent_name'];
                ?>
                <?php
                if ($count_parent_name == 1) {
                    $marketplace = Channels::find()->Where(['parent_name' => $parent_name])->one();
                    if ($marketplace->parent_name == 'channel_Square') {
                        continue;
                    }
                    ?>
                    <?php if ($marketplace->stripe_Channel_id == 'channel_Instagram') { ?>
                        <div class="col-sm-3">
                            <div class="bs-grid-block comingsoon" data-toggle="modal"  data-target="#<?php echo $marketplace->stripe_Channel_id; ?> ">
                                <div class="content">
                                    <span class="label label-success custom-span-channels-label span-top-22" style=" position: absolute; right: 30px; ">Coming Soon</span>
                                    <img src="<?php echo $marketplace->channel_image; ?>" style="width: 100%" alt="<?php echo $marketplace->stripe_Channel_id; ?>">
                                </div>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="col-sm-3">
                            <div class="bs-grid-block">
                                <div class="content">
                                    <img src="<?php echo $marketplace->channel_image; ?>" style="width: 100%;" alt="" class="<?php echo $marketplace->stripe_Channel_id == "channel_Xiao_Hong_Shu" ? 'resize_Xiao' : ''; ?>">
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <?php
                } else if ($count_parent_name != 1) {
                    $marketplace = Channels::find()->Where(['parent_name' => $parent_name])->one();
                    if ($marketplace->parent_name == 'channel_WeChat') {
                        ?>
                        <div class="col-sm-3">
                            <div class="bs-grid-block" data-toggle="modal"  data-target="#<?php echo $marketplace->stripe_Channel_id; ?> ">
                                <div class="content">
                                    <!--span  class="label label-success span-top-22" style=" position: absolute; right: 30px; ">Connected</span-->
                                    <img src="<?php echo $marketplace->channel_image; ?>" style="width: 100%" alt="<?php echo $marketplace->stripe_Channel_id; ?>">
                                </div>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="col-sm-3">
                            <div class="bs-grid-block" data-toggle="modal"  data-target="#<?php echo $marketplace->stripe_Channel_id; ?> ">
                                <div class="content">

                                    <img src="<?php echo $marketplace->channel_image; ?>" style="width: 100%;" alt="<?php echo $marketplace->stripe_Channel_id; ?>">
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                }
            }
            ?>
        </div>
    </div>

    <?php
    $connection1 = \Yii::$app->db;

    $model2 = $connection->createCommand('SELECT COUNT(parent_name), parent_name FROM channels GROUP BY parent_name');

    $channels1 = $model2->queryAll();

    foreach ($channels1 as $channels_data1) {
        $count_parent_name1 = $channels_data1['COUNT(parent_name)'];

        $parent_name1 = $channels_data1['parent_name'];

        $marketplace1 = Channels::find()->Where(['parent_name' => $parent_name1])->one();

        if (empty($marketplace1)):
            $name = "test";
        else:
            $name = $marketplace1->channel_name;
        endif;
        ?>
        <?php if ($count_parent_name1 == 1) { ?>
            <div id="<?php echo $marketplace1->stripe_Channel_id; ?>" tabindex="-1" role="dialog" class="modal fade admin_pg">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                        </div>
                        <div class="modal-body">
                            <div class="text-center">
                                <div class="text-primary"><img src="<?php echo $marketplace1->channel_image; ?>" style="width: 50%;" alt=""></span></div>
                                <h3>Coming Soon</h3>
                                <p>Subscribe to get updated when <?= $name ?> becomes available in Elliot.</p>
                                <div class="xs-mt-50"> 
                                    <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Close</button>
                                    <button type="button" data-dismiss="modal" class="btn btn-space btn-primary">Subscribe</button>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer"></div>
                    </div>
                </div>
            </div>
        <?php } else if ($count_parent_name1 != 1) { ?>  
            <div id="<?php echo $marketplace1->stripe_Channel_id; ?>" tabindex="-1" role="dialog" class="modal fade admin_pg">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                        </div>
                        <div class="modal-body">
                            <div class="text-center">
                                <div class="text-primary"><img src="<?php echo $marketplace1->channel_image; ?>" style="width: 50%;" alt=""></span></div>
                                <div class="row">
                                    <div class="form-group">
                                        <?php
                                        $channels_group_data = Channels::find()->Where(['parent_name' => $parent_name1])->all();
                                        foreach ($channels_group_data as $channel_group) {
                                            ?>
                                            <div class="col-sm-3">
                                                <div class="be-radio inline">
                                                    <input type="radio" checked="" name="rad3" id="<?php echo $channel_group->channel_name; ?>" value="<?php echo $channel_group->channel_name; ?>">
                                                    <label class="channel-modal-span-group" for="<?php echo $channel_group->channel_name; ?>"><b><?php echo $channel_group->channel_name; ?></b></label>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="xs-mt-50"> 
                                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Close</button>
                                        <button type="button" class="btn btn-space btn-primary">Subscribe</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer"></div>
                    </div>
                </div>
            </div>

            <?php
        }
    }
} else {
    ?>
    <!--if channel and store is not connected Only show Stores In Index Page!-->
    <?php
    $store_connection_data = StoresConnection::find()->Where(['user_id' => $user_id])->with('stores')->asArray()->all();
    $channels_connection_data = ChannelConnection::find()->Where(['user_id' => $user_id])->with('channels')->asArray()->all();
    $connected_data = array_merge($store_connection_data, $channels_connection_data);
    ?>
    <?php if (empty($connected_data)): ?>
        <div class="row be-connections store_logo100px">
            <div class="col-md-12">
                <div class="widget widget-fullwidth be-loading">
                    <div class="widget-head">

                        <span class="title">Connect Your Store</span>
                        <div class="list">
                            <p class="connectstore_line">Choose from the eCommerce platforms below to easily integrate your existing catalog and storefront data.</p>
                            <div class="content store_listing_container">
                                <div class="row">
                                    <div class="col-sm-3 text-center store_listing_container_sub"><a href="/stores/bigcommerce" class="connection-item"><img src="/img/bigcommerce.png" alt="Github">
                                            <p>BigCommerce</p></a>
                                    </div>
                                    <div class="col-sm-3 text-center store_listing_container_sub"><a href="/stores/magento" class="connection-item"><img src="/img/magento.png" alt="Bitbucket">
                                            <p>Magento</p></a>.
                                    </div>

                                    <div class="col-sm-3 text-center store_listing_container_sub"><a href="/magento2" class="connection-item"><img src="/img/magento2.png" alt="Magento2">
                                            <p>Magento2</p></a>
                                    </div>
<!--                                    <div class="col-sm-3 text-center store_listing_container_sub"><a href="javascript:void(0)" class="connection-item"><img src="/img/netsuite.png" alt="Oracle Commerce Cloud">
                                            <p>NetSuite SuiteCommerce</p></a>
                                    </div>
                                    <div class="col-sm-3 text-center store_listing_container_sub"><a href="javascript:void(0)" class="connection-item"><img src="/img/oraclecommercecloud.png" alt="Oracle Commerce Cloud">
                                            <p>Oracle Commerce Cloud</p></a>
                                    </div>-->

                                    <div class="col-sm-3 text-center store_listing_container_sub"><a href="/stores/shopify" class="connection-item"><img src="/img/shopify.png" alt="Github">
                                            <p>Shopify</p></a>
                                    </div>
                                    <div class="col-sm-3 text-center store_listing_container_sub"><a href="/stores/shopify" class="connection-item"><img src="/img/shopifyplus.png" alt="ShopifyPlus">
                                            <p>ShopifyPlus</p></a>
                                    </div>
<!--                                    <div class="col-sm-3 text-center store_listing_container_sub"><a href="javascript:void(0)" class="connection-item"><img src="/img/salesforcecommercecloud.png" alt="Salesforce Commerce Cloud">
                                            <p>Salesforce Commerce Cloud</p></a>
                                    </div>-->
                                    <div class="col-sm-3 text-center store_listing_container_sub"><a href="/vtex" class="connection-item"><img src="/img/vtex.png" alt="VTEX">
                                            <p>VTEX</p></a>.
                                    </div>
                                    <div class="col-sm-3 text-center store_listing_container_sub"><a href="stores/woocommerce" class="connection-item"><img src="/img/woocommerce.png" alt="Bitbucket">
                                            <p>WooCommerce</p></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    <?php else: ?>
        <!--END if channel and store is not connected Only show Stores In Index Page!-->
        <?php
        /*         * for showing loader for first time when the data is importing* */
        $curr_userid = Yii::$app->user->identity->id;
        $connection = \Yii::$app->db;
        $check_store = $connection->createCommand("SELECT COUNT(stores_connection_id) AS stores FROM stores_connection WHERE user_id='" . $curr_userid . "'");
        $check_store_query = $check_store->queryAll();
        $store_count = @$check_store_query[0]['stores'];
        $check_channel = $connection->createCommand("SELECT COUNT(channel_connection_id) AS channels FROM channel_connection WHERE user_id='" . $curr_userid . "'");
        $check_channel_query = $check_channel->queryAll();
        $channel_count = @$check_channel_query[0]['channels'];


        $store_import_status = $connection->createCommand("SELECT import_status FROM stores_connection WHERE user_id='" . $curr_userid . "'");
        $store_import = $store_import_status->queryAll();
        $store_status = @$store_import[0]['import_status'];

        $channel_import_status = $connection->createCommand("SELECT import_status, channel_id FROM channel_connection WHERE user_id='" . $curr_userid . "'");
        $channel_import = $channel_import_status->queryAll();
        $channel_status = @$channel_import[0]['import_status'];
        ?>
        <!--END if channel and store is connected show Index Page!-->
        <?php $loader_active = ''; ?>
        <?php
        if ($store_count == 1 && $channel_count == 0) {
            if ($store_status == '') {
                $loader_active = 'be-loading-active';
            }
        } elseif ($store_count == 0 && $channel_count == 1) {

            $get_connected_channel = ChannelConnection::find()->where(['user_id' => $curr_userid])->one();
            $channelID = $get_connected_channel->channel_id;
            $channel_quer = Channels::find()->where(['channel_ID' => $channelID])->one();


            if ($channel_status == '') {

                if ($channel_quer->parent_name == 'channel_Facebook' || $channel_quer->parent_name == 'channel_google-shopping') {

                    $loader_active = '';
                    // $chart_load_class = '';
                } else {
                    $loader_active = 'be-loading-active';
                }
            }
        }
        ?>
        <div class="be-loading <?php echo $loader_active; ?>" style="z-index:999;">

            <!--For Date range hidden input!-->
            <div class="row">
                <div class="col-md-8"></div>
                <div class="col-md-4 dashboard_daterange">
                    <div class="form-group">
                        <div class="col-md-12">
                           <!-- <input style="opacity:0;"type="text" name="daterange" value="01/01/2017 - 01/31/2017" class="form-control daterange">-->
                            <input id="date_range_graph" style="opacity:0;"type="text" name="daterange" value="<?php echo date('m/d/Y') . '-' . date('m/d/Y') ?>"  class="form-control daterange">
                        </div>
                    </div>
                </div>

            </div>
            <!--*********************************DASHBOARD MAIN CHART START HERE********************************************************************************-->

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default OrderChaRT <?php
                    if (empty($loader_active)) {
                        echo 'be-loading';
                    }
                    ?>" style="z-index:999;">
                        <div class="panel-heading panel-heading-divider">
                            <div class="widget-head">
                                <div class="tools">
                                    <div class="dropdown responsive_dropdown"><span data-toggle="dropdown" class="icon mdi mdi-more-vert visible-xs-inline-block dropdown-toggle" aria-expanded="false"></span>
                                        <ul role="menu" class="dropdown-menu">
                                            <li><a  id="dashboardchartweekmob" class="btn btn-default customPeople  "  style="cursor:pointer">Week</a></li>
                                            <li><a  id="dashboardchartmonthmob" class="btn btn-default customPeople " style="cursor:pointer">Month</a></li>
                                            <li><a  id="dashboardchartquartermob" class="btn btn-default customPeople " style="cursor:pointer">Quarter</a></li>
                                            <li><a  id="dashboardchartyearmob" class="btn btn-default customPeople " style="cursor:pointer">Year</a></li>
                                            <li class="divider"></li>
                                            <li><a  id="dashboardcharttodaymob" class="btn btn-default customPeople  "  style="cursor:pointer">Today</a></li>
                                            <li><a   id="dateRange"  class="btn btn-default daterangeBoth customPeople dateRange" style="cursor:pointer">Date Range</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="btn-group btn-space pull-right">
                                    <button class="btn btn-default show-selected-channel-dashboard" type="button">Filter by Channel</button>
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="true"><span class="mdi mdi-chevron-down"></span><span class="sr-only">Toggle Dropdown</span></button>
                                    <ul class="dropdown-menu  SortChannelList" role="menu">
                                        <li  attr-value=""><a>View All</a></li>
                                        <?php
                                        if (isset($store_details_data) and ! empty($store_details_data)) {
                                            foreach ($store_details_data as $key => $single) {
                                                $channel_accquired = $single['channel_accquired'];
                                                $country = $single['country'];
                                                $type = 'store';

                                                if (empty($single['store_connection_id'])) {
                                                    $type = 'channel';
                                                    $fin_channel = trim($channel_accquired);
                                                } else {
                                                    $fin_channel = trim($channel_accquired . ' ' . $country);
                                                }

                                                $StoresConnection_id = ($single['store_connection_id']) ? $single['store_connection_id'] : $single['channel_connection_id'];
                                                ?>
                                                <li attr-value="<?php echo $StoresConnection_id; ?>"  attr-type="<?php echo $type ?>" class=""><a><?php echo $fin_channel; ?></a></li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                                <div class="button-toolbar hidden-xs">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default customPeople " id="dashboardchartweek">Week</button>
                                        <button type="button" class="btn btn-default customPeople " id="dashboardchartmonth">Month</button>
                                        <button type="button" class="btn btn-default customPeople " id="dashboardchartquarter">Quarter</button>
                                        <button type="button" class="btn btn-default customPeople " id="dashboardchartyear">Year</button>
                                        <input name="" value="Week" id="hidden_graph" type="hidden">
                                    </div>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default customPeople " id="dashboardcharttoday">Today</button>
                                        <button id="dateRange" type="button" class="btn btn-default dateRange customPeople daterangeBoth ">Date Range</button>
                                    </div>
                                </div>
                                <span class="title">Global Performance - Orders</span> 
                            </div>
                            <div class="col-sm-12 custom_class showdaterange_elliot hide">

                            </div>
                            <div class="be-spinner" style="width:100%; text-align: center; right:auto">
                                <svg width="40px" height="40px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">

                                <circle fill="none" stroke-width="4" stroke-linecap="round" cx="33" cy="33" r="30" class="circle"></circle>
                                </svg>
                                <?php if (isset($loader_active) and ! empty($loader_active) and $loader_active == 'be-loading-active') {
                                    ?><span style="display:block; padding-top:30px;">Your data is importing, you will be emailed when it is complete.</span><?php } else {
                                    ?>
                                    <span style="display:block; padding-top:30px;">Your data is loading.</span>	
                                <?php }
                                ?>
                            </div>
                            <span class="panel-subtitle space">
                                <ul id="chartInfoPeople" class="chart-legend-horizontal"></ul>
                            </span>
                        </div>
                        <div id="padding-panel-body" class="panel-body"> 
                            <div class="dashboard-chart" id="dashboard-chart" style="height: 250px;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--NEW DASHBOARD FOUR GRAPHS START HERE-->
            <div class="row"  style="display:block;">
                <div class="col-xs-12 col-md-3">
                    <div class="widget widget-tile be-loading fourgraph1">
                        <div id="newsparknm1" class="chart sparkline newsparknm1"></div>
                        <?php if (empty($loader_active)) { ?>
                            <div class="be-spinner">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 66 66" height="40px" width="40px">

                                <circle class="circle" r="25" cy="33" cx="33" stroke-linecap="round" stroke-width="4" fill="none"/>
                                </svg>
                            </div>
                        <?php } ?>
                        <div class="data-info">
                            <div class="desc">Revenue Earned</div>
                            <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span>                              
                                <span class="numberneworders"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3">
                    <div class="widget widget-tile be-loading fourgraph2">
                        <div id="newsparknm2" class="chart sparkline newsparknm2"></div>
                        <?php if (empty($loader_active)) { ?>
                            <div class="be-spinner">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 66 66" height="40px" width="40px">

                                <circle class="circle" r="25" cy="33" cx="33" stroke-linecap="round" stroke-width="4" fill="none"/>
                                </svg>
                            </div>
                        <?php } ?>
                        <div class="data-info">
                            <div class="desc sales">Monthly Sales</div>
                            <div class="value"><span class="indicator indicator-positive mdi mdi-chevron-up"></span>                             
                                <span data-toggle="counter" data-end="" data-suffix="%" class="numbersales"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3">
                    <div class="widget widget-tile be-loading fourgraph4">                       
                        <div id="newsparknm3" class="chart sparkline newsparknm3"></div>
                        <?php if (empty($loader_active)) { ?>
                            <div class="be-spinner">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 66 66" height="40px" width="40px">

                                <circle class="circle" r="25" cy="33" cx="33" stroke-linecap="round" stroke-width="4" fill="none"/>
                                </svg>
                            </div>
                        <?php } ?>
                        <div class="data-info">
                            <div class="desc">Products Sold</div>
                            <div class="value"><span class="indicator indicator-positive mdi mdi-chevron-up"></span><span class="numberproductsold"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-3">
                    <div class="widget widget-tile be-loading fourgraph4">                      
                        <div id="newsparknm4" class="chart sparkline newsparknm4"></div>
                        <?php if (empty($loader_active)) { ?>
                            <div class="be-spinner">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 66 66" height="40px" width="40px">

                                <circle class="circle" r="25" cy="33" cx="33" stroke-linecap="round" stroke-width="4" fill="none"/>
                                </svg>
                            </div>
                        <?php } ?>
                        <div class="data-info">
                            <div class="desc">Avg. Order Value</div>
                            <div class="value"><span class="indicator indicator-negative mdi mdi-chevron-down"></span>
                                <span data-toggle="counter" data-end="" class="numberavgorder"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   

            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default panel-table be-loading recentorders">
                        <div class="panel-heading"> 
                            <a href="/orders"><button type="button" class="btn btn-default btn-primary pull-right">View All</button></a>
                            <div class="title">Recent Orders</div>

                        </div>
                        <?php if (empty($loader_active)) { ?>
                            <div class="be-spinner">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 66 66" height="40px" width="40px">

                                <circle class="circle" r="25" cy="33" cx="33" stroke-linecap="round" stroke-width="4" fill="none"/>
                                </svg>
                            </div>
                        <?php } ?>
                        <?php if (empty($StoresConnection) && empty($ChannelConnection)) : ?>
                            <center><button class="btn btn-space btn-primary" id="see">Connect Your Store to see Data</button></center>
                        <?php else : ?>
                            <div class="panel-body table-responsive ">
                                <table id="recent_orders_dashboard_ajax" class="table-borderless table table-striped table-hover table-fw-widget dataTable">
                                    <thead>
                                        <tr>

                                            <th>Customer Name</th>
                                            <th class="number12" style="text-align:left;">Amount</th>
                                           <!--  <th style="width:20%;">Date</th> -->
                                            <th >Status</th>
                                            <th >View Order</th>
                                        </tr>
                                    </thead>
                                    <tbody class="no-border-x recentOrderData">
                                    </tbody>
                                </table>
                            </div>
                        <?php
                        endif;
                        ?> 

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default panel-table be-loading latestengagements">
                        <div class="panel-heading">
                            <div class="title">Latest Engagements</div>
                        </div>
                        <?php if (empty($loader_active)) { ?>
                            <div class="be-spinner">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 66 66" height="40px" width="40px">

                                <circle class="circle" r="25" cy="33" cx="33" stroke-linecap="round" stroke-width="4" fill="none"/>
                                </svg>
                            </div>
                        <?php } ?>
                        <div class="panel-body table-responsive">
                            <table id="latest_engagement_dashboard" class="table-borderless table table-striped table-hover table-fw-widget dataTable latest-engagements">
                                <thead>
                                    <tr>
                                        <th style="width:37%;">Name</th>
                                        <th style="width:36%;">Engagement Type</th>
                                        <th>Channel</th>
                                    </tr>
                                </thead>
                                <tbody class="renderlatestengagement">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <!--*********************************Carousels********************************************************************************************-->

            <div class="row wizard-row panel newdashboard_container">
                <div class="col-md-12 fuelux be-loading topProductsByRevenue">
                    <?php if (empty($loader_active)) { ?>
                        <div class="be-spinner">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 66 66" height="40px" width="40px">

                            <circle class="circle" r="25" cy="33" cx="33" stroke-linecap="round" stroke-width="4" fill="none"/>
                            </svg>
                        </div>
                    <?php } ?>
                    <div id="revenue" class="carouseller"> 
                        <div class="widget-head panel-heading-divider ">
                            <div class="tools">
<!--                                <div class="col-md-8">

                                    <div class="btn-group btn-space pull-right">
                                        <button class="btn btn-default show-selected-country-revenue" type="button">Sort</button>
                                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="true"><span class="mdi mdi-chevron-down"></span><span class="sr-only">Toggle Dropdown</span></button>
                                        <ul class="dropdown-menu CountriesLists" role="menu">
                                            <li attr-id="revenueSelect" attr-value=""><a>All</a></li>
                                            <?php
                                            if (isset($countries) and ! empty($countries)) {
                                                foreach ($countries as $key => $single) {
                                                    ?>
                                                    <li attr-value="<?php // echo $key; ?>"  attr-id="revenueSelect" class=""><a><?php // echo $single; ?></a></li>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>-->
                                <div class="col-md-4">
                                    <a href="/products" class="btn btn-space btn-primary">View All</a>
                                </div>
                            </div>
                            <span class="title">Top Products</span>
                            <span class="description">By Revenue</span>
                        </div>
                        <div class="revenueProducts">
                        <?php if (isset($products_by_revenue_count) and ! empty($products_by_revenue_count)) {
                            ?>     
                            <!--                    <a href="javascript:void(0)" class="carouseller__left">‹</a>-->
                            <?php if ($products_by_revenue_count > 4) { ?>
                                <a class="icon-container carouseller__left carouseller__left1" href="javascript:void(0)">
                                    <span class="icon"><span class="mdi mdi-arrow-left"></span></span>
                                </a>
                            <?php } ?>
                            <div class="carouseller__wrap"> 
                                <div class="carouseller__list "> 
                                    <?php
                                    if (isset($products_by_revenue) and ! empty($products_by_revenue)) {
                                        foreach ($products_by_revenue as $key => $single) {
                                            if (empty($single['product']))
                                                continue;
                                            ?>
                                            <div class="car__3" data-value="<?php echo $single['product_price'] ?>">
                                                <?php
                                                if (isset($single['product']['productImages']) and ! empty($single['product']['productImages']) and isset($single['product']['productImages'][0]) and ! empty($single['product']['productImages'][0])) {
                                                    $src = $single['product']['productImages'][0]['link'];
                                                } else {
                                                    $src = '/img/elliot-logo.svg';
                                                }
                                                ?>    <a href="/products/<?php echo $single['product']['id']; ?>" ><img src="<?php echo $src ?>" height="120" width="120"></a>
                                                <?php ?>
                                                <br>
                                                <h5><a href="/products/<?php echo $single['product']['id'] ?>"><?php echo @$single['product']['product_name'] ?></a></h5>

                                                <h6><?php echo $symbol . ' ' . number_format(@$single['product']['price'], 2) ?></h6>

                                            </div>

                                            <?php
                                        }
                                    }
                                    ?>

                                </div>
                            </div>
                            <!--                        <a href="javascript:void(0)" class="carouseller__right">›</a>-->
                            <?php if ($products_by_revenue_count > 4) { ?>
                                <a class="icon-container carouseller__right carouseller__right1" href="javascript:void(0)">
                                    <span class="icon"><span class="mdi mdi-arrow-right"></span></span>
                                </a>
                            <?php } ?>
                        <?php } else {
                            ?> <h3 style="text-align: center;">No data available in table.</h3><?php
                        }
                        ?>

                    </div>
                    </div>
                </div>
            </div>
            <div class="row wizard-row panel newdashboard_container">
                <div class="col-md-12 fuelux be-loading topProductsByVolume">
                    <?php if (empty($loader_active)) { ?>
                        <div class="be-spinner">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 66 66" height="40px" width="40px">

                            <circle class="circle" r="25" cy="33" cx="33" stroke-linecap="round" stroke-width="4" fill="none"/>
                            </svg>
                        </div>
                    <?php } ?>
                    <div id="volume" class="carouseller"> 
                        <div class="widget-head panel-heading-divider ">
                            <div class="tools">
<!--                                <div class="col-md-8">

                                    <div class="btn-group btn-space pull-right">
                                        <button class="btn btn-default show-selected-country-volume" type="button">Sort</button>
                                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" aria-expanded="true"><span class="mdi mdi-chevron-down"></span><span class="sr-only">Toggle Dropdown</span></button>
                                        <ul class="dropdown-menu CountriesLists" role="menu">
                                            <li attr-id="volumeSelect" attr-value=""><a>All</a></li>
                                            <?php
                                            if (isset($countries) and ! empty($countries)) {
                                                foreach ($countries as $key => $single) {
                                                    ?>
                                                    <li attr-value="<?php // echo $key; ?>"  attr-id="volumeSelect" class=""><a><?php // echo $single; ?></a></li>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>-->
                                <div class="col-md-4">
                                    <a href="/products" class="btn btn-space btn-primary">View All</a>
                                </div>
                            </div>
                            <span class="title">Top Products</span>
                            <span class="description">By Volume</span>
                        </div>
                           <div class="volumeProducts">
                        <?php
                        if (isset($products_by_volume_count) and ! empty($products_by_volume_count)) {
                            if ($products_by_volume_count > 4) {
                                ?> <a href="javascript:void(0)" class="icon-container carouseller__left carouseller__left1">
                                    <span class="icon"><span class="mdi mdi-arrow-left"></span></span>
                                </a><?php }
                            ?>

                            <div class="carouseller__wrap"> 
                                <div class="carouseller__list"> 
                                    <?php
                                    if (isset($products_by_volume) and ! empty($products_by_volume)) {
                                        foreach ($products_by_volume as $key => $single) {
                                            if (empty($single['product']))
                                                continue;
                                            ?>
                                            <div class="car__3" data-value="<?php echo $single['product_quantity'] ?>">
                                                <?php
                                                if (isset($single['product']['productImages']) and ! empty($single['product']['productImages']) and isset($single['product']['productImages'][0]) and ! empty($single['product']['productImages'][0])) {
                                                    $src = $single['product']['productImages'][0]['link'];
                                                } else {
                                                    $src = '/img/elliot-logo.svg';
                                                }
                                                ?>                                      <a href="/products/<?php echo $single['product']['id']; ?>" ><img src="<?php echo $src ?>" height="120" width="120"></a>
                                                <?php ?>
                                                <br>
                                                <h5><a href="/products/<?php echo $single['product']['id'] ?>"><?php echo @$single['product']['product_name'] ?></a></h5>

                                                <h6><?php echo $symbol . ' ' . number_format(@$single['product']['price'], 2) ?></h6>

                                            </div>

                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <?php
                            if ($products_by_volume_count > 4) {
                                ?>  <a href="javascript:void(0)" class="icon-container carouseller__right carouseller__right1">
                                    <span class="icon"><span class="mdi mdi-arrow-right"></span></span>
                                </a>         <?php
                            }
                        } else {
                            ?>  <h3 style="text-align: center;">No data available in table.</h3><?php }
                        ?>
                    </div>
                    </div>
                </div>
            </div>


            <script type="text/javascript">
                $(function () {
                    $('#revenue').carouseller();
                    $('#volume').carouseller();
                    $('.CountriesLists li').on('click', function () {

                        $this = $(this);
                        country_code = $this.attr('attr-value');
                        id = $this.attr('attr-id');
                        selected_country = $this.text();

                        country_code_volume = '';
                        country_code_revenue = '';
                        if (id == 'volumeSelect') {
                            $('.show-selected-country-volume').html(selected_country);
                            country_code_volume = country_code;
                            divClass = 'volumeProducts';

                        }
                        if (id == 'revenueSelect') {
                            $('.show-selected-country-revenue').html(selected_country);
                            country_code_revenue = country_code;
                            divClass = 'revenueProducts';

                        }
                        $.ajax({
                            type: 'Post',
                            //                dataType: 'json',
                            data: {country_code_revenue: country_code_revenue, country_code_volume: country_code_volume, id: id},
                            url: '/lazada/get-filtered-products',
                            success: function (data) {
                                if (data) {

                                    $('.' + divClass).html(data);
                                    console.log($('.' + divClass).children().length);
                                    if ($('.' + divClass).children().length <= 4) {
                                        $('#' + id).find('.icon-container').hide();
                                    } else {
                                        $('#' + id).find('.icon-container').show();

                                    }
                                } else {
                                    //                                $('#' + id).prop('selectedIndex', 0);
                                    $('.product_ajax_request_error').modal('show');
                                }

                            }
                        });
                    });


                });

            </script>
        <?php endif; ?>
    <?php } ?> 
</div>
<div id="mod-danger" tabindex="-1" role="dialog" class="modal fade in product_ajax_request_error" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close lazada_error_modal_close"></span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
                    <h3 id=''>Error</h3>
                    <p id="">No data Found for the selected Region.</p>
                    <div class="xs-mt-50">
                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default lazada_error_modal_close">Close</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<style>
    .SortChannelList li a{
        cursor: pointer;
    }
</style>