<?php
/* @var $this yii\web\View */

use backend\models\Channels;
use yii\db\Query;
use backend\models\Orders;
use backend\models\OrderChannel;
use backend\models\OrdersProducts;
use backend\models\StoresConnection;
use backend\models\Stores;
use backend\models\ChannelConnection;
use backend\models\CronTasks;
use backend\models\CustomerUser;
use backend\models\Products;
use backend\models\ProductChannel;
use yii\web\Session;

$this->title = 'Dashboard | Elliot';
$basepath = Yii::getAlias('@baseurl');
$user_id = Yii::$app->user->identity->id;

$store_img = '';

/* GET ALL PRODUCTS COUNT FOR SALES */
$products_count = count(Products::find()->all());
/* END ALL PRODUCTS COUNT FOR SALES */
// For recent orders 
$orders_data_recent = Orders::find()->Where(['elliot_user_id' => Yii::$app->user->identity->id])->with('customer')->orderBy(['order_ID' => SORT_DESC,])->limit(5)->all();
/* Latest Engagements */
$customer_eng_data = CustomerUser::find()->asArray()->all();
$products_eng_data = Products::find()->asArray()->all();
$orders_eng_data = Orders::find()->asArray()->all();

$late_engag_arr = array_merge($customer_eng_data, $products_eng_data, $orders_eng_data);

$sort = array();
foreach ($late_engag_arr as $k => $v) {
    $sort['created_at'][$k] = $v['created_at'];
}
if (!empty($sort['created_at'])) {
    array_multisort($sort['created_at'], SORT_DESC, $late_engag_arr);
}

$lates_engagements = array_slice($late_engag_arr, 0, 5);

/* End Latest Engagements */

$StoresConnection = StoresConnection::find()->all();
$ChannelConnection = ChannelConnection::find()->all();

if (Yii::$app->user->identity->role == 'superadmin') {
    echo "<p>Admin Dashboards</p>";
    //use for base directory//
    $basedir = Yii::getAlias('@basedir');
    //include stripe init file//
    require $basedir . '/stripe/init.php';
    \Stripe\Stripe::setApiKey("sk_test_2GPaBt5mT2k0yyTA9ho3CPIQ");
    //get subscription plan through stripe//
    $collection_plans = \Stripe\Plan::all(array("limit" => 100));
    $stripe_plans = $collection_plans->__toArray(true);
    $stripe_plan_data = $stripe_plans['data'];
    foreach ($stripe_plan_data as $data) {
        $db_stripe_channel_data = Channels::find()->Where(['channel_name' => $data['name']])->one();
        if (empty($db_stripe_channel_data)) {
            $meta = @$data['metadata']['image_url'];
            $id = $data['id'];
            $prefix_channel = substr($id, 0, 7);
            if ($prefix_channel == 'channel') {
                $stripe_Channel = new Channels();
                $stripe_Channel->stripe_Channel_id = stripslashes($data['id']);
                $stripe_Channel->amount = stripslashes($data['amount'] / 100);
                $stripe_Channel->currency = stripslashes($data['currency']);
                $stripe_Channel->channel_image = stripslashes(@$data['metadata']['image_url']);
                $stripe_Channel->channel_name = stripslashes($data['name']);
                $stripe_Channel->parent_name = stripslashes(@$data['metadata']['parent_name']);
                $stripe_Channel->trial_period_days = stripslashes($data['trial_period_days']);
                $stripe_Channel->save(false);
            }
        } else {
            $meta = @$data['metadata']['image_url'];
            $id = $data['id'];
            $prefix_channel = substr($id, 0, 7);
            if ($prefix_channel == 'channel') {
                $stripe_Channel = Channels::find()->Where(['channel_name' => $data['name']])->one();
                $stripe_Channel->stripe_Channel_id = stripslashes($data['id']);
                $stripe_Channel->amount = stripslashes($data['amount'] / 100);
                $stripe_Channel->currency = stripslashes($data['currency']);
                $stripe_Channel->channel_image = stripslashes(@$data['metadata']['image_url']);
                $stripe_Channel->channel_name = stripslashes($data['name']);
                $stripe_Channel->parent_name = stripslashes(@$data['metadata']['parent_name']);
                $stripe_Channel->trial_period_days = stripslashes($data['trial_period_days']);
                $stripe_Channel->save(false);
            }
        }
    }
    ?>
    <div class="main-content container-fluid">

        <div class="row marketplace-listing">
            <?php
            $connection = \Yii::$app->db;
            //$model1 = $connection->createCommand('SELECT DISTINCT parent_name stripe_Channel_id,channel_image,parent_name FROM channels');
            $model1 = $connection->createCommand('SELECT COUNT(parent_name), parent_name FROM channels GROUP BY parent_name');
            $channels = $model1->queryAll();
            foreach ($channels as $channels_data) {
                $count_parent_name = $channels_data['COUNT(parent_name)'];
                $parent_name = $channels_data['parent_name'];
                ?>
                <?php
                if ($count_parent_name == 1) {
                    $marketplace = Channels::find()->Where(['parent_name' => $parent_name])->one();
                    if ($marketplace->parent_name == 'channel_Square') {
                        continue;
                    }
                    ?>
                    <?php if ($marketplace->stripe_Channel_id == 'channel_Instagram') { ?>
                        <div class="col-sm-3">
                            <div class="bs-grid-block comingsoon" data-toggle="modal"  data-target="#<?php echo $marketplace->stripe_Channel_id; ?> ">
                                <div class="content">
                                    <span class="label label-success custom-span-channels-label span-top-22" style=" position: absolute; right: 30px; ">Coming Soon</span>
                                    <img src="<?php echo $marketplace->channel_image; ?>" style="width: 100%" alt="<?php echo $marketplace->stripe_Channel_id; ?>">
                                </div>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="col-sm-3">
                            <div class="bs-grid-block">
                                <div class="content">
                                    <img src="<?php echo $marketplace->channel_image; ?>" style="width: 100%;" alt="" class="<?php echo $marketplace->stripe_Channel_id == "channel_Xiao_Hong_Shu" ? 'resize_Xiao' : ''; ?>">
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <?php
                } else if ($count_parent_name != 1) {
                    $marketplace = Channels::find()->Where(['parent_name' => $parent_name])->one();
                    if ($marketplace->parent_name == 'channel_WeChat') {
                        ?>
                        <div class="col-sm-3">
                            <div class="bs-grid-block" data-toggle="modal"  data-target="#<?php echo $marketplace->stripe_Channel_id; ?> ">
                                <div class="content">
                                    <!--span  class="label label-success span-top-22" style=" position: absolute; right: 30px; ">Connected</span-->
                                    <img src="<?php echo $marketplace->channel_image; ?>" style="width: 100%" alt="<?php echo $marketplace->stripe_Channel_id; ?>">
                                </div>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="col-sm-3">
                            <div class="bs-grid-block" data-toggle="modal"  data-target="#<?php echo $marketplace->stripe_Channel_id; ?> ">
                                <div class="content">

                                    <img src="<?php echo $marketplace->channel_image; ?>" style="width: 100%;" alt="<?php echo $marketplace->stripe_Channel_id; ?>">
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                }
            }
            ?>
        </div>
    </div>

    <?php
    $connection1 = \Yii::$app->db;

    $model2 = $connection->createCommand('SELECT COUNT(parent_name), parent_name FROM channels GROUP BY parent_name');

    $channels1 = $model2->queryAll();

    foreach ($channels1 as $channels_data1) {
        $count_parent_name1 = $channels_data1['COUNT(parent_name)'];

        $parent_name1 = $channels_data1['parent_name'];

        $marketplace1 = Channels::find()->Where(['parent_name' => $parent_name1])->one();

        if (empty($marketplace1)):
            $name = "test";
        else:
            $name = $marketplace1->channel_name;
        endif;
        ?>
        <?php if ($count_parent_name1 == 1) { ?>
            <div id="<?php echo $marketplace1->stripe_Channel_id; ?>" tabindex="-1" role="dialog" class="modal fade admin_pg">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                        </div>
                        <div class="modal-body">
                            <div class="text-center">
                                <div class="text-primary"><img src="<?php echo $marketplace1->channel_image; ?>" style="width: 50%;" alt=""></span></div>
                                <h3>Coming Soon</h3>
                                <p>Subscribe to get updated when <?= $name ?> becomes available in Elliot.</p>
                                <div class="xs-mt-50"> 
                                    <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Close</button>
                                    <button type="button" data-dismiss="modal" class="btn btn-space btn-primary">Subscribe</button>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer"></div>
                    </div>
                </div>
            </div>
        <?php } else if ($count_parent_name1 != 1) { ?>  
            <div id="<?php echo $marketplace1->stripe_Channel_id; ?>" tabindex="-1" role="dialog" class="modal fade admin_pg">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                        </div>
                        <div class="modal-body">
                            <div class="text-center">
                                <div class="text-primary"><img src="<?php echo $marketplace1->channel_image; ?>" style="width: 50%;" alt=""></span></div>
                                <div class="row">
                                    <div class="form-group">
                                        <?php
                                        $channels_group_data = Channels::find()->Where(['parent_name' => $parent_name1])->all();
                                        foreach ($channels_group_data as $channel_group) {
                                            ?>
                                            <div class="col-sm-3">
                                                <div class="be-radio inline">
                                                    <input type="radio" checked="" name="rad3" id="<?php echo $channel_group->channel_name; ?>" value="<?php echo $channel_group->channel_name; ?>">
                                                    <label class="channel-modal-span-group" for="<?php echo $channel_group->channel_name; ?>"><b><?php echo $channel_group->channel_name; ?></b></label>
                                                </div>

                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="xs-mt-50"> 
                                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Close</button>
                                        <button type="button" class="btn btn-space btn-primary">Subscribe</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer"></div>
                    </div>
                </div>
            </div>

            <?php
        }
    }
} else {
    ?>
    <!--if channel and store is not connected Only show Stores In Index Page!-->
    <?php
    $store_connection_data = StoresConnection::find()->Where(['user_id' => $user_id])->with('stores')->asArray()->all();
    $channels_connection_data = ChannelConnection::find()->Where(['user_id' => $user_id])->with('channels')->asArray()->all();
    $connected_data = array_merge($store_connection_data, $channels_connection_data);
    ?>
    <?php if (empty($connected_data)): ?>
        <div class="row be-connections store_logo100px">
            <div class="col-md-12">
                <div class="widget widget-fullwidth be-loading">
                    <div class="widget-head">

                        <span class="title">Connect Your Store</span>
                        <div class="list">
                            <p class="connectstore_line">Choose from the eCommerce platforms below to easily integrate your existing catalog and storefront data.</p>
                            <div class="content store_listing_container">
                                <div class="row">
                                    <div class="col-sm-3 text-center store_listing_container_sub"><a href="/stores/bigcommerce" class="connection-item"><img src="/img/bigcommerce.png" alt="Github">
                                            <p>BigCommerce</p></a>
                                    </div>
                                    <div class="col-sm-3 text-center store_listing_container_sub"><a href="/stores/magento" class="connection-item"><img src="/img/magento.png" alt="Bitbucket">
                                            <p>Magento</p></a>.
                                    </div>

                                    <div class="col-sm-3 text-center store_listing_container_sub"><a href="/magento2" class="connection-item"><img src="/img/magento2.png" alt="Magento2">
                                            <p>Magento2</p></a>
                                    </div>
                                    <div class="col-sm-3 text-center store_listing_container_sub"><a href="javascript:void(0)" class="connection-item"><img src="/img/netsuite.png" alt="Oracle Commerce Cloud">
                                            <p>NetSuite SuiteCommerce</p></a>
                                    </div>
                                   <div class="col-sm-3 text-center store_listing_container_sub"><a href="javascript:void(0)" class="connection-item"><img src="/img/oraclecommercecloud.png" alt="Oracle Commerce Cloud">
                                            <p>Oracle Commerce Cloud</p></a>
                                    </div>

                                    <div class="col-sm-3 text-center store_listing_container_sub"><a href="/stores/shopify" class="connection-item"><img src="/img/shopify.png" alt="Github">
                                            <p>Shopify</p></a>
                                    </div>
                                    <div class="col-sm-3 text-center store_listing_container_sub"><a href="/stores/shopify" class="connection-item"><img src="/img/shopifyplus.png" alt="ShopifyPlus">
                                            <p>ShopifyPlus</p></a>
                                    </div>
                                <div class="col-sm-3 text-center store_listing_container_sub"><a href="javascript:void(0)" class="connection-item"><img src="/img/salesforcecommercecloud.png" alt="Salesforce Commerce Cloud">
                                            <p>Salesforce Commerce Cloud</p></a>
                                    </div>
                                    <div class="col-sm-3 text-center store_listing_container_sub"><a href="/vtex" class="connection-item"><img src="/img/vtex.png" alt="VTEX">
                                            <p>VTEX</p></a>.
                                    </div>
                                    <div class="col-sm-3 text-center store_listing_container_sub"><a href="stores/woocommerce" class="connection-item"><img src="/img/woocommerce.png" alt="Bitbucket">
                                            <p>WooCommerce</p></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!--                    <div class="widget-chart-container">
                                            <center><button class="btn btn-space btn-primary" id="see-overview" style="margin-bottom: 26px;">Connect Your Store to see Data</button></center>
                                        </div>-->

                </div>
            </div>
        </div>
    <?php else: ?>
        <!--END if channel and store is not connected Only show Stores In Index Page!-->
        <?php
        /*         * for showing loader for first time when the data is importing* */
        $curr_userid = Yii::$app->user->identity->id;
        $connection = \Yii::$app->db;
        $check_store = $connection->createCommand("SELECT COUNT(stores_connection_id) AS stores FROM stores_connection WHERE user_id='" . $curr_userid . "'");
        $check_store_query = $check_store->queryAll();
        $store_count = @$check_store_query[0]['stores'];
        $check_channel = $connection->createCommand("SELECT COUNT(channel_connection_id) AS channels FROM channel_connection WHERE user_id='" . $curr_userid . "'");
        $check_channel_query = $check_channel->queryAll();
        $channel_count = @$check_channel_query[0]['channels'];


        $store_import_status = $connection->createCommand("SELECT import_status FROM stores_connection WHERE user_id='" . $curr_userid . "'");
        $store_import = $store_import_status->queryAll();
        $store_status = @$store_import[0]['import_status'];

        $channel_import_status = $connection->createCommand("SELECT import_status FROM channel_connection WHERE user_id='" . $curr_userid . "'");
        $channel_import = $channel_import_status->queryAll();
        $channel_status = @$channel_import[0]['import_status'];

// if($store_count == 1 || $channel_count == 0) {
//     // $channel_import_status = $connection->createCommand("SELECT import_status FROM channel_connection WHERE user_id='".$curr_userid."'");
//     // $channel_import = $channel_import_status->queryAll();
//     // $channel_status = @$channel_import[0]['import_status'];
// }
// if($store_count == 0 || $channel_count == 1) {
// }
        ?>
        <!--END if channel and store is connected show Index Page!-->
        <?php $loader_active = ''; ?>
        <?php
        if ($store_count == 1 && $channel_count == 0) {
            if ($store_status == '') {
                $loader_active = 'be-loading-active';
            }
        } elseif ($store_count == 0 && $channel_count == 1) {
            if ($channel_status == '') {
                $loader_active = 'be-loading-active';
            }
        }
        ?>
        <div class="be-loading <?php echo $loader_active; ?>" style="z-index:999;">
            <!--*********************************DASHBOARD MAIN CHART START HERE********************************************************************************-->
                      <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default OrderChaRT <?php if(empty($loader_active)) {echo 'be-loading';} ?>" style="z-index:999;">
                                    <div class="panel-heading panel-heading-divider">
                                        <div class="widget-head">
                                            <div class="tools">
                                                <div class="dropdown"><span data-toggle="dropdown" class="icon mdi mdi-more-vert visible-xs-inline-block dropdown-toggle" aria-expanded="false"></span>
                                                    <ul role="menu" class="dropdown-menu">
                                                        <li><a  id="dashboardchartweekmob" class="btn btn-default customPeople "  style="cursor:pointer">Week</a></li>
                                                        <li><a  id="dashboardchartmonthmob" class="btn btn-default customPeople" style="cursor:pointer">Month</a></li>
                                                        <li><a  id="dashboardchartyearmob" class="btn btn-default customPeople" style="cursor:pointer">Year</a></li>
                                                        <li class="divider"></li>
                                                      <li><a  id="dashboardcharttodaymob" class="btn btn-default customPeople "  style="cursor:pointer">Today</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="button-toolbar hidden-xs">
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-default customPeople " id="dashboardchartweek">Week</button>
                                                    <button type="button" class="btn btn-default customPeople" id="dashboardchartmonth">Month</button>
                                                    <button type="button" class="btn btn-default customPeople" id="dashboardchartyear">Year</button>
                                                    <input name="" value="Year" id="hidden_graph" type="hidden">
                                                </div>
                                                <div class="btn-group">
                                                   <button type="button" class="btn btn-default customPeople" id="dashboardcharttoday">Today</button>
                                                </div>
                                            </div>
                                            <span class="title">Global Performance</span> 
                                        </div>
                                        <div class="be-spinner" style="width:100%; text-align: center; right:auto">
                                            <svg width="40px" height="40px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
            
                                            <circle fill="none" stroke-width="4" stroke-linecap="round" cx="33" cy="33" r="30" class="circle"></circle>
                                            </svg>
            <?php if (isset($loader_active) and !empty($loader_active) and $loader_active == 'be-loading-active') {
                ?><span style="display:block; padding-top:30px;">Your data is importing, you will be emailed when it is complete.</span><?php } else {
					?>
				<span style="display:block; padding-top:30px;">Your data is loading.</span>	
			<?php 	}
            ?>
                                        </div>
            
                                        <span class="panel-subtitle space"><ul id="chartInfoPeople" class="chart-legend-horizontal">
            
                                            </ul>
                                        </span>
            
                                    </div>
                                    <div id="padding-panel-body" class="panel-body"> 
            
                                        <div class="dashboard-chart" id="dashboard-chart" style="height: 250px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
            <!--**************************************************************************#############################*****************************************************-->
               

            <!--STARTS WEEKLY PERFORMANCE GRAPH!-->
            <div class="row" id="week_row_div" style="display:none">
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <div class="widget widget-tile">
                        <div id="spark1" class="chart sparkline"></div>
                        <div class="data-info">
                            <div class="desc">New Orders</div>
                            <div class="value">
                                <span class="indicator indicator-equal mdi mdi-chevron-right"></span>
                                <?php
                                $current_date = date('Y-m-d h:i:s', time());
                                $Week_previous_date_orders = date('Y-m-d h:i:s', strtotime('-6 days'));
                                $new_orders_count = count(Orders::find()->Where(['between', 'created_at', $Week_previous_date_orders, $current_date])->all());
                                ?>
                                <span data-toggle="counter" data-end="<?= $new_orders_count; ?>" class="number"><?= $new_orders_count; ?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <div class="widget widget-tile">
                        <div id="spark2" class="chart sparkline"></div>
                        <div class="data-info">
                            <div class="desc">Weekly Sales</div>
                            <div class="value">
                                <span class="indicator indicator-positive mdi mdi-chevron-up"></span>
                                <?php if ($new_orders_count != 0): ?> 
                                    <span data-toggle="counter" data-end="<?= $new_orders_count * 100 / $products_count; ?>" data-suffix="%" class="number"><?= $new_orders_count * 100 / $products_count; ?></span>
                                <?php else: ?>
                                    <span data-toggle="counter" data-end="0" data-suffix="%" class="number">0</span>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <div class="widget widget-tile">
                        <div id="spark3" class="chart sparkline"></div>
                        <div class="data-info">
                            <div class="desc">Products Sold</div>
                            <div class="value">
                                <span class="indicator indicator-positive mdi mdi-chevron-up"></span>
                                <?php
                                $orders_products_data = count(OrdersProducts::find()->Where(['between', 'created_at', $Week_previous_date_orders, $current_date])->all());
                                if (!empty($orders_products_data)):
                                    ?>
                                    <span data-toggle="counter" data-end="<?= $orders_products_data; ?>" class="number"><?= $orders_products_data; ?></span>
                                <?php else: ?>
                                    <span data-toggle="counter" data-end="0" class="number">0</span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <div class="widget widget-tile">
                        <div id="spark4" class="chart sparkline"></div>
                        <div class="data-info">
                            <div class="desc">Avg. Order Value</div>
                            <div class="value">
                                <span class="indicator indicator-negative mdi mdi-chevron-down"></span>
                                <?php
                                $connection = \Yii::$app->db;
                                $orders_average_data = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE created_at BETWEEN "' . $Week_previous_date_orders . '" AND "' . $current_date . '"');
                                $orders_average = $orders_average_data->queryAll();
                                ?>
                                <?php if (!empty($orders_average)): ?>
                                    <span data-toggle="counter" data-end="<?= $orders_average[0]['average']; ?>" class="number"><?= $orders_average[0]['average']; ?></span>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!--END STARTS MONTHLY PERFORMANCE GRAPH!-->
            <div class="row" id="month_row_div" style="display:none">
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <div class="widget widget-tile">
                        <div id="sparkm1" class="chart sparkline"></div>
                        <div class="data-info">
                            <div class="desc">New Orders</div>
                            <div class="value">
                                <span class="indicator indicator-equal mdi mdi-chevron-right"></span>
                                <?php
                                $current_date = date('Y-m-d h:i:s', time());
                                $month_previous_date_orders = date('Y-m-d h:i:s', strtotime('-30 days'));
                                $new_orders_count = count(Orders::find()->Where(['between', 'created_at', $month_previous_date_orders, $current_date])->all());
                                ?>
                                <span data-toggle="counter" data-end="<?= $new_orders_count; ?>" class="number"><?= $new_orders_count; ?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <div class="widget widget-tile">
                        <div id="sparkm2" class="chart sparkline"></div>
                        <div class="data-info">
                            <div class="desc">Weekly Sales</div>
                            <div class="value">
                                <span class="indicator indicator-positive mdi mdi-chevron-up"></span>
                                <?php if ($new_orders_count != 0): ?> 
                                    <span data-toggle="counter" data-end="<?= $new_orders_count * 100 / $products_count; ?>" data-suffix="%" class="number"><?= $new_orders_count * 100 / $products_count; ?></span>
                                <?php else: ?>
                                    <span data-toggle="counter" data-end="0" data-suffix="%" class="number">0</span>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <div class="widget widget-tile">
                        <div id="sparkm3" class="chart sparkline"></div>
                        <div class="data-info">
                            <div class="desc">Products Sold</div>
                            <div class="value">
                                <span class="indicator indicator-positive mdi mdi-chevron-up"></span>
                                <?php
                                $orders_products_data = count(OrdersProducts::find()->Where(['between', 'created_at', $month_previous_date_orders, $current_date])->all());
                                if (!empty($orders_products_data)):
                                    ?>
                                    <span data-toggle="counter" data-end="<?= $orders_products_data; ?>" class="number"><?= $orders_products_data; ?></span>
                                <?php else: ?>
                                    <span data-toggle="counter" data-end="0" class="number">0</span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <div class="widget widget-tile">
                        <div id="sparkm4" class="chart sparkline"></div>
                        <div class="data-info">
                            <div class="desc">Avg. Order Value</div>
                            <div class="value">
                                <span class="indicator indicator-negative mdi mdi-chevron-down"></span>
                                <?php
                                $connection = \Yii::$app->db;
                                $orders_average_data = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE created_at BETWEEN "' . $month_previous_date_orders . '" AND "' . $current_date . '" GROUP BY date(created_at)');
                                $orders_average = $orders_average_data->queryAll();
                                ?>
                                <?php if (!empty($orders_average)): ?>
                                    <span data-toggle="counter" data-end="<?= $orders_average[0]['average']; ?>" class="number"><?= $orders_average[0]['average']; ?></span>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!--STARTS TODAY PERFORMANCE GRAPH!-->
            <div class="row" id="today_row_div" style="display:none">
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <div class="widget widget-tile">
                        <div id="sparkToday1" class="chart sparkline"></div>
                        <div class="data-info">
                            <div class="desc">New Orders</div>
                            <div class="value">
                                <span class="indicator indicator-equal mdi mdi-chevron-right"></span>
                                <?php
                                $current_date_today = date('Y-m-d', time());
                                $connection = \Yii::$app->db;
                                $orders_data_query = $connection->createCommand('select * FROM orders WHERE date(created_at)="' . $current_date_today . '" ');
                                $orders_data = $orders_data_query->queryAll();
                                $count_orders_data = count($orders_data);
                                ?>
                                <span data-toggle="counter" data-end="<?= $count_orders_data; ?>" class="number"><?= $count_orders_data; ?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <div class="widget widget-tile">
                        <div id="sparkToday2" class="chart sparkline"></div>
                        <div class="data-info">
                            <div class="desc">Today Sales</div>
                            <div class="value">
                                <span class="indicator indicator-positive mdi mdi-chevron-up"></span>
                                <?php if ($count_orders_data != 0): ?> 
                                    <span data-toggle="counter" data-end="<?= $new_orders_count * 100 / $products_count; ?>" data-suffix="%" class="number"><?= $new_orders_count * 100 / $products_count; ?></span>
                                <?php else: ?>
                                    <span data-toggle="counter" data-end="0" data-suffix="%" class="number">0</span>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <div class="widget widget-tile">
                        <div id="sparkToday3" class="chart sparkline"></div>
                        <div class="data-info">
                            <div class="desc">Products Sold</div>
                            <div class="value">
                                <span class="indicator indicator-positive mdi mdi-chevron-up"></span>
                                <?php
                                $connection = \Yii::$app->db;
                                $orders_data_query = $connection->createCommand('select * FROM orders_products WHERE date(created_at)="' . $current_date_today . '" ');
                                $orders_products_data = $orders_data_query->queryAll();
                                $count_orders_products_data = count($orders_products_data);
                                if (!empty($orders_products_data)):
                                    ?>
                                    <span data-toggle="counter" data-end="<?= $count_orders_products_data; ?>" class="number"><?= $count_orders_products_data; ?></span>
                                <?php else: ?>
                                    <span data-toggle="counter" data-end="0" class="number">0</span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <div class="widget widget-tile">
                        <div id="sparkToday4" class="chart sparkline"></div>
                        <div class="data-info">
                            <div class="desc">Avg. Order Value</div>
                            <div class="value">
                                <span class="indicator indicator-negative mdi mdi-chevron-down"></span>
                                <?php
                                $current_date = date('Y-m-d', time());
                                $connection = \Yii::$app->db;
                                $orders_data_query = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE date(created_at)="' . $current_date . '" GROUP BY created_at');
                                $orders_average = $orders_data_query->queryAll();
                                $count_orders_average = count($orders_average);
                                ?>
                                <?php if (!empty($orders_average)): ?>
                                    <span data-toggle="counter" data-end="<?= $orders_average[0]['average']; ?>" class="number"><?= $orders_average[0]['average']; ?></span>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--STARTS YEARLY PERFORMANCE GRAPH!-->
            <div class="row" id="Year_row_div">
                <div class="col-xs-12 col-md-6 col-lg-3 ">
                    <div class="widget widget-tile be-loading orderDaSH">
                        <div id="sparkYear1" class="chart sparkline"></div>
                        <?php if (empty($loader_active)) { ?>
                            <div class="be-spinner">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 66 66" height="40px" width="40px">

                                <circle class="circle" r="25" cy="33" cx="33" stroke-linecap="round" stroke-width="4" fill="none"/>
                                </svg>
                            </div>
                        <?php } ?>

                        <div class="data-info">
                            <div class="desc">New Orders</div>
                            <div class="value">
                                <span class="indicator indicator-equal mdi mdi-chevron-right"></span>
                                <?php
                                $current_d = date('m');
                                $current_y = date('Y');
                                $month = (int) $current_d;

                                $connection = \Yii::$app->db;
                                $orders_data = $connection->createCommand('SELECT * from orders WHERE YEAR(created_at)="' . $current_y . '"  ');
                                $orders_count = count($orders_data->queryAll());

                                if (!empty($orders_count)):
                                    $new_orders_count = $orders_count;
                                else:
                                    $new_orders_count = 0;
                                endif;
                                ?>
                                <span data-toggle="counter" data-end="<?= $new_orders_count; ?>" class="number"><?= $new_orders_count; ?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <div class="widget widget-tile be-loading orderDaSH">
                        <div id="sparkYear2" class="chart sparkline"></div>
                        <?php if (empty($loader_active)) { ?>
                            <div class="be-spinner">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 66 66" height="40px" width="40px">

                                <circle class="circle" r="25" cy="33" cx="33" stroke-linecap="round" stroke-width="4" fill="none"/>
                                </svg>
                            </div>
                        <?php } ?>
                        <div class="data-info">
                            <div class="desc">Yearly Sales</div>
                            <div class="value">
                                <span class="indicator indicator-positive mdi mdi-chevron-up"></span>
                                <?php if ($new_orders_count != 0): ?> 
                                    <span data-toggle="counter" data-end="<?= $new_orders_count * 100 / $products_count; ?>" data-suffix="%" class="number"><?= $new_orders_count * 100 / $products_count; ?></span>
                                <?php else: ?>
                                    <span data-toggle="counter" data-end="0" data-suffix="%" class="number">0</span>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <div class="widget widget-tile be-loading ProductsDaSH">
                        <div id="sparkYear3" class="chart sparkline"></div>
                        <?php if (empty($loader_active)) { ?>
                            <div class="be-spinner">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 66 66" height="40px" width="40px">

                                <circle class="circle" r="25" cy="33" cx="33" stroke-linecap="round" stroke-width="4" fill="none"/>
                                </svg>
                            </div>
                        <?php } ?>
                        <div class="data-info">
                            <div class="desc">Products Sold</div>
                            <div class="value">
                                <span class="indicator indicator-positive mdi mdi-chevron-up"></span>
                                <?php
                                $current_d = date('m');
                                $current_y = date('Y');
                                $month = (int) $current_d;
                                $connection = \Yii::$app->db;
                                $orders_data = $connection->createCommand('SELECT * from orders_products WHERE YEAR(created_at)="' . $current_y . '"  ');
                                $orders_products_data = count($orders_data->queryAll());

                                if (!empty($orders_products_data)):
                                    ?>
                                    <span data-toggle="counter" data-end="<?= $orders_products_data; ?>" class="number"><?= $orders_products_data; ?></span>
                                <?php else: ?>
                                    <span data-toggle="counter" data-end="0" class="number">0</span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-3">
                    <div class="widget widget-tile be-loading avgOrderDaSH">
                        <div id="sparkYear4" class="chart sparkline"></div>
                        <?php if (empty($loader_active)) { ?>
                            <div class="be-spinner">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 66 66" height="40px" width="40px">

                                <circle class="circle" r="25" cy="33" cx="33" stroke-linecap="round" stroke-width="4" fill="none"/>
                                </svg>
                            </div>
                        <?php } ?>
                        <div class="data-info">
                            <div class="desc">Avg. Order Value</div>
                            <div class="value">
                                <span class="indicator indicator-negative mdi mdi-chevron-down"></span>
                                <?php
                                $current_d = date('m');
                                $current_y = date('Y');
                                $month = (int) $current_d;
                                $connection = \Yii::$app->db;
                                $orders_average_data = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE YEAR(created_at)="' . $current_y . '"');
                                $orders_average = $orders_average_data->queryAll();
                                ?>
                                <?php if (!empty($orders_average)): ?>
                                    <span data-toggle="counter" data-end="<?= $orders_average[0]['average']; ?>" class="number"><?= $orders_average[0]['average']; ?></span>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>






            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default panel-table">
                        <div class="panel-heading"> 
                            <div class="title">Recent Orders</div>
                        </div>
                        <?php if (empty($StoresConnection) && empty($ChannelConnection)) : ?>
                            <center><button class="btn btn-space btn-primary" id="see">Connect Your Store to see Data</button></center>
                        <?php else : ?>
                            <div class="panel-body table-responsive ">
                                <table id="recent_orders_dashboard" class="table-borderless table table-striped table-hover table-fw-widget dataTable">
                                    <thead>
                                        <tr>
                                            <th style="width:20%;">Order ID</th>
                                            <th>Customer Name</th>
                                            <th class="number">Amount</th>
                                            <th style="width:20%;">Date</th>
                                            <th style="width:20%;">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody class="no-border-x">
                                        <?php if (empty($orders_data_recent)): ?>
                                                                                    <!--tr class="odd"><td valign="top" colspan="7" class="dataTables_empty">No data available in table.</td> </tr-->
                                            <?php
                                        else :
                                            foreach ($orders_data_recent as $orders_data_value) :
                                                $channel_abb_id = isset($orders_data_value->channel_abb_id) ? $orders_data_value->channel_abb_id : "";
                                                $firstname = $orders_data_value->customer->first_name;
                                                $lname = $orders_data_value->customer->last_name;
                                                $order_amount = isset($orders_data_value->total_amount) ? $orders_data_value->total_amount : 0;
                                                $order_value = number_format((float) $order_amount, 2, '.', '');
                                                $date_order = date('M-d-Y', strtotime($orders_data_value->order_date));
                                                $order_status = $orders_data_value->order_status;
                                                $label = '';
                                                if ($order_status == 'Completed') :
                                                    $label = 'label-success';
                                                endif;

                                                if ($order_status == 'Returned' || $order_status == 'Refunded' || $order_status == 'Cancel') :
                                                    $label = 'label-danger';
                                                endif;

                                                if ($order_status == 'In Transit' || $order_status == 'On Hold'):
                                                    $label = 'label-primary';
                                                endif;

                                                if ($order_status == 'Awaiting Fulfillment' || $order_status == 'Incomplete' || $order_status == 'waiting-for-shipment' || $order_status == 'Pending'):
                                                    $label = 'label-warning';
                                                endif;
                                                if ($order_status == 'Shipped'):
                                                    $label = 'label-primary';
                                                endif;


                                                $conversion_rate = 1;
                                                $user = Yii::$app->user->identity;
                                                if (isset($user->currency) and $user->currency != 'USD') {
                                                    $username = Yii::$app->params['xe_account_id'];
                                                    $password = Yii::$app->params['xe_account_api_key'];
                                                    $URL = Yii::$app->params['xe_base_url'] . 'convert_from.json/?from=USD&to=' . $user->currency . '&amount=1';

                                                    $ch = curl_init();
                                                    curl_setopt($ch, CURLOPT_URL, $URL);
                                                    curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
                                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                                    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                                                    curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
                                                    $result = curl_exec($ch);
                                                    $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
                                                    curl_close($ch);
//echo'<pre>';
                                                    $result = json_decode($result, true);
                                                    if (isset($result) and !empty($result) and isset($result['to']) and isset($result['to'][0]) and isset($result['to'][0]['quotecurrency'])) {
                                                        $conversion_rate = $result['to'][0]['mid'];
                                                        $conversion_rate = number_format((float) $conversion_rate, 2, '.', '');
                                                        $order_value = $order_value * $conversion_rate;
                                                        $order_value = number_format((float) $order_value, 2, '.', '');
                                                    }
                                                }

                                                $selected_currency = \backend\models\CurrencySymbols::find()->where(['name' => strtolower($user->currency)])->select(['id', 'symbol'])->asArray()->one();
                                                if (isset($selected_currency) and !empty($selected_currency)) {
                                                    $currency_symbol = $selected_currency['symbol'];
                                                }
                                                ?>
                                                <tr>
                                                    <td><a href="orders/view/<?php echo $orders_data_value->order_ID; ?>"><?= $channel_abb_id; ?></a></td>
                                                    <td class="captialize"><?= $firstname . ' ' . $lname; ?></td>
                                                    <td class="number"><?php echo $currency_symbol ?><?= number_format($order_value, 2); ?></td>
                                                    <td><?= $date_order; ?></td>
                                                    <td><span class="label  <?= $label; ?>"><?= $order_status; ?></span></td>
                                                </tr>
                                                <?php
                                            endforeach;
                                        endif;
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php
                        endif;
                        ?> 

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default panel-table">
                        <div class="panel-heading">
                            <div class="title">Latest Engagements</div>
                        </div>
                        <div class="panel-body table-responsive">
                            <table id="latest_engagement_dashboard" class="table-borderless table table-striped table-hover table-fw-widget dataTable latest-engagements">
                                <thead>
                                    <tr>
                                        <th style="width:37%;">Name</th>
                                        <th style="width:36%;">Engagement Type</th>
                                        <th>Channel</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php if (empty($lates_engagements)) : ?>
                                                                                <!--tr class="odd"><td valign="top" colspan="7" class="dataTables_empty">No data available in table.</td> </tr-->
                                    <?php else : ?> 
                                        <?php foreach ($lates_engagements as $latest) :
                                            ?>
                                            <!-- For Orders!--->
                                            <?php if (array_key_exists("order_ID", $latest)): ?>
                                                <?php
                                                $order_channel_store = OrderChannel::find()->where(['order_id' => $latest['order_ID']])->with('stores')->one();

                                                if ($order_channel_store['store_id'] == NULL):
                                                    $order_channel = OrderChannel::find()->where(['order_id' => $latest['order_ID']])->with('channel')->one();

                                                    if (!empty($order_channel)):
                                                        $store_img = $order_channel->channel['parent_name'] == "channel_WeChat" ? 'img/marketplace_logos/elliot-wechat-connection-icon.png' : $order_channel->channel['channel_image'];

                                                    endif;
                                                else:
                                                    if (!empty($order_channel_store)):
                                                        $store_img = $order_channel_store->stores['store_image'];
                                                    endif;

                                                endif;
                                                ?>
                                                <tr>
                                                    <td style="width:37%;"><a href="orders/view/<?php echo $latest['order_ID']; ?>"><?= $latest['channel_abb_id']; ?></a></td>
                                                    <td style="width:36%;"><?= "Order"; ?></td>
                                                    <td><img class="ch_img" src="<?php echo $store_img; ?>" width="50" height="50" alt="Image"></td>

                                                </tr>
                                            <?php endif; ?>
                                            <!-- For Products!--->
                                            <?php if (array_key_exists("id", $latest)): ?>
                                                <?php
                                                $product_channel_store = ProductChannel::find()->where(['product_id' => $latest['id']])->with('stores')->one();
                                                if ($product_channel_store['store_id'] == NULL):
                                                    $product_channel = ProductChannel::find()->where(['product_id' => $latest['id']])->with('channel')->one();
                                                    //$store_img = isset($product_channel->channel['channel_image']) ? $product_channel->channel['channel_image'] : '';
                                                    if (isset($product_channel->channel['channel_image'])):
                                                        if ($product_channel->channel['parent_name'] == "channel_WeChat"):
                                                            $store_img = 'img/marketplace_logos/elliot-wechat-connection-icon.png';
                                                        else:
                                                            $store_img = $product_channel->channel['channel_image'];
                                                        endif;
                                                    endif;
                                                else:
                                                    $store_img = $product_channel_store->stores['store_image'];
                                                endif;
                                                ?>
                                                <tr>
                                                    <td style="width:37%;" class="captialize"><a href="products/<?php echo $latest['id']; ?>"><?= $latest['product_name']; ?></td>
                                                    <td style="width:36%;"><?= "Product"; ?></td>
                                                    <td><img class="ch_img" src="<?php echo $store_img; ?>" width="50" height="50" alt="Image"></td>

                                                </tr>
                                            <?php endif; ?>
                                            <!-- For Customers!--->
                                            <?php if (array_key_exists("customer_ID", $latest)): ?>
                                                <?php
                                                $ch_acc = $latest['channel_acquired'];
                                                $stores = Stores::find()->where(['store_name' => $ch_acc])->one();
                                                if (!empty($stores)):
                                                    $store_img = $stores->store_image;
                                                else:
                                                    if ($ch_acc == 'WeChat'):
                                                        $channels = Channels::find()->where(['parent_name' => 'channel_' . $ch_acc])->one();
                                                    else:
                                                        $channels = Channels::find()->where(['channel_name' => $ch_acc])->one();
                                                    endif;

                                                    $store_img = $channels->parent_name == "channel_WeChat" ? 'img/marketplace_logos/elliot-wechat-connection-icon.png' : $channels->channel_image;
                                                endif;
                                                ?>
                                                <tr>
                                                    <td style="width:37%;" class="captialize"><a href="/people/view?id=<?php echo $latest['customer_ID']; ?>"><?= $latest['first_name'] . ' ' . $latest['last_name']; ?></a></td>
                                                    <td style="width:36%;"><?= "Customer"; ?></td>
                                                    <td><img class="ch_img" src="<?php echo $store_img; ?>" width="50" height="50" alt="Image"></td>

                                                </tr>
                                            <?php endif; ?>

                                        <?php endforeach; ?>
                                    <?php endif; ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--          <div class="row">
                        <div class="col-xs-12 col-md-4">
                          <div class="panel panel-default">
                            <div class="panel-heading panel-heading-divider xs-pb-15">Current Progress</div>
                            <div class="panel-body xs-pt-25">
                              <div class="row user-progress user-progress-small">
                                <div class="col-md-5"><span class="title">Bootstrap Admin</span></div>
                                <div class="col-md-7">
                                  <div class="progress">
                                    <div style="width: 40%" class="progress-bar progress-bar-success"></div>
                                  </div>
                                </div>
                              </div>
                              <div class="row user-progress user-progress-small">
                                <div class="col-md-5"><span class="title">Custom Work</span></div>
                                <div class="col-md-7">
                                  <div class="progress">
                                    <div style="width: 65%" class="progress-bar progress-bar-success"></div>
                                  </div>
                                </div>
                              </div>
                              <div class="row user-progress user-progress-small">
                                <div class="col-md-5"><span class="title">Clients Module</span></div>
                                <div class="col-md-7">
                                  <div class="progress">
                                    <div style="width: 30%" class="progress-bar progress-bar-success"></div>
                                  </div>
                                </div>
                              </div>
                              <div class="row user-progress user-progress-small">
                                <div class="col-md-5"><span class="title">Email Templates</span></div>
                                <div class="col-md-7">
                                  <div class="progress">
                                    <div style="width: 80%" class="progress-bar progress-bar-success"></div>
                                  </div>
                                </div>
                              </div>
                              <div class="row user-progress user-progress-small">
                                <div class="col-md-5"><span class="title">Plans Module</span></div>
                                <div class="col-md-7">
                                  <div class="progress">
                                    <div style="width: 45%" class="progress-bar progress-bar-success"></div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-12 col-md-4">
                          <div class="widget be-loading">
                            <div class="widget-head">
                              <div class="tools"><span class="icon mdi mdi-chevron-down"></span><span class="icon mdi mdi-refresh-sync toggle-loading"></span><span class="icon mdi mdi-close"></span></div>
                              <div class="title">Top Sales</div>
                            </div>
                            <div class="widget-chart-container">
                              <div id="top-sales" style="height: 178px;"></div>
                              <div class="chart-pie-counter">36</div>
                            </div>
                            <div class="chart-legend">
                              <table>
                                <tr>
                                  <td class="chart-legend-color"><span data-color="top-sales-color1"></span></td>
                                  <td>Premium Purchases</td>
                                  <td class="chart-legend-value">125</td>
                                </tr>
                                <tr>
                                  <td class="chart-legend-color"><span data-color="top-sales-color2"></span></td>
                                  <td>Standard Plans</td>
                                  <td class="chart-legend-value">1569</td>
                                </tr>
                                <tr>
                                  <td class="chart-legend-color"><span data-color="top-sales-color3"></span></td>
                                  <td>Services</td>
                                  <td class="chart-legend-value">824</td>
                                </tr>
                              </table>
                            </div>
                            <div class="be-spinner">
                              <svg width="40px" height="40px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                                <circle fill="none" stroke-width="4" stroke-linecap="round" cx="33" cy="33" r="30" class="circle"></circle>
                              </svg>
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-12 col-md-4">
                          <div class="widget widget-calendar">
                            <div id="calendar-widget"></div>
                          </div>
                        </div>
                      </div>-->
            <!--          <div class="row">
                        <div class="col-xs-12 col-md-6">
                          <div class="panel panel-default">
                            <div class="panel-heading">Latest Activity</div>
                            <div class="panel-body">
                              <ul class="user-timeline user-timeline-compact">
                                <li class="latest">
                                  <div class="user-timeline-date">Just Now</div>
                                  <div class="user-timeline-title">Create New Page</div>
                                  <div class="user-timeline-description">Vestibulum lectus nulla, maximus in eros non, tristique.</div>
                                </li>
                                <li>
                                  <div class="user-timeline-date">Today - 15:35</div>
                                  <div class="user-timeline-title">Back Up Theme</div>
                                  <div class="user-timeline-description">Vestibulum lectus nulla, maximus in eros non, tristique.</div>
                                </li>
                                <li>
                                  <div class="user-timeline-date">Yesterday - 10:41</div>
                                  <div class="user-timeline-title">Changes In The Structure</div>
                                  <div class="user-timeline-description">Vestibulum lectus nulla, maximus in eros non, tristique.      </div>
                                </li>
                                <li>
                                  <div class="user-timeline-date">Yesterday - 3:02</div>
                                  <div class="user-timeline-title">Fix the Sidebar</div>
                                  <div class="user-timeline-description">Vestibulum lectus nulla, maximus in eros non, tristique.</div>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                          <div class="widget be-loading">
                            <div class="widget-head">
                              <div class="tools"><span class="icon mdi mdi-chevron-down"></span><span class="icon mdi mdi-refresh-sync toggle-loading"></span><span class="icon mdi mdi-close"></span></div>
                              <div class="title">Conversions</div>
                            </div>
                            <div class="widget-chart-container">
                              <div class="widget-chart-info xs-mb-20">
                                <div class="indicator indicator-positive pull-right"><span class="icon mdi mdi-chevron-up"></span><span class="number">15%</span></div>
                                <div class="counter counter-inline">
                                  <div class="value">156k</div>
                                  <div class="desc">Impressions</div>
                                </div>
                              </div>
                              <div id="map-widget" style="height: 265px;"></div>
                            </div>
                            <div class="be-spinner">
                              <svg width="40px" height="40px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                                <circle fill="none" stroke-width="4" stroke-linecap="round" cx="33" cy="33" r="30" class="circle"></circle>
                              </svg>
                            </div>
                          </div>
                        </div>
                      </div>-->


            <!--
                    </div>
                  </div>-->




        <?php endif; ?>

    <?php } ?> 
</div>