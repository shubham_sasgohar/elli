<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$basepath=Yii::getAlias('@baseurl');
$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="be-wrapper be-login">
      <div class="be-content">
        <div class="main-content container-fluid">
          <div class="splash-container forgot-password">
            <div class="panel panel-default panel-border-color panel-border-color-primary">
              <div class="panel-heading"><img src="<?php echo $basepath?>/img/elliot-logo.svg" alt="logo" width="102" height="27" class="logo-img"><span class="splash-description">Omnichannel Syndication for Global Merchants</span></div>
              <div class="panel-body">
                  <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
                  <p class="f12">Don't worry, we'll send you an email to reset your password.</p>
                  <div class="form-group xs-pt-20">
                    <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>
                  </div>
                  <div class="form-group xs-pt-5">
                     <?= Html::submitButton('Send', ['class' => 'btn btn-block btn-primary btn-xl']) ?>
                  </div>
                <?php ActiveForm::end(); ?>
              </div>
            </div>
            <div class="splash-footer">&copy; 2017 Elliot</div>
          </div>
        </div>
      </div>
    </div>