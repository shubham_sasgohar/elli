<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Shipstation */

$this->title = 'Update Shipstation: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Shipstations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="shipstation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
