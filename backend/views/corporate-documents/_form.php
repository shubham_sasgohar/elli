<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model backend\models\CorporateDocuments */
/* @var $form yii\widgets\ActiveForm */
$base_url = Yii::getAlias('@baseurl');
$this->title = 'Corporate Documents';
$username = Yii::$app->user->identity->username;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-head">
    <h2 class="page-head-title"><?= Html::encode($this->title) ?></h2>
    <ol class="breadcrumb page-head-nav">
        <?php
        echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
        ?>
    </ol>
</div>

<div class="corporate-documents-form row">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'elliot_user_id')->textInput() ?>

    <?= $form->field($model, 'Business_License')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Business_Papers')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Tax_ID')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'channel')->textInput(['maxlength' => true]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default panel-border-color panel-border-color-primary">
            <div class="panel-heading panel-heading-divider"><span class="panel-subtitle">Upload Your Corporate Documents</span></div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal group-border-dashed' ]]); ?>
                <!--<form action="#" style="border-radius: 0px;" class="form-horizontal group-border-dashed">!-->

                    <!--<div class="form-group">-->
                        <!--<label class="col-sm-3 control-label">Business License</label>-->
                        <!--<div class="col-sm-6">-->
                            
                            <?= $form->field($model, 'Business_License')->fileInput(['class' => 'inputfile','name'=>"file-2",'id'=>"file-2","data-multiple-caption"=>"{count} files selected",'multiple'])->label(false) ?>
                            <label for="file-2" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                        <!--</div>-->
                    <!--</div>-->
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Business Incorporation Papers</label>
                        <div class="col-sm-6">
                            <input type="file" name="file-2" id="file-2" data-multiple-caption="{count} files selected" multiple class="inputfile">
                            <label for="file-2" class="btn-primary"> <i class="mdi mdi-upload"></i><span>Browse files...</span></label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tax ID</label>
                        <div class="col-sm-6">
                            <a id="username" href="#" data-type="text" data-title="Enter username" class="editable editable-click" data-original-title="" title="">superuser</a>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Channels</label>
                        <div class="col-sm-2">
                            <select class="form-control">
                                <option value=""></option>
                                <option value="Mercado Libre">Mercado Libre</option>
                                <option value="Jumia">Jumia</option>
                                <option value="WeChat">WeChat</option>
                                <option value="Lazada">Lazada</option>
                                <option value="Tokopedia">Tokopedia</option>
                                <option value="Shopee">Shopee</option>
                                <option value="Xiao Hong">Xiao Hong</option>
                            </select>
                        </div>
                        
                    </div>
                    <button type="submit" class="btn btn-space btn-primary">Submit</button>
                    <div class="form-group">
                        
                    </div>
                <?php ActiveForm::end(); ?>
               <!-- </form>!-->
            </div>
        </div>
    </div>
</div>
