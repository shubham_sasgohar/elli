<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CorporateDocuments */

$this->title = 'Update Corporate Documents: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Corporate Documents', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="corporate-documents-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
