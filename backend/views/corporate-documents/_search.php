<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\CorporateDocumentsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="corporate-documents-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'elliot_user_id') ?>

    <?= $form->field($model, 'Business_License') ?>

    <?= $form->field($model, 'Business_Papers') ?>

    <?= $form->field($model, 'Tax_ID') ?>

    <?php // echo $form->field($model, 'channel') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
