<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model backend\models\CorporateDocuments */

//$this->title = 'Create Corporate Documents';
//$this->params['breadcrumbs'][] = ['label' => 'Corporate Documents', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>

<div class="corporate-documents-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
