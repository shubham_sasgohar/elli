<?php
use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Channels;
use backend\models\Shipstation;
use yii\widgets\Breadcrumbs;
use backend\models\Integrations;
use backend\models\ChannelConnection;
?>
<?php 
$this->title = 'All POS';
$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => 'javascript: void(0)', 'class' => 'non_link'];
$this->params['breadcrumbs'][] = ['label' => 'Integrations', 'url' => 'javascript: void(0)', 'class' => 'non_link'];
$this->params['breadcrumbs'][] = ['label' => 'POS', 'url' => ['/integrations/pos-all']];
$this->params['breadcrumbs'][] = 'View All';
$user_id = Yii::$app->user->identity->id;

$channel_data = Channels::find()->where(['channel_name' => 'Square'])->one();
$square_channel_id = $channel_data->channel_ID;
$square_channel_data = ChannelConnection::find()->where(['elliot_user_id' => $user_id, 'channel_id' => $square_channel_id])->one();

$connection_status = '';
if(empty($square_channel_data)){
    $connection_status = '<a href="square">Get Connected</a>';
}
else{
    $connection_status = '<a href="/channelsetting/?id='.$square_channel_id.'&type=square">Connected</a>';
}

?>

<div class="page-head">
    <h2 class="page-head-title"><?= Html::encode($this->title) ?></h2>
    <ol class="breadcrumb page-head-nav">
        <?php
        echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
        ?>
    </ol>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default panel-table" >
            <div class="panel-heading">
                <div class="tools">
                    <a href="<?php //echo $new_basepath  ?>/export-user"><span class="icon mdi mdi-download"></span></span></a>
                    <span class="icon mdi mdi-more-vert"></span>
                </div>
            </div>
            <div class="panel-body">
                <table id="channels_view_table" class="table table-striped table-hover table-fw-widget">
                    <thead>

                        <tr>
                            <th>POS</th>
                            <!--<th>Products in Fulfillment</th>
                            <th>Orders Pending in Fulfillment</th>!-->
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr class="odd" role="row">
                                <td class="sorting_1">Square</td>
                                <td><?php echo $connection_status; ?></td>   
                            </tr>
                            </tbody>
                </table>
            </div>
        </div>
    </div>
</div>