<?php 
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\db\Query;
use yii\widgets\Pjax;
use yii\grid\GridView;
use backend\models\ChannelConnection;
use backend\models\Channels;
use backend\models\Integrations;

$this->title = 'Add Your Square Channel';
$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => 'javascript: void(0)', 'class' => 'non_link'];
$this->params['breadcrumbs'][] = ['label' => 'Integrations', 'url' => 'javascript: void(0)', 'class' => 'non_link'];
$this->params['breadcrumbs'][] = ['label' => 'POS', 'url' => ['/integrations/pos-all']];
$this->params['breadcrumbs'][] = 'Square';
$user_id = Yii::$app->user->identity->id;

$channel_data = Channels::find()->where(['channel_name' => 'Square'])->one();
$square_channel_id = $channel_data->channel_ID;
$square_integrations = ChannelConnection::find()->where(['user_id' => $user_id, 'channel_id' => $square_channel_id])->one();
?>
<div class="page-head">
    <h2 class="page-head-title">Add Your Square Channel</h2>
    <ol class="breadcrumb page-head-nav">
        <?php
        echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
        ?>
    </ol>
</div>
<div class="row wizard-row">
    <div class="col-md-12 fuelux">
        <div class="block-wizard panel panel-default">
            <div id="wizard-store-connection" class="wizard wizard-ux">
                <ul class="steps">
                    <li data-step="1" class="active">Authorize<span class="chevron"></span></li>
                </ul>
                <div class="step-content">
                    <?php if(empty($square_integrations)): ?>
                    <form id="square-authorize-form" action="#" data-parsley-namespace="data-parsley-" data-parsley-validate="" novalidate="" class="form-horizontal group-border-dashed">
                        <div class="form-group no-padding main-title">
                            <div class="col-sm-12">
                                <label class="control-label">Please provide your Square APP account details for Authorizing the Integration :</label>
                                <p> To integrate Square channel with Elliot, you'll need to create an Application on Square account Please <a target="_blank" href="https://docs.connect.squareup.com/">Follow</a> this link.</p>  
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Application ID</label>
                            <div id="square_application_id" class="col-sm-6">
                                <input type="text" placeholder="Please Enter value" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Personal Access Token</label>
                            <div id="square_personal_access_token" class="col-sm-6">
                                <input type="text" placeholder="Please Enter value" class="form-control">
                            </div>
                        </div>
                        <input id="user_id" type="hidden" value="<?= Yii::$app->user->identity->id; ?>" />
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button class="btn btn-default btn-space"><a href="/">Cancel</a></button>
                                <button data-wizard="#wizard-store-connection" class="btn btn-primary btn-space square-auth-channel">Authorize & Connect</button>
                            </div>
                        </div>
                    </form>
                    <?php else: ?>
                    <div id="bgc_conn_text" class="form-group no-padding main-title">
                        <div class="col-sm-12">
                            <label class="control-label">Square is now connected.</label>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="square_ajax_request" tabindex="-1" role="dialog" class="modal fade in">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close square_modal_close"></span></button>
        </div>
        <div class="modal-body">
          <div class="text-center">
            <div class="text-success"><span class="modal-main-icon mdi mdi-check"></span></div>
            <h3 id="ajax_header_msg">Success!</h3>
            <p id="square_ajax_msg"></p>
            <div class="xs-mt-50">
              <button type="button" data-dismiss="modal" class="btn btn-space btn-default square_modal_close">Close</button>
            </div>
          </div>
        </div>
        <div class="modal-footer"></div>
      </div>
    </div>
</div>
<div id="mod-danger" tabindex="-1" role="dialog" class="modal fade in square_ajax_request_error" style="display: none;">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close square_error_modal_close"></span></button>
          </div>
          <div class="modal-body">
            <div class="text-center">
              <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
              <h3 id='ajax_header_error_msg'></h3>
              <p id="square_ajax_msg_eror"></p>
              <div class="xs-mt-50">
                <button type="button" data-dismiss="modal" class="btn btn-space btn-default square_error_modal_close">Close</button>
              </div>
            </div>
          </div>
          <div class="modal-footer"></div>
        </div>
      </div>
    </div>  