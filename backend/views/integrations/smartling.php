<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\User;
use yii\widgets\Breadcrumbs;
use backend\models\Smartling;
use backend\models\StoresConnection;
use backend\models\StoreDetails;
use backend\models\Notification;
use backend\models\Fulfillment;
use backend\models\FulfillmentList;
use backend\models\ChannelConnection;
use backend\models\Channels;
use backend\models\CronTasks;
use backend\models\Orders;
use backend\models\OrderChannel;
use backend\models\CustomerUser;
use backend\models\CustomerAbbrivation;
use backend\models\Channelsetting;
use backend\models\Products;
use yii\web\Session;

$this->title = 'Translations';
$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => 'javascript: void(0)', 'class' => 'non_link'];
$this->params['breadcrumbs'][] = ['label' => 'Integrations', 'url' => 'javascript: void(0)', 'class' => 'non_link'];
$this->params['breadcrumbs'][] = ['label' => 'Translations', 'url' => ['/integrations/translation-all']];
$this->params['breadcrumbs'][] = 'Smartling';

$user_id = Yii::$app->user->identity->id;
$account_id = $sm_user_id = $secret_key = '';
$smartlingData = Smartling::find()->Where(['elliot_user_id' => $user_id])->one();
if (!empty($smartlingData)) {

    $account_id = $smartlingData->account_id;
    $sm_user_id = $smartlingData->sm_user_id;
    $secret_key = $smartlingData->secret_key;
}
$smartling_details = Smartling::find()->Where(['<>', 'token', ''])->all();
?>
<div class="page-head">
    <h2 class="page-head-title">Smartling</h2>
    <ol class="breadcrumb page-head-nav">
<?php
echo Breadcrumbs::widget([
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]);
?>
    </ol>
</div>

<!--Swtich Component-->
<div class="row wizard-row proidget">
    <div class="col-md-12 fuelux">
        <div class="main-content container-fluid">
            <!--Tabs-->
            <div class="row">
                <!--Default Tabs-->
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">General Information </div>
                        <div class="tab-container">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab1" data-toggle="tab">Account Specific Details</a></li>
                                <li><a href="#tab2" data-toggle="tab">Jobs History</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="tab1" class="tab-pane active cont">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <label class="col-sm-3 control-label panel-heading">Account ID</label>
                                            <input type="text" class="form-control" style="width:60%"  disabled="disabled" value="<?= $account_id; ?>" />
                                        </div>
                                        <div class="col-md-12">
                                            <label class="col-sm-3 control-label panel-heading">Keys</label>
                                            <input type="text" class="form-control" style="width:60%"  disabled="disabled" value="<?= $sm_user_id; ?>" />
                                        </div>
                                        <div class="col-md-12">
                                            <label class="col-sm-3 control-label panel-heading">Secret Key</label>
                                            <input type="text" class="form-control" style="width:60%"  disabled="disabled" value="<?= $secret_key; ?>" />
                                        </div>
                                    </div>
                                </div>
                                <div id="tab2" class="tab-pane cont">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="panel panel-default panel-table">
                                                <div class="panel-heading">Default
                                                    <div class="tools"><span class="icon mdi mdi-download"></span><span class="icon mdi mdi-more-vert"></span></div>
                                                </div>
                                                <div class="panel-body">
                                                    <table id="table1" class="table table-striped table-hover table-fw-widget">
                                                        <thead>
                                                            <tr>
                                                                <th>Job ID</th>
                                                                <th>Selected Channel</th>
                                                                <th>Selected Language</th>
                                                                <th>Status</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
							<?php if (!empty($smartling_details)) { ?>
								<?php
								foreach ($smartling_details as $Smartling_value) {
								    $status='';
                                                                    $label='';
								    $token = $Smartling_value->token;
								    $Channelsetting_connections = Channelsetting::find()->Where(['channel_name' => $token, 'setting_key' => 'Enable_Smartling'])->one();
								    if ($Smartling_value->job_translationJobUid == 'Service Account') {
									$channel = 'Wechat';
								    } else {
									$channel = $Smartling_value->job_translationJobUid;
								    }
								    if ($Channelsetting_connections->setting_value == 'yes') {
									$label= 'label-primary';
									$status='In Progress';
								    } 
								    ?>
	                                                            <tr class="odd gradeX">
	                                                                <td><?= $channel ?></td>
	                                                                <td><?= $Smartling_value->token; ?></td>
	                                                                <td class="center"><?= $Smartling_value->target_locale ?></td>
	                                                                <td class="center"><span class="label  <?= $label; ?>"><?= $status; ?></td>
	                                                            </tr>
							<?php } } ?>
						    </table>
						</div>
					    </div>
					</div>
				    </div>
				</div>
			    </div>
			</div>
		    </div>
		</div>
	    </div>
	</div>
    </div>
</div>







