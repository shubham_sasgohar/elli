<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\CronTasks */

$this->title = 'Create Cron Tasks';
$this->params['breadcrumbs'][] = ['label' => 'Cron Tasks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cron-tasks-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
