<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\VariationsSet */

$this->title = $model->variations_set_id;
$this->params['breadcrumbs'][] = ['label' => 'Variations Sets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="variations-set-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->variations_set_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->variations_set_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'variations_set_id',
            'channel_abb_id',
            'elliot_user_id',
            'variations_set_name',
            'variations_set_options_ids',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
