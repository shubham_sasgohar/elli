<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\VariationsSetSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="variations-set-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'variations_set_id') ?>

    <?= $form->field($model, 'channel_abb_id') ?>

    <?= $form->field($model, 'elliot_user_id') ?>

    <?= $form->field($model, 'variations_set_name') ?>

    <?= $form->field($model, 'variations_set_options_ids') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
