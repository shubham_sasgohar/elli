<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ProductsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'product_id') ?>

    <?= $form->field($model, 'product_name') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'SKU') ?>

    <?= $form->field($model, 'EAN') ?>

    <?php // echo $form->field($model, 'JAN') ?>

    <?php // echo $form->field($model, 'ISBN') ?>

    <?php // echo $form->field($model, 'MPN') ?>

    <?php // echo $form->field($model, 'weather') ?>

    <?php // echo $form->field($model, 'occasion') ?>

    <?php // echo $form->field($model, 'age_group') ?>

    <?php // echo $form->field($model, 'weight') ?>

    <?php // echo $form->field($model, 'available_quantity') ?>

    <?php // echo $form->field($model, 'brand') ?>

    <?php // echo $form->field($model, 'gender') ?>

    <?php // echo $form->field($model, 'category') ?>

    <?php // echo $form->field($model, 'price') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
