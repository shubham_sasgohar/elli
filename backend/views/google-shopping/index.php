<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use backend\models\Channels;
use backend\models\User;
use yii\db\Query;
use backend\models\ChannelConnection;

$this->title = 'Google Shopping';
$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => "javascript: void(0)", 'class' => 'non_link'];
$this->params['breadcrumbs'][] = ['label' => 'Channels', 'url' => "javascript: void(0)", 'class' => 'non_link'];
$this->params['breadcrumbs'][] = ['label' => 'View All', 'url' => "/channels/create"];
$this->params['breadcrumbs'][] = 'Google Shopping';

$id = Yii::$app->user->identity->id;
$channel = Channels::find()->Where(['channel_name' => 'Google shopping'])->one();
$channel_id = $channel['channel_ID'];
$channel_connection = ChannelConnection::find()->Where(['user_id' => $id, 'channel_id' => $channel_id])->one();


/*For Checking Google shopping Is enabled or Not*/
$user_id=Yii::$app->user->identity->id;
$domain_name=Yii::$app->user->identity->domain_name;
/*Connectivity For Main database*/


$user_data=User::find()->where(['id'=>$user_id,'google_feed' => 'yes'])->one();
if(!empty($user_data)):
    $google_enable='yes';
else:
    $google_enable='no';
endif;

/*Connectivity For subdomain database*/


/*End For Checking Google shopping Is enabled or Not*/

?>
<div class="page-head">
    <h2 class="page-head-title">Google Shopping</h2>
    <ol class="breadcrumb page-head-nav">
<?php
echo Breadcrumbs::widget([
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]);
?>
    </ol>
</div>

<div class="main-content container-fluid">
    <div class="row wizard-row">
        <div class="col-md-12 fuelux Google12">
            <div class="block-wizard panel panel-default">
                <div id="wizard1" class="wizard wizard-ux">
                    <ul class="steps">
                        <li data-step="1" class="data1 active " >Google Connection<span class="chevron"></span></li>
                                     <!--<li data-step="2" class="data2 <?php if (!empty($channel_connection)) { ?>active <?php } ?>">Connect<span class="chevron"></span></li>-->

                    </ul>
                    <!--div class="actions">
                      <button type="button" class="btn btn-xs btn-prev btn-default" disabled="disabled"><i class="icon mdi mdi-chevron-left"></i>Prev</button>
                      <button type="button" data-last="Finish" class="btn btn-xs btn-next btn-default" disabled="disabled">Next<i class="icon mdi mdi-chevron-right"></i></button>
                    </div-->
                    <div class="step-content">
                        <!--	<div data-step="2" class="step-pane active div23">
                                 <p class="afterwechatrequestmsg_already" style="display:none;">Thank you for your submission for an Official Google Store. Your request is currently being proceed and an Elliot team member will reach out within 24 hours to confirm your store creation.</p>
                            <?php if (!empty($channel_connection)) { ?>
                                <p class="afterwechatrequestmsg" >Thank you for your submission for an Official Google Store. Your request is currently being proceed and an Elliot team member will reach out within 24 hours to confirm your store creation..</p>
                            <?php
                            $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

                            $actual_link1 = $actual_link . 'google/';
                            $actual_link2 = $actual_link . 'oauth2callback/';
                            $actual_link3 = $actual_link . 'inventoryupdate/';
                            ?>
                                                    <p><h4>Instructions: </h4>
                                                    1. Open url : <a href="https://console.developers.google.com/apis/credentials">https://console.developers.google.com/apis/credentials</a>.</br>
                                                    2. Edit the web OAuth 2.0 client ID which you want to use for google shopping. </br>
                                                    3. Under Authorized redirect URIs please enter redirect url's
                                                    <p> <?php echo $actual_link2; ?>
                                                    <p> <?php echo $actual_link3; ?></br>
                                                    4. Click on Library tab and search "Drive API" api and enable it.</br>
                                                    5. Click on Library tab and search "Content API for Shopping" api and enable it.
                                                    </p>
                                                    </p> </p>
                            <?php } ?> 
                                  
                                   <div id="bgc_text2" class="form-group no-padding main-title" style="display: none">
                                    <div class="col-sm-12">
                                        <label class="control-label">Your Google shopping store is connecting, you will be notified via email when complete.</label>
                                    </div>
                                </div>
<?php $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>
                                <form id="bigcommerce-connect-form" action="<?php echo $actual_link . 'oauth2callback/'; ?>" data-parsley-namespace="data-parsley-" data-parsley-validate="" novalidate="" class="form-horizontal group-border-dashed">
                                   
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                           
                                            <button data-wizard="#wizard-store-connection" type="submit" class="btn btn-primary btn-space ">Connect</button>
                                        </div>
                                    </div>
                                    <input id="user_id" type="hidden" value="<?= Yii::$app->user->identity->id; ?>" />
                                </form>
                                  
                                  
                                  
                                  
                                  
                                  
                                  </div>
                      <div data-step="1" class="step-pane active div21">
                                  <p class="afterwechatrequestmsg_already" style="display:none;">Thank you for your submission for an Official Google Store. Your request is currently being proceed and an Elliot team member will reach out within 24 hours to confirm your store creation.</p>
<?php if (!empty($channel_connection)) { ?>
                                <p class="afterwechatrequestmsg" >Thank you for your submission for an Official Google Store. Your request is currently being proceed and an Elliot team member will reach out within 24 hours to confirm your store creation..</p>
                            <?php
                            $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

                            $actual_link1 = $actual_link . 'google/';
                            $actual_link2 = $actual_link . 'oauth2callback/';
                            $actual_link3 = $actual_link . 'inventoryupdate/';
                            ?>
                                                    <p><h4>Instructions: </h4>
                                                    1. Open url : <a href="https://console.developers.google.com/apis/credentials">https://console.developers.google.com/apis/credentials</a>.</br>
                                                    2. Edit the web OAuth 2.0 client ID which you want to use for google shopping. </br>
                                                    3. Under Authorized redirect URIs please enter redirect url's
                                                    <p> <?php echo $actual_link2; ?>
                                                    <p> <?php echo $actual_link3; ?></br>
                                                    4. Click on Library tab and search "Drive API" api and enable it.</br>
                                                    5. Click on Library tab and search "Content API for Shopping" api and enable it.
                                                    </p>
                                                    </p> </p>
<?php } else { ?>
                          <form action="#" data-parsley-namespace="data-parsley-" data-parsley-validate="" novalidate="" class="form-horizontal group-border-dashed Googlerequestform">
                            <div class="form-group no-padding">
                              <div class="col-sm-12">
                                <label class="control-label">Please provide Google Store Client ID, Client Secret Key and Google Shopping Merchant ID for authorizing this connection:</label>
    <?php
    $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

    $actual_link1 = $actual_link . 'google/';
    $actual_link2 = $actual_link . 'oauth2callback/';
    $actual_link3 = $actual_link . 'inventoryupdate/';
    ?>
                                                    <p><h4>Instructions: </h4>
                                                    1. Open url : <a href="https://console.developers.google.com/apis/credentials">https://console.developers.google.com/apis/credentials</a>.</br>
                                                    2. Edit the web OAuth 2.0 client ID which you want to use for google shopping. </br>
                                                    3. Under Authorized redirect URIs please enter redirect url's
                                                    <p> <?php echo $actual_link2; ?>
                                                    <p> <?php echo $actual_link3; ?></br>
                                                    4. Click on Library tab and search "Drive API" api and enable it.</br>
                                                    5. Click on Library tab and search "Content API for Shopping" api and enable it.
                                                    </p>
                                                    </p> </p>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-3 control-label"><?php echo isset($_GET['already']) && $_GET['already'] == 'yes' ? 'Client ID' : 'Client ID'; ?></label>
                              <div class="col-sm-6">
                                <input type="text" placeholder="Client ID" class="form-control" id="Google_clientid" required>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-3 control-label"><?php echo isset($_GET['already']) && $_GET['already'] == 'yes' ? 'Client Secret Key' : 'Client Secret Key'; ?></label>
                              <div class="col-sm-6">
                                <input type="text" placeholder="Client Secret Key" class="form-control" id="Google_secretkey" required>
                                
                              </div>
                            </div>
                                            <div class="form-group">
                              <label class="col-sm-3 control-label"><?php echo isset($_GET['already']) && $_GET['already'] == 'yes' ? 'Merchant ID' : 'Merchant ID'; ?></label>
                              <div class="col-sm-6">
                                <input type="text" placeholder="Merchant ID" class="form-control" id="Google_merchantid" required>
                               
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-sm-offset-2 col-sm-10">
    <?php if (isset($_GET['already']) && $_GET['already'] == 'yes') { ?>
                                      <input type="buttom" class="btn btn-primary btn-space Google_sbmt1_already" value="Submit">
                            <?php } else { ?>
                                    <input type="buttom" class="btn btn-primary btn-space Google_sbmt1" value="Submit">
                                  <!--  <a href="?already=yes<?php echo isset($_GET['type']) ? '&type=' . $_GET['type'] : ''; ?>" class="btn btn-primary btn-space Google_sbmt_already"></a> -->
                                <!--button data-wizard="#wizard1" class="btn btn-primary btn-space wizard-next Google_sbmt">Next Step</button-->
    <?php } ?>
                            <!--   </div>
                             </div>
                           </form>
<?php } ?>
                     </div>
                        -->
						
						<?php if($google_enable == 'yes') { 
							$id = Yii::$app->user->identity->domain_name;
						?> 
								<div class="panel-body code2">
                                                        <h4>Instructions: </h4>
                                                    <ol>
                                                        <li>Open url : <a href="https://merchants.google.com/">https://merchants.google.com/</a>.</li>
                                                        <li>SignIn/SignUp and enter the merchant account.</li>
                                                        <li>Click on product tab and create a feed after click on "+" button.</li>
                                                        <li>Choose a scheduled way to enter the xml-feed name as given below.</li>
                                                    </ol>
                                                    <div class=" col-sm-4 control-label panel-heading custom_Transs">Product Feed URL</div>
								<!--label class="col-sm-6 control-label panel-heading custom_Transs">Product Feed URL :</label-->
								<input type="text" class="form-control" style="width:42%"  disabled="disabled" readonly="readonly" value="<?php echo Yii::$app->params['BASE_URL'].'googlefeeds/'.$id.'.xml' ?>"/>
								</div>
								
								
						<?php } ?>
						
						<div class="panel-body code" style="display:none;">
                                                    <h4>Instructions: </h4>
                                                    <ol>
                                                        <li>Open url : <a href="https://merchants.google.com/">https://merchants.google.com/</a>.</li>
                                                        <li>SignIn/SignUp and enter the merchant account. </li>
                                                        <li>Click on product tab and create a feed after click on "+" button.<li>
                                                        <li>Choose a scheduled way to enter the xml-feed name as given below.</li>
                                                    </ol>
                                                <div class=" col-sm-4 control-label panel-heading custom_Transs">Product Feed URL</div>   
						<!--label class="col-sm-3 control-label panel-heading">Product Feed URL :</label-->
								<input type="text" disabled="disabled" style="width:42%" readonly="readonly" class="form-control" value="<?php $id = Yii::$app->user->identity->domain_name; echo Yii::$app->params['BASE_URL'].'googlefeeds/'.$id.'.xml' ?>" />
								</div>
						
                        <div class="panel-body">
                            <form action="#" style="border-radius: 0px;" class="group-border-dashed">
                                <div class="form-group">
                                    <div class="col-sm-4 control-label panel-heading custom_Transs">Enable Google Merchant</div>
                                    <!--label class="col-sm-6 control-label panel-heading custom_Transs">Enable Google Merchant</label-->
                                    <div class="col-sm-6 xs-pt-5">
                                        <div class="switch-button switch-button-success TransLable">
                                            <input type="checkbox" <?php if($google_enable == 'yes') { echo 'checked=""';} ?>  name="googlefeedcheck" id="googlefeedcheck"><span>
                                                <label for="googlefeedcheck"></label></span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div id="walkthechat_request" tabindex="-1" role="dialog" style="display: none;" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                            </div>
                            <div class="modal-body">
                                <div class="text-center">
                                    <div class="text-success"><span class="modal-main-icon mdi mdi-check"></span></div>
                                    <h3>Success!</h3>
                                    <p>Your Google application is being processed and you will receive an email containing documentation necessary to sell across borders. Additionally, an Elliot representative will be in touch 
                                        within 24 hours to complete your on boarding. Please have your documentation ready to expedite this process.</p>
                                    <div class="xs-mt-50">
                                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Close</button>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer"></div>
                        </div>
                    </div>
                </div>

                <div id="walkthechat_request_error" tabindex="-1" role="dialog" class="modal fade" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                            </div>
                            <div class="modal-body">
                                <div class="text-center">
                                    <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
                                    <h3>Error!</h3>
                                    <p>Please provide valid login details</p>
                                    <div class="xs-mt-50">
                                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Close</button>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer"></div>
                        </div>
                    </div>
                </div>
				
				
				  <div tabindex="-1" role="dialog" class="modal fade in googlemod-success" style="display: none; background: rgba(0,0,0,0.6);">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close google_modal_close"></span></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="text-center">
                                                <div class="text-success"><span class="modal-main-icon mdi mdi-check"></span></div>
                                                <h3 id="ajax_header_msg">Success!</h3>
                                                <p id="magento_ajax_msg">Your Google Shopping Merchant Feed account is now enabled and available for use.</p>
                                                <div class="xs-mt-50">
                                                    <button type="button" data-dismiss="modal" class="btn btn-space btn-default google_modal_close">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer"></div>
                                    </div>
                                </div>
                            </div>
				<div id="mod-danger" tabindex="-1" role="dialog" class="modal fade in googlemod-error" style="display: none; background: rgba(0,0,0,0.6);">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close google_error_modal_close"></span></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="text-center">
                                                <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
                                                <h3 id='ajax_header_error_msg'>Success!</h3>
                                                <p id="magento_ajax_msg_eror">You have disconnected your Google Shopping Merchant Feed. Your file will still be available; however, it will not be updated. To re-enable, simply switch Google Shopping back on.</p>
                                                <div class="xs-mt-50">
                                                    <button type="button" data-dismiss="modal" class="btn btn-space btn-default google_error_modal_close">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer"></div>
                                    </div>
                                </div>
                            </div>
				<!--
<?php
//$value = $_SESSION["googlecheck"];
//if (!empty($value)) {
//    if ($value == 'enter') {
//        
//    } else {
//        if ($value == 'Okay') {
            ?>
                            <div tabindex="-1" role="dialog" class="modal fade in googlemod-success" style="display: block; background: rgba(0,0,0,0.6);">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close google_modal_close"></span></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="text-center">
                                                <div class="text-success"><span class="modal-main-icon mdi mdi-check"></span></div>
                                                <h3 id="ajax_header_msg">Success!</h3>
                                                <p id="magento_ajax_msg">Your google shop is connected and syncronization.</p>
                                                <div class="xs-mt-50">
                                                    <button type="button" data-dismiss="modal" class="btn btn-space btn-default google_modal_close">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer"></div>
                                    </div>
                                </div>
                            </div>
        <?php // } else { ?>

                            <div id="mod-danger" tabindex="-1" role="dialog" class="modal fade in googlemod-success" style="display: block; background: rgba(0,0,0,0.6);">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close google_error_modal_close"></span></button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="text-center">
                                                <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
                                                <h3 id='ajax_header_error_msg'>Error!</h3>
                                                <p id="magento_ajax_msg_eror">Your API credentials are not working.</p>
                                                <div class="xs-mt-50">
                                                    <button type="button" data-dismiss="modal" class="btn btn-space btn-default google_error_modal_close">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer"></div>
                                    </div>
                                </div>
                            </div>

        <?php  //}
   // }
//} ?>			
-->



            </div>

        </div>
    </div>
</div>
