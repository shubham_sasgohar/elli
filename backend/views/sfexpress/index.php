<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\Breadcrumbs;
use backend\models\StoresConnection;
use backend\models\Stores;
use backend\models\Fulfillment;
use backend\models\SfexpressRate;

$this->title = 'Your SF Express fulfillment';
$this->params['breadcrumbs'][] = 'SF Express';
$checkConnection = '';
$Fulfillment_check = Fulfillment::find()->where(['name' => 'SF Express'])->one();
//echo  '<pre>'; print_r($Fulfillment_check); echo '</pre>';
?>
<div class="page-head">
    <h2 class="page-head-title"><?= Html::encode($this->title) ?></h2>
    <ol class="breadcrumb page-head-nav">
        <?php
        echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
        ?>
    </ol>
</div>
<div class="row wizard-row">
    <div class="col-md-12 fuelux">
        <div class="block-wizard panel panel-default">
            <div id="wizard-store-connection" class="wizard wizard-ux">
                <ul class="steps">
				<?php if(!empty($Fulfillment_check)){ ?> 
				<li data-step="1" class="active">Connection<span class="chevron"></span></li>
				<?php } else { ?>
                    <li data-step="1" class="active">Connection<span class="chevron"></span></li>
				<?php } ?>
                </ul>
                <div class="step-content">
                    <div data-step="1" class="step-pane active">
                        
                           <!-- <div id="bgc_text1" class="form-group no-padding main-title" style="display: none">
                                <div class="col-sm-12">
                                    <label class="control-label">Your SfExpress Fullfillment is connecting, you will be notified via email when complete.</label>
                                </div>
                            </div>-->
							<?php if(empty($Fulfillment_check)): ?>
							  <div class="panel-body">
                            <form action="#" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label panel-heading">Enable SF Express</label>
                                    <div class="col-sm-6 xs-pt-5">
                                        <div class="switch-button switch-button-success">
                                            <input type="checkbox"   name="sfexpresscheck" id="sfexpresscheck"><span>
                                                <label for="sfexpresscheck"></label></span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
							<!--<a href="sfexpress/default"><button type="button" data-dismiss="modal" class="btn btn-space btn-default SfExpress_modal_close">Use Default Elliot Credentials</button></a>
                            <form novalidate=""  id="SfExpress-authorize-form" action="#" data-parsley-namespace="data-parsley-" data-parsley-validate="" novalidate="" class="form-horizontal group-border-dashed">
                                <div class="form-group no-padding main-title">
                                    <div class="col-sm-12">
                                        <label class="control-label">Please provide your SF Express  details for Authorizing the Integration:</label>
                                        <p>To integrate SF Express Fullfillment with Elliot, </p>                             
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">SF Express UserName:</label>
                                    <div id="SfExpress_email" class="col-sm-6">
                                        <input type="text" id="SfExpress_channel_email" placeholder="Please Enter value" class="form-control mercado_validate">
                                      
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">SF Express Password:</label>
                                    <div id="SfExpress_password" class="col-sm-6">
                                        <input type="text" id="SfExpress_channel_password" placeholder="Please Enter value" class="form-control mercado_validate">
                                        
                                    </div>
                                </div>
                                <div class="col-sm-offset-2 col-sm-10">
								 <input type="buttom" class="btn btn-primary btn-space sfexpress_sbmt1_already" value="Submit">
								</div>
								
                            </form>-->
							<?php else : ?>
						 <div class="panel-body">
                            <form action="#" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label panel-heading">Disable SF Express</label>
                                    <div class="col-sm-6 xs-pt-5">
                                        <div class="switch-button switch-button-success">
                                            <input type="checkbox" checked=""  name="sfexpresscheck" id="sfexpresscheck"><span>
                                                <label for="sfexpresscheck"></label></span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
						
						

						<!--<a href="sfexpress/price"><button type="button" data-dismiss="modal" class="btn btn-space btn-default SfExpress_modal_close">Price Table</button></a>-->
							<?php endif; ?>
                        </div>
                   
                    
                </div>
            </div>
        </div>
    </div>
</div>
<div id="SfExpress_ajax_request" tabindex="-1" role="dialog" class="modal fade in">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close SfExpress_modal_close"></span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="text-success"><span class="modal-main-icon mdi mdi-check"></span></div>
                    <h3 id="ajax_header_msg">Success!</h3>
                    <p id="SfExpress_ajax_msg"></p>
                    <div class="xs-mt-50">
                        <a href="sfexpress">Next</a>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<div id="mod-danger" tabindex="-1" role="dialog" class="modal fade in SfExpress_ajax_request_error" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close SfExpress_error_modal_close"></span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
                    <h3 id='ajax_header_error_msg'></h3>
                    <p id="SfExpress_ajax_msg_eror"></p>
                    <div class="xs-mt-50">
                      <button type="button" data-dismiss="modal" class="btn btn-space btn-default SfExpress_error_modal_close">Close</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
