<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ProductVariation */

$this->title = 'Update Product Variation: ' . $model->product_variation_id;
$this->params['breadcrumbs'][] = ['label' => 'Product Variations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->product_variation_id, 'url' => ['view', 'id' => $model->product_variation_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-variation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
