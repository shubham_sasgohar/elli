<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use backend\models\Channels;
use yii\db\Query;
use backend\models\ChannelConnection;

$this->title = 'Add Your Flipkart Store';
$this->params['breadcrumbs'][] = 'Add Your Flipkart Store';

$id = Yii::$app->user->identity->id;
$channel = ChannelConnection::find()->Where(['user_id' => $id])->one();
$users_Id = Yii::$app->user->identity->id;
                                $flipkart = Channels::find()->where(['channel_name' => 'Flipkart'])->one();
                                $channel_id = $flipkart->channel_ID;
                                $channel_connection = ChannelConnection::find()->where(['user_id' => $users_Id, 'channel_id' => $channel_id])->one();
?>
<div class="page-head">
    <h2 class="page-head-title">Add Your Flipkart Store</h2>
    <ol class="breadcrumb page-head-nav">
        <?php
        echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
        ?>
    </ol>
</div>
<div class="main-content container-fluid">
    <div class="row wizard-row">
            <div class="panel panel-default">
                <div class="panel-heading">Flipkart</div>
                <div class="tab-container">
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="#home" data-toggle="tab">Products</a></li>
                    <li><a href="#profile" data-toggle="tab">Orders</a></li>
                  </ul>
                  <div class="tab-content">
                    <div id="home" class="tab-pane active cont">

                              1) <b>Firstly Download xls file of Products from Your Flipkart Store. </b><br><br>
                            2) <b>Then Upload that xls file Here.</b><br><br>
                            3) <b>Doing so will import Your Flipkart Products to Elliot.</b>
                            <!--<p class="afterwechatrequestmsg">-->
                            <?php if (Yii::$app->user->identity->role != 'superadmin'){ ?>
                            <form role="form" action="/flipkart/" class="form-group group-border-dashed" enctype="multipart/form-data" method="post">
                                <div class="form-group no-padding">
                                    <div class="col-sm-10 text-center">
                                        <label class="control-label pull-left">Upload Flipkart Products  xls</label>
                                        <br>
                                        <input type="file" name="flipkart_csv" style="margin-top:20px;" > 
                                    </div>
                                </div>
                                <input type="submit" class="btn btn-primary btn-space" style="margin-top:30px;" >
                            </form>
                            <?php } ?>
                       
                    </div>
                    <div id="profile" class="tab-pane cont">
                         1) <b>Firstly Download CSV file of Orders from Your Flipkart Store. </b><br><br>
                            2) <b>Then Upload that CSV file Here.</b><br><br>
                            3) <b>Doing so will import Your Flipkart Orders to Elliot.</b>
                            <!--<p class="afterwechatrequestmsg">-->
                            <?php if (Yii::$app->user->identity->role != 'superadmin'){ ?>
                            <form role="form" action="/flipkart/orders/" class="form-group group-border-dashed" enctype="multipart/form-data" method="post">
                                <div class="form-group no-padding">
                                    <div class="col-sm-10 text-center">
                                        <label class="control-label pull-left">Upload Flipkart Orders  CSV</label>
                                        <br>
                                        <input type="file" name="flipkart_orders_csv" style="margin-top:20px;" > 
                                    </div>
                                </div>
                                <input type="submit" class="btn btn-primary btn-space" style="margin-top:30px;" >
                            </form>
                            <?php } ?>
                    </div>
                  </div>
                </div>
              </div>
    </div>
</div>
