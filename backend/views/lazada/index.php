<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Channels;
use yii\widgets\Breadcrumbs;
use backend\models\ChannelConnection;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ChannelsSerach */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Channels';
$this->params['breadcrumbs'][] = $this->title;

$basedir = Yii::getAlias('@basedir');
//include stripe init file//
require $basedir . '/stripe/init.php';
\Stripe\Stripe::setApiKey("sk_test_2GPaBt5mT2k0yyTA9ho3CPIQ");
//get subscription plan through stripe//
$collection_plans = \Stripe\Plan::all(array("limit" => 100));

$stripe_plans = $collection_plans->__toArray(true);
$stripe_plan_data = $stripe_plans['data'];
//echo "<pre>";
//print_r($stripe_plan_data);
//echo "</pre>"; die;


foreach ($stripe_plan_data as $data) {
    $db_stripe_channel_data = Channels::find()->Where(['channel_name' => $data['name']])->one();
    if (empty($db_stripe_channel_data)) {
        $meta = @$data['metadata']['image_url'];
        $id = $data['id'];
        $prefix_channel = substr($id, 0, 7);
        if ($prefix_channel == 'channel') {
            $stripe_Channel = new Channels();
            $stripe_Channel->stripe_Channel_id = stripslashes($data['id']);
            $stripe_Channel->amount = stripslashes($data['amount'] / 100);
            $stripe_Channel->currency = stripslashes($data['currency']);
            $stripe_Channel->channel_image = stripslashes(@$data['metadata']['image_url']);
            $stripe_Channel->channel_name = stripslashes($data['name']);
            $stripe_Channel->parent_name = stripslashes(@$data['metadata']['parent_name']);
            $stripe_Channel->trial_period_days = stripslashes($data['trial_period_days']);
            $stripe_Channel->save(false);
        }
    } else {
        $meta = @$data['metadata']['image_url'];
        $id = $data['id'];
        $prefix_channel = substr($id, 0, 7);
        if ($prefix_channel == 'channel') {
            $stripe_Channel = Channels::find()->Where(['channel_name' => $data['name']])->one();
            $stripe_Channel->stripe_Channel_id = stripslashes($data['id']);
            $stripe_Channel->amount = stripslashes($data['amount'] / 100);
            $stripe_Channel->currency = stripslashes($data['currency']);
            $stripe_Channel->channel_image = stripslashes(@$data['metadata']['image_url']);
            $stripe_Channel->channel_name = stripslashes($data['name']);
            $stripe_Channel->parent_name = stripslashes(@$data['metadata']['parent_name']);
            $stripe_Channel->trial_period_days = stripslashes($data['trial_period_days']);
            $stripe_Channel->save(false);
        }
    }
}


/* Get Channels data */
//$channels_data = Channels::find()->all();
//echo "<pre>";
//print_r($channels_data);
//echo "</pre>";

$connection = \Yii::$app->db;
//$model1 = $connection->createCommand('SELECT DISTINCT parent_name stripe_Channel_id,channel_image,parent_name FROM channels');
$model1 = $connection->createCommand('SELECT COUNT(parent_name), parent_name FROM channels GROUP BY parent_name');
$channels = $model1->queryAll();

$integrated_channels = array('channel_Facebook', 'channel_Flipkart', 'channel_google-shopping', 'channel_Lazada', 'channel_Square', 'channel_WeChat');
?>

<div class="page-head">
    <h2 class="page-head-title"><?= Html::encode($this->title) ?></h2>
    <ol class="breadcrumb page-head-nav">
        <?php
        echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
        ?>
    </ol>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default panel-table" >
            <div class="panel-heading">
                <div class="tools">
                    <a href="<?php //echo $new_basepath                 ?>/export-user"><span class="icon mdi mdi-download"></span></span></a>
                    <span class="icon mdi mdi-more-vert"></span>
                </div>
            </div>
            <div class="panel-body">
                <table id="channels_view_table" class="table table-striped table-hover table-fw-widget">
                    <thead>

                        <tr>
                            <th>Channel Name</th>
                            <th>Products Listed in Channel</th>
                            <th>Channel Revenue</th>
                            <th>Channel Sales</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($_GET['a']) and ! empty($_GET['a'])) {
                            echo"<pre>";
                            print_R($channels);
//                            die;
                        }
                        $parent_name_array = [];
                        foreach ($channels as $single) {

                            $parent_name = $single['parent_name'];
                            $channels_data = Channels::find()->where(['parent_name' => $parent_name])->with(['channelconnections' => function($query) {
                                            $query->andWhere(['user_id' => Yii::$app->user->identity->id]);
                                        }])->asArray()->all();
                            $channel_revenue = $channels_data[0]['channel_revenue'];
                            $channel_sales = $channels_data[0]['channel_sales'];
                            $status = $channels_data[0]['status'];
                            $stripe_Channel_id = $channels_data[0]['stripe_Channel_id'];
                            if ($parent_name == 'channel_Square') {
                                continue;
                            }
                            $current_channel_seller_center_count = $single['COUNT(parent_name)'];
                            $channel_connections = [];
                            if ($current_channel_seller_center_count == 1) {
                                $channel_connections = $channels_data[0]['channelconnections'];
                            } else {
                                $multiseller_center_connected_channel_count = 0;
                                if (isset($channels_data) and ! empty($channels_data)) {
                                    foreach ($channels_data as $single_multiseller_channel) {
                                        if (isset($single_multiseller_channel) and ! empty($single_multiseller_channel) and ! empty($single_multiseller_channel['channelconnections'])) {
//                                        $channel_connections[] = $single_multiseller_channel['channelconnections'];
                                            $multiseller_center_connected_channel_count++;
                                        }
                                    }
                                }
                            }


//                            
                            ?>
                            <tr class="odd gradeX">
                                <?php //if(in_array($parent_name, $integrated_channels) == true) {
                                ?>
                                <td><?php
                                    $c_name = explode("_", $parent_name);
                                    echo str_replace("-", " ", $c_name[1]) == 'eBay' ? str_replace("-", " ", $c_name[1]) : ucwords(str_replace("-", " ", $c_name[1]));
                                    ?></td>
                                <td><?php //echo $strip_channelID;            ?></td>
                                <td><?php echo $channel_revenue; ?></td>
                                <td><?php echo $channel_sales; ?></td>
                                <?php if ($status == 'Get Connected') { ?>
                                    <td><?php
                                        if ($current_channel_seller_center_count <= 1) {
                                            if ($parent_name == 'channel_WeChat' and ! empty($channel_connections) and ! empty($channel_connections[0]) && $channel_connections[0]['wechat_admin_approve'] == 'yes') {
                                                $channel_id = $channel_connections[0]['channel_id'];

                                                echo "<a href='/channelsetting/?id=" . $channel_id . "'>Connected</a>";
                                            } elseif (in_array($parent_name, ['channel_google-shopping', 'channel_Facebook', 'channel_Flipkart', 'channel_TMall'])) {
                                                switch ($parent_name) {
                                                    case 'channel_google-shopping':
                                                        $channel_link = '/google-shopping/';
                                                        break;
                                                    case 'channel_Facebook':
                                                        $channel_link = '/integrations/facebook';
                                                        break;
                                                    case 'channel_Flipkart':
                                                        $channel_link = '/flipkart/';
                                                        break;
                                                    case 'channel_TMall':
                                                        $channel_link = '/tmall/';
                                                        break;
                                                    default:
                                                        break;
                                                }
                                                $status_text = 'Get Connected';
                                                if (isset($channel_connections[0]) and ! empty($channel_connections[0])) {
                                                    $status_text = 'Connected';
                                                }
                                                ?>

                                                <a  href="<?php echo $channel_link; ?>"  ><?php echo $status_text ?></a>
                                                <?php
                                            } else {
                                                ?><a href="javascript:" data-toggle="modal" class=""  data-target="#<?php echo $stripe_Channel_id; ?> "><?php echo $parent_name == "channel_Instagram" ? 'Coming Soon' : $status; ?></a><?php
                                            }
                                        } elseif ($current_channel_seller_center_count > 1) {
                                            $status = 'Get Connected';
                                            if (isset($multiseller_center_connected_channel_count) and ! empty($multiseller_center_connected_channel_count)) {
                                                $status = $multiseller_center_connected_channel_count . ' of ' . $current_channel_seller_center_count . ' Seller Centers Connected';
                                            }
                                            ?>
                                            <a href="javascript:" data-toggle="modal" class=""  data-target="#<?php echo $stripe_Channel_id; ?> "><?php echo $status; ?></a>
                                            <?php
                                        }
                                        ?></td>
                                <?php }
                                ?>
                            </tr>
                            <?php
                        }
                        ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<?php
$connection1 = \Yii::$app->db;

$model2 = $connection->createCommand('SELECT COUNT(parent_name), parent_name FROM channels GROUP BY parent_name');

$channels1 = $model2->queryAll();
//echo '<pre>'; print_r($channels1); echo '</pre>';
foreach ($channels1 as $channels_data1) {
    $count_parent_name1 = $channels_data1['COUNT(parent_name)'];

    $parent_name1 = $channels_data1['parent_name'];

    $marketplace1 = Channels::find()->Where(['parent_name' => $parent_name1])->one();



    if (empty($marketplace1)):
        $name = "test";
    else:
        $name = $marketplace1->channel_name;
    endif;
    ?>
    <?php if ($count_parent_name1 == 1) { ?>
        <div id="<?php echo $marketplace1->stripe_Channel_id; ?>" tabindex="-1" role="dialog" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                    </div>
                    <div class="modal-body">
                        <div class="text-center">
                            <div class="text-primary"><img src="<?php echo $marketplace1->channel_image; ?>" style="width: 50%" alt="" class="<?php echo $marketplace1->stripe_Channel_id == "channel_Xiao_Hong_Shu" ? 'resize_Xiao' : ''; ?>"></span></div>
                            <?php if ($marketplace1->stripe_Channel_id == 'channel_Instagram') { ?>
                                <h3>Coming Soon</h3>
                            <?php } ?>
                            <p>Subscribe to get updated when <?= $name ?> becomes available in Elliot.</p>
                            <div class="xs-mt-50"> 
                                <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Close</button>
                                <?php if ($channels_data1['parent_name'] == 'channel_google-shopping'): ?>
                                    <a href="/google-shopping/"><button type="button" class="btn btn-space btn-primary">Subscribe</button></a>

                                <?php else: ?>
                                    <button type="button" data-dismiss="modal" class="btn btn-space btn-primary subscribeChannel" data-type="<?php echo $channels_data1['parent_name'] ?>">Subscribe</button>
                                <?php endif;
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer"></div>
                </div>
            </div>
        </div>
    <?php } else if ($count_parent_name1 != 1) { ?>  
        <div id="<?php echo $marketplace1->stripe_Channel_id; ?>" tabindex="-1" role="dialog" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                    </div>
                    <div class="modal-body">
                        <div class="text-center">
                            <div class="text-primary"><img src="<?php echo $marketplace1->channel_image; ?>" style="width: 50%" alt=""></span></div>
                            <div class="row">
                                <div class="form-group">
                                    <?php
                                    $channels_group_data = Channels::find()->Where(['parent_name' => $parent_name1])->all();
                                    $arr = array();
                                    foreach ($channels_group_data as $val) {
                                        $arr[] = $val->channel_name;
                                    }
                                    sort($arr);

                                    foreach ($arr as $channel_group) {
                                        ?>
                                        <div class="col-sm-3">
                                            <div class="be-radio inline">
                                                <input type="radio" checked="" name="rad3" id="<?php echo $channel_group; ?>" value="<?php echo $channel_group; ?>">
                                                <label class="channel-modal-span-group" for="<?php echo $channel_group; ?>"><b><?php echo $channel_group; ?></b></label>
                                            </div>

                                        </div>
                                    <?php } ?>


                                </div>
                            </div>
                            <div class="row">
                                <div class="xs-mt-50"> 
                                    <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Close</button>
                                    <button type="button" class="btn btn-space btn-primary">Subscribe</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer"></div>
                </div>
            </div>
        </div>

        <?php
    }
}
?>


