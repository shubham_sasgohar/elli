

<div class="row wizard-row">
    <div class="col-md-12 fuelux">

        <div id="revenue" class="carouseller"> 
            <a href="javascript:void(0)" class="carouseller__left">‹</a> <div class="carouseller__wrap"> 
                <div class="" style="
                     margin-bottom: 60px;
                     ">

                    <div id="" class="col-sm-2 pull-right " style="
                         padding-bottom: 10px;
                         ">

                        <select class="form-control customer_validate countrySelection" id="revenueSelect" name="cu-Country" style="color:#989696;">
                            <option value="0">Please Select Value</option>
                            <?php
                            if (isset($countries) and ! empty($countries)) {
                                foreach ($countries as $key => $single) {
                                    ?>
                                    <option name="country" value="<?php echo $key; ?>" class=""><?php echo $single; ?></option>
                                    <?php
                                }
                            }
                            ?>

                        </select>
                    </div>
                </div>
                <div class="carouseller__list revenueProducts"> 
                    <?php
                    if (isset($products_by_revenue) and ! empty($products_by_revenue)) {
                        foreach ($products_by_revenue as $key => $single) {
                            if (empty($single['product']))
                                continue;
                            ?>
                            <div class="car__2" data-value="<?php echo $single['product_price'] ?>">
                                <?php
                                if (isset($single['product']['productImages']) and ! empty($single['product']['productImages']) and isset($single['product']['productImages'][0]) and ! empty($single['product']['productImages'][0])) {
                                    $src = $single['product']['productImages'][0]['link'];
                                } else {
                                    $src = '/img/elliot-logo.svg';
                                }
                                ?>                                        <img src="<?php echo $src ?>" height="120" width="120">
                                <?php ?>
                                <br>
                                <h5><a href="/products/<?php echo $single['product']['id'] ?>"><?php echo @$single['product']['product_name'] ?></a></h5>

                                <h6><?php echo $user->currency . ' ' . @$single['product']['price'] ?></h6>

                            </div>

                            <?php
                        }
                    }
                    ?>




                </div>
            </div>
            <a href="javascript:void(0)" class="carouseller__right">›</a>
        </div>
    </div>
</div>

<div class="row wizard-row">
    <div class="col-md-12 fuelux">

        <div id="volume" class="carouseller"> 
            <a href="javascript:void(0)" class="carouseller__left">‹</a> <div class="carouseller__wrap"> 
                <div class="" style="
                     margin-bottom: 60px;
                     ">

                    <div id="" class="col-sm-2 pull-right " style="
                         padding-bottom: 10px;
                         ">

                        <select class="form-control customer_validate countrySelection" id="volumeSelect" name="cu-Country" style="color:#989696;">
                            <option value="0">Please Select Value</option>
                            <?php
                            if (isset($countries) and ! empty($countries)) {
                                foreach ($countries as $key => $single) {
                                    ?>
                                    <option name="country" value="<?php echo $key; ?>" class=""><?php echo $single; ?></option>
                                    <?php
                                }
                            }
                            ?>

                        </select>
                    </div>
                </div>
                <div class="carouseller__list volumeProducts"> 
                    <?php
                    if (isset($products_by_volume) and ! empty($products_by_volume)) {
                        foreach ($products_by_volume as $key => $single) {
                            if (empty($single['product']))
                                continue;
                            ?>
                            <div class="car__2" data-value="<?php echo $single['product_quantity'] ?>">
                                <?php
                                if (isset($single['product']['productImages']) and ! empty($single['product']['productImages']) and isset($single['product']['productImages'][0]) and ! empty($single['product']['productImages'][0])) {
                                    $src = $single['product']['productImages'][0]['link'];
                                } else {
                                    $src = '/img/elliot-logo.svg';
                                }
                                ?>                                        <img src="<?php echo $src ?>" height="120" width="120">
                                <?php ?>
                                <br>
                                <h5><a href="/products/<?php echo $single['product']['id'] ?>"><?php echo @$single['product']['product_name'] ?></a></h5>

                                <h6><?php echo $user->currency . ' ' . @$single['product']['price'] ?></h6>

                            </div>

                            <?php
                        }
                    }
                    ?>




                </div>
            </div>
            <a href="javascript:void(0)" class="carouseller__right">›</a>
        </div>
    </div>
</div>

<div id="mod-danger" tabindex="-1" role="dialog" class="modal fade in product_ajax_request_error" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close lazada_error_modal_close"></span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
                    <h3 id=''>Error</h3>
                    <p id="">No data Found for the selected Region.</p>
                    <div class="xs-mt-50">
                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default lazada_error_modal_close">Close</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('#revenue').carouseller();
        $('#volume').carouseller();
        $('.countrySelection').on('change', function () {
            $this = $(this);
            country_code = $this.val();
            id = $this.attr('id');
            country_code_volume = '';
            country_code_revenue = '';
            if (id == 'volumeSelect') {
                country_code_volume = country_code;
                divClass = 'volumeProducts';

            }
            if (id == 'revenueSelect') {
                country_code_revenue = country_code;
                divClass = 'revenueProducts';

            }
            $.ajax({
                type: 'Post',
//                dataType: 'json',
                data: {country_code_revenue: country_code_revenue, country_code_volume: country_code_volume},
                url: '/lazada/get-filtered-products',
                success: function (data) {
                    if (data) {
                        $('.' + divClass).html(data);
                    } else {
                        $('#' + id).prop('selectedIndex', 0);
                        $('.product_ajax_request_error').modal('show');
                    }

                }
            });
        });
    });
</script>