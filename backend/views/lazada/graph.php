<div class="row">

    <div class="col-sm-6">
        <div class="widget widget-fullwidth">
            <div class="widget-head">
                <div class="tools"><span class="icon mdi mdi-chevron-down"></span><span class="icon mdi mdi-refresh-sync"></span><span class="icon mdi mdi-close"></span></div><span class="title">Customer Acquired</span>
            </div>
            <div class="widget-chart-container">
                <div class="counter">
                    <div class="value">80%</div>
                    <div class="desc">More Clients</div>
                </div>
                <div id="bar-chart1" class="bar-chart1"style="height: 230px;"></div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    $(document).ready(function () {
        var obj = {};
        var final_repeat_customer_list = '<?php echo json_encode($final_repeat_customer_list); ?>';
        var final_new_customer_list = '<?php echo json_encode($final_new_customer_list); ?>';
        console.log(final_new_customer_list);
        console.log(final_repeat_customer_list);
        final_new_customer_list = JSON.parse(final_new_customer_list);
        final_repeat_customer_list = JSON.parse(final_repeat_customer_list);
        console.log(final_new_customer_list);
        console.log(final_repeat_customer_list);
        (function ($) {
            $(function () {
                "use strict";
                if ($('#bar-chart1').hasClass('bar-chart1') == true) {
                    var plot_statistics = $.plot($("#bar-chart1"), [
                        {
//                            data: [
//                                [0, 45], [1, 55], [2, 19], [3, 28], [4, 30], [5, 37], [6, 35], [7, 38], [8, 48], [9, 43], [10, 38], [11, 32], [12, 38]
//                            ],
                            data: final_new_customer_list,
                            label: "Page Views"
                        },
                        {
//                            data: [
//                                [0, 7], [1, 10], [2, 65], [3, 23], [4, 24], [5, 29], [6, 25], [7, 33], [8, 35], [9, 38], [10, 32], [11, 27], [12, 32]
//                            ],
//data: [["0,0"],["1,0"],["2,0"],["3,0"],["4,0"],["5,0"],["6,0"],["7,2"],["8,1"],["9,1"],["10,3"],["11,3"],["12,25"]],
                            data: final_repeat_customer_list,
                            label: "Unique Visitor"
                        }
                    ], {
                        series: {
                            bars: {
                                align: 'center',
                                show: true,
                                lineWidth: 1,
                                barWidth: 0.6,
                                fill: true,
                                fillColor: {
                                    colors: [{
                                            opacity: 1
                                        }, {
                                            opacity: 1
                                        }
                                    ]
                                }
                            },
                            shadowSize: 2
                        },
                        legend: {
                            show: false
                        },
                        grid: {
                            margin: 0,
                            show: false,
                            labelMargin: 10,
                            axisMargin: 500,
                            hoverable: true,
                            clickable: true,
                            tickColor: "rgba(0,0,0,0.15)",
                            borderWidth: 0
                        },
                        colors: ['blue', 'green'],
                        xaxis: {
                            ticks:final_repeat_customer_list.length,
                            tickDecimals: 0
                        },
                        yaxis: {
                            autoscaleMargin: 0.5,
                            ticks: 4,
                            tickDecimals: 0
                        }
                    });
                }
                ;
            });
        })(jQuery);
    });
</script>