<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\Breadcrumbs;
use backend\models\Attributes;
use backend\models\AttributeType;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Product Attributes';
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['/products']];
$this->params['breadcrumbs'][] = ['label' => 'Attributes', 'url' => ['/attributes']];
$this->params['breadcrumbs'][] = 'View All';
//$this->title
$id = Yii::$app->user->identity->id;
$attributes_data = Attributes::find()->Where(['elliot_user_id' => $id])->all();
?>
<div class="page-head">
    <h2 class="page-head-title"><?= Html::encode($this->title) ?></h2>
    <ol class="breadcrumb page-head-nav">
        <?php
        echo Breadcrumbs::widget([
          'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
        ?>
    </ol>
    <?= Html::a('Add Attribute', ['create'], ['class' => 'btn btn-primary']) ?>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default panel-table">
            <div class="panel-heading"><?php echo $this->title; ?>
                <div class="tools"><span class="icon mdi mdi-download"></span><span class="icon mdi mdi-more-vert"></span></div>
            </div>
            <div class="panel-body">
                <table id="variations_table" class="table table-striped table-hover table-fw-widget">
                    <thead>
                        <tr>
                            <th>Attribute Name</th>
                            <th>Attribute Type</th>
                            <th>Attribute Performance</th>
                            <th>Items Used By</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($attributes_data as $attributes) :
                          $attr_type_obj = AttributeType::find()->where(['elliot_user_id' => $id, 'attribute_type_id' => $attributes->attribute_type])->one();
                          ?>
                          <tr>
                              <td><a href="attributes/<?= $attributes->attribute_id; ?>"><?= $attributes->attribute_name; ?></a></td>
                              <td><a href="attribute-type/<?= $attributes->attribute_type; ?>"><?= ucfirst($attr_type_obj->attribute_type_name); ?></a></td>
                              <td>-</td>                     
                              <td>-</td>
                          </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>






