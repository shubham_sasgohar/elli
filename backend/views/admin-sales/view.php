<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\AdminSales */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Admin Sales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-sales-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'transaction_id',
            'user_id',
            'subscription_id',
            'channel_id',
            'transaction_amount',
            'transaction_date',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
