<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\AdminSales */

$this->title = 'Create Admin Sales';
$this->params['breadcrumbs'][] = ['label' => 'Admin Sales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-sales-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
