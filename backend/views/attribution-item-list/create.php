<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\AttributionItemList */

$this->title = 'Create Attribution Item List';
$this->params['breadcrumbs'][] = ['label' => 'Attribution Item Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attribution-item-list-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
