<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\AttributionItemListSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="attribution-item-list-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'attribution_item_list_id') ?>

    <?= $form->field($model, 'elliot_user_id') ?>

    <?= $form->field($model, 'attribution_id') ?>

    <?= $form->field($model, 'item_name') ?>

    <?= $form->field($model, 'item_value') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
