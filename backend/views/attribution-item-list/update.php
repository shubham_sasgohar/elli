<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\AttributionItemList */

$this->title = 'Update Attribution Item List: ' . $model->attribution_item_list_id;
$this->params['breadcrumbs'][] = ['label' => 'Attribution Item Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->attribution_item_list_id, 'url' => ['view', 'id' => $model->attribution_item_list_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="attribution-item-list-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
