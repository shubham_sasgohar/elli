<?php
/**
 * Created by PhpStorm.
 * User: sevenstars
 * Date: 8/13/17
 * Time: 2:15 PM
 */?>
<script>
    var hashStr = window.location.hash;
    var params = hashStr.split('&');
    var refresh_token = params[3].split('=');
    window.location.href = 'auth-token?refresh_token=' + refresh_token[1];
</script>
