<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\Breadcrumbs;
use backend\models\ChannelConnection;
use backend\models\Channels;

$this->title = 'Add Your Tmall Store';
$this->params['breadcrumbs'][] = 'TMall';

//$get_tmallrow = Stores::find()->select('store_id')->where(['store_name' => 'Vtex'])->one();
$get_tmall_row = Channels::find()->select('channel_ID')->where(['channel_name' => 'TMall'])->one();
//$tmallstore_id = '';
$tmall_channel_id = '';
//if(!empty($get_tmallrow))
//{
//    $tmallstore_id = $get_tmallrow->store_id;
//}
//$checkConnection = StoresConnection::find()->where(['store_id' => $tmallstore_id, 'user_id' => Yii::$app->user->identity->id])->one();
if(!empty($get_tmall_row)) {
    $tmall_channel_id = $get_tmall_row->channel_ID;
}
$checkConnection = ChannelConnection::find()->where(['channel_ID' => $tmall_channel_id, 'user_id' => Yii::$app->user->identity->id])->one();
?>
<input type="hidden" id="hidParam" value="<?php if(isset($param)) echo $param; ?>">
<input type="hidden" id="hidURL" value="<?php echo $_SERVER['HTTP_HOST']; ?>">

<div class="page-head">
    <h2 class="page-head-title"><?= Html::encode($this->title) ?></h2>
    <ol class="breadcrumb page-head-nav">
        <?php
        echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
        ?>
    </ol>
</div>
<div class="row wizard-row">
    <div class="col-md-12 fuelux">
        <div class="block-wizard panel panel-default">
            <div id="wizard-store-connection" class="wizard wizard-ux">
                <ul class="steps">
                    <li data-step="1" class="active">Authorize<span class="chevron"></span></li>
                    <!--li data-step="2">Connect<span class="chevron"></span></li-->
                </ul>
                <div class="step-content">
                    <input id="user_id" type="hidden" value="<?= Yii::$app->user->identity->id; ?>" />
                    <?php if (empty($checkConnection)) : ?>
                        <form id="tmall-authorize-form" action="#" data-parsley-namespace="data-parsley-" data-parsley-validate="" novalidate="" class="form-horizontal group-border-dashed">
                            <div class="form-group no-padding main-title">
                                <div class="col-sm-12">
                                    <label class="control-label">Please provide your Tmall store Credentials to integrate with Elliot:</label>
                                    <p> To integrate Tmall store with Elliot, you'll need to create App Key and App Secret in your Tmall store. Please <a target="_blank" href="http://open.taobao.com">Follow</a> this link.</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">App Key</label>
                                <div id="tmall_app_key" class="col-sm-6">
                                    <input type="text" placeholder="Please Enter value" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">App Secret</label>
                                <div id="tmall_app_secret" class="col-sm-6">
                                    <input type="text" placeholder="Please Enter value" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-default btn-space"><a href='/channels'>Cancel</a></button>
                                    <button data-wizard="#wizard-store-connection" class="btn btn-primary btn-space tmall_auth_connection">Authorize & Connect</button>
                                </div>
                            </div>
                        </form>
                    <?php else: ?>
                        <div id="bgc_conn_text" class="form-group no-padding main-title">
                            <div class="col-sm-12">
                                <label class="control-label">Your Tmall store is now connected. When your data has been fully imported, you will receive a confirmation notification.</label>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="tmall_ajax_request" tabindex="-1" role="dialog" class="modal fade in">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close tmall_modal_close"></span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="text-success"><span class="modal-main-icon mdi mdi-check"></span></div>
                    <h3 id="tmall_ajax_header_msg"></h3>
                    <p id="tmall_ajax_msg"></p>
                    <div class="xs-mt-50">
                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default tmall_modal_close">Close</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<div id="mod-danger" tabindex="-1" role="dialog" class="modal fade in tmall_ajax_request_error" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close tmall_error_modal_close"></span></button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
                    <h3 id='tmall_ajax_header_error_msg'></h3>
                    <p id="tmall_ajax_msg_eror"></p>
                    <div class="xs-mt-50">
                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default tmall_error_modal_close">Close</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
