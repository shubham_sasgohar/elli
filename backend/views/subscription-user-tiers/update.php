<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\SubscriptionUserTiers */

$this->title = 'Update Subscription User Tiers: ' . $model->subscription_id;
$this->params['breadcrumbs'][] = ['label' => 'Subscription User Tiers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->subscription_id, 'url' => ['view', 'id' => $model->subscription_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="subscription-user-tiers-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
