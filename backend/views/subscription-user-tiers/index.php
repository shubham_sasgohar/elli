<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SubscriptionUserTiersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Subscription User Tiers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscription-user-tiers-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Subscription User Tiers', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'subscription_id',
            'plan_name',
            'user_range',
            'amount',
            'created_at',
            // 'updated_At',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
