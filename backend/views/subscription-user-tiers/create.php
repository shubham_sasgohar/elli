<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\SubscriptionUserTiers */

$this->title = 'Create Subscription User Tiers';
$this->params['breadcrumbs'][] = ['label' => 'Subscription User Tiers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscription-user-tiers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
