<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\CategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- <form class="form" action="/elliot-live/backend/views/categories/import_cat.php" method="POST"  name="SubmitionForm" id="SubmitionForm" accept-charset='UTF-8' enctype="multipart/form-data">
                   
                    <div class="file_csv_upload">
                      <input type="file" name="csv" id="file_csv"/>
                    </div>
    
                        <input type="submit" name="submit" value="Submit" tabindex="3" />
                    
                  
                </form>-->
<div class="categories-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Categories', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'category_ID',
            'category_name',
            'parent_category_ID',
            'created_at',
            'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
