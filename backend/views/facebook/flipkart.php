<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use backend\models\Channels;
use yii\db\Query;
use backend\models\ChannelConnection;

$this->title = 'Add Your Flipkart Store';
$this->params['breadcrumbs'][] = 'Add Your Flipkart Store';

$id = Yii::$app->user->identity->id;
$channel = ChannelConnection::find()->Where(['user_id' => $id])->one();
$users_Id = Yii::$app->user->identity->id;
                                $flipkart = Channels::find()->where(['channel_name' => 'Flipkart'])->one();
                                $channel_id = $flipkart->channel_ID;
                                $channel_connection = ChannelConnection::find()->where(['user_id' => $users_Id, 'channel_id' => $channel_id])->one();
?>
<div class="page-head">
    <h2 class="page-head-title">Add Your Flipkart Store</h2>
    <ol class="breadcrumb page-head-nav">
        <?php
        echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
        ?>
    </ol>
</div>
<div class="main-content container-fluid">
    <div class="row wizard-row">
        <div class="col-md-12 fuelux wechat12">
            <div class="block-wizard panel panel-default">
                <div id="wizard1" class="wizard wizard-ux">
                    <ul class="steps">
<!--                        <li data-step="1" class="<?php echo isset($channel_connection) && $channel_connection->connected == 'yes' ? 'complete' : 'active'; ?>"<?php echo isset($channel_connection) && $channel_connection->connected == 'yes' ? 'disabled' : ''; ?>>Step 1<span class="chevron"></span></li>-->
                        <li data-step="1" class="active"<?php echo isset($channel_connection) && $channel_connection->connected == 'yes' ? 'disabled' : ''; ?>>Flipkart<span class="chevron"></span></li>
<!--                        <li data-step="2" class="<?php echo isset($channel_connection) && $channel_connection->connected == 'yes' ? 'active' : ''; ?>">Step 2<span class="chevron"></span></li>-->
                    </ul>
                    <!--div class="actions">
                      <button type="button" class="btn btn-xs btn-prev btn-default" disabled="disabled"><i class="icon mdi mdi-chevron-left"></i>Prev</button>
                      <button type="button" data-last="Finish" class="btn btn-xs btn-next btn-default" disabled="disabled">Next<i class="icon mdi mdi-chevron-right"></i></button>
                    </div-->
                    <div class="step-content">
                        <div data-step="1" class="step-pane active">
                            <!--<p class="afterwechatrequestmsg">-->
                            <?php if (Yii::$app->user->identity->role != 'superadmin'){ ?>
                            <form role="form" action="/facebook/flipkart" class="form-group group-border-dashed" enctype="multipart/form-data" method="post">
                                <div class="form-group no-padding">
                                    <div class="col-sm-10 text-center">
                                        <label class="control-label pull-left">Upload Flipkart Products  CSV</label>
                                        <br>
                                        <input type="file" name="flipkart_csv" style="margin-top:20px;" > 
                                    </div>
                                </div>
                                <input type="submit" class="btn btn-primary btn-space" style="margin-top:30px;" >
                            </form>
                            <?php }else{
                                $products_inactive = \backend\models\Products::find()->Where(['product_status'=>'0'])->all();
//                                echo'<pre>';                                print_r($products_inactive);die;
                                if (isset($products_inactive) and ! empty($products_inactive)) {
                                    ?><p><?php echo count($products_inactive) ?> Products have been added by user via Flipkart Product excel file. Do you Want to approve All those Products.?</p><?php
                                }
                                
                                        ?>   <?php
                            } ?>
                        </div>
            
                    </div>
                </div>

                <div id="walkthechat_request" tabindex="-1" role="dialog" style="display: none;" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                            </div>
                            <div class="modal-body">
                                <div class="text-center">
                                    <div class="text-success"><span class="modal-main-icon mdi mdi-check"></span></div>
                                    <h3>Success!</h3>
                                    <p>Your Flipkart application is being processed and you will receive an email containing documentation necessary to sell across borders. Additionally, an Elliot representative will be in touch 
                                        within 24 hours to complete your on boarding. Please have your documentation ready to expedite this process.</p>
                                    <div class="xs-mt-50">
                                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Close</button>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer"></div>
                        </div>
                    </div>
                </div>

                <div id="walkthechat_request_error" tabindex="-1" role="dialog" class="modal fade" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close"></span></button>
                            </div>
                            <div class="modal-body">
                                <div class="text-center">
                                    <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
                                    <h3>Error!</h3>
                                    <p>Please provide valid login details</p>
                                    <div class="xs-mt-50">
                                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Close</button>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer"></div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
