<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\Breadcrumbs;
use backend\models\Channels;
use backend\models\ChannelConnection;

$channel_type='';
if($type=='Malaysia')$channel_type='Lazada Malaysia';
$this->title = 'Add Your Lazada '.$type.' Store';
$this->params['breadcrumbs'][] = $this->title;

$lazada = Channels::find()->where(['channel_name' => $channel_type])->one();
//echo'<pre>';
//print_r($lazada);die;
$channel_id = $lazada->channel_ID;
//echo $channel_id;die;
$checkConnection = ChannelConnection::find()->Where(['user_id'=>Yii::$app->user->identity->id,'channel_id'=>$channel_id])->one();
//echo'<Pre>';
//print_r($checkConnection);die;
?>
<div class="page-head">
    <h2 class="page-head-title"><?= Html::encode($this->title) ?></h2>
    <ol class="breadcrumb page-head-nav">
        <?php
        echo Breadcrumbs::widget([
          'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
        ?>
    </ol>
</div>
<div class="row wizard-row">
    <div class="col-md-12 fuelux">
        <div class="block-wizard panel panel-default">
            <div id="wizard-store-connection" class="wizard wizard-ux">
                <ul class="steps">
                    <li data-step="1" class="active">Authorize<span class="chevron"></span></li>
                    <!--<li data-step="2">Connect<span class="chevron"></span></li>-->
                </ul>
                <div class="step-content">
                    <?php if (empty($checkConnection)) : ?>
                    <div id="bgc_text1" class="form-group no-padding main-title" style="display: none">
                        <div class="col-sm-12">
                            <label class="control-label">Your Lazada store is connecting, you will be notified via email when complete.</label>
                        </div>
                    </div>
                    <form id="lazada-authorize-form" action="#" data-parsley-namespace="data-parsley-" data-parsley-validate="" novalidate="" class="form-horizontal group-border-dashed">
                        <div class="form-group no-padding main-title">
                            <div class="col-sm-12">
                                <label class="control-label">Please provide your Lazada <?php echo $type; ?> API details for Authorizing the Integration:</label>
                                                            
                            </div>
                        </div>
                        <input type="hidden" id="channel_type" value="<?php echo $type; ?>">
                        <input type="hidden" id="channel_id" value="<?php echo $channel_id; ?>">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Lazada <?php echo $type; ?> User Email  *</label>
                            <div id="lazada_url" class="col-sm-6">
                                <input type="text" id="lazada_user_email" placeholder="Please Enter value" class="form-control customer_validate">
                                <!--<em class="notes">Please use full store url ex "http://Lazada.com"</em>-->
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="col-sm-3 control-label">Lazada <?php echo $type; ?> API URL  *</label>
                            <div id="lazada_url" class="col-sm-6">
                                <input type="text" id="lazada_api_url" placeholder="Please Enter value" class="form-control customer_validate">
                                <em class="notes">Please use full url ex "https://api.sellercenter.lazada.com.my"</em>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Lazada <?php echo $type; ?> Store API  Key *</label>
                            <div id="lazada_consumer" class="col-sm-6">
                                <input type="text" placeholder="Please Enter value" class="form-control customer_validate" id="lazada_api_key">
                                <em class="notes">Example: JeVfOHaBm0RdndrqR1HsZGyL20xHwGSxi3gngcsvOaCnPE_1nsf2Yb7f</em>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button class="btn btn-default btn-space">Cancel</button>
                                <button data-wizard="#wizard-store-connection" class="btn btn-primary btn-space wizard-next-auth-store-lazada" type="button">Authorize & Connect</button>
                            </div> 
                        </div>
                    </form>
                    <?php else: ?>
                    <div id="bgc_conn_text" class="form-group no-padding main-title">
                        <div class="col-sm-12">
                            <label class="control-label">Your Lazada store is connected. When data is import you will get a notify message</label>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="lazada_ajax_request" tabindex="-1" role="dialog" class="modal fade in">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close lazada_modal_close"></span></button>
        </div>
        <div class="modal-body">
          <div class="text-center">
            <div class="text-success"><span class="modal-main-icon mdi mdi-check"></span></div>
            <h3 id="ajax_header_msg">Success!</h3>
            <p id="lazada_ajax_msg"></p>
            <div class="xs-mt-50">
              <button type="button" data-dismiss="modal" class="btn btn-space btn-default lazada_modal_close">Close</button>
            </div>
          </div>
        </div>
        <div class="modal-footer"></div>
      </div>
    </div>
</div>
<div id="mod-danger" tabindex="-1" role="dialog" class="modal fade in lazada_ajax_request_error" style="display: none;">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close lazada_error_modal_close"></span></button>
          </div>
             <div class="modal-body">
                  <div class="text-center">
                    <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
                    <h3>Error!</h3>
                    <p>Please provide valid login details</p>
                    <div class="xs-mt-50">
                      <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Close</button>
                    </div>
                  </div>
                </div>
          <div class="modal-footer"></div>
        </div>
      </div>
    </div>
            