<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use backend\models\Channels;
use yii\db\Query;
use backend\models\ChannelConnection;

$this->title = 'Add Your Facebook Store';
$this->params['breadcrumbs'][] = 'Add Your Facebook Store';

$id = Yii::$app->user->identity->id;
$facebook = Channels::find()->where(['channel_name' => 'Facebook'])->one();
//echo'<pre>';
//print_r($lazada);die;
$channel_id = $facebook->channel_ID;
//echo $channel_id;die;
$checkConnection = ChannelConnection::find()->Where(['user_id' => Yii::$app->user->identity->id, 'channel_id' => $channel_id, 'connected' => 'yes'])->one();
?>
<div class="page-head">
    <h2 class="page-head-title">Add Your Facebook Store</h2>
    <ol class="breadcrumb page-head-nav">
        <?php
        echo Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);
        ?>
    </ol>
</div>
<div class="main-content container-fluid">
    <div class="row wizard-row">
        <div class="col-md-12 fuelux wechat12">
            <div class="block-wizard panel panel-default">
                <div id="wizard1" class="wizard wizard-ux">
                    <ul class="steps">
                        <li data-step="1" class="active   " >Steps<span class="chevron"></span></li>
                    </ul>
                    <div class="step-content">
                        <div data-step="1" class=" active">
                            1) <b>Firstly create a catalog in your facebook Business Account. </b><br><br>
                            2) <b>Then Add a Product feed. You will have to add Url <b><?php echo Yii::$app->params['BASE_URL'] . 'facebook/feed/?u_id=' . base64_encode(base64_encode($id)); ?></b> in Feed Url. </b><br><br>
                            3) <b>Doing this will enable Facebook Products Feed.</b>
                        </div>
                        <?php
                        $style = '';
                        if (empty($checkConnection)) {
                            $style = 'style="display:none;"';
                        }
                        ?>
                        <div class="row">

                            <div class="panel-bod code " <?php echo $style ?>>
                                <div class="col-md-3">
                                    <label class=" control-label panel-heading">Schedule File :</label></div>
                                <div class="col-md-5" style="margin-top:7px;">
                                    <input type="text" readonly="readonly" value="<?php echo Yii::$app->params['BASE_URL'] . 'feed/?u_id=' . base64_encode(base64_encode($id)) ?>" class="form-control"></div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <form action="#" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label panel-heading">Facebook Connectivity</label>
                                    <div class="col-sm-6 xs-pt-5">
                                        <div class="switch-button switch-button-success">
                                            <input type="checkbox" <?php
                                                   if ($checkConnection) {
                                                       echo 'checked=""';
                                                   }
                                                   ?>  name="facebookfeedcheck" id="facebookfeedcheck"><span>        <label for="facebookfeedcheck"></label></span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                <div tabindex="-1" role="dialog" class="modal fade in fbmod-success" style="display: none; background: rgba(0,0,0,0.6);">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close fb_modal_close"></span></button>
                            </div>
                            <div class="modal-body">
                                <div class="text-center">
                                    <div class="text-success"><span class="modal-main-icon mdi mdi-check"></span></div>
                                    <h3 id="ajax_header_msg">Success!</h3>
                                    <p id="magento_ajax_msg">Your Facebook Feed account is now enabled and available for use.</p>
                                    <div class="xs-mt-50">
                                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default fb_modal_close">Close</button>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer"></div>
                        </div>
                    </div>
                </div>
                <div id="mod-danger" tabindex="-1" role="dialog" class="modal fade in fbmod-error" style="display: none; background: rgba(0,0,0,0.6);">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="mdi mdi-close fb_error_modal_close"></span></button>
                            </div>
                            <div class="modal-body">
                                <div class="text-center">
                                    <div class="text-danger"><span class="modal-main-icon mdi mdi-close-circle-o"></span></div>
                                    <h3 id='ajax_header_error_msg'>Success!</h3>
                                    <p id="magento_ajax_msg_eror">You have disconnected your FacebookFeed. Your file will still be available; however, it will not be updated. To re-enable, simply switch Facebook Shopping back on.</p>
                                    <div class="xs-mt-50">
                                        <button type="button" data-dismiss="modal" class="btn btn-space btn-default fb_error_modal_close">Close</button>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer"></div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
<script type="text/javascript">$(document).ready(function () {
        $('.class').on('click', function () {
            $this = $(this);
            $.ajax({
                type: 'Post',
                dataType: 'json',
                data: {id: id},
                url: '/user/get',
                success: function (data) {
                    console.log(
                            );
                }
            });
        });
    });</script>