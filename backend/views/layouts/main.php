<?php
/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
            <!--<script src="https://s86.co/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js"></script>-->

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->
        <?php $this->registerAssetBundle(yii\web\JqueryAsset::className(), \yii\web\View::POS_HEAD); ?>

        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <?php
        $this->registerJs(
                'jQuery(document).ready(function () {
              //initialize the javascript
              App.init();
              App.chartsSparklines();
              App.uiNotifications();
              App.textEditors();
              App.formEditable();
              App.dataTables();
              App.dashboard();
              App.wizard();
              App.formElements();
              App.uiNestableLists();
              App.masks();
              
              });'
        );
        ?>
        <style type="text/css">
            .stripe-button-el {
                display: none !important;
            }
        </style>
    </head>
    <?php
    if (Yii::$app->session->hasFlash('success')):
        $header_cls = 'be-color-header be-color-header-success';
    elseif (Yii::$app->session->hasFlash('warning')):
        $header_cls = 'be-color-header be-color-header-warning';
    elseif (Yii::$app->session->hasFlash('danger')):
        $header_cls = 'be-color-header be-color-header-danger';
    elseif (Yii::$app->session->hasFlash('info')):
        $header_cls = 'be-color-header';
    else:
        $header_cls = '';
    endif;


    $action = Yii::$app->controller->id . '/' . Yii::$app->controller->action->id;
    if ($action == 'warehouse/shipstation') :
        $shipclass = "shipmainfile";
    else:
        $shipclass = "";
    endif;

    // $product_list = Yii::$app->controller->id . '/' . Yii::$app->controller->action->id;
    // if($product_list == 'products/index') :
    //     $productClass = 'productListTablE';
    // else:
    //     $productClass = '';
    // endif;


//    $curr_action='shipstation/'.$action;
    ?>
    <body>
        <div class="be-wrapper be-fixed-sidebar <?php echo $header_cls ?> be-loading">
            <?php $this->beginBody() ?>

            <?php include 'header.php'; ?>

            <?= Alert::widget() ?>
            <div class="be-content">
                <div class="main-content container-fluid <?php echo $shipclass ?>">
                    <?=
                    $content
                    ?>
                </div>
            </div>


            <?php include 'footer.php'; ?>

            <div class="be-spinner">
                <svg width="40px" height="40px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                <circle fill="none" stroke-width="4" stroke-linecap="round" cx="33" cy="33" r="30" class="circle"/>
                </svg>
            </div>

            <?php $this->endBody() ?>
        </div>
    </body>
    
    <script>
        window.intercomSettings = {
            app_id: "iswfr8g9",
            email: "<?= Yii::$app->user->identity->email; ?>",
            phone: "<?= Yii::$app->user->identity->phoneno; ?>",
            "subdomain": "<?= Yii::$app->user->identity->domain_name; ?>",
            "STORENAME": "<?= Yii::$app->user->identity->domain_name; ?>",
            "FIRSTNAME": "<?= Yii::$app->user->identity->first_name; ?>",
            "LASTNAME": "<?= Yii::$app->user->identity->last_name; ?>",
            "Subscription Plan": "<?= Yii::$app->user->identity->subscription_plan_id; ?>",
            "customer since": "<?= Yii::$app->user->identity->created_at; ?>"

        };
    </script>
    <script>(function () {
            var w = window;
            var ic = w.Intercom;
            if (typeof ic === "function") {
                ic('reattach_activator');
                ic('update', intercomSettings);
            } else {
                var d = document;
                var i = function () {
                    i.c(arguments)
                };
                i.q = [];
                i.c = function (args) {
                    i.q.push(args)
                };
                w.Intercom = i;
                function l() {
                    var s = d.createElement('script');
                    s.type = 'text/javascript';
                    s.async = true;
                    s.src = 'https://widget.intercom.io/widget/iswfr8g9';
                    var x = d.getElementsByTagName('script')[0];
                    x.parentNode.insertBefore(s, x);
                }
                if (w.attachEvent) {
                    w.attachEvent('onload', l);
                } else {
                    w.addEventListener('load', l, false);
                }
            }
        })()</script>
</html>
<?php $this->endPage() ?>
