<?php

use backend\models\TrialPeriod;
use backend\models\User;
use backend\models\Stores;
use backend\models\StoresConnection;
use backend\models\StoreDetails;
use backend\models\Notification;
use backend\models\Fulfillment;
use backend\models\FulfillmentList;
use backend\models\ChannelConnection;
use backend\models\Channels;
use backend\models\CronTasks;
use backend\models\Orders;
use backend\models\OrderChannel;
use backend\models\CustomerUser;
use backend\models\CustomerAbbrivation;
use backend\models\Products;
use yii\web\Session;

/* @var $this yii\web\View */
$basepath = Yii::getAlias('@baseurl');
$baseurl_profile_images = Yii::getAlias('@baseurl_profile_images');
/* Get current Action */
$action = Yii::$app->controller->action->id; //name of the current action

$controller_action_name = Yii::$app->controller->id . '/' . $action; //the name of the current controller and action

/* End Get current Action */
$user_id = Yii::$app->user->identity->id;

/* get current url */
/* merchant redirect a subdomain */
$actual_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$result = preg_match("#^" . Yii::$app->params['BASE_URL_WITHOUT_SLASH'] . "(.*)$#i", $actual_link);
if (!$result == 0) {
    $domain = Yii::$app->user->identity->domain_name;
    $url = Yii::$app->params['PROTOCOL'] . $domain . '.' . Yii::$app->params['DOMAIN_NAME'];
    //return Yii::$app->getResponse()->redirect($url);
    header("Location: $url"); /* Redirect browser */
    exit;
} else {
    /* get current url */
    /* Check is a user or merchant or unauthorized */
    $curr_url = "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
    $path = explode('.', $curr_url);
    $name = $path[0];
    $dir_name = $_SERVER['DOCUMENT_ROOT'] . '/subdomains/' . $name;

    if (is_dir($dir_name)) {
        
    } else {
        $domain = Yii::$app->user->identity->domain_name;
        $url = Yii::$app->params['PROTOCOL'] . $domain . '.' . Yii::$app->params['DOMAIN_NAME'];
        //return Yii::$app->getResponse()->redirect($url);
        header("Location: $url"); /* Redirect browser */
        exit;
    }
}

/* @var basepath+backend */
//$basepath_backend=Yii::getAlias('@baseurl').'/backend';
//$basepath_backend = Yii::getAlias('@baseurl');
if (!Yii::$app->user->isGuest) {
    $merchantdomain = Yii::$app->user->identity->domain_name;
    $basepath_backend = Yii::getAlias('@baseurl');
    $basepath_backend1 = $merchantdomain . '.' . Yii::$app->params['DOMAIN_NAME'];
}

/* get current user login id */
$users_Id = Yii::$app->user->identity->id;
$updated_userdata = User::find()->where(['id' => $users_Id])->one();

/* get Available User name */
$name = Yii::$app->user->identity->first_name . ' ' . Yii::$app->user->identity->last_name;
if ($name == ' '):
    $user_name = isset(Yii::$app->user->identity->company_name) ? Yii::$app->user->identity->company_name : 'N/A';
else:
    $user_name = $name;
endif;
/* END get Available User name */

/* get user data */
$userdata = User::find()->where(['id' => $users_Id])->one();
$username = $userdata->username;
$trial_days_data = TrialPeriod::find()->one();
$trial_period_days = $trial_days_data->trial_days;
$user_create_date = $userdata->created_at;

/* convert trial period days to hour */
$trial_days_hours = $trial_period_days * 24;
//echo 'trial days hour'.$trial_days_hours.'<br>';

/* get current date */
$current_date = date("Y-m-d h:i:s");

/* Date Diff */
$date1 = date_create($current_date);
$date2 = date_create($user_create_date);
$diff = date_diff($date1, $date2);
//echo'<pre>';
//print_r($diff);
/* get diffrence hours */
$hours = $diff->h;
//echo 'hours'.$hours.'<br>';
/* Total hours */
$trial_diff = $hours + ($diff->days * 24);
//echo 'trial diff hour'.$trial_diff.'<br>';

/* pending hours */
$df = $trial_diff / 24;

$day_diff = (int) $df;

if ($day_diff < 1) {
    $remaining_day = $trial_period_days;
} else {
    $remaining_day = $trial_period_days - $day_diff;
    if ($remaining_day < 0) {
        $userdata = User::find()->where(['id' => $users_Id])->one();
        $userdata->trial_period_status = 'deactivate';
        $userdata->save(false);
    }
}
$updated_userdata = User::find()->where(['id' => $users_Id])->one();


$trial_days_status = $updated_userdata->trial_period_status;
$page_header = $this->title;
if ($page_header == 'Dashboard | Elliot'):
    $page_header = 'Dashboard';
endif;

// Check For Shipstation title
$ship_data = Fulfillment::find()->Where(['elliot_user_id' => Yii::$app->user->identity->id])->one();
if (empty($ship_data)):
    if ($page_header == 'ShipStation') :
        $page_header = 'ShipStation - <a href="/fulfillment/instruction" target="_blank" class="shipanchor"><h4>Instructions for Connecting to Elliot</h4></a>';
    endif;
endif;


if (Yii::$app->user->identity->role != 'superadmin'):
    /* use Session for show notifiaction one time */
    $session = Yii::$app->session;
    $notifiaction = $session->get('notification');
    $notifiaction_result = $session->hasFlash('insert');
    if ($notifiaction_result == 1) {
        ?>
        <!--app ui sticky notification check-->
        <script>
            var remaining_days = '<?php echo $remaining_day; ?>';
            setTimeout(function () {
                jQuery(function () {
                    // stickynotification(remaining_days)
                });
            }, 3000);
        </script>
        <?php
    }

    $bgcnotifs = StoresConnection::find()->Where(['user_id' => Yii::$app->user->identity->id, 'store_id' => 1])->one();
    $notify = 'false';
    if (!empty($bgcnotifs) && $bgcnotifs->import_status == 'Completed'):
        $session = Yii::$app->session;
        $session->setFlash('import', 'You have successfully imported.');
        $bgcnotifs->import_status = 'Completed-Read';
        $bgcnotifs->save(false);
    endif;
    $import_result = $session->hasFlash('import');
    if ($import_result == 1):
        $notify = 'true';
    endif;
    if ($notify == 'true') :
        ?>
        <!--app ui sticky notification check-->
        <script>
            var import_done = '<?php echo $notify; ?>';
            setTimeout(function () {
                jQuery(function () {
                    sticky_ntf_bigcommerce_import(import_done, 'BigCommerce')
                });
            }, 3000);

        </script>
        <?php
    endif;
    /*
     * Shopify import notification
     */
    $shpcnotifs = StoresConnection::find()->Where(['user_id' => Yii::$app->user->identity->id, 'store_id' => 3])->one();

    $notify = 'false';
    if (!empty($shpcnotifs) && $shpcnotifs->import_status == 'Completed'):
        $session = Yii::$app->session;
        $session->setFlash('Shopify_import', 'You have successfully imported.');
        $shpcnotifs->import_status = 'Completed-Read';
        $shpcnotifs->save(false);
    endif;
    $import_result = $session->hasFlash('Shopify_import');
    if ($import_result == 1):
        $notify = 'true';
    endif;
    if ($notify == 'true') :
        ?>
        <!--app ui sticky notification check-->
        <script>
            var import_done = '<?php echo $notify; ?>';
            setTimeout(function () {
                jQuery(function () {
                    sticky_ntf_bigcommerce_import(import_done, 'Shopify')
                });
            }, 3000);
        </script>
        <?php
    endif;

    /*
     * Magento import notification
     */
    $shpcnotifs = StoresConnection::find()->Where(['user_id' => Yii::$app->user->identity->id, 'store_id' => 2])->one();

    $notify = 'false';
    if (!empty($shpcnotifs) && $shpcnotifs->import_status == 'Completed'):
        $session = Yii::$app->session;
        $session->setFlash('magento-import', 'You have successfully imported.');
        $shpcnotifs->import_status = 'Completed-Read';
        $shpcnotifs->save(false);
    endif;
    $import_result = $session->hasFlash('magento-import');
    if ($import_result == 1):
        $notify = 'true';
    endif;
    if ($notify == 'true') :
        ?>
        <!--app ui sticky notification check-->
        <script>
            var import_done = '<?php echo $notify; ?>';
            setTimeout(function () {
                jQuery(function () {
                    sticky_ntf_bigcommerce_import(import_done, 'Magento')
                });
            }, 3000);
        </script>
        <?php
    endif;

    /*
     * Magento 2X import notification
     */
    $store = Stores::find()->select('store_id')->where(['store_name' => 'Magento2'])->one();
    $magento_store_id = $store->store_id;
    $shpcnotifs = StoresConnection::find()->Where(['user_id' => Yii::$app->user->identity->id, 'store_id' => $magento_store_id])->one();

    $notify = 'false';
    if (!empty($shpcnotifs) && $shpcnotifs->import_status == 'Completed'):
        $session = Yii::$app->session;
        $session->setFlash('magento2-import', 'You have successfully imported.');
        $shpcnotifs->import_status = 'Completed-Read';
        $shpcnotifs->save(false);
    endif;
    $import_result = $session->hasFlash('magento2-import');
    if ($import_result == 1):
        $notify = 'true';
    endif;
    if ($notify == 'true') :
        ?>
        <!--app ui sticky notification check-->
        <script>
            var import_done = '<?php echo $notify; ?>';
            setTimeout(function () {
                jQuery(function () {
                    sticky_ntf_bigcommerce_import(import_done, 'Magento2')
                });
            }, 3000);
        </script>
        <?php
    endif;

    /*
     * VTEX store import notification
     */
    $store = Stores::find()->select('store_id')->where(['store_name' => 'VTEX'])->one();
    $vtex_store_id = $store->store_id;
    $shpcnotifs = StoresConnection::find()->Where(['user_id' => Yii::$app->user->identity->id, 'store_id' => $vtex_store_id])->one();

    $notify = 'false';
    if (!empty($shpcnotifs) && $shpcnotifs->import_status == 'Completed'):
        $session = Yii::$app->session;
        $session->setFlash('vtex-import', 'You have successfully imported.');
        $shpcnotifs->import_status = 'Completed-Read';
        $shpcnotifs->save(false);
    endif;
    $import_result = $session->hasFlash('vtex-import');
    if ($import_result == 1):
        $notify = 'true';
    endif;
    if ($notify == 'true') :
        ?>
        <!--app ui sticky notification check-->
        <script>
            var import_done = '<?php echo $notify; ?>';
            setTimeout(function () {
                jQuery(function () {
                    sticky_ntf_bigcommerce_import(import_done, 'VTEX')
                });
            }, 3000);
        </script>
        <?php
    endif;

    /*
     * Woocommerce import notification
     */
    $wcnotifs = StoresConnection::find()->Where(['user_id' => Yii::$app->user->identity->id, 'store_id' => 4])->one();

    $notify = 'false';
    if (!empty($wcnotifs) && $wcnotifs->import_status == 'Completed'):
        $session = Yii::$app->session;
        $session->setFlash('WooCommerce Import', 'You have successfully imported.');
        $wcnotifs->import_status = 'Completed-Read';
        $wcnotifs->save(false);
    endif;
    $import_result = $session->hasFlash('WooCommerce Import');
    if ($import_result == 1):
        $notify = 'true';
    endif;
    if ($notify == 'true') :
        ?>
        <!--app ui sticky notification check-->
        <script>
            var import_done = '<?php echo $notify; ?>';
            setTimeout(function () {
                jQuery(function () {
                    sticky_ntf_bigcommerce_import(import_done, 'WooCommerce')
                });
            }, 3000);
        </script>
        <?php
    endif;


    $channel_conn = ChannelConnection::find()->Where(['user_id' => Yii::$app->user->identity->id])->one();
    $notify_chnl = 'false';
    if (!empty($channel_conn) && $channel_conn->import_status == 'Completed'):
        $channel_name = Channels::find()->Where(['channel_ID' => $channel_conn->channel_id])->one();
        $channel_name_arr = explode("_", $channel_name->parent_name);
        $session = Yii::$app->session;
        $session->setFlash('WeChat_import', 'You have successfully imported.');
        $channel_conn->import_status = 'Completed-Read';
        $channel_conn->save(false);
    endif;
    $import_result = $session->hasFlash('WeChat_import');
    if ($import_result == 1):
        $notify_chnl = 'true';
    endif;
    if ($notify_chnl == 'true') :
        ?>
        <!--app ui sticky notification check-->
        <script>
            var import_done = '<?php echo $notify_chnl; ?>';
            var import_channel = '<?php echo $channel_name_arr[1]; ?>';
            setTimeout(function () {
                jQuery(function () {
                    sticky_ntf_bigcommerce_import(import_done, import_channel)
                });
            }, 3000);

        </script>
        <?php
    endif;

endif;


$store_connection = StoresConnection::find()->where(['user_id' => $users_Id])->with('storesDetails')->all();
$channel_connection = ChannelConnection::find()->where(['user_id' => $users_Id, 'connected' => 'yes'])->all();
$connections = array();
foreach ($store_connection as $con) {
    $store_country = @$con->storesDetails->country;
    $store_connection_id = $con->stores_connection_id;
    $store = Stores::find()->select(['store_name', 'store_image', 'store_id'])->where(['store_id' => $con->store_id])->one();
    $arr_store_channels[] = array("id" => $store_connection_id, "text" => $store->store_name . ' ' . $store_country);
}
foreach ($channel_connection as $con1) {
    $channel = Channels::find()->select(['channel_name', 'channel_image', 'parent_name', 'channel_ID'])->where(['channel_id' => $con1->channel_id])->one();
    $p_name = explode("_", $channel->parent_name);
    if ($p_name[1] != 'Lazada') {
        $arr_store_channels[] = array("id" => $channel->channel_ID, "text" => $p_name[1]);
    } else {
        $p_name = $channel->channel_name;
        $arr_store_channels[] = array("id" => $channel->channel_ID, "text" => $p_name);
    }
}

$all_orders = Orders::find()->all();
$all_customers = CustomerUser::find()->all();
$all_products = Products::find()->all();
?>

<!--Flash Page Header Notification Start-->
<?php
$flash_msg = '';
$cls_header_title = 'show';
$cls_header_flash = 'hide';
if (Yii::$app->session->hasFlash('success')):
    $flash_msg = Yii::$app->session->getFlash('success');
    $cls_header_title = 'hide';
    $cls_header_flash = 'show';
endif;
?>
<?php
if (Yii::$app->session->hasFlash('warning')):
    $flash_msg = Yii::$app->session->getFlash('warning');
    $cls_header_title = 'hide';
    $cls_header_flash = 'show';
endif;
?>
<?php
if (Yii::$app->session->hasFlash('danger')):
    $flash_msg = Yii::$app->session->getFlash('danger');
    $cls_header_title = 'hide';
    $cls_header_flash = 'show';
endif;
?>
<?php
if (Yii::$app->session->hasFlash('info')):
    $flash_msg = Yii::$app->session->getFlash('info');
    $cls_header_title = 'hide';
    $cls_header_flash = 'show';
endif;
?>

<!--Flash Page Header Notification End-->
<!--Store Connection Check Start-->
<?php
$store_connection = StoresConnection::find()->where(['user_id' => $users_Id])->one();
if (!empty($store_connection)):
    $store_id = $store_connection->store_id;
    $store = Stores::find()->where(['store_id' => $store_id])->one();
    if (!empty($store)):
        $store_name = $store->store_name;
    endif;
endif;
//Save the Notifications for Trail Period & User Subscription Plan
if ($trial_days_status == 'activate'):
    $get_prev_notf = Notification::find()->Where(['notif_type' => 'trial_period'])->one();
    if (empty($get_prev_notf)):
    // $notf_model = new Notification;
    // $notf_model->user_id = Yii::$app->user->identity->id;
    // $notf_model->notif_type = 'trial_period';
    // $des = $remaining_day . ' days trial period is remaining';
    // $notf_model->notif_description = $des;
    // $notf_model->created_at = date('Y-m-d h:i:s', time());
    // $notf_model->save(false);
    else:
    // $des = $remaining_day . ' days trial period is remaining';
    // $get_prev_notf->notif_description = $des;
    // $get_prev_notf->notif_status = 'UnRead';
    // $get_prev_notf->save(false);
    endif;
else:
    $user_data_plan = User::find()->where(['id' => Yii::$app->user->identity->id])->one();
    $des = $user_data_plan->subscription_plan_id . ' Subscription plan is activated';
    $get_prev_notf = Notification::find()->Where(['notif_type' => 'user_plan'])->one();
    if (empty($get_prev_notf)):
        $notf_model = new Notification;
        $notf_model->user_id = Yii::$app->user->identity->id;
        $notf_model->notif_type = 'user_plan';
        $notf_model->notif_description = $des;
        $notf_model->created_at = date('Y-m-d h:i:s', time());
        $notf_model->save(false);
    else:
        $get_prev_notf->notif_description = $des;
        $get_prev_notf->notif_status = 'UnRead';
        $get_prev_notf->save(false);
    endif;
endif;

//For Big Commerce Notification
$notif_type = 'BigCommerce';
$notif_db = Notification::find()->Where(['notif_type' => $notif_type])->one();

//Get all Notifications
$new_notifs = Notification::find()->Where(['notif_status' => 'UnRead'])->orderBy(['created_at' => SORT_DESC])->all();
?>



<!--Store Connection Check End-->
<nav class="navbar navbar-default navbar-fixed-top be-top-header">
    <div class="container-fluid">
        <div class="navbar-header"><a href="<?php echo $basepath_backend; ?>" class="navbar-brand"></a>
        </div>
        <div class="be-right-navbar">

            <ul class="nav navbar-nav navbar-right be-user-nav">
                <?php if (!empty(Yii::$app->user->identity->profile_img)) { ?>
                    <li class="dropdown"><a href="javascript:" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><img src="<?php echo $baseurl_profile_images . '/' . Yii::$app->user->identity->profile_img; ?>" alt="Avatar"><span class="user-name"><?php echo $user_name; ?></span></a>
                        <?php
                    } else {
                        ?> 
                    <li class="dropdown acount-dropdown"><a href="javascript:" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><img src="<?php echo $basepath; ?>/img/avatar-150.png" alt="Avatar"><span class="user-name"><?php echo $user_name; ?></span></a>
                    <?php } ?>
                    <ul role="menu" class="dropdown-menu">
                        <li>
                            <div class="user-info">
                                <div class="user-name captialize"><?php echo $user_name; ?></div>
                                <div class="user-position online">Available</div>
                            </div>
                        </li>
                        <li><a href="<?php $basepath_backend1 ?>/profile"><span class="icon mdi mdi-face"></span> Account</a></li>
                        <li><a href="/change-password"><span class="icon mdi mdi-settings"></span>Change Password</a></li>
                        <li><a href="<?php $basepath_backend1; ?>/logout" data-method="Post"><span class="icon mdi mdi-power"></span> Logout</a></li>
                    </ul>
                </li>
            </ul>

            <div class="page-title">
                <span class="flash-message title <?= $cls_header_title ?>"><?php echo $page_header; ?></span>
                <span class="flash-message msg <?= $cls_header_flash ?>"><?php echo $flash_msg; ?></span>
            </div>
            <ul class="elliot_notif nav navbar-nav navbar-right be-icons-nav">
                <li class="dropdown notif"><a href="javascript:" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle">
                        <span class="icon mdi mdi-notifications" style="<?php
                        if (!empty($new_notifs)) {
                            echo 'color:#4285f4;';
                        } else {
                            echo 'color:#737373;';
                        }
                        ?>"></span>
                        <?php if (!empty($new_notifs)): ?>
                            <span class="indicator"></span>
                        <?php endif; ?>
                    </a>
                    <ul class="dropdown-menu be-notifications">
                        <li>
                            <div class="title">Notifications<span class="badge"><?= count($new_notifs) ?></span></div>
                            <div class="list">
                                <div class="be-scroller">
                                    <div class="content">
                                        <ul>
                                        <?php foreach ($new_notifs as $notif): ?>
                                                <li id="li_<?= $notif->id; ?>" class="notification notification-unread">
                                                    <a href="javascript: ">
                                                        <div class="notification-info">
                                                            <div class="text">
                                                                <?php echo strrpos($notif->notif_description, ".") ? $notif->notif_description : $notif->notif_description . '.'; ?>
                                                                <span id="<?= $notif->id ?>" class="mdi mdi-check-circle notif_check" title="Click to clear the notification"></span>
                                                            </div>
                                                        </div>
                                                    </a>

                                                </li>
                                        <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!--<div class="footer"> <a href="javascript:">View all notifications</a></div>-->
                        </li>
                    </ul>
                </li>
                <?php if (Yii::$app->user->identity->role != 'superadmin') { ?>
                    <li class="dropdown connect"><a href="javascript:" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><span class="icon mdi mdi-apps"></span></a>
                        <ul class="dropdown-menu be-connections test tc-scroll">
                            <li>
                                <div class="list">
                                    <div class="content">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <a href="/stores/bigcommerce" class="connection-item">
                                                    <img src="/img/bigcommerce.png" alt="Github">
                                                    <span>BigCommerce</span>
                                                    <?php
                                                    $store_details = Stores::find()->where(['store_name' => 'BigCommerce'])->one();
                                                    $store_count = StoresConnection::find()->where(['store_id' => $store_details->store_id])->all();
                                                    if (!empty($store_count)) {
                                                        ?>
                                                        <span class="label label-success span-top-22" style="display: inline-block;margin-right: 8px;">Connected</span><span class="label label-success span-top-22" style="display: inline-block;"><?php echo count($store_count); ?></span>
                                        <?php }
                                        ?>
                                                </a>
                                            </div>
                                            <div class="col-sm-6">
                                                <a href="/stores/magento" class="connection-item">
                                                    <img src="/img/magento.png" alt="Bitbucket">
                                                    <span>Magento</span>
                                                    <?php
                                                    $store_details = Stores::find()->where(['store_name' => 'Magento'])->one();
                                                    $store_count = StoresConnection::find()->where(['store_id' => $store_details->store_id])->all();
                                                    if (!empty($store_count)) {
                                                        ?>
                                                        <span class="label label-success span-top-22" style="display: inline-block;margin-right: 8px;">Connected</span><span class="label label-success span-top-22" style="display: inline-block;"><?php echo count($store_count); ?></span>
                                        <?php }
                                        ?>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <a href="/magento2" class="connection-item">
                                                    <img src="/img/magento2.png" alt="Github">
                                                    <span>Magento2</span>
                                                    <?php
                                                    $store_details = Stores::find()->where(['store_name' => 'Magento2'])->one();
                                                    $store_count = StoresConnection::find()->where(['store_id' => $store_details->store_id])->all();
                                                    if (!empty($store_count)) {
                                                        ?>
                                                        <span class="label label-success span-top-22" style="display: inline-block;margin-right: 8px;">Connected</span><span class="label label-success span-top-22" style="display: inline-block;"><?php echo count($store_count); ?></span>
                                                <?php }
                                                ?>
                                                </a>
                                            </div>
                                            
                                            <div class="col-sm-6">
                                                <a href="/stores/shopify" class="connection-item">
                                                    <img src="/img/shopify.png" alt="Github">
                                                    <span>Shopify</span>
                                                    <?php
                                                    $store_details = Stores::find()->where(['store_name' => 'Shopify'])->one();
                                                    $store_count = StoresConnection::find()->where(['store_id' => $store_details->store_id])->all();
                                                    if (!empty($store_count)) {
                                                        ?>
                                                        <span class="label label-success span-top-22" style="display: inline-block;margin-right: 8px;">Connected</span><span class="label label-success span-top-22" style="display: inline-block;"><?php echo count($store_count); ?></span>
                                            <?php }
                                            ?>
                                                </a>
                                            </div>
                                            
                                        </div>
<!--                                        <div class="row">
                                            <div class="col-sm-6">
                                                <a href="javascript:void(0)" class="connection-item">
                                                    <img src="/img/oraclecommercecloud.png" alt="Github">
                                                    <span>Oracle Commerce Cloud</span>
                                                    <?php /*
                                                    $store_details = Stores::find()->where(['store_name' => 'Oracle Commerce Cloud'])->one();
                                                    $store_count = StoresConnection::find()->where(['store_id' => $store_details->store_id])->all();
                                                    if (!empty($store_count)) {
                                                        ?>
                                                        <span class="label label-success span-top-22" style="display: inline-block;margin-right: 8px;">Connected</span><span class="label label-success span-top-22" style="display: inline-block;"><?php echo count($store_count); ?></span>
                                            <?php }*/
                                            ?>
                                                </a>
                                            </div>
                                            <div class="col-sm-6">
                                                <a href="javascript:void(0)" class="connection-item">
                                                    <img src="/img/netsuite.png" alt="Bitbucket">
                                                    <span>NetSuite SuiteCommerce</span>
                                                    <?php /*
                                                    $store_details = Stores::find()->where(['store_name' => 'NetSuite SuiteCommerce'])->one();
                                                    $store_count = StoresConnection::find()->where(['store_id' => $store_details->store_id])->all();
                                                    if (!empty($store_count)) {
                                                        ?>
                                                        <span class="label label-success span-top-22" style="display: inline-block;margin-right: 8px;">Connected</span><span class="label label-success span-top-22" style="display: inline-block;"><?php //echo count($store_count); ?></span>
                                                    <?php } */
                                                    ?>
                                                </a>
                                            </div>
                                        </div>-->
                                        
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <a href="/stores/shopify" class="connection-item">
                                                    <img src="/img/shopifyplus.png" alt="Bitbucket">
                                                    <span>ShopifyPlus</span>
                                                    <?php
                                                    $store_details = Stores::find()->where(['store_name' => 'ShopifyPlus'])->one();
                                                    $store_count = StoresConnection::find()->where(['store_id' => $store_details->store_id])->all();
                                                    if (!empty($store_count)) {
                                                        ?>
                                                        <span class="label label-success span-top-22" style="display: inline-block;margin-right: 8px;">Connected</span><span class="label label-success span-top-22" style="display: inline-block;"><?php echo count($store_count); ?></span>
                                            <?php }
                                            ?>
                                                </a>
                                            </div>
                                            <div class="col-sm-6">
                                                <a href="/stores/woocommerce" class="connection-item">
                                                    <img src="/img/woocommerce.png" alt="Bitbucket">
                                                    <span>WooCommerce</span>
                                                    <?php
                                                    $store_details = Stores::find()->where(['store_name' => 'WooCommerce'])->one();
                                                    $store_count = StoresConnection::find()->where(['store_id' => $store_details->store_id])->all();
                                                    if (!empty($store_count)) {
                                                        ?>
                                                        <span class="label label-success span-top-22" style="display: inline-block;margin-right: 8px;">Connected</span><span class="label label-success span-top-22" style="display: inline-block;"><?php echo count($store_count); ?></span>
                                            <?php }
                                            ?>
                                                </a>
                                            </div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <a href="/vtex" class="connection-item">
                                                    <img src="/img/vtex.png" alt="Bitbucket">
                                                    <span>VTEX</span>
                                                    <?php
                                                    $store_details = Stores::find()->where(['store_name' => 'VTEX'])->one();
                                                    $store_count = StoresConnection::find()->where(['store_id' => $store_details->store_id])->all();
                                                    if (!empty($store_count)) {
                                                        ?>
                                                        <span class="label label-success span-top-22" style="display: inline-block;margin-right: 8px;">Connected</span><span class="label label-success span-top-22" style="display: inline-block;"><?php echo count($store_count); ?></span>
                                            <?php }
                                            ?>
                                                </a>
                                            </div> 
                                            
<!--                                            <div class="col-sm-6">
                                                <a href="javascript:void(0)" class="connection-item">
                                                    <img src="/img/salesforcecommercecloud.png" alt="Salesforce Commerce Cloud">
                                                    <span>Salesforce Commerce Cloud</span>
                                                    <?php /*
                                                    $store_details = Stores::find()->where(['store_name' => 'Salesforce Commerce Cloud'])->one();
                                                    $store_count = StoresConnection::find()->where(['store_id' => $store_details->store_id])->all();
                                                    if (!empty($store_count)) {
                                                  */  ?>
                                                        <span class="label label-success span-top-22" style="display: inline-block;margin-right: 8px;">Connected</span><span class="label label-success span-top-22" style="display: inline-block;"><?php echo count($store_count); ?></span>
                                            <?php /*} */?>
                                                </a>
                                            </div>-->
                                            
                                        </div>

                                    </div>
                                </div>
                                <div class="footer"> <a href="/channels/create">Connect to Marketplaces</a></div>
                            </li>
                        </ul>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</nav>


<?php
$facebook = Channels::find()->where(['channel_name' => 'Facebook'])->one();
//echo'<pre>';
//print_r($lazada);die;
if (isset($facebook) and ! empty($facebook)) {

    $channel_id = $facebook->channel_ID;
//echo $channel_id;die;
    $fb_connected = ChannelConnection::find()->Where(['user_id' => Yii::$app->user->identity->id, 'channel_id' => $channel_id, 'connected' => 'yes'])->one();
}

$google_enable = User::find()->where(['id' => $user_id, 'google_feed' => 'yes'])->one();
?>

<div class="be-left-sidebar">
    <div class="left-sidebar-wrapper"><a href="javascript:" class="left-sidebar-toggle">Dashboard</a>
        <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">
                <div class="left-sidebar-content">
                    <ul class="sidebar-elements">
<?php if (Yii::$app->user->identity->role == 'superadmin') { ?>
                            <li><a href="/"><i class="icon mdi mdi-home"></i><span>Dashboard</span></a>
                            <li><a href="/channels/wechatconnect"><i class="icon mdi mdi-collection-item"></i><span>WeChat</span></a>    
                            <li><a href="/facebook/flipkart"><i class="icon mdi mdi-collection-item"></i><span>Flipkart</span></a>    
<?php } else { ?>
                            <li class="divider">Menu</li>
                            <li><a href="/"><i class="icon mdi mdi-home"></i><span>Dashboard</span></a></li>
                            <li class="parent"><a href="javascript:"><i class="icon mdi mdi-face"></i><span>People</span></a>
                                <ul class="sub-menu">
                                    <li><a href="/people">View All</a> </li>
                                    <li><a href="/people/create">Add New</a></li>
                                            <?php if (isset($all_customers) && $all_customers) { ?>
                                        <li class="parent"><a href="javascript:"></i><span>Connected Channels</span></a>
                                            <ul class="sub-menu">
                                                <?php
                                                if (isset($arr_store_channels)):
                                                    foreach ($arr_store_channels as $sc):
                                                        $show_people = '';
                                                        $channel_name_explode = explode(" ", $sc['text']);
                                                        $channel_name_check = $channel_name_explode[0];
                                                        $check_valid_store = Stores::find()->where(['store_name' => $channel_name_check])->one();
                                                        if (!empty($check_valid_store)) {
                                                            $fin_channel = $channel_name_check;
                                                            $channel_space_pos = strpos($sc['text'], ' ');
                                                            $country_sub_str = substr($sc['text'], $channel_space_pos);
                                                            $store_country = trim($country_sub_str);
                                                            $store_details_data = StoreDetails::find()->Where(['channel_accquired' => $fin_channel, 'country' => $store_country])->with('storeConnection')->one();
                                                            if (!empty($store_details_data)) {
                                                                $store_connection_id = $store_details_data->store_connection_id;
                                                                $customer_abbrivation = CustomerAbbrivation::find()->Where(['mul_store_id' => $store_connection_id])->one();
                                                                if (!empty($customer_abbrivation)) {
                                                                    $show_people = $sc['text'];
                                                                    $inactive_users = CustomerUser::find()->where(['channel_acquired' => $fin_channel, 'people_visible_status' => 'in_active'])->joinWith('customerabbrivation')->andWhere(['customer_abbrivation.mul_store_id' => $store_connection_id])->one();
                                                                    ?>
                                                                    <li class="parent"><a href="javascript:"></i><span><?= $show_people ?></span></a>
                                                                        <ul class="sub-menu">
                                                                            <li><a href="/people/connected-customer?customers=<?= $sc['text']; ?>"><i class="icon mdi mdi-undefined"></i><span>View All</span></a></li>
                                                                            <?php if (!empty($inactive_users)) { ?>
                                                                                <li><a href='/people/inactive-customers/?customers=<?= $sc['text'] ?>'><i class="icon mdi mdi-undefined"></i><span>Hidden People</span></a></li>
                                                                    <?php } ?>
                                                                        </ul>
                                                                    </li>
                                                                    <?php
                                                                }
                                                            }
                                                        } else {
                                                            $fin_channel = $channel;
                                                            if ($fin_channel != 'google-shopping' || $fin_channel != 'Facebook') {
                                                                $users_data = CustomerUser::find()->where(['channel_acquired' => $sc['text']])->one();
                                                                if (!empty($users_data)) {
                                                                    $show_people = $sc['text'];
                                                                    $inactive_users = CustomerUser::find()->where(['channel_acquired' => $sc['text'], 'people_visible_status' => 'in_active'])->one();
                                                                    ?>
                                                                    <li class="parent"><a href="javascript:"></i><span><?= $show_people ?></span></a>
                                                                        <ul class="sub-menu">
                                                                            <li><a href="/people/connected-customer?customers=<?= $sc['text']; ?>"><i class="icon mdi mdi-undefined"></i><span>View All</span></a></li>
                                                                            <?php if (!empty($inactive_users)) { ?>
                                                                                <li><a href='/people/inactive-customers/?customers=<?= $sc['text'] ?>'><i class="icon mdi mdi-undefined"></i><span>Hidden People</span></a></li>
                                                                    <?php } ?>
                                                                        </ul>
                                                                    </li>
                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                        ?>

            <?php endforeach;
        endif;
        ?>
                                            </ul>
                                        </li>
    <?php } ?>
                                </ul>
                            </li>
                            <li class="parent"><a href="javascript:"><i class="icon mdi mdi-collection-item"></i><span>Products</span></a>
                                <ul class="sub-menu">
                                    <li><a href="/products">View All</a></li>
                                    <li><a href="/products/create">Add New</a></li>
                                    <?php
                                    if (isset($all_products) && $all_products) {
                                        $channel_IDs = array();
                                        $final_arr = array();
                                        ?>
                                        <li class="parent"><a href="javascript:"></i><span>Connected Channels</span></a>
                                            <ul class="sub-menu">
                                                <?php
                                                if (isset($arr_store_channels)):
                                                    $inactive_products = Products::find()->where(['product_status' => 'in_active', 'permanent_hidden' => 'active'])->with(['productChannels'])->all();
                                                    foreach ($inactive_products as $prod) {
                                                        $channel_IDs[] = $prod->productChannels;
                                                    }
                                                    foreach ($channel_IDs as $ganpati) {
                                                        foreach ($ganpati as $bappa) {
                                                            $final_arr[] = $bappa->store_id;
                                                            $final_arr[] = $bappa->channel_id;
                                                        }
                                                    }
                                                    ?>
                                                            <?php foreach ($arr_store_channels as $sc): ?> 
                                                        <li class="parent"><a href="javascript:"></i><span><?php echo ucwords(str_replace("-", " ", $sc['text'])); ?></span></a>
                                                            <ul class="sub-menu">
                                                                <?php
                                                                if ($sc['text'] == 'google-shopping') {
                                                                    $connected_product_link = 'Google Shopping';
                                                                } else {
                                                                    $connected_product_link = $sc['text'];
                                                                }
                                                                ?>
                                                                <li><a href="/products/connected-products?product=<?= $connected_product_link ?>"><i class="icon mdi mdi-undefined"></i><span>View All</span></a></li>
                                                                <?php
                                                                if (in_array($sc['id'], $final_arr)) {
                                                                    if ($sc['text'] == 'google-shopping') {
                                                                        $inactive_product_link = 'Google Shopping';
                                                                    } else {
                                                                        $inactive_product_link = $sc['text'];
                                                                    }
                                                                    ?>
                                                                    <li><a href="/products/inactive-products/?product=<?= $inactive_product_link ?>"><i class="icon mdi mdi-undefined"></i><span>Hidden Products </span></a></li>                        
                                                <?php } ?>
                                                            </ul>
            <?php endforeach;
        endif;
        ?>
                                            </ul>
                                        </li>  
    <?php } ?>
                                    <li class="parent"><a href="javascript:"></i><span>Attributes</span></a>
                                        <ul class="sub-menu">
                                            <li><a href="/attributes"><i class="icon mdi mdi-undefined"></i><span>View All</span></a></li>
                                            <li><a href="/attributes/create"><i class="icon mdi mdi-undefined"></i><span>Add New</span></a></li>
                                            <li class="parent"><a href="javascript:"></i><span>Attribute Types</span></a>
                                                <ul class="sub-menu">
                                                    <li><a href="/attribute-type"><i class="icon mdi mdi-undefined"></i><span>View All</span></a></li>
                                                    <li><a href="/attribute-type/create"><i class="icon mdi mdi-undefined"></i><span>Add New</span></a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="parent"><a href="javascript:"></i><span>Categories</span></a>
                                        <ul class="sub-menu">
                                            <li><a href="/categories"><i class="icon mdi mdi-undefined"></i><span>View All</span></a></li>
                                            <li><a href="/categories/create"><i class="icon mdi mdi-undefined"></i><span>Add New</span></a></li>
                                        </ul>
                                    </li>
                                    <li class="parent"><a href="javascript:"></i><span>Variations</span></a>
                                        <ul class="sub-menu">
                                            <li><a href="/variations"><i class="icon mdi mdi-undefined"></i><span>View All</span></a></li>
                                            <li><a href="/variations/create"><i class="icon mdi mdi-undefined"></i><span>Add New</span></a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li class="parent"><a href="javascript:"><i class="icon mdi mdi-receipt"></i><span>Orders</span></a>
                                <ul class="sub-menu">
                                    <li><a href="/orders">View All</a>
                                    </li>
                                            <?php if (isset($all_orders) && $all_orders) { ?>
                                        <li class="parent"><a href="javascript:"></i><span>Connected Channels</span></a>
                                            <ul class="sub-menu">
                                                <?php
                                                if (isset($arr_store_channels)):
                                                    $channels_ID = array();
                                                    $final = array();
                                                    $inactive_orders = Orders::find()->where(['order_visible_status' => 'in_active'])->with(['orderChannels'])->all();
                                                    foreach ($inactive_orders as $orders) {
                                                        $channels_ID[] = $orders->orderChannels;
                                                    }
                                                    foreach ($channels_ID as $array) {
                                                        foreach ($array as $val) {
                                                            // echo $val->created_at;
                                                            $final[] = $val->store_id;
                                                            $final[] = $val->channel_id;
                                                        }
                                                    }
                                                    ?>
                                                    <?php
                                                    foreach ($arr_store_channels as $sc):
                                                        $order_data = OrderChannel::find()->where(['store_id' => $sc['id']])->orWhere(['channel_id' => $sc['id']])->one();
                                                        if (!empty($order_data)):
                                                            if ($sc['text'] == 'google-shopping' || $sc['text'] == "Facebook"):
                                                            else:
                                                                ?>
                                                                <li class="parent"><a href="javascript:"></i><span><?php echo $sc['text']; ?></span></a>
                                                                    <ul class="sub-menu">
                                                                        <li><a href="/orders/connected?orders=<?= $sc['text']; ?>"><i class="icon mdi mdi-undefined"></i><span>View All</span></a></li>
                                                                    <?php if (in_array($sc['id'], $final)) { ?>
                                                                            <li><a href="/orders/inactive-orders/?orders=<?php echo $sc['text']; ?>"><i class="icon mdi mdi-undefined"></i><span>Hidden Orders </span></a></li>
                        <?php } ?>
                                                                    </ul>
                                                    <?php
                                                    endif;
                                                endif;
                                            endforeach;
                                        endif;
                                        ?>
                                            </ul>
                                        </li>
    <?php } ?>
                                </ul>
                            </li>

                            <li class="parent"><a href="javascript:"><i class="icon mdi mdi-collection-image-o"></i><span>Content</span></a>
                                <ul class="sub-menu">
                                    <li><a href="/content/index">View All</a>
                                    </li>
                                    <li><a href="/content/create">Add New</a>
                                    </li>
    <!--                                <li class="parent"><a href="javascript:"></i><span>Categories</span></a>
                                        <ul class="sub-menu">
                                            <li><a href="javascript:"><i class="icon mdi mdi-undefined"></i><span>View All</span></a></li>
                                            <li><a href="javascript:"><i class="icon mdi mdi-undefined"></i><span>Add New</span></a></li>
                                        </ul>
                                    </li>-->
    <!--                                <li class="parent"><a href="javascript:"></i><span>Tags</span></a>
                                        <ul class="sub-menu">
                                            <li><a href="javascript:"><i class="icon mdi mdi-undefined"></i><span>View All</span></a></li>
                                            <li><a href="javascript:"><i class="icon mdi mdi-undefined"></i><span>Add New</span></a></li>
                                        </ul>
                                    </li>-->
                                </ul>
                            </li>

                            <li class="parent"><a href="javascript:"><i class="icon mdi mdi-settings"></i><span>Settings</span></a>
                                <ul class="sub-menu">

                                    <li><a href="<?php $basepath_backend1 ?>/general">General</a></li>
                                    <li><a href="/documents">Corporate Documents</a></li>
    <?php if (Yii::$app->user->identity->role == 'merchant'): ?>
                                        <li class="parent"><a href="javascript:"></i><span>User Management</span></a>
                                            <ul class="sub-menu">
                                                <li><a href="/user"><i class="icon mdi mdi-undefined"></i><span>View All</span></a></li>
                                                <li><a href="/user/create"><i class="icon mdi mdi-undefined"></i><span>Add New</span></a></li>
                                            </ul>
                                        </li>
    <?php endif; ?>

                                    <!--START INTEGRATIONS!-->
                                    <li class="parent"><a href="javascript:"></i><span>Integrations</span></a>
                                        <ul class="sub-menu">
                                            <!--*********DO NOT DELETE THESE MENUS THEY ARE COMMENTED FOR A REASON*************-->

            <!--<li class="parent"><a href="javascript:"><i class="icon mdi mdi-undefined"></i><span>Accounting</span></a>
            <ul class="sub-menu">
            <li><a href="/integrations/accounting"><i class="icon mdi mdi-undefined"></i><span>View All</span></a></li>
            </ul>
            </li>-->
            <!--<li class="parent"><a href="javascript:"><i class="icon mdi mdi-undefined"></i><span>Amazon</span></a>
            <ul class="sub-menu">
            <li><a href="javascript:"><i class="icon mdi mdi-undefined"></i><span>Alexa Skills</span></a></li>
            </ul>
            </li>-->
                                            <!--Email !-->
                                            <!--<li class="parent"><a href="javascript:"><i class="icon mdi mdi-undefined"></i><span>Email</span></a>
                                                <ul class="sub-menu">
                                                    <li><a href="/integrations/email"><i class="icon mdi mdi-undefined"></i><span>View All </span></a></li>
                                                    <li><a href="javascript:"><i class="icon mdi mdi-undefined"></i><span>Constant Contact</span></a></li>
                                                    <li><a href="javascript:"><i class="icon mdi mdi-undefined"></i><span>MailChimp</span></a></li>
                                                    <li><a href="javascript:"><i class="icon mdi mdi-undefined"></i><span>Windsor Circle</span></a></li>
                                                </ul>
                                            </li>-->  

                                            <!--*********DO NOT DELETE THESE MENUS THEY ARE COMMENTED FOR A REASON*************-->

                                            <li class="parent"><a href="javascript:"><i class="icon mdi mdi-undefined"></i><span>ERP</span></a>
                                                <ul class="sub-menu">
                                                    <li><a href="/integrations/erplist"><i class="icon mdi mdi-undefined"></i><span>View All</span></a></li>
                                                    <li><a href="/integrations/netsuite"><i class="icon mdi mdi-undefined"></i><span>NetSuite</span></a></li>
                                                    <!-- <li><a href="javascript:"><i class="icon mdi mdi-undefined"></i><span>Sage</span></a></li>
                                                    <li><a href="javascript:"><i class="icon mdi mdi-undefined"></i><span>SAP</span></a></li> -->
                                                </ul>
                                            </li>
                                            <!--*********DO NOT DELETE THESE MENUS THEY ARE COMMENTED FOR A REASON*************-->        

                            <!--<li class="parent"><a href="javascript:"><i class="icon mdi mdi-undefined"></i><span>Google</span></a>
                                <ul class="sub-menu">
                                    <li><a href="javascript:"><i class="icon mdi mdi-undefined"></i><span>AdWords</span></a></li>
                                    <li><a href="javascript:"><i class="icon mdi mdi-undefined"></i><span>Analytics</span></a></li>
                                    <li><a href="javascript:"><i class="icon mdi mdi-undefined"></i><span>Optimize</span></a></li>
                                    <li><a href="javascript:"><i class="icon mdi mdi-undefined"></i><span>Tag Manager</span></a></li>
                                </ul>
                            </li>-->


            <!--<li class="parent"><a href="javascript:"><i class="icon mdi mdi-undefined"></i><span>Facebook</span></a>
                <ul class="sub-menu">
                    <li><a href="javascript:"><i class="icon mdi mdi-undefined"></i><span>Facebook/Instagram Advertising</span></a></li>
                </ul>
            </li>-->

                                            <li class="parent"><a href="javascript:"><i class="icon mdi mdi-undefined"></i><span>POS</span></a>
                                                <ul class="sub-menu">
                                                    <li><a href="/integrations/pos-all"><i class="icon mdi mdi-undefined"></i><span>View All</span></a></li>
                                                    <li><a href="/integrations/square"><i class="icon mdi mdi-undefined"></i><span>Square</span></a></li>
                                                </ul>
                                            </li>
                                            <!--*********DO NOT DELETE THESE MENUS THEY ARE COMMENTED FOR A REASON*************-->                                    
                                            <li class="parent"><a href="<?php $basepath_backend1 ?>/translations"><i class="icon mdi mdi-undefined"></i><span>Translations</span></a>
                                                <ul class="sub-menu">
                                                    <li><a href="/integrations/translation-all"><i class="icon mdi mdi-undefined"></i><span>View All</span></a></li>
                                                    <li><a href="<?php $basepath_backend1 ?>/integrations/smartling"><i class="icon mdi mdi-undefined"></i><span>Smartling</span></a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <!--END INTEGRATIONS!-->

                                    <li class="parent"><a href="javascript:"></i><span>Channels</span></a>
                                        <ul class="sub-menu">
                                            <?php
                                            if ((Yii::$app->controller->id == 'integrations' and Yii::$app->controller->action->id == 'facebook' and empty($fb_connected)) or ( Yii::$app->controller->id == 'google-shopping' and empty($google_enable))) {
                                                $view_all_class = 'class="active"';
                                            }
                                            ?>
                                            <li <?php echo @$view_all_class; ?>><a href="/channels"><i class="icon mdi mdi-undefined"></i><span>View All</span></a></li>
                                            <li><a href="/channels/create"><i class="icon mdi mdi-undefined"></i><span>Add New</span></a></li>
                                            <?php
                                            if ((Yii::$app->controller->id == 'integrations' and Yii::$app->controller->action->id == 'facebook' and ! empty($fb_connected)) or ( Yii::$app->controller->id == 'google-shopping' and ! empty($google_enable))) {
                                                $connected_channels_class = 'class="active"';
                                            }
                                            ?>

                                            <?php
                                            $users_Id = Yii::$app->user->identity->id;
                                            $store_connection = StoresConnection::find()->where(['user_id' => $users_Id])->all();
                                            if (!empty($store_connection)) {
                                                foreach ($store_connection as $con) {
                                                    $store = StoreDetails::find()->select(['channel_accquired', 'country'])->where(['store_connection_id' => $con->stores_connection_id])->one();
                                                    if (!empty($store)) {
                                                        $arr[] = array("value" => $store->channel_accquired . ' ' . $store->country, "text" => $store->channel_accquired, 'id' => $con->store_id, 'mul_store_id' => $con->stores_connection_id, 'type' => 'store');
                                                    }
                                                }
                                            }
                                            $channel_connection = ChannelConnection::find()->where(['user_id' => $users_Id, 'graph_status' => 'yes', 'connected' => 'yes'])->all();
                                            $channel_connection_2 = ChannelConnection::find()->all();
                                            $connections = array();
                                            if (!empty($channel_connection)) {
                                                foreach ($channel_connection as $con1) {
                                                    $channel = Channels::find()->select(['channel_name', 'channel_image', 'parent_name'])->where(['channel_id' => $con1->channel_id])->one();
                                                    $p_name = explode("_", $channel->parent_name);
                                                    $name = ucwords(str_replace("-", " ", $p_name[1]));

                                                    if ($name == 'Lazada') {
                                                        $name = $channel->channel_name;
                                                    }
                                                    $arr[] = array("value" => $name, "text" => $name, "id" => $con1->channel_id, 'mul_store_id' => '', 'type' => 'channel');
                                                }
                                            }


                                            //echo '<pre>'; print_r($arr); echo '</pre>';
                                            if (!empty($arr)) {
                                                ?>
                                                <li class="parent" <?php echo @$connected_channels_class ?>><a href="javascript:"></i><span>Connected Channels</span></a>
                                                    <ul class="sub-menu">
        <?php foreach ($arr as $data) {
            ?>
                                                            <li><a href="/channelsetting/?id=<?php echo $data['id']; ?>&type=<?php echo $data['type'] ?>&mul_store_id=<?php echo $data['mul_store_id'] ?>"></i><span><?php echo $data['value']; ?></span></a>
        <?php } ?>
                                                    </ul>
    <?php } ?>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="parent"><a href="javascript:"></i><span>Fulfillment</span></a>

                                        <ul class="submenu">
                                            <li class="parent"><a href="javascript:"><span>Carriers</span></a>
                                                <ul class="submenu">
                                                    <li><a href="/fulfillment/carriers"><span>View All</span></a></li>
                                                    <!-- code for the connected carrier starts-->
                                                    <?php
                                                    $connected_carrier = Fulfillment::find()->where(['elliot_user_id' => $user_id])->all();
                                                    $fullfilled_carrier = array();
                                                    if (!empty($connected_carrier)) {

                                                        foreach ($connected_carrier as $carr) {
                                                            $fullfilled_carrier[] = $carr->fulfillment_id;
                                                        }
                                                    }
                                                    $Carriers_all = FulfillmentList::find()->where(['type' => 'Carriers'])->all();
                                                    foreach ($Carriers_all as $carrier) {
                                                        if (in_array($carrier->id, $fullfilled_carrier)) {
                                                            ?>
                                                            <li class="parent"><a href="javascript:"><span>Connected Channels</span></a>
                                                                <ul class="sub-menu">
                                                                    <li><a href="<?php echo $carrier->fulfillment_link; ?>"><span><?php echo $carrier->fulfillment_name; ?></span></a></li>
                                                            <?php if ($carrier->fulfillment_name == 'SF Express') { ?>
                                                                        <li><a href="<?php echo '/sfexpress/price' ?>"><span><?php echo 'Price Table' ?></span></a></li>	
                                                            <?php }
                                                            ?>

                                                                </ul></li>

            <?php
        }
    }
    ?>
                                                    <!-- code for the connected carrier ends-->
                                                </ul>

                                            </li>
                                            <li class="parent"><a href="javascript:">Software Partners</a>
                                                <ul class="submenu">
                                                    <li><a href="/fulfillment/software"><span>View All</span></a></li>
                                                    <!-- code for the connected software partners starts-->
                                                    <?php
                                                    $connected_carrier = Fulfillment::find()->where(['elliot_user_id' => $user_id])->all();
                                                    $fullfilled_carrier = array();
                                                    if (!empty($connected_carrier)) {

                                                        foreach ($connected_carrier as $carr) {
                                                            $fullfilled_carrier[] = $carr->fulfillment_id;
                                                        }
                                                    }
                                                    $Carriers_all = FulfillmentList::find()->where(['type' => 'Software'])->all();
                                                    foreach ($Carriers_all as $carrier) {
                                                        if (in_array($carrier->id, $fullfilled_carrier)) {
                                                            ?>
                                                            <li class="parent"><a href="javascript:"><span>Connected Channels</span></a>
                                                                <ul class="sub-menu">
                                                                    <li><a href="<?php echo $carrier->fulfillment_link; ?>"><span><?php echo $carrier->fulfillment_name; ?></span></a></li>
                                                                </ul></li>

            <?php
        }
    }
    ?>


                                                    <!-- code for the connected software partners ends-->
                                                </ul>
                                            </li>
                                        </ul>

                                    </li>
                                    <!--*********DO NOT DELETE THESE MENUS THEY ARE COMMENTED FOR A REASON*************-->

                              <!--<li class="parent"><a href="javascript:"></i><span>Inventory</span></a>
                                  <ul class="sub-menu">
                                      <li><a href="/integrations/inventory"><i class="icon mdi mdi-undefined"></i><span>View All</span></a></li>
                                      <li><a href="javascript:"><i class="icon mdi mdi-undefined"></i><span>Marketplace Subscriptions</span></a></li-->
                                      <!-- <li><a href="<?php $basepath_backend1 ?>/user-subscription"><i class="icon mdi mdi-undefined"></i><span>User Subscriptions</span></a></li> -->
                                    <!--</ul>
                                </li>-->

                                    <li class="parent"><a href="javascript:"></i><span>Billing</span></a>
                                        <ul class="sub-menu">
                                            <li><a href="/orders/invoice"><i class="icon mdi mdi-undefined"></i><span>View Invoices</span></a></li>
                                            <!--li><a href="javascript:"><i class="icon mdi mdi-undefined"></i><span>Marketplace Subscriptions</span></a></li-->
                                            <li><a href="<?php $basepath_backend1 ?>/user-subscription"><i class="icon mdi mdi-undefined"></i><span>User Subscriptions</span></a></li>
                                        </ul>
                                    </li>
                                    <!--*********DO NOT DELETE THESE MENUS THEY ARE COMMENTED FOR A REASON*************-->
                                    <!--<li class="parent"><a href="javascript:"></i><span>Server Settings</span></a>
                          
                          
                          
                                        <ul class="sub-menu">
                                            <li><a href="javascript:"><i class="icon mdi mdi-undefined"></i><span>FTP</span></a></li>
                                            
                                        </ul>
                                    </li>-->
                                </ul>

                            </li>
                            <li class="divider">General</li>
                            <li><a href="https://intercom.help/elliot" target="_blank"><i class="icon mdi mdi-pin-help"></i><span>Help Page</span></a></li>
                            <li><a href="/system-updates"><i class="icon mdi mdi-notifications-active"></i><span>System Updates</span></a></li>
                            <li><a href="/system-status"><i class="icon mdi mdi-info-outline"></i><span>System Status</span></a></li>
                            <li><a href="/terms-conditions"><i class="icon mdi mdi-assignment-check"></i><span>Terms & Conditions</span></a></li>
        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
        <!--for performance meter-->
        <?php
        $order_amount_total = Orders::find()->Where(['elliot_user_id' => Yii::$app->user->identity->id])->sum('total_amount');
        $annual_revenue = Yii::$app->user->identity->annual_revenue;

        if (!empty($annual_revenue) and Yii::$app->user->identity->annual_revenue != '') {
            $fraction = $order_amount_total / $annual_revenue;
            $percentage = $fraction * 100;
            $percentage = number_format((float) $percentage, 0, ".", '');
            ?>
            <div class="progress-widget">
                <div class="progress-data"><span class="progress-value"><?php echo $percentage; ?>%</span><span class="name">Revenue Projection</span></div>
                <div class="progress">
                    <div style="width: <?php echo $percentage; ?>%;" class="progress-bar progress-bar-primary"></div>
                </div>
            </div>

<?php } else { ?>

            <a href="/general"><button class="btn btn-space btn-default"><i class="icon icon-left mdi mdi-globe"></i> Activate Revenue Targeting</button></a>
<?php }
?>


        <!---->
    </div>
</div>
<!--for current Action !-->
<input type="hidden" value="<?= $action; ?>" id="current_action">
<input type="hidden" value="<?= $controller_action_name; ?>" id="current_controller_action">   