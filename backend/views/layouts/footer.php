<?php
use backend\models\CronTasks;
use yii\web\Session;
use backend\models\StoresConnection;
use backend\models\Stores;
use backend\models\Channels; 
use backend\models\ChannelConnection;
/* @var $this yii\web\View */
$basepath=Yii::getAlias('@baseurl');
$user_domain=Yii::$app->user->identity->domain_name;
$elliot_user_id=Yii::$app->user->identity->id;

?>

<div class="row">
    <input type="hidden" value="<?=$user_domain;?>" id="hidden_user_domain">
<script>
    var BASE_URL='<?php echo Yii::$app->params['BASE_URL']?>';
    var DOMAIN_NAME='<?php echo Yii::$app->params['DOMAIN_NAME']?>';
	console.log(DOMAIN_NAME);
        function runwechatimport1(user_id){
			console.log("from footer file first ajax");
            $.ajax({
            type: 'post',
            url: '/channels/wechatimportonlogin',
            data: {
                user_id: user_id,
                task: 'import',
                task_source: 'WeChat',
                
            },
            beforeSend: function (xhr) { 
//                $(".be-wrapper").addClass("be-loading-active");
                    },
                    success: function(value) {
						console.log(value);
						console.log("from footer file");
                        if (value != "error") {
                            $.ajax({
                                type: 'get',
                                url: BASE_URL + 'channels/wechatimport/' + user_id,
                                data: {
                                    type: value,
                                    process: 'backend',
                                },
                                beforeSend: function(xhr) {
                                },
                                success: function(value) {
                                },
                            });
                        }

                    },
                });
            }
            function runVtextImportCron(user_id){
            var BASE_URL='<?php echo Yii::$app->params['BASE_URL']?>';
            var DOMAIN_NAME='<?php echo Yii::$app->params['DOMAIN_NAME']?>';
            $.ajax({
                type: 'get',
                url: BASE_URL + 'vtex/vtex-cron-importing?user_id=' + user_id + '&channel=Vtex',
                data: '',
                beforeSend: function(xhr) {
                },
                success: function(value) {
                    //alert(value);
                },
            });
        }
        function runSquareImportCron(user_id){
            var BASE_URL='<?php echo Yii::$app->params['BASE_URL']?>';
            var DOMAIN_NAME='<?php echo Yii::$app->params['DOMAIN_NAME']?>';
            $.ajax({
                type: 'get',
                url: BASE_URL + 'square/square-cron-importing?user_id=' + user_id + '&channel=Square',
                data: '',
                beforeSend: function(xhr) {
                },
                success: function(value) {
                    //alert(value);
                },
            });
        }
        
        
          function runLazadaImportCron(user_id){
            var BASE_URL='<?php echo Yii::$app->params['BASE_URL']?>';
            var DOMAIN_NAME='<?php echo Yii::$app->params['DOMAIN_NAME']?>';
            $.ajax({
                type: 'get',
                url: BASE_URL + 'lazada/malaysia-cron-importing?user_id=' + user_id + '&channel=Lazada Malaysia',
                data: '',
                beforeSend: function(xhr) {
                },
                success: function(value) {
                    //alert(value);
                },
            });
        }
        </script>
        <?php  
        
        //echo "current time ". time(); //date('Y-m-d h:i:s', time());
        $CronTasks_data = CronTasks::find()->where(['elliot_user_id' => $user_id,'task_source'=>'WeChat','status'=>'Completed'])->one(); 
        if(!empty($CronTasks_data)){
            $datetime1 = new DateTime(date('Y-m-d h:i:s'));
            $datetime2 = new DateTime($CronTasks_data->updated_at);
            $interval = $datetime1->diff($datetime2);
            $time=$interval->format('%i')*60000;
            $hours=$interval->format('%h');
            $run_time=$interval->format('%i')>30?0:30-$interval->format('%i');
            $run_time1=$run_time*60000;

        
        if($hours || $time>=1800000){
        ?>
        <script>
            runwechatimport1(<?php echo $user_id; ?>);
            
        </script>
        <?php
        }else{ ?>
        <script>
           setInterval(function(){ 
                runwechatimport1(<?php echo $user_id; ?>);
           }, <?php echo $run_time1; ?>);
        </script>
        <?php  } } ?>
        
        <?php
        /** 30 min cron import for Vtex  **/
        if(isset($elliot_user_id)){
            $store = Stores::find()->select('store_id')->where(['store_name' => 'VTEX'])->one();
            $vtex_store_id = $store->store_id;
            $checkConnection = StoresConnection::find()->where(['store_id' => $vtex_store_id, 'user_id' => $elliot_user_id])->andWhere(['LIKE', 'import_status', 'Completed'])->all();
            if(!empty($checkConnection)){
                $cron_task_vtex = CronTasks::find()->where(['elliot_user_id' => $elliot_user_id, 'task_source' => 'Vtex'])->one();
                if(empty($cron_task_vtex)){
                    $CronTasks = new CronTasks();
                    $CronTasks->elliot_user_id = $elliot_user_id;
                    $CronTasks->task_name = 'import';
                    $CronTasks->task_source = 'Vtex';
                    $CronTasks->status = 'Completed';
                    $CronTasks->created_at = date('Y-m-d h:i:s', time());
                    $CronTasks->updated_at = date('Y-m-d h:i:s', time());
                    $CronTasks->save();
                }
                else{
                    $cron_tasks_data = CronTasks::find()->where(['elliot_user_id' => $elliot_user_id, 'task_source' => 'Vtex', 'status' => 'Completed'])->one();
                    if(!empty($cron_tasks_data)){
                        $datetime1 = new DateTime(date('Y-m-d h:i:s', time()));
                        $datetime2 = new DateTime($cron_tasks_data->updated_at);
                        $interval = date_diff($datetime1,$datetime2);
                        $minute=$interval->format('%i');
                        $hours=$interval->format('%h');
                        if($minute>30 || $hours>=1)
                        { ?>
                            <script type="text/javascript">
                                runVtextImportCron(<?php echo $elliot_user_id; ?>);
                            </script>
                        <?php }
                    }
                }
            }
        }
        ?>
                            
        <?php
        /** 30 min cron import for Square  **/
        if(isset($elliot_user_id)){
            $channel_data = Channels::find()->where(['channel_name' => 'Square'])->one();
            $square_channel_id = $channel_data->channel_ID;
            $checkConnection = ChannelConnection::find()->where(['elliot_user_id' => $user_id, 'channel_id' => $square_channel_id])->andWhere(['LIKE', 'import_status', 'Completed'])->one();
            if(!empty($checkConnection)){
                $cron_tasks_data = CronTasks::find()->where(['elliot_user_id' => $elliot_user_id, 'task_source' => 'Square', 'status' => 'Completed'])->one();
                if(!empty($cron_tasks_data)){
                    $datetime1 = new DateTime(date('Y-m-d h:i:s', time()));
                    $datetime2 = new DateTime($cron_tasks_data->updated_at);
                    $interval = date_diff($datetime1,$datetime2);
                    echo $minute=$interval->format('%i');
                    echo "<br>";
                    echo $hours=$interval->format('%h');
                    if($minute>30 || $hours>=1)
                    { ?>
                        <script type="text/javascript">
                            runSquareImportCron(<?php echo $elliot_user_id; ?>);
                        </script>
                    <?php }
                }
            }
        }
        ?>
<?php
        /** 30 min cron import for Square  **/
        if(isset($elliot_user_id)){
            $channel_data = Channels::find()->where(['channel_name' => 'Lazada Malaysia'])->one();
            $channel_id = $channel_data->channel_ID;
            $checkConnection = ChannelConnection::find()->where(['elliot_user_id' => $user_id, 'channel_id' => $channel_id])->andWhere(['LIKE', 'import_status', 'Completed'])->one();
            if(!empty($checkConnection)){
                $cron_tasks_data = CronTasks::find()->where(['elliot_user_id' => $elliot_user_id, 'task_source' => 'Lazada Malaysia', 'status' => 'Completed'])->one();
                if(!empty($cron_tasks_data)){
                    $datetime1 = new DateTime(date('Y-m-d h:i:s', time()));
                    $datetime2 = new DateTime($cron_tasks_data->updated_at);
                    $interval = date_diff($datetime1,$datetime2);
                    echo $minute=$interval->format('%i');
                    echo "<br>";
                    echo $hours=$interval->format('%h');
                    if($minute>5 || $hours>=1)
                    { ?>
                        <script type="text/javascript">
                            runLazadaImportCron(<?php echo $elliot_user_id; ?>);
                        </script>
                    <?php }
                }
            }
        }
        ?>
</div>  