<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\AttributeType;

/**
 * AttributeTypeSearch represents the model behind the search form about `backend\models\AttributeType`.
 */
class AttributeTypeSearch extends AttributeType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attribute_type_id', 'elliot_user_id'], 'integer'],
            [['attribute_type_name', 'attribute_type_label', 'attribute_type_description', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AttributeType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'attribute_type_id' => $this->attribute_type_id,
            'elliot_user_id' => $this->elliot_user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'attribute_type_name', $this->attribute_type_name])
            ->andFilterWhere(['like', 'attribute_type_label', $this->attribute_type_label])
            ->andFilterWhere(['like', 'attribute_type_description', $this->attribute_type_description]);

        return $dataProvider;
    }
}
