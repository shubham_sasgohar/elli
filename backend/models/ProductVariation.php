<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "product_variation".
 *
 * @property integer $product_variation_id
 * @property integer $variation_id
 * @property integer $product_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Variations $variation
 * @property Products $product
 */
class ProductVariation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_variation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['variation_id', 'product_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['variation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Variations::className(), 'targetAttribute' => ['variation_id' => 'variations_ID']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_variation_id' => 'Product Variation ID',
            'variation_id' => 'Variation ID',
            'product_id' => 'Product ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVariation()
    {
        return $this->hasOne(Variations::className(), ['variations_ID' => 'variation_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }
}
