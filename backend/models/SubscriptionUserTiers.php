<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "subscription_user_tiers".
 *
 * @property integer $subscription_id
 * @property string $plan_name
 * @property integer $user_range
 * @property string $amount
 * @property string $created_at
 * @property string $updated_At
 */
class SubscriptionUserTiers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Subscription_User_Tiers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['plan_name', 'user_range', 'amount'], 'required'],
            [['user_range'], 'integer'],
            [['created_at', 'updated_At'], 'safe'],
            [['plan_name', 'amount'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'subscription_id' => 'Subscription ID',
            'plan_name' => 'Plan Name',
            'user_range' => 'User Range',
            'amount' => 'Amount',
            'created_at' => 'Created At',
            'updated_At' => 'Updated  At',
        ];
    }
}
