<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "attribute_type".
 *
 * @property integer $attribute_type_id
 * @property integer $elliot_user_id
 * @property string $attribute_type_name
 * @property string $attribute_type_label
 * @property string $attribute_type_description
 * @property string $created_at
 * @property string $updated_at
 */
class AttributeType extends \yii\db\ActiveRecord {

  /**
   * @inheritdoc
   */
  public static function tableName() {
    return 'attribute_type';
  }

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
      [['elliot_user_id'], 'integer'],
      [['created_at', 'updated_at'], 'safe'],
      [['attribute_type_name', 'attribute_type_label'], 'required'],
      [['attribute_type_name', 'attribute_type_label', 'attribute_type_description'], 'string', 'max' => 255],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
      'attribute_type_id' => 'Attribute Type ID',
      'elliot_user_id' => 'Elliot User ID',
      'attribute_type_name' => 'Attribute Type Name',
      'attribute_type_label' => 'Label',
      'attribute_type_description' => 'Description',
      'created_at' => 'Created At',
      'updated_at' => 'Updated At',
    ];
  }

}
