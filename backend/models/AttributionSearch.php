<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Attribution;

/**
 * AttributionSearch represents the model behind the search form about `backend\models\Attribution`.
 */
class AttributionSearch extends Attribution
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attribution_id', 'elliot_user_id'], 'integer'],
            [['attribution_name', 'attribution_description', 'attribution_type', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Attribution::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'attribution_id' => $this->attribution_id,
            'elliot_user_id' => $this->elliot_user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'attribution_name', $this->attribution_name])
            ->andFilterWhere(['like', 'attribution_description', $this->attribution_description])
            ->andFilterWhere(['like', 'attribution_type', $this->attribution_type]);

        return $dataProvider;
    }
}
