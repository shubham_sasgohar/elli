<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "store_details".
 *
 * @property integer $id
 * @property integer $store_connection_id
 * @property integer $channel_connection_id
 * @property string $store_name
 * @property string $store_url
 * @property string $country
 * @property string $country_code
 * @property string $currency
 * @property string $currency_symbol
 * @property string $created_at
 * @property string $updated_at
 *
 * @property StoresConnection $storeConnection
 * @property ChannelConnection $channelConnection
 */
class StoreDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'store_connection_id', 'channel_connection_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['store_name', 'store_url', 'country', 'country_code', 'currency', 'currency_symbol'], 'string', 'max' => 255],
            [['store_connection_id'], 'exist', 'skipOnError' => true, 'targetClass' => StoresConnection::className(), 'targetAttribute' => ['store_connection_id' => 'stores_connection_id']],
            [['channel_connection_id'], 'exist', 'skipOnError' => true, 'targetClass' => ChannelConnection::className(), 'targetAttribute' => ['channel_connection_id' => 'channel_connection_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'store_connection_id' => 'Store Connection ID',
            'channel_connection_id' => 'Channel Connection ID',
            'store_name' => 'Store Name',
            'store_url' => 'Store Url',
            'country' => 'Country',
            'country_code' => 'Country Code',
            'currency' => 'Currency',
            'currency_symbol' => 'Currency Symbol',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreConnection()
    {
        return $this->hasOne(StoresConnection::className(), ['stores_connection_id' => 'store_connection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChannelConnection()
    {
        return $this->hasOne(ChannelConnection::className(), ['channel_connection_id' => 'channel_connection_id']);
    }
}
