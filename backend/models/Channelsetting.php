<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "channelsetting".
 *
 * @property integer $id
 * @property integer $channel_id
 * @property string $channel_name
 * @property string $setting_key
 * @property string $setting_value
 * @property string $created_at
 * @property string $updated_at
 */
class Channelsetting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'channelsetting';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['channel_id'], 'required'],
            [['channel_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['channel_name', 'setting_key', 'setting_value'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'channel_id' => 'Channel ID',
            'channel_name' => 'Channel Name',
            'setting_key' => 'Setting Key',
            'setting_value' => 'Setting Value',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
