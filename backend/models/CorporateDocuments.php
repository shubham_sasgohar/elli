<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "Corporate_documents".
 *
 * @property integer $id
 * @property integer $elliot_user_id
 * @property string $Business_License
 * @property string $Business_Papers
 * @property string $Tax_ID
 * @property string $channel
 * @property string $created_at
 * @property string $updated_at
 */
class CorporateDocuments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Corporate_documents';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['elliot_user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['Business_License', 'Business_Papers'], 'string', 'max' => 500],
            [['Tax_ID', 'channel'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'elliot_user_id' => 'Elliot User ID', 
            'Business_License' => 'Business  License',
            'Business_Papers' => 'Business  Papers',
            'Tax_ID' => 'Tax  ID',
            'channel' => 'Channel',
//            'created_at' => 'Created At',
//            'updated_at' => 'Updated At',
        ];
    }
}
