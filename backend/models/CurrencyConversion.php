<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "currency_conversion".
 *
 * @property integer $id
 * @property string $from_currency
 * @property integer $from_value
 * @property string $to_currency
 * @property integer $to_value
 * @property string $created_at
 * @property string $updated_at
 */
class CurrencyConversion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency_conversion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_value', 'to_value'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['from_currency', 'to_currency'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from_currency' => 'From Currency',
            'from_value' => 'From Value',
            'to_currency' => 'To Currency',
            'to_value' => 'To Value',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
