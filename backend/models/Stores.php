<?php

namespace backend\models;

use Yii;
use backend\models\StoresConnection;
use backend\models\stores;
use Bigcommerce\Api\Connection;
use Bigcommerce\Api\Client as Bigcommerce;
use backend\models\Channels;
use backend\models\ChannelConnection;
use backend\controllers\ChannelsController;
use backend\models\Categories;
use Bigcommerce\Api\ShopifyClient as Shopify;
use SoapClient;
use backend\models\MagentoStores;
use backend\models\ProductImages;
use backend\models\ProductChannel;
use backend\models\ProductAbbrivation;
use backend\models\ProductVariation;
use backend\models\CategoryAbbrivation;
use backend\models\StoreDetails;
use backend\models\Products;
use backend\models\CurrencySymbols;
use backend\models\CurrencyConversion;
use Automattic\WooCommerce\Client as Woocommerce;
use Automattic\WooCommerce\HttpClient\HttpClientException;
use backend\models\Channelsetting;
use backend\controllers\VtexController as Vtex;

/**
 * This is the model class for table "stores".
 *
 * @property integer $store_id
 * @property string $store_name
 * @property string $store_url
 * @property integer $store_amount
 * @property string $store_image
 * @property string $created_at
 * @property string $updated_at
 */
class Stores extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
	return 'stores';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
	return [
		[['store_amount'], 'integer'],
		[['created_at', 'updated_at'], 'safe', 'url', 'api_key', 'key_password', 'token'],
		[['store_name', 'store_url', 'store_image'], 'string', 'max' => 255],
	];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
	return [
	    'store_id' => 'Store ID',
	    'store_name' => 'Store Name',
	    'store_url' => 'Store Url',
	    'store_amount' => 'Store Amount',
	    'store_image' => 'Store Image',
	    'created_at' => 'Created At',
	    'updated_at' => 'Updated At',
	];
    }

    public function getStoreconnection() {
	return $this->hasOne(StoresConnection::className(), ['store_id' => 'store_id']);
    }

    /* Function to Fetch Data from BigCommerce Store */

    public function bigcommerce_initial_import($user, $store_connection_id) {


	$users_Id = $user->id;
	$user_company = $user->company_name;
	$domain_name = $user->domain_name;
	$store_connection = StoresConnection::find()->where(['user_id' => $users_Id])->all();
	$notify = 'false';
	if (!empty($store_connection)) {
	    $get_big_id = Stores::find()->select('store_id')->where(['store_name' => 'BigCommerce'])->one();
	    $bigcommerce_store_id = $get_big_id->store_id;
	    $store_big = StoresConnection::find()->where(['stores_connection_id' => $store_connection_id, 'user_id' => $users_Id])->one();
	    //BigCommerce Configuration Object
	    $client_id = $store_big->big_client_id;
	    $access_token = $store_big->big_access_token;
	    $store_hash = $store_big->big_client_id;
	    Bigcommerce::configure(array(
		'client_id' => $store_big->big_client_id,
		'auth_token' => $store_big->big_access_token,
		'store_hash' => $store_big->big_store_hash
	    ));
//      products hooks
	    $hookrs = Bigcommerce::createWebhook(array(
			'scope' => 'store/product/*',
			'destination' => Yii::$app->params['BASE_URL'] . "people/producthooksbg?id=" . $users_Id . "&connection_id=" . $store_connection_id,
			'is_active' => true
	    ));


	    //product inventory hooks
	    $hookrs = Bigcommerce::createWebhook(array(
			'scope' => 'store/product/inventory/*',
			'destination' => Yii::$app->params['BASE_URL'] . 'people/producthooksbg?id=' . $users_Id . "&connection_id=" . $store_connection_id,
			'is_active' => true
	    ));

	    //SKU inventory hooks
	    $hookrs = Bigcommerce::createWebhook(array(
			'scope' => 'store/sku/inventory/*',
			'destination' => Yii::$app->params['BASE_URL'] . 'people/producthooksbg?id=' . $users_Id . "&connection_id=" . $store_connection_id,
			'is_active' => true
	    ));

	    //Orders create hooks
	    $hookrs = Bigcommerce::createWebhook(array(
			'scope' => 'store/order/created',
			'destination' => Yii::$app->params['BASE_URL'] . 'people/orderbigcommercecreate?id=' . $users_Id . "&connection_id=" . $store_connection_id,
			'is_active' => true
	    ));

	    //Orders update hooks
	    $hookrs = Bigcommerce::createWebhook(array(
			'scope' => 'store/order/updated',
			'destination' => Yii::$app->params['BASE_URL'] . 'people/orderbigcommerceupdate?id=' . $users_Id . "&connection_id=" . $store_connection_id,
			'is_active' => true
	    ));

	    //Category hooks
	    $hookrs = Bigcommerce::createWebhook(array(
			'scope' => 'store/category/*',
			'destination' => Yii::$app->params['BASE_URL'] . 'people/categoryhooksbg?id=' . $users_Id . "&connection_id=" . $store_connection_id,
			'is_active' => true
	    ));

	    //SKU hooks
	    $hookrs = Bigcommerce::createWebhook(array(
			'scope' => 'store/sku/*',
			'destination' => Yii::$app->params['BASE_URL'] . 'people/skuupdategb?id=' . $users_Id . "&connection_id=" . $store_connection_id,
			'is_active' => true
	    ));



	    //customer hooks
//            $hookrs = Bigcommerce::createWebhook(array(
//                        'scope' => 'store/customer/*',
//                        'destination' => Yii::$app->params['BASE_URL'] . 'people/create-customer-hook?id=' . $users_Id,
//                        'is_active' => true
//            ));*/

	    $webhookContent = "";
	    $webhook = fopen('php://input', 'rb');
	    while (!feof($webhook)) {
		$webhookContent .= fread($webhook, 4096);
	    }
	    /* Save Store Details */
	    $Bgc_store_details = Bigcommerce::getStore();
	    $country_code = $Bgc_store_details->country_code;
	    $currency_code = $Bgc_store_details->currency;
	    if (!empty($Bgc_store_details)) {
		$symbol = self::getCurrencySymbol($Bgc_store_details->currency);
		$bgcdoamin = $Bgc_store_details->domain;
		$save_store_details = new StoreDetails();
		$save_store_details->store_connection_id = $store_connection_id;
		$save_store_details->store_name = $Bgc_store_details->name;
		$save_store_details->store_url = $Bgc_store_details->domain;
		$save_store_details->country = $Bgc_store_details->country;
		$save_store_details->country_code = $country_code;
		$save_store_details->currency = $Bgc_store_details->currency;
		$save_store_details->currency_symbol = $symbol;
		$save_store_details->channel_accquired = 'BigCommerce';
		$save_store_details->created_at = date('Y-m-d H:i:s', time());
		$save_store_details->save(false);
	    }
	    if ($currency_code != '') {

		$conversion_rate = Stores::getCurrencyConversionRate($currency_code, 'USD');
		$currency_check = CurrencyConversion::find()->Where(['to_currency' => $currency_code])->one();
		if (empty($currency_check)) {
		    $store_currency_conversion = new CurrencyConversion();
		    $store_currency_conversion->from_currency = 'USD';
		    $store_currency_conversion->from_value = 1;
		    $store_currency_conversion->to_currency = $currency_code;
		    $store_currency_conversion->to_value = $conversion_rate;
		    $store_currency_conversion->created_at = date('Y-m-d H:i:s', time());
		    $store_currency_conversion->save(false);
		} else {
		    $currency_check->from_currency = 'USD';
		    $currency_check->from_value = 1;
		    $currency_check->to_currency = $currency_code;
		    $currency_check->to_value = $conversion_rate;
		    $currency_check->save(false);
		}
	    }

	    /* End Save Store Details */
	    /* Fetching All the Categories from BigCommerce Store */
	    $categories = Bigcommerce::getCategories();
	    if (!empty($categories)) {
		foreach ($categories as $category) {
		    $cat_channel_abb_id = 'BGC' . $category->id;
		    $cat_name = $category->name;
		    $cat_parent_id = $category->parent_id;
		    //Check whelther category with the same name available
		    $checkModel = Categories::find()->where(['category_name' => $cat_name])->one();
		    if (empty($checkModel)) {
			//Create Model for each new category fetched
			$categoryModel = new Categories();
			$categoryModel->category_name = $cat_name;
			if ($cat_parent_id > 0) {
			    $get_parent = Bigcommerce::getCategory($cat_parent_id);
			    $find_category = Categories::find()->select('category_ID')->where(['category_name' => $get_parent->name])->one();
			    if (!empty($find_category)) {
				$cat_parent_id = $find_category->category_ID;
			    }
			}
			$categoryModel->channel_abb_id = '';
			$categoryModel->parent_category_ID = $cat_parent_id;
			$created_date = date('Y-m-d H:i:s', time());
			$categoryModel->created_at = $created_date;
			//Save Elliot User id
			$categoryModel->elliot_user_id = $users_Id;
			if ($categoryModel->save()) {
			    /*			     * Save Category Id in Category Abbrivation table */
			    $CategoryAbbrivation = new CategoryAbbrivation();
			    $CategoryAbbrivation->channel_abb_id = $cat_channel_abb_id;
			    $CategoryAbbrivation->mul_store_id = $store_connection_id;
			    $CategoryAbbrivation->category_ID = $categoryModel->category_ID;
			    $CategoryAbbrivation->channel_accquired = 'BigCommerce';
			    $CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
			    $CategoryAbbrivation->save(false);
			}
		    } else {
			/** If category is already exist in elliot then Save Category Id in Category Abbrivation table */
			$categorytAbbrivation_check = CategoryAbbrivation::find()->Where(['channel_abb_id' => $cat_channel_abb_id, 'mul_store_id' => $store_connection_id])->one();
			if (empty($categorytAbbrivation_check)) {
			    $CategoryAbbrivation = new CategoryAbbrivation();
			    $CategoryAbbrivation->channel_abb_id = $cat_channel_abb_id;
			    $CategoryAbbrivation->mul_store_id = $store_connection_id;
			    $CategoryAbbrivation->category_ID = $checkModel->category_ID;
			    $CategoryAbbrivation->channel_accquired = 'BigCommerce';
			    $CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
			    $CategoryAbbrivation->save(false);
			}
		    }
		}
	    }

	    // To count all Products
	    $count = Bigcommerce::getProductsCount();
	    //To list all the products
	    $products = Bigcommerce::getProducts();
	    $customers = Bigcommerce::getCustomers();

	    if (!empty($products)) {
		foreach ($products as $product) {
		    $p_id = $product->id;
		    $p_name = $product->name;
		    //Give prefix big Commerce Product id
		    $prefix_product_id = 'BGC' . $p_id;
		    $p_sku = $product->sku;
		    $p_upc = $product->upc;
		    $p_des = $product->description;
		    $p_avail = $product->availability;
		    if ($p_avail == 'available' || $p_avail == 'preorder') {
			$product_status = 1;
		    } else {
			$product_status = 0;
		    }

		    $p_brand = ucfirst($user_company);
		    $product_url = $bgcdoamin . $product->custom_url;
		    //$product->brand_id;
		    //$product->brand; //object
		    $p_weight = $product->weight;
		    $p_price = $product->price;
		    $p_saleprice = $product->sale_price;
		    /* For if sale price empty null or 0 then price value is  sale price value */
		    if ($p_saleprice == '' || $p_saleprice == Null || $p_saleprice == 0) {
			$p_saleprice = $p_price;
		    }
		    $p_visibility = $product->is_visible;
		    $p_stk_lvl = $product->inventory_level;
		    $p_stk_warning_lvl = $product->inventory_warning_level;
		    if ($p_stk_warning_lvl == '' || $p_stk_warning_lvl == 0 || $p_stk_warning_lvl == Null) {
			$p_stk_warning_lvl = 5;
		    }
		    $p_stk_track = $product->inventory_tracking;
		    $p_sale = $product->total_sold;
		    $p_condition = $product->condition;
		    //Fields which are required but not avaialable @Bigcommerce
		    $p_ean = '';
		    $p_jan = '';
		    $p_isbn = '';
		    $p_mpn = '';
		    $p_created_date = date('Y-m-d H:i:s', strtotime($product->date_created));
		    $p_updated_date = date('Y-m-d H:i:s', strtotime($product->date_modified));

		    $product_categories_ids = $product->categories;

		    $product_image_data = array();
		    if (!empty($product->images)) {
			foreach ($product->images as $_image) {
			    $p_image_link = $_image->standard_url;
			    $p_image_label = $_image->description;
			    $p_image_priority = $_image->sort_order;
			    $p_image_created_date = date('Y-m-d H:i:s', strtotime($_image->date_created));
			    $p_image_updated_date = date('Y-m-d H:i:s', strtotime($_image->date_created));

			    $product_image_data[] = array(
				'image_url' => $p_image_link,
				'label' => $p_image_label,
				'position' => $p_image_priority,
				'base_img' => $p_image_link,
			    );
			}
		    }

		    $product_data = array(
			'product_id' => $p_id, // Stores/Channel product ID
			'name' => $p_name, // Product name
			'sku' => $p_sku, // Product SKU
			'description' => $p_des, // Product Description
			'product_url_path' => $product_url, // Product url if null give blank value
			'weight' => $p_weight, // Product weight if null give blank value
			'status' => $product_status, // Product status (visible or hidden). Give in 0 or 1 form (0 = hidden, 1 = visible).
			'price' => $p_price, // Porduct price
			'sale_price' => $p_saleprice, // Product sale price if null give Product price value
			'qty' => $p_stk_lvl, //Product quantity 
			'stock_status' => $p_visibility, // Product stock status ("in stock" or "out of stock"). 
			// Give in 0 or 1 form (0 = out of stock, 1 = in stock)
			'websites' => array(), //This is for only magento give only and blank array
			'brand' => $p_brand, // Product brand if any
			'low_stock_notification' => $p_stk_warning_lvl, // Porduct low stock notification if any otherwise give default 5 value
			'created_at' => $p_created_date, // Product created at date format date('Y-m-d H:i:s')
			'updated_at' => $p_updated_date, // Product updated at date format date('Y-m-d H:i:s')
			'mul_store_id' => $store_connection_id, //Give multiple store id
			'mul_channel_id' => '', // Give multiple channel id
			'channel_store_name' => 'BigCommerce', // Channel or Store name
			'channel_store_prefix' => 'BGC', // Channel or store prefix id
			'elliot_user_id' => $users_Id, // Elliot user id
			'store_id' => $bigcommerce_store_id, // if your are importing store give store id
			'channel_id' => '', // if your are importing channel give channel id
			'country_code' => $country_code, // store or channel country code
			'currency_code' => $currency_code,
			'conversion_rate' => $conversion_rate,
			'upc' => $p_upc, // Product barcode if any
			'ean' => '', // Product ean if any
			'jan' => '', // Product jan if any
			'isban' => '', // Product isban if any
			'mpn' => '', // Product mpn if any
			'categories' => $product_categories_ids, // Product categroy array. If null give a blank array 
			'images' => $product_image_data, // Product images data
		    );
		    Stores::productImportingCommon($product_data);
		}
	    }
	    /* Fetching All  Customers from BigCommerce Store */
	    $channel_name = 'BigCommerce';

	    if (!empty($customers)) {
		foreach ($customers as $customers_data) {
		    //get customer Adderss
		    $customers_add = Bigcommerce::getCollection('/customers/' . $customers_data->id . '/addresses', 'Address');
		    $customer_abb_id = $customers_data->id;
		    //Prefix Customer Abbrivation Id
		    $Prefix_customer_abb_id = 'BGC' . $customer_abb_id;

		    $first_name = $customers_data->first_name;
		    $last_name = $customers_data->last_name;
		    $customer_email = $customers_data->email;
		    $create_at = $customers_data->date_created;
		    $update_at = $customers_data->date_modified;

		    $street1 = isset($customers_add[0]->street_1) ? $customers_add[0]->street_1 : "";
		    $street2 = isset($customers_add[0]->street_2) ? $customers_add[0]->street_2 : "";
		    $city = isset($customers_add[0]->city) ? $customers_add[0]->city : "";
		    $country = isset($customers_add[0]->country) ? $customers_add[0]->country : "";
		    $country_iso = isset($customers_add[0]->country_iso2) ? $customers_add[0]->country_iso2 : "";
		    $state = isset($customers_add[0]->state) ? $customers_add[0]->state : "";
		    $zip = isset($customers_add[0]->zip) ? $customers_add[0]->zip : "";
		    $phone_number = isset($customers_data->phone) ? $customers_data->phone : $customers_add[0]->phone_number;
		    $add_type = isset($customers_add[0]->address_type) ? $customers_add[0]->address_type : "";

		    $customer_data = array(
			'customer_id' => $customer_abb_id,
			'elliot_user_id' => $users_Id,
			'first_name' => $first_name,
			'last_name' => $last_name,
			'email' => $customer_email,
			'channel_store_name' => 'BigCommerce',
			'channel_store_prefix' => 'BGC',
			'mul_store_id' => $store_connection_id,
			'mul_channel_id' => '', // Give multiple channel id
			'customer_created_at' => $create_at,
			'customer_updated_at' => $update_at,
			'billing_address' => array(
			    'street_1' => $street1,
			    'street_2' => $street2,
			    'city' => $city,
			    'state' => $state,
			    'country' => $country,
			    'country_iso' => $country_iso,
			    'zip' => $zip,
			    'phone_number' => $phone_number,
			    'address_type' => $add_type,
			),
			'shipping_address' => array(
			    'street_1' => '',
			    'street_2' => '',
			    'city' => '',
			    'state' => '',
			    'country' => '',
			    'zip' => '',
			),
		    );
		    self::customerImportingCommon($customer_data);
		}
	    }

	    $orders = Bigcommerce::getOrders();
	    /* For Orders */
	    if (!empty($orders)) {
		foreach ($orders as $orders_data) {
		    //get BigCommerce Order id
		    $bigcommerce_order_id = $orders_data->id;
		    //Give prefix big Commerce Order id
		    $prefix_order_id = 'BGC' . $bigcommerce_order_id;


		    //Fetch Ship details
		    $ship_details = Bigcommerce::getCollection('/orders/' . $orders_data->id . '/shipping_addresses', 'Order');
		    $order_customer_id = $orders_data->customer_id;  //0
		    $order_status = $orders_data->status;
		    $product_qauntity = $orders_data->items_total;
		    //billing Address
		    $billing_add1 = isset($orders_data->billing_address->street_1) ? $orders_data->billing_address->street_1 : "" . ',' . isset($orders_data->billing_address->street_2) ? $orders_data->billing_address->street_2 : "";
		    $billing_add2 = isset($orders_data->billing_address->city) ? $orders_data->billing_address->city : "" . ',' . isset($orders_data->billing_address->state) ? $orders_data->billing_address->state : "" .
			    ',' . isset($orders_data->billing_address->zip) ? $orders_data->billing_address->zip : "" . ',' . isset($orders_data->billing_address->country) ? $orders_data->billing_address->country : "";
		    $billing_address = $billing_add1 . ',' . $billing_add2;

		    //billing Address
		    $bill_street_1 = isset($orders_data->billing_address->street_1) ? $orders_data->billing_address->street_1 : '';
		    $bill_street_2 = isset($orders_data->billing_address->street_2) ? $orders_data->billing_address->street_2 : '';
		    $bill_city = isset($orders_data->billing_address->city) ? $orders_data->billing_address->city : '';
		    $bill_state = isset($orders_data->billing_address->state) ? $orders_data->billing_address->state : '';
		    $bill_zip = isset($orders_data->billing_address->zip) ? $orders_data->billing_address->zip : '';
		    $bill_country = isset($orders_data->billing_address->country) ? $orders_data->billing_address->country : '';
		    $bill_country_iso = isset($orders_data->billing_address->country_iso2) ? $orders_data->billing_address->country_iso2 : '';
		    $get_email = isset($orders_data->billing_address->email) ? $orders_data->billing_address->email : "";
		    $get_phone = isset($orders_data->billing_address->phone) ? $orders_data->billing_address->phone : "";
		    $bill_first_name = isset($orders_data->billing_address->first_name) ? $orders_data->billing_address->first_name : "";
		    $bill_last_name = isset($orders_data->billing_address->last_name) ? $orders_data->billing_address->last_name : "";

		    //Shipping Address
		    $ship_street_1 = isset($ship_details[0]->street_1) ? $ship_details[0]->street_1 : $orders_data->billing_address->street_1;
		    $ship_street_2 = isset($ship_details[0]->street_2) ? $ship_details[0]->street_2 : $orders_data->billing_address->street_2;
		    $ship_city = isset($ship_details[0]->city) ? $ship_details[0]->city : $orders_data->billing_address->city;
		    $ship_state = isset($ship_details[0]->state) ? $ship_details[0]->state : $orders_data->billing_address->state;
		    $ship_zip = isset($ship_details[0]->zip) ? $ship_details[0]->zip : $orders_data->billing_address->zip;
		    $ship_country = isset($ship_details[0]->country) ? $ship_details[0]->country : $orders_data->billing_address->country;
		    $ship_country_iso = isset($ship_details[0]->country_iso2) ? $ship_details[0]->country_iso2 : $orders_data->billing_address->country_iso2;

		    $shipping_address = $ship_street_1 . ',' . $ship_street_2 . ',' . $ship_city . ',' . $ship_state . ',' . $ship_zip . ',' . $ship_country;

		    $total_amount = $orders_data->total_inc_tax;
		    $base_shipping_cost = $orders_data->base_shipping_cost;
		    $shipping_cost_tax = $orders_data->shipping_cost_tax;
		    $base_handling_cost = $orders_data->base_handling_cost;
		    $handling_cost_tax = $orders_data->handling_cost_tax;
		    $base_wrapping_cost = $orders_data->base_wrapping_cost;
		    $wrapping_cost_tax = $orders_data->wrapping_cost_tax;
		    $payment_method = $orders_data->payment_method;
		    $payment_provider_id = $orders_data->payment_provider_id;
		    $payment_status = $orders_data->payment_status;
		    $refunded_amount = $orders_data->refunded_amount;
		    $discount_amount = $orders_data->discount_amount;
		    $coupon_discount = $orders_data->coupon_discount;
		    $order_date = $orders_data->date_created;
		    $order_last_modified_date = $orders_data->date_modified;

		    $order_product_data = array();
		    $product_dtails = Bigcommerce::getCollection('/orders/' . $orders_data->id . '/products', 'Order');
		    foreach ($product_dtails as $product_dtails_data) {
			$product_id = $product_dtails_data->product_id;
			$product_name = $product_dtails_data->name;
			$product_qty = $product_dtails_data->quantity;
			$order_product_sku = $product_dtails_data->sku;
			$price = $product_dtails_data->total_inc_tax;
			$product_weight = $product_dtails_data->weight;
			$order_product_data[] = array(
			    'product_id' => $product_id,
			    'name' => $product_name,
			    'sku' => $order_product_sku,
			    'price' => $price,
			    'qty_ordered' => $product_qty,
			    'weight' => $product_weight,
			);
		    }

		    $order_data = array(
			'order_id' => $bigcommerce_order_id,
			'mul_store_id' => $store_connection_id, //Give multiple store id
			'mul_channel_id' => '', // Give multiple channel id
			'status' => $order_status,
			'magento_store_id' => '',
			'order_grand_total' => $total_amount,
			'customer_id' => $order_customer_id,
			'customer_email' => $get_email,
			'order_shipping_amount' => $shipping_cost_tax,
			'order_tax_amount' => '',
			'total_qty_ordered' => $product_qauntity,
			'created_at' => $order_date,
			'updated_at' => $order_last_modified_date,
			'payment_method' => $payment_method,
			'refund_amount' => $refunded_amount,
			'discount_amount' => $discount_amount,
			'channel_store_name' => 'BigCommerce',
			'channel_store_prefix' => 'BGC',
			'elliot_user_id' => $users_Id,
			'currency_code' => $currency_code,
			'conversion_rate' => $conversion_rate,
			'store_id' => $bigcommerce_store_id,
			'channel_id' => '',
			'shipping_address' => array(
			    'street_1' => $ship_street_1,
			    'street_2' => $ship_street_2,
			    'state' => $ship_state,
			    'city' => $ship_city,
			    'country' => $ship_country,
			    'country_id' => $ship_country_iso,
			    'postcode' => $ship_zip,
			    'email' => $get_email,
			    'telephone' => $get_phone,
			    'firstname' => '',
			    'lastname' => '',
			),
			'billing_address' => array(
			    'street_1' => $bill_street_1,
			    'street_2' => $bill_street_2,
			    'state' => $bill_state,
			    'city' => $bill_city,
			    'country' => $bill_country,
			    'country_id' => $bill_country_iso,
			    'postcode' => $bill_zip,
			    'email' => $get_email,
			    'telephone' => $get_phone,
			    'firstname' => $bill_first_name,
			    'lastname' => $bill_last_name,
			),
			'items' => $order_product_data,
		    );
		    Stores::orderImportingCommon($order_data);
		}
	    }
	    /* End All  Orders from BigCommerce Store */
	    $notify = 'true';

	    $currency_channel_exists = Channelsetting::find()->Where(['mul_store_id' => $store_connection_id, 'setting_key' => 'currency', 'user_id' => $users_Id])->one();
	    if (empty($currency_channel_exists)) {
		$channel_setting = new Channelsetting();
		$channel_setting->store_id = $bigcommerce_store_id;
		$channel_setting->user_id = $users_Id;
		$channel_setting->channel_name = 'BigCommerce';
		$channel_setting->setting_key = 'currency';
		$channel_setting->setting_value = $currency_code;
		$channel_setting->mul_store_id = $store_connection_id;
		$channel_setting->created_at = date('Y-m-d H:i:s');
		$channel_setting->save(false);
	    }
	}
	return $notify;
    }

    /* BigCommerce Customer Profile Update */

    public function bigcommerce_update_customer($channel_abb_id, $object) {

	$users_Id = Yii::$app->user->identity->id;
	$store_connection = StoresConnection::find()->where(['user_id' => $users_Id])->all();
	$notify = 'false';
	if (!empty($store_connection)):
	    $get_big_id = Stores::find()->select('store_id')->where(['store_name' => 'BigCommerce'])->one();
	    $bigcommerce_store_id = $get_big_id->store_id;
	    $store_big = StoresConnection::find()->where(['store_id' => $bigcommerce_store_id, 'user_id' => $users_Id])->one();
	    //BigCommerce Configuration Object
	    Bigcommerce::configure(array(
		'client_id' => $store_big->big_client_id,
		'auth_token' => $store_big->big_access_token,
		'store_hash' => $store_big->big_store_hash
	    ));

	    $update_data = Bigcommerce::updateResource('/customers/' . $channel_abb_id, $object);

	endif;
    }

    /* End BigCommerce Customer Profile Update */



    /* Start BigCommerce Product Update */

    public function bigcommerce_update_product($bigcm_id, $object, $pcat) {


	$users_Id = Yii::$app->user->identity->id;
	$store_connection = StoresConnection::find()->where(['user_id' => $users_Id])->all();
	$notify = 'false';
	if (!empty($store_connection)):
	    $get_big_id = Stores::find()->select('store_id')->where(['store_name' => 'BigCommerce'])->one();
	    $bigcommerce_store_id = $get_big_id->store_id;
	    $store_big = StoresConnection::find()->where(['store_id' => $bigcommerce_store_id, 'user_id' => $users_Id])->one();
	    //BigCommerce Configuration Object
	    Bigcommerce::configure(array(
		'client_id' => $store_big->big_client_id,
		'auth_token' => $store_big->big_access_token,
		'store_hash' => $store_big->big_store_hash
	    ));
//            //For product categories
//            if (!empty($pcat)):
//                foreach ($pcat as $upcat) :
//                    //get Product Categories
//                    $filter = array("name" => $upcat);
//                    $categories = Bigcommerce::getCategories($filter);
//                    if (empty($categories)) :
//                        // if category is not exist in BIGCOMMERCE then Create
//                        $create_object = array("name" => $upcat);
//                        $create_cat = Bigcommerce::createResource('/categories', $create_object);
//                        $cat_bigcom_id = $create_cat->id;
//                        $arrdata[] = $cat_bigcom_id;
//                    else :
//                        foreach ($categories as $cn):
//                            $cat_bigcom_id = $cn->id;
//                            $arrdata[] = $cat_bigcom_id;
//                        endforeach;
//                    endif;
//                endforeach;
//            endif;


	    $id = $bigcm_id;
	    $products = Bigcommerce::getResource('/products/' . $id, 'Product');

	    if (!empty($products)) :
		$update_product = Bigcommerce::updateResource('/products/' . $id, $object);
	    //for update categories
//                if (!empty($pcat)):
//                    $object1 = array("categories" => $arrdata);
//                    $update_product1 = Bigcommerce::updateResource('/products/' . $id, $object1);
//                endif;

	    endif;

	endif;
    }

    /* End BigCommerce Product Update */


    /* Start BigCommerce Product Create */

    public function bigcommerce_create_product($object, $product_id) {

	$users_Id = Yii::$app->user->identity->id;
	$store_connection = StoresConnection::find()->where(['user_id' => $users_Id])->all();

	if (!empty($store_connection)):
	    $get_big_id = Stores::find()->select('store_id')->where(['store_name' => 'BigCommerce'])->one();

	    $bigcommerce_store_id = $get_big_id->store_id;
	    $store_big = StoresConnection::find()->where(['store_id' => $bigcommerce_store_id, 'user_id' => $users_Id])->one();

	    //BigCommerce Configuration Object
	    Bigcommerce::configure(array(
		'client_id' => $store_big->big_client_id,
		'auth_token' => $store_big->big_access_token,
		'store_hash' => $store_big->big_store_hash
	    ));

	    $products = Bigcommerce::createResource('/products', $object);
	    if (!empty($products)) :
		// Get Image From Db
		$product_images = ProductImages::find()->where(['product_ID' => $product_id])->all();
		$count_product_images = count($product_images);
		$BigCm_product_id = $products->id;
		//Save multiple Image
		foreach ($product_images as $product_img):

		    $image_link = $product_img->link;

		    $objectimg = array(
			"image_file" => $image_link,
			"is_thumbnail" => true
		    );
		    $save_product_image = Bigcommerce::createResource('/products/' . $BigCm_product_id . '/images', $objectimg);
		endforeach;

	    endif;
	endif;
    }

    /* End BigCommerce Product Create */

    /* Start BigCommerce Customer Create */

    public function bigcommerce_create_customer($object, $customer_add_object) {

	$users_Id = Yii::$app->user->identity->id;
	$store_connection = StoresConnection::find()->where(['user_id' => $users_Id])->all();

	if (!empty($store_connection)):
	    $get_big_id = Stores::find()->select('store_id')->where(['store_name' => 'BigCommerce'])->one();

	    $bigcommerce_store_id = $get_big_id->store_id;
	    $store_big = StoresConnection::find()->where(['store_id' => $bigcommerce_store_id, 'user_id' => $users_Id])->one();

	    //BigCommerce Configuration Object
	    Bigcommerce::configure(array(
		'client_id' => $store_big->big_client_id,
		'auth_token' => $store_big->big_access_token,
		'store_hash' => $store_big->big_store_hash
	    ));

	    $create_customers = Bigcommerce::createResource('/customers', $object);
	    if (!empty($create_customers)):
		$bgc_customer_id = $create_customers->id;
		$create_customer_add = Bigcommerce::createResource('/customers/' . $bgc_customer_id . '/addresses', $customer_add_object);
	    endif;
	endif;
    }

    /* End BigCommerce Customer Create */

    /*     * *********************WE Chat*************************************** */

    /* Start We Chat Customer Create */

    public function wechat_create_product($product_id, $wechat_object) {


	$users_Id = Yii::$app->user->identity->id;
	$channel_data = Channels::find()->Where(['channel_name' => 'Service Account'])->with('channelconnections')->one();
	$wechat_token = $channel_data->channelconnections[0]->token;
	$wechat_project_id = $channel_data->channelconnections[0]->wechat_project_id;
	$c_cat = curl_init();
	curl_setopt_array($c_cat, array(
	    CURLOPT_URL => "https://cms-api.walkthechat.com/products",
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_ENCODING => "",
	    CURLOPT_MAXREDIRS => 10,
	    CURLOPT_TIMEOUT => 30,
	    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	    CURLOPT_CUSTOMREQUEST => 'POST',
	    CURLOPT_POSTFIELDS => http_build_query($wechat_object),
	    CURLOPT_HTTPHEADER => array(
		"x-access-token: $wechat_token",
		"x-id-project: $wechat_project_id"
	    ),
	));

	$data = json_decode(curl_exec($c_cat));

	if ($data->token->success == 'true'):
	    $wechat_abb_id = $data->product->groupId;
	    $product_data = Products::find()->Where(['id' => $product_id])->one();
	    $product_data->channel_abb_id = 'WCSA' . $wechat_abb_id;
	    $product_data->save(false);
	endif;
    }

    /* End WE Chat Customer Create */




    /* Start Create Category  */

//    public function create_category($cat_name, $parent_name) {
//
//	$users_Id = Yii::$app->user->identity->id;
//	// Create Category Connected Stores
//	$store_connection = StoresConnection::find()->where(['user_id' => $users_Id])->with('stores')->all();
//	if (!empty($store_connection)) :
//	    foreach ($store_connection as $sn) :
//		$connected_channel = $sn->stores->store_name;
//		if ($connected_channel == 'BigCommerce') :
//		    // Connected Big Comemrce Channel
//		    $get_big_id = Stores::find()->select('store_id')->where(['store_name' => 'BigCommerce'])->one();
//		    $bigcommerce_store_id = $get_big_id->store_id;
//		    $store_big = StoresConnection::find()->where(['store_id' => $bigcommerce_store_id, 'user_id' => $users_Id])->one();
//		    //BigCommerce Configuration Object
//		    Bigcommerce::configure(array(
//			'client_id' => $store_big->big_client_id,
//			'auth_token' => $store_big->big_access_token,
//			'store_hash' => $store_big->big_store_hash
//		    ));
//		    // Check For Duplicacy of category
//		    $filter1 = array("name" => $cat_name);
//		    $categories1 = Bigcommerce::getCategories($filter1);
//		    if (empty($categories1)) :
//			// Parent Cat ID
//			if ($parent_name != 'Please Select'):
//
//			    $parent_filter1 = array("name" => $parent_name);
//			    $parent_categories1 = Bigcommerce::getCategories($parent_filter1);
//			    if (!empty($parent_categories1)):
//				$parent_id = $parent_categories1[0]->id;
//			    endif;
//			else:
//			    $parent_id = 0;
//			endif;
//			$object = array("name" => $cat_name, "parent_id" => $parent_id);
//			$create_cat_bigCommerce = Bigcommerce::createResource('/categories', $object);
//			//Return Bigcommerce data
//			$return_object = (object) [];
//			$abb_id = $create_cat_bigCommerce->id;
//			$return_object->abb_id = 'BGC' . $channel_abb_id;
//			$return_object->group_id = 0;
//			return $return_object;
//
//		    endif;
//		endif;
//	    endforeach;
//	endif;
//	// End Big commerce Create category code//
//	// Category Create on connected Channels
//	$channel_connection = ChannelConnection::find()->where(['user_id' => $users_Id])->with('channels')->all();
//	foreach ($channel_connection as $cn):
//	    $channel_name = $cn['channels'][0]->channel_name;
//	    if ($channel_name == 'Service Account') :
//		// Get Categories from Db
//		$db_categories = Categories::find()->all();
//
//		foreach ($db_categories as $dbcat):
//
//		    if ($parent_name != 'Please Select') :
//			//find parent name in db
//			$check_db_parent_cat = Categories::find()->Where(['category_name' => $parent_name])->andwhere('channel_abb_id LIKE :query')->addParams([':query' => 'WCSA%'])->one();
//
//			if (!empty($check_db_parent_cat)):
//			    //if parent
//			    // $check_db_parent_cat
//			    $parent_id = $check_db_parent_cat->store_category_groupid;
//			    // Create a New object
//			    $new_cat = (object) [];
//			    $new_cat->language = "en";
//			    $new_cat->name = $cat_name;
//			    $new_cat->parentGroupId = $parent_id;
//			    //convert object to json
//			    $wechat_object = json_encode(array($new_cat));
//			    $wechat_cat_object = array("category" => $wechat_object);
//
//			else :
//			    // if Parent category Not Exist in WE Chat
//			    $wechat_cat_group_id = $dbcat->store_category_groupid;
//			    $token = $cn->token;
//			    $pid = $cn->wechat_project_id;
//			    $url = 'https://cms-api.walkthechat.com/categories/product/groups/' . $wechat_cat_group_id . '';
//			    $rs_cat = ChannelsController::wechatcurl($url, $token, $pid);
//			    $categories = json_decode($rs_cat);
//			    if (empty($categories->categories)) :
//				// Category Not Exist 
//				// Create a New object
//				$new_cat = (object) [];
//				$new_cat->language = "en";
//				$new_cat->name = $parent_name;
//				//convert object to json
//				$wechat_object = json_encode(array($new_cat));
//				$wechat_cat_object = array("category" => $wechat_object);
//				//$wechat_cat_object=   [category] => [{"language":"en","name":"shub"}]
//				$c_cat = curl_init();
//				curl_setopt_array($c_cat, array(
//				    CURLOPT_URL => "https://cms-api.walkthechat.com/categories/product/groups",
//				    CURLOPT_RETURNTRANSFER => true,
//				    CURLOPT_ENCODING => "",
//				    CURLOPT_MAXREDIRS => 10,
//				    CURLOPT_TIMEOUT => 30,
//				    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//				    CURLOPT_CUSTOMREQUEST => 'POST',
//				    CURLOPT_POSTFIELDS => $wechat_cat_object,
//				    CURLOPT_HTTPHEADER => array(
//					"x-access-token: $token",
//					"x-id-project: $pid"
//				    ),
//				));
//				$data1 = json_decode(curl_exec($c_cat));
//
//				$save_parent_id = $data1->categories[0]->groupId;
//
//			    endif;
//			    // Create a New object
//			    $new_cat = (object) [];
//			    $new_cat->language = "en";
//			    $new_cat->name = $cat_name;
//			    $new_cat->parentGroupId = $save_parent_id;
//			    //convert object to json
//			    $wechat_object = json_encode(array($new_cat));
//			    $wechat_cat_object = array("category" => $wechat_object);
//
//			endif;
//		    else :
//			//if not parent
//			// Create a New object
//			$new_cat = (object) [];
//			$new_cat->language = "en";
//			$new_cat->name = $cat_name;
//			//convert object to json
//			$wechat_object = json_encode(array($new_cat));
//			$wechat_cat_object = array("category" => $wechat_object);
//		    endif;
//		    // Save Category to Wechat Account
//		    $token = $cn->token;
//		    $pid = $cn->wechat_project_id;
//		    //$wechat_cat_object=   [category] => [{"language":"en","name":"shub"}]
//		    $c_cat = curl_init();
//		    curl_setopt_array($c_cat, array(
//			CURLOPT_URL => "https://cms-api.walkthechat.com/categories/product/groups",
//			CURLOPT_RETURNTRANSFER => true,
//			CURLOPT_ENCODING => "",
//			CURLOPT_MAXREDIRS => 10,
//			CURLOPT_TIMEOUT => 30,
//			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//			CURLOPT_CUSTOMREQUEST => 'POST',
//			CURLOPT_POSTFIELDS => $wechat_cat_object,
//			CURLOPT_HTTPHEADER => array(
//			    "x-access-token: $token",
//			    "x-id-project: $pid"
//			),
//		    ));
//		    $data = json_decode(curl_exec($c_cat));
//
//		    $return_object = (object) [];
//		    $return_object->group_id = $data->categories[0]->groupId;
//		    $return_object->abb_id = 'WCSA' . $data->categories[0]->_id;
//		    return $return_object;
//		    // Category Exist
//		endforeach;
//	    endif;
//	endforeach;
//    }

    /* End Ctrate Categories  */


    /* Start BigCommerce Category Update */

    public function bigcommerce_update_sku($Big_product_id, $sku_id, $object) {

	$users_Id = Yii::$app->user->identity->id;


	$store_connection = StoresConnection::find()->where(['user_id' => $users_Id])->with('stores')->all();
	if (!empty($store_connection)) :

	    foreach ($store_connection as $sn) :
		$connected_channel = $sn->stores->store_name;
		if ($connected_channel == 'BigCommerce') :
		    // Connected Big Comemrce Channel
		    $get_big_id = Stores::find()->select('store_id')->where(['store_name' => 'BigCommerce'])->one();
		    $bigcommerce_store_id = $get_big_id->store_id;
		    $store_big = StoresConnection::find()->where(['store_id' => $bigcommerce_store_id, 'user_id' => $users_Id])->one();
		    //BigCommerce Configuration Object
		    Bigcommerce::configure(array(
			'client_id' => $store_big->big_client_id,
			'auth_token' => $store_big->big_access_token,
			'store_hash' => $store_big->big_store_hash
		    ));
		    $big_update_sku = Bigcommerce::updateResource('/products/' . $Big_product_id . '/skus/' . $sku_id, $object);
		endif;

	    endforeach;
	endif;
    }

    /* End Categories Update */

    /* Start BigCommerce Category Update */

    public function bigcommerce_update_category($channel_abb_id, $object, $parent_name, $cat_name, $id) {

	$users_Id = Yii::$app->user->identity->id;
	// get All Connected Stores
	$store_connection = StoresConnection::find()->where(['user_id' => $users_Id])->all();
	if (!empty($store_connection)) :
	    foreach ($store_connection as $sn) :
		//get stores name
		$connected_channel = $sn->stores->store_name;
		if ($connected_channel == 'BigCommerce') :
		    if (!empty($store_connection)):
			//Update category for bigconmmerce
			$get_big_id = Stores::find()->select('store_id')->where(['store_name' => 'BigCommerce'])->one();
			$bigcommerce_store_id = $get_big_id->store_id;
			$store_big = StoresConnection::find()->where(['store_id' => $bigcommerce_store_id, 'user_id' => $users_Id])->one();
			//BigCommerce Configuration Object
			Bigcommerce::configure(array(
			    'client_id' => $store_big->big_client_id,
			    'auth_token' => $store_big->big_access_token,
			    'store_hash' => $store_big->big_store_hash
			));

			//If Category Exist in Big Commerce Then Update.
			$category_name = $object['name'];
			$filter = array("name" => $category_name);
			$categories = Bigcommerce::getCategories($filter);
			if (!empty($categories)) :
			    //if parent cat is not in Big Commerce then Create
			    if ($parent_name != 'Please Select'):

				$filter1 = array("name" => $parent_name);
				$categories1 = Bigcommerce::getCategories($filter1);
				if (empty($categories1)):
				    $create_object = array("name" => $parent_name);
				    $create_parent = Bigcommerce::createResource('/categories', $create_object);
				endif;
			    endif;
			    //Update Category and parent category
			    $update_categories = Bigcommerce::updateResource('/categories/' . $channel_abb_id, $object);
			endif;
		    endif;
		endif;
	    endforeach;
	endif;


	// End Big commerce Create category code//
	// Category Create on connected Channels

	$channel_connection = ChannelConnection::find()->where(['user_id' => $users_Id])->with('channels')->all();
	foreach ($channel_connection as $cn):
	    $channel_name = $cn['channels'][0]->channel_name;
	    if ($channel_name == 'Service Account') :
		$token = $cn->token;
		$pid = $cn->wechat_project_id;

		$categories_data = Categories::find()->Where(['category_ID' => $id])->one();
		$gpid = $categories_data->store_category_groupid;

		$parent_categories_data = Categories::find()->Where(['category_name' => $parent_name])->one();
		$parent_gpid = $parent_categories_data->store_category_groupid;

		$url = 'https://cms-api.walkthechat.com/categories/product/groups/' . $gpid;

		$rs_cat = ChannelsController::wechatcurl($url, $token, $pid);
		$categories = json_decode($rs_cat);
		$param = !empty($categories->categories[0]) ? $categories->categories[0] : '';
		$param->name = $cat_name;
		if (!empty($gpid)):
		    $param->parentGroupId = $parent_gpid;
		else:
		    $param->parentGroupId = '';
		endif;

		$url = 'https://cms-api.walkthechat.com/categories/product/groups/' . $gpid;

		$rs_cat_updated = ChannelsController::wechatcurl($url, $token, $pid, json_encode(array($param)), "PUT");

	    endif;
	endforeach;
    }

    /* End Categories Update */

    /*     * ********************SHOPIFY******************* */
    /* Start Shopify Customer Create */

    public function shopify_create_customer($customer_object) {

	$users_Id = Yii::$app->user->identity->id;
	// Create Category Connected Stores
	$store_connection = StoresConnection::find()->where(['user_id' => $users_Id])->with('stores')->one();

	$get_shopify_id = Stores::find()->select('store_id')->where(['store_name' => 'Shopify'])->one();
	$shopify_store_id = $get_shopify_id->store_id;
	$store_connection = StoresConnection::find()->where(['store_id' => $shopify_store_id, 'user_id' => $users_Id])->one();

	$shopify_shop = $store_connection->url;
	$shopify_api_key = $store_connection->api_key;
	$shopify_api_password = $store_connection->key_password;
	$shopify_shared_secret = $store_connection->shared_secret;

	$sc = new Shopify($shopify_shop, $shopify_api_password, $shopify_api_key, $shopify_shared_secret);
	$customer_create = $sc->call('POST', '/admin/customers.json', $customer_object);
    }

    /* End Shopify Customer Create */

    /** Magento store details import * */
    public function getMagentoStoresDetails($magento_shop, $store_connection_id, $country_detail) {
	$check_store_detail = StoreDetails::find()->where(['store_connection_id' => $store_connection_id])->one();
	if (empty($check_store_detail)) {
	    $save_store_details = new StoreDetails();
	    $save_store_details->store_connection_id = $store_connection_id;
	    $save_store_details->store_name = '';
	    $save_store_details->store_url = $magento_shop;
	    $save_store_details->country = $country_detail->name;
	    $save_store_details->country_code = $country_detail->sortname;
	    $save_store_details->currency = $country_detail->currency_code;
	    $save_store_details->currency_symbol = $country_detail->currency_symbol;
	    $save_store_details->others = '';
	    $save_store_details->channel_accquired = 'Magento';
	    $save_store_details->created_at = date('Y-m-d H:i:s', time());
	    $save_store_details->save(false);
	}
    }

    /** Magento get all stores * */
    public function getMagentoStores($stores_list, $user_id) {
	foreach ($stores_list as $_store) {
	    $checkMagentoStoreModel = MagentoStores::find()->where(['elliot_user_id' => $user_id, 'magento_store_id' => $_store['store_id']])->one();
	    if (empty($checkMagentoStoreModel)) {
		$magentoStoreModel = new MagentoStores();
		$magentoStoreModel->elliot_user_id = $user_id;
		$magentoStoreModel->magento_store_id = $_store['store_id'];
		$magentoStoreModel->magento_store_name = $_store['name'];
		$magentoStoreModel->magento_store_is_active = $_store['is_active'];
		$magentoStoreModel->created_at = date('Y-m-d h:i:s', time());
		$magentoStoreModel->save();
	    }
	}
    }

    /** Get Magento all category * */
    public function getMagentoAllCategory($cat_arr, $user_id, $store_connection_id) {
	$mag_cat_id = $cat_arr['category_id'];
	$mag_parent_id = $cat_arr['parent_id'];
	$mag_cat_name = $cat_arr['name'];
	$mage_level = $cat_arr['level'];
	$cat_child = $cat_arr['children'];
	$category_data = array(
	    'category_id' => $mag_cat_id, // Give category id of Store/channels
	    'parent_id' => 0, // Give Category parent id of Elliot if null then give 0
	    'name' => $mag_cat_name, // Give category name
	    'channel_store_name' => 'Magento', // Give Channel/Store name
	    'channel_store_prefix' => 'MG', // Give Channel/Store prefix id
	    'mul_store_id' => $store_connection_id, // Give Channel/Store prefix id
	    'mul_channel_id' => '', // Give Channel/Store prefix id
	    'elliot_user_id' => $user_id, // Give Elliot user id
	    'created_at' => date('Y-m-d h:i:s'), // Give Created at date if null then give current date format date('Y-m-d H:i:s')
	    'updated_at' => date('Y-m-d h:i:s'), // Give Updated at date if null then give current date format date('Y-m-d H:i:s')
	);
	self::categoryImportingCommon($category_data);    // Then call store modal function and give $category data

	if (count($cat_child) > 0) {
	    foreach ($cat_child as $value):
		self::getMagentoAllCategory($value, $user_id, $store_connection_id);
	    endforeach;
	}
    }

    /** Magento Parent Category Assign * */
    public function magentoParentCategoryAssign($cat_arr, $store_connection_id) {
	$mag_cat_id = $cat_arr['category_id'];
	$mag_parent_id = $cat_arr['parent_id'];
	$mag_cat_name = $cat_arr['name'];
	$mage_level = $cat_arr['level'];
	$cat_child = $cat_arr['children'];
	if ($mag_parent_id != '') {
	    $checkCatModel = CategoryAbbrivation::find()->where(['channel_abb_id' => 'MG' . $mag_cat_id, 'mul_store_id' => $store_connection_id])->one();
	    if (!empty($checkCatModel)) {
		$elliot_cat_id = $checkCatModel->category_ID;
		$checkParentModel = CategoryAbbrivation::find()->where(['channel_abb_id' => 'MG' . $mag_parent_id, 'mul_store_id' => $store_connection_id])->one();
		if (!empty($checkParentModel)) {
		    $categoey_model = Categories::find()->where(['category_ID' => $elliot_cat_id])->one();
		    $elliot_parent_id = $checkParentModel->category_ID;
		    $categoey_model->parent_category_ID = $elliot_parent_id;
		    $categoey_model->save();
		}
	    }
	}
	if (count($cat_child) > 0) {
	    foreach ($cat_child as $value) {
		self::magentoParentCategoryAssign($value, $store_connection_id);
	    }
	}
    }

    /** Get magento all product * */
    public function getMagentoAllProducts($product_list, $user_id, $session_id, $cli, $magento_shop, $store_connection_id, $country_detail, $conversion_rate) {
	foreach ($product_list as $_product) {
	    $mage_product_id = $_product['product_id'];
	    $product_info = $cli->call($session_id, 'catalog_product.info', $mage_product_id);
	    $product_name = $product_info['name'];
	    $product_sku = $product_info['sku'];
	    if ($product_sku == "") {
		$product_sku = '';
	    }
	    $product_type = $product_info['type'];
	    $product_categories_ids = $product_info['categories'];
	    $product_description = $product_info['description'];
	    $product_url_path = $product_info['url_path'];
	    $product_url = $magento_shop . '/' . $product_url_path;
	    $product_weight = '';
	    if (array_key_exists('weight', $product_info)) {
		$product_weight = $product_info['weight'];
	    }
	    $product_status = ($product_info['status'] == 1) ? $product_info['status'] : 0;
	    $product_created_at = $product_info['created_at'];
	    $product_updated_at = $product_info['updated_at'];
	    $product_price = $product_info['price'];
	    $stock_qty = $cli->call($session_id, 'cataloginventory_stock_item.list', $mage_product_id);
	    $qty = $stock_qty[0]['qty'];
	    $stock_status = ($stock_qty[0]['is_in_stock'] == 1) ? $stock_qty[0]['is_in_stock'] : 0;
	    $websites = $product_info['websites'];
	    $barcode = '';
	    $p_ean = '';
	    $p_jan = '';
	    $p_isbn = '';
	    $p_mpn = '';

	    $product_image = $cli->call($session_id, 'catalog_product_attribute_media.list', $mage_product_id);
	    $product_image_data = array();
	    foreach ($product_image as $_image) {
		$magento_product_image_url = $_image['url'];
		$image_position = $_image['position'];
		$image_type = $_image['types'];
		if (in_array('image', $image_type)) {
		    $base_image = $magento_product_image_url;
		}
		$product_image_data[] = array(
		    'image_url' => $magento_product_image_url,
		    'label' => '',
		    'position' => $image_position,
		    'base_img' => $base_image,
		);
	    }

	    $store_id = Stores::find()->select('store_id')->where(['store_name' => 'Magento'])->one();
	    $product_data = array(
		'product_id' => $mage_product_id, // Stores/Channel product ID
		'name' => $product_name, // Product name
		'sku' => $product_sku, // Product SKU
		'description' => $product_description, // Product Description
		'product_url_path' => $product_url, // Product url if null give blank value
		'weight' => $product_weight, // Product weight if null give blank value
		'status' => $product_status, // Product status (visible or hidden). Give in 0 or 1 form (0 = hidden, 1 = visible).
		'price' => $product_price, // Porduct price
		'sale_price' => $product_price, // Product sale price if null give Product price value
		'qty' => $qty, //Product quantity 
		'stock_status' => $stock_status, // Product stock status ("in stock" or "out of stock"). 
		// Give in 0 or 1 form (0 = out of stock, 1 = in stock)
		'websites' => $websites, //This is for only magento give only and blank array
		'brand' => '', // Product brand if any
		'low_stock_notification' => 5, // Porduct low stock notification if any otherwise give default 5 value
		'created_at' => $product_created_at, // Product created at date format date('Y-m-d H:i:s')
		'updated_at' => $product_updated_at, // Product updated at date format date('Y-m-d H:i:s')
		'channel_store_name' => 'Magento', // Channel or Store name
		'channel_store_prefix' => 'MG', // Channel or store prefix id.
		'mul_store_id' => $store_connection_id, //Give multiple store id
		'mul_channel_id' => '', // Give multiple channel id
		'elliot_user_id' => $user_id, // Elliot user id
		'store_id' => $store_id->store_id, // if your are importing store give store id
		'channel_id' => '', // if your are importing channel give channel id
		'country_code' => $country_detail->sortname,
		'currency_code' => $country_detail->currency_code,
		'conversion_rate' => $conversion_rate,
		'upc' => '', // Product barcode if any
		'ean' => '', // Product ean if any
		'jan' => '', // Product jan if any
		'isban' => '', // Product isban if any
		'mpn' => '', // Product mpn if any
		'categories' => $product_categories_ids, // Product categroy array. If null give a blank array 
		'images' => $product_image_data, // Product images data
	    );
	    self::productImportingCommon($product_data);
	}
    }

    /**
     * Get Magento all customers
     */
    public function getMagentoAllCustomers($customer_list, $user_id, $session_id, $cli, $store_connection_id) {
	foreach ($customer_list as $_customer):
	    $cus_id = $_customer['customer_id'];
	    $cus_created_at = $_customer['created_at'];
	    $cus_updated_at = $_customer['updated_at'];
	    $cus_email = $_customer['email'];
	    $cus_f_name = $_customer['firstname'];
	    $cus_l_name = $_customer['lastname'];
	    $website_id = $_customer['website_id'];
	    $bill_city = $bill_company = $bill_country_id = $bill_zip = $bill_region = $bill_add = $bill_telephone = '';
	    if (isset($_customer['default_billing'])):
		$customer_default_bill_id = $_customer['default_billing'];
		$customer_bill_address = $cli->call($session_id, 'customer_address.info', $customer_default_bill_id);
		$bill_city = $customer_bill_address['city'];
		$bill_company = $customer_bill_address['company'];
		$bill_country_id = $customer_bill_address['country_id'];
		$bill_zip = $customer_bill_address['postcode'];
		$bill_region = $customer_bill_address['region'];
		$bill_add = $customer_bill_address['street'];
		$bill_telephone = $customer_bill_address['telephone'];
	    endif;
	    $ship_city = $ship_company = $ship_country_id = $ship_zip = $ship_region = $ship_street = $ship_telephone = '';
	    if (isset($_customer['default_shipping'])):
		$customer_default_ship_id = $_customer['default_billing'];
		$customer_ship_address = $cli->call($session_id, 'customer_address.info', $customer_default_ship_id);
		$ship_city = $customer_ship_address['city'];
		$ship_company = $customer_ship_address['company'];
		$ship_country_id = $customer_ship_address['country_id'];
		$ship_zip = $customer_ship_address['postcode'];
		$ship_region = $customer_ship_address['region'];
		$ship_street = $customer_ship_address['street'];
		$ship_telephone = $customer_ship_address['telephone'];
	    endif;

	    $customer_data = array(
		'customer_id' => $cus_id,
		'elliot_user_id' => $user_id,
		'first_name' => $cus_f_name,
		'last_name' => $cus_l_name,
		'email' => $cus_email,
		'channel_store_name' => 'Magento',
		'channel_store_prefix' => 'MG',
		'mul_store_id' => $store_connection_id,
		'mul_channel_id' => '',
		'customer_created_at' => $cus_created_at,
		'customer_updated_at' => $cus_updated_at,
		'billing_address' => array(
		    'street_1' => $bill_add,
		    'street_2' => '',
		    'city' => $bill_city,
		    'state' => $bill_region,
		    'country' => $bill_country_id,
		    'country_iso' => $bill_country_id,
		    'zip' => $bill_zip,
		    'phone_number' => $bill_telephone,
		    'address_type' => 'Default',
		),
		'shipping_address' => array(
		    'street_1' => $ship_street,
		    'street_2' => '',
		    'city' => $ship_city,
		    'state' => $ship_region,
		    'country' => $ship_city,
		    'zip' => $ship_zip,
		),
	    );
	    self::customerImportingCommon($customer_data);
	endforeach;
    }

    /**
     * Get Magento all Order
     */
    public function getMagentoAllOrders($orders_list, $user_id, $session_id, $cli, $store_connection_id, $country_detail, $conversion_rate) {
	foreach ($orders_list as $_order) {
	    $order_increment_id = $_order['increment_id'];
	    $order_data = $cli->call($session_id, 'sales_order.info', $order_increment_id);
	    /**
	     * Order data
	     */
	    $order_id = $order_data['order_id'];
	    $order_state = $order_data['state'];
	    $order_status = $order_data['status'];
	    $order_store_id = $order_data['store_id'];
	    $order_shipping_description = $order_data['shipping_description'];
	    $order_total = $order_data['base_grand_total'];
	    $customer_email = $order_data['customer_email'];
	    $order_shipping_amount = $order_data['base_shipping_amount'];
	    $order_tax_amount = $order_data['base_tax_amount'];
	    $order_product_qty = $order_data['total_qty_ordered'];
	    $customer_id = $order_data['customer_id'];
	    $order_created_at = $order_data['created_at'];
	    $order_updated_at = $order_data['updated_at'];
	    $payment_method = $order_data['payment']['method'];
	    $refund_amount = $order_data['subtotal_refunded'];
	    $discount_amount = $order_data['discount_amount'];

	    switch (strtolower($order_status)) {
		case "pending":
		    $order_status_2 = 'Pending';
		    break;
		case "complete":
		    $order_status_2 = "Completed";
		    break;
		case "canceled":
		    $order_status_2 = "Cancel";
		    break;
		case "closed":
		    $order_status_2 = "Closed";
		    break;
		case "fraud":
		    $order_status_2 = "Cancel";
		    break;
		case "holded":
		    $order_status_2 = "On Hold";
		    break;
		case "payment_review":
		    $order_status_2 = "Pending";
		    break;
		case "paypal_canceled_reversal":
		    $order_status_2 = "Cancel";
		    break;
		case "paypal_canceled_reversal":
		    $order_status_2 = "Cancel";
		    break;
		case "paypal_reversed":
		    $order_status_2 = "Cancel";
		    break;
		case "pending_payment":
		    $order_status_2 = "Pending";
		    break;
		case "pending_paypal":
		    $order_status_2 = "Pending";
		    break;
		case "processing":
		    $order_status_2 = "In Transit";
		    break;
		default:
		    $order_status_2 = "Pending";
	    }

	    /**
	     * Order Shipping address
	     */
	    $ship_state = $ship_zip = $ship_address = $ship_city = $ship_customer_email = $ship_phone = $ship_country_id = $ship_firstname = $ship_lastname = $ship_middlename = $shipping_address = '';
	    if (isset($order_data['shipping_address'])) {
		$order_shipping_add = $order_data['shipping_address'];
		$ship_state = isset($order_shipping_add['region']) ? $order_shipping_add['region'] : '';
		$ship_zip = isset($order_shipping_add['postcode']) ? $order_shipping_add['postcode'] : 0000;
		$ship_address = isset($order_shipping_add['street']) ? $order_shipping_add['street'] : '';
		$ship_city = isset($order_shipping_add['city']) ? $order_shipping_add['city'] : '';
		$ship_customer_email = isset($order_shipping_add['email']) ? $order_shipping_add['email'] : 'test@test.com';
		$ship_phone = isset($order_shipping_add['telephone']) ? $order_shipping_add['telephone'] : '';
		$ship_country_id = isset($order_shipping_add['country_id']) ? $order_shipping_add['country_id'] : '';
		$ship_firstname = isset($order_shipping_add['firstname']) ? $order_shipping_add['firstname'] : '';
		$ship_lastname = isset($order_shipping_add['lastname']) ? $order_shipping_add['lastname'] : '';
		$ship_middlename = isset($order_shipping_add['middlename']) ? $order_shipping_add['middlename'] : '';
		$shipping_address = $ship_address . "," . $ship_city . "," . $ship_state . "," . $ship_zip . "," . $ship_country_id;
	    }

	    /**
	     * Order Billing address
	     */
	    $bill_state = $bill_zip = $bill_address = $bill_city = $bill_customer_email = $bill_phone = $bill_country_id = $bill_firstname = $bill_lastname = $bill_middlename = $billing_address = '';
	    if (isset($order_data['billing_address'])) {
		$order_billing_add = $order_data['billing_address'];
		$bill_state = isset($order_billing_add['region']) ? $order_billing_add['region'] : '';
		$bill_zip = isset($order_billing_add['postcode']) ? $order_billing_add['postcode'] : 0000;
		$bill_address = isset($order_billing_add['street']) ? $order_billing_add['street'] : '';
		$bill_city = isset($order_billing_add['city']) ? $order_billing_add['city'] : '';
		$bill_customer_email = isset($order_billing_add['email']) ? $order_billing_add['email'] : '';
		$bill_phone = isset($order_billing_add['telephone']) ? $order_billing_add['telephone'] : '';
		$bill_country_id = isset($order_billing_add['country_id']) ? $order_billing_add['country_id'] : '';
		$bill_firstname = isset($order_billing_add['firstname']) ? $order_billing_add['firstname'] : '';
		$bill_lastname = isset($order_billing_add['lastname']) ? $order_billing_add['lastname'] : '';
		$bill_middlename = isset($order_billing_add['middlename']) ? $order_billing_add['middlename'] : '';
		$billing_address = $bill_address . "," . $bill_city . "," . $bill_state . "," . $bill_zip . "," . $bill_country_id;
	    }

	    $order_items = $order_data['items'];
	    $order_product_data = array();
	    foreach ($order_items as $_items) {
		$title = $_items['name'];
		$quantity = $_items['qty_ordered'];
		$price = $_items['price'];
		$sku = '';
		if ($_items['sku'] != "") {
		    $sku = $_items['sku'];
		}
		$product_id = ($_items['product_id'] == '') ? 0 : $_items['product_id'];
		$product_weight = $_items['weight'];
		$order_product_data[] = array(
		    'product_id' => $product_id,
		    'name' => $title,
		    'sku' => $sku,
		    'price' => $price,
		    'qty_ordered' => $quantity,
		    'weight' => $product_weight,
		);
	    }

	    $ship_country = Countries::countryDetails($ship_country_id);
	    $bill_country = Countries::countryDetails($bill_country_id);
	    $get_shopify_id = Stores::find()->select('store_id')->where(['store_name' => 'Magento'])->one();
	    $customer_id = ($customer_id == '') ? 0 : $customer_id;
	    $order_data = array(
		'order_id' => $order_id,
		'status' => $order_status_2,
		'magento_store_id' => '',
		'order_grand_total' => $order_total,
		'customer_id' => $customer_id,
		'customer_email' => $customer_email,
		'order_shipping_amount' => $order_shipping_amount,
		'order_tax_amount' => $order_tax_amount,
		'total_qty_ordered' => $order_product_qty,
		'created_at' => $order_created_at,
		'updated_at' => $order_updated_at,
		'payment_method' => $payment_method,
		'refund_amount' => $refund_amount,
		'discount_amount' => $discount_amount,
		'channel_store_name' => 'Magento',
		'channel_store_prefix' => 'MG',
		'mul_store_id' => $store_connection_id,
		'mul_channel_id' => '',
		'elliot_user_id' => $user_id,
		'store_id' => $get_shopify_id->store_id,
		'currency_code' => $country_detail->currency_code,
		'conversion_rate' => $conversion_rate,
		'channel_id' => '',
		'shipping_address' => array(
		    'street_1' => $ship_address,
		    'street_2' => '',
		    'state' => $ship_state,
		    'city' => $ship_city,
		    'country' => $ship_country->name,
		    'country_id' => $ship_country->sortname,
		    'postcode' => $ship_zip,
		    'email' => $customer_email,
		    'telephone' => $ship_phone,
		    'firstname' => $ship_firstname,
		    'lastname' => $ship_lastname,
		),
		'billing_address' => array(
		    'street_1' => $bill_address,
		    'street_2' => '',
		    'state' => $bill_state,
		    'city' => $bill_city,
		    'country' => $bill_country->name,
		    'country_id' => $bill_country->sortname,
		    'postcode' => $bill_zip,
		    'email' => $customer_email,
		    'telephone' => $bill_phone,
		    'firstname' => $bill_firstname,
		    'lastname' => $bill_lastname,
		),
		'items' => $order_product_data,
	    );
	    Stores::orderImportingCommon($order_data);
	}
    }

    /* Single Product Page Create product on channel mangaer */

    public function channel_manager_create_product($channel_sale, $pId, $post_data) {

	/* Get data For Product Id */
	$users_Id = Yii::$app->user->identity->id;
	$ProductChannel = ProductChannel::find()->where(['product_id' => $pId])->all();
	$names = array();
	foreach ($ProductChannel as $pc) {

	    if (!empty($pc->store_id)) {
		/* For Stores */
		$store_connection_data = StoresConnection::find()->Where(['stores_connection_id' => $pc->store_id])->with('storesDetails')->one();
		if (!empty($store_connection_data)) {
		    $store_details_country = @$store_connection_data->storesDetails->country;
		    if (!empty($store_details_country)) {
			$store_country = $store_connection_data->storesDetails->country;
		    } else {
			$store_country = '';
		    }
		    $store_connection_id = $store_connection_data->stores_connection_id;
		    $get_store_id = $store_connection_data->store_id;
		    $store_names = Stores::find()->Where(['store_id' => $get_store_id])->one();
		    $names[] = trim($store_names->store_name . ' ' . $store_country);
		}
	    }

	    if (!empty($pc->channel_id)) {
		/* For Channels */
		/* For Channels */
		$channels_name = channels::find()->where(['channel_id' => $pc->channel_id])->one();
		$channel_parent_name = $channels_name->parent_name;

		$channel_name_org = ucwords(str_replace("-", " ", substr($channel_parent_name, 8)));
		if ($channel_name_org == 'Lazada') {
		    $channel_name_org = $channels_name->channel_name;
		}
		$names[] = trim($channel_name_org);
	    }
	}



	if (!empty($channel_sale)) {
	    /* Delete value in array already exist product channel or store */
	    /* For array Diff Swap the variable */
	    $result = array();
	    foreach ($channel_sale as $ch) {
		if (!in_array($ch, $names)) {
		    $result[] = $ch;
		}
	    }

	    /* $var1 = (count($names) > count($channel_sale)) ? $names : $channel_sale;
	      $var2 = $channel_sale;
	      if ($var1 == $channel_sale) {
	      $var2 = $names;
	      }
	      $result = array_diff($var1, $var2); */


	    /* Schedule Sale date */
	    $schedule_date = $_POST['pSchedule_date'];
	    $schedule_date_time = date('Y-m-d h:i:s', strtotime($schedule_date));
	    $stock_qty = $_POST['pStk_qty'];
	    $low_stc_ntf = $_POST['plow_stk_ntf'];

	    /* Create Product In Selected Channels */

	    foreach ($result as $channel) {

		$channel_name_explode = explode(" ", $channel);
		$channel_name_check = $channel_name_explode[0];
		$check_valid_store = Stores::find()->where(['store_name' => $channel_name_check])->one();
		if (!empty($check_valid_store)) {
		    $fin_channel = $channel_name_check;
		} else {
		    $fin_channel = $channel;
		}

		/* Product Create For WeChat */
		if ($fin_channel == 'WeChat') {

		    //get wechat Service account id 
		    $get_big_id = Channels::find()->where(['parent_name' => 'channel_' . $channel])->all();
		    foreach ($get_big_id as $ids) {
			$arr_id[] = $ids->channel_ID;
			$arr_name[] = $ids->channel_name;
			$ids->channel_name;
		    }
		    $channel_ids = implode(",", $arr_id);
		    $con_details = ChannelConnection::find()->where('channel_id IN(' . $channel_ids . ')')->one();
		    $wechat_token = $con_details->token;
		    $wechat_project_id = $con_details->wechat_project_id;
		    $users_id = $con_details->user_id;
		    $channel_id = $con_details->channel_id;
		    /* For WEchat Categories  */
		    $pcat = isset($post_data['pcat']) ? $post_data['pcat'] : '';
		    /* Create array for a store category id */
		    $cat_obj_id = array();
		    if (!empty($pcat)) {
			foreach ($pcat as $pcat_data) {
			    /* get wechat category acc to name */
			    $wechat_db_cat_data = Categories::find()->Where(['category_name' => $pcat_data])->one();
			    $wechat_db_cat_data_cat_id = $wechat_db_cat_data->category_ID;

			    $cat_abb_data = CategoryAbbrivation::find()->Where(['category_ID' => $wechat_db_cat_data_cat_id, 'channel_accquired' => 'WeChat'])->one();

			    if (empty($cat_abb_data)) {
				$wechat_cat_group_id = 0;
			    } else {
				$wechat_cat_group_id = $cat_abb_data->store_category_groupid;
			    }
			    $url = 'https://cms-api.walkthechat.com/categories/product/groups/' . $wechat_cat_group_id . '';

			    $rs_cat = ChannelsController::wechatcurl($url, $wechat_token, $wechat_project_id);
			    $wechat_categories = json_decode($rs_cat);

			    $wechat_cat_id = array();
			    /* create cat in wechat if cat is not exist in wechat */
			    if (empty($wechat_categories->categories)) {
				/* create category object to send in wechat */
				$new_cat = (object) [];
				$new_cat->language = "en";
				$new_cat->name = $pcat_data;
				//convert object to json
				$wechat_object = json_encode(array($new_cat));
				$wechat_cat_object = array("category" => $wechat_object);
				//$wechat_cat_object=   [category] => [{"language":"en","name":"shub"}]
				$c_cat = curl_init();
				curl_setopt_array($c_cat, array(
				    CURLOPT_URL => "https://cms-api.walkthechat.com/categories/product/groups",
				    CURLOPT_RETURNTRANSFER => true,
				    CURLOPT_ENCODING => "",
				    CURLOPT_MAXREDIRS => 10,
				    CURLOPT_TIMEOUT => 30,
				    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				    CURLOPT_CUSTOMREQUEST => 'POST',
				    CURLOPT_POSTFIELDS => $wechat_cat_object,
				    CURLOPT_HTTPHEADER => array(
					"x-access-token: $wechat_token",
					"x-id-project: $wechat_project_id"
				    ),
				));
				$create_cat_wechat_data = json_decode(curl_exec($c_cat));

				/* store save cat id in array */
				if (!empty($create_cat_wechat_data->categories[0])) {

				    $wechat_cat_id[] = $create_cat_wechat_data->categories[0]->groupId;
				    /* If wechat cat delete in wechat then update if not then create */
				    $cat_wechat_check = CategoryAbbrivation::find()->Where(['category_ID' => $wechat_db_cat_data_cat_id, 'store_category_groupid' => $wechat_cat_group_id, 'channel_accquired' => 'WeChat'])->one();
				    if (!empty($cat_wechat_check)) {

					$cat_wechat_check->channel_abb_id = 'WC' . $create_cat_wechat_data->categories[0]->_id;
					$cat_wechat_check->category_ID = $wechat_db_cat_data_cat_id;
					$cat_wechat_check->store_category_groupid = $create_cat_wechat_data->categories[0]->groupId;
					$cat_wechat_check->channel_accquired = 'WeChat';
					$cat_wechat_check->created_at = date('Y-m-d H:i:s', time());
					$cat_wechat_check->save(false);
				    } else {
					$CategoryAbbrivation = new CategoryAbbrivation();
					$CategoryAbbrivation->channel_abb_id = 'WC' . $create_cat_wechat_data->categories[0]->_id;
					$CategoryAbbrivation->category_ID = $wechat_db_cat_data_cat_id;
					$CategoryAbbrivation->store_category_groupid = $create_cat_wechat_data->categories[0]->groupId;
					$CategoryAbbrivation->channel_accquired = 'WeChat';
					$CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
					$CategoryAbbrivation->save(false);
				    }
				}
				/* if category is exist in we chat then get category */
			    } else {
				/* store cat id in array */
				$wechat_cat_id[] = $wechat_categories->categories[0]->groupId;
			    }
			}
		    }
		    $product_images = ProductImages::find()->where(['product_ID' => $pId])->one();
		    if (!empty($product_images)) {
			$thumb_image = $product_images->link;
		    } else {
			$thumb_image = '';
		    }
		    $p_saleprice = $post_data['pSale_price'];
		    /* For if sale price empty null or 0 then price value is  sale price value */
		    if ($post_data['pSale_price'] == '' || $post_data['pSale_price'] == Null || $post_data['pSale_price'] == 0) {
			$p_saleprice = $post_data['pPrice'];
		    }
		    $product_price = $post_data['pPrice'];
		    $conversion_rate=1;
		    $currency='CNY';
		    if (!empty($currency)) {
			$conversion_rate = self::getDbConversionRate($currency);
		    }
		    //$conversion_rate = self::getCurrencyConversionRate('USD', 'CNY');
		    $wechat_product_price = number_format((float) $conversion_rate * $product_price, 2, '.', '');
		    $wechat_product_sale_price = number_format((float) $conversion_rate * $p_saleprice, 2, '.', '');

		    $wechat_object = array(
			"title" => $post_data['pName'],
			"description" => $post_data['pDescription'],
			"sku" => $post_data['pSKU'],
			"barcode" => $post_data['pUPC'],
			"price" => $wechat_product_price,
			"salesPrice" => $wechat_product_sale_price,
			"unit" => $post_data['pStk_qty'],
			"categoryGroupId" => $wechat_cat_id,
			"thumbnail" => $thumb_image,
			"weight" => $post_data['pWeight']
		    );

		    $c_cat = curl_init();
		    curl_setopt_array($c_cat, array(
			CURLOPT_URL => "https://cms-api.walkthechat.com/products",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => http_build_query($wechat_object),
			CURLOPT_HTTPHEADER => array(
			    "x-access-token: $wechat_token",
			    "x-id-project: $wechat_project_id"
			),
		    ));

		    $data = json_decode(curl_exec($c_cat));
		    if ($data->token->success == 1 || $data->token->success == true) {
			$product_data = new ProductChannel;
			$product_data->channel_id = $channel_id;
			$product_data->product_id = $pId;
			$product_data->created_at = date('Y-m-d h:i:s', time());
			if ($product_data->save(false)) {

			    if (!empty($pcat)) {
				/* Delaete Product Categories */
				$Delete_product_categories = ProductCategories::deleteAll(['product_ID' => $pId]);
				foreach ($pcat as $pcat_data1) {
				    /* get wechat category acc to name */
				    $wechat_db_cat_data1 = Categories::find()->Where(['category_name' => $pcat_data1])->one();
				    $wechat_db_cat_data_cat_id1 = $wechat_db_cat_data1->category_ID;

				    $productCategoryModel = new ProductCategories;
				    $productCategoryModel->category_ID = $wechat_db_cat_data_cat_id1;
				    $productCategoryModel->product_ID = $pId;
				    $productCategoryModel->created_at = date('Y-m-d H:i:s', time());
				    //Save Elliot User id
				    $productCategoryModel->elliot_user_id = Yii::$app->user->identity->id;
				    $productCategoryModel->save(false);
				}
			    }
			    $productAbbrivation = new ProductAbbrivation();
			    $productAbbrivation->channel_abb_id = 'WC' . $data->product->groupId;
			    $productAbbrivation->product_id = $pId;
			    $productAbbrivation->channel_accquired = 'WeChat';
			    $productAbbrivation->price = $post_data['pPrice'];
			    $productAbbrivation->salePrice = $post_data['pSale_price'];
			    $productAbbrivation->schedules_sales_date = $schedule_date_time;
			    $productAbbrivation->stock_quantity = $post_data['pStk_qty'];
			    $productAbbrivation->stock_level = $_POST['pStk_lvl'];
			    $productAbbrivation->stock_status = $_POST['pStk_status'];
			    $productAbbrivation->created_at = date('Y-m-d H:i:s', time());
			    $productAbbrivation->save(false);
			}
		    }
		}


		/* Product Create For BigCommerce */
		if ($fin_channel == 'BigCommerce') {
		    $channel_space_pos = strpos($channel, ' ');
		    $country_sub_str = substr($channel, $channel_space_pos);
		    $store_country = trim($country_sub_str);
		    $store_details_data = StoreDetails::find()->Where(['channel_accquired' => 'BigCommerce', 'country' => $store_country])->with('storeConnection')->one();

		    if (!empty($store_details_data)) {
			$store_connection_id = $store_details_data->store_connection_id;
			$big_client_id = $store_details_data->storeConnection->big_client_id;
			$big_access_token = $store_details_data->storeConnection->big_access_token;
			$big_store_hash = $store_details_data->storeConnection->big_store_hash;
			$currency = $store_details_data->currency;
			//BigCommerce Configuration Object
			Bigcommerce::configure(array(
			    'client_id' => $big_client_id,
			    'auth_token' => $big_access_token,
			    'store_hash' => $big_store_hash
			));

			/* For Bigcommerce Categories */
			$pcat = isset($post_data['pcat']) ? $post_data['pcat'] : '';
			/* Create array for a store category id */
			$cat_obj_id = array();
			if (!empty($pcat)) {
			    foreach ($pcat as $pcat_data) {
				$bigcommerce_db_cat_data = Categories::find()->Where(['category_name' => $pcat_data])->one();
				$bigcommerce_db_cat_data_cat_id = $bigcommerce_db_cat_data->category_ID;
				/* get Bigcommerce category acc to name */
				$parent_filter1 = array("name" => $pcat_data);
				$parent_categories1 = Bigcommerce::getCategories($parent_filter1);
				if (empty($parent_categories1)) {
				    /* create category in Bigcommerce if category not exist in bgc */
				    $catobject = array("name" => $pcat_data, "parent_id" => 0);
				    $create_cat_bigCommerce = Bigcommerce::createResource('/categories', $catobject);
				    if (!empty($create_cat_bigCommerce)) {
					$cat_bigcommerce_check = CategoryAbbrivation::find()->Where(['category_ID' => $bigcommerce_db_cat_data_cat_id, 'channel_accquired' => 'BigCommerce', 'mul_store_id' => $store_connection_id])->one();
					if (!empty($cat_wechat_check)) {
					    $cat_bigcommerce_check->channel_abb_id = 'BGC' . $create_cat_bigCommerce->id;
					    $cat_bigcommerce_check->category_ID = $wechat_db_cat_data_cat_id;
					    $cat_bigcommerce_check->channel_accquired = 'BigCommerce';
					    $cat_bigcommerce_check->created_at = date('Y-m-d H:i:s', time());
					    $cat_bigcommerce_check->save(false);
					} else {
					    $CategoryAbbrivation = new CategoryAbbrivation();
					    $CategoryAbbrivation->channel_abb_id = 'BGC' . $create_cat_bigCommerce->id;
					    $CategoryAbbrivation->mul_store_id = $store_connection_id;
					    $CategoryAbbrivation->category_ID = $bigcommerce_db_cat_data_cat_id;
					    $CategoryAbbrivation->channel_accquired = 'BigCommerce';
					    $CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
					    $CategoryAbbrivation->save(false);
					}
				    }
				    /* store cat id in array */
				    $cat_obj_id[] = $create_cat_bigCommerce->id;
				} else {
				    /* store cat id in array */
				    $cat_obj_id[] = $parent_categories1[0]->id;
				}
			    }
			}

			$pstk_status = $post_data['pStk_status'];
			if ($pstk_status == "Visible") {
			    $update_pstk_status = "simple";
			} else {
			    $update_pstk_status = "none";
			}

			$product_name = !empty($post_data['pName']) ? $post_data['pName'] : "";
			$product_sku = ($post_data['pSKU'] == 'Empty' || $post_data['pSKU'] == '') ? '' : $post_data['pSKU'];
			$product_upc = ($post_data['pUPC'] == 'Empty' || $post_data['pUPC'] == '') ? '' : $post_data['pUPC'];
			$product_cond = !empty($post_data['pCond']) ? $post_data['pCond'] : "New";
			$product_sale_price = !empty($post_data['pSale_price']) ? str_replace(",", "", $post_data['pSale_price']) : 0;
			$product_price = !empty($post_data['pPrice']) ? str_replace(",", "", $post_data['pPrice']) : 0;
			$product_stk_qty = !empty($post_data['pStk_qty']) ? $post_data['pStk_qty'] : 0;
			$product_stk_ntf = !empty($post_data['plow_stk_ntf']) ? $post_data['plow_stk_ntf'] : 2;
			$product_weight = !empty($post_data['pWeight']) ? $post_data['pWeight'] : 0;
			$product_desc = !empty($post_data['pDescription']) ? $post_data['pDescription'] : '';

			/* For if sale price empty null or 0 then price value is  sale price value */
			if ($product_sale_price == '' || $product_sale_price == Null || $product_sale_price == 0) {
			    $product_sale_price = $product_price;
			}
			if ($product_stk_qty < 0) {
			    $product_stk_qty = 0;
			} else {
			    $product_stk_qty = $product_stk_qty;
			}
			
			$conversion_rate=1;
			if (!empty($currency)) {
			    $conversion_rate = self::getDbConversionRate($currency);
			}
			//$conversion_rate = self::getCurrencyConversionRate('USD', $currency);
			$bigcommerce_product_price = number_format((float) $conversion_rate * $product_price, 2, '.', '');
			$bigcommerce_product_sale_price = number_format((float) $conversion_rate * $product_sale_price, 2, '.', '');

			$Bigcommerce_object = array(
			    "name" => $product_name,
			    "sku" => $product_sku,
			    "upc" => $product_upc,
			    "condition" => $product_cond,
			    "type" => 'physical',
			    "description" => $product_desc,
			    "sale_price" => $bigcommerce_product_sale_price,
			    "price" => $bigcommerce_product_price,
			    //"categories" => '',
			    "categories" => $cat_obj_id,
			    "inventory_level" => $product_stk_qty,
			    "availability" => 'available',
			    "inventory_warning_level" => $product_stk_ntf,
			    "inventory_tracking" => $update_pstk_status,
			    "is_visible" => true,
			    "weight" => $product_weight
			);

			$products = Bigcommerce::createResource('/products', $Bigcommerce_object);
			if (!empty($products)) {

			    /* Save images on Bigcommerce */
			    // Get Image From Db
			    $product_images = ProductImages::find()->where(['product_ID' => $pId])->all();
			    $count_product_images = count($product_images);
			    $BigCm_product_id = $products->id;

			    //Save multiple Image
			    foreach ($product_images as $product_img) {

				$image_link = $product_img->link;

				$objectimg = array(
				    "image_file" => $image_link,
				    "is_thumbnail" => true
				);
				$save_product_image = Bigcommerce::createResource('/products/' . $BigCm_product_id . '/images', $objectimg);
			    }

			    $product_data = new ProductChannel;
			    $product_data->store_id = $store_connection_id;
			    $product_data->product_id = $pId;
			    $product_data->elliot_user_id = $users_Id;
			    $product_data->created_at = date('Y-m-d h:i:s', time());
			    if ($product_data->save(false)) {

				if (!empty($pcat)) {
				    /* Delaete Product Categories */
				    $Delete_product_categories = ProductCategories::deleteAll(['product_ID' => $pId]);
				    foreach ($pcat as $pcat_data1) {
					/* get wechat category acc to name */
					$bigcommerce_db_cat_data1 = Categories::find()->Where(['category_name' => $pcat_data1])->one();
					$bigcommerce_db_cat_data_cat_id1 = $bigcommerce_db_cat_data1->category_ID;
					$productCategoryModel = new ProductCategories();
					$productCategoryModel->category_ID = $bigcommerce_db_cat_data_cat_id1;
					$productCategoryModel->product_ID = $pId;
					$productCategoryModel->created_at = date('Y-m-d H:i:s', time());
					//Save Elliot User id
					$productCategoryModel->elliot_user_id = $users_Id;
					$productCategoryModel->save(false);
				    }
				}
				$productAbbrivation = new ProductAbbrivation();
				$productAbbrivation->channel_abb_id = 'BGC' . $products->id;
				$productAbbrivation->product_id = $pId;
				$productAbbrivation->mul_store_id = $store_connection_id;
				$productAbbrivation->channel_accquired = 'BigCommerce';
				$productAbbrivation->price = $product_price;
				$productAbbrivation->salePrice = $product_sale_price;
				$productAbbrivation->schedules_sales_date = $schedule_date_time;
				$productAbbrivation->stock_quantity = $post_data['pStk_qty'];
				$productAbbrivation->stock_level = $_POST['pStk_lvl'];
				$productAbbrivation->stock_status = $_POST['pStk_status'];
				//$productAbbrivation->low_stock_notification = $low_stc_ntf;
				$productAbbrivation->created_at = date('Y-m-d H:i:s', time());
				$productAbbrivation->save(false);
			    }
			}
		    }
		}
		/*		 * Product Create For Woocommerce */
		if ($fin_channel == 'WooCommerce') {

		    $channel_space_pos = strpos($channel, ' ');
		    $country_sub_str = substr($channel, $channel_space_pos);
		    $store_country = trim($country_sub_str);
		    $store_details_data = StoreDetails::find()->Where(['channel_accquired' => 'WooCommerce', 'country' => $store_country])->with('storeConnection')->one();

		    if (!empty($store_details_data)) {

			$store_connection_id = $store_details_data->store_connection_id;
			$woo_store_url = $store_details_data->storeConnection->woo_store_url;
			$woo_consumer_key = $store_details_data->storeConnection->woo_consumer_key;
			$woo_secret_key = $store_details_data->storeConnection->woo_secret_key;
			$woocommerce_store_id = $store_details_data->storeConnection->store_id;
			$store_country_code = $store_details_data->country_code;
			$currency = $store_details_data->currency;

			$pstk_status = $post_data['pStk_status'];
			if ($pstk_status == "Visible") {
			    $update_pstk_status = "simple";
			} else {
			    $update_pstk_status = "none";
			}

			$product_name = !empty($post_data['pName']) ? $post_data['pName'] : "";
			$product_sku = ($post_data['pSKU'] == 'Empty' || $post_data['pSKU'] == '') ? '' : $post_data['pSKU'];
			$product_upc = ($post_data['pUPC'] == 'Empty' || $post_data['pUPC'] == '') ? '' : $post_data['pUPC'];
			$product_cond = !empty($post_data['pCond']) ? $post_data['pCond'] : "New";
			$product_sale_price = !empty($post_data['pSale_price']) ? str_replace(",", "", $post_data['pSale_price']) : 0;
			$product_price = !empty($post_data['pPrice']) ? str_replace(",", "", $post_data['pPrice']) : 0;
			$product_stk_qty = !empty($post_data['pStk_qty']) ? $post_data['pStk_qty'] : 0;
			$product_stk_ntf = !empty($post_data['plow_stk_ntf']) ? $post_data['plow_stk_ntf'] : 2;
			$product_weight = !empty($post_data['pWeight']) ? $post_data['pWeight'] : 0;
			$product_desc = !empty($post_data['pDescription']) ? $post_data['pDescription'] : '';

			/* For if sale price empty null or 0 then price value is  sale price value */
			if ($product_sale_price == '' || $product_sale_price == Null || $product_sale_price == 0) {
			    $product_sale_price = $product_price;
			}
			if ($product_stk_qty < 0) {
			    $product_stk_qty = 0;
			} else {
			    $product_stk_qty = $product_stk_qty;
			}
			
			$conversion_rate=1;
			if (!empty($currency)) {
			    $conversion_rate = self::getDbConversionRate($currency);
			}
			//$conversion_rate = self::getCurrencyConversionRate('USD', $currency);
			$woocommerce_product_price = number_format((float) $conversion_rate * $product_price, 2, '.', '');
			$woocommerce_product_sale_price = number_format((float) $conversion_rate * $product_sale_price, 2, '.', '');

			/* For Bigcommerce Categories */
			$product_categories = isset($post_data['pcat']) ? $post_data['pcat'] : '';
			$woo_cat_array = array();
			if (!empty($product_categories)) {
			    foreach ($product_categories as $cat) {
				$checkCategoryName = Categories::find()->where(['category_name' => $cat])->one();
				if (!empty($checkCategoryName)) {
				    $category_id = $checkCategoryName->category_ID;
				    $checkCategoryAbbrivation = CategoryAbbrivation::find()->where(['category_ID' => $category_id, 'channel_accquired' => 'WooCommerce', 'mul_store_id' => $store_connection_id])->one();
				    if (!empty($checkCategoryAbbrivation)) {
					$woo_cat_id = substr($checkCategoryAbbrivation->channel_abb_id, 3);
					$woo_cat_array[]['id'] = $woo_cat_id;
				    }
				}
			    }
			}

			$product_images = ProductImages::find()->where(['product_ID' => $pId])->all();
			$image_array = array();
			foreach ($product_images as $product_img) {
			    $image_link = $product_img->link;
			    $position = $product_img->priority;
			    $image_array[] = array(
				'src' => $image_link,
				'position' => $position
			    );
			}
			$data = [
			    'name' => $product_name,
			    'sku' => $product_sku,
			    'type' => 'simple',
			    'regular_price' => $woocommerce_product_price,
			    //'sale_price' => $woocommerce_product_sale_price,
			    'description' => $product_desc,
			    'status' => 'publish',
			    'stock_quantity' => $product_stk_qty,
			    'weight' => $product_weight,
			    'categories' => $woo_cat_array,
			    'images' => $image_array
//                            'images' => [
//                                [
//                                    'src' => 'https://cdn6.bigcommerce.com/s-amnictxy47/products/120/images/383/0446a03d291edee574e8d18e7517b224_bec40f51-4302-4587-aa5c-726bcd511c2f__23442.1505563444.190.285.jpg?c=2',
//                                    'position' => 0
//                                ]
//                             ]
			];


			$woocommerce = new Woocommerce(
				$woo_store_url, $woo_consumer_key, $woo_secret_key, [
			    'wp_api' => true,
			    'version' => 'wc/v1',
			    "query_string_auth" => true,
				]
			);

			try {
			    $woo_create_product = $woocommerce->post('products', $data);

			    $lastRequest = $woocommerce->http->getRequest();
			    $lastRequest->getUrl(); // Requested URL (string).
			    $lastRequest->getMethod(); // Request method (string).
			    $lastRequest->getParameters(); // Request parameters (array).
			    $lastRequest->getHeaders(); // Request headers (array).
			    $lastRequest->getBody(); // Request body (JSON).
			    // Last response data.
			    $lastResponse = $woocommerce->http->getResponse();
			    $lastResponse->getCode(); // Response code (int).
			    $lastResponse->getHeaders(); // Response headers (array).
			    $lastResponse->getBody(); // Response body (JSON).

			    if (!empty($woo_create_product)) {
				$woo_product_id = $woo_create_product['id'];
				$product_data = new ProductChannel;
				$product_data->store_id = $store_connection_id;
				$product_data->product_id = $pId;
				$product_data->elliot_user_id = $users_Id;
				$product_data->created_at = date('Y-m-d h:i:s', time());
				if ($product_data->save(false)) {
				    if (!empty($pcat)) {
					/* Delaete Product Categories */
					$Delete_product_categories = ProductCategories::deleteAll(['product_ID' => $pId]);
					foreach ($pcat as $pcat_data1) {
					    /* get wechat category acc to name */
					    $woo_db_cat_data1 = Categories::find()->Where(['category_name' => $pcat_data1])->one();
					    $woo_db_cat_data_cat_id1 = $woo_db_cat_data1->category_ID;

					    $productCategoryModel = new ProductCategories;
					    $productCategoryModel->category_ID = $woo_db_cat_data_cat_id1;
					    $productCategoryModel->product_ID = $pId;
					    $productCategoryModel->created_at = date('Y-m-d H:i:s', time());
					    //Save Elliot User id
					    $productCategoryModel->elliot_user_id = $users_Id;
					    $productCategoryModel->save(false);
					}
				    }
				}

				$productAbbrivation = new ProductAbbrivation();
				$productAbbrivation->channel_abb_id = 'WOO' . $woo_product_id;
				$productAbbrivation->product_id = $pId;
				$productAbbrivation->mul_store_id = $store_connection_id;
				$productAbbrivation->channel_accquired = 'WooCommerce';
				$productAbbrivation->price = $product_price;
				$productAbbrivation->salePrice = $product_sale_price;
				$productAbbrivation->schedules_sales_date = $schedule_date_time;
				$productAbbrivation->stock_quantity = $post_data['pStk_qty'];
				$productAbbrivation->stock_level = $_POST['pStk_lvl'];
				$productAbbrivation->stock_status = $_POST['pStk_status'];
				//$productAbbrivation->low_stock_notification = $low_stc_ntf;
				$productAbbrivation->created_at = date('Y-m-d H:i:s', time());
				$productAbbrivation->save(false);
			    }
			} catch (HttpClientException $ex) {
//                            echo "-------------------";
//                                echo'<pre>';
//                                 $msg = $ex->getMessage();
//                                echo'<pre>';
//                                print_r($msg);
//                                die('msg');
			}
		    }
		}

		/* For Google Shopping */
		if ($fin_channel == 'Google Shopping') {
		    $product_sale_price = !empty($post_data['pSale_price']) ? str_replace(",", "", $post_data['pSale_price']) : 0;
		    $product_price = !empty($post_data['pPrice']) ? str_replace(",", "", $post_data['pPrice']) : 0;

		    /* For if sale price empty null or 0 then price value is  sale price value */
		    if ($product_sale_price == '' || $product_sale_price == Null || $product_sale_price == 0) {
			$product_sale_price = $product_price;
		    }
		    $channel_data = Channels::find()->Where(['channel_name' => 'Google Shopping'])->one();
		    $channel_id = $channel_data->channel_ID;
		    $Google_product_abb_check = ProductAbbrivation::find()->Where(['product_id' => $pId, 'channel_accquired' => 'Google Shopping']);
		    if (!empty($Google_product_abb_check)) {
			$product_data = new ProductChannel;
			$product_data->channel_id = $channel_id;
			$product_data->product_id = $pId;
			$product_data->created_at = date('Y-m-d h:i:s', time());
			if ($product_data->save(false)) {
			    $productAbbrivation = new ProductAbbrivation();
			    $productAbbrivation->channel_abb_id = 'GS' . $pId;
			    $productAbbrivation->product_id = $pId;
			    $productAbbrivation->channel_accquired = 'Google Shopping';
			    $productAbbrivation->price = $product_price;
			    $productAbbrivation->salePrice = $product_price;
			    $productAbbrivation->schedules_sales_date = $schedule_date_time;
			    $productAbbrivation->stock_quantity = $post_data['pStk_qty'];
			    $productAbbrivation->stock_level = $_POST['pStk_lvl'];
			    $productAbbrivation->stock_status = $_POST['pStk_status'];
			    //$productAbbrivation->low_stock_notification = $low_stc_ntf;
			    $productAbbrivation->created_at = date('Y-m-d H:i:s', time());
			    $productAbbrivation->save(false);
			}
		    }
		}

		/* For Facebook */
		if ($fin_channel == 'Facebook') {
		    $product_sale_price = !empty($post_data['pSale_price']) ? str_replace(",", "", $post_data['pSale_price']) : 0;
		    $product_price = !empty($post_data['pPrice']) ? str_replace(",", "", $post_data['pPrice']) : 0;

		    /* For if sale price empty null or 0 then price value is  sale price value */
		    if ($product_sale_price == '' || $product_sale_price == Null || $product_sale_price == 0) {
			$product_sale_price = $product_price;
		    }

		    $channel_data = Channels::find()->Where(['channel_name' => 'Facebook'])->one();
		    $channel_id = $channel_data->channel_ID;
		    $Google_product_abb_check = ProductAbbrivation::find()->Where(['product_id' => $pId, 'channel_accquired' => 'Facebook']);
		    if (!empty($Google_product_abb_check)) {
			$product_data = new ProductChannel;
			$product_data->channel_id = $channel_id;
			$product_data->product_id = $pId;
			$product_data->created_at = date('Y-m-d h:i:s', time());
			if ($product_data->save(false)) {
			    $productAbbrivation = new ProductAbbrivation();
			    $productAbbrivation->channel_abb_id = 'FB' . $pId;
			    $productAbbrivation->product_id = $pId;
			    $productAbbrivation->channel_accquired = 'Facebook';
			    $productAbbrivation->price = $product_price;
			    $productAbbrivation->salePrice = $product_sale_price;
			    $productAbbrivation->schedules_sales_date = $schedule_date_time;
			    $productAbbrivation->stock_quantity = $post_data['pStk_qty'];
			    $productAbbrivation->stock_level = $_POST['pStk_lvl'];
			    $productAbbrivation->stock_status = $_POST['pStk_status'];
			    //$productAbbrivation->low_stock_notification = $low_stc_ntf;
			    $productAbbrivation->created_at = date('Y-m-d H:i:s', time());
			    $productAbbrivation->save(false);
			}
		    }
		}
		/**
		 * For Square product create
		 */
		if ($fin_channel == 'Square') {
		    self::squareProductCreate($pId, $post_data);
		}
		/* For Shopify Product create */
		if ($fin_channel == 'Shopify') {
		    $channel_space_pos = strpos($channel, ' ');
		    $country_sub_str = substr($channel, $channel_space_pos);
		    $store_country = trim($country_sub_str);
		    $store_name = 'Shopify';
		    $store_details_data = StoreDetails::find()->Where(['channel_accquired' => $store_name, 'country' => $store_country])->with('storeConnection')->one();
		    self::shopifyProductCreate($pId, $post_data, $store_details_data, $store_name);
		}
		if ($fin_channel == 'ShopifyPlus') {
		    $channel_space_pos = strpos($channel, ' ');
		    $country_sub_str = substr($channel, $channel_space_pos);
		    $store_country = trim($country_sub_str);
		    $store_name = 'ShopifyPlus';
		    $store_details_data = StoreDetails::find()->Where(['channel_accquired' => $store_name, 'country' => $store_country])->with('storeConnection')->one();
		    self::shopifyProductCreate($pId, $post_data, $store_details_data, $store_name);
		}
		/* For Lazada Product create */
		if ($fin_channel == 'Lazada Malaysia') {
		    self::lazadaMalaysiaProductCreate($pId, $post_data, $channel);
		}
		/* For Magento 1X Product create */
		if ($fin_channel == 'Magento') {
		    $channel_space_pos = strpos($channel, ' ');
		    $country_sub_str = substr($channel, $channel_space_pos);
		    $store_country = trim($country_sub_str);
		    $magento_store_details_data = StoreDetails::find()->Where(['channel_accquired' => 'Magento', 'country' => $store_country])->with('storeConnection')->one();
		    self::magentoProductCreate($pId, $post_data, $magento_store_details_data);
		}
		/** For Magento 2X Product create */
		if ($fin_channel == 'Magento2') {
		    $channel_space_pos = strpos($channel, ' ');
		    $country_sub_str = substr($channel, $channel_space_pos);
		    $store_country = trim($country_sub_str);
		    $magento_store_details_data = StoreDetails::find()->Where(['channel_accquired' => 'Magento2', 'country' => $store_country])->with('storeConnection')->one();
		    self::magento2ProductCreate($pId, $post_data, $magento_store_details_data);
		}
	    }
	}
    }

    /* For Product Update Channel or Stores */

    public function update_product($pId, $post_data, $channel_sale) {
	$users_Id = Yii::$app->user->identity->id;
	/* Update Object */
	$product_stk_qty = empty($post_data['pStk_qty']) || $post_data['pStk_qty'] == 'Empty' ? 0 : $post_data['pStk_qty'];
	$product_stk_ntf = empty($post_data['plow_stk_ntf']) || $post_data['plow_stk_ntf'] == 'Empty' ? 2 : $post_data['plow_stk_ntf'];
	$pstk_status = $post_data['pStk_status'];
	if ($pstk_status == "Visible") :
	    $update_pstk_status = "simple";
	else :
	    $update_pstk_status = "none";
	endif;

	foreach ($channel_sale as $channel) {
	    $channel_name_explode = explode(" ", $channel);
	    $channel_name_check = $channel_name_explode[0];
	    $check_valid_store = Stores::find()->where(['store_name' => $channel_name_check])->one();
	    if (!empty($check_valid_store)) {
		$fin_channel = $channel_name_check;
	    } else {
		$fin_channel = $channel;
	    }

	    if ($fin_channel == 'BigCommerce') {
		$channel_space_pos = strpos($channel, ' ');
		$country_sub_str = substr($channel, $channel_space_pos);
		$store_country = trim($country_sub_str);
		$store_details_data = StoreDetails::find()->Where(['channel_accquired' => 'BigCommerce', 'country' => $store_country])->with('storeConnection')->one();

		if (!empty($store_details_data)) {
		    $store_connection_id = $store_details_data->store_connection_id;
		    $big_client_id = $store_details_data->storeConnection->big_client_id;
		    $big_access_token = $store_details_data->storeConnection->big_access_token;
		    $big_store_hash = $store_details_data->storeConnection->big_store_hash;
		    $currency = $store_details_data->currency;

		    //BigCommerce Configuration Object
		    Bigcommerce::configure(array(
			'client_id' => $big_client_id,
			'auth_token' => $big_access_token,
			'store_hash' => $big_store_hash
		    ));


		    $ProductChannel = ProductChannel::find()->where(['product_id' => $pId, 'store_id' => $store_connection_id])->one();
		    if (!empty($ProductChannel)) {

			$product_sale_price = $product_price = $product_schedule_date = $product_stock_qty = $product_stock_notification = $product_stock_status = $product_stock_level = '';
			$final_product_stock_status = false;

			/* Connected Bigcommerce Price */
			$product_price = self::getConnectedPrice($pId, 'BigCommerce' . $store_connection_id);

			$conversion_rate = 1;
			if (!empty($currency)) {
			    $conversion_rate = self::getDbConversionRate($currency);
			}

			//$conversion_rate = self::getCurrencyConversionRate('USD', $currency);
			$bigcommerce_product_price = number_format((float) $conversion_rate * $product_price, 2, '.', '');

			/* Connected Bigcommerce Sale Price */
			$product_sale_price = self::getConnectedSalePrice($pId, 'BigCommerce' . $store_connection_id);
			$bigcommerce_product_sale_price = number_format((float) $conversion_rate * $product_sale_price, 2, '.', '');

			/* Connected Bigcommerce Schedule date */
			$product_schedule_date = self::getConnectedScheduleDate($pId, 'BigCommerce' . $store_connection_id);
			/* Connected Bigcommerce Stock Quantity */
			$product_stock_qty = self::getConnectedStockQty($pId, 'BigCommerce' . $store_connection_id);
			/* Connected Bigcommerce Stock Status */
			$product_stock_status = self::getConnectedStockStatus($pId, 'BigCommerce' . $store_connection_id);
			/* Connected Bigcommerce Stock Levael */
			$product_stock_level = self::getConnectedStockLevel($pId, 'BigCommerce' . $store_connection_id);
			/* Connected Bigcommerce Stock Allocate inv */
			$product_allocate_inv = self::getConnectedAllocateInventory($pId, 'BigCommerce' . $store_connection_id);

			$object = array(
			    "sale_price" => $bigcommerce_product_sale_price,
			    "price" => $bigcommerce_product_price,
			    "inventory_level" => $product_stock_qty,
			    "is_visible" => $final_product_stock_status,
			    "inventory_warning_level" => $product_stk_ntf
			);

			/* Get Abbrivation Id */
			$product_data = ProductAbbrivation::find()->where(['product_id' => $pId, 'channel_accquired' => 'BigCommerce', 'mul_store_id' => $store_connection_id])->one();
			if (!empty($product_data)) {
			    $id = substr($product_data->channel_abb_id, 3);
			    $products = Bigcommerce::getResource('/products/' . $id, 'Product');
			    if (!empty($products)) {
				$update_product = Bigcommerce::updateResource('/products/' . $id, $object);
				/* Update Price Product Abbrivation table */
				$update_product_abb = ProductAbbrivation::find()->where(['product_id' => $pId, 'channel_accquired' => 'BigCommerce', 'mul_store_id' => $store_connection_id])->one();
				$update_product_abb->price = $product_price;
				$update_product_abb->salePrice = $product_sale_price;
				$update_product_abb->schedules_sales_date = $product_schedule_date;
				$update_product_abb->stock_quantity = $product_stock_qty;
				$update_product_abb->allocate_inventory = $product_allocate_inv;
				$update_product_abb->stock_level = $product_stock_level;
				$update_product_abb->stock_status = $product_stock_status;
				$update_product_abb->save(false);
			    }
			}
		    }
		}
	    }

	    /* For WooCommerce */
	    if ($fin_channel == 'WooCommerce') {
		$channel_space_pos = strpos($channel, ' ');
		$country_sub_str = substr($channel, $channel_space_pos);
		$store_country = trim($country_sub_str);
		$store_details_data = StoreDetails::find()->Where(['channel_accquired' => 'WooCommerce', 'country' => $store_country])->with('storeConnection')->one();

		if (!empty($store_details_data)) {
		    $store_connection_id = $store_details_data->store_connection_id;
		    $woo_store_url = $store_details_data->storeConnection->woo_store_url;
		    $woo_consumer_key = $store_details_data->storeConnection->woo_consumer_key;
		    $woo_secret_key = $store_details_data->storeConnection->woo_secret_key;
		    $woocommerce_store_id = $store_details_data->storeConnection->store_id;
		    $store_country_code = $store_details_data->country_code;
		    $currency = $store_details_data->currency;

		    $ProductChannel = ProductChannel::find()->where(['product_id' => $pId, 'store_id' => $store_connection_id])->one();
		    if (!empty($ProductChannel)) {

			$product_sale_price = $product_price = $product_schedule_date = $product_stock_qty = $product_stock_notification = $product_stock_status = $product_stock_level = '';
			$final_product_stock_status = false;

			/* Connected Bigcommerce Price */
			$product_price = self::getConnectedPrice($pId, 'WooCommerce' . $store_connection_id);
			
			$conversion_rate = 1;
			if (!empty($currency)) {
			    $conversion_rate = self::getDbConversionRate($currency);
			}
			//$conversion_rate = self::getCurrencyConversionRate('USD', $currency);
			$woocommerce_product_price = number_format((float) $conversion_rate * $product_price, 2, '.', '');

			/* Connected Bigcommerce Sale Price */
			$product_sale_price = self::getConnectedSalePrice($pId, 'WooCommerce' . $store_connection_id);
			$woocommerce_product_sale_price = number_format((float) $conversion_rate * $product_sale_price, 2, '.', '');

			/* Connected Bigcommerce Schedule date */
			$product_schedule_date = self::getConnectedScheduleDate($pId, 'WooCommerce' . $store_connection_id);
			/* Connected Bigcommerce Stock Quantity */
			$product_stock_qty = self::getConnectedStockQty($pId, 'WooCommerce' . $store_connection_id);
			/* Connected Bigcommerce Stock Status */
			$product_stock_status = self::getConnectedStockStatus($pId, 'WooCommerce' . $store_connection_id);
			/* Connected Bigcommerce Stock Levael */
			$product_stock_level = self::getConnectedStockLevel($pId, 'WooCommerce' . $store_connection_id);
			/* Connected Bigcommerce Stock Allocate inv */
			$product_allocate_inv = self::getConnectedAllocateInventory($pId, 'WooCommerce' . $store_connection_id);

			if ($product_stock_level == 'In Stock') {
			    $stock = 1;
			} else {
			    $stock = 0;
			}
			if ($product_stock_status == 'Visible') {
			    $catalog_visibility = 'visible';
			} else {
			    $catalog_visibility = 'hidden';
			}

			/* Get Abbrivation Id */
			$product_data = ProductAbbrivation::find()->where(['product_id' => $pId, 'channel_accquired' => 'WooCommerce', 'mul_store_id' => $store_connection_id])->one();
			if (!empty($product_data)) {

			    $woocommerce_product_id = substr($product_data->channel_abb_id, 3);
			    $manage_stock = true;

			    $check_url = parse_url($woo_store_url);
			    $url_protocol = $check_url['scheme'];
			    if ($url_protocol == 'http') {
				/* For Http Url */
				$woocommerce = new Woocommerce(
					$woo_store_url, $woo_consumer_key, $woo_secret_key, [
				    'wp_api' => true,
				    'version' => 'wc/v1',
					]
				);
			    } else {
				/* For Https Url */
				$woocommerce = new Woocommerce(
					//  '' . $woo_store_url . '/wp-json/wc/v1/products/' . $woocommerce_product_id . '?catalog_visibility=' . $catalog_visibility . '&in_stock=' . $stock . '&regular_price=' . $product_sale_price . '&manage_stock='. $manage_stock .'&stock_quantity=' . $product_stock_qty . '&sale_price=' . $woocommerce_product_sale_price . '&consumer_key=' . $woo_consumer_key . '&consumer_secret=' . $woo_secret_key . '', $woo_consumer_key, $woo_secret_key, [
					'' . $woo_store_url . '/wp-json/wc/v1/products/' . $woocommerce_product_id . '?catalog_visibility=' . $catalog_visibility . '&in_stock=' . $stock . '&regular_price=' . $woocommerce_product_price . '&manage_stock=' . $manage_stock . '&stock_quantity=' . $product_stock_qty . '&consumer_key=' . $woo_consumer_key . '&consumer_secret=' . $woo_secret_key . '', $woo_consumer_key, $woo_secret_key, [
				    'wp_api' => true,
				    'version' => 'wc/v1',
				    "query_string_auth" => true,
					]
				);
			    }
			    // $data = ['regular_price' => $product_sale_price, 'manage_stock'=>$manage_stock, 'stock_quantity' => $product_stock_qty, 'sale_price' => $woocommerce_product_sale_price, 'in_stock' => $stock, 'catalog_visibility' => $catalog_visibility];
			    $data = ['regular_price' => $woocommerce_product_price, 'manage_stock' => $manage_stock, 'stock_quantity' => $product_stock_qty, 'in_stock' => $stock, 'catalog_visibility' => $catalog_visibility];

			    try {
				$woocommerce_product_update = $woocommerce->put('products/' . $woocommerce_product_id . '', $data);

				$lastRequest = $woocommerce->http->getRequest();
				$lastRequest->getUrl(); // Requested URL (string).
				$lastRequest->getMethod(); // Request method (string).
				$lastRequest->getParameters(); // Request parameters (array).
				$lastRequest->getHeaders(); // Request headers (array).
				$lastRequest->getBody(); // Request body (JSON).
				// Last response data.
				$lastResponse = $woocommerce->http->getResponse();
				$lastResponse->getCode(); // Response code (int).
				$lastResponse->getHeaders(); // Response headers (array).
				$lastResponse->getBody(); // Response body (JSON).

				/* Update Price Product Abbrivation table */
				$update_product_abb = ProductAbbrivation::find()->where(['product_id' => $pId, 'channel_accquired' => 'WooCommerce', 'mul_store_id' => $store_connection_id])->one();
				$update_product_abb->price = $product_price;
				$update_product_abb->salePrice = $product_sale_price;
				$update_product_abb->schedules_sales_date = $product_schedule_date;
				$update_product_abb->stock_quantity = $product_stock_qty;
				$update_product_abb->allocate_inventory = $product_allocate_inv;
				$update_product_abb->stock_level = $product_stock_level;
				$update_product_abb->stock_status = $product_stock_status;
				$update_product_abb->save(false);
			    } catch (HttpClientException $ex) {
				
			    }
			}
		    }
		}
	    }
	    /* For Wechat */
	    if ($fin_channel == 'WeChat') {
		$channel_id = $channel_ids = '';
		$currency='CNY';
		$channel_big = Channels::find()->where(['parent_name' => 'channel_WeChat'])->all();
		/* Get data For Product Id */
		$channel_ids = '';
		foreach ($channel_big as $pc):
		    $channel_ids .= "'" . $pc->channel_ID . "',";
		endforeach;
		$channel_ids = rtrim($channel_ids, ',');
		/* Get Assign Channel Name */
		if (!empty($channel_ids)):
		    $channels = ChannelConnection::find()->where('channel_id IN(' . $channel_ids . ')')->one();
		    $channel_id = $channels->channel_id;
		endif;

		$ProductChannel = ProductChannel::find()->where(['product_id' => $pId, 'channel_id' => $channel_id])->one();
		if (!empty($ProductChannel)):
		    $product_sale_price = $product_price = $product_schedule_date = $product_stock_qty = $product_stock_notification = '';

		    /* Connected WeChat Price */
		    $product_price = self::getConnectedPrice($pId, 'WeChat');
		    
		    $conversion_rate = 1;
		    if (!empty($currency)) {
			$conversion_rate = self::getDbConversionRate($currency);
		    }
		    //$conversion_rate = self::getCurrencyConversionRate('USD', 'CNY');
		    $wechat_product_price = number_format((float) $conversion_rate * $product_price, 2, '.', '');

		    /* Connected WeChat Sale Price */
		    $product_sale_price = self::getConnectedSalePrice($pId, 'WeChat');
		    $wechat_product_sale_price = number_format((float) $conversion_rate * $product_sale_price, 2, '.', '');

		    /* Connected WeChat Schedule date */
		    $product_schedule_date = self::getConnectedScheduleDate($pId, 'WeChat');
		    /* Connected WeChat Stock Quantity */
		    $product_stock_qty = self::getConnectedStockQty($pId, 'WeChat');
		    /* Connected WeChat Stock Status */
		    $product_stock_status = self::getConnectedStockStatus($pId, 'WeChat');
		    /* Connected WeChat Stock Levael */
		    $product_stock_level = self::getConnectedStockLevel($pId, 'WeChat');
		    /* Connected WeChat Stock Allocate inv */
		    $product_allocate_inv = self::getConnectedAllocateInventory($pId, 'WeChat');

		    //get wechat Service account id 
		    $get_big_id = Channels::find()->where(['parent_name' => 'channel_' . $channel])->all();
		    foreach ($get_big_id as $ids) {
			$arr_id[] = $ids->channel_ID;
			$arr_name[] = $ids->channel_name;
			$ids->channel_name;
		    }
		    $channel_ids = implode(",", $arr_id);
		    $con_details = ChannelConnection::find()->where('channel_id IN(' . $channel_ids . ')')->one();
		    $token = $con_details->token;
		    $pid = $con_details->wechat_project_id;
		    $user_id = $con_details->user_id;


		    $product_data1 = ProductAbbrivation::find()->where(['product_id' => $pId, 'channel_accquired' => 'WeChat'])->one();
		    $bigcm_id = substr($product_data1->channel_abb_id, 2);
		    $wechat_id = $product_data1->wechat_id;


//              get the products all data
		    $url = 'https://cms-api.walkthechat.com/products/groups/' . $bigcm_id;
		    $c_cat = curl_init();
		    $method = "GET";
		    curl_setopt_array($c_cat, array(
			CURLOPT_URL => "$url",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => $method,
			CURLOPT_POSTFIELDS => "category=",
			CURLOPT_HTTPHEADER => array(
			    "x-access-token: $token",
			    "x-id-project: $pid"
			),
		    ));
		    $rs = curl_exec($c_cat);
		    $d_rs = json_decode($rs);
		    $wechat_product_exists = isset($d_rs->products) ? $d_rs->products : '';
		    if (!empty($wechat_product_exists)):
			$product_data = $d_rs->products->en->product;
			$product_data->price = $wechat_product_price;
			$product_data->salesPrice = $wechat_product_sale_price;
			$product_data->unit = $product_stock_qty;
			//unset($product_data->categoryGroupId);
			$product_data_arr = (array) $product_data;
			//Update Product in wechat
			$url = 'https://cms-api.walkthechat.com/products/' . $wechat_id;
			$c_cat = curl_init();
			$method = "PUT";
			curl_setopt_array($c_cat, array(
			    CURLOPT_URL => "$url",
			    CURLOPT_RETURNTRANSFER => true,
			    CURLOPT_ENCODING => "",
			    CURLOPT_MAXREDIRS => 10,
			    CURLOPT_TIMEOUT => 30,
			    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			    CURLOPT_CUSTOMREQUEST => $method,
			    CURLOPT_POSTFIELDS => http_build_query($product_data_arr),
			    CURLOPT_HTTPHEADER => array(
				"x-access-token: $token",
				"x-id-project: $pid"
			    ),
			));
			$rs1 = curl_exec($c_cat);
			$d_rs1 = json_decode($rs1);

			if (!empty($d_rs1)):

			    /* Update Price Product Abbrivation table */
			    $update_product_abb = ProductAbbrivation::find()->where(['product_iD' => $pId, 'channel_accquired' => 'WeChat'])->one();
			    $update_product_abb->price = $product_price;
			    $update_product_abb->salePrice = $product_sale_price;
			    $update_product_abb->schedules_sales_date = $product_schedule_date;
			    $update_product_abb->stock_quantity = $product_stock_qty;
			    $update_product_abb->allocate_inventory = $product_allocate_inv;
			    $update_product_abb->stock_level = $product_stock_level;
			    $update_product_abb->stock_status = $product_stock_status;
			    $update_product_abb->save(false);
			endif;
		    endif;
		endif;
	    }
	    /* For FaceBook */
	    if ($fin_channel == 'Facebook') {
		$channel_big = Channels::find()->where(['channel_name' => 'Facebook'])->one();
		if (!empty($channel_big)) {
		    $channel_id = $channel_big->channel_ID;
		}
		$ProductChannel = ProductChannel::find()->where(['product_id' => $pId, 'channel_id' => $channel_id])->one();
		if (!empty($ProductChannel)) :
		    $product_sale_price = $product_price = $product_schedule_date = $product_stock_qty = $product_stock_notification = '';

		    /* Connected Facebook Price */
		    $product_price = self::getConnectedPrice($pId, 'Facebook');
		    /* Connected Facebook Sale Price */
		    $product_sale_price = self::getConnectedSalePrice($pId, 'Facebook');
		    /* Connected Facebook Schedule date */
		    $product_schedule_date = self::getConnectedScheduleDate($pId, 'Facebook');
		    /* Connected Facebook Stock Quantity */
		    $product_stock_qty = self::getConnectedStockQty($pId, 'Facebook');
		    /* Connected Facebook Stock Status */
		    $product_stock_status = self::getConnectedStockStatus($pId, 'Facebook');
		    /* Connected Facebook Stock Levael */
		    $product_stock_level = self::getConnectedStockLevel($pId, 'Facebook');
		    /* Connected Facebook Stock Allocate inv */
		    $product_allocate_inv = self::getConnectedAllocateInventory($pId, 'Facebook');

		    /* Update Price Product Abbrivation table */
		    $update_product_abb = ProductAbbrivation::find()->where(['product_iD' => $pId, 'channel_accquired' => 'Facebook'])->one();
		    $update_product_abb->price = $product_price;
		    $update_product_abb->salePrice = $product_sale_price;
		    $update_product_abb->schedules_sales_date = $product_schedule_date;
		    $update_product_abb->stock_quantity = $product_stock_qty;
		    $update_product_abb->allocate_inventory = $product_allocate_inv;
		    $update_product_abb->stock_level = $product_stock_level;
		    $update_product_abb->stock_status = $product_stock_status;
		    $update_product_abb->save(false);
		endif;
	    }
	    /* For Google Shopping */
	    if ($fin_channel == 'Google Shopping') {
		$channel_big = Channels::find()->where(['channel_name' => 'Google Shopping'])->one();
		if (!empty($channel_big)) {
		    $channel_id = $channel_big->channel_ID;
		}
		$ProductChannel = ProductChannel::find()->where(['product_id' => $pId, 'channel_id' => $channel_id])->one();
		if (!empty($ProductChannel)):
		    $product_sale_price = $product_price = $product_schedule_date = $product_stock_qty = $product_stock_notification = '';

		    /* Connected Google Shopping Price */
		    $product_price = self::getConnectedPrice($pId, 'Google Shopping');
		    /* Connected Google Shopping Sale Price */
		    $product_sale_price = self::getConnectedSalePrice($pId, 'Google Shopping');
		    /* Connected Google Shopping Schedule date */
		    $product_schedule_date = self::getConnectedScheduleDate($pId, 'Google Shopping');
		    /* Connected Google Shopping Stock Quantity */
		    $product_stock_qty = self::getConnectedStockQty($pId, 'Google Shopping');
		    /* Connected Google Shopping Stock Status */
		    $product_stock_status = self::getConnectedStockStatus($pId, 'Google Shopping');
		    /* Connected Google Shopping Stock Levael */
		    $product_stock_level = self::getConnectedStockLevel($pId, 'Google Shopping');
		    /* Connected Google Shopping Stock Allocate inv */
		    $product_allocate_inv = self::getConnectedAllocateInventory($pId, 'Google Shopping');

		    /* Update Price Product Abbrivation table */
		    $update_product_abb = ProductAbbrivation::find()->where(['product_iD' => $pId, 'channel_accquired' => 'Google Shopping'])->one();
		    $update_product_abb->price = $product_price;
		    $update_product_abb->salePrice = $product_sale_price;
		    $update_product_abb->schedules_sales_date = $product_schedule_date;
		    $update_product_abb->stock_quantity = $product_stock_qty;
		    $update_product_abb->allocate_inventory = $product_allocate_inv;
		    $update_product_abb->stock_level = $product_stock_level;
		    $update_product_abb->stock_status = $product_stock_status;
		    $update_product_abb->save(false);
		endif;
	    }

	    /* For Flipkart */
	    if ($fin_channel == 'Flipkart') {
		
		$product_sale_price = $product_price = $product_schedule_date = $product_sale_price_actual = '';
		/* Connected Flipkart Price */
		$connected_price = isset($_POST['Connected_price']) ? $_POST['Connected_price'] : '';
		if (!empty($connected_price)):
		    foreach ($connected_price as $cp_key_price => $cp) :
			if ($cp_key_price == 'Flipkart'):
			    $product_price_actual = $product_price = $cp;
			endif;
		    endforeach;
		endif;

		/* Connected Flipkart Sale Price */
		$connected_sale_price = isset($_POST['Connected_sale_price']) ? $_POST['Connected_sale_price'] : '';
		if (!empty($connected_sale_price)):
		    foreach ($connected_sale_price as $cp_key_sale_price => $csp) :
			if ($cp_key_sale_price == 'Flipkart'):
			    $product_sale_price_actual = $product_sale_price = $csp;
			endif;
		    endforeach;
		endif;

		/* Connected Flipkart Schedule date */
		$Connected_schedule_date = isset($_POST['Connected_schedule_date']) ? $_POST['Connected_schedule_date'] : '';
		if (!empty($Connected_schedule_date)):
		    foreach ($Connected_schedule_date as $cs_key_schedule_date => $csd) :
			if ($cs_key_schedule_date == 'Flipkart'):
			    $product_schedule_date = $csd;
			endif;
		    endforeach;
		endif;
		$user = Yii::$app->user->identity;
		$currency=$user->currency;
		$conversion_rate = 1;
		if (!empty($currency)) {
		    $conversion_rate = self::getDbConversionRate($currency);
		}
		
//		if (isset($user->currency) and $user->currency != 'INR') {
//		    $conversion_rate = self::getCurrencyConversionRate($user->currency, 'INR');
//		}
		$product_price = $product_price * $conversion_rate;
		$product_sale_price = $product_sale_price * $conversion_rate;
		$product_price = number_format((int) $product_price, 2, '.', '');
		$product_sale_price = number_format((int) $product_sale_price, 2, '.', '');
//                echo $product_price;die;

		$current_product = Products::find()->where(['id' => $pId])->select(['id', 'channel_abb_id', 'SKU'])->asArray()->one();
		if (isset($current_product) and ! empty($current_product) and ! empty($current_product['channel_abb_id'])) {
		    $listing_id = str_replace('FPK', '', $current_product['channel_abb_id']);
		    $curl = curl_init();
		    $data = ['skuId' =>
			$current_product['SKU'],
			'attributeValues' => [
			    'mrp' => $product_price,
			    'selling_price' => $product_sale_price,
			    "procurement_type" => "REGULAR",
			    "stock_count" => @$post_data['pStk_qty'],
			    "listing_status" => (@$post_data['pStk_status'] == 'Visible') ? 'ACTIVE' : 'INACTIVE',
		    ]];
		    //print_r($data);die;
		    curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.flipkart.net/sellers/skus/listings/" . $listing_id,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			//  CURLOPT_POSTFIELDS => "{\r\n    \"skuId\": \"HLTHTIPS\",\r\n    \"attributeValues\": {\r\n        \"mrp\": 140,\r\n        \"selling_price\": 120,\r\n        \"procurement_type\": \"REGULAR\"\r\n    }\r\n}",
			CURLOPT_POSTFIELDS => json_encode($data),
			CURLOPT_HTTPHEADER => array(
			    "authorization: Bearer 2f2c5d70-ec70-475f-98c7-1d5cdbf2a1bb",
			    "cache-control: no-cache",
			    "content-type: application/json",
			),
		    ));

		    $response = curl_exec($curl);
		    $err = curl_error($curl);

		    curl_close($curl);
		    $status_decoded = json_decode($response, true);
		    //print_r($status_decoded);die;  
		    if (!empty($status_decoded) and isset($status_decoded['status']) and $status_decoded['status'] == 'success') {
			/* Update Price Product Abbrivation table */
			$update_product_abb = ProductAbbrivation::find()->where(['product_iD' => $pId, 'channel_accquired' => 'Flipkart'])->one();
			$update_product_abb->price = $product_price_actual;
			$update_product_abb->salePrice = $product_sale_price_actual;
			$update_product_abb->schedules_sales_date = @$product_schedule_date;
			$update_product_abb->save(false);
		    }
		}
	    }

	    /** For Square product update */
	    if ($fin_channel == 'Square') {
		self::SquareProductUpdate($pId, $post_data);
	    }

	    /** For Shopify product update */
	    if ($fin_channel == 'Shopify') {
		$users_Id = Yii::$app->user->identity->id;
		$channel_space_pos = strpos($channel, ' ');
		$country_sub_str = substr($channel, $channel_space_pos);
		$store_country = trim($country_sub_str);
		$store_name = 'Shopify';
		$store_details_data = StoreDetails::find()->Where(['channel_accquired' => $store_name, 'country' => $store_country])->with('storeConnection')->one();
		self::ShopifyProductUpdate($pId, $post_data, $store_details_data, $store_name);
	    }
	    /** For Shopify product update */
	    if ($fin_channel == 'ShopifyPlus') {
		$users_Id = Yii::$app->user->identity->id;
		$channel_space_pos = strpos($channel, ' ');
		$country_sub_str = substr($channel, $channel_space_pos);
		$store_country = trim($country_sub_str);
		$store_name = 'ShopifyPlus';
		$store_details_data = StoreDetails::find()->Where(['channel_accquired' => $store_name, 'country' => $store_country])->with('storeConnection')->one();
		self::ShopifyProductUpdate($pId, $post_data, $store_details_data, $store_name);
	    }
	    /** For Vtex Product Update */
	    if ($fin_channel == 'VTEX') {
		$users_Id = Yii::$app->user->identity->id;
		$channel_space_pos = strpos($channel, ' ');
		$country_sub_str = substr($channel, $channel_space_pos);
		$store_country = trim($country_sub_str);
		$store_name = 'VTEX';
		$store_details_data = StoreDetails::find()->Where(['channel_accquired' => $store_name, 'country' => $store_country])->with('storeConnection')->one();
		self::vtexProductUpdate($pId, $post_data, $store_details_data);
	    }
	    /** For Lazada Malaysia product update */
	    if ($fin_channel == 'Lazada Malaysia') {
		self::LazadaMalasysiaProductUpdate($pId, $post_data, $channel);
	    }
	    /** For Magento 1 product update */
	    if ($fin_channel == 'Magento') {
		$users_Id = Yii::$app->user->identity->id;
		$channel_space_pos = strpos($channel, ' ');
		$country_sub_str = substr($channel, $channel_space_pos);
		$store_country = trim($country_sub_str);
		$magento_store_details_data = StoreDetails::find()->Where(['channel_accquired' => 'Magento', 'country' => $store_country])->with('storeConnection')->one();
		self::magentoProductUpdate($pId, $post_data, $magento_store_details_data);
	    }
	    /** For Magento 1 product update */
	    if ($fin_channel == 'Magento2') {
		$users_Id = Yii::$app->user->identity->id;
		$channel_space_pos = strpos($channel, ' ');
		$country_sub_str = substr($channel, $channel_space_pos);
		$store_country = trim($country_sub_str);
		$magento_store_details_data = StoreDetails::find()->Where(['channel_accquired' => 'Magento2', 'country' => $store_country])->with('storeConnection')->one();
		self::magento2ProductUpdate($pId, $post_data, $magento_store_details_data);
	    }
	}
    }

    /* For Product Update Channel or Stores */

    public function update_stock_connected_channels($channel_sale, $pId, $post_data) {

	$users_Id = Yii::$app->user->identity->id;
	/* If Channel sale is empty then create a blank array */
	if (is_scalar($channel_sale)) {
	    $channel_sale = array();
	}


	/* Get data For Product Id */
	$ProductChannel = ProductChannel::find()->where(['product_id' => $pId])->all();
	$names = array();
	foreach ($ProductChannel as $pc) {
	    if (!empty($pc->store_id)) {
		/* For Stores */
		$store_connection_data = StoresConnection::find()->Where(['stores_connection_id' => $pc->store_id])->with('storesDetails')->one();
		if (!empty($store_connection_data)) {
		    $store_details_country = @$store_connection_data->storesDetails->country;
		    if (!empty($store_details_country)) {
			$store_country = $store_connection_data->storesDetails->country;
		    } else {
			$store_country = '';
		    }
		    $store_connection_id = $store_connection_data->stores_connection_id;
		    $get_store_id = $store_connection_data->store_id;
		    $store_names = Stores::find()->Where(['store_id' => $get_store_id])->one();
		    $names[] = trim($store_names->store_name . ' ' . $store_country);
		}
	    }
	    if (!empty($pc->channel_id)) {
		/* For Channels */
		$channels_name = channels::find()->where(['channel_id' => $pc->channel_id])->one();
		$channel_parent_name = $channels_name->parent_name;

		$channel_name_org = ucwords(str_replace("-", " ", substr($channel_parent_name, 8)));
		if ($channel_name_org == 'Lazada') {
		    $channel_name_org = $channels_name->channel_name;
		}
		$names[] = trim($channel_name_org);
	    }
	}
	$channel_final = array();
	foreach ($names as $_name) {
	    if (!in_array($_name, $channel_sale)) {
		$channel_final[] = $_name;
	    }
	}


	/* Create Product In Selected Channels */
	foreach ($channel_final as $channel) {
	    $channel_name_explode = explode(" ", $channel);
	    $channel_name_check = $channel_name_explode[0];
	    $check_valid_store = Stores::find()->where(['store_name' => $channel_name_check])->one();
	    if (!empty($check_valid_store)) {
		$fin_channel = $channel_name_check;
	    } else {
		$fin_channel = $channel;
	    }
	    /*	     * **************** Stock Update For BigCommerce ******************* */

	    if ($fin_channel == 'BigCommerce') {

		$channel_space_pos = strpos($channel, ' ');
		$country_sub_str = substr($channel, $channel_space_pos);
		$store_country = trim($country_sub_str);
		$store_details_data = StoreDetails::find()->Where(['channel_accquired' => 'BigCommerce', 'country' => $store_country])->with('storeConnection')->one();

		if (!empty($store_details_data)) {
		    $store_connection_id = $store_details_data->store_connection_id;
		    $big_client_id = $store_details_data->storeConnection->big_client_id;
		    $big_access_token = $store_details_data->storeConnection->big_access_token;
		    $big_store_hash = $store_details_data->storeConnection->big_store_hash;

		    //BigCommerce Configuration Object
		    Bigcommerce::configure(array(
			'client_id' => $big_client_id,
			'auth_token' => $big_access_token,
			'store_hash' => $big_store_hash
		    ));

		    /* Get channel Abbrivation Id */
		    $big_product_abb = ProductAbbrivation::find()->where(['product_id' => $pId, 'channel_accquired' => 'BigCommerce', 'mul_store_id' => $store_connection_id])->one();
		    $bgc_id = preg_replace("/[^0-9,]/", "", $big_product_abb->channel_abb_id);

		    $product_stk_qty = 0;
		    $Bigcommerce_object = array("inventory_level" => $product_stk_qty);
		    $products_res = Bigcommerce::updateResource('/products/' . $bgc_id, $Bigcommerce_object);

		    /* For Status inactive in product channel */
		    $ProductChannel = ProductChannel::find()->where(['product_id' => $pId, 'store_id' => $store_connection_id])->one();
		    $ProductChannel->status = 'no';
		    $ProductChannel->save(false);
		}
	    }

	    /*	     * *********Stock update for Woocommerce***************** */
	    if ($fin_channel == 'WooCommerce') {
		$channel_space_pos = strpos($channel, ' ');
		$country_sub_str = substr($channel, $channel_space_pos);
		$store_country = trim($country_sub_str);
		$store_details_data = StoreDetails::find()->Where(['channel_accquired' => 'WooCommerce', 'country' => $store_country])->with('storeConnection')->one();

		if (!empty($store_details_data)) {
		    $store_connection_id = $store_details_data->store_connection_id;
		    $woo_store_url = $store_details_data->storeConnection->woo_store_url;
		    $woo_consumer_key = $store_details_data->storeConnection->woo_consumer_key;
		    $woo_secret_key = $store_details_data->storeConnection->woo_secret_key;
		    $woocommerce_store_id = $store_details_data->storeConnection->store_id;
		    $store_country_code = $store_details_data->country_code;
		    $store_currency_code = $store_details_data->currency;
		    $stock_qty = 0;
		    /* For Https server */
		    /* Get channel Abbrivation Id */
		    $woo_product_abb = ProductAbbrivation::find()->where(['product_id' => $pId, 'channel_accquired' => 'WooCommerce', 'mul_store_id' => $store_connection_id])->one();
		    $woocommerce_product_id = preg_replace("/[^0-9,]/", "", $woo_product_abb->channel_abb_id);
		    $check_url = parse_url($woo_store_url);
		    $url_protocol = $check_url['scheme'];
		    if ($url_protocol == 'http'):
			/* For Http Url */
			$woocommerce = new Woocommerce(
				$woo_store_url, $woo_consumer_key, $woo_secret_key, [
			    'wp_api' => true,
			    'version' => 'wc/v1',
				]
			);
		    else:
			/* For Https Url */
			$woocommerce = new Woocommerce(
				'' . $woo_store_url . '/wp-json/wc/v1/products/' . $woocommerce_product_id . '?stock_quantity=' . $stock_qty . '&consumer_key=' . $woo_consumer_key . '&consumer_secret=' . $woo_secret_key . '', $woo_consumer_key, $woo_secret_key, [
			    'wp_api' => true,
			    'version' => 'wc/v1',
			    "query_string_auth" => true,
				]
			);
		    endif;

		    try {

			$data = ['stock_quantity' => 0];
			$update_stock = $woocommerce->put('products/' . $woocommerce_product_id . '', $data);

			$lastRequest = $woocommerce->http->getRequest();
			$lastRequest->getUrl(); // Requested URL (string).
			$lastRequest->getMethod(); // Request method (string).
			$lastRequest->getParameters(); // Request parameters (array).
			$lastRequest->getHeaders(); // Request headers (array).
			$lastRequest->getBody(); // Request body (JSON).
			// Last response data.
			$lastResponse = $woocommerce->http->getResponse();
			$lastResponse->getCode(); // Response code (int).
			$lastResponse->getHeaders(); // Response headers (array).
			$lastResponse->getBody(); // Response body (JSON).

			/* For Status inactive in product channel */
			$ProductChannel = ProductChannel::find()->where(['product_id' => $pId, 'store_id' => $store_connection_id])->one();
			$ProductChannel->status = 'no';
			$ProductChannel->save(false);
		    } catch (HttpClientException $ex) {
//                                echo'<pre>';
//                                 $msg = $ex->getMessage();
//                                echo'<pre>';
//                                print_r($msg);
//                                die('msg');
		    }
		}
	    }

	    /* Stock Null For WeChat */
	    if ($fin_channel == 'WeChat'):
		//get wechat Service account id 
		$get_big_id = Channels::find()->where(['parent_name' => 'channel_' . $channel])->all();
		foreach ($get_big_id as $ids) {
		    $arr_id[] = $ids->channel_ID;
		    $arr_name[] = $ids->channel_name;
		    $ids->channel_name;
		}
		$channel_ids = implode(",", $arr_id);
		$con_details = ChannelConnection::find()->where('channel_id IN(' . $channel_ids . ')')->one();
		$token = $con_details->token;
		$pid = $con_details->wechat_project_id;
		$user_id = $con_details->user_id;
		$channel_id = $con_details->channel_id;


		$product_data = ProductAbbrivation::find()->where(['product_id' => $pId, 'channel_accquired' => 'WeChat'])->one();
		$bigcm_id = substr($product_data->channel_abb_id, 2);
		$wechat_id = $product_data->wechat_id;

		$url = 'https://cms-api.walkthechat.com/products/groups/' . $bigcm_id;
		$c_cat = curl_init();
		$method = "GET";
		curl_setopt_array($c_cat, array(
		    CURLOPT_URL => "$url",
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_ENCODING => "",
		    CURLOPT_MAXREDIRS => 10,
		    CURLOPT_TIMEOUT => 30,
		    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		    CURLOPT_CUSTOMREQUEST => $method,
		    CURLOPT_POSTFIELDS => "category=",
		    CURLOPT_HTTPHEADER => array(
			"x-access-token: $token",
			"x-id-project: $pid"
		    ),
		));
		$rs = curl_exec($c_cat);
		$d_rs = json_decode($rs);
		$wechat_product_exists = isset($d_rs->products) ? $d_rs->products : '';

		if (!empty($wechat_product_exists)) :


		    $product_data = $d_rs->products->en->product;
		    $product_data->unit = 0;
		    //unset($product_data->categoryGroupId);
		    $product_data_arr = (array) $product_data;
		    $url = 'https://cms-api.walkthechat.com/products/' . $wechat_id;
		    $c_cat = curl_init();
		    $method = "PUT";
		    curl_setopt_array($c_cat, array(
			CURLOPT_URL => "$url",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => $method,
			CURLOPT_POSTFIELDS => http_build_query($product_data_arr),
			CURLOPT_HTTPHEADER => array(
			    "x-access-token: $token",
			    "x-id-project: $pid"
			),
		    ));
		    $rs1 = curl_exec($c_cat);
		    $d_rs1 = json_decode($rs1);
		    /* For Status inactive in product channel */
		    $ProductChannel = ProductChannel::find()->where(['product_id' => $pId, 'channel_id' => $channel_id])->one();
		    if (!empty($ProductChannel)) {
			$ProductChannel->status = 'no';
			$ProductChannel->save(false);
		    }

		endif;
	    endif;
	    /* Stock Null For Google shopping */
	    if ($fin_channel == 'Google Shopping'):
		/* For Status inactive in product channel */
		$channel_data = Channels::find()->Where(['channel_name' => 'Google Shopping'])->one();
		$channel_id = $channel_data->channel_ID;
		$ProductChannel = ProductChannel::find()->where(['product_id' => $pId, 'channel_id' => $channel_id])->one();
		if (!empty($ProductChannel)) {
		    $ProductChannel->status = 'no';
		    $ProductChannel->save(false);
		}
	    endif;

	    /* Stock Null For FaceBook */
	    if ($fin_channel == 'Facebook'):
		/* For Status inactive in product channel */
		$channel_data = Channels::find()->Where(['channel_name' => 'Facebook'])->one();
		$channel_id = $channel_data->channel_ID;
		$ProductChannel = ProductChannel::find()->where(['product_id' => $pId, 'channel_id' => $channel_id])->one();
		if (!empty($ProductChannel)) {
		    $ProductChannel->status = 'no';
		    $ProductChannel->save(false);
		}
	    endif;

	    /*  Stock Null at Square */
	    if ($fin_channel == 'Square'):
		self::squareProductStockNull($pId, 0);
		/* For Status inactive in product channel */
		$channel_data = Channels::find()->Where(['channel_name' => 'Square'])->one();
		$channel_id = $channel_data->channel_ID;
		$ProductChannel = ProductChannel::find()->where(['product_id' => $pId, 'channel_id' => $channel_id])->one();
		if (!empty($ProductChannel)) {
		    $ProductChannel->status = 'no';
		    $ProductChannel->save(false);
		}
	    endif;

	    /* Stock 0 at Shopify */
	    if ($fin_channel == 'Shopify'):
		$channel_space_pos = strpos($channel, ' ');
		$country_sub_str = substr($channel, $channel_space_pos);
		$store_country = trim($country_sub_str);
		$store_name = 'Shopify';
		$store_details_data = StoreDetails::find()->Where(['channel_accquired' => $store_name, 'country' => $store_country])->with('storeConnection')->one();
		if (!empty($store_details_data)):
		    $store_connection_id = $store_details_data->store_connection_id;
		    self::shopifyProductStockNull($pId, 0, $store_details_data, $store_name);
		    /* For Status inactive in product channel */
		    $ProductChannel = ProductChannel::find()->where(['product_id' => $pId, 'store_id' => $store_connection_id])->one();
		    if (!empty($ProductChannel)) {
			$ProductChannel->status = 'no';
			$ProductChannel->save(false);
		    }
		endif;
	    endif;
	    /* Stock 0 at ShopifyPlus */
	    if ($fin_channel == 'ShopifyPlus'):
		$channel_space_pos = strpos($channel, ' ');
		$country_sub_str = substr($channel, $channel_space_pos);
		$store_country = trim($country_sub_str);
		$store_name = 'ShopifyPlus';
		$store_details_data = StoreDetails::find()->Where(['channel_accquired' => $store_name, 'country' => $store_country])->with('storeConnection')->one();
		if (!empty($store_details_data)):
		    $store_connection_id = $store_details_data->store_connection_id;
		    self::shopifyProductStockNull($pId, 0, $store_details_data, $store_name);
		    /* For Status inactive in product channel */
		    $ProductChannel = ProductChannel::find()->where(['product_id' => $pId, 'store_id' => $store_connection_id])->one();
		    if (!empty($ProductChannel)) {
			$ProductChannel->status = 'no';
			$ProductChannel->save(false);
		    }
		endif;
	    endif;
	    /* Stock 0 at Vtex */
	    if ($fin_channel == 'VTEX'):
		$channel_space_pos = strpos($channel, ' ');
		$country_sub_str = substr($channel, $channel_space_pos);
		$store_country = trim($country_sub_str);
		$store_name = 'VTEX';
		$store_details_data = StoreDetails::find()->Where(['channel_accquired' => $store_name, 'country' => $store_country])->with('storeConnection')->one();
		if (!empty($store_details_data)):
		    $store_connection_id = $store_details_data->store_connection_id;
		    self::vtexProductStockNull($pId, 0, $store_details_data, $store_name);
		    /* For Status inactive in product channel */
		    $ProductChannel = ProductChannel::find()->where(['product_id' => $pId, 'store_id' => $store_connection_id])->one();
		    if (!empty($ProductChannel)) {
			$ProductChannel->status = 'no';
			$ProductChannel->save(false);
		    }
		endif;
	    endif;

	    /* Stock 0 at Lazada Malaysia */
	    if ($fin_channel == 'Lazada Malaysia'):
		self::lazadaMalaysiaProductStockNull($pId, $channel, 0);
		/* For Status inactive in product channel */
		$get_shopify_data = Channels::find()->select('channel_ID')->where(['channel_name' => $channel])->one();
		$channel_id = $get_shopify_data->channel_ID;
		$ProductChannel = ProductChannel::find()->where(['product_id' => $pId, 'channel_id' => $channel_id])->one();
		$ProductChannel->status = 'no';
		$ProductChannel->save(false);
	    endif;
	    /* Stock 0 at Magento */
	    if ($fin_channel == 'Magento'):
		$channel_space_pos = strpos($channel, ' ');
		$country_sub_str = substr($channel, $channel_space_pos);
		$store_country = trim($country_sub_str);
		$magento_store_details_data = StoreDetails::find()->Where(['channel_accquired' => 'Magento', 'country' => $store_country])->with('storeConnection')->one();
		if (!empty($magento_store_details_data)) {
		    $store_connection_id = $magento_store_details_data->store_connection_id;
		    self::magentoProductStockNull($pId, 0, $magento_store_details_data);
		    /* For Status inactive in product channel */
		    $get_magento_data = Stores::find()->select('store_id')->where(['store_name' => 'Magento'])->one();
		    $magento_store_id = $get_magento_data->store_id;
		    $ProductChannel = ProductChannel::find()->where(['product_id' => $pId, 'store_id' => $store_connection_id])->one();
		    $ProductChannel->status = 'no';
		    $ProductChannel->save(false);
		}
	    endif;
	    /*  Stock 0 at Magento 2 */
	    if ($fin_channel == 'Magento2'):
		$channel_space_pos = strpos($channel, ' ');
		$country_sub_str = substr($channel, $channel_space_pos);
		$store_country = trim($country_sub_str);
		$magento_store_details_data = StoreDetails::find()->Where(['channel_accquired' => 'Magento2', 'country' => $store_country])->with('storeConnection')->one();
		if (!empty($magento_store_details_data)) {
		    $store_connection_id = $magento_store_details_data->store_connection_id;
		    self::magento2ProductStockNull($pId, 0, $magento_store_details_data);
		    /* For Status inactive in product channel */
		    $ProductChannel = ProductChannel::find()->where(['product_id' => $pId, 'store_id' => $store_connection_id])->one();
		    $ProductChannel->status = 'no';
		    $ProductChannel->save(false);
		}
	    endif;
	}
    }

    public function squareProductCreate($pId, $post_data) {
	$name = $post_data['pName'];
	$sku = $post_data['pSKU'];
	$description = $post_data['pDescription'];
	$product_categories = (isset($post_data['pcat'])) ? $post_data['pcat'] : array();
	$stock_qty = $post_data['pStk_qty'];
	$price = $post_data['pPrice'];
	$stock_ntf = '0';
	if ($post_data['plow_stk_ntf'] != '') {
	    $stock_ntf = $post_data['plow_stk_ntf'];
	}

	//echo "<pre>"; print_r($post_data); echo "</pre>";
	$user_id = Yii::$app->user->identity->id;
	$channel_data = Channels::find()->where(['channel_name' => 'Square'])->one();
	$square_channel_id = $channel_data->channel_ID;
	$square_details = ChannelConnection::find()->where(['user_id' => $user_id, 'channel_id' => $square_channel_id])->one();
	if (!empty($square_details)) {
	    require_once($_SERVER['DOCUMENT_ROOT'] . '/backend/web/square/vendor/autoload.php');
	    \SquareConnect\Configuration::getDefaultConfiguration()->setAccessToken($square_details->square_personal_access_token);
	    $api_instance = new \SquareConnect\Api\V1ItemsApi();
	    $location_id = self::getSquareLocationId();
	    $item_body = new \SquareConnect\Model\V1Item();
	    $inventory = new \SquareConnect\Model\V1AdjustInventoryRequest();
	    $category_body = new \SquareConnect\Model\V1Category();
	    $sq_cat_id = '';
	    if (count($product_categories) > 0) {
		$catalog_api = new \SquareConnect\Api\CatalogApi();
		$types = join(",", [
		    "CATEGORY"
		]);
		$cursor = null;
		$category_result = [];
		do {
		    $apiResponse = $catalog_api->listCatalog($cursor, $types);
		    $cursor = $apiResponse['cursor'];
		    if ($apiResponse['objects'] != null) {
			$category_result = array_merge($category_result, $apiResponse['objects']);
		    }
		} while ($apiResponse['cursor']);
		$cat_id = '';
		$square_cat_list = array();
		foreach ($category_result as $_cat) {
		    $cat_id = $_cat['id'];
		    $cat_name = $_cat['category_data']['name'];
		    $square_cat_list[$cat_id] = $cat_name;
		}
		foreach ($product_categories as $_product_cat) {
		    $cat_name_exists = array_search($_product_cat, $square_cat_list);
		    if ($cat_name_exists !== FALSE) {
			$sq_cat_id = $cat_name_exists;
		    }
		}
		if ($sq_cat_id == '') {
		    $category_body = array(
			'name' => $product_categories[0],
		    );
		    $category_response = $api_instance->createCategory($location_id, $category_body);
		    $sq_cat_id = $category_response['id'];
		}
	    }
	    $item_body = array(
		'name' => $name,
		'description' => $description,
		'visibility' => 'PRIVATE',
		'category_id' => $sq_cat_id,
		'variations' => [array(
		'id' => uniqid(),
		'name' => 'Regular',
		'pricing_type' => 'FIXED_PRICING',
		'price_money' => array(
		    'currency_code' => 'USD',
		    'amount' => $price * 100,
		),
		'sku' => $sku,
		'track_inventory' => true,
		'inventory_alert_type' => 'LOW_QUANTITY',
		'inventory_alert_threshold' => $stock_ntf,
		    )],
	    );
	    try {
		$product_data = $api_instance->createItem($location_id, $item_body);
		$variation = $product_data['variations'];
		foreach ($variation as $_variation) {
		    $variation_id = $_variation['id'];
		    $inventory['quantity_delta'] = $stock_qty;
		    $inventory['adjustment_type'] = '';
		    $inventory['memo'] = '';
		    try {
			$inventory_data = $api_instance->adjustInventory($location_id, $variation_id, $inventory);
		    } catch (Exception $e) {
			echo 'Exception when calling V1ItemsApi->adjustInventory: ', $e->getMessage();
		    }
		}
		$channel_data = Channels::find()->where(['channel_name' => 'Square'])->one();
		$square_channel_id = $channel_data->channel_ID;
		$product_channel = new ProductChannel;
		$product_channel->channel_id = $square_channel_id;
		$product_channel->product_id = $pId;
		$product_channel->created_at = date('Y-m-d h:i:s', time());
		if ($product_channel->save(false)) {
		    if (!empty($product_categories)) {
			// Delete Product Categories //
			$Delete_product_categories = ProductCategories::deleteAll(['product_ID' => $pId]);
			foreach ($product_categories as $p_cat) :
			    // get wechat category acc to name //
			    $category_data = Categories::find()->Where(['category_name' => $p_cat])->one();
			    $category_id = $category_data->category_ID;
			    $productCategoryModel = new ProductCategories;
			    $productCategoryModel->category_ID = $category_id;
			    $productCategoryModel->product_ID = $pId;
			    $productCategoryModel->created_at = date('Y-m-d H:i:s', time());
			    //Save Elliot User id
			    $productCategoryModel->elliot_user_id = Yii::$app->user->identity->id;
			    $productCategoryModel->save(false);
			endforeach;
		    }
		    $productAbbrivation = new ProductAbbrivation();
		    $productAbbrivation->channel_abb_id = 'SQ' . $product_data['id'];
		    $productAbbrivation->product_id = $pId;
		    $productAbbrivation->channel_accquired = 'Square';
		    $productAbbrivation->price = $post_data['pPrice'];
		    $productAbbrivation->salePrice = $post_data['pSale_price'];
		    $schedule_date = ($_POST['pSchedule_date'] == '') ? date('Y-m-d') : $_POST['pSchedule_date'];
		    $productAbbrivation->schedules_sales_date = date('Y-m-d', strtotime($schedule_date));
		    $productAbbrivation->stock_quantity = $post_data['pStk_qty'];
		    $productAbbrivation->stock_level = $_POST['pStk_lvl'];
		    $productAbbrivation->stock_status = $_POST['pStk_status'];
		    $productAbbrivation->created_at = date('Y-m-d H:i:s', time());
		    $productAbbrivation->save(false);
		}
	    } catch (Exception $e) {
		echo 'Exception when calling V1ItemsApi->createItem: ', $e->getMessage(), PHP_EOL;
	    }
	}
    }

    /**
     * Square Get LocationId
     */
    public function getSquareLocationId() {
	try {
	    $api = new \SquareConnect\Api\LocationsApi();
	    $location_result = $api->listLocations();
	    return $location_result['locations'][0]['id'];
	} catch (Exception $ex) {
	    echo 'Exception when calling : ';
	    echo $ex->getMessage();
	}
    }

    /**
     * Square product update function
     */
    public function SquareProductUpdate($pId, $post_data) {
	$name = $post_data['pName'];
	$sku = $post_data['pSKU'];
	$description = $post_data['pDescription'];
	$product_categories = (isset($post_data['pcat'])) ? $post_data['pcat'] : array();
	$product_qty = $post_data['pStk_qty'];
	$price = $post_data['pPrice'];
	$stock_ntf = '0';
	if ($post_data['plow_stk_ntf'] != '') {
	    $stock_ntf = $post_data['plow_stk_ntf'];
	}
	$suqare_catetory_id = '';
	if (!empty($product_categories)) {
	    foreach ($product_categories as $cat) {
		$cat = Categories::find()->where(['category_name' => $cat])->one();
		$suqare_catetory_data = CategoryAbbrivation::find()->where(['category_ID' => $cat->category_ID, 'channel_accquired' => 'Square'])->one();
		if (!empty($suqare_catetory_data)) {
		    $suqare_catetory_id = substr($suqare_catetory_data->channel_abb_id, 2);
		    break;
		}
	    }
	}

	$product_price = $product_sale_price = $product_stock_status = $product_stock_level = $product_stock_qty = $product_schedule_date = '';
	/* Connected Square Price */
	$product_price = self::getConnectedPrice($pId, 'Square');

	/* Connected Square Sale Price */
	$product_sale_price = self::getConnectedSalePrice($pId, 'Square');

	/* Connected Square Schedule date */
	$product_schedule_date = self::getConnectedScheduleDate($pId, 'Square');

	/* Connected Square Stock Quantity */
	$product_stock_qty = self::getConnectedStockQty($pId, 'Square');

	/* Connected Square Stock Status */
	$product_stock_status = self::getConnectedStockStatus($pId, 'Square');

	/* Connected Square Stock Level */
	$product_stock_level = self::getConnectedStockLevel($pId, 'Square');

	/* Connected Square Stock Allocate inv */
	$product_allocate_inv = self::getConnectedAllocateInventory($pId, 'Square');

	$user_id = Yii::$app->user->identity->id;
	$channel_data = Channels::find()->where(['channel_name' => 'Square'])->one();
	$square_channel_id = $channel_data->channel_ID;
	$square_details = ChannelConnection::find()->where(['user_id' => $user_id, 'channel_id' => $square_channel_id])->one();
	if (!empty($square_details)) {
	    require_once($_SERVER['DOCUMENT_ROOT'] . '/backend/web/square/vendor/autoload.php');
	    \SquareConnect\Configuration::getDefaultConfiguration()->setAccessToken($square_details->square_personal_access_token);
	    $location_id = self::getSquareLocationId();
	    $product_data = ProductAbbrivation::find()->Where(['product_id' => $pId, 'channel_accquired' => 'Square'])->one();
	    if (empty($product_data)) {
		return;
	    }
	    $square_product_id = $product_data->channel_abb_id;
	    $item_id = substr($square_product_id, 2);
	    $api_instance = new \SquareConnect\Api\V1ItemsApi();
	    $inventory_body = new \SquareConnect\Model\V1AdjustInventoryRequest();
	    $item_body = new \SquareConnect\Model\V1Item();
	    $variation_body = new \SquareConnect\Model\V1Variation();
	    try {
		$item_body = array(
		    'name' => $name,
		    'description' => $description,
		    'visibility' => 'PRIVATE',
		    'category_id' => $suqare_catetory_id,
		);
		$product_result = $api_instance->updateItem($location_id, $item_id, $item_body);
		//echo "<pre>"; print_r($product_result); echo "</pre>";
		$variation = $product_result['variations'];
		foreach ($variation as $_variation) {
		    $variation_id = $_variation['id'];
		    $inventory_list = self::retireveSquareProductInventory();
		    foreach ($inventory_list as $_inventory) {
			if ($_inventory['variation_id'] == $variation_id) {
			    $qty_on_hand = $_inventory['quantity_on_hand'];
			    if ($product_stock_qty > $qty_on_hand) {
				$final_product_qty = $product_stock_qty - $qty_on_hand;
				$inventory_body['quantity_delta'] = $final_product_qty;
				$inventory_body['adjustment_type'] = '';
				$inventory_body['memo'] = '';
				try {
				    $result = $api_instance->adjustInventory($location_id, $variation_id, $inventory_body);
				} catch (Exception $e) {
				    echo 'Exception when calling V1ItemsApi->adjustInventory: ', $e->getMessage();
				}
			    }
			    if ($product_stock_qty < $qty_on_hand) {
				$final_product_qty = $qty_on_hand - $product_stock_qty;
				$inventory_body['quantity_delta'] = -$final_product_qty;
				$inventory_body['adjustment_type'] = '';
				$inventory_body['memo'] = '';
				try {
				    $result = $api_instance->adjustInventory($location_id, $variation_id, $inventory_body);
				} catch (Exception $e) {
				    echo 'Exception when calling V1ItemsApi->adjustInventory: ', $e->getMessage();
				}
			    }
			}
		    }
		}
		try {
		    $variation_body = array(
			'name' => 'Regular',
			'pricing_type' => 'FIXED_PRICING',
			'price_money' => array(
			    'currency_code' => 'USD',
			    'amount' => $product_price * 100,
			),
			'sku' => $sku,
			'track_inventory' => true,
			'inventory_alert_type' => 'LOW_QUANTITY',
			'inventory_alert_threshold' => $stock_ntf,
		    );
		    $variation_result = $api_instance->updateVariation($location_id, $item_id, $variation_id, $variation_body);

		    $update_product_abb = ProductAbbrivation::find()->where(['product_iD' => $pId, 'channel_accquired' => 'Square'])->one();
		    $update_product_abb->price = $product_price;
		    $update_product_abb->salePrice = $product_sale_price;
		    $update_product_abb->schedules_sales_date = $product_schedule_date;
		    $update_product_abb->stock_quantity = $product_stock_qty;
		    $update_product_abb->allocate_inventory = $product_allocate_inv;
		    $update_product_abb->stock_level = $product_stock_level;
		    $update_product_abb->stock_status = $product_stock_status;
		    //$update_product_abb->low_stock_notification = $product_stock_notification;
		    $update_product_abb->save(false);

		    //echo "<pre>"; print_r($variation_result); echo "<pre>";
		} catch (Exception $e) {
		    echo 'Exception when calling V1ItemsApi->updateVariation: ', $e->getMessage(), PHP_EOL;
		}
	    } catch (Exception $e) {
		echo 'Exception when calling V1ItemsApi->updateItem: ', $e->getMessage(), PHP_EOL;
	    }
	}
    }

    public function squareGetSingleProduct($item_id) {
	$api_instance = new \SquareConnect\Api\V1ItemsApi();
	$location_id = self::getSquareLocationId();
	try {
	    $result = $api_instance->retrieveItem($location_id, $item_id);
	    return $result;
	} catch (Exception $e) {
	    echo 'Exception when calling V1ItemsApi->retrieveItem: ', $e->getMessage(), PHP_EOL;
	}
    }

    public function squareProductStockNull($pId, $qty) {
	$user_id = Yii::$app->user->identity->id;
	$channel_data = Channels::find()->where(['channel_name' => 'Square'])->one();
	$square_channel_id = $channel_data->channel_ID;
	$square_details = ChannelConnection::find()->where(['user_id' => $user_id, 'channel_id' => $square_channel_id])->one();
	if (!empty($square_details)) {
	    require_once($_SERVER['DOCUMENT_ROOT'] . '/backend/web/square/vendor/autoload.php');
	    \SquareConnect\Configuration::getDefaultConfiguration()->setAccessToken($square_details->square_personal_access_token);
	    $api_instance = new \SquareConnect\Api\V1ItemsApi();
	    $inventory_body = new \SquareConnect\Model\V1AdjustInventoryRequest();
	    $location_id = self::getSquareLocationId();
	    $product_data = ProductAbbrivation::find()->Where(['product_id' => $pId, 'channel_accquired' => 'Square'])->one();
	    $square_product_id = $product_data->channel_abb_id;
	    $item_id = substr($square_product_id, 2);
	    $square_product_data = self::squareGetSingleProduct($item_id);
	    $variation = $square_product_data['variations'];
	    foreach ($variation as $_variation) {
		$variation_id = $_variation['id'];
		$inventory_list = self::retireveSquareProductInventory();
		foreach ($inventory_list as $_inventory) {
		    if ($_inventory['variation_id'] == $variation_id) {
			$qty_on_hand = $_inventory['quantity_on_hand'];
			if ($qty > $qty_on_hand) {
			    $final_product_qty = $qty - $qty_on_hand;
			    $inventory_body['quantity_delta'] = $final_product_qty;
			    $inventory_body['adjustment_type'] = '';
			    $inventory_body['memo'] = '';
			    try {
				$result = $api_instance->adjustInventory($location_id, $variation_id, $inventory_body);
			    } catch (Exception $e) {
				echo 'Exception when calling V1ItemsApi->adjustInventory: ', $e->getMessage();
			    }
			}
			if ($qty < $qty_on_hand) {
			    $final_product_qty = $qty_on_hand - $qty;
			    $inventory_body['quantity_delta'] = -$final_product_qty;
			    $inventory_body['adjustment_type'] = '';
			    $inventory_body['memo'] = '';
			    try {
				$result = $api_instance->adjustInventory($location_id, $variation_id, $inventory_body);
			    } catch (Exception $e) {
				echo 'Exception when calling V1ItemsApi->adjustInventory: ', $e->getMessage();
			    }
			}
		    }
		}
	    }
	}
    }

    public function retireveSquareProductInventory() {
	$api_instance = new \SquareConnect\Api\V1ItemsApi();
	$location_id = self::getSquareLocationId();
	$limit = 1000;
	try {
	    $result = $api_instance->listInventory($location_id, $limit);
	    return $result;
	} catch (Exception $e) {
	    echo 'Exception when calling V1ItemsApi->listInventory: ', $e->getMessage(), PHP_EOL;
	}
    }

    public function getConnectedPrice($pId, $name) {

	$product_price = '';
	$connected_price = isset($_POST['Connected_price']) ? $_POST['Connected_price'] : '';
	if (!empty($connected_price)) {
	    foreach ($connected_price as $cp_key_price => $cp) :
		if ($cp_key_price == $name):
		    $product_price = $cp;
		endif;
	    endforeach;
	}
	else {
	    $product_price_data = Products::find()->where(['id' => $pId])->one();
	    $product_price = $product_price_data->price;
	}

	if ($product_price == '' || $product_price == null) {
	    $product_price_data = Products::find()->where(['id' => $pId])->one();
	    $product_price = $product_price_data->price;
	}
	return $product_price;
    }

    public function getConnectedSalePrice($pId, $name) {
	$product_sale_price = '';
	$connected_sale_price = isset($_POST['Connected_sale_price']) ? $_POST['Connected_sale_price'] : '';
	if (!empty($connected_sale_price)):
	    foreach ($connected_sale_price as $cp_key_sale_price => $csp) :
		if ($cp_key_sale_price == $name):
		    $product_sale_price = $csp;
		endif;
	    endforeach;
	else:
	    $product_saleprice_data = Products::find()->where(['id' => $pId])->one();
	    $product_sale_price = $product_saleprice_data->sales_price;
	endif;

	if ($product_sale_price == '' || $product_sale_price == null):
	    $product_saleprice_data = Products::find()->where(['id' => $pId])->one();
	    $product_sale_price = $product_saleprice_data->sales_price;
	endif;
	return $product_sale_price;
    }

    public function getConnectedScheduleDate($pId, $name) {
	$product_schedule_date = '';
	$Connected_schedule_date = isset($_POST['Connected_schedule_date']) ? $_POST['Connected_schedule_date'] : '';
	if (!empty($Connected_schedule_date)):
	    foreach ($Connected_schedule_date as $cs_key_schedule_date => $csd) :
		if ($cs_key_schedule_date == $name and ! empty($csd) and $csd != 'Empty'):
		    $product_schedule_date = $csd;
		endif;
	    endforeach;
	else:
	    $product_scheduledate_data = Products::find()->where(['id' => $pId])->one();
	    $product_schedule_date = $product_scheduledate_data->schedules_sales_date;
	endif;

	if ($product_schedule_date == '' || $product_schedule_date == null):
	    $product_scheduledate_data = Products::find()->where(['id' => $pId])->one();
	    $product_schedule_date = $product_scheduledate_data->schedules_sales_date;
	endif;
	return $product_schedule_date;
    }

    public function getConnectedStockQty($pId, $name) {
	$product_stock_qty = '';
	$Connected_stock_quantity = isset($_POST['Connected_stock_qty']) ? $_POST['Connected_stock_qty'] : '';
	if (!empty($Connected_stock_quantity)):
	    foreach ($Connected_stock_quantity as $cs_key_stock_quantity => $csq) :
		if ($cs_key_stock_quantity == $name):
		    $product_stock_qty = $csq;
		endif;
	    endforeach;
	else:
	    $stock_status_qty = Products::find()->where(['id' => $pId])->one();
	    $product_stock_qty = $stock_status_qty->stock_quantity;
	endif;

	if ($product_stock_qty == '' || $product_stock_qty == null):
	    $stock_status_qty = Products::find()->where(['id' => $pId])->one();
	    $product_stock_qty = $stock_status_qty->stock_quantity;
	endif;
	return $product_stock_qty;
    }

    public function getConnectedStockStatus($pId, $name) {
	$product_stock_status = '';
	$Connected_stock_status = isset($_POST['Connected_stock_status']) ? $_POST['Connected_stock_status'] : '';
	if (!empty($Connected_stock_status)):
	    foreach ($Connected_stock_status as $cs_key_stock_status => $cstatus) :
		if ($cs_key_stock_status == $name):
		    $product_stock_status = $cstatus;
		    if ($product_stock_status == 'Hidden'):
			$final_product_stock_status = false;
		    else:
			$final_product_stock_status = true;
		    endif;
		endif;
	    endforeach;
	else:
	    $stock_status_product = Products::find()->where(['id' => $pId])->one();
	    $product_stock_status = $stock_status_product->stock_status;
	endif;

	if ($product_stock_status == '' || $product_stock_status == null):
	    $stock_status_product = Products::find()->where(['id' => $pId])->one();
	    $product_stock_status = $stock_status_product->stock_status;
	endif;
	return $product_stock_status;
    }

    public function getConnectedStockLevel($pId, $name) {
	$product_stock_level = '';
	$Connected_stock_level = isset($_POST['Connected_stock_level']) ? $_POST['Connected_stock_level'] : '';
	if (!empty($Connected_stock_level)):
	    foreach ($Connected_stock_level as $cs_key_stock_level => $clevel) :
		if ($cs_key_stock_level == $name):
		    $product_stock_level = $clevel;
		endif;
	    endforeach;
	else:
	    $stock_product = Products::find()->where(['id' => $pId])->one();
	    $product_stock_level = $stock_product->stock_level;
	endif;

	if ($product_stock_level == '' || $product_stock_level == null):
	    $stock_product = Products::find()->where(['id' => $pId])->one();
	    $product_stock_level = $stock_product->stock_level;
	endif;
	return $product_stock_level;
    }

    public function getConnectedAllocateInventory($pId, $name) {
	$product_allocate_inv = '';
	$Connected_allocate = isset($_POST['Connected_allocate_inv']) ? $_POST['Connected_allocate_inv'] : '';
	$product_allocate_inv = 0;
	if (!empty($Connected_allocate)):
	    foreach ($Connected_allocate as $cs_Connected_allocate => $callocate) :
		if ($cs_Connected_allocate == $name):
		    $product_allocate_inv = $callocate;
		endif;
	    endforeach;
	else:
	    $stock_allocate = Products::find()->where(['id' => $pId])->one();
	    $product_allocate_inv = $stock_allocate->allocate_inventory;
	endif;
	return $product_allocate_inv;
    }

    /**
     * Shopify product create
     */
    public function shopifyProductCreate($pId, $post_data, $store_details_data, $store_name) {
	$user_id = Yii::$app->user->identity->id;
	$name = $post_data['pName'];
	$sku = $post_data['pSKU'];
	$barcode = $post_data['pUPC'];
	$description = $post_data['pDescription'];
	$product_categories = isset($post_data['pcat']) ? $post_data['pcat'] : array();
	$stock_qty = $post_data['pStk_qty'];
	$price = $post_data['pPrice'];

	$schedule_date_time = $post_data['pSchedule_date'];
	$weight = $post_data['pWeight'];
	$stock_ntf = '0';
	if ($post_data['plow_stk_ntf'] != '') {
	    $stock_ntf = $post_data['plow_stk_ntf'];
	}

	if (!empty($store_details_data)) {
	    $store_connection_id = $store_details_data->store_connection_id;
	    $shopify_shop = $store_details_data->storeConnection->url;
	    $shopify_api_key = $store_details_data->storeConnection->api_key;
	    $shopify_api_password = $store_details_data->storeConnection->key_password;
	    $shopify_shared_secret = $store_details_data->storeConnection->shared_secret;
	    $currency = $store_details_data->currency;
	    
	    $conversion_rate = 1;
	    if (!empty($currency)) {
		$conversion_rate = self::getDbConversionRate($currency);
	    }
	    
	    //$conversion_rate = self::getCurrencyConversionRate('USD', $currency);
	    $product_price = number_format((float) $conversion_rate * $price, 2, '.', '');

	    $product_images = ProductImages::find()->where(['product_ID' => $pId])->all();
	    $image_array = array();
	    foreach ($product_images as $product_img) {
		$image_link = $product_img->link;
		$position = $product_img->priority;
		$image_array[] = array(
		    'position' => $position,
		    'src' => $image_link
		);
	    }

	    $sc = new Shopify($shopify_shop, $shopify_api_password, $shopify_api_key, $shopify_shared_secret);

	    $product_object = array(
		'product' => array(
		    'title' => $name,
		    'body_html' => $description,
		    'variants' => [array(
		    "inventory_management" => "shopify",
		    'inventory_quantity' => $stock_qty,
		    'sku' => $sku,
		    'price' => $product_price,
		    'barcode' => $barcode,
		    'weight' => $weight,
			)],
		    'images' => [$image_array],
		),
	    );

	    $product_result = $sc->call('POST', '/admin/products.json', $product_object);
	    $shopify_product_id = $product_result['id'];

	    $productAbbrivation = new ProductAbbrivation();
	    $productAbbrivation->channel_abb_id = 'SP' . $shopify_product_id;
	    $productAbbrivation->product_id = $pId;
	    $productAbbrivation->mul_store_id = $store_connection_id;
	    $productAbbrivation->channel_accquired = $store_name;
	    $productAbbrivation->price = $price;
	    $productAbbrivation->salePrice = $price;
	    $productAbbrivation->schedules_sales_date = $schedule_date_time;
	    $productAbbrivation->stock_quantity = $post_data['pStk_qty'];
	    $productAbbrivation->stock_level = $post_data['pStk_lvl'];
	    $productAbbrivation->stock_status = $post_data['pStk_status'];
	    //$productAbbrivation->low_stock_notification = $low_stc_ntf;
	    $productAbbrivation->created_at = date('Y-m-d H:i:s', time());
	    $productAbbrivation->save(false);

	    $product_data = new ProductChannel;
	    $product_data->store_id = $store_connection_id;
	    $product_data->product_id = $pId;
	    $product_data->status = 'yes';
	    $product_data->created_at = date('Y-m-d h:i:s', time());
	    $product_data->save(false);

	    if (!empty($product_categories)) {
		/* Delaete Product Categories */
		$Delete_product_categories = ProductCategories::deleteAll(['product_ID' => $pId]);
		foreach ($product_categories as $_category) {
		    /* get wechat category acc to name */
		    $category_detail = Categories::find()->Where(['category_name' => $_category])->one();
		    $productCategoryModel = new ProductCategories;
		    $productCategoryModel->category_ID = $category_detail->category_ID;
		    ;
		    $productCategoryModel->product_ID = $pId;
		    $productCategoryModel->created_at = date('Y-m-d H:i:s', time());
		    //Save Elliot User id
		    $productCategoryModel->elliot_user_id = $user_id;
		    $productCategoryModel->save(false);
		}
	    }
	}
    }

    public function ShopifyProductUpdate($pId, $post_data, $store_details_data, $store_name) {
	$name = $post_data['pName'];
	$sku = $post_data['pSKU'];
	$barcode = $post_data['pUPC'];
	$description = $post_data['pDescription'];
	//$product_categories = $post_data['pcat'];
	$stock_qty = $post_data['pStk_qty'];
	$price = $post_data['pPrice'];
	$schedule_date_time = $post_data['pSchedule_date'];
	$weight = $post_data['pWeight'];
	$stock_ntf = '0';
	if ($post_data['plow_stk_ntf'] != '') {
	    $stock_ntf = $post_data['plow_stk_ntf'];
	}

	if (!empty($store_details_data)) {
	    $user_id = Yii::$app->user->identity->id;
	    $store_connection_id = $store_details_data->store_connection_id;
	    $shopify_shop = $store_details_data->storeConnection->url;
	    $shopify_api_key = $store_details_data->storeConnection->api_key;
	    $shopify_api_password = $store_details_data->storeConnection->key_password;
	    $shopify_shared_secret = $store_details_data->storeConnection->shared_secret;
	    $currency = $store_details_data->currency;

	    $product_data = ProductAbbrivation::find()->Where(['product_id' => $pId, 'channel_accquired' => $store_name, 'mul_store_id' => $store_connection_id])->one();
	    if (!empty($product_data)) {
		$square_product_id = $product_data->channel_abb_id;
		$Shopify_product_id = substr($square_product_id, 2);
		$product_price = $product_sale_price = $product_stock_status = $product_stock_level = $product_stock_qty = $product_schedule_date = '';
		/* Connected Shopify Price */
		$product_price = self::getConnectedPrice($pId, $store_name . $store_connection_id);

		$conversion_rate = 1;
		if (!empty($currency)) {
		    $conversion_rate = self::getDbConversionRate($currency);
		}
		//$conversion_rate = self::getCurrencyConversionRate('USD', $currency);
		$shopify_product_price = number_format((float) $conversion_rate * $product_price, 2, '.', '');

		/* Connected Shopify Sale Price */
		$product_sale_price = self::getConnectedSalePrice($pId, $store_name . $store_connection_id);
		/* Connected Shopify Schedule date */
		$product_schedule_date = self::getConnectedScheduleDate($pId, $store_name . $store_connection_id);
		/* Connected Shopify Stock Quantity */
		$product_stock_qty = self::getConnectedStockQty($pId, $store_name . $store_connection_id);
		/* Connected Shopify Stock Status */
		$product_stock_status = self::getConnectedStockStatus($pId, $store_name . $store_connection_id);
		/* Connected Shopify Stock Level */
		$product_stock_level = self::getConnectedStockLevel($pId, $store_name . $store_connection_id);
		/* Connected Shopify Stock Allocate inv */
		$product_allocate_inv = self::getConnectedAllocateInventory($pId, $store_name . $store_connection_id);

		$sc = new Shopify($shopify_shop, $shopify_api_password, $shopify_api_key, $shopify_shared_secret);
		$result = $sc->call('GET', '/admin/products/' . $Shopify_product_id . '.json');
		$variant_id = $result['variants'][0]['id'];
		/* $variant_images = $result['images'];
		  $product_images = ProductImages::find()->where(['product_ID' => $pId])->all();
		  if(!empty($product_images)){
		  $image_array = array();
		  foreach ($product_images as $product_img){
		  $image_link = $product_img->link;
		  $position = $product_img->priority;
		  $image_array[] = array(
		  'position'=>$position,
		  'src'=>$image_link
		  );
		  }
		  } */

		//$product_object = array(
		//    'product' => array(
		//        'id' => $Shopify_product_id,
		//        'title' => $name,
		//        'body_html' => $description,
		//        'variants' => [array(
		//        'id' => $variant_id,
		//        "inventory_management" => "shopify",
		//        'inventory_quantity' => $product_stock_qty,
		//        'sku' => $sku,
		//        'price' => $shopify_product_price,
		//        'barcode' => $barcode,
		//        'weight' => $weight,
		//            )],
		//    ),
		//);

		$variant = array(
		    'variant' => array(
			'id' => $variant_id,
			'price' => $shopify_product_price,
			"inventory_management" => "shopify",
			"inventory_quantity" => $product_stock_qty,
		    ),
		);
		$product_variants = $sc->call('PUT', '/admin/variants/' . $variant_id . '.json', $variant);
		//echo "<pre>"; print_r($product_object); die('hererererererer!');
		//$result = $sc->call('PUT', '/admin/products/' . $Shopify_product_id . '.json', $product_object);

		$update_product_abb = ProductAbbrivation::find()->where(['product_id' => $pId, 'channel_accquired' => $store_name, 'mul_store_id' => $store_connection_id])->one();
		if (!empty($update_product_abb)) {
		    $update_product_abb->price = $product_price;
		    $update_product_abb->salePrice = $product_sale_price;
		    $update_product_abb->schedules_sales_date = $product_schedule_date;
		    $update_product_abb->stock_quantity = $product_stock_qty;
		    $update_product_abb->allocate_inventory = $product_allocate_inv;
		    $update_product_abb->stock_level = $product_stock_level;
		    $update_product_abb->stock_status = $product_stock_status;
		    $update_product_abb->save(false);
		}
	    }
	}
    }

    public function vtexProductUpdate($pId, $post_data, $store_details_data) {
	$sku = $post_data['pSKU'];

	if (!empty($store_details_data)) {
	    $user_id = Yii::$app->user->identity->id;
	    $store_connection_id = $store_details_data->store_connection_id;
	    $vtex_account = $store_details_data->storeConnection->vtex_account_name;
	    $vtex_app_key = $store_details_data->storeConnection->vtex_app_key;
	    $vtex_app_token = $store_details_data->storeConnection->vtex_app_token;
	    $currency = $store_details_data->currency;
	    $store_name = $store_details_data->channel_accquired;

	    $product_data = ProductAbbrivation::find()->Where(['product_id' => $pId, 'channel_accquired' => $store_name, 'mul_store_id' => $store_connection_id])->one();
	    if (!empty($product_data)) {
		//$square_product_id = $product_data->channel_abb_id;
		//$Shopify_product_id = substr($square_product_id, 2);
		$product_price = $product_sale_price = $product_stock_status = $product_stock_level = $product_stock_qty = $product_schedule_date = '';
		/* Connected Shopify Price */
		$product_price = self::getConnectedPrice($pId, $store_name . $store_connection_id);
		
		$conversion_rate = 1;
		if (!empty($currency)) {
		    $conversion_rate = self::getDbConversionRate($currency);
		}
		//$conversion_rate = self::getCurrencyConversionRate('USD', $currency);
		$vtex_product_price = number_format((float) $conversion_rate * $product_price, 2, '.', '');

		/* Connected Shopify Sale Price */
		$product_sale_price = self::getConnectedSalePrice($pId, $store_name . $store_connection_id);
		/* Connected Shopify Schedule date */
		$product_schedule_date = self::getConnectedScheduleDate($pId, $store_name . $store_connection_id);
		/* Connected Shopify Stock Quantity */
		$product_stock_qty = self::getConnectedStockQty($pId, $store_name . $store_connection_id);
		/* Connected Shopify Stock Status */
		$product_stock_status = self::getConnectedStockStatus($pId, $store_name . $store_connection_id);
		/* Connected Shopify Stock Level */
		$product_stock_level = self::getConnectedStockLevel($pId, $store_name . $store_connection_id);
		/* Connected Shopify Stock Allocate inv */
		$product_allocate_inv = self::getConnectedAllocateInventory($pId, $store_name . $store_connection_id);

		Vtex::vtexChangeProductPrice($vtex_account, $vtex_app_key, $vtex_app_token, $sku, $vtex_product_price);

		//Vtex::getVtexWarehouses($vtex_account, $vtex_app_key, $vtex_app_token);
		Vtex::vtexChangeProductQty($vtex_account, $vtex_app_key, $vtex_app_token, $sku, $product_stock_qty);

		$update_product_abb = ProductAbbrivation::find()->where(['product_id' => $pId, 'channel_accquired' => $store_name, 'mul_store_id' => $store_connection_id])->one();
		if (!empty($update_product_abb)) {
		    $update_product_abb->price = $product_price;
		    $update_product_abb->salePrice = $product_sale_price;
		    $update_product_abb->schedules_sales_date = $product_schedule_date;
		    $update_product_abb->stock_quantity = $product_stock_qty;
		    $update_product_abb->allocate_inventory = $product_allocate_inv;
		    $update_product_abb->stock_level = $product_stock_level;
		    $update_product_abb->stock_status = $product_stock_status;
		    $update_product_abb->save(false);
		}
	    }
	}
    }

    /**
     * Shopify product stock null
     */
    public function shopifyProductStockNull($pId, $product_stk_qty, $store_details_data, $store_name) {
	$user_id = Yii::$app->user->identity->id;
	if (!empty($store_details_data)) {
	    $store_connection_id = $store_details_data->store_connection_id;
	    $shopify_shop = $store_details_data->storeConnection->url;
	    $shopify_api_key = $store_details_data->storeConnection->api_key;
	    $shopify_api_password = $store_details_data->storeConnection->key_password;
	    $shopify_shared_secret = $store_details_data->storeConnection->shared_secret;

	    /* Get channel Abbrivation Id */
	    $product_data = ProductAbbrivation::find()->where(['product_id' => $pId, 'channel_accquired' => $store_name, 'mul_store_id' => $store_connection_id])->one();
	    if (!empty($product_data)) {
		$square_product_id = $product_data->channel_abb_id;
		$shopify_product_id = substr($square_product_id, 2);
		$sc = new Shopify($shopify_shop, $shopify_api_password, $shopify_api_key, $shopify_shared_secret);
		$result = $sc->call('GET', '/admin/products/' . $shopify_product_id . '.json');
		$variant_id = $result['variants'][0]['id'];

		//$product_update = array(
		//    'product' => array(
		//        'id' => $shopify_product_id,
		//        'variants' => array([
		//                'id' => $variant_id,
		//                'inventory_quantity' => $product_stk_qty,
		//            ]),
		//    )
		//);
		//$sc->call('PUT', '/admin/products/' . $shopify_product_id . '.json', $product_update);

		$variant = array(
		    'variant' => array(
			'id' => $variant_id,
			"inventory_management" => "shopify",
			"inventory_quantity" => $product_stk_qty,
		    ),
		);
		$product_variants = $sc->call('PUT', '/admin/variants/' . $variant_id . '.json', $variant);
	    }
	}
    }

    public function vtexProductStockNull($pId, $product_stk_qty, $store_details_data, $store_name) {
	if (!empty($store_details_data)) {
	    $user_id = Yii::$app->user->identity->id;
	    $store_connection_id = $store_details_data->store_connection_id;
	    $vtex_account = $store_details_data->storeConnection->vtex_account_name;
	    $vtex_app_key = $store_details_data->storeConnection->vtex_app_key;
	    $vtex_app_token = $store_details_data->storeConnection->vtex_app_token;
	    $currency = $store_details_data->currency;
	    $store_name = $store_details_data->channel_accquired;
	    $product_data = ProductAbbrivation::find()->where(['product_id' => $pId, 'channel_accquired' => $store_name, 'mul_store_id' => $store_connection_id])->with('product')->one();
	    if (!empty($product_data)) {
		$sku = $product_data->product->SKU;
		Vtex::vtexChangeProductQty($vtex_account, $vtex_app_key, $vtex_app_token, $sku, $product_stk_qty);
	    }
	}
    }

    /**
     * Magento product create function
     */
    public function magentoProductCreate($pId, $post_data, $store_details_data) {
	$name = $post_data['pName'];
	$sku = $post_data['pSKU'];
	$barcode = $post_data['pUPC'];
	$description = $post_data['pDescription'];
	$product_categories = isset($post_data['pcat']) ? $post_data['pcat'] : array();
	$stock_qty = isset($post_data['pStk_qty']) ? $post_data['pStk_qty'] : 0;
	$price = isset($post_data['pPrice']) ? $post_data['pPrice'] : 0;
	$schedule_date_time = $post_data['pSchedule_date'];
	$weight = isset($post_data['pWeight']) ? $post_data['pWeight'] : '';
	$is_in_stock = $post_data['pStk_lvl'];
	if ($is_in_stock == 'In Stock') {
	    $stock_status = 1;
	} else {
	    $stock_status = 0;
	}
	$stock_ntf = '0';
	if ($post_data['plow_stk_ntf'] != '') {
	    $stock_ntf = $post_data['plow_stk_ntf'];
	}
	$magento_cat_array = array();
	if (!empty($product_categories)) {
	    foreach ($product_categories as $cat) {
		$checkCategoryName = Categories::find()->where(['category_name' => $cat])->one();
		if (!empty($checkCategoryName)) {
		    $category_id = $checkCategoryName->category_ID;
		    $checkCategoryAbbrivation = CategoryAbbrivation::find()->where(['category_ID' => $category_id, 'channel_accquired' => 'Magento'])->one();
		    if (!empty($checkCategoryAbbrivation)) {
			$magento_cat_id = substr($checkCategoryAbbrivation->channel_abb_id, 2);
			$magento_cat_array[] = $magento_cat_id;
		    }
		}
	    }
	}
	$user_id = Yii::$app->user->identity->id;
	if (!empty($store_details_data)) {
	    $store_connection_id = $store_details_data->store_connection_id;
	    $magento_shop = $store_details_data->storeConnection->mag_shop;
	    $magento_soap_user = $store_details_data->storeConnection->mag_soap_user;
	    $magento_soap_api_key = $store_details_data->storeConnection->mag_soap_api;
	    $currency = $store_details_data->currency;
	    
	    $conversion_rate = 1;
	    if (!empty($currency)) {
		$conversion_rate = self::getDbConversionRate($currency);
	    }
	    //$conversion_rate = self::getCurrencyConversionRate('USD', $currency);
	    $product_price = number_format((float) $conversion_rate * $price, 2, '.', '');

	    $get_magento_id = Stores::find()->select('store_id')->where(['store_name' => 'Magento'])->one();
	    $magento_store_id = $get_magento_id->store_id;

	    try {
		$api_url = $magento_shop . '/api/soap/?wsdl';
		$cli = new SoapClient($api_url);
		$session_id = $cli->login($magento_soap_user, $magento_soap_api_key);
		$checkProduct = ProductAbbrivation::find()->where(['product_id' => $pId, 'channel_accquired' => 'Magento', 'mul_store_id' => $store_connection_id])->one();
		if (empty($checkProduct)) {
		    $attributeSets = $cli->call($session_id, 'product_attribute_set.list');
		    $attributeSet = current($attributeSets);
		    $product_result = $cli->call($session_id, 'catalog_product.create', array('simple', $attributeSet['set_id'], $sku, array(
			    'categories' => $magento_cat_array,
			    'websites' => array(1),
			    'name' => $name,
			    'description' => $description,
			    'short_description' => $description,
			    'weight' => $weight,
			    'status' => '1',
			    'visibility' => '4',
			    'price' => $product_price,
			    'tax_class_id' => 1,
			    'stock_data' => array(
				'qty' => $stock_qty,
				'is_in_stock' => $stock_status
			    ),
		    )));
		    $product_images = ProductImages::find()->where(['product_ID' => $pId, 'default_image' => 'yes'])->one();
		    $image_array = array();
		    if (!empty($product_images)) {
			$image_link = $product_images->link;
			$position = $product_images->priority;
			$file_data = base64_encode(file_get_contents($image_link));
			$file = array(
			    'content' => $file_data,
			    'mime' => 'image/jpeg'
			);
			$result = $cli->call($session_id, 'catalog_product_attribute_media.create', array(
			    $product_result, array(
				'file' => $file,
				'label' => 'image',
				'position' => $position,
				'types' => array('thumbnail', 'small_image', 'image'),
				'exclude' => 0
			    )
				)
			);
		    }

		    $productAbbrivation = new ProductAbbrivation();
		    $productAbbrivation->channel_abb_id = 'MG' . $product_result;
		    $productAbbrivation->product_id = $pId;
		    $productAbbrivation->channel_accquired = 'Magento';
		    $productAbbrivation->price = $price;
		    $productAbbrivation->mul_store_id = $store_connection_id;
		    $productAbbrivation->salePrice = $price;
		    $productAbbrivation->schedules_sales_date = $schedule_date_time;
		    $productAbbrivation->stock_quantity = $post_data['pStk_qty'];
		    $productAbbrivation->stock_level = $post_data['pStk_lvl'];
		    $productAbbrivation->stock_status = $post_data['pStk_status'];
		    //$productAbbrivation->low_stock_notification = $low_stc_ntf;
		    $productAbbrivation->created_at = date('Y-m-d H:i:s', time());
		    $productAbbrivation->save(false);

		    $product_data = new ProductChannel;
		    $product_data->store_id = $store_connection_id;
		    $product_data->product_id = $pId;
		    $product_data->status = 'yes';
		    $product_data->created_at = date('Y-m-d h:i:s', time());
		    $product_data->save(false);

		    if (!empty($product_categories)) {
			/* Delaete Product Categories */
			$Delete_product_categories = ProductCategories::deleteAll(['product_ID' => $pId]);
			foreach ($product_categories as $_category) {
			    /* get wechat category acc to name */
			    $category_detail = Categories::find()->Where(['category_name' => $_category])->one();
			    $productCategoryModel = new ProductCategories;
			    $productCategoryModel->category_ID = $category_detail->category_ID;
			    ;
			    $productCategoryModel->product_ID = $pId;
			    $productCategoryModel->created_at = date('Y-m-d H:i:s', time());
			    //Save Elliot User id
			    $productCategoryModel->elliot_user_id = $user_id;
			    $productCategoryModel->save(false);
			}
		    }
		} else {
		    $magento_product_id = preg_replace("/[^A-Z]+/", "", $checkProduct->channel_abb_id);
		    $stockItemData = array(
			'qty' => $stock_qty,
			'is_in_stock' => $stock_status,
		    );
		    $price = preg_replace('/[^a-zA-Z0-9_.]/', '', $price);
		    $product_update_data = array(
			'price' => $price,
		    );
		    $result = $cli->call($session_id, 'product_stock.update', array($magento_product_id, $stockItemData));
		    $result = $cli->call($session_id, 'catalog_product.update', array($magento_product_id, $product_update_data));

		    $product_data = ProductChannel::find()->where(['product_id' => $pId, 'store_id' => $magento_store_id])->one();
		    $product_data->status = 'yes';
		    $product_data->save(false);
		}
	    } catch (SoapFault $e) {
		$e->faultstring;
	    }
	}
    }

    /**
     * Magento product update
     */
    public function magentoProductUpdate($pId, $post_data, $store_details_data) {
	$name = $post_data['pName'];
	$sku = $post_data['pSKU'];
	$barcode = $post_data['pUPC'];
	$description = $post_data['pDescription'];
	$product_categories = isset($post_data['pcat']) ? $post_data['pcat'] : array();
	$stock_qty = isset($post_data['pStk_qty']) ? $post_data['pStk_qty'] : 0;
	$price = isset($post_data['pPrice']) ? $post_data['pPrice'] : 0;
	$schedule_date_time = $post_data['pSchedule_date'];
	$weight = isset($post_data['pWeight']) ? $post_data['pWeight'] : '';
	$is_in_stock = $post_data['pStk_lvl'];
	$stock_ntf = '0';
	if ($post_data['plow_stk_ntf'] != '') {
	    $stock_ntf = $post_data['plow_stk_ntf'];
	}
	$magento_cat_array = array();
	if (!empty($product_categories)) {
	    foreach ($product_categories as $cat) {
		$checkCategoryName = Categories::find()->where(['category_name' => $cat])->one();
		if (!empty($checkCategoryName)) {
		    $category_id = $checkCategoryName->category_ID;
		    $checkCategoryAbbrivation = CategoryAbbrivation::find()->where(['category_ID' => $category_id, 'channel_accquired' => 'Magento'])->one();
		    if (!empty($checkCategoryAbbrivation)) {
			$magento_cat_id = substr($checkCategoryAbbrivation->channel_abb_id, 2);
			$magento_cat_array[] = $magento_cat_id;
		    }
		}
	    }
	}
	$user_id = Yii::$app->user->identity->id;

	if (!empty($store_details_data)) {
	    $store_connection_id = $store_details_data->store_connection_id;
	    $magento_shop = $store_details_data->storeConnection->mag_shop;
	    $magento_soap_user = $store_details_data->storeConnection->mag_soap_user;
	    $magento_soap_api_key = $store_details_data->storeConnection->mag_soap_api;
	    $currency = $store_details_data->currency;

	    $get_magento_id = Stores::find()->select('store_id')->where(['store_name' => 'Magento'])->one();
	    $magento_store_id = $get_magento_id->store_id;

	    try {
		$api_url = $magento_shop . '/api/soap/?wsdl';
		$cli = new SoapClient($api_url);
		$session_id = $cli->login($magento_soap_user, $magento_soap_api_key);
		$checkProduct = ProductAbbrivation::find()->where(['product_id' => $pId, 'channel_accquired' => 'Magento', 'mul_store_id' => $store_connection_id])->one();
		if (!empty($checkProduct)) {
		    /* Connected Shopify Price */
		    $product_price = self::getConnectedPrice($pId, 'Magento' . $store_connection_id);
		    
		    $conversion_rate = 1;
		    if (!empty($currency)) {
			$conversion_rate = self::getDbConversionRate($currency);
		    }
		    //$conversion_rate = self::getCurrencyConversionRate('USD', $currency);
		    $magento_product_price = number_format((float) $conversion_rate * $product_price, 2, '.', '');

		    /* Connected Shopify Sale Price */
		    $product_sale_price = self::getConnectedSalePrice($pId, 'Magento' . $store_connection_id);
		    /* Connected Shopify Schedule date */
		    $product_schedule_date = self::getConnectedScheduleDate($pId, 'Magento' . $store_connection_id);
		    /* Connected Shopify Stock Quantity */
		    $product_stock_qty = self::getConnectedStockQty($pId, 'Magento' . $store_connection_id);
		    /* Connected Shopify Stock Status */
		    $product_stock_status = self::getConnectedStockStatus($pId, 'Magento' . $store_connection_id);
		    if ($product_stock_status == 'In Stock') {
			$stock_status = 1;
		    } else {
			$stock_status = 0;
		    }
		    /* Connected Shopify Stock Level */
		    $product_stock_level = self::getConnectedStockLevel($pId, 'Magento' . $store_connection_id);
		    /* Connected Shopify Stock Allocate inv */
		    $product_allocate_inv = self::getConnectedAllocateInventory($pId, 'Magento' . $store_connection_id);
		    $product_result = $cli->call($session_id, 'catalog_product.update', array($sku, array(
			    'categories' => $magento_cat_array,
			    'websites' => array(1),
			    'name' => $name,
			    'description' => $description,
			    'short_description' => $description,
			    'weight' => $weight,
			    'status' => '1',
			    'visibility' => '4',
			    'price' => $magento_product_price,
			    'tax_class_id' => 1,
			    'stock_data' => array(
				'qty' => $product_stock_qty,
				'is_in_stock' => $stock_status
			    ),
		    )));

		    $update_product_abb = ProductAbbrivation::find()->where(['product_iD' => $pId, 'channel_accquired' => 'Magento', 'mul_store_id' => $store_connection_id])->one();
		    $update_product_abb->price = $product_price;
		    $update_product_abb->salePrice = $product_sale_price;
		    $update_product_abb->schedules_sales_date = $product_schedule_date;
		    $update_product_abb->stock_quantity = $product_stock_qty;
		    $update_product_abb->allocate_inventory = $product_allocate_inv;
		    $update_product_abb->stock_level = $product_stock_level;
		    $update_product_abb->stock_status = $product_stock_status;
		    $update_product_abb->save(false);
		}
	    } catch (SoapFault $e) {
		$e->faultstring;
	    }
	}
    }

    /**
     * Magento Product stock Null
     */
    public function magentoProductStockNull($pId, $qty, $store_details_data) {
	$user_id = Yii::$app->user->identity->id;
	if (!empty($store_details_data)) {
	    $store_connection_id = $store_details_data->store_connection_id;
	    $magento_shop = $store_details_data->storeConnection->mag_shop;
	    $magento_soap_user = $store_details_data->storeConnection->mag_soap_user;
	    $magento_soap_api_key = $store_details_data->storeConnection->mag_soap_api;

	    $product_data = ProductAbbrivation::find()->Where(['product_id' => $pId, 'channel_accquired' => 'Magento', 'mul_store_id' => $store_connection_id])->one();
	    if (!empty($product_data)) {
		$magento_product_id = $product_data->channel_abb_id;
		$magento_product_id = substr($magento_product_id, 2);

		$get_magento_id = Stores::find()->select('store_id')->where(['store_name' => 'Magento'])->one();
		$magento_store_id = $get_magento_id->store_id;

		$stockItemData = array(
		    'qty' => $qty,
		    'is_in_stock' => 0,
		);

		try {
		    $cli = new SoapClient($magento_shop . "/api/soap/?wsdl");
		    $session_id = $cli->login($magento_soap_user, $magento_soap_api_key);
		    $result = $cli->call($session_id, 'product_stock.update', array($magento_product_id, $stockItemData));
		} catch (SoapFault $e) {
		    $e->faultcode;
		    $msg = $e->faultstring;
		    //error_log($msg . " Magento product id is " . $magento_product_id . " and Elliot id is " . $pId, 3, 'product_update_error.log');
		}
	    }
	}
    }

    /**
     * Magento 2 product create
     */
    public function magento2ProductCreate($pId, $post_data, $store_details_data) {
	$name = $post_data['pName'];
	$sku = $post_data['pSKU'];
	$barcode = $post_data['pUPC'];
	$description = $post_data['pDescription'];
	$product_categories = isset($post_data['pcat']) ? $post_data['pcat'] : array();
	$stock_qty = isset($post_data['pStk_qty']) ? $post_data['pStk_qty'] : 0;
	$price = isset($post_data['pPrice']) ? $post_data['pPrice'] : 0;
	$schedule_date_time = $post_data['pSchedule_date'];
	$weight = isset($post_data['pWeight']) ? $post_data['pWeight'] : '';
	$is_in_stock = $post_data['pStk_lvl'];
	if ($is_in_stock == 'In Stock') {
	    $stock_status = 1;
	} else {
	    $stock_status = 0;
	}
	$stock_ntf = '0';
	if ($post_data['plow_stk_ntf'] != '') {
	    $stock_ntf = $post_data['plow_stk_ntf'];
	}

	if (!empty($store_details_data)) {
	    $store_connection_id = $store_details_data->store_connection_id;
	    $magento_shop = $store_details_data->storeConnection->mag_shop;
	    $magento_2_access_token = $store_details_data->storeConnection->mag_soap_api;
	    $currency = $store_details_data->currency;
	    $product_create_url = $magento_shop . '/index.php/rest/V1/products';
	    
	    $conversion_rate = 1;
	    if (!empty($currency)) {
		$conversion_rate = self::getDbConversionRate($currency);
	    }
	    //$conversion_rate = self::getCurrencyConversionRate('USD', $currency);
	    $product_price = number_format((float) $conversion_rate * $price, 2, '.', '');

	    $magento_cat_array = array();
	    if (!empty($product_categories)) {
		foreach ($product_categories as $cat) {
		    $checkCategoryName = Categories::find()->where(['category_name' => $cat])->one();
		    if (!empty($checkCategoryName)) {
			$category_id = $checkCategoryName->category_ID;
			$checkCategoryAbbrivation = CategoryAbbrivation::find()->where(['category_ID' => $category_id, 'channel_accquired' => 'Magento2', 'mul_store_id' => $store_connection_id])->one();
			$magento_cat_id = substr($checkCategoryAbbrivation->channel_abb_id, 3);
			$magento_cat_array[] = $magento_cat_id;
		    }
		}
	    }
	    $user_id = Yii::$app->user->identity->id;
	    $get_magento2_id = Stores::find()->select('store_id')->where(['store_name' => 'Magento2'])->one();
	    $magento2_store_id = $get_magento2_id->store_id;

	    $product_data = array(
		"product" => array(
		    "sku" => $sku,
		    "name" => $name,
		    "attribute_set_id" => 4,
		    "price" => $product_price,
		    "status" => 1,
		    "visibility" => 4,
		    "type_id" => "simple",
		    "extension_attributes" => array(
			"stock_item" => array(
			    "manage_stock" => 1,
			    "is_in_stock" => $stock_status,
			    "qty" => $stock_qty
			),
		    ),
		    "custom_attributes" => array(
			array(
			    "attribute_code" => "tax_class_id",
			    "value" => 2
			),
			array(
			    "attribute_code" => "category_ids",
			    "value" => $magento_cat_array
			),
			array(
			    "attribute_code" => "description",
			    "value" => $description
			),
			array(
			    "attribute_code" => "short_description",
			    "value" => $description
			),
		    ),
		),
	    );
	    $result = self::magentoProductCurl($product_create_url, $magento_2_access_token, $product_data, "POST");
	    $mage_product_data = json_decode($result);
	    if (isset($mage_product_data->message) && $mage_product_data->message == "Request does not match any route." || isset($mage_product_data->message) && $mage_product_data->message == "Consumer is not authorized to access %resources") {
		return;
	    } else {
		$product_images = ProductImages::find()->where(['product_ID' => $pId, 'priority' => 1])->one();
		if (!empty($product_images)) {
		    $image_link = $product_images->link;
		    $position = $product_images->priority;
		    $filename_from_url = parse_url($image_link);
		    $ext = pathinfo($filename_from_url['path'], PATHINFO_EXTENSION);
		    $file_data = base64_encode(file_get_contents($image_link));
		    $product_image = array(
			"entry" => array(
			    "media_type" => "image",
			    "label" => $sku,
			    "disabled" => false,
			    "types" => array(
				"image",
				"small_image",
				"thumbnail"
			    ),
			    "file" => "string",
			    "content" => array(
				"base64_encoded_data" => $file_data,
				"type" => "image/jpeg",
				"name" => $sku . ".jpeg"
			    ),
			),
		    );

		    $product_image_create_url = $magento_shop . '/index.php/rest/V1/products/' . $sku . '/media';
		    self::magentoProductCurl($product_image_create_url, $magento_2_access_token, $product_image, "POST");
		}
		$productAbbrivation = new ProductAbbrivation();
		$productAbbrivation->channel_abb_id = 'MG2' . $mage_product_data->id;
		$productAbbrivation->product_id = $pId;
		$productAbbrivation->channel_accquired = 'Magento2';
		$productAbbrivation->mul_store_id = $store_connection_id;
		$productAbbrivation->price = $price;
		$productAbbrivation->salePrice = $price;
		$productAbbrivation->schedules_sales_date = $schedule_date_time;
		$productAbbrivation->stock_quantity = $stock_qty;
		$productAbbrivation->stock_level = $post_data['pStk_lvl'];
		$productAbbrivation->stock_status = $post_data['pStk_status'];
		//$productAbbrivation->low_stock_notification = $low_stc_ntf;
		$productAbbrivation->created_at = date('Y-m-d H:i:s', time());
		$productAbbrivation->save(false);

		$product_data = new ProductChannel;
		$product_data->store_id = $store_connection_id;
		$product_data->product_id = $pId;
		$product_data->status = 'yes';
		$product_data->created_at = date('Y-m-d h:i:s', time());
		$product_data->save(false);
	    }
	}
    }

    /**
     * Magento 2 product update
     */
    public function magento2ProductUpdate($pId, $post_data, $store_details_data) {
	$name = $post_data['pName'];
	$sku = $post_data['pSKU'];
	$barcode = $post_data['pUPC'];
	$description = $post_data['pDescription'];
	$product_categories = isset($post_data['pcat']) ? $post_data['pcat'] : array();
	$stock_qty = isset($post_data['pStk_qty']) ? $post_data['pStk_qty'] : 0;
	$price = isset($post_data['pPrice']) ? $post_data['pPrice'] : 0;
	$schedule_date_time = $post_data['pSchedule_date'];
	$weight = isset($post_data['pWeight']) ? $post_data['pWeight'] : '';
	$is_in_stock = $post_data['pStk_lvl'];
	$stock_ntf = '0';
	if ($post_data['plow_stk_ntf'] != '') {
	    $stock_ntf = $post_data['plow_stk_ntf'];
	}

	if (!empty($store_details_data)) {
	    $store_connection_id = $store_details_data->store_connection_id;
	    $magento_shop = $store_details_data->storeConnection->mag_shop;
	    $magento_2_access_token = $store_details_data->storeConnection->mag_soap_api;
	    $currency = $store_details_data->currency;

	    $product_create_url = $magento_shop . '/index.php/rest/V1/products';

	    $magento_cat_array = array();
	    if (!empty($product_categories)) {
		foreach ($product_categories as $cat) {
		    $checkCategoryName = Categories::find()->where(['category_name' => $cat])->one();
		    if (!empty($checkCategoryName)) {
			$category_id = $checkCategoryName->category_ID;
			$checkCategoryAbbrivation = CategoryAbbrivation::find()->where(['category_ID' => $category_id, 'channel_accquired' => 'Magento', 'mul_store_id' => $store_connection_id])->one();
			if (!empty($checkCategoryAbbrivation)) {
			    $magento_cat_id = substr($checkCategoryAbbrivation->channel_abb_id, 2);
			    $magento_cat_array[] = $magento_cat_id;
			}
		    }
		}
	    }
	    $user_id = Yii::$app->user->identity->id;
	    $get_magento2_id = Stores::find()->select('store_id')->where(['store_name' => 'Magento2'])->one();
	    $magento2_store_id = $get_magento2_id->store_id;


	    /* Connected Shopify Price */
	    $product_price = self::getConnectedPrice($pId, 'Magento2' . $store_connection_id);
	    $conversion_rate = 1;
	    if (!empty($currency)) {
		$conversion_rate = self::getDbConversionRate($currency);
	    }
	    //$conversion_rate = self::getCurrencyConversionRate('USD', $currency);
	    $magento_product_price = number_format((float) $conversion_rate * $product_price, 2, '.', '');
	    /* Connected Shopify Sale Price */
	    $product_sale_price = self::getConnectedSalePrice($pId, 'Magento2' . $store_connection_id);
	    /* Connected Shopify Schedule date */
	    $product_schedule_date = self::getConnectedScheduleDate($pId, 'Magento2' . $store_connection_id);
	    /* Connected Shopify Stock Quantity */
	    $product_stock_qty = self::getConnectedStockQty($pId, 'Magento2' . $store_connection_id);
	    /* Connected Shopify Stock Status */
	    $product_stock_status = self::getConnectedStockStatus($pId, 'Magento2' . $store_connection_id);
	    if ($product_stock_status == 'In Stock') {
		$stock_status = 1;
	    } else {
		$stock_status = 0;
	    }
	    /* Connected Shopify Stock Level */
	    $product_stock_level = self::getConnectedStockLevel($pId, 'Magento2' . $store_connection_id);
	    /* Connected Shopify Stock Allocate inv */

	    $product_allocate_inv = self::getConnectedAllocateInventory($pId, 'Magento2' . $store_connection_id);
	    $product_data = array(
		"product" => array(
		    "sku" => $sku,
		    "name" => $name,
		    "attribute_set_id" => 4,
		    "price" => $magento_product_price,
		    "status" => 1,
		    "visibility" => 4,
		    "type_id" => "simple",
		    "extension_attributes" => array(
			"stock_item" => array(
			    "manage_stock" => 1,
			    "is_in_stock" => $stock_status,
			    "qty" => $product_stock_qty
			),
		    ),
		    "custom_attributes" => array(
			array(
			    "attribute_code" => "tax_class_id",
			    "value" => 2
			),
			array(
			    "attribute_code" => "category_ids",
			    "value" => $magento_cat_array
			),
			array(
			    "attribute_code" => "description",
			    "value" => $description
			),
			array(
			    "attribute_code" => "short_description",
			    "value" => $description
			),
		    ),
		),
	    );
	    $result = self::magentoProductCurl($product_create_url, $magento_2_access_token, $product_data, "POST");

	    $update_product_abb = ProductAbbrivation::find()->where(['product_id' => $pId, 'channel_accquired' => 'Magento2', 'mul_store_id' => $store_connection_id])->one();
	    if (!empty($update_product_abb)) {
		$update_product_abb->price = $product_price;
		$update_product_abb->salePrice = $product_sale_price;
		$update_product_abb->schedules_sales_date = $product_schedule_date;
		$update_product_abb->stock_quantity = $product_stock_qty;
		$update_product_abb->allocate_inventory = $product_allocate_inv;
		$update_product_abb->stock_level = $product_stock_level;
		$update_product_abb->stock_status = $product_stock_status;
		$update_product_abb->save(false);
	    }
	}
    }

    /**
     * Magento Product stock Null
     */
    public function magento2ProductStockNull($pId, $qty, $store_details_data) {
	if (!empty($store_details_data)) {
	    $store_connection_id = $store_details_data->store_connection_id;
	    $magento_shop = $store_details_data->storeConnection->mag_shop;
	    $magento_2_access_token = $store_details_data->storeConnection->mag_soap_api;
	    $product_data = ProductAbbrivation::find()->Where(['product_id' => $pId, 'channel_accquired' => 'Magento2', 'mul_store_id' => $store_connection_id])->one();
	    if (!empty($product_data)) {
		$user_id = Yii::$app->user->identity->id;
		$product = Products::find()->Where(['id' => $pId])->one();
		$sku = $product->SKU;
		$stock_data = array(
		    'stockItem' => array(
			'qty' => $qty,
		    ),
		);
		$product_stock_update_url = $magento_shop . '/index.php/rest/V1/products/' . $sku . '/stockItems/1';
		$result = self::magentoProductCurl($product_stock_update_url, $magento_2_access_token, $stock_data, 'PUT');
	    }
	}
    }

    /**
     * For magento product create curl
     */
    public function magentoProductCurl($url, $access_token, $post_data, $methode) {
	$curl = curl_init();

	curl_setopt_array($curl, array(
	    CURLOPT_URL => $url,
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_ENCODING => "",
	    CURLOPT_MAXREDIRS => 10,
	    CURLOPT_TIMEOUT => 30,
	    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	    CURLOPT_CUSTOMREQUEST => $methode,
	    CURLOPT_POSTFIELDS => json_encode($post_data),
	    CURLOPT_HTTPHEADER => array(
		"authorization: Bearer " . $access_token,
		"cache-control: no-cache",
		"content-type: application/json",
		"postman-token: a42259e6-9a49-399c-084d-e195a4247a5b"
	    ),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);
	curl_close($curl);
	if ($err) {
	    //echo "cURL Error #:" . $err;
	    return 0;
	} else {
	    return $response;
	}
    }

    /* Use For Percentage */

    public function get_percentage($percentage, $of) {
	//courtesy : http://stackoverflow.com/a/14880194
	$percent = $percentage / $of;
	return number_format($percent * 100, 2) . '%';
    }

    protected function lazadaMalaysiaProductCreate($pId, $post_data, $channel) {
	//get wechat Service account id 
	$lazada = \backend\models\Channels::find()->where(['channel_name' => $channel])->one();
//echo'<pre>';
	$channel_id = $lazada->channel_ID;
//        print_r($lazada);
//        echo $channel_id;
//        echo 'new';
//die;
	$user_id = Yii::$app->user->identity->id;

	$channel_connected = ChannelConnection::find()->where(['channel_id' => $channel_id, 'user_id' => $user_id])->one();
	$user_email = $channel_connected->lazada_user_email;
	$url = $channel_connected->lazada_api_url;
	$api_key = $channel_connected->lazada_api_key;
	$name = @$post_data['pName'];
	$brand = @$post_data['pBrand'];
	$sku = @$post_data['pSKU'];
	$barcode = @$post_data['pUPC'];
	$description = @$post_data['pDescription'];
	$product_categories = @$post_data['pcat'];
	$stock_qty = @$post_data['pStk_qty'];
	$price = @$post_data['pPrice'];
	$special_price = @$post_data['pSale_price'];
	$schedule_date = @$post_data['pSchedule_date'];
//        echo $schedule_date;
//        $schedule_date_time = date('Y-m-d', strtotime(@$schedule_date));
	$date1 = strtr(@$schedule_date, '/', '-');
	$schedule_date_time = date('Y-m-d', strtotime($date1));
//echo $schedule_date_time;
	$weight = $post_data['pWeight'];
	$stock_ntf = '0';
	if ($post_data['plow_stk_ntf'] != '') {
	    $stock_ntf = $post_data['plow_stk_ntf'];
	}

	$conversion_rate = 1;
	$channel_setting = Channelsetting::find()->where(['channel_name' => $channel, 'user_id' => Yii::$app->user->identity->id, 'setting_key' => 'currency'])->asArray()->one();
//        print_r($channel_setting);
//print_r($channel_setting);
	if (isset($channel_setting) and ! empty($channel_setting)) {
	    $channel_currency = $channel_setting['setting_value'];
	    if (!empty($channel_currency)) {
		$conversion_rate = self::getDbConversionRate($channel_currency);
	    }
	    //$conversion_rate = self::getCurrencyConversionRate('USD', $channel_currency);
	}
//print_R($conversion_rate);


	$product_images = ProductImages::find()->where(['product_ID' => $pId])->all();



	$parameters = array(
	    // The user ID for which we are making the call.
	    'UserID' => $user_email,
	    // The API version. Currently must be 1.0
	    'Version' => '1.0',
	    // The API method to call.
	    'Action' => 'CreateProduct',
	    // The format of the result.
	    'Format' => 'JSON',
	    // The current time formatted as ISO8601
	    'Timestamp' => date('c')
	);
// Sort parameters by name.
	ksort($parameters);
	$encoded = array();
	foreach ($parameters as $names => $value) {
	    $encoded[] = rawurlencode($names) . '=' . rawurlencode($value);
	}
	$concatenated = implode('&', $encoded);
	$parameters['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, $api_key, false));

	$queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);

	$input_request = "<?xml version='1.0' encoding='UTF-8'?>
            <Request>
            <Product>
                <PrimaryCategory>10000671</PrimaryCategory>
            <SPUId></SPUId>
        <Attributes>
            <name>" . $name . "</name>
            <name_ms>" . $name . "</name_ms>
            <warranty_type>Local Manufacturer Warranty</warranty_type>
             <short_description>" . $name . "</short_description>
             <brand>Remark</brand>
             <model>" . $brand . "</model>
             <warranty>11 Months</warranty>
        </Attributes>
        <Skus>
            <Sku>
                <SellerSku>" . $sku . "</SellerSku>
                <tax_class>default</tax_class>
                <color_family>Green</color_family>
                <size>40</size>
                <quantity>" . $stock_qty . "</quantity>
                <price>" . number_format((float) $price * $conversion_rate, 2, '.', '') . "</price>
                <special_price>" . number_format((float) $special_price * $conversion_rate, 2, '.', '') . "</special_price>
                <special_from_date>" . $schedule_date_time . "</special_from_date>
                <package_length>11</package_length>
                <package_height>22</package_height>
                <package_weight>33</package_weight>
                <package_width>44</package_width>
                <package_content>this is what's in the box</package_content>
                 <Images>
                 </Images>
            </Sku>          
        </Skus>
    </Product>
</Request>";

	$target = $url . '/?' . $queryString;
	file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/backend/web/test.xml', $input_request);
	$tmpFile = $_SERVER['DOCUMENT_ROOT'] . '/backend/web/test.xml';
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_PUT, 1);
	curl_setopt($curl, CURLOPT_HEADER, false);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($curl, CURLOPT_INFILESIZE, filesize($tmpFile));
	curl_setopt($curl, CURLOPT_INFILE, ($in = fopen($tmpFile, 'r')));
	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
	curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/x-www-form-urlencoded']);
	curl_setopt($curl, CURLOPT_URL, $target);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	$result = curl_exec($curl);
	curl_close($curl);
	fclose($in);
	$result_decoded = json_decode($result, true);
//        echo'<pre>';
//        echo'ithe';
//        print_r($input_request);
////
//        print_R($result);
//        print_R($result_decoded);
////        
//        die;

	if (isset($result_decoded['SuccessResponse']) and ! empty($result_decoded['SuccessResponse'])) {
//            echo 'in';
	    $productAbbrivation = new ProductAbbrivation();
	    $productAbbrivation->channel_abb_id = 'LZ' . $sku;
	    $productAbbrivation->product_id = $pId;
	    $productAbbrivation->channel_accquired = $channel;
	    $productAbbrivation->price = $price;
	    $productAbbrivation->salePrice = $price;
	    $productAbbrivation->schedules_sales_date = $schedule_date_time;
	    $productAbbrivation->stock_quantity = $post_data['pStk_qty'];
	    $productAbbrivation->stock_level = $post_data['pStk_lvl'];
	    $productAbbrivation->stock_status = $post_data['pStk_status'];
	    //$productAbbrivation->low_stock_notification = $low_stc_ntf;
	    $productAbbrivation->created_at = date('Y-m-d H:i:s', time());
	    $productAbbrivation->save(false);

	    $product_data = new ProductChannel;
	    $product_data->channel_id = $channel_id;
	    $product_data->product_id = $pId;
	    $product_data->status = 'yes';
	    $product_data->created_at = date('Y-m-d h:i:s', time());
	    $product_data->save(false);

	    if (!empty($product_categories)) {
		/* Delaete Product Categories */
		$Delete_product_categories = ProductCategories::deleteAll(['product_ID' => $pId]);
		foreach ($product_categories as $_category) {
		    /* get wechat category acc to name */
		    $category_detail = Categories::find()->Where(['category_name' => $_category])->one();
		    $productCategoryModel = new ProductCategories;
		    $productCategoryModel->category_ID = $category_detail->category_ID;
		    $productCategoryModel->product_ID = $pId;
		    $productCategoryModel->created_at = date('Y-m-d H:i:s', time());
		    //Save Elliot User id
		    $productCategoryModel->elliot_user_id = $user_id;
		    $productCategoryModel->save(false);
		}
	    }
	}
    }

    public function LazadaMalasysiaProductUpdate($pId, $post_data, $channel) {
	$lazada = \backend\models\Channels::find()->where(['channel_name' => $channel])->one();
//echo'<pre>';
	$channel_id = $lazada->channel_ID;
//        print_r($lazada);
//        echo $channel_id;
//        echo 'new';
//die;
	$user_id = Yii::$app->user->identity->id;

	$channel_connected = ChannelConnection::find()->where(['channel_id' => $channel_id, 'user_id' => $user_id])->one();
	$user_email = $channel_connected->lazada_user_email;
	$url = $channel_connected->lazada_api_url;
	$api_key = $channel_connected->lazada_api_key;



	$name = $post_data['pName'];
	$sku = $post_data['pSKU'];
	$barcode = $post_data['pUPC'];
	$description = $post_data['pDescription'];
	//$product_categories = $post_data['pcat'];
	$stock_qty = $post_data['pStk_qty'];
	$price = $post_data['pPrice'];
	$schedule_date_time = $post_data['pSchedule_date'];
	$weight = $post_data['pWeight'];
	$stock_ntf = '0';
	if ($post_data['plow_stk_ntf'] != '') {
	    $stock_ntf = $post_data['plow_stk_ntf'];
	}

	$product_data = ProductAbbrivation::find()->Where(['product_id' => $pId, 'channel_accquired' => 'Lazada Malaysia'])->one();
	if (!empty($product_data)) {
	    $square_product_id = $product_data->channel_abb_id;
	    $Shopify_product_id = substr($square_product_id, 2);
	    $product_price = $product_sale_price = $product_stock_status = $product_stock_level = $product_stock_qty = $product_schedule_date = '';
	    /* Connected Shopify Price */
	    $product_price = self::getConnectedPrice($pId, 'Lazada Malaysia');
	    /* Connected Shopify Sale Price */
	    $product_sale_price = self::getConnectedSalePrice($pId, 'Lazada Malaysia');
	    /* Connected Shopify Schedule date */
	    $product_schedule_date = self::getConnectedScheduleDate($pId, 'Lazada Malaysia');
	    if (empty($product_schedule_date)) {
		$product_schedule_date = $_POST['pSchedule_date'];
	    }
	    $product_schedule_date = date('Y-m-d', strtotime($product_schedule_date));



	    /* Connected Shopify Stock Quantity */
	    $product_stock_qty = self::getConnectedStockQty($pId, 'Lazada Malaysia');
	    /* Connected Shopify Stock Status */
	    $product_stock_status = self::getConnectedStockStatus($pId, 'Lazada Malaysia');
	    /* Connected Shopify Stock Level */
	    $product_stock_level = self::getConnectedStockLevel($pId, 'Lazada Malaysia');
	    /* Connected Shopify Stock Allocate inv */
	    $product_allocate_inv = self::getConnectedAllocateInventory($pId, 'Lazada Malaysia');

	    $user_id = Yii::$app->user->identity->id;
	    /* $variant_images = $result['images'];
	      $product_images = ProductImages::find()->where(['product_ID' => $pId])->all();
	      if(!empty($product_images)){
	      $image_array = array();
	      foreach ($product_images as $product_img){
	      $image_link = $product_img->link;
	      $position = $product_img->priority;
	      $image_array[] = array(
	      'position'=>$position,
	      'src'=>$image_link
	      );
	      }
	      } */
	    $conversion_rate = 1;
	    $channel_setting = Channelsetting::find()->where(['channel_name' => 'Lazada Malaysia', 'user_id' => Yii::$app->user->identity->id, 'setting_key' => 'currency'])->asArray()->one();
	    //print_r($prdouct_pricing);
//print_r($channel_setting);die;
	    if (isset($channel_setting) and ! empty($channel_setting)) {
		$channel_currency = $channel_setting['setting_value'];
		if (!empty($channel_currency)) {
		    $conversion_rate = self::getDbConversionRate($channel_currency);
		}
		//$conversion_rate = self::getCurrencyConversionRate($channel_currency, 'USD');
	    }


	    $parameters = array(
		// The user ID for which we are making the call.
		'UserID' => $user_email,
		// The API version. Currently must be 1.0
		'Version' => '1.0',
		// The API method to call.
		'Action' => 'UpdateProduct',
		// The format of the result.
		'Format' => 'JSON',
		// The current time formatted as ISO8601
		'Timestamp' => date('c')
	    );
	    ksort($parameters);
	    $encoded = array();
	    foreach ($parameters as $name => $value) {
		$encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
	    }

	    $concatenated = implode('&', $encoded);
	    $parameters['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, $api_key, false));

	    $queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);
	    $target = $url . '/?' . $queryString;

	    $content = "<?xml version='1.0' encoding='UTF-8'?>
                    <Request>
                        <Product>
                            <Attributes>
                            </Attributes>
                            <Skus>
                                <Sku>
                                    <SellerSku>" . $sku . "</SellerSku>
                                    <quantity>" . $product_stock_qty . "</quantity>
                                    <price>" . $product_price . "</price>
                                    <special_price>" . $product_sale_price . "</special_price>
                                    <special_from_date>" . $product_schedule_date . "</special_from_date>
                                </Sku>

                            </Skus>
                        </Product>
                    </Request>
                    ";

	    file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/backend/web/test.xml', $content);

	    $tmpFile = $_SERVER['DOCUMENT_ROOT'] . '/backend/web/test.xml';
	    $curl = curl_init();
	    curl_setopt($curl, CURLOPT_PUT, 1);
	    curl_setopt($curl, CURLOPT_HEADER, true);
	    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
	    curl_setopt($curl, CURLOPT_INFILESIZE, filesize($tmpFile));
	    curl_setopt($curl, CURLOPT_INFILE, ($in = fopen($tmpFile, 'r')));
	    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
	    curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/x-www-form-urlencoded']);
	    curl_setopt($curl, CURLOPT_URL, $target);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	    $result = curl_exec($curl);
//            echo"<pre>";
//            print_r($result);
	    curl_close($curl);
//closing the open file CURLOPT_INFILE
	    fclose($in);

	    $update_product_abb = ProductAbbrivation::find()->where(['product_iD' => $pId, 'channel_accquired' => 'Lazada Malaysia'])->one();
	    $update_product_abb->price = number_format((float) $product_price * $conversion_rate, 2, '.', '');
	    $update_product_abb->salePrice = number_format((float) $product_sale_price * $conversion_rate, 2, '.', '');
	    $update_product_abb->schedules_sales_date = $product_schedule_date;
	    $update_product_abb->stock_quantity = $product_stock_qty;
	    $update_product_abb->allocate_inventory = $product_allocate_inv;
	    $update_product_abb->stock_level = $product_stock_level;
	    $update_product_abb->stock_status = $product_stock_status;
	    $update_product_abb->save(false);
	}
    }

    public function lazadaMalaysiaProductStockNull($pId, $channel, $quantity) {

	$lazada = \backend\models\Channels::find()->where(['channel_name' => $channel])->one();
//echo'<pre>';
	$channel_id = $lazada->channel_ID;
//        print_r($lazada);
//        echo $channel_id;
//        echo 'new';
//die;
	$user_id = Yii::$app->user->identity->id;

	$channel_connected = ChannelConnection::find()->where(['channel_id' => $channel_id, 'user_id' => $user_id])->one();
	$user_email = $channel_connected->lazada_user_email;
	$url = $channel_connected->lazada_api_url;
	$api_key = $channel_connected->lazada_api_key;
	$product_data = Products::find()->Where(['id' => $pId])->one();
	if (!empty($product_data)) {


	    $parameters = array(
		// The user ID for which we are making the call.
		'UserID' => $user_email,
		// The API version. Currently must be 1.0
		'Version' => '1.0',
		// The API method to call.
		'Action' => 'UpdateProduct',
		// The format of the result.
		'Format' => 'JSON',
		// The current time formatted as ISO8601
		'Timestamp' => date('c')
	    );
	    ksort($parameters);
	    $encoded = array();
	    foreach ($parameters as $name => $value) {
		$encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
	    }

	    $concatenated = implode('&', $encoded);
	    $parameters['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, $api_key, false));

	    $queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);
	    $target = $url . '/?' . $queryString;

	    $content = "<?xml version='1.0' encoding='UTF-8'?>
                    <Request>
                        <Product>
                            <Attributes>
                            </Attributes>
                            <Skus>
                                <Sku>
                                    <SellerSku>" . $product_data->SKU . "</SellerSku>
                                    <quantity>" . $quantity . "</quantity>
                                    <price>" . $product_data->price . "</price>
                                    <special_price>" . $product_data->sales_price . "</special_price>
                                    <special_from_date>" . $product_data->schedules_sales_date . "</special_from_date>
                                </Sku>

                            </Skus>
                        </Product>
                    </Request>
                    ";

	    file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/backend/web/test.xml', $content);

	    $tmpFile = $_SERVER['DOCUMENT_ROOT'] . '/backend/web/test.xml';
	    $curl = curl_init();
	    curl_setopt($curl, CURLOPT_PUT, 1);
	    curl_setopt($curl, CURLOPT_HEADER, true);
	    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
	    curl_setopt($curl, CURLOPT_INFILESIZE, filesize($tmpFile));
	    curl_setopt($curl, CURLOPT_INFILE, ($in = fopen($tmpFile, 'r')));
	    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
	    curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/x-www-form-urlencoded']);
	    curl_setopt($curl, CURLOPT_URL, $target);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	    $result = curl_exec($curl);
	    curl_close($curl);
//closing the open file CURLOPT_INFILE
	    fclose($in);
	}
    }

    /**
     * Common function for Customer Importing for all channels/stores
     */
    public static function customerImportingCommon($customer_data) {

	/** Customer Basic Information   */
	$customer_id = $customer_data['customer_id'];
	$user_id = $customer_data['elliot_user_id'];
	$prefix_id = $customer_data['channel_store_prefix'];
	$Prefix_customer_abb_id = $prefix_id . $customer_id;
	$first_name = $customer_data['first_name'];
	$last_name = $customer_data['last_name'];
	$customer_email = $customer_data['email'];
	$channel_name = $customer_data['channel_store_name'];
	$store_connection_id = $customer_data['mul_store_id'];
	$channel_connection_id = $customer_data['mul_channel_id'];
	$customer_create_date = $customer_data['customer_created_at'];
	$customer_update_date = $customer_data['customer_updated_at'];

	/** Customer Billing Address information */
	$bill_add_1 = $customer_data['billing_address']['street_1'];
	$bill_add_2 = $customer_data['billing_address']['street_2'];
	$bill_city = $customer_data['billing_address']['city'];
	$bill_country = $customer_data['billing_address']['country'];
	$bill_country_code = $customer_data['billing_address']['country_iso'];
	$bill_state = $customer_data['billing_address']['state'];
	$bill_zip = $customer_data['billing_address']['zip'];
	$bill_phone = $customer_data['billing_address']['phone_number'];
	$address_type = $customer_data['billing_address']['address_type'];

	/** Customer Shipping Address information  */
	$ship_add_1 = $customer_data['shipping_address']['street_1'];
	$ship_add_2 = $customer_data['shipping_address']['street_2'];
	$ship_city = $customer_data['shipping_address']['city'];
	$ship_country = $customer_data['shipping_address']['country'];
	$ship_state = $customer_data['shipping_address']['state'];
	$ship_zip = $customer_data['shipping_address']['zip'];


	if ($store_connection_id == '') {
	    $customer_db = CustomerAbbrivation::find()->Where(['channel_abb_id' => $Prefix_customer_abb_id])->one();
	    $check_customer_phone = CustomerUser::find()->Where(['phone_number' => $bill_phone])
			    ->with(['customerabbrivation' => function($query) use ($channel_name) {
				    $query->andWhere(['channel_accquired' => $channel_name]);
				}])->asArray()->one();
	    $check_customer_email = CustomerUser::find()->Where(['email' => $customer_email])
			    ->with(['customerabbrivation' => function($query) use ($channel_name) {
				    $query->andWhere(['channel_accquired' => $channel_name]);
				}])->asArray()->one();
	    $check_customer_name = CustomerUser::find()->Where(['first_name' => $first_name, 'last_name' => $last_name])
			    ->with(['customerabbrivation' => function($query) use ($channel_name) {
				    $query->andWhere(['channel_accquired' => $channel_name]);
				}])->asArray()->one();
	} else {
	    $customer_db = CustomerAbbrivation::find()->Where(['channel_abb_id' => $Prefix_customer_abb_id, 'mul_store_id' => $store_connection_id])->one();
	    $check_customer_phone = CustomerUser::find()->Where(['phone_number' => $bill_phone])
			    ->with(['customerabbrivation' => function($query) use ($channel_name, $store_connection_id) {
				    $query->andWhere(['channel_accquired' => $channel_name]);
				    $query->andWhere(['mul_store_id' => $store_connection_id]);
				}])->asArray()->one();
	    $check_customer_email = CustomerUser::find()->Where(['email' => $customer_email])
			    ->with(['customerabbrivation' => function($query) use ($channel_name, $store_connection_id) {
				    $query->andWhere(['channel_accquired' => $channel_name]);
				    $query->andWhere(['mul_store_id' => $store_connection_id]);
				}])->asArray()->one();
	    $check_customer_name = CustomerUser::find()->Where(['first_name' => $first_name, 'last_name' => $last_name])
			    ->with(['customerabbrivation' => function($query) use ($channel_name, $store_connection_id) {
				    $query->andWhere(['channel_accquired' => $channel_name]);
				    $query->andWhere(['mul_store_id' => $store_connection_id]);
				}])->asArray()->one();
	}

	if (empty($customer_db)) {
	    if ($bill_phone != '' && !empty($check_customer_phone) && empty($check_customer_phone['customerabbrivation'])) {
		$Customers_abbrivation = new CustomerAbbrivation();
		$Customers_abbrivation->channel_abb_id = $Prefix_customer_abb_id;
		$Customers_abbrivation->customer_id = $check_customer_phone['customer_ID'];
		$Customers_abbrivation->channel_accquired = $channel_name;
		$Customers_abbrivation->bill_street_1 = $bill_add_1;
		$Customers_abbrivation->bill_street_2 = $bill_add_2;
		$Customers_abbrivation->bill_city = $bill_city;
		$Customers_abbrivation->bill_state = $bill_state;
		$Customers_abbrivation->bill_zip = $bill_zip;
		$Customers_abbrivation->bill_country = $bill_country;
		$Customers_abbrivation->bill_country_iso = $bill_country_code;
		$Customers_abbrivation->ship_street_1 = $ship_add_1;
		$Customers_abbrivation->ship_street_2 = $ship_add_2;
		$Customers_abbrivation->ship_city = $ship_city;
		$Customers_abbrivation->ship_state = $ship_state;
		$Customers_abbrivation->ship_country = $ship_country;
		$Customers_abbrivation->ship_zip = $ship_zip;
		if ($store_connection_id != '') {
		    $Customers_abbrivation->mul_store_id = $store_connection_id;
		}
		if ($channel_connection_id != '') {
		    $Customers_abbrivation->mul_channel_id = $channel_connection_id;
		}
		$Customers_abbrivation->created_at = date('Y-m-d H:i:s', strtotime($customer_create_date));
		$Customers_abbrivation->updated_at = date('Y-m-d H:i:s', strtotime($customer_update_date));
		$Customers_abbrivation->save(false);
	    } elseif ($customer_email != '' && !empty($check_customer_email) && empty($check_customer_email['customerabbrivation'])) {
		$Customers_abbrivation = new CustomerAbbrivation();
		$Customers_abbrivation->channel_abb_id = $Prefix_customer_abb_id;
		$Customers_abbrivation->customer_id = $check_customer_email['customer_ID'];
		$Customers_abbrivation->channel_accquired = $channel_name;
		$Customers_abbrivation->bill_street_1 = $bill_add_1;
		$Customers_abbrivation->bill_street_2 = $bill_add_2;
		$Customers_abbrivation->bill_city = $bill_city;
		$Customers_abbrivation->bill_state = $bill_state;
		$Customers_abbrivation->bill_zip = $bill_zip;
		$Customers_abbrivation->bill_country = $bill_country;
		$Customers_abbrivation->bill_country_iso = $bill_country_code;
		$Customers_abbrivation->ship_street_1 = $ship_add_1;
		$Customers_abbrivation->ship_street_2 = $ship_add_2;
		$Customers_abbrivation->ship_city = $ship_city;
		$Customers_abbrivation->ship_state = $ship_state;
		$Customers_abbrivation->ship_country = $ship_country;
		$Customers_abbrivation->ship_zip = $ship_zip;
		if ($store_connection_id != '') {
		    $Customers_abbrivation->mul_store_id = $store_connection_id;
		}
		if ($channel_connection_id != '') {
		    $Customers_abbrivation->mul_channel_id = $channel_connection_id;
		}
		$Customers_abbrivation->created_at = date('Y-m-d H:i:s', strtotime($customer_create_date));
		$Customers_abbrivation->updated_at = date('Y-m-d H:i:s', strtotime($customer_update_date));
		$Customers_abbrivation->save(false);
	    } elseif ($first_name != '' && !empty($check_customer_name) && empty($check_customer_name['customerabbrivation'])) {
		$Customers_abbrivation = new CustomerAbbrivation();
		$Customers_abbrivation->channel_abb_id = $Prefix_customer_abb_id;
		$Customers_abbrivation->customer_id = $check_customer_name['customer_ID'];
		$Customers_abbrivation->channel_accquired = $channel_name;
		$Customers_abbrivation->bill_street_1 = $bill_add_1;
		$Customers_abbrivation->bill_street_2 = $bill_add_2;
		$Customers_abbrivation->bill_city = $bill_city;
		$Customers_abbrivation->bill_state = $bill_state;
		$Customers_abbrivation->bill_zip = $bill_zip;
		$Customers_abbrivation->bill_country = $bill_country;
		$Customers_abbrivation->bill_country_iso = $bill_country_code;
		$Customers_abbrivation->ship_street_1 = $ship_add_1;
		$Customers_abbrivation->ship_street_2 = $ship_add_2;
		$Customers_abbrivation->ship_city = $ship_city;
		$Customers_abbrivation->ship_state = $ship_state;
		$Customers_abbrivation->ship_country = $ship_country;
		$Customers_abbrivation->ship_zip = $ship_zip;
		if ($store_connection_id != '') {
		    $Customers_abbrivation->mul_store_id = $store_connection_id;
		}
		if ($channel_connection_id != '') {
		    $Customers_abbrivation->mul_channel_id = $channel_connection_id;
		}
		$Customers_abbrivation->created_at = date('Y-m-d H:i:s', strtotime($customer_create_date));
		$Customers_abbrivation->updated_at = date('Y-m-d H:i:s', strtotime($customer_update_date));
		$Customers_abbrivation->save(false);
	    } else {
		$Customers_model = new CustomerUser();
		$Customers_model->channel_abb_id = "";
		$Customers_model->first_name = $first_name;
		$Customers_model->last_name = $last_name;
		$Customers_model->email = $customer_email;
		$Customers_model->channel_acquired = $channel_name;
		$Customers_model->date_acquired = date('Y-m-d H:i:s', strtotime($customer_create_date));
		$Customers_model->street_1 = $bill_add_1;
		$Customers_model->street_2 = $bill_add_2;
		$Customers_model->city = $bill_city;
		$Customers_model->country = $bill_country;
		$Customers_model->country_iso = $bill_country_code;
		$Customers_model->state = $bill_state;
		$Customers_model->zip = $bill_zip;
		$Customers_model->phone_number = $bill_phone;
		$Customers_model->address_type = $address_type;
		$Customers_model->ship_street_1 = $ship_add_1;
		$Customers_model->ship_street_2 = $ship_add_2;
		$Customers_model->ship_city = $ship_city;
		$Customers_model->ship_state = $ship_state;
		$Customers_model->ship_zip = $ship_zip;
		$Customers_model->ship_country = $ship_country;
		$Customers_model->elliot_user_id = $user_id;
		$Customers_model->created_at = date('Y-m-d H:i:s', strtotime($customer_create_date));
		$Customers_model->updated_at = date('Y-m-d H:i:s', strtotime($customer_update_date));
		$Customers_model->save(false);
		if ($Customers_model->save(false)) {
		    $customer_abbrivation_save = new CustomerAbbrivation();
		    $customer_abbrivation_save->channel_abb_id = $Prefix_customer_abb_id;
		    $customer_abbrivation_save->customer_id = $Customers_model->customer_ID;
		    $customer_abbrivation_save->channel_accquired = $channel_name;
		    $customer_abbrivation_save->bill_street_1 = $bill_add_1;
		    $customer_abbrivation_save->bill_street_2 = $bill_add_2;
		    $customer_abbrivation_save->bill_city = $bill_city;
		    $customer_abbrivation_save->bill_state = $bill_state;
		    $customer_abbrivation_save->bill_zip = $bill_zip;
		    $customer_abbrivation_save->bill_country = $bill_country;
		    $customer_abbrivation_save->bill_country_iso = $bill_country_code;
		    $customer_abbrivation_save->ship_street_1 = $ship_add_1;
		    $customer_abbrivation_save->ship_street_2 = $ship_add_2;
		    $customer_abbrivation_save->ship_city = $ship_city;
		    $customer_abbrivation_save->ship_state = $ship_state;
		    $customer_abbrivation_save->ship_country = $ship_country;
		    $customer_abbrivation_save->ship_zip = $ship_zip;
		    if ($store_connection_id != '') {
			$customer_abbrivation_save->mul_store_id = $store_connection_id;
		    }
		    if ($channel_connection_id != '') {
			$customer_abbrivation_save->mul_channel_id = $channel_connection_id;
		    }
		    $customer_abbrivation_save->created_at = date('Y-m-d H:i:s', strtotime($customer_create_date));
		    $customer_abbrivation_save->updated_at = date('Y-m-d H:i:s', strtotime($customer_update_date));
		    $customer_abbrivation_save->save(false);
		}
	    }
	}
    }

    /**
     * Common function for Category Importing for all channels/stores
     */
    public static function categoryImportingCommon($category) {
	$category_id = $category['category_id'];
	$channel_abb_parent_id = $category['parent_id'];
	$cat_name = $category['name'];
	$channel_name = $category['channel_store_name'];
	$prefix_id = $category['channel_store_prefix'];
	$mul_store_id = $category['mul_store_id'];
	$mul_channel_id = $category['mul_channel_id'];
	$user_id = $category['elliot_user_id'];
	$created_at = $category['created_at'];
	$updated_at = $category['updated_at'];

	$channel_abb_id = $prefix_id . $category_id;
	$checkCategoryName = Categories::find()->where(['category_name' => $cat_name])->one();
	if (empty($checkCategoryName)) {
	    $categoryModel = new Categories();
	    $categoryModel->channel_abb_id = '';
	    $categoryModel->category_name = $cat_name;
	    $categoryModel->parent_category_ID = $channel_abb_parent_id;
	    //Save Elliot User id
	    $categoryModel->elliot_user_id = $user_id;
	    $created_date = $categoryModel->created_at = date('Y-m-d h:i:s', strtotime($created_at));
	    $categoryModel->updated_at = date('Y-m-d h:i:s', strtotime($updated_at));
	    $categoryModel->save();

	    $checkCategoryAbbrivation = CategoryAbbrivation::find()->where(['channel_abb_id' => $channel_abb_id])->one();
	    if (empty($checkCategoryAbbrivation)) {
		$CategoryAbbrivation = new CategoryAbbrivation();
		$CategoryAbbrivation->channel_abb_id = $channel_abb_id;
		$CategoryAbbrivation->category_ID = $categoryModel->category_ID;
		$CategoryAbbrivation->channel_accquired = $channel_name;
		if ($mul_store_id != '') {
		    $CategoryAbbrivation->mul_store_id = $mul_store_id;
		}
		if ($mul_channel_id != '') {
		    $CategoryAbbrivation->mul_channel_id = $mul_channel_id;
		}
		$CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
		$CategoryAbbrivation->save(false);
	    }
	} else {
	    $checkCategoryAbbrivation = CategoryAbbrivation::find()->where(['channel_abb_id' => $channel_abb_id])->one();
	    if (empty($checkCategoryAbbrivation)) {
		$CategoryAbbrivation = new CategoryAbbrivation();
		$CategoryAbbrivation->channel_abb_id = $channel_abb_id;
		$CategoryAbbrivation->category_ID = $checkCategoryName->category_ID;
		if ($mul_store_id != '') {
		    $CategoryAbbrivation->mul_store_id = $mul_store_id;
		}
		if ($mul_channel_id != '') {
		    $CategoryAbbrivation->mul_channel_id = $mul_channel_id;
		}
		$CategoryAbbrivation->channel_accquired = $channel_name;
		$CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
		$CategoryAbbrivation->save(false);
	    }
	}
    }

    /**
     * Common function for Product Importing for all channels/stores
     */
    public static function productImportingCommon($product) {
	$product_abb_id = $product['product_id'];
	$product_name = $product['name'];
	$product_sku = ($product['sku'] == '') ? '' : $product['sku'];
	$product_description = $product['description'];
	$product_url_path = $product['product_url_path'];
	$product_weight = $product['weight'];
	$product_status = $product['status'];
	$price = $product['price'];
	$sale_price = $product['sale_price'];

	$qty = $product['qty'];
	if ($qty < 0) {
	    $qty = 0;
	}
	$stock_status = $product['stock_status'];
	$product_created_at = $product['created_at'];
	$product_updated_at = $product['updated_at'];
	$websites = $product['websites'];
	$product_brand = $product['brand'];
	$low_stock_notification = $product['low_stock_notification'];
	if (isset($product['conversion_rate'])) {
	    $conversion_rate = $product['conversion_rate'];
	} else {
	    $conversion_rate = 1;
	}



	$product_categories_ids = $product['categories'];
	$product_image = $product['images'];

	$channel_name = $product['channel_store_name'];
	$prefix_id = $product['channel_store_prefix'];
	$user_id = $product['elliot_user_id'];
	$store_id = $product['store_id'];
	$channel_id = $product['channel_id'];
	$country_code = $product['country_code'];
	$product_price = $price;
	$product_sale_price = $sale_price;

	if (array_key_exists('currency_code', $product)) {
	    $currency = $product['currency_code'];
	    if ($product['currency_code'] != '') {
		//$conversion_rate = self::getCurrencyConversionRate($currency, 'USD');
		$product_price = number_format((float) $conversion_rate * $price, 2, '.', '');
		$product_sale_price = number_format((float) $conversion_rate * $sale_price, 2, '.', '');
	    }
	}

	$barcode = empty($product['upc']) ? '' : $product['upc'];
	$ean = empty($product['ean']) ? '' : $product['ean'];
	$jan = empty($product['jan']) ? '' : $product['jan'];
	$isban = empty($product['isban']) ? '' : $product['isban'];
	$mpn = empty($product['mpn']) ? '' : $product['mpn'];

	$store_connection_id = $product['mul_store_id'];
	$channel_connection_id = $product['mul_channel_id'];
	$wechat_id = "";
	if (array_key_exists('wechat_id', $product)) {
	    $wechat_id = $product['wechat_id'];
	}

	$channel_abb_id = $prefix_id . $product_abb_id;
	$last_pro_id = '';
	$checkProductModel = Products::find()->where(['sku' => $product_sku])->andWhere(['<>', 'sku', ''])->one();
	if (empty($checkProductModel)) {
	    $productModel = new Products ();
	    $productModel->channel_abb_id = '';
	    $productModel->product_name = $product_name;
	    $productModel->SKU = $product_sku;
	    $productModel->product_url = $product_url_path;
	    $productModel->UPC = $barcode;
	    $productModel->EAN = $ean;
	    $productModel->Jan = $jan;
	    $productModel->ISBN = $isban;
	    $productModel->MPN = $mpn;
	    $productModel->description = $product_description;
	    $productModel->adult = 'no';
	    $productModel->age_group = NULL;
	    $productModel->gender = 'Unisex';
	    $productModel->brand = '';
	    $productModel->weight = $product_weight;
	    $productModel->product_status = 'active';
	    $productModel->stock_quantity = $qty;
	    if ($stock_status <= 0) {
		$productModel->availability = 'Out of Stock';
		$productModel->stock_level = 'Out of Stock';
	    } else {
		$productModel->availability = 'In Stock';
		$productModel->stock_level = 'In Stock';
	    }
	    if ($product_status == 1) {
		$productModel->stock_status = 'Visible';
	    } else {
		$productModel->stock_status = 'Hidden';
	    }
	    if ($low_stock_notification == '') {
		$productModel->low_stock_notification = 5;
	    } else {
		$productModel->low_stock_notification = $low_stock_notification;
	    }
	    $productModel->price = $product_price;
	    $productModel->sales_price = $product_sale_price;
	    $productModel->country_code = $country_code;
	    $productModel->created_at = date('Y-m-d h:i:s', strtotime($product_created_at));
	    $productModel->updated_at = date('Y-m-d h:i:s', strtotime($product_updated_at));
	    $productModel->elliot_user_id = $user_id;
	    if (count($websites) > 0) {
		$productModel->magento_store_id = implode(',', $websites);
	    }
	    $productModel->save(false);
	    $last_pro_id = $productModel->id;

	    $product_abberivation = new ProductAbbrivation();
	    $product_abberivation->channel_abb_id = $channel_abb_id;
	    $product_abberivation->mul_store_id = $store_connection_id;
	    $product_abberivation->mul_channel_id = $channel_connection_id;
	    $product_abberivation->product_id = $last_pro_id;
	    $product_abberivation->price = $product_price;
	    $product_abberivation->salePrice = $product_sale_price;
	    $product_abberivation->schedules_sales_date = date('Y-m-d', time());
	    $product_abberivation->stock_quantity = $qty;
	    if ($qty > 0) {
		$product_abberivation->stock_level = 'In Stock';
	    } else {
		$product_abberivation->stock_level = 'Out of Stock';
	    }
	    if ($product_status == 1) {
		$product_abberivation->stock_status = 'Visible';
	    } else {
		$product_abberivation->stock_status = 'Hidden';
	    }
	    $product_abberivation->channel_accquired = $channel_name;
	    $product_abberivation->created_at = date('Y-m-d H:i:s', time());
	    $product_abberivation->updated_at = date('Y-m-d H:i:s', time());
	    $product_abberivation->save(false);

	    //$store_data = Stores::find()->where(['store_name' => $channel_name])->one();
	    //$_store_id = $store_data->store_id;
	    $productChannelModel = new ProductChannel();
	    if ($store_connection_id != '') {
		$productChannelModel->store_id = $store_connection_id;
	    }
	    if ($channel_id != '') {
		$productChannelModel->channel_id = $channel_id;
	    }
	    $productChannelModel->product_id = $last_pro_id;
	    $productChannelModel->elliot_user_id = $user_id;
	    $productChannelModel->status = 'yes';
	    $productChannelModel->created_at = date('Y-m-d h:i:s', time());
	    $productChannelModel->save(false);

	    foreach ($product_image as $_image) {
		$product_image_url = $_image['image_url'];
		$label = $_image['label'];
		$image_position = $_image['position'];
		$base_image = $_image['base_img'];

		$productImageModel = new ProductImages();
		$productImageModel->elliot_user_id = $user_id;
		$productImageModel->product_ID = $last_pro_id;
		$productImageModel->label = $label;
		$productImageModel->link = $product_image_url;
		$productImageModel->priority = $image_position;
		$productImageModel->image_status = 1;
		if ($base_image == $product_image_url) {
		    $productImageModel->default_image = 'Yes';
		}
		$productImageModel->created_at = date('Y-m-d h:i:s', strtotime($product_created_at));
		$productImageModel->updated_at = date('Y-m-d h:i:s', strtotime($product_updated_at));
		$productImageModel->save(false);
	    }

	    foreach ($product_categories_ids as $_cat) {
		if (empty($store_connection_id)) {
		    $category_id = CategoryAbbrivation::find()->where(['channel_abb_id' => $prefix_id . $_cat])->one();
		} else {
		    $category_id = CategoryAbbrivation::find()->where(['channel_abb_id' => $prefix_id . $_cat, 'mul_store_id' => $store_connection_id])->with('category')->one();
		}

		if (!empty($category_id)) {
		    $category_model = new ProductCategories();
		    $category_model->elliot_user_id = $user_id;
		    $category_model->category_ID = $category_id->category_ID;
		    $category_model->product_ID = $last_pro_id;
		    $category_model->created_at = date('Y-m-d h:i:s', strtotime($product_created_at));
		    $category_model->save();
		}
	    }
	} else {
	    if (empty($store_connection_id)) {
		$check_abbrivation = ProductAbbrivation::find()->where(['channel_abb_id' => $channel_abb_id])->one();
	    } else {
		$check_abbrivation = ProductAbbrivation::find()->Where(['channel_abb_id' => $channel_abb_id, 'mul_store_id' => $store_connection_id])->one();
	    }
	    if (empty($check_abbrivation)) {
		$last_pro_id = $checkProductModel->id;
		$product_abberivation = new ProductAbbrivation();
		$product_abberivation->channel_abb_id = $channel_abb_id;
		$product_abberivation->mul_store_id = $store_connection_id;
		$product_abberivation->product_id = $last_pro_id;
		$product_abberivation->price = $product_price;
		$product_abberivation->salePrice = $product_sale_price;
		$product_abberivation->schedules_sales_date = date('Y-m-d', time());
		$product_abberivation->stock_quantity = $qty;
		if ($qty > 0) {
		    $product_abberivation->stock_level = 'In Stock';
		} else {
		    $product_abberivation->stock_level = 'Out of Stock';
		}
		$product_abberivation->channel_accquired = $channel_name;
		$product_abberivation->created_at = date('Y-m-d H:i:s', time());
		$product_abberivation->updated_at = date('Y-m-d H:i:s', time());
		$product_abberivation->save(false);

		$productChannelModel = new ProductChannel();
		if ($store_id != '') {
		    $productChannelModel->store_id = $store_connection_id;
		}
		if ($channel_id != '') {
		    $productChannelModel->channel_id = $channel_id;
		}
		$productChannelModel->product_id = $checkProductModel->id;
		$productChannelModel->elliot_user_id = $user_id;
		$productChannelModel->status = 'yes';
		$productChannelModel->created_at = date('Y-m-d h:i:s', time());
		$productChannelModel->save(false);
	    } else {
		self::producUpdateCommon($product);
	    }
	}
	return $last_pro_id;
    }

    /**
     * Common function for Order Importing for all channels/stores
     */
    public static function orderImportingCommon($order_data) {

	$order_id = $order_data['order_id'];
	$store_connection_id = $order_data['mul_store_id'];
	$channel_connection_id = $order_data['mul_channel_id'];
	$order_status = $order_data['status'];
	$order_store_id = $order_data['magento_store_id'];
	$order_total_amount = $order_data['order_grand_total'];
	$customer_id = $order_data['customer_id'];
	$customer_email = $order_data['customer_email'];
	$order_shipping = $order_data['order_shipping_amount'];
	$order_tax = $order_data['order_tax_amount'];
	$order_product_qty = $order_data['total_qty_ordered'];
	$order_created_at = $order_data['created_at'];
	$order_updated_at = $order_data['updated_at'];
	$payment_method = $order_data['payment_method'];
	$order_refund_amount = $order_data['refund_amount'];
	$order_discount_amount = $order_data['discount_amount'];
	if (isset($order_data['conversion_rate'])) {
	    $conversion_rate = $order_data['conversion_rate'];
	} else {
	    $conversion_rate = 1;
	}



	$order_import_create = isset($order_data['import_or_create']) ? strtolower($order_data['import_or_create']) : '';

	$order_items = $order_data['items'];

	$channel_name = $order_data['channel_store_name'];
	$prefix_id = $order_data['channel_store_prefix'];
	$user_id = $order_data['elliot_user_id'];
	$store_id = $order_data['store_id'];
	$channel_id = $order_data['channel_id'];
	$Prefix_customer_abb_id = $prefix_id . $customer_id;

	$order_total = $order_total_amount;
	$order_tax_amount = $order_tax;
	$order_shipping_amount = $order_shipping;
	$refund_amount = $order_refund_amount;
	$discount_amount = $order_discount_amount;
	if (array_key_exists('currency_code', $order_data)) {
	    $currency_code = $order_data['currency_code'];
	    // $conversion_rate = self::getCurrencyConversionRate($currency_code, 'USD');
	    $order_total = number_format((float) $conversion_rate * $order_total_amount, 2, '.', '');
	    $order_tax_amount = number_format((float) $conversion_rate * $order_tax, 2, '.', '');
	    $order_shipping_amount = number_format((float) $conversion_rate * $order_shipping, 2, '.', '');
	    $refund_amount = number_format((float) $conversion_rate * $order_refund_amount, 2, '.', '');
	    $discount_amount = number_format((float) $conversion_rate * $order_discount_amount, 2, '.', '');
	}


	//Order Shipping address
	$order_shipping_add = $order_data['shipping_address'];
	$ship_state = $order_shipping_add['state'];
	$ship_zip = $order_shipping_add['postcode'];
	$ship_add_1 = $order_shipping_add['street_1'];
	$ship_add_2 = $order_shipping_add['street_2'];
	$ship_city = $order_shipping_add['city'];
	$ship_customer_email = $order_shipping_add['email'];
	$ship_phone = $order_shipping_add['telephone'];
	$ship_country_id = $order_shipping_add['country_id'];
	$ship_country = $order_shipping_add['country'];
	$ship_firstname = $order_shipping_add['firstname'];
	$ship_lastname = $order_shipping_add['lastname'];
	$shipping_address = $ship_add_1 . "," . $ship_add_2 . "," . $ship_city . "," . $ship_state . "," . $ship_zip . "," . $ship_country;

	//Order Billing address
	$order_billing_add = $order_data['billing_address'];
	$bill_state = $order_billing_add['state'];
	$bill_zip = $order_billing_add['postcode'];
	$bill_add_1 = $order_billing_add['street_1'];
	$bill_add_2 = $order_billing_add['street_2'];
	$bill_city = $order_billing_add['city'];
	$bill_customer_email = $order_billing_add['email'];
	$bill_phone = $order_billing_add['telephone'];
	$bill_country_id = $order_billing_add['country_id'];
	$bill_country = $order_billing_add['country'];
	$bill_firstname = $order_billing_add['firstname'];
	$bill_lastname = $order_billing_add['lastname'];
	$billing_address = $bill_add_1 . "," . $bill_add_2 . "," . $bill_city . "," . $bill_state . "," . $bill_zip . "," . $bill_country;

	$order_prefix_id = $prefix_id . $order_id;


	if (empty($store_connection_id)) {
	    $check_order = Orders::find()->Where(['channel_abb_id' => $order_prefix_id])->one();
	} else {
	    $check_order = Orders::find()->Where(['channel_abb_id' => $order_prefix_id, 'mul_store_id' => $store_connection_id])->one();
	}


	if (empty($check_order)) {
	    /* Check Customer exists or not */
	    if ($customer_id == '' || $customer_id == 0) {
		if (empty($store_connection_id)) {
		    $check_customer_phone = CustomerUser::find()->Where(['phone_number' => $bill_phone])
				    ->with(['customerabbrivation' => function($query) use ($channel_name) {
					    $query->andWhere(['channel_accquired' => $channel_name]);
					}])->asArray()->one();
		    $check_customer_email = CustomerUser::find()->Where(['email' => $customer_email])
				    ->with(['customerabbrivation' => function($query) use ($channel_name) {
					    $query->andWhere(['channel_accquired' => $channel_name]);
					}])->asArray()->one();
		    $check_customer_name = CustomerUser::find()->Where(['first_name' => $bill_firstname, 'last_name' => $bill_lastname])
				    ->with(['customerabbrivation' => function($query) use ($channel_name) {
					    $query->andWhere(['channel_accquired' => $channel_name]);
					}])->asArray()->one();
		} else {
		    $check_customer_phone = CustomerUser::find()->Where(['phone_number' => $bill_phone])
				    ->with(['customerabbrivation' => function($query) use ($channel_name, $store_connection_id) {
					    $query->andWhere(['channel_accquired' => $channel_name]);
					    $query->andWhere(['mul_store_id' => $store_connection_id]);
					}])->asArray()->one();

		    $check_customer_email = CustomerUser::find()->Where(['email' => $customer_email])
				    ->with(['customerabbrivation' => function($query) use ($channel_name, $store_connection_id) {
					    $query->andWhere(['channel_accquired' => $channel_name]);
					    $query->andWhere(['mul_store_id' => $store_connection_id]);
					}])->asArray()->one();

		    $check_customer_name = CustomerUser::find()->Where(['first_name' => $bill_firstname, 'last_name' => $bill_lastname])
				    ->with(['customerabbrivation' => function($query) use ($channel_name, $store_connection_id) {
					    $query->andWhere(['channel_accquired' => $channel_name]);
					    $query->andWhere(['mul_store_id' => $store_connection_id]);
					}])->asArray()->one();
		}
		if ($bill_phone != '' && !empty($check_customer_phone) && empty($check_customer_phone['customerabbrivation'])) {
		    $Customers_abbrivation = new CustomerAbbrivation();
		    $Customers_abbrivation->channel_abb_id = $Prefix_customer_abb_id;
		    $Customers_abbrivation->customer_id = $check_customer_phone['customer_ID'];
		    $Customers_abbrivation->channel_accquired = $channel_name;
		    $Customers_abbrivation->bill_street_1 = $bill_add_1;
		    $Customers_abbrivation->bill_street_2 = $bill_add_2;
		    $Customers_abbrivation->bill_city = $bill_city;
		    $Customers_abbrivation->bill_state = $bill_state;
		    $Customers_abbrivation->bill_zip = $bill_zip;
		    $Customers_abbrivation->bill_country = $bill_country;
		    $Customers_abbrivation->bill_country_iso = $bill_country_id;
		    $Customers_abbrivation->ship_street_1 = $ship_add_1;
		    $Customers_abbrivation->ship_street_2 = $ship_add_2;
		    $Customers_abbrivation->ship_city = $ship_city;
		    $Customers_abbrivation->ship_state = $ship_state;
		    $Customers_abbrivation->ship_country = $ship_country;
		    $Customers_abbrivation->ship_zip = $ship_zip;
		    if ($store_connection_id != '') {
			$Customers_abbrivation->mul_store_id = $store_connection_id;
		    }
		    if ($channel_connection_id != '') {
			$Customers_abbrivation->mul_channel_id = $channel_connection_id;
		    }
		    $Customers_abbrivation->created_at = date('Y-m-d H:i:s', strtotime($order_created_at));
		    $Customers_abbrivation->updated_at = date('Y-m-d H:i:s', strtotime($order_updated_at));
		    $Customers_abbrivation->save(false);
		    $elliot_customer_id = $check_customer_phone['customer_ID'];
		} elseif ($customer_email != '' && !empty($check_customer_email) && empty($check_customer_email['customerabbrivation'])) {
		    $Customers_abbrivation = new CustomerAbbrivation();
		    $Customers_abbrivation->channel_abb_id = $Prefix_customer_abb_id;
		    $Customers_abbrivation->customer_id = $check_customer_email['customer_ID'];
		    $Customers_abbrivation->channel_accquired = $channel_name;
		    $Customers_abbrivation->bill_street_1 = $bill_add_1;
		    $Customers_abbrivation->bill_street_2 = $bill_add_2;
		    $Customers_abbrivation->bill_city = $bill_city;
		    $Customers_abbrivation->bill_state = $bill_state;
		    $Customers_abbrivation->bill_zip = $bill_zip;
		    $Customers_abbrivation->bill_country = $bill_country;
		    $Customers_abbrivation->bill_country_iso = $bill_country_id;
		    $Customers_abbrivation->ship_street_1 = $ship_add_1;
		    $Customers_abbrivation->ship_street_2 = $ship_add_2;
		    $Customers_abbrivation->ship_city = $ship_city;
		    $Customers_abbrivation->ship_state = $ship_state;
		    $Customers_abbrivation->ship_country = $ship_country;
		    $Customers_abbrivation->ship_zip = $ship_zip;
		    if ($store_connection_id != '') {
			$Customers_abbrivation->mul_store_id = $store_connection_id;
		    }
		    if ($channel_connection_id != '') {
			$Customers_abbrivation->mul_channel_id = $channel_connection_id;
		    }
		    $Customers_abbrivation->created_at = date('Y-m-d H:i:s', strtotime($order_created_at));
		    $Customers_abbrivation->updated_at = date('Y-m-d H:i:s', strtotime($order_updated_at));
		    $Customers_abbrivation->save(false);
		    $elliot_customer_id = $check_customer_email['customer_ID'];
		} elseif ($bill_firstname != '' && !empty($check_customer_name) && empty($check_customer_name['customerabbrivation'])) {
		    $Customers_abbrivation = new CustomerAbbrivation();
		    $Customers_abbrivation->channel_abb_id = $Prefix_customer_abb_id;
		    $Customers_abbrivation->customer_id = $check_customer_name['customer_ID'];
		    $Customers_abbrivation->channel_accquired = $channel_name;
		    $Customers_abbrivation->bill_street_1 = $bill_add_1;
		    $Customers_abbrivation->bill_street_2 = $bill_add_2;
		    $Customers_abbrivation->bill_city = $bill_city;
		    $Customers_abbrivation->bill_state = $bill_state;
		    $Customers_abbrivation->bill_zip = $bill_zip;
		    $Customers_abbrivation->bill_country = $bill_country;
		    $Customers_abbrivation->bill_country_iso = $bill_country_id;
		    $Customers_abbrivation->ship_street_1 = $ship_add_1;
		    $Customers_abbrivation->ship_street_2 = $ship_add_2;
		    $Customers_abbrivation->ship_city = $ship_city;
		    $Customers_abbrivation->ship_state = $ship_state;
		    $Customers_abbrivation->ship_country = $ship_country;
		    $Customers_abbrivation->ship_zip = $ship_zip;
		    if ($store_connection_id != '') {
			$Customers_abbrivation->mul_store_id = $store_connection_id;
		    }
		    if ($channel_connection_id != '') {
			$Customers_abbrivation->mul_channel_id = $channel_connection_id;
		    }
		    $Customers_abbrivation->created_at = date('Y-m-d H:i:s', strtotime($order_created_at));
		    $Customers_abbrivation->updated_at = date('Y-m-d H:i:s', strtotime($order_updated_at));
		    $Customers_abbrivation->save(false);
		    $elliot_customer_id = $check_customer_name['customer_ID'];
		} elseif ($bill_phone != '' && !empty($check_customer_phone) && !empty($check_customer_phone['customerabbrivation'])) {
		    $elliot_customer_id = $check_customer_phone['customer_ID'];
		} elseif ($customer_email != '' && !empty($check_customer_email) && !empty($check_customer_email['customerabbrivation'])) {
		    $elliot_customer_id = $check_customer_email['customer_ID'];
		} elseif ($bill_firstname != '' && !empty($check_customer_name) && !empty($check_customer_name['customerabbrivation'])) {
		    $elliot_customer_id = $check_customer_name['customer_ID'];
		} else {
		    $Customers_model = new CustomerUser();
		    $Customers_model->channel_abb_id = "";
		    $Customers_model->first_name = $bill_firstname;
		    $Customers_model->last_name = $bill_lastname;
		    $Customers_model->email = $customer_email;
		    $Customers_model->channel_acquired = $channel_name;
		    $Customers_model->date_acquired = date('Y-m-d H:i:s', strtotime($order_created_at));
		    $Customers_model->street_1 = $bill_add_1;
		    $Customers_model->street_2 = $bill_add_2;
		    $Customers_model->city = $bill_city;
		    $Customers_model->country = $bill_country;
		    $Customers_model->country_iso = $bill_country_id;
		    $Customers_model->state = $bill_state;
		    $Customers_model->zip = $bill_zip;
		    $Customers_model->phone_number = $bill_phone;
		    $Customers_model->address_type = 'Default';
		    $Customers_model->ship_street_1 = $ship_add_1;
		    $Customers_model->ship_street_2 = $ship_add_2;
		    $Customers_model->ship_city = $ship_city;
		    $Customers_model->ship_state = $ship_state;
		    $Customers_model->ship_zip = $ship_zip;
		    $Customers_model->ship_country = $ship_country;
		    $Customers_model->elliot_user_id = $user_id;
		    $Customers_model->created_at = date('Y-m-d H:i:s', strtotime($order_created_at));
		    $Customers_model->updated_at = date('Y-m-d H:i:s', strtotime($order_updated_at));
		    $Customers_model->save(false);
		    $elliot_customer_id = $Customers_model->customer_ID;
		    if ($Customers_model->save(false)) {
			$customer_abbrivation_save = new CustomerAbbrivation();
			$customer_abbrivation_save->channel_abb_id = $Prefix_customer_abb_id;
			$customer_abbrivation_save->customer_id = $Customers_model->customer_ID;
			$customer_abbrivation_save->channel_accquired = $channel_name;
			$customer_abbrivation_save->bill_street_1 = $bill_add_1;
			$customer_abbrivation_save->bill_street_2 = $bill_add_2;
			$customer_abbrivation_save->bill_city = $bill_city;
			$customer_abbrivation_save->bill_state = $bill_state;
			$customer_abbrivation_save->bill_zip = $bill_zip;
			$customer_abbrivation_save->bill_country = $bill_country;
			$customer_abbrivation_save->bill_country_iso = $bill_country_id;
			$customer_abbrivation_save->ship_street_1 = $ship_add_1;
			$customer_abbrivation_save->ship_street_2 = $ship_add_2;
			$customer_abbrivation_save->ship_city = $ship_city;
			$customer_abbrivation_save->ship_state = $ship_state;
			$customer_abbrivation_save->ship_country = $ship_country;
			$customer_abbrivation_save->ship_zip = $ship_zip;
			if ($store_connection_id != '') {
			    $customer_abbrivation_save->mul_store_id = $store_connection_id;
			}
			if ($channel_connection_id != '') {
			    $customer_abbrivation_save->mul_channel_id = $channel_connection_id;
			}
			$customer_abbrivation_save->created_at = date('Y-m-d H:i:s', strtotime($order_created_at));
			$customer_abbrivation_save->updated_at = date('Y-m-d H:i:s', strtotime($order_updated_at));
			$customer_abbrivation_save->save(false);
		    }
		}
	    } else {
		if (!empty($store_connection_id)) {
		    $customer_db1 = CustomerAbbrivation::find()->Where(['channel_abb_id' => $prefix_id . $customer_id, 'mul_store_id' => $store_connection_id])->one();
		} else {
		    $customer_db1 = CustomerAbbrivation::find()->Where(['channel_abb_id' => $prefix_id . $customer_id])->one();
		}
		if (empty($customer_db1)) {

		    if (empty($store_connection_id)) {
			$customer_id_email_check = CustomerUser::find()->Where(['email' => $customer_email])
					->with(['customerabbrivation' => function($query) use ($prefix_id, $customer_id) {
						$query->andWhere(['channel_abb_id' => $prefix_id . $customer_id]);
					    }
					])->asArray()->one();
		    } else {
			$customer_id_email_check = CustomerUser::find()->Where(['email' => $customer_email])
					->with(['customerabbrivation' => function($query) use ($prefix_id, $customer_id, $store_connection_id) {
						$query->andWhere(['channel_abb_id' => $prefix_id . $customer_id, 'mul_store_id' => $store_connection_id]);
					    }
					])->asArray()->one();
		    }

		    if (!empty($customer_id_email_check) && !empty($customer_id_email_checks['customerabbrivation'])) {
			$elliot_customer_id = $customer_id_email_check['customer_ID'];
		    } else {
			$Customers_model = new CustomerUser();
			$Customers_model->channel_abb_id = "";
			$Customers_model->first_name = $bill_firstname;
			$Customers_model->last_name = $bill_lastname;
			$Customers_model->email = $customer_email;
			$Customers_model->channel_acquired = $channel_name;
			$Customers_model->date_acquired = date('Y-m-d H:i:s', strtotime($order_created_at));
			$Customers_model->street_1 = $bill_add_1;
			$Customers_model->street_2 = $bill_add_2;
			$Customers_model->city = $bill_city;
			$Customers_model->country = $bill_country;
			$Customers_model->country_iso = $bill_country_id;
			$Customers_model->state = $bill_state;
			$Customers_model->zip = $bill_zip;
			$Customers_model->phone_number = $bill_phone;
			$Customers_model->address_type = 'Default';
			$Customers_model->ship_street_1 = $ship_add_1;
			$Customers_model->ship_street_2 = $ship_add_2;
			$Customers_model->ship_city = $ship_city;
			$Customers_model->ship_state = $ship_state;
			$Customers_model->ship_zip = $ship_zip;
			$Customers_model->ship_country = $ship_country;
			$Customers_model->elliot_user_id = $user_id;
			$Customers_model->created_at = date('Y-m-d H:i:s', strtotime($order_created_at));
			$Customers_model->updated_at = date('Y-m-d H:i:s', strtotime($order_updated_at));
			if ($Customers_model->save(false)) {
			    $Customers_abbrivation = new CustomerAbbrivation();
			    $Customers_abbrivation->channel_abb_id = $prefix_id . '0';
			    $Customers_abbrivation->mul_store_id = $store_connection_id;
			    $Customers_abbrivation->customer_id = $Customers_model->customer_ID;
			    $Customers_abbrivation->channel_accquired = $channel_name;
			    $Customers_abbrivation->bill_street_1 = $bill_add_1;
			    $Customers_abbrivation->bill_street_2 = $bill_add_2;
			    $Customers_abbrivation->bill_city = $bill_city;
			    $Customers_abbrivation->bill_state = $bill_state;
			    $Customers_abbrivation->bill_zip = $bill_zip;
			    $Customers_abbrivation->bill_country = $bill_country;
			    $Customers_abbrivation->bill_country_iso = $bill_country_id;
			    $Customers_abbrivation->ship_street_1 = $ship_add_1;
			    $Customers_abbrivation->ship_street_2 = $ship_add_2;
			    $Customers_abbrivation->ship_city = $ship_city;
			    $Customers_abbrivation->ship_state = $ship_state;
			    $Customers_abbrivation->ship_country = $ship_country;
			    $Customers_abbrivation->ship_zip = $ship_zip;
			    $Customers_abbrivation->created_at = date('Y-m-d H:i:s', strtotime($order_created_at));
			    $Customers_abbrivation->updated_at = date('Y-m-d H:i:s', strtotime($order_updated_at));
			    $Customers_abbrivation->save(false);
			}
			$elliot_customer_id = $Customers_model->customer_ID;
		    }
		} else {
		    $elliot_customer_id = $customer_db1->customer_id;
		}
	    }
	    $order_model = new Orders();
	    $order_model->elliot_user_id = $user_id;
	    $order_model->channel_abb_id = $order_prefix_id;
	    $order_model->mul_store_id = $store_connection_id;
	    $order_model->customer_id = $elliot_customer_id;
	    $order_model->order_status = $order_status;
	    $order_model->product_qauntity = $order_product_qty;
	    $order_model->shipping_address = $shipping_address;
	    $order_model->ship_street_1 = $ship_add_1;
	    $order_model->ship_street_2 = $ship_add_2;
	    $order_model->ship_city = $ship_city;
	    $order_model->ship_state = $ship_state;
	    $order_model->ship_zip = $ship_zip;
	    $order_model->ship_country = $ship_country;
	    $order_model->billing_address = $billing_address;
	    $order_model->bill_street_1 = $bill_add_1;
	    $order_model->bill_street_2 = $bill_add_2;
	    $order_model->bill_city = $bill_city;
	    $order_model->bill_state = $bill_state;
	    $order_model->bill_zip = $bill_zip;
	    $order_model->bill_country = $bill_country;
	    $order_model->base_shipping_cost = $order_shipping_amount;
	    $order_model->shipping_cost_tax = $order_tax_amount;
	    $order_model->payment_method = $payment_method;
	    $order_model->payment_status = $order_status;
	    $order_model->refunded_amount = $refund_amount;
	    $order_model->discount_amount = $discount_amount;
	    $order_model->total_amount = $order_total;
	    $order_model->magento_store_id = $order_store_id;
	    $order_model->order_date = $order_created_at;
	    $order_model->channel_accquired = $channel_name;
	    $order_model->created_at = date('Y-m-d h:i:s', strtotime($order_created_at));
	    $order_model->updated_at = date('Y-m-d h:i:s', strtotime($order_updated_at));
	    $order_model->save(false);
	    $last_order_id = $order_model->order_ID;

	    $order_channels_model = new OrderChannel();
	    $order_channels_model->elliot_user_id = $user_id;
	    $order_channels_model->order_id = $last_order_id;
	    if ($store_connection_id != '') {
		$order_channels_model->store_id = $store_connection_id;
	    }
	    if ($channel_id != '') {
		$order_channels_model->channel_id = $channel_id;
	    }
	    $order_channels_model->created_at = date('Y-m-d H:i:s', strtotime($order_created_at));
	    $order_channels_model->save(FALSE);
	    foreach ($order_items as $_items) {
		$title = $_items['name'];
		$quantity = ($_items['qty_ordered'] == '') ? 0 : $_items['qty_ordered'];
		$product_price = ($_items['price'] == '') ? 0 : $_items['price'];
		$price = $product_price;
		$sku = ($_items['sku'] == '') ? '' : $_items['sku'];
		$product_id = $_items['product_id'];
		$order_total_product_price = $price * $quantity;
		$product_weight = $_items['weight'];
		$total_product_price = $order_total_product_price;
		if (array_key_exists('currency_code', $order_data)) {
		    $currency_code = $order_data['currency_code'];
		    //$conversion_rate = self::getCurrencyConversionRate($currency_code, 'USD');
		    $price = number_format((float) $conversion_rate * $product_price, 2, '.', '');
		    $total_product_price = number_format((float) $conversion_rate * $order_total_product_price, 2, '.', '');
		}

		if (!empty($store_connection_id)) {
		    $product_sku_check = Products::find()->Where(['SKU' => $sku])
				    ->with(['productAbbrivation' => function($query) use ($prefix_id, $product_id, $store_connection_id) {
					    $query->andWhere(['channel_abb_id' => $prefix_id . $product_id]);
					    $query->andWhere(['mul_store_id' => $store_connection_id]);
					}])->asArray()->one();
		} else {
		    $product_sku_check = Products::find()->Where(['SKU' => $sku])
				    ->with(['productAbbrivation' => function($query) use ($prefix_id, $product_id) {
					    $query->andWhere(['channel_abb_id' => $prefix_id . $product_id]);
					}])->asArray()->one();
		}

		if (!empty($product_sku_check) && !empty($product_sku_check['productAbbrivation'])) {
		    $last_pro_id = $product_sku_check['id'];

		    //when order create by a hooks
		    if ($order_import_create == 'create') {
			if (empty($store_connection_id)) {
			    $qty_update = ProductAbbrivation::find()->Where(['product_id' => $last_pro_id, 'channel_accquired' => $channel_name])->one();
			} else {
			    $qty_update = ProductAbbrivation::find()->Where(['product_id' => $last_pro_id, 'channel_accquired' => $channel_name, 'mul_store_id' => $store_connection_id])->one();
			}

			$pre_qry = $qty_update->stock_quantity;
			if ($pre_qry > 0) {
			    $available_qty = $pre_qry - $quantity;
			    if ($available_qty > 0) {
				$qty_update->stock_quantity = $available_qty;
				$qty_update->save();
			    } else {
				$qty_update->stock_quantity = 0;
				$qty_update->save();
			    }
			}
		    }
		} elseif (!empty($product_sku_check) && empty($product_sku_check['productAbbrivation'])) {
		    $last_pro_id = $product_sku_check['id'];
		    $product_abberivation = new ProductAbbrivation();
		    $product_abberivation->channel_abb_id = $prefix_id . $product_id;
		    $product_abberivation->mul_store_id = $store_connection_id;
		    $product_abberivation->product_id = $last_pro_id;
		    $product_abberivation->price = $price;
		    $product_abberivation->salePrice = $price;
		    $product_abberivation->schedules_sales_date = date('Y-m-d', time());
		    $product_abberivation->stock_quantity = 0;
		    $product_abberivation->stock_level = 'Out of Stock';
		    $product_abberivation->channel_accquired = $channel_name;
		    $product_abberivation->created_at = date('Y-m-d H:i:s', time());
		    $product_abberivation->updated_at = date('Y-m-d H:i:s', time());
		    $product_abberivation->save(false);

		    $custom_productChannelModel = new ProductChannel();
		    if ($store_id != '') {
			$custom_productChannelModel->store_id = $store_connection_id;
		    }
		    if ($channel_id != '') {
			$custom_productChannelModel->channel_id = $channel_id;
		    }
		    $custom_productChannelModel->product_id = $last_pro_id;
		    $custom_productChannelModel->elliot_user_id = $user_id;
		    $custom_productChannelModel->status = 'no';
		    $custom_productChannelModel->created_at = date('Y-m-d h:i:s', time());
		    $custom_productChannelModel->save(false);
		} elseif (empty($product_sku_check)) {
		    $productModel = new Products ();
		    $productModel->channel_abb_id = '';
		    $productModel->product_name = $title;
		    $productModel->SKU = $sku;
		    $productModel->UPC = '';
		    $productModel->EAN = '';
		    $productModel->Jan = '';
		    $productModel->ISBN = '';
		    $productModel->MPN = '';
		    $productModel->description = '';
		    $productModel->adult = 'no';
		    $productModel->age_group = NULL;
		    $productModel->gender = 'Unisex';
		    $productModel->brand = '';
		    $productModel->stock_quantity = 1;
		    $productModel->availability = 'Out of Stock';
		    $productModel->stock_level = 'Out of Stock';
		    $productModel->stock_status = 'Hidden';
		    $productModel->price = $price;
		    $productModel->sales_price = $price;
		    $productModel->elliot_user_id = $user_id;
		    $productModel->product_status = 'in_active';
		    $productModel->permanent_hidden = 'in_active';
		    $productModel->save(false);
		    $last_pro_id = $productModel->id;

		    $custom_productChannelModel = new ProductChannel();
		    if ($store_id != '') {
			$custom_productChannelModel->store_id = $store_id;
		    }
		    if ($channel_id != '') {
			$custom_productChannelModel->channel_id = $channel_id;
		    }
		    $custom_productChannelModel->product_id = $last_pro_id;
		    $custom_productChannelModel->elliot_user_id = $user_id;
		    $custom_productChannelModel->status = 'no';
		    $custom_productChannelModel->created_at = date('Y-m-d H:i:s', strtotime($order_created_at));
		    $custom_productChannelModel->save(false);

		    $product_abberivation = new ProductAbbrivation();
		    $product_abberivation->channel_abb_id = $prefix_id . $product_id;
		    $product_abberivation->mul_store_id = $store_connection_id;
		    $product_abberivation->product_id = $last_pro_id;
		    $product_abberivation->price = $price;
		    $product_abberivation->salePrice = $price;
		    $product_abberivation->schedules_sales_date = date('Y-m-d', time());
		    $product_abberivation->stock_quantity = 0;
		    $product_abberivation->stock_level = 'Out of Stock';
		    $product_abberivation->channel_accquired = $channel_name;
		    $product_abberivation->created_at = date('Y-m-d H:i:s', time());
		    $product_abberivation->updated_at = date('Y-m-d H:i:s', time());
		    $product_abberivation->save(false);
		}
		$order_products_model = new OrdersProducts;
		$order_products_model->order_Id = $last_order_id;
		$order_products_model->product_Id = $last_pro_id;
		$order_products_model->qty = $quantity;
		$order_products_model->order_product_sku = $sku;
		$order_products_model->price = $total_product_price;
		$order_products_model->created_at = date('Y-m-d h:i:s', strtotime($order_created_at));
		$order_products_model->updated_at = date('Y-m-d h:i:s', strtotime($order_created_at));
		$order_products_model->elliot_user_id = $user_id;
		$order_products_model->save(false);
	    }
	}
    }

    /**
     * Common function for Product update function channels/stores
     * @param type $product
     */
    public static function producUpdateCommon($product) {
	$product_abb_id = $product['product_id'];
	$product_status = $product['status'];
	$price = $product['price'];
	$sale_price = $product['sale_price'];
	$qty = $product['qty'];
	if ($qty < 0) {
	    $qty = 0;
	}
	$stock_status = $product['stock_status'];
	$prefix_id = $product['channel_store_prefix'];
	$product_price = $price;
	$product_sale_price = $sale_price;
	if (array_key_exists('currency_code', $product)) {
	    $currency = $product['currency_code'];
	    if ($product['currency_code'] != '') {
		$conversion_rate=1;
		if (!empty($currency)) {
		    $conversion_rate = self::getDbConversionRate($currency);
		}
		//$conversion_rate = self::getCurrencyConversionRate($currency, 'USD');
		$product_price = number_format((float) $conversion_rate * $price, 2, '.', '');
		$product_sale_price = number_format((float) $conversion_rate * $sale_price, 2, '.', '');
	    }
	}

	$channel_abb_id = $prefix_id . $product_abb_id;

	if (empty($store_connection_id)) {
	    $product_check = ProductAbbrivation::find()->where(['channel_abb_id' => $channel_abb_id])->one();
	} else {
	    $product_check = ProductAbbrivation::find()->Where(['channel_abb_id' => $channel_abb_id, 'mul_store_id' => $store_connection_id])->one();
	}
	if (!empty($product_check)) {
	    $product_check->price = $product_price;
	    $product_check->salePrice = $product_sale_price;
	    $product_check->stock_quantity = $qty;
	    if ($qty > 0) {
		$product_check->stock_level = 'In Stock';
	    } else {
		$product_check->stock_level = 'Out of Stock';
	    }
	    if ($stock_status == 1) {
		$product_check->stock_status = 'Visible';
	    } else {
		$product_check->stock_status = 'Hidden';
	    }
	    if ($product_status == 1) {
		$product_check->stock_status = 'Visible';
	    } else {
		$product_check->stock_status = 'Hidden';
	    }
	    $product_check->updated_at = date('Y-m-d H:i:s', time());
	    $product_check->save(false);
	}
    }

    /**
     * Common funtion for Order update channels/stores
     */
    public function orderUpdateCommon($order_data) {
	$order_id = $order_data['order_id'];
	$order_status = $order_data['status'];
	$order_store_id = $order_data['magento_store_id'];
	$order_total_amount = $order_data['order_grand_total'];
	$customer_id = $order_data['customer_id'];
	$customer_email = $order_data['customer_email'];
	$order_shipping = $order_data['order_shipping_amount'];
	$order_tax = $order_data['order_tax_amount'];
	$order_product_qty = $order_data['total_qty_ordered'];
	$order_created_at = $order_data['created_at'];
	$order_updated_at = $order_data['updated_at'];
	$payment_method = $order_data['payment_method'];
	$order_refund_amount = $order_data['refund_amount'];
	$order_discount_amount = $order_data['discount_amount'];

	$order_items = $order_data['items'];

	$channel_name = $order_data['channel_store_name'];
	$prefix_id = $order_data['channel_store_prefix'];
	$user_id = $order_data['elliot_user_id'];
	$store_id = $order_data['store_id'];
	$channel_id = $order_data['channel_id'];
	$store_connection_id = $order_data['mul_store_id'];
	$channel_connection_id = $order_data['mul_channel_id'];

	$order_total = $order_total_amount;
	$order_tax_amount = $order_tax;
	$order_shipping_amount = $order_shipping;
	$refund_amount = $order_refund_amount;
	$discount_amount = $order_discount_amount;
	if (array_key_exists('currency_code', $order_data)) {
	    $currency_code = $order_data['currency_code'];
	    $conversion_rate=1;
	    if (!empty($currency_code)) {
		$conversion_rate = self::getDbConversionRate($currency_code);
	    }
	    //$conversion_rate = self::getCurrencyConversionRate($currency_code, 'USD');
	    $product_price = number_format((float) $conversion_rate * $order_total_amount, 2, '.', '');
	    $product_sale_price = number_format((float) $conversion_rate * $order_tax, 2, '.', '');
	    $order_shipping_amount = number_format((float) $conversion_rate * $order_shipping, 2, '.', '');
	    $refund_amount = number_format((float) $conversion_rate * $order_refund_amount, 2, '.', '');
	    $discount_amount = number_format((float) $conversion_rate * $order_discount_amount, 2, '.', '');
	}

	//Order Shipping address
	$order_shipping_add = $order_data['shipping_address'];
	$ship_state = $order_shipping_add['state'];
	$ship_zip = $order_shipping_add['postcode'];
	$ship_add_1 = $order_shipping_add['street_1'];
	$ship_add_2 = $order_shipping_add['street_2'];
	$ship_city = $order_shipping_add['city'];
	$ship_customer_email = $order_shipping_add['email'];
	$ship_phone = $order_shipping_add['telephone'];
	$ship_country_id = $order_shipping_add['country_id'];
	$ship_country = $order_shipping_add['country'];
	$ship_firstname = $order_shipping_add['firstname'];
	$ship_lastname = $order_shipping_add['lastname'];
	$shipping_address = $ship_add_1 . "," . $ship_add_2 . "," . $ship_city . "," . $ship_state . "," . $ship_zip . "," . $ship_country_id;

	//Order Billing address
	$order_billing_add = $order_data['billing_address'];
	$bill_state = $order_billing_add['state'];
	$bill_zip = $order_billing_add['postcode'];
	$bill_add_1 = $order_billing_add['street_1'];
	$bill_add_2 = $order_billing_add['street_2'];
	$bill_city = $order_billing_add['city'];
	$bill_customer_email = $order_billing_add['email'];
	$bill_phone = $order_billing_add['telephone'];
	$bill_country_id = $order_billing_add['country_id'];
	$bill_country = $order_billing_add['country'];
	$bill_firstname = $order_billing_add['firstname'];
	$bill_lastname = $order_billing_add['lastname'];
	$billing_address = $bill_add_1 . "," . $bill_add_2 . "," . $bill_city . "," . $bill_state . "," . $bill_zip . "," . $bill_country_id;

	$order_prefix_id = $prefix_id . $order_id;

	if (empty($store_connection_id)) {
	    $check_order = Orders::find()->Where(['channel_abb_id' => $order_prefix_id])->one();
	} else {
	    $check_order = Orders::find()->Where(['channel_abb_id' => $order_prefix_id, 'mul_store_id' => $store_connection_id])->one();
	}

	if (!empty($check_order)) {
	    $check_order->order_status = $order_status;
	    $check_order->product_qauntity = $order_product_qty;
	    $check_order->shipping_address = $shipping_address;
	    $check_order->ship_street_1 = $ship_add_1;
	    $check_order->ship_street_2 = $ship_add_2;
	    $check_order->ship_city = $ship_city;
	    $check_order->ship_state = $ship_state;
	    $check_order->ship_zip = $ship_zip;
	    $check_order->ship_country = $ship_country_id;
	    $check_order->billing_address = $billing_address;
	    $check_order->bill_street_1 = $bill_add_1;
	    $check_order->bill_street_2 = $bill_add_2;
	    $check_order->bill_city = $bill_city;
	    $check_order->bill_state = $bill_state;
	    $check_order->bill_zip = $bill_zip;
	    $check_order->bill_country = $bill_country_id;
	    $check_order->base_shipping_cost = $order_shipping_amount;
	    $check_order->shipping_cost_tax = $order_tax_amount;
	    $check_order->payment_method = $payment_method;
	    $check_order->payment_status = $order_status;
	    $check_order->refunded_amount = $refund_amount;
	    $check_order->discount_amount = $discount_amount;
	    $check_order->total_amount = $order_total;
	    $check_order->updated_at = date('Y-m-d h:i:s', strtotime($order_updated_at));
	    $check_order->save(false);
	    $last_order_id = $check_order->order_ID;

	    $OrdersProduct = OrdersProducts::deleteAll(['order_Id' => $last_order_id]);
	    $order_items = $order_data['items'];
	    foreach ($order_items as $_items) {
		$title = $_items['name'];
		$quantity = $_items['qty_ordered'];
		$product_price = $_items['price'];
		$sku = $_items['sku'];
		$product_id = $_items['product_id'];
		$product_weight = $_items['weight'];
		$order_total_product_price = $product_price * $quantity;
		$price = $product_price;
		$total_product_price = $order_total_product_price;
		if (array_key_exists('currency_code', $order_data)) {
		    $currency_code = $order_data['currency_code'];
		    $conversion_rate=1;
		    if (!empty($currency_code)) {
			$conversion_rate = self::getDbConversionRate($currency_code);
		    }
		    //$conversion_rate = self::getCurrencyConversionRate($currency_code, 'USD');
		    $price = number_format((float) $conversion_rate * $product_price, 2, '.', '');
		    $total_product_price = number_format((float) $conversion_rate * $order_total_product_price, 2, '.', '');
		}

		$product_sku_check = Products::find()->Where(['SKU' => $sku])
				->with(['productAbbrivation' => function($query) use ($prefix_id, $product_id) {
					$query->andWhere(['channel_abb_id' => $prefix_id . $product_id]);
				    }])->asArray()->one();
		if (!empty($product_sku_check) && !empty($product_sku_check['productAbbrivation'])) {
		    $last_pro_id = $product_sku_check['id'];
		} elseif (!empty($product_sku_check) && empty($product_sku_check['productAbbrivation'])) {
		    $last_pro_id = $product_sku_check['id'];
		    $product_abberivation = new ProductAbbrivation();
		    $product_abberivation->channel_abb_id = $prefix_id . $product_id;
		    $product_abberivation->product_id = $last_pro_id;
		    $product_abberivation->price = $price;
		    $product_abberivation->salePrice = $price;
		    $product_abberivation->schedules_sales_date = date('Y-m-d', time());
		    $product_abberivation->stock_quantity = 0;
		    $product_abberivation->stock_level = 'Out of Stock';
		    $product_abberivation->channel_accquired = $channel_name;
		    $product_abberivation->created_at = date('Y-m-d H:i:s', time());
		    $product_abberivation->updated_at = date('Y-m-d H:i:s', time());
		    $product_abberivation->save(false);

		    $custom_productChannelModel = new ProductChannel();
		    if ($store_id != '') {
			$custom_productChannelModel->store_id = $store_id;
		    }
		    if ($channel_id != '') {
			$custom_productChannelModel->channel_id = $channel_id;
		    }
		    $custom_productChannelModel->product_id = $last_pro_id;
		    $custom_productChannelModel->elliot_user_id = $user_id;
		    $custom_productChannelModel->status = 'no';
		    $custom_productChannelModel->created_at = date('Y-m-d h:i:s', time());
		    $custom_productChannelModel->save(false);
		} elseif (empty($product_sku_check)) {
		    $productModel = new Products ();
		    $productModel->channel_abb_id = '';
		    $productModel->product_name = $title;
		    $productModel->SKU = $sku;
		    $productModel->UPC = '';
		    $productModel->EAN = '';
		    $productModel->Jan = '';
		    $productModel->ISBN = '';
		    $productModel->MPN = '';
		    $productModel->description = '';
		    $productModel->adult = 'no';
		    $productModel->age_group = NULL;
		    $productModel->gender = 'Unisex';
		    $productModel->brand = '';
		    $productModel->stock_quantity = 1;
		    $productModel->availability = 'Out of Stock';
		    $productModel->stock_level = 'Out of Stock';
		    $productModel->stock_status = 'Hidden';
		    $productModel->price = $price;
		    $productModel->sales_price = $price;
		    $productModel->elliot_user_id = $user_id;
		    $productModel->product_status = 'in_active';
		    $productModel->permanent_hidden = 'in_active';
		    $productModel->save(false);
		    $last_pro_id = $productModel->id;

		    $custom_productChannelModel = new ProductChannel();
		    if ($store_id != '') {
			$custom_productChannelModel->store_id = $store_id;
		    }
		    if ($channel_id != '') {
			$custom_productChannelModel->channel_id = $channel_id;
		    }
		    $custom_productChannelModel->product_id = $last_pro_id;
		    $custom_productChannelModel->elliot_user_id = $user_id;
		    $custom_productChannelModel->status = 'no';
		    $custom_productChannelModel->created_at = date('Y-m-d H:i:s', strtotime($order_created_at));
		    $custom_productChannelModel->save(false);

		    $product_abberivation = new ProductAbbrivation();
		    $product_abberivation->channel_abb_id = $prefix_id . $product_id;
		    $product_abberivation->product_id = $last_pro_id;
		    $product_abberivation->price = $price;
		    $product_abberivation->salePrice = $price;
		    $product_abberivation->schedules_sales_date = date('Y-m-d', time());
		    $product_abberivation->stock_quantity = 0;
		    $product_abberivation->stock_level = 'Out of Stock';
		    $product_abberivation->channel_accquired = $channel_name;
		    $product_abberivation->created_at = date('Y-m-d H:i:s', time());
		    $product_abberivation->updated_at = date('Y-m-d H:i:s', time());
		    $product_abberivation->save(false);
		}

		$order_products_model = new OrdersProducts;
		$order_products_model->order_Id = $last_order_id;
		$order_products_model->product_Id = $last_pro_id;
		$order_products_model->qty = $quantity;
		$order_products_model->order_product_sku = $sku;
		$order_products_model->price = $total_product_price;
		$order_products_model->created_at = date('Y-m-d h:i:s', strtotime($order_created_at));
		$order_products_model->updated_at = date('Y-m-d h:i:s', strtotime($order_created_at));
		$order_products_model->elliot_user_id = $user_id;
		$order_products_model->save(false);
	    }
	}
    }

    public static function getCurrencyConversionRate($from_currency = 'USD', $to_currency = 'USD') {
	$conversion_rate = 1;

	$username = Yii::$app->params['xe_account_id'];
	$password = Yii::$app->params['xe_account_api_key'];
	$URL = Yii::$app->params['xe_base_url'] . 'convert_from.json/?from=' . $from_currency . '&to=' . $to_currency . '&amount=1';

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $URL);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
	curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
	$result = curl_exec($ch);
	$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
	curl_close($ch);
//echo'<pre>'; print_r($result);die('end here');

	$result = json_decode($result, true);
	//       print_r($result);
	if (isset($result) and ! empty($result) and isset($result['to']) and isset($result['to'][0]) and isset($result['to'][0]['quotecurrency'])) {
	    $conversion_rate = $result['to'][0]['mid'];
	    $no_without_decimal = number_format(str_replace('.', '', $conversion_rate), 5, '', '');
	    if (strstr($conversion_rate, 'E-')) {
		$pos = strpos($conversion_rate, 'E-');
		$number_to_be_divided = substr($conversion_rate, $pos + 2);
		$output = substr($conversion_rate, 0, strrpos($conversion_rate, 'E-'));
		$string_pos = strpos($output, '.');
		$no_of_zeros = $number_to_be_divided - $string_pos;
		$zeros = '.';
		for ($i = 0; $i < $no_of_zeros; $i++) {
		    $zeros .= 0;
		}
		$conversion_rate = $zeros . $no_without_decimal;
	    }
	    $conversion_rate = number_format((float) $conversion_rate, 2, '.', '');
	}
	return $conversion_rate;
    }

    public static function getCurrencySymbol($name) {
	$symbol_data = CurrencySymbols::find()->Where(['name' => $name])->one();
	if (!empty($symbol_data)) {
	    $symbol = $symbol_data->symbol;
	} else {
	    $symbol = '';
	}
	return $symbol;
    }

    public static function getDbConversionRate($currency) {
	$currency_conversion_data = CurrencyConversion::find()->Where(['to_currency' => $currency])->one();
	if (!empty($currency_conversion_data)) {
	    $conversion_rate = $currency_conversion_data->to_value;
	    return $conversion_rate;
	}
    }

}
