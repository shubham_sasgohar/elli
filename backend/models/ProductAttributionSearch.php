<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\ProductAttribution;

/**
 * ProductAttributionSearch represents the model behind the search form about `backend\models\ProductAttribution`.
 */
class ProductAttributionSearch extends ProductAttribution
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_attribution_id', 'attribution_id', 'product_id'], 'integer'],
            [['item_name', 'item_value', 'store_attribution_id', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductAttribution::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'product_attribution_id' => $this->product_attribution_id,
            'attribution_id' => $this->attribution_id,
            'product_id' => $this->product_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'item_name', $this->item_name])
            ->andFilterWhere(['like', 'item_value', $this->item_value])
            ->andFilterWhere(['like', 'store_attribution_id', $this->store_attribution_id]);

        return $dataProvider;
    }
}
