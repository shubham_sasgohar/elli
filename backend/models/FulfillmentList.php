<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "fulfillment_list".
 *
 * @property integer $id
 * @property string $fulfillment_name
 * @property string $image_link
 */
class FulfillmentList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fulfillment_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'fulfillment_name', 'image_link'], 'required'],
            [['id'], 'integer'],
            [['fulfillment_name', 'image_link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fulfillment_name' => 'Fulfillment Name',
            'image_link' => 'Image Link',
        ];
    }
}
