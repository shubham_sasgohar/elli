<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "customer_abbrivation".
 *
 * @property integer $id
 * @property integer $channel_abb_id
 * @property integer $customer_id
 * @property string $channel_accquired
 * @property string $created_at
 * @property string $updated_at
 *
 * @property CustomerUser $customer
 */
class CustomerAbbrivation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_abbrivation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['channel_abb_id', 'customer_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['channel_accquired'], 'string', 'max' => 255],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerUser::className(), 'targetAttribute' => ['customer_id' => 'customer_ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'channel_abb_id' => 'Channel Abb ID',
            'customer_id' => 'Customer ID',
            'channel_accquired' => 'Channel Accquired',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(CustomerUser::className(), ['customer_ID' => 'customer_id']);
    }
}
