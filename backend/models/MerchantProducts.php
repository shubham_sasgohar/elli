<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "merchant_products".
 *
 * @property integer $merchant_products_id
 * @property integer $merchant_ID
 * @property integer $product_ID
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $merchant
 * @property Products $product
 */
class MerchantProducts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'merchant_products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['merchant_ID', 'product_ID'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['merchant_ID'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['merchant_ID' => 'id']],
            [['product_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_ID' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'merchant_products_id' => 'Merchant Products ID',
            'merchant_ID' => 'Merchant  ID',
            'product_ID' => 'Product  ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMerchant()
    {
        return $this->hasOne(User::className(), ['id' => 'merchant_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_ID']);
    }
}
