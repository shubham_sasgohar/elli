<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Orders;

/**
 * OrdersSerach represents the model behind the search form about `backend\models\Orders`.
 */
class OrdersSerach extends Orders
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_ID', 'customer_id', 'market_place_fees', 'credit_card_fees', 'processing_fees', 'total_amount'], 'integer'],
            [['stripe_transaction_ID', 'order_status', 'product_qauntity', 'shipping_address', 'billing_address', 'brand_logo_image', 'tracking_link', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orders::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'order_ID' => $this->order_ID,
            'customer_id' => $this->customer_id,
            'market_place_fees' => $this->market_place_fees,
            'credit_card_fees' => $this->credit_card_fees,
            'processing_fees' => $this->processing_fees,
            'total_amount' => $this->total_amount,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'stripe_transaction_ID', $this->stripe_transaction_ID])
            ->andFilterWhere(['like', 'order_status', $this->order_status])
            ->andFilterWhere(['like', 'product_qauntity', $this->product_qauntity])
            ->andFilterWhere(['like', 'shipping_address', $this->shipping_address])
            ->andFilterWhere(['like', 'billing_address', $this->billing_address])
            ->andFilterWhere(['like', 'brand_logo_image', $this->brand_logo_image])
            ->andFilterWhere(['like', 'tracking_link', $this->tracking_link]);

        return $dataProvider;
    }
}
