<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\User;

/**
 * UserSearch represents the model behind the search form about `backend\models\User`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'parent_id', 'status', 'subscription_plan_id', 'corporate_add_zipcode'], 'integer'],
            [['username', 'first_name', 'last_name', 'email','dob','phoneno','gender', 'password_hash', 'account_status', 'password_reset_token', 'auth_key', 'default_language', 'default_weight_preference', 'date_last_login', 'role', 'trial_period_status', 'plan_status', 'account_confirm_status', 'corporate_add_street1', 'corporate_add_street2', 'corporate_add_city', 'corporate_add_state', 'corporate_add_country', 'billing_address_street1', 'billing_address_street2', 'billing_address_city', 'billing_address_state', 'billing_address_zipcode', 'billing_address_country', 'created_at', 'updated_at'], 'safe'],
            [['tax_rate'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'tax_rate' => $this->tax_rate,
            'date_last_login' => $this->date_last_login,
            'status' => $this->status,
            'subscription_plan_id' => $this->subscription_plan_id,
            'corporate_add_zipcode' => $this->corporate_add_zipcode,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'dob', $this->dob])
            ->andFilterWhere(['like', 'phoneno', $this->phoneno])
            ->andFilterWhere(['like', 'gender', $this->gender])    
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'account_status', $this->account_status])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'default_language', $this->default_language])
            ->andFilterWhere(['like', 'default_weight_preference', $this->default_weight_preference])
            ->andFilterWhere(['like', 'role', $this->role])
            ->andFilterWhere(['like', 'trial_period_status', $this->trial_period_status])
            ->andFilterWhere(['like', 'plan_status', $this->plan_status])
            ->andFilterWhere(['like', 'account_confirm_status', $this->account_confirm_status])
            ->andFilterWhere(['like', 'corporate_add_street1', $this->corporate_add_street1])
            ->andFilterWhere(['like', 'corporate_add_street2', $this->corporate_add_street2])
            ->andFilterWhere(['like', 'corporate_add_city', $this->corporate_add_city])
            ->andFilterWhere(['like', 'corporate_add_state', $this->corporate_add_state])
            ->andFilterWhere(['like', 'corporate_add_country', $this->corporate_add_country])
            ->andFilterWhere(['like', 'billing_address_street1', $this->billing_address_street1])
            ->andFilterWhere(['like', 'billing_address_street2', $this->billing_address_street2])
            ->andFilterWhere(['like', 'billing_address_city', $this->billing_address_city])
            ->andFilterWhere(['like', 'billing_address_state', $this->billing_address_state])
            ->andFilterWhere(['like', 'billing_address_zipcode', $this->billing_address_zipcode])
            ->andFilterWhere(['like', 'billing_address_country', $this->billing_address_country]);

        return $dataProvider;
    }
}
