<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "smartling".
 *
 * @property integer $id
 * @property integer $elliot_user_id
 * @property string $project_name
 * @property string $token
 * @property string $sm_user_id
 * @property string $secret_key
 * @property string $project_id
 * @property string $connected
 * @property string $created_at
 * @property string $updated_at
 */
class Smartling extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'smartling';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['elliot_user_id'], 'integer'],
            [['connected'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['project_name', 'token', 'project_id'], 'string', 'max' => 255],
            [['sm_user_id', 'secret_key'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'elliot_user_id' => 'Elliot User ID',
            'project_name' => 'Project Name',
            'token' => 'Token',
            'sm_user_id' => 'Sm User ID',
            'secret_key' => 'Secret Key',
            'project_id' => 'Project ID',
            'connected' => 'Connected',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
