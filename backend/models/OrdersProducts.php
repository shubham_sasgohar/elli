<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "orders_products".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $product_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Orders $id0
 * @property Products $product
 */
class OrdersProducts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders_products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id'], 'required'],
            [['order_id', 'product_id'], 'integer'],
            [['created_at', 'updated_at','qty'], 'safe'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['id' => 'order_id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'product_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'product_id' => 'Product ID',
            'qty' => 'Quantity',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId()
    {
        return $this->hasOne(Orders::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_Id']);
    }
        public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['order_ID' => 'order_Id']);
    }
}
