<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "order_fullfillment".
 *
 * @property integer $id
 * @property integer $elliot_user_id
 * @property integer $order_id
 * @property string $name
 * @property string $key_data
 * @property string $value_data
 * @property string $created_at
 * @property string $updated_at
 */
class OrderFullfillment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_fullfillment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'elliot_user_id', 'order_id', 'name', 'key_data', 'value_data', 'created_at'], 'required'],
            [['id', 'elliot_user_id', 'order_id'], 'integer'],
            [['value_data'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'key_data'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'elliot_user_id' => 'Elliot User ID',
            'order_id' => 'Order ID',
            'name' => 'Name',
            'key_data' => 'Key Data',
            'value_data' => 'Value Data',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
