<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "admin_sales".
 *
 * @property integer $id
 * @property integer $transaction_id
 * @property integer $user_id
 * @property integer $subscription_id
 * @property integer $channel_id
 * @property double $transaction_amount
 * @property string $transaction_date
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $user
 * @property Subscription $subscription
 */
class AdminSales extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'admin_sales';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['transaction_id', 'user_id', 'subscription_id', 'channel_id'], 'integer'],
            [['transaction_amount'], 'number'],
            [['transaction_date', 'created_at', 'updated_at'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['subscription_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subscription::className(), 'targetAttribute' => ['subscription_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'transaction_id' => 'Transaction ID',
            'user_id' => 'User ID',
            'subscription_id' => 'Subscription ID',
            'channel_id' => 'Channel ID',
            'transaction_amount' => 'Transaction Amount',
            'transaction_date' => 'Transaction Date',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscription()
    {
        return $this->hasOne(Subscription::className(), ['id' => 'subscription_id']);
    }
}
