<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "product_categories".
 *
 * @property integer $product_categories_id
 * @property integer $category_ID
 * @property integer $product_ID
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Categories $category
 * @property Products $product
 */
class ProductCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_ID', 'product_ID'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['category_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['category_ID' => 'category_ID']],
            [['product_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_ID' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_categories_id' => 'Product Categories ID',
            'category_ID' => 'Category  ID',
            'product_ID' => 'Product  ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['category_ID' => 'category_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_ID']);
    }
}
