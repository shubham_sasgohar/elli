<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Products;

/**
 * ProductsSearch represents the model behind the search form about `app\models\Products`.
 */
class ProductsSearch extends Products {

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
      [['id', 'low_stock_notification'], 'integer'],
      [['product_name', 'SKU', 'UPC', 'EAN', 'Jan', 'ISBN', 'MPN', 'description', 'adult', 'age_group', 'availability', 'brand', 'condition', 'gender', 'weight', 'stock_quantity', 'stock_level', 'stock_status', 'price', 'sales_price', 'schedules_sales_date', 'created_at', 'updated_at', 'occasion', 'weather'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios() {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function search($params) {
    $query = Products::find();

    // add conditions that should always apply here

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);

    $this->load($params);

    if (!$this->validate()) {
      // uncomment the following line if you do not want to return any records when validation fails
      // $query->where('0=1');
      return $dataProvider;
    }

    // grid filtering conditions
    $query->andFilterWhere([
      'id' => $this->id,
      'low_stock_notification' => $this->low_stock_notification,
      'schedules_sales_date' => $this->schedules_sales_date,
      'created_at' => $this->created_at,
      'updated_at' => $this->updated_at,
    ]);

    $query->andFilterWhere(['like', 'product_name', $this->product_name])
        ->andFilterWhere(['like', 'SKU', $this->SKU])
        ->andFilterWhere(['like', 'UPC', $this->UPC])
        ->andFilterWhere(['like', 'EAN', $this->EAN])
        ->andFilterWhere(['like', 'Jan', $this->Jan])
        ->andFilterWhere(['like', 'ISBN', $this->ISBN])
        ->andFilterWhere(['like', 'MPN', $this->MPN])
        ->andFilterWhere(['like', 'description', $this->description])
        ->andFilterWhere(['like', 'adult', $this->adult])
        ->andFilterWhere(['like', 'age_group', $this->age_group])
        ->andFilterWhere(['like', 'availability', $this->availability])
        ->andFilterWhere(['like', 'brand', $this->brand])
        ->andFilterWhere(['like', 'condition', $this->condition])
        ->andFilterWhere(['like', 'gender', $this->gender])
        ->andFilterWhere(['like', 'weight', $this->weight])
        ->andFilterWhere(['like', 'stock_quantity', $this->stock_quantity])
        ->andFilterWhere(['like', 'stock_level', $this->stock_level])
        ->andFilterWhere(['like', 'stock_status', $this->stock_status])
        ->andFilterWhere(['like', 'price', $this->price])
        ->andFilterWhere(['like', 'sales_price', $this->sales_price])
        ->andFilterWhere(['like', 'occasion', $this->occasion])
        ->andFilterWhere(['like', 'weather', $this->weather]);

    return $dataProvider;
  }

}
