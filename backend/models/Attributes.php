<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "attributes".
 *
 * @property integer $attribute_id
 * @property integer $elliot_user_id
 * @property string $attribute_name
 * @property string $attribute_label
 * @property string $attribute_description
 * @property string $attribute_type
 * @property string $created_at
 * @property string $updated_at
 *
 * @property ProductAttribution[] $productAttributions
 */
class Attributes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attributes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['elliot_user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['attribute_name', 'attribute_label', 'attribute_description', 'attribute_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'attribute_id' => 'Attribute ID',
            'elliot_user_id' => 'Elliot User ID',
            'attribute_name' => 'Attribute Name',
            'attribute_label' => 'Attribute Label',
            'attribute_description' => 'Attribute Description',
            'attribute_type' => 'Attribute Type',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAttributions()
    {
        return $this->hasMany(ProductAttribution::className(), ['attribution_id' => 'attribute_id']);
    }
}
