<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "variations".
 *
 * @property integer $variations_ID
 * @property string $variation_name
 * @property string $variation_description
 * @property string $created_at
 * @property string $updated_at
 * @property string $variation_type
 *
 * @property ProductVariation[] $productVariations
 * @property VariationsItemList[] $variationsItemLists
 */
class Variations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'variations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['variation_name', 'variation_description', 'variation_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'variations_ID' => 'Variations  ID',
            'variation_name' => 'Variation Name',
            'variation_description' => 'Variation Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'variation_type' => 'Variation Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductVariations()
    {
        return $this->hasMany(ProductVariation::className(), ['variation_id' => 'variations_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVariationsItemLists()
    {
        return $this->hasMany(VariationsItemList::className(), ['variation_id' => 'variations_ID']);
    }
}
