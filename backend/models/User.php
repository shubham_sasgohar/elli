<?php

namespace backend\models;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\filters\Cors;
//use backend\models\User;
//use backend\models\User;
use backend\models\SubscriptionUserTiers;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $username
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $password_hash
 * @property string $account_status
 * @property string $password_reset_token
 * @property string $auth_key
 * @property double $tax_rate
 * @property string $default_language
 * @property string $default_weight_preference
 * @property string $date_last_login
 * @property integer $status
 * @property string $role
 * @property string $trial_period_status
 * @property integer $subscription_plan_id
 * @property string $plan_status
 * @property string $account_confirm_status
 * @property string $corporate_add_street1
 * @property string $corporate_add_street2
 * @property string $corporate_add_city
 * @property string $corporate_add_state
 * @property integer $corporate_add_zipcode
 * @property string $corporate_add_country
 * @property string $billing_address_street1
 * @property string $billing_address_street2
 * @property string $billing_address_city
 * @property string $billing_address_state
 * @property string $billing_address_zipcode
 * @property string $billing_address_country
 * @property string $created_at
 * @property string $updated_at
 *
 * @property MerchantProducts[] $merchantProducts
 */
class User extends \yii\db\ActiveRecord
{
    
      const role_merchant           ='merchant';
      const role_merchant_user      ='merchant_user';
      const plan_status_activate    ='activate';
      const plan_status_deactivate  ='deactivate';
      const default_plan            ='Starter';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

     
  public function behaviors()
    {
//        return [
//           // TimestampBehavior::className(),
//        ];
        return [                                                                                                                                                                                                                                                                                                                                            
            'timestamp' => [
                
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    self::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => function () {
                return new Expression('CURRENT_TIMESTAMP');
                }
            ],
        ];
            
       
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'status', 'subscription_plan_id', 'corporate_add_zipcode'], 'integer'],
            ['username', 'trim'],
            ['username', 'unique', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            
            ['company_name', 'trim'],
            ['company_name', 'required'],
            ['company_name', 'unique', 'message' => 'This username has already been taken.'],
            ['company_name', 'string', 'min' => 2, 'max' => 255],
            
            ['last_name', 'required'],
            
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'message' => 'This email address has already been taken.'],
            ['email', 'checkEmail'],
            
            ['role', 'required'],
            ['role', 'check_user_range'],
//            
            [['account_status', 'role', 'trial_period_status', 'plan_status', 'account_confirm_status','gender'], 'string'],
            [['tax_rate'], 'number'],
            [['date_last_login', 'created_at', 'updated_at','dob','phoneno','img','timezone'], 'safe'],
            [['username', 'first_name','domain_name', 'default_language', 'default_weight_preference', 'corporate_add_street1', 'corporate_add_street2', 'corporate_add_city', 'corporate_add_state', 'corporate_add_country', 'billing_address_street1', 'billing_address_street2', 'billing_address_city', 'billing_address_state', 'billing_address_zipcode', 'billing_address_country'], 'string', 'max' => 255],
            [['last_name', 'email'], 'string', 'max' => 45],
            [['password_hash', 'password_reset_token', 'auth_key'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'username' => 'Username',
            'company_name' => 'Company Name',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'domain_name' => 'Domain',
            'dob' => 'Date Of Birth',
            'phoneno' => 'Phone No',
            'gender' => 'Gender',
            'password_hash' => 'Password Hash',
            'account_status' => 'Account Status',
            'password_reset_token' => 'Password Reset Token',
            'auth_key' => 'Auth Key',
            'tax_rate' => 'Tax Rate',
            'default_language' => 'Default Language',
            'default_weight_preference' => 'Default Weight Preference',
            'timezone' => 'Timezone',
            'date_last_login' => 'Date Last Login',
            'status' => 'Status',
            'role' => 'Role',
            'trial_period_status' => 'Trial Period Status',
            'subscription_plan_id' => 'Subscription Plan ID',
            'plan_status' => 'Plan Status',
            'account_confirm_status' => 'Account Confirm Status',
            'corporate_add_street1' => 'Corporate Add Street1',
            'corporate_add_street2' => 'Corporate Add Street2',
            'corporate_add_city' => 'Corporate Add City',
            'corporate_add_state' => 'Corporate Add State',
            'corporate_add_zipcode' => 'Corporate Add Zipcode',
            'corporate_add_country' => 'Corporate Add Country',
            'billing_address_street1' => 'Billing Address Strret1',
            'billing_address_street2' => 'Billing Address Strret2',
            'billing_address_city' => 'Billing Address City',
            'billing_address_state' => 'Billing Address State',
            'billing_address_zipcode' => 'Billing Address Zipcode',
            'billing_address_country' => 'Billing Address Country',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    
    
    /**
     * check merchant_user Range
     */
    public function check_user_range($attribute, $params)
    {
        /*Check merchant user range*/
        $users_Id = Yii::$app->user->identity->id;
        $merchant_data=User::find()->Where(['parent_id'=>$users_Id])->all();
        
        
        $user_range=count($merchant_data);
        /*get subscription paln id*/
        $users_sub_active_plan = Yii::$app->user->identity->subscription_plan_id;
        $plan_status = Yii::$app->user->identity->plan_status;
        
        /*get subscription data*/
        if($plan_status==User::plan_status_deactivate) {
            
           $subscription_data=SubscriptionUserTiers::find()->Where(['plan_name'=>User::default_plan])->one();
        }else {
           $subscription_data=SubscriptionUserTiers::find()->Where(['plan_name'=>$users_sub_active_plan])->one(); 
        }
        
        $create_user_range=$subscription_data->user_range;
        
        if($user_range >= $create_user_range ) {
            
            // no real check at the moment to be sure that the error is triggered
            $this->addError('role','You Cannot add More User Please Change Your Plan');                
            // $this->addError($attribute, Yii::t('user', 'You entered an invalid date format.')); 
        }
    }
    
        /**
     * check User range
     */
    public function checkEmail($attribute, $params)
    {
            $new_user_email=$this->email;
            /*explode a user new email*/
            $explode_new_user_email=explode("@",$new_user_email);
            $match_new_user_email=$explode_new_user_email[1];
         
            
            /*explode a merchant email*/
            $merchant_email=Yii::$app->user->identity->email;
            $explode_merchant_email=explode("@",$merchant_email);
            $merchant_email_domain=$explode_merchant_email[1];
         
            
            if($match_new_user_email != $merchant_email_domain){
                 // no real check at the moment to be sure that the error is triggered
                 $this->addError('email','This Email Domain not Match Merchant Email Domain');                
                 // $this->addError($attribute, Yii::t('user', 'You entered an invalid date format.')); 
            } 
    }
    
    
    public function setPassword($password)
    {
        //echo"<pre>"; print_r($password); die;
        return Yii::$app->security->generatePasswordHash($password);
    }
 

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        return Yii::$app->security->generateRandomString();
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMerchantProducts()
    {
        return $this->hasMany(MerchantProducts::className(), ['merchant_ID' => 'id']);
    }
}
