<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "google_product_categories".
 *
 * @property integer $google_category_ID
 * @property string $google_category_name
 * @property integer $google_parent_category_ID
 * @property string $created_at
 * @property string $updated_at
 */
class GoogleProductCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'google_product_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['google_parent_category_ID'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['google_category_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'google_category_ID' => 'Google Category  ID',
            'google_category_name' => 'Google Category Name',
            'google_parent_category_ID' => 'Google Parent Category  ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
