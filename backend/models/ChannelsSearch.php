<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Channels;

/**
 * ChannelsSerach represents the model behind the search form about `backend\models\Channels`.
 */
class ChannelsSearch extends Channels
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['channel_ID', 'channel_amount'], 'integer'],
            [['channel_name', 'channel_url', 'channel_image','status', 'created_at', 'updated_at', 'channel_revenue', 'channel_sales'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Channels::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'channel_ID' => $this->channel_ID,
            'channel_amount' => $this->channel_amount,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'channel_name', $this->channel_name])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'channel_url', $this->channel_url])
            ->andFilterWhere(['like', 'channel_image', $this->channel_image])
            ->andFilterWhere(['like', 'channel_revenue', $this->channel_revenue])
            ->andFilterWhere(['like', 'channel_sales', $this->channel_sales]);

        return $dataProvider;
    }
}
