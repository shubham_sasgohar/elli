<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "channels".
 *
 * @property integer $channel_ID
 * @property string $channel_name
 * @property string $channel_url
 * @property integer $channel_amount
 * @property string $channel_image
 * @property string $created_at
 * @property string $updated_at
 * @property string $channel_revenue
 * @property string $channel_sales
 *
 * @property OrderChannel[] $orderChannels
 * @property ProductChannel[] $productChannels
 * @property UserChannel[] $userChannels
 */
class Channels extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'channels';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['channel_amount'], 'integer'],
            [['trial_period_days'], 'integer'],
            [['created_at', 'updated_at','status'], 'safe'],
            [['channel_name','parent_name','stripe_Channel_id','amount', 'currency','channel_url', 'channel_image', 'channel_revenue', 'channel_sales'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'channel_ID' => 'Channel  ID',
            'channel_name' => 'Channel  name',
            'parent_name' => 'parent  name',
            'channel_ID' => 'Channel  ID',
            'stripe_Channel_id' => 'Channel',
            'amount' => 'Channel Amount',
            'currency' => 'Channel currency',
            'channel_url' => 'Channel Url',
            'channel_amount' => 'Channel Amount',
            'channel_image' => 'Channel Image',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'channel_revenue' => 'Channel Revenue',
            'channel_sales' => 'Channel Sales',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderChannels()
    {
        return $this->hasMany(OrderChannel::className(), ['channel_id' => 'channel_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductChannels()
    {
        return $this->hasMany(ProductChannel::className(), ['channel_id' => 'channel_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserChannels()
    {
        return $this->hasMany(UserChannel::className(), ['channel_id' => 'channel_ID']);
    }
     /**
     * @return \yii\db\ActiveQuery
     */
    public function getChannelconnections()
    {
        return $this->hasMany(ChannelConnection::className(), ['channel_id' => 'channel_ID']);
    }
    
}
