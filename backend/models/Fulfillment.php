<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "warehouse".
 *
 * @property integer $id
 * @property integer $elliot_user_id
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 */
class Fulfillment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'warehouse';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['elliot_user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'elliot_user_id' => 'Elliot User ID',
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
        /** 
     * @return \yii\db\ActiveQuery
     */
    public function getFulfillmentList()
    {
        return $this->hasOne(FulfillmentList::className(), ['id' => 'id']);
    }
}
 