<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "billing_invoice".
 *
 * @property integer $id
 * @property integer $elliot_user_id
 * @property string $customer_email
 * @property string $invoice_name
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 */
class BillingInvoice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'billing_invoice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['elliot_user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['customer_email', 'invoice_name', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'elliot_user_id' => 'Elliot User ID',
            'customer_email' => 'Customer Email',
            'invoice_name' => 'Invoice Name',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
