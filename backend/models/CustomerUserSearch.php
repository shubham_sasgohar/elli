<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CustomerUser;

/**
 * CustomerUserSerach represents the model behind the search form about `backend\models\CustomerUser`.
 */
class CustomerUserSearch extends CustomerUser
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_ID'], 'integer'],
            [['first_name', 'last_name', 'email', 'DOB','img','gender', 'channel_acquired', 'date_acquired', 'phone_number', 'billing_address', 'shipping_address', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomerUser::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1'); 
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'customer_ID' => $this->customer_ID,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'DOB', $this->DOB])
           // ->andFilterWhere(['like', 'phoneno', $this->phoneno])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'img', $this->img])
            ->andFilterWhere(['like', 'channel_acquired', $this->channel_acquired])
            ->andFilterWhere(['like', 'date_acquired', $this->date_acquired])
            ->andFilterWhere(['like', 'phone_number', $this->phone_number])
            ->andFilterWhere(['like', 'billing_address', $this->billing_address])
            ->andFilterWhere(['like', 'shipping_address', $this->shipping_address]);

        return $dataProvider;
    }
}
