<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "variations_set".
 *
 * @property integer $variations_set_id
 * @property string $channel_abb_id
 * @property integer $elliot_user_id
 * @property string $variations_set_name
 * @property string $variations_set_options_ids
 * @property string $created_at
 * @property string $updated_at
 */
class VariationsSet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'variations_set';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['elliot_user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['channel_abb_id', 'variations_set_name', 'variations_set_options_ids'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'variations_set_id' => 'Variations Set ID',
            'channel_abb_id' => 'Channel Abb ID',
            'elliot_user_id' => 'Elliot User ID',
            'variations_set_name' => 'Variations Set Name',
            'variations_set_options_ids' => 'Variations Set Options Ids',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
