<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "product_abbrivation".
 *
 * @property integer $id
 * @property integer $channel_abb_id
 * @property integer $product_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $price
 * @property string $salePrice
 * @property string $channel_accquired
 *
 * @property Products $product
 */
class ProductAbbrivation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_abbrivation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['channel_abb_id', 'product_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['price', 'salePrice', 'channel_accquired'], 'string', 'max' => 255],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'channel_abb_id' => 'Channel Abb ID',
            'product_id' => 'Product ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'price' => 'Price',
            'salePrice' => 'Sale Price',
            'channel_accquired' => 'Channel Accquired',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }
    
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductChannel()
    {
        return $this->hasMany(ProductChannel::className(), ['product_id' => 'product_id']);
    }
}
