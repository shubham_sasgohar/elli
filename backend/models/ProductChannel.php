<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;
use backend\models\Channels;
use backend\models\Stores;
use backend\models\StoresConnection;

/**
 * This is the model class for table "product_channel".
 *
 * @property integer $product_channel_id
 * @property integer $channel_id
 * @property integer $product_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Channels $channel
 * @property Products $product
 */
class ProductChannel extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'product_channel';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['channel_id', 'product_id'], 'integer'],
                [['created_at', 'updated_at'], 'safe'],
                [['channel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Channels::className(), 'targetAttribute' => ['channel_id' => 'channel_ID']],
                [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'product_channel_id' => 'Product Channel ID',
            'channel_id' => 'Channel ID',
            'product_id' => 'Product ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChannel() {
        return $this->hasOne(Channels::className(), ['channel_ID' => 'channel_id']);
    }

    public function getStores() {
        return $this->hasOne(Stores::className(), ['store_id' => 'store_id']);
    }
    
    public function getChannelConnection() {
        return $this->hasOne(ChannelConnection::className(), ['channel_connection_id' => 'channel_id']);
    }

    public function getStoresConnection() {
        return $this->hasOne(StoresConnection::className(), ['stores_connection_id' => 'store_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct() {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }

    public static function getProductChannelImage($product_id) {
        $records = self::find()->where(['product_id' => $product_id,'status'=>'yes'])->All();
        $channelImageArray = array();        
        foreach ($records as $record):
            if ($record->channel_id == NULL):
                 $mulstoreId = $record->store_id;
    
                /*get store connection Data*/
                $StoresConnectionData = StoresConnection::find()->Where(['stores_connection_id'=>$mulstoreId])->one();
                $storeId=$StoresConnectionData->store_id;
                
                $storedata = Stores::find()->select(['store_image','store_name'])->where(['store_id' => $storeId])->One();
                $storeImage = $storedata->store_image;
                $storename = $storedata->store_name;
//                array_push($channelImageArray, $storeImage);
                if($storename=='ShopifyPlus'){
                    $storeImage = 'img/shopifyplus-listing.png';
                }
                $channelImageArray[$storename]=$storeImage;
            else:
                $chId = $record->channel_id;
                $chImage = Channels::find()->select(['channel_image','parent_name'])->where(['channel_ID' => $chId])->One();
                 $chnl_name=str_replace("channel_","",$chImage->parent_name);
//                $chImage =  $chImage->parent_name=="channel_WeChat"?'img/marketplace_logos/elliot-wechat-connection-icon.png':$chImage->channel_image;
              
                if($chImage->parent_name=="channel_WeChat"){
                     $channelImageArray[$chnl_name] =  'img/marketplace_logos/elliot-wechat-connection-icon.png';
                }elseif($chImage->parent_name=="channel_Facebook"){
                       $channelImageArray[$chnl_name] =  'img/marketplace_logos/fb-icon.png';
                }elseif($chImage->parent_name=="channel_google-shopping"){
                      $channelImageArray[$chnl_name] =  'img/marketplace_logos/g-icon.png';
                }elseif($chImage->parent_name=="channel_Square"){
                     $channelImageArray[$chnl_name] =  'img/square_logo_grid.png';
                }
                elseif($chImage->parent_name=="channel_Lazada"){
                     $channelImageArray[$chnl_name] =  'img/marketplace_logos/lazada-listing-logo.png';
                }
                elseif($chImage->parent_name=="channel_TMall"){
                     $channelImageArray[$chnl_name] =  'img/marketplace_logos/tmall-icon-logo.png';
                }
                else{
                       $channelImageArray[$chnl_name] =  $chImage->channel_image;
                } 
               // array_push($channelImageArray, $chImage);
            endif;
        endforeach;
        
        return array_unique($channelImageArray);
    }


    public static function getHiddenProductChannelImage($product_id) {
        $records = self::find()->where(['product_id' => $product_id,'status'=>'no'])->All();
        $channelImageArray = array();
         foreach ($records as $record):
            if ($record->channel_id == NULL):
                $mulstoreId = $record->store_id;
                /*get store connection Data*/
                $StoresConnectionData=StoresConnection::find()->Where(['stores_connection_id'=>$mulstoreId])->one();
                $storeId=$StoresConnectionData->store_id;
                
                $storedata = Stores::find()->select(['store_image','store_name'])->where(['store_id' => $storeId])->One();
                $storeImage = $storedata->store_image;
                $storename = $storedata->store_name;
//                array_push($channelImageArray, $storeImage);
				if($storename=='ShopifyPlus'){
                    $storeImage = 'img/shopifyplus-listing.png';
                }
                $channelImageArray[$storename]=$storeImage;
            else:
                $chId = $record->channel_id;
                $chImage = Channels::find()->select(['channel_image','parent_name'])->where(['channel_ID' => $chId])->One();
                 $chnl_name=str_replace("channel_","",$chImage->parent_name);
//                $chImage =  $chImage->parent_name=="channel_WeChat"?'img/marketplace_logos/elliot-wechat-connection-icon.png':$chImage->channel_image;
              
                if($chImage->parent_name=="channel_WeChat"){
                     $channelImageArray[$chnl_name] =  'img/marketplace_logos/elliot-wechat-connection-icon.png';
                }elseif($chImage->parent_name=="channel_Facebook"){
                       $channelImageArray[$chnl_name] =  'img/marketplace_logos/fb-icon.png';
                }elseif($chImage->parent_name=="channel_google-shopping"){
                      $channelImageArray[$chnl_name] =  'img/marketplace_logos/g-icon.png';
                }elseif($chImage->parent_name=="channel_Square"){
                     $channelImageArray[$chnl_name] =  'img/square_logo_grid.png';
                }
                elseif($chImage->parent_name=="channel_Lazada"){
                     $channelImageArray[$chnl_name] =  'img/marketplace_logos/lazada-listing-logo.png';
                }
                elseif($chImage->parent_name=="channel_TMall"){
                     $channelImageArray[$chnl_name] =  'img/marketplace_logos/tmall-icon-logo.png';
                }
                else{
                       $channelImageArray[$chnl_name] =  $chImage->channel_image;
                } 
               // array_push($channelImageArray, $chImage);
            endif;
        endforeach;
        
        return array_unique($channelImageArray);

    }
}
