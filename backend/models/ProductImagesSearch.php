<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\ProductImages;

/**
 * ProductImagesSearch represents the model behind the search form about `backend\models\ProductImages`.
 */
class ProductImagesSearch extends ProductImages
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image_ID', 'product_ID', 'image_status'], 'integer'],
            [['label', 'link', 'tag_status', 'tag', 'priority', 'default_image', 'alternative_image', 'html_video_link', '_360_degree_video_link', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductImages::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'image_ID' => $this->image_ID,
            'product_ID' => $this->product_ID,
            'image_status' => $this->image_status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'link', $this->link])
            ->andFilterWhere(['like', 'tag_status', $this->tag_status])
            ->andFilterWhere(['like', 'tag', $this->tag])
            ->andFilterWhere(['like', 'priority', $this->priority])
            ->andFilterWhere(['like', 'default_image', $this->default_image])
            ->andFilterWhere(['like', 'alternative_image', $this->alternative_image])
            ->andFilterWhere(['like', 'html_video_link', $this->html_video_link])
            ->andFilterWhere(['like', '_360_degree_video_link', $this->_360_degree_video_link]);

        return $dataProvider;
    }
}
