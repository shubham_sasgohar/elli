<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "channel_connection".
 *
 * @property integer $channel_connection_id
 * @property integer $channel_id
 * @property integer $user_id
 * @property string $connected
 * @property string $username
 * @property string $password
 * @property string $token
 * @property string $wechat_project_id
 * @property string $created
 * @property string $updated
 */
class ChannelConnection extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'channel_connection';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['channel_id', 'user_id'], 'required'],
                [['channel_id', 'user_id'], 'integer'],
                [['connected'], 'string'],
                [['created', 'updated'], 'safe'],
                [['username', 'password', 'token', 'wechat_project_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'channel_connection_id' => 'Channel Connection ID',
            'channel_id' => 'Channel ID',
            'user_id' => 'User ID',
            'connected' => 'Connected',
            'username' => 'Username',
            'password' => 'Password',
            'token' => 'Token',
            'wechat_project_id' => 'Wechat Project ID',
            'created' => 'Created',
            'updated' => 'Updated',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChannels() {
        return $this->hasMany(Channels::className(), ['channel_ID' => 'channel_id']);
    }

    public function getStoresDetails() {
        return $this->hasOne(StoreDetails::className(), ['channel_connection_id' => 'channel_connection_id']);
    }

}
