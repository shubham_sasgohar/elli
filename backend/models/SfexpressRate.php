<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "sfexpress_rate".
 *
 * @property integer $id
 * @property integer $weight
 * @property double $hongkong
 * @property double $macau
 * @property double $taiwan
 * @property double $mainlandchina
 * @property double $singapore
 * @property double $malaysia
 * @property double $japan
 * @property double $southkorea
 */
class SfexpressRate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sfexpress_rate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['weight', 'hongkong', 'macau', 'taiwan', 'mainlandchina', 'singapore', 'malaysia', 'japan', 'southkorea'], 'required'],
            [['weight'], 'integer'],
            [['hongkong', 'macau', 'taiwan', 'mainlandchina', 'singapore', 'malaysia', 'japan', 'southkorea'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'weight' => 'Weight',
            'hongkong' => 'Hongkong',
            'macau' => 'Macau',
            'taiwan' => 'Taiwan',
            'mainlandchina' => 'Mainlandchina',
            'singapore' => 'Singapore',
            'malaysia' => 'Malaysia',
            'japan' => 'Japan',
            'southkorea' => 'Southkorea',
        ];
    }
}
