<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "Product_images".
 *
 * @property integer $image_ID
 * @property integer $product_ID
 * @property string $label
 * @property string $link
 * @property string $tag_status
 * @property string $tag
 * @property string $priority
 * @property string $default_image
 * @property string $alternative_image
 * @property string $html_video_link
 * @property string $_360_degree_video_link
 * @property integer $image_status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Products $product
 */
class ProductImages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Product_images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_ID', 'image_status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['label', 'link', 'tag_status', 'tag', 'priority', 'default_image', 'alternative_image', 'html_video_link', '_360_degree_video_link'], 'string', 'max' => 255],
            [['product_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_ID' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'image_ID' => 'Image  ID',
            'product_ID' => 'Product  ID',
            'label' => 'Label',
            'link' => 'Link',
            'tag_status' => 'Tag Status',
            'tag' => 'Tag',
            'priority' => 'Priority',
            'default_image' => 'Default Image',
            'alternative_image' => 'Alternative Image',
            'html_video_link' => 'Html Video Link',
            '_360_degree_video_link' => '360 Degree Video Link',
            'image_status' => 'Image Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_ID']);
    }
}
