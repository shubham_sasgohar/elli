<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "cron_tasks".
 *
 * @property integer $task_id
 * @property integer $elliot_user_id
 * @property string $task_name
 * @property string $task_source
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 */
class CronTasks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cron_tasks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['elliot_user_id'], 'required'],
            [['elliot_user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['task_name', 'task_source', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'task_id' => 'Task ID',
            'elliot_user_id' => 'Elliot User ID',
            'task_name' => 'Task Name',
            'task_source' => 'Task Source',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
