<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "order_channel".
 *
 * @property integer $order_channel_id
 * @property integer $order_id
 * @property integer $store_id
 * @property integer $channel_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Orders $order
 * @property Channels $channel
 */
class OrderChannel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_channel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'channel_id', 'created_at'], 'required'],
            [['order_id', 'store_id', 'channel_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'order_ID']],
            [['channel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Channels::className(), 'targetAttribute' => ['channel_id' => 'channel_ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_channel_id' => 'Order Channel ID',
            'order_id' => 'Order ID',
            'store_id' => 'Store ID',
            'channel_id' => 'Channel ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['order_ID' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChannel()
    {
        return $this->hasOne(Channels::className(), ['channel_ID' => 'channel_id']);
    }
    
     public function getStores()
    {
        return $this->hasOne(Stores::className(), ['store_id' => 'store_id']);
    }
}
