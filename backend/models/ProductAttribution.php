<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "product_attribution".
 *
 * @property integer $product_attribution_id
 * @property integer $attribution_id
 * @property integer $product_id
 * @property string $item_name
 * @property string $item_value
 * @property string $store_attribution_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Attribution $attribution
 * @property Products $product
 */
class ProductAttribution extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_attribution';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['attribution_id', 'product_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['item_name', 'item_value', 'store_attribution_id'], 'string', 'max' => 255],
            [['attribution_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attribution::className(), 'targetAttribute' => ['attribution_id' => 'attribution_id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_attribution_id' => 'Product Attribution ID',
            'attribution_id' => 'Attribution ID',
            'product_id' => 'Product ID',
            'item_name' => 'Item Name',
            'item_value' => 'Item Value',
            'store_attribution_id' => 'Store Attribution ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttribution()
    {
        return $this->hasOne(Attribution::className(), ['attribution_id' => 'attribution_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }
}
