<?php

namespace backend\models;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

use Yii;

/**
 * This is the model class for table "shipstation".
 *
 * @property integer $id
 * @property integer $elliot_user_id
 * @property string $connected
 * @property string $created_at
 * @property string $updated_at
 */
class Shipstation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shipstation';
    }
    
      public function behaviors()
    {

        return [                                                                                                                                                                                                                                                                                                                                            
            'timestamp' => [
                
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    self::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => function () {
                return new Expression('CURRENT_TIMESTAMP');
                }
            ],
        ];
            
       
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['elliot_user_id'], 'integer'],
            [['connected'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'elliot_user_id' => 'Elliot User ID',
            'connected' => 'Connected',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
