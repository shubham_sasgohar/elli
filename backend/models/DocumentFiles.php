<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "document_info".
 *
 * @property integer $id
 * @property string $user_id
 * @property string $channel_id
 * @property string $banking_account_no
 * @property string $banking_routing_no
 * @property string $banking_bank_code
 * @property string $banking_bank_name
 * @property string $banking_bank_address
 * @property string $banking_swift
 * @property string $banking_account_type
 * @property integer $business_tax_id
 * @property string $created_at
 * @property string $updated_at
 */
class DocumentFiles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'document_files';
    }

}