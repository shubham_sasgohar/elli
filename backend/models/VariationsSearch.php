<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Variations;

/**
 * VariationsSearch represents the model behind the search form about `backend\models\Variations`.
 */
class VariationsSearch extends Variations
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['variations_ID'], 'integer'],
            [['variation_name', 'variation_description', 'created_at', 'updated_at', 'variation_type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Variations::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'variations_ID' => $this->variations_ID,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'variation_name', $this->variation_name])
            ->andFilterWhere(['like', 'variation_description', $this->variation_description])
            ->andFilterWhere(['like', 'variation_type', $this->variation_type]);

        return $dataProvider;
    }
}
