<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "attribution_item_list".
 *
 * @property integer $attribution_item_list_id
 * @property integer $elliot_user_id
 * @property integer $attribution_id
 * @property string $item_name
 * @property string $item_value
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Attribution $attribution
 */
class AttributionItemList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attribution_item_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['elliot_user_id', 'attribution_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['item_name'], 'string', 'max' => 45],
            [['item_value'], 'string', 'max' => 255],
            [['attribution_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attribution::className(), 'targetAttribute' => ['attribution_id' => 'attribution_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'attribution_item_list_id' => 'Attribution Item List ID',
            'elliot_user_id' => 'Elliot User ID',
            'attribution_id' => 'Attribution ID',
            'item_name' => 'Item Name',
            'item_value' => 'Item Value',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttribution()
    {
        return $this->hasOne(Attribution::className(), ['attribution_id' => 'attribution_id']);
    }
}
