<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "customer_user".
 *
 * @property integer $customer_ID
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $DOB
 * @property string $channel_acquired
 * @property string $date_acquired
 * @property string $phone_number
 * @property string $billing_address
 * @property string $shipping_address
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Orders[] $orders
 */
class CustomerUser extends \yii\db\ActiveRecord {

    public $customer_id;
    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'Customer_User';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['created_at', 'updated_at'], 'safe'],
                [['first_name', 'last_name', 'email','img', 'DOB','gender', 'street_1', 'street_2', 'city', 'state', 'zip', 'country','ship_street_1', 'ship_street_2', 'ship_city', 'ship_state', 'ship_zip', 'ship_country', 'address_type', 'channel_acquired', 'date_acquired', 'phone_number', 'billing_address', 'shipping_address'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'customer_ID' => 'Customer  ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'DOB' => 'DOB',
            'gender' => 'Gender',
           // 'phoneno' => 'PhoneNo',
            'img' => 'Image',
            'street_1' => 'street_1',
            'street_2' => 'street_2',
            'city' => 'city',
            'state' => 'state',
            'zip' => 'zip',
            'country' => 'country',
            'address_type' => 'address_type',
            'channel_acquired' => 'Channel Acquired',
            'date_acquired' => 'Date Acquired',
            'phone_number' => 'Phone Number', 
            'billing_address' => 'Billing Address',
            'shipping_address' => 'Shipping Address',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders() {
        return $this->hasMany(Orders::className(), ['customer_id' => 'customer_ID']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerabbrivation() {
        return $this->hasMany(CustomerAbbrivation::className(), ['customer_id' => 'customer_ID']);
    }

}
  