<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "variations_item_list".
 *
 * @property integer $variations_item_list_id
 * @property integer $variation_id
 * @property string $item_name
 * @property string $item_value
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Variations $variation
 */
class VariationsItemList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'variations_item_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['variation_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['item_name'], 'string', 'max' => 45],
            [['item_value'], 'string', 'max' => 255],
            [['variation_id'], 'exist', 'skipOnError' => true, 'targetClass' => Variations::className(), 'targetAttribute' => ['variation_id' => 'variations_ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'variations_item_list_id' => 'Variations Item List ID',
            'variation_id' => 'Variation ID',
            'item_name' => 'Item Name',
            'item_value' => 'Item Value',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVariation()
    {
        return $this->hasOne(Variations::className(), ['variations_ID' => 'variation_id']);
    }
}
