<?php

namespace backend\models;

use Yii;
use backend\models\CustomerUser;
use backend\models\OrdersProducts;
/**
 * This is the model class for table "orders".
 *
 * @property integer $order_ID
 * @property integer $customer_id
 * @property string $stripe_transaction_ID
 * @property string $order_status
 * @property string $product_qauntity
 * @property string $shipping_address
 * @property string $billing_address
 * @property string $brand_logo_image
 * @property string $tracking_link
 * @property integer $market_place_fees
 * @property integer $credit_card_fees
 * @property integer $processing_fees
 * @property integer $total_amount
 * @property string $created_at
 * @property string $updated_at
 *
 * @property OrderChannel[] $orderChannels
 * @property CustomerUser $customer
 * @property OrdersProducts[] $ordersProducts
 */
class Orders extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'orders';
    }
public $items;
public $customerDetails;
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['customer_id'], 'required'],
            [['customer_id', 'market_place_fees', 'credit_card_fees', 'processing_fees', 'total_amount'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['stripe_transaction_ID', 'order_status', 'product_qauntity', 'shipping_address', 'billing_address', 'brand_logo_image', 'tracking_link'], 'string', 'max' => 255],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustomerUser::className(), 'targetAttribute' => ['customer_id' => 'customer_ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'order_ID' => 'Order  ID',
            'customer_id' => 'Customer ID',
            'stripe_transaction_ID' => 'Stripe Transaction  ID',
            'order_status' => 'Order Status',
            'product_qauntity' => 'Product Qauntity',
            'shipping_address' => 'Shipping Address',
            'billing_address' => 'Billing Address',
            'brand_logo_image' => 'Brand Logo Image',
            'tracking_link' => 'Tracking Link',
            'market_place_fees' => 'Market Place Fees',
            'credit_card_fees' => 'Credit Card Fees',
            'processing_fees' => 'Processing Fees',
            'total_amount' => 'Total Amount',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderChannels() {
        return $this->hasMany(OrderChannel::className(), ['order_id' => 'order_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer() {
        return $this->hasOne(CustomerUser::className(), ['customer_ID' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersProducts() {
        return $this->hasMany(OrdersProducts::className(), ['order_Id' => 'order_ID']);
    }

//    public function afterSave($insert, $changedAttributes) {
//        $array4 = array();
//        $OrderDATA = Orders::find()->where(['order_ID' => $this->order_ID])->one();
//        $ordersProducts = OrdersProducts::find()->where(['order_Id' => $this->order_ID])->all();
//        $array2 = $ordersProducts;
//        $customerDATA = CustomerUser::find()->where(['customer_ID' => $this->customer_id])->one();
//        $array4 = array('order'=>$OrderDATA, 'items'=>$ordersProducts, 'customerDetails'=>$customerDATA);
//        $data = \yii\helpers\Json::encode($array4);
//    }

}
