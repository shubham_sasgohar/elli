<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "categories".
 *
 * @property integer $category_ID
 * @property string $category_name
 * @property integer $parent_category_ID
 * @property string $created_at
 * @property string $updated_at
 *
 * @property ProductCategories[] $productCategories
 */
class Categories extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'categories';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['parent_category_ID'], 'integer'],
                [['created_at', 'updated_at'], 'safe'],
                [['category_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'category_ID' => 'Category  ID',
            'category_name' => 'Category Name',
            'parent_category_ID' => 'Parent Category  ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategories() {
        return $this->hasMany(ProductCategories::className(), ['category_ID' => 'category_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryAbbrivation() {
        return $this->hasMany(CategoryAbbrivation::className(), ['category_ID' => 'category_ID']);
    }

}
 