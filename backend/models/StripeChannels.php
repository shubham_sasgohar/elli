<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "stripe_channels".
 *
 * @property integer $id
 * @property string $stripe_Channel_id
 * @property string $amount
 * @property string $currency
 * @property string $image_url
 * @property string $name
 * @property integer $trial_period_days
 * @property string $created_at
 * @property string $update_at
 */
class StripeChannels extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stripe_channels';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trial_period_days'], 'integer'],
            [['created_at', 'update_at'], 'safe'],
            [['stripe_Channel_id', 'amount', 'currency', 'name'], 'string', 'max' => 255],
            [['image_url'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'stripe_Channel_id' => 'Stripe  Channel ID',
            'amount' => 'Amount',
            'currency' => 'Currency',
            'image_url' => 'Image Url',
            'name' => 'Name',
            'trial_period_days' => 'Trial Period Days',
            'created_at' => 'Created At',
            'update_at' => 'Update At',
        ];
    }
}
