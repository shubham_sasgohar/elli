<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "businesstools".
 *
 * @property integer $id
 * @property string $name
 * @property integer $user_id
 * @property string $key_name
 * @property string $value
 * @property string $created_at
 * @property string $updated_at
 */
class Integrations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'integrations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'key_name', 'value'], 'required'],
            [['user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'key_name', 'value'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'user_id' => 'User ID',
            'key_name' => 'Key Name',
            'value' => 'Value',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
