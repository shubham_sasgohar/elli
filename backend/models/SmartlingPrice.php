<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "smartling_price".
 *
 * @property integer $id
 * @property string $target_language
 * @property string $locale_id
 * @property string $editing
 * @property string $rate1
 * @property string $rate2
 * @property string $post_edit
 * @property string $created_at
 * @property string $updated_at
 */
class SmartlingPrice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'smartling_price';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['target_language', 'locale_id', 'editing', 'rate1', 'rate2', 'post_edit'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'target_language' => 'Target Language',
            'locale_id' => 'Locale ID',
            'editing' => 'Editing',
            'rate1' => 'Rate1',
            'rate2' => 'Rate2',
            'post_edit' => 'Post Edit',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
