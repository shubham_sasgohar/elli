<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "magento_stores".
 *
 * @property integer $id
 * @property integer $elliot_user_id
 * @property integer $magento_store_id
 * @property string $magento_store_name
 * @property integer $magento_store_is_active
 * @property string $created_at
 * @property string $updated_at
 */
class MagentoStores extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'magento_stores';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['elliot_user_id', 'magento_store_id', 'magento_store_is_active'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['magento_store_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'elliot_user_id' => 'Elliot User ID',
            'magento_store_id' => 'Magento Store ID',
            'magento_store_name' => 'Magento Store Name',
            'magento_store_is_active' => 'Magento Store Is Active',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
