<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "stores_connection".
 *
 * @property integer $stores_connection_id
 * @property integer $store_id
 * @property integer $user_id
 * @property string $connected
 * @property string $big_api_path
 * @property string $big_access_token
 * @property string $big_client_id
 * @property string $big_client_secret
 * @property string $big_store_hash
 * @property string $created
 * @property string $updated
 */
class StoresConnection extends \yii\db\ActiveRecord {

  /**
   * @inheritdoc
   */
  public static function tableName() {
    return 'stores_connection';
  }

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
      [['store_id', 'user_id'], 'integer'],
      [['connected'], 'string'],
      [['created', 'updated'], 'safe'],
      [['big_api_path', 'big_access_token', 'big_client_id', 'big_client_secret', 'big_store_hash', 'import_status'], 'string', 'max' => 255],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
      'stores_connection_id' => 'Stores Connection ID',
      'store_id' => 'Store ID',
      'user_id' => 'User ID',
      'connected' => 'Connected',
      'big_api_path' => 'Big Api Path',
      'big_access_token' => 'Big Access Token',
      'big_client_id' => 'Big Client ID',
      'big_client_secret' => 'Big Client Secret',
      'big_store_hash' => 'Big Store Hash',
      'import_status' => 'Import Status',
      'created' => 'Created',
      'updated' => 'Updated',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getStores() {
    return $this->hasOne(Stores::className(), ['store_id' => 'store_id']);
  }

  public function getStoresDetails() {
    return $this->hasOne(StoreDetails::className(), ['store_connection_id' => 'stores_connection_id']);
  }

}
