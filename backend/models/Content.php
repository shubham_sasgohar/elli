<?php

namespace backend\models;
use Yii;

class Content extends \yii\db\ActiveRecord {
    
     public static function tableName()
    {
        return 'content';
    }
    
    public function rules() {
        return [
                [['page_title'], 'required'],
                [['created_at', 'updated_at'], 'safe'] ,
                [['page_title','page_description'],'string','max' => 255]
        ];
    }
    
    public function attributeLabels() {
        return [
            'page_ID' => 'Page ID',
            'page_title' => 'Page Name',
            'page_description' => 'Page Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At'
        ];
    }
}

?>