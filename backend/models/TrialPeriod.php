<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "trial_period".
 *
 * @property integer $trial_id
 * @property integer $trial_days
 * @property string $created_at
 * @property string $updated_at
 */
class TrialPeriod extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'trial_period';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['trial_days', 'created_at', 'updated_at'], 'required'],
            [['trial_days'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'trial_id' => 'Trial ID',
            'trial_days' => 'Trial Days',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
