<?php

namespace backend\models;

use Yii;

class DocumentDirectors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'document_directors';
    }
}