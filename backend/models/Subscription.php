<?php

namespace backend\models;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use Yii;

/**
 * This is the model class for table "subscription".
 *
 * @property integer $id
 * @property string $subscription_name
 * @property double $amount
 * @property integer $max_merchant_user
 * @property string $created_at
 * @property string $updated_at
 *
 * @property AdminSales[] $adminSales
 * @property User[] $users
 */
class Subscription extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscription';
    }
    /**
     * @inheritdoc
     */
 
        public function behaviors()
          {
              return [                                                                                                                                                                                                                                                                                                                                            
                  'timestamp' => [

                      'class' => 'yii\behaviors\TimestampBehavior',
                      'attributes' => [
                          self::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                          self::EVENT_BEFORE_UPDATE => 'updated_at',
                      ],
                      'value' => function () {
                      return new Expression('CURRENT_TIMESTAMP');
                      }
                  ],
              ];
          }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subscription_name', 'amount', 'max_merchant_user'], 'required'],
            [['amount'], 'number'],
            [['max_merchant_user'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['subscription_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subscription_name' => 'Subscription Name',
            'amount' => 'Amount',
            'max_merchant_user' => 'Max Merchant User',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdminSales()
    {
        return $this->hasMany(AdminSales::className(), ['subscription_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['current_subscription_id' => 'id']);
    }
}
