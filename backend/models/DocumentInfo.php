<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "document_info".
 *
 * @property integer $id
 * @property string $user_id
 * @property string $channel_id
 * @property string $banking_account_no
 * @property string $banking_routing_no
 * @property string $banking_bank_code
 * @property string $banking_bank_name
 * @property string $banking_bank_address
 * @property string $banking_swift
 * @property string $banking_account_type
 * @property integer $business_tax_id
 * @property string $created_at
 * @property string $updated_at
 */
class DocumentInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'document_info';
    }

    /**
     * @inheritdoc
     */

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'channel_id' => 'Channel ID',
            'banking_account_no' => 'Banking Account No',
            'banking_routing_no' => 'Banking Routing No',
            'banking_bank_code' => 'Banking Bank Code',
            'banking_bank_name' => 'Banking Bank Name',
            'banking_bank_address' => 'Banking Bank Address',
            'banking_swift' => 'Banking Swift',
            'banking_account_type' => 'Banking Account Type',
            'business_tax_id' => 'Business Tax ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}