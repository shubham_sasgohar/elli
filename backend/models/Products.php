<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property integer $id
 * @property string $product_name
 * @property string $SKU
 * @property string $UPC
 * @property string $EAN
 * @property string $Jan
 * @property string $ISBN
 * @property string $MPN
 * @property string $description
 * @property string $adult
 * @property string $age_group
 * @property string $availability
 * @property string $brand
 * @property string $condition
 * @property string $gender
 * @property string $weight
 * @property string $stock_quantity
 * @property string $stock_level
 * @property string $stock_status
 * @property integer $low_stock_notification
 * @property string $price
 * @property string $sales_price
 * @property string $schedules_sales_date
 * @property string $created_at
 * @property string $updated_at
 * @property string $occasion
 * @property string $weather
 *
 * @property ProductImages[] $productImages
 * @property MerchantProducts[] $merchantProducts
 * @property OrdersProducts[] $ordersProducts
 * @property ProductCategories[] $productCategories
 * @property ProductChannel[] $productChannels
 * @property ProductVariation[] $productVariations
 */
class Products extends \yii\db\ActiveRecord {

  /**
   * @inheritdoc
   */
  public static function tableName() {
    return 'products';
  }

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
      [['product_name', 'SKU', 'UPC', 'EAN', 'Jan', 'ISBN', 'MPN', 'brand'], 'required'],
      [['adult', 'age_group', 'availability', 'condition', 'gender', 'stock_level', 'stock_status'], 'string'],
      [['low_stock_notification'], 'integer'],
      [['weight','price','sales_price','schedules_sales_date', 'created_at', 'updated_at'], 'safe'],
      [['product_name', 'SKU', 'EAN', 'Jan', 'ISBN', 'MPN', 'description', 'brand', 'occasion', 'weather'], 'string', 'max' => 255],
      [['weight', 'stock_quantity'], 'string', 'max' => 45],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
      'id' => 'ID',
      'product_name' => 'Product Name',
      'SKU' => 'SKU',
      'UPC' => 'UPC',
      'EAN' => 'EAN',
      'Jan' => 'JAN',
      'ISBN' => 'ISBN',
      'MPN' => 'MPN',
      'description' => 'Description',
      'adult' => 'Adult',
      'age_group' => 'Age Group',
      'availability' => 'Availability',
      'brand' => 'Brand',
      'condition' => 'Condition',
      'gender' => 'Gender',
      'weight' => 'Weight',
      'stock_quantity' => 'Stock Quantity',
      'stock_level' => 'Stock Level',
      'stock_status' => 'Stock Status',
      'low_stock_notification' => 'Low Stock Notification',
      'price' => 'Price',
      'sales_price' => 'Sales Price',
      'schedules_sales_date' => 'Schedules Sales Date',
      'created_at' => 'Created At',
      'updated_at' => 'Updated At',
      'occasion' => 'Occasion',
      'weather' => 'Weather',
        'product_status'=>'Product Status'
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getProductImages() {
    return $this->hasMany(ProductImages::className(), ['product_ID' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getMerchantProducts() {
    return $this->hasMany(MerchantProducts::className(), ['product_ID' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getOrdersProducts() {
    return $this->hasMany(OrdersProducts::className(), ['product_Id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getProductCategories() {
    return $this->hasMany(ProductCategories::className(), ['product_ID' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getProductChannels() {
    return $this->hasMany(ProductChannel::className(), ['product_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getProductVariations() {
    return $this->hasMany(ProductVariation::className(), ['product_id' => 'id']);
  }
  
  
  /**
   * @return \yii\db\ActiveQuery
   */
  public function getProductAbbrivation() {
    return $this->hasMany(ProductAbbrivation::className(), ['product_id' => 'id']);
  }

}
 