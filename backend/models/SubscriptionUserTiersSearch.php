<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\SubscriptionUserTiers;

/**
 * SubscriptionUserTiersSearch represents the model behind the search form about `backend\models\SubscriptionUserTiers`.
 */
class SubscriptionUserTiersSearch extends SubscriptionUserTiers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subscription_id', 'user_range'], 'integer'],
            [['plan_name', 'amount', 'created_at', 'updated_At'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SubscriptionUserTiers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'subscription_id' => $this->subscription_id,
            'user_range' => $this->user_range,
            'created_at' => $this->created_at,
            'updated_At' => $this->updated_At,
        ]);

        $query->andFilterWhere(['like', 'plan_name', $this->plan_name])
            ->andFilterWhere(['like', 'amount', $this->amount]);

        return $dataProvider;
    }
}
