<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\VariationsSet;

/**
 * VariationsSetSearch represents the model behind the search form about `backend\models\VariationsSet`.
 */
class VariationsSetSearch extends VariationsSet
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['variations_set_id', 'elliot_user_id'], 'integer'],
            [['channel_abb_id', 'variations_set_name', 'variations_set_options_ids', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VariationsSet::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'variations_set_id' => $this->variations_set_id,
            'elliot_user_id' => $this->elliot_user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'channel_abb_id', $this->channel_abb_id])
            ->andFilterWhere(['like', 'variations_set_name', $this->variations_set_name])
            ->andFilterWhere(['like', 'variations_set_options_ids', $this->variations_set_options_ids]);

        return $dataProvider;
    }
}
