<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "category_abbrivation".
 *
 * @property integer $id
 * @property string $channel_abb_id
 * @property integer $category_ID
 * @property string $store_category_groupid
 * @property string $channel_accquired
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Categories $category
 */
class CategoryAbbrivation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category_abbrivation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_ID'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['channel_abb_id', 'store_category_groupid', 'channel_accquired'], 'string', 'max' => 255],
            [['category_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['category_ID' => 'category_ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'channel_abb_id' => 'Channel Abb ID',
            'category_ID' => 'Category  ID',
            'store_category_groupid' => 'Store Category Groupid',
            'channel_accquired' => 'Channel Accquired',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['category_ID' => 'category_ID']);
    }
}
