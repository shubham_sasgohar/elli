<?php

namespace common\models;

use Yii;
use yii\base\Model;
use backend\models\TrialPeriod;

//$model = new \backend\models\User;
//$model->name = 'Users';

/**
 * CustomFunction
 */
class CustomFunction extends Model {
    /* define constant variable  */

    const trial_status_activate = 'activate';
    const trial_status_deactivate = 'deactivate';
    const plan_status_activate = 'activate';
    const plan_status_deactivate = 'deactivate';

    /**
     * @inheritdoc
     */
    public static function checkSubscriptionPlan() {
        /* get current user login id */
        $users_Id = Yii::$app->user->identity->id;

        /* get user data */
        $userdata = User::find()->where(['id' => $users_Id])->one();

        $trial_period_status = $userdata->trial_period_status;
        $plan_status = $userdata->plan_status;
        if ($trial_period_status == CustomFunction::trial_status_deactivate && $plan_status == CustomFunction::plan_status_activate) {
            return 'true';
        } elseif ($trial_period_status == CustomFunction::trial_status_deactivate && $plan_status == CustomFunction::plan_status_deactivate) {
            return 'false';
        } elseif ($trial_period_status == CustomFunction::trial_status_activate && $plan_status == CustomFunction::plan_status_activate) {
            $userdata->trial_period_status = 'deactivate';
            $userdata->save(false);
            return 'true';
        } else {
            return 'true';
        }
    }

    public static function createMainLocal($new_user_name) {

        $basedir = Yii::getAlias('@basedir');
        $db_name = $new_user_name . '_elliot';
        $new_folder_path = $basedir . '/common/users_config' . '/' . $new_user_name;

        if (is_dir($new_folder_path)) {
            // is_dir - tells whether the filename is a directory
            echo "This Folder is already exist";
            return false;
        } else {
            //mkdir - tells that need to create a directory
            mkdir($new_folder_path);

            $myfile = fopen($new_folder_path . '/main-local.php', "w") or die("Unable to open file!");
              $db_username=Yii::$app->params['DB_USERNAME'];
            $db_password=Yii::$app->params['DB_PASSWORD'];
            $data = "<?php\n"
                    . "return [\n"
                    . "'components' => [\n"
                    . "'db' => [\n"
                    . "'class' => 'yii\db\Connection',\n"
                    . "'dsn' => 'mysql:host=localhost;dbname=$db_name',\n"
                    . "'username' => '$db_username',\n"
                    . "'password' => '$db_password',\n"
                    . "'charset' => 'utf8',\n"
                    . "],\n"
                    . "'mailer' => [\n"
                    . "'class' => 'yii\swiftmailer\Mailer',\n"
                    . "'viewPath' => '@common/mail',\n"
                    . "// send all mails to a file by default. You have to set\n"
                    . "// 'useFileTransport' to false and configure a transport\n"
                    . "// for the mailer to send real emails.\n"
                    . "'useFileTransport' => true,\n"
                    . "],\n"
                    . "],\n"
                    . "];\n";
            fwrite($myfile, $data);
            fclose($myfile);

            return true;
        }
    }

    public static function createMerchantDomain($new_user_name) {

        $basedir = Yii::getAlias('@basedir');
        $domain_name = $new_user_name;
        $domain_folder_path = $basedir . '/subdomains' . '/' . $domain_name;
        $assets_folder_path = $basedir . '/subdomains' . '/' . $domain_name . '/' . 'assets';

        if (is_dir($domain_folder_path)) {
            // is_dir - tells whether the filename is a directory
            echo "This Domain is already exist";
            return false;
        } else {
            //mkdir - tells that need to create a directory
            mkdir($domain_folder_path);
            mkdir($assets_folder_path);
            $myfile = fopen($domain_folder_path . '/index.php', "w") or die("Unable to open file!");
            $data = "<?php\n"
                    . "defined('YII_DEBUG') or define('YII_DEBUG', true);\n"
                    . "defined('YII_ENV') or define('YII_ENV', 'dev');\n\n"
                    . "require(__DIR__ . '/../../vendor/autoload.php');\n"
                    . "require(__DIR__ . '/../../vendor/yiisoft/yii2/Yii.php');\n"
                    . "require(__DIR__ . '/../../common/config/bootstrap.php');\n"
                    . "require(__DIR__ . '/../../backend/config/bootstrap.php');\n\n"
                    . "\$config = yii\helpers\ArrayHelper::merge(\n"
                    . "require(__DIR__ . '/../../common/config/main.php'),\n"
                    . "require(__DIR__ . '/../../common/users_config/$new_user_name/main-local.php'),\n"
                    . "require(__DIR__ . '/../../backend/config/main.php'),\n"
                    . "require(__DIR__ . '/../../backend/config/main-local.php')\n"
                    . ");\n\n"
                    . "\$application = new yii\web\Application(\$config);\n"
                    . "\$application->run();\n";
            fwrite($myfile, $data);
            fclose($myfile);

            $myfile2 = fopen($domain_folder_path . '/.htaccess', "w") or die("Unable to open file!");
            $data2 = "RewriteEngine on\n"
                    . "# If a directory or a file exists, use it directly\n"
                    . "RewriteCond %{REQUEST_FILENAME} !-f\n"
                    . "RewriteCond %{REQUEST_FILENAME} !-d\n"
                    . "# Otherwise forward it to index.php\n"
                    . "RewriteRule . index.php\n";
            fwrite($myfile2, $data2);
            fclose($myfile2);

            return true;
        }
    }

    public static function createdb($user_name) {

        $servername = "localhost";
        $username = Yii::$app->params['DB_USERNAME'];
        $password = Yii::$app->params['DB_PASSWORD'];
        $db_name = $user_name . '_elliot';

        // Create connection
        $conn = mysqli_connect($servername, $username, $password);

        // Check connection
        if (!$conn) {
            die("Connection failed: " . mysqli_connect_error());
        }

        // Create database
        $sql = "CREATE DATABASE $db_name";

        if (mysqli_query($conn, $sql)) {
            return 'true';
        } else {
            echo "Error creating database: " . mysqli_error($conn);
            return 'false';
        }

        mysqli_close($conn);
    }

    public static function creat_db_tables($user_name) {
        $basedir = Yii::getAlias('@basedir');
        // Name of the file
        $filename = $basedir . '/sql/Elli.sql';
        $mysql_host = 'localhost';
        // MySQL username
        $mysql_username = Yii::$app->params['DB_USERNAME'];
        // MySQL password
        $mysql_password = Yii::$app->params['DB_PASSWORD'];
        // Database name
        $mysql_database = $user_name . '_elliot';

        // Connect to MySQL server
        $conn = mysqli_connect($mysql_host, $mysql_username, $mysql_password, $mysql_database) or die('Error connecting to MySQL server: ' . mysqli_error());
        // Select database
        //mysqli_select_db($mysql_database) or die('Error selecting MySQL database: ' . mysqli_error());
        // Temporary variable, used to store current query
        $templine = '';
        // Read in entire file
        $lines = file($filename);
        // Loop through each line
        foreach ($lines as $line) {
            // Skip it if it's a comment
            if (substr($line, 0, 2) == '--' || $line == '')
                continue;

            // Add this line to the current segment
            $templine .= $line;
            // If it has a semicolon at the end, it's the end of the query
            if (substr(trim($line), -1, 1) == ';') {
                // Perform the query
                mysqli_query($conn, $templine) or print('Error performing query \'<strong>' . $templine . '\': ' . mysql_error() . '<br /><br />');
                // Reset temp variable to empty
                $templine = '';
            }
        }
        // echo "Tables imported successfully";

        return true;
    }

    public static function createMerchantDBConfig($user_name, $new_user_email) {
        /* Main DB Connection Object */
        $connection1 = \Yii::$app->db;
        $sql_get = "SELECT * FROM user where email = '$new_user_email'";
        // To get from db
        $data1 = $connection1->createCommand($sql_get)->queryAll();
        foreach ($data1 as $user_data_row) {
            $keyarray = array_keys($user_data_row);
            $array = array_values($user_data_row);
            $keyarray = array();
            $valarray = array();
            foreach ($user_data_row as $key => $value) {
                if ($value != '') {
                    $keyarray[] = $key;
                    $valarray[] = $value;
                }
            }
            $sql_insert = "INSERT INTO user (" . implode(', ', $keyarray) . ") "
                    . "VALUES ('" . implode("', '", $valarray) . "')";
        }

        /* Merchant DB Config Object */
        $new_db_name = $user_name . '_elliot';
        $config = [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=' . $new_db_name . '',
            'username' =>Yii::$app->params['DB_USERNAME'],
            'password' => Yii::$app->params['DB_PASSWORD'],
            'charset' => 'utf8',
        ];

        $db_merchant = Yii::createObject($config);
        //insert the Merchant Details to Merchant Database
        $data2 = $db_merchant->createCommand($sql_insert)->query();

        return true;
    }

    //update password main database
    public static function update_pass_main_db($pass) {

        $new_user_email = Yii::$app->user->identity->email;

        /* Main DB Connection Object */
        $connection2 = \Yii::$app->db;

        $sql_get = "SELECT * FROM user where email = '$new_user_email'";


        // To get from db
        //$data1 = $connection2->createCommand($sql_get)->queryAll();
        $sql_insert = "UPDATE user SET password_hash ='$pass'  WHERE email = '$new_user_email'";


        /* Merchant DB Config Object */
        $config = [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=Elli',
            'username' => Yii::$app->params['DB_USERNAME'],
            'password' => Yii::$app->params['DB_PASSWORD'],
            'charset' => 'utf8',
        ];

        $db_merchant = Yii::createObject($config);
        //insert the Merchant Details to Merchant Database
        $data2 = $db_merchant->createCommand($sql_insert)->query();

        return true;
    }

    public static function getemaildomain($new_user_email) {

        $explode_email = substr(strrchr($new_user_email, "@"), 1);
        $explode_domain = explode('.', $explode_email);
        $new_domain = $explode_domain[0];

        return $new_domain;
    }

//Use For Send Welconme Mail for signup Time
    public static function SendWelcomeEmail($new_user_email) {

        Yii::$app->mailer->compose()
                ->setFrom('mail@elliot.com')
                ->setTo($new_user_email)
                ->setSubject('Welcome Mail')
//                    ->setTextBody('123456')
                ->setHtmlBody('<h2>Hi ' . $new_user_email . '<h2>'
                        . '<h3>Thanks for Signing up</h3>'
                        . '')
                ->send();
    }

    //Use For Send Mail for Connect BigCommerce store
    public static function ConnectBigCommerceEmail($email) {

        Yii::$app->mailer->compose()
                ->setFrom('mail@elliot.com')
                ->setTo($email)
                ->setSubject('Connect BigCommerce')
                ->setHtmlBody('<h2>Hi ' . $email . '<h2>'
                        . '<h3>Your BigCommerce Store has been connected.</h3>'
                        . '')
                ->send();
    }

}
