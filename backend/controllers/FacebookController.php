<?php

namespace backend\controllers;

ob_start();

//session_start();

use Yii;
use backend\models\CustomerUser;
use backend\models\User;
use backend\models\CustomerUserSearch;
use backend\models\Stores;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\Countries;
use backend\models\States;
use backend\models\Cities;
use backend\models\StoresConnection;
use Bigcommerce\Api\Client as Bigcommerce;
use backend\models\Orders;
use backend\models\OrdersProducts;
use backend\models\OrderChannel;
use backend\models\Products;
use backend\models\Categories;
use backend\models\ProductCategories;
use backend\models\ProductChannel;
use backend\models\ProductImages;
use Bigcommerce\Api\ShopifyClient as Shopify;
use backend\models\ProductVariation;
use backend\models\VariationsSet;
use backend\models\Variations;
use backend\models\VariationsItemList;
use backend\models\MerchantProducts;
use Automattic\WooCommerce\Client as Woocommerce;
use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use Facebook\FacebookRequestException;
use Facebook\FacebookRedirectLoginHelper;
use backend\models\ProductAbbrivation;
use backend\models\CurrencySymbols;
use backend\models\StoreDetails;

/**
 * CustomerUserController implements the CRUD actions for CustomerUser model.
 */
class FacebookController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['view', 'create', 'update', 'savecustomer', 'create-customer-hook', 'orderbigcommerce', 'orderhookcreate', 'producthooksbg', 'skuupdategb', 'productskuhooksbg', 'categoryhooksbg', 'shopify-product-create', 'shopify-product-update', 'shopify-product-delete', 'shopify-order-create', 'shopify-order-paid', 'shopify-order-update', 'shopify-customer-create', 'shopify-customer-update', 'sku', 'facebook', 'lazada', 'flipkart'],
                'rules' => [
                    [
                        'actions' => ['signup', 'create-customer-hook', 'orderbigcommerce', 'orderhookcreate', 'producthooksbg', 'skuupdategb', 'productskuhooksbg', 'categoryhooksbg', 'shopify-product-create', 'shopify-product-update', 'shopify-product-delete', 'shopify-order-create', 'shopify-order-paid', 'shopify-order-update', 'shopify-customer-create', 'shopify-customer-update', 'sku', 'index'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'index', 'view', 'create', 'update', 'savecustomer', 'create-customer-hook', 'orderbigcommerce', 'orderhookcreate', 'producthooksbg', 'skuupdategb', 'productskuhooksbg', 'categoryhooksbg', 'shopify-product-create', 'shopify-product-update', 'shopify-product-delete', 'shopify-order-create', 'shopify-order-paid', 'shopify-order-update', 'shopify-customer-create', 'shopify-customer-update', 'sku', 'facebook', 'lazada', 'flipkart'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CustomerUser models.
     * @return mixed
     */
    public function beforeAction($action) {

        $this->enableCsrfValidation = false;


        return parent::beforeAction($action);
    }

    public function actionIndex() {


        $curl = curl_init();
        $data = ['skuId' =>
            'HLTHTIPS',
            'attributeValues' => [
                'mrp' => 120,
                'selling_price' => 100,
                "procurement_type" => "REGULAR"
        ]];
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.flipkart.net/sellers/skus/listings/LSTRBKEUYKBQXBYHYHWFS4ZZ0",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
//  CURLOPT_POSTFIELDS => "{\r\n    \"skuId\": \"HLTHTIPS\",\r\n    \"attributeValues\": {\r\n        \"mrp\": 140,\r\n        \"selling_price\": 120,\r\n        \"procurement_type\": \"REGULAR\"\r\n    }\r\n}",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer 2f2c5d70-ec70-475f-98c7-1d5cdbf2a1bb",
                "cache-control: no-cache",
                "content-type: application/json",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }





        die;


//        print_R($_SERVER['DOCUMENT_ROOT']);die;
        $action = 'https://api.flipkart.net/oauth-service/oauth/token?grant_type=client_credentials&scope=Seller_Api';

        $headers = array();
        $headers[] = 'content-type: application/json';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $action);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
//        curl_setopt($curl, CURLOPT_USERPWD, "<id>:<key>");
        curl_setopt($curl, CURLOPT_USERPWD, '167932401b63770b6742a076b7ab367214216:acf1d29ab75a3c509d9c3ddd9741f154');
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        $result = curl_exec($curl);
        echo '<pre>';
        print_r($result);
        die;
        curl_close($curl);
        echo'<pre>';
//        print_R($_SERVER);die;
        require_once $_SERVER['DOCUMENT_ROOT'] . '/backend/web/Facebook/autoload.php';
//        require_once $_SERVER['DOCUMENT_ROOT'].'/backend/web/Facebook/src/Facebook/FacebookSession.php';

        FacebookSession::setDefaultApplication('1908857686030345', '46a386504e151f4155d25dab0eb5b25a');

// create a helper opject which is needed to create a login URL
// the $redirect_login_url is the page a visitor will come to after login
        $helper = new FacebookRedirectLoginHelper('https://s86.co/facebook');
//        print_r($_SESSION);DIE;
// First check if this is an existing PHP session
        if (isset($_SESSION) && isset($_SESSION['fb_token'])) {
            // create new session from the existing PHP sesson
            $session = new FacebookSession($_SESSION['fb_token']);
            try {
                // validate the access_token to make sure it's still valid
                if (!$session->validate())
                    $session = null;
            } catch (Exception $e) {
                // catch any exceptions and set the sesson null
                $session = null;
                echo 'No session: ' . $e->getMessage();
            }
        } elseif (empty($session)) {
            // the session is empty, we create a new one
            try {
                // the visitor is redirected from the login, let's pickup the session
                $session = $helper->getSessionFromRedirect();
            } catch (FacebookRequestException $e) {
                // Facebook has returned an error
                echo 'Facebook (session) request error: ' . $e->getMessage();
            } catch (Exception $e) {
                // Any other error
                echo 'Other (session) request error: ' . $e->getMessage();
            }
        }
        if (isset($session)) {
            // store the session token into a PHP session
            $_SESSION['fb_token'] = $session->getToken();
            // and create a new Facebook session using the cururent token 
            // or from the new token we got after login
            $session = new FacebookSession($session->getToken());
            try {
                // with this session I will post a message to my own timeline
//                $request = new FacebookRequest(
//                        $session, 'GET', '/me/accounts'
//                );
                $request = new FacebookRequest(
                        $session, 'GET', 'me/accounts'
                );
                $response = $request->execute();
//                echo'<pre>';                print_r($response);
//                die;
                $url = "https://graph.facebook.com/240955149743370/feed/?access_token=" . $_SESSION['fb_token'];

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
                $content = curl_exec($ch);
                $content = json_decode($content);
                var_dump($content);



//                die;
                $graphObject = $response->getGraphObject();
                print_r($graphObject);
                die;
                $msgid = $graphObject->getProperty('id');
            } catch (FacebookRequestException $e) {
                // show any error for this facebook request
                echo 'Facebook (post) request error: ' . $e->getMessage();
            }
        } else {
            // we need to create a new session, provide a login link
            echo 'No session, please <a href="' . $helper->getLoginUrl(array('publish_actions', 'manage_pages')) . '">login</a>.';
        }
    }

    public function actionFacebook() {
        return $this->render('facebook_info');
    }

    public function actionFlipkart() {

//        die;
        if (isset($_FILES) and ! empty($_FILES) and isset($_FILES["flipkart_csv"]) and ! empty($_FILES["flipkart_csv"]['size'])) {
            $path = $_FILES['flipkart_csv']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);

            if (in_array($ext, ['xls', 'xlsx'])) {

                $flipkart = \backend\models\Channels::find()->where(['channel_name' => 'Flipkart'])->one();
                $channel_id = $flipkart->channel_ID;

                $filename = $_FILES["flipkart_csv"]["tmp_name"];
                $name = time() . '_' . basename($_FILES["flipkart_csv"]["name"]);
                move_uploaded_file($filename, $_SERVER['DOCUMENT_ROOT'] . '/backend/web/uploads/' . $name);
                require_once $_SERVER['DOCUMENT_ROOT'] . '/backend/web/excel/PHPExcel.php';
                $inputfilename = $_SERVER['DOCUMENT_ROOT'] . '/backend/web/uploads/' . $name;
                $exceldata = array();

                try {
                    $inputfiletype = \PHPExcel_IOFactory::identify($inputfilename);
                    $objReader = \PHPExcel_IOFactory::createReader($inputfiletype);
                    $objPHPExcel = $objReader->load($inputfilename);
                } catch (Exception $e) {
                    die('Error loading file "' . pathinfo($inputfilename, PATHINFO_BASENAME) . '": ' . $e->getMessage());
                }
//  Get worksheet dimensions
                $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
//            print_r($allDataInSheet);
//            die;
                $user = Yii::$app->user->identity;
                if (isset($allDataInSheet) and ! empty($allDataInSheet)) {
//echo'<pre>';
//print_r($lazada);die;
                    $channel_id = $flipkart->channel_ID;
//echo $channel_id;die;
                    $checkConnection = \backend\models\ChannelConnection::find()->Where(['user_id' => Yii::$app->user->identity->id, 'channel_id' => $channel_id])->one();
                    if (empty($checkConnection)) {
                        $checkConnection = new \backend\models\ChannelConnection();
                        $checkConnection->channel_id = $channel_id;
                        $checkConnection->user_id = $user->id;
                        $checkConnection->elliot_user_id = $user->id;
                        $checkConnection->connected = 'yes';
                        $checkConnection->save(false);
                    }

                    $conversion_rate = 1;
                    if (isset($user->currency)) {
                        $username = Yii::$app->params['xe_account_id'];
                        $password = Yii::$app->params['xe_account_api_key'];
                        $URL = Yii::$app->params['xe_base_url'] . 'convert_from.json/?from=INR&to=USD&amount=1';

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $URL);
                        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
                        $result = curl_exec($ch);
                        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
                        curl_close($ch);
//echo'<pre>';
                        $result = json_decode($result, true);
                        if (isset($result) and ! empty($result) and isset($result['to']) and isset($result['to'][0]) and isset($result['to'][0]['quotecurrency'])) {
                            $conversion_rate = $result['to'][0]['mid'];
//                        $conversion_rate = number_format((float) $conversion_rate, 2, '.', '');
                        }
                    }
//echo $conversion_rate;echo'<br>';
                    foreach ($allDataInSheet as $key => $single) {
                        if (in_array($key, [1, 2]))
                            continue;
                        $checkProductModel = Products::find()->where(['product_name' => $single['D'], 'SKU' => $single['F']])->one();
                        $prefix_product_id = 'FPK' . @$single['B'];
                        if (empty($checkProductModel)):
//                            echo $single['G']*$conversion_rate;die;
                            if ($single['Y'] == 'instock') {
                                $single['Y'] = 'In Stock';
                            }
                            $productModel = new Products ();
                            $productModel->channel_abb_id = 'FPK' . @$single['B'];
                            $productModel->product_name = @$single['D'];
                            $productModel->SKU = @ $single['F'];
                            $productModel->description = @$single['D'];
                            $productModel->adult = 'no';
                            //Refrence to Google Product Search Categories
                            $productModel->age_group = NULL;
                            $productModel->gender = 'Unisex';
                            $productModel->availability = @$single['Y'];

//                            $productModel->product_status = 'active';
                            $productModel->brand = 'Test';
                            $productModel->condition = 'New';
                            $productModel->weight = @ $single['X'];
                            $productModel->UPC = '';
                            $productModel->EAN = '';
                            $productModel->Jan = '';
                            $productModel->ISBN = '';
                            $productModel->MPN = '';
                            //Inventory Fields Mapping
                            $productModel->stock_quantity = @$single['P'];
                            $productModel->stock_level = @$single['Y'];
                            $productModel->stock_status = 'Visible';
                            $productModel->low_stock_notification = NULL;
                            $productModel->price = @$single['G'] * $conversion_rate;
                            $productModel->sales_price = @$single['H'] * $conversion_rate;
                            $productModel->created_at = date('Y-m-d H:i:s', time());
                            $productModel->updated_at = date('Y-m-d H:i:s', time());
                            ;
                            //Save Elliot User id
                            $productModel->elliot_user_id = $user->id;
                            $productModel->save(false);

                            //After Product Save Fetch Product ID
                            $saved_product_id = $productModel->id;
                            //  echo '<Pre>';print_r($productModel);die;


                            $CategoryAbbrivation = new ProductAbbrivation();
                            $CategoryAbbrivation->channel_abb_id = 'FPK' . @$single['B'];
                            $CategoryAbbrivation->product_id = $saved_product_id;
                            $CategoryAbbrivation->channel_accquired = 'Flipkart';
                            $CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
                            $CategoryAbbrivation->save(false);
                            $get_category = Categories::find()->select('category_ID')->where(['channel_abb_id' => $single['C']])->one();
                            if (empty($get_category)) {
                                $category = new Categories();
                                $category->channel_abb_id = $single['C'];
                                $category->category_name = $single['C'];
                                $category->parent_category_ID = '';
                                $category->elliot_user_id = $user->id;
                                $category->store_category_groupid = '';
                                $category->created_at = date('Y-m-d H:i:s', time());
                                $category->updated_at = date('Y-m-d H:i:s', time());
                                $category->save(false);
                                $category_id = $category->category_ID;

                                $productCategoryModel = new ProductCategories();
                                $productCategoryModel->category_ID = $category_id;
                                $productCategoryModel->product_ID = $saved_product_id;
                                $productCategoryModel->created_at = date('Y-m-d H:i:s', time());
                                $productCategoryModel->updated_at = date('Y-m-d H:i:s', time());
                                //Save Elliot User id
                                $productCategoryModel->elliot_user_id = $user->id;
                                $productCategoryModel->save(false);
                            }
                            $productChannelModel = new ProductChannel();
                            $productChannelModel->channel_id = $channel_id;
//                                $productChannelModel->store_id = 0;
                            $productChannelModel->product_id = $saved_product_id;
                            $productChannelModel->save(false);


                            $merchantproductsModel = new MerchantProducts;
                            $merchantproductsModel->merchant_ID = $user->id;
                            $merchantproductsModel->product_ID = $saved_product_id;
                            //Save Elliot User id
                            $merchantproductsModel->elliot_user_id = $user->id;
                            $merchantproductsModel->save(false);

                        else:

                            /* Save Product Abbrivation Id */
                            $saved_product_id = $checkProductModel->id;

                            $ProductAbbrivation_check = ProductAbbrivation::find()->Where(['channel_abb_id' => $prefix_product_id])->one();
                            if (empty($ProductAbbrivation_check)):
                                $CategoryAbbrivation = new ProductAbbrivation();
                                $CategoryAbbrivation->channel_abb_id = $prefix_product_id;
                                $CategoryAbbrivation->product_id = $saved_product_id;
                                $CategoryAbbrivation->channel_accquired = 'Flipkart';
                                $CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
                                $CategoryAbbrivation->save(false);
                            endif;

                            $ProductChannel_check = ProductChannel::find()->Where(['channel_id' => $channel_id, 'product_id' => $saved_product_id])->one();
                            if (empty($ProductAbbrivation_check)):
                                //Product Channel/Store Table Entry
                                $productChannelModel = new ProductChannel;
                                $productChannelModel->channel_id = $channel_id;
                                $productChannelModel->product_id = $saved_product_id;
                                $productChannelModel->status = 'yes';
                                $productChannelModel->created_at = date('Y-m-d H:i:s', time());
                                $productChannelModel->save(false);
                            endif;
                        endif;
                    }
                }
            }
        }
        return $this->render('flipkart');
    }

    function flatten($element) {
        $flatArray = array();
        foreach ($element as $key => $node) {
            if (array_key_exists('children', $node)) {
                $flatArray = array_merge($flatArray, $this->flatten($node['children']));
                unset($node['children']);
                $flatArray[] = $node;
            } else {
                $flatArray[] = $node;
            }
        }


        return $flatArray;
    }

    function actionLazada($type = null) {
//        echo'd';die;
        if (empty($_GET['type']))
            die('Not Allowed');

        $channel_type = '';
        if ($type == 'Malaysia')
            $channel_type = 'Lazada Malaysia';

        $lazada = \backend\models\Channels::find()->where(['channel_name' => $channel_type])->one();
//echo'<pre>';
//print_r($lazada);die;
        $channel_id = $lazada->channel_ID;

        if (isset($_POST) and ! empty($_POST)) {
            $request_data = Yii::$app->request->post();
//            echo'<pre>';
//            print_r($request_data);die;
            date_default_timezone_set("UTC");
            $channel_connected = \backend\models\ChannelConnection::find()->where(['channel_ID' => $request_data['channel_id']])->one();
//                        print_r($channel_connected);
//die;
            if (empty($channel_connected)) {
                $user_email = $request_data['lazada_user_email'];
                $url = $request_data['lazada_api_url'];
                $api_key = $request_data['lazada_api_key'];
            } else {
                $user_email = $channel_connected->lazada_user_email;
                $url = $channel_connected->lazada_api_url;
                $api_key = $channel_connected->lazada_api_key;
            }
            $parameters = array(
                // The user ID for which we are making the call.
                'UserID' => $user_email,
                // The API version. Currently must be 1.0
                'Version' => '1.0',
                // The API method to call.
                'Action' => 'GetProducts',
                // The format of the result.
                'Format' => 'JSON',
                // The current time formatted as ISO8601
                'Timestamp' => date('c')
//    'Timestamp' => $utc 
            );

// Sort parameters by name.
            ksort($parameters);

// URL encode the parameters.
            $encoded = array();
            foreach ($parameters as $name => $value) {
                $encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
            }

// Concatenate the sorted and URL encoded parameters into a string.
            $concatenated = implode('&', $encoded);


// Compute signature and add it to the parameters.
            $parameters['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, $api_key, false));

// Build Query String
            $queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);

// Open cURL connection
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url . "?" . $queryString);

// Save response to the variable $data
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            $data = curl_exec($ch);
//            print_r($parameters);
// Close Curl connection
            curl_close($ch);
            $response = json_decode($data, true);
//            print_r($response);
//die;
            if (empty($response) or ( isset($response) and ! empty($response) and isset($response['ErrorResponse']))) {
                echo'error';
                die;
            } else {
                $channel_connection = new \backend\models\ChannelConnection;
//                echo'<pre>';
//                print_r($response);
//                die;
                if (empty($channel_connected)) {
                    $channel_connection->elliot_user_id = Yii::$app->user->identity->id;
                    $channel_connection->user_id = Yii::$app->user->identity->id;
                    $channel_connection->channel_id = $request_data['channel_id'];
                    $channel_connection->lazada_user_email = $request_data['lazada_user_email'];
                    $channel_connection->lazada_api_key = $request_data['lazada_api_key'];
                    $channel_connection->lazada_api_url = $request_data['lazada_api_url'];
                    $channel_connection->connected = 'yes';
                    $channel_connection->save();
                }
                if (isset($response['SuccessResponse']) and ! empty($response['SuccessResponse']) and isset($response['SuccessResponse']['Body']) and ! empty($response['SuccessResponse']['Body']['Products'])) {
                    $products = $response['SuccessResponse']['Body']['Products'];
                    if (isset($products) and ! empty($products)) {
                        $user = Yii::$app->user->identity;
//                    print_r($user);die;
                        $user_company = $user->company_name;
                        foreach ($products as $single_product) {

//echo"<pre>";
//print_r($single_product);die;

                            $parameters_category = array(
                                // The user ID for which we are making the call.
                                'UserID' => $user_email,
                                // The API version. Currently must be 1.0
                                'Version' => '1.0',
                                // The API method to call.
                                'Action' => 'GetCategoryTree',
                                // The format of the result.
                                'Format' => 'JSON',
                                // The current time formatted as ISO8601
                                'Timestamp' => date('c')
//    'Timestamp' => $utc 
                            );

// Sort parameters by name.
                            ksort($parameters_category);

// URL encode the parameters.
                            $encoded = array();
                            foreach ($parameters_category as $name => $value) {
                                $encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
                            }

// Concatenate the sorted and URL encoded parameters into a string.
                            $concatenated = implode('&', $encoded);


// Compute signature and add it to the parameters.
                            $parameters_category['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, $api_key, false));

// Build Query String
                            $queryString = http_build_query($parameters_category, '', '&', PHP_QUERY_RFC3986);

// Open cURL connection
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $url . "?" . $queryString);

// Save response to the variable $data
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                            $data = curl_exec($ch);
// Close Curl connection
                            curl_close($ch);
                            $response = json_decode($data, true);
                            echo'<pre>';
//            print_r($response);die;
                            $flat_array = $this->flatten($response['SuccessResponse']['Body']);

                            $key_found = array_search($single_product['PrimaryCategory'], array_column($flat_array, 'categoryId'));
                            $category_name = $flat_array[$key_found]['name'];

//                             echo $category_name;die;

                            $p_name = $single_product['Attributes']['name'];
                            //check for the Duplicate entry in Product Table
                            $checkProductModel = Products::find()->where(['product_name' => $p_name])->one();
                            if (empty($checkProductModel)):
//                        $p_abb_id = $product->id;
//                        //Give prefix big Commerce Product id
//                        $prefix_product_id = 'BGC' . $p_abb_id;
                                $p_sku = $single_product['Skus'][0]['SellerSku'];
//                        $p_upc = $product->upc;
                                $p_des = $single_product['Attributes']['short_description'];
                                $p_avail = $single_product['Skus'][0]['Available'];

                                $p_brand = ucfirst($user_company);
                                //$product->brand_id;
                                //$product->brand; //object
                                $p_weight = $single_product['Skus'][0]['package_weight'];
                                $p_price = $single_product['Skus'][0]['price'];
                                $p_saleprice = $single_product['Skus'][0]['special_price'] ? $single_product['Skus'][0]['special_price'] : $single_product['Skus'][0]['price'];
                                $p_visibility = ($single_product['Skus'][0]['Status'] == 'active') ? true : false;
                                //Create Model for Each new Product
                                $productModel = new Products ();
                                $productModel->channel_abb_id = 'LZ';
                                $productModel->product_name = $p_name;
                                $productModel->SKU = $p_sku;
                                $productModel->description = $p_des;
                                $productModel->adult = 'no';
                                //Refrence to Google Product Search Categories
                                $productModel->age_group = NULL;
                                $productModel->gender = 'Unisex';
                                $productModel->availability = 'In Stock';

                                $productModel->brand = $p_brand;
                                $productModel->condition = 'New';
                                $productModel->weight = $p_weight;
                                $productModel->UPC = '';
                                $productModel->EAN = '';
                                $productModel->Jan = '';
                                $productModel->ISBN = '';
                                $productModel->MPN = '';
                                //Inventory Fields Mapping
                                $productModel->stock_quantity = $single_product['Skus'][0]['Available'];
                                $productModel->stock_level = 'In Stock';
                                $productModel->stock_status = 'Visible';
                                $productModel->low_stock_notification = NULL;
                                $productModel->price = $p_price;
                                $productModel->sales_price = $p_saleprice;
                                $productModel->created_at = date('Y-m-d H:i:s', time());
                                $productModel->updated_at = date('Y-m-d H:i:s', time());
                                ;
                                //Save Elliot User id
                                $productModel->elliot_user_id = $user->id;
                                $productModel->save(false);



                                //After Product Save Fetch Product ID
                                $saved_product_id = $productModel->id;


                                $CategoryAbbrivation = new ProductAbbrivation();
                                $CategoryAbbrivation->channel_abb_id = 'LZ';
                                $CategoryAbbrivation->product_id = $saved_product_id;
                                $CategoryAbbrivation->price = $p_price;
                                $CategoryAbbrivation->salePrice = $p_saleprice;
                                $CategoryAbbrivation->channel_accquired = $channel_type;
                                $CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
                                $CategoryAbbrivation->save(false);

                                $get_category = Categories::find()->select('category_ID')->where(['channel_abb_id' => $single_product['PrimaryCategory']])->one();
//                                print_r($get_category);die;
                                if (empty($get_category)) {
                                    $category = new Categories();
                                    $category->channel_abb_id = $single_product['PrimaryCategory'];
                                    $category->category_name = $category_name;
                                    $category->parent_category_ID = '';
                                    $category->elliot_user_id = $user->id;
                                    $category->store_category_groupid = '';
                                    $category->created_at = date('Y-m-d H:i:s', time());
                                    $category->updated_at = date('Y-m-d H:i:s', time());
                                    $category->save(false);
                                    $category_id = $category->category_ID;

                                    $productCategoryModel = new ProductCategories();
                                    $productCategoryModel->category_ID = $category_id;
                                    $productCategoryModel->product_ID = $saved_product_id;
                                    $productCategoryModel->created_at = date('Y-m-d H:i:s', time());
                                    $productCategoryModel->updated_at = date('Y-m-d H:i:s', time());
                                    //Save Elliot User id
                                    $productCategoryModel->elliot_user_id = $user->id;
                                    $productCategoryModel->save(false);
                                }
                                //Product Images Table Entry
                                if (!empty($single_product['Skus'][0]['Images'])):
                                    foreach ($single_product['Skus'][0]['Images'] as $product_image):
                                        if (empty($product_image))
                                            continue;
                                        // echo '<Pre>';print_r*$product_image;
                                        $p_image_link = $product_image;
                                        //Create Model for Each new Product Image
                                        $productImageModel = new ProductImages;
                                        $productImageModel->link = $p_image_link;
                                        $productImageModel->product_ID = $saved_product_id;
                                        $productImageModel->default_image = 'Yes';
                                        $productImageModel->image_status = 1;
                                        $productImageModel->save(false);
                                    endforeach;
                                endif;
                                //Product Channel/Store Table Entry
                                $productChannelModel = new ProductChannel();
                                $productChannelModel->channel_id = $channel_id;
//                                $productChannelModel->store_id = 0;
                                $productChannelModel->product_id = $saved_product_id;
                                $productChannelModel->save(false);
                                //Product Variations Table Entry
                                $product_skus = $single_product['Skus'];
//                                print_r($product_skus);die;
                                if (!empty($product_skus)):
                                    foreach ($product_skus as $key => $product_sku):
                                        if ($key == 0)
                                            continue;
                                        //SKU
                                        if (isset($product_sku['SellerSku'])):
                                            $newP_Var_Model = new ProductVariation;
                                            $newP_Var_Model->product_id = $saved_product_id;
                                            $newP_Var_Model->elliot_user_id = $user->id;
                                            $newP_Var_Model->item_name = 'SKU';
                                            $newP_Var_Model->item_value = $product_sku['SellerSku'];
//                                        $newP_Var_Model->store_variation_id = 'BGC' . $product_sku->id;
                                            $created_date = date('Y-m-d H:i:s', time());
                                            $newP_Var_Model->created_at = $created_date;
                                            $newP_Var_Model->save(false);
                                        endif;
                                    endforeach;
                                endif;

                                //Merchant Products Table Entry
                                $merchantproductsModel = new MerchantProducts;
                                $merchantproductsModel->merchant_ID = $user->id;
                                $merchantproductsModel->product_ID = $saved_product_id;
                                //Save Elliot User id
                                $merchantproductsModel->elliot_user_id = $user->id;
                                $merchantproductsModel->save(false);
                            else:

                                /* Save Product Abbrivation Id */
                                $saved_product_id = $checkProductModel->id;

                                $ProductAbbrivation_check = ProductAbbrivation::find()->Where(['channel_abb_id' => 'LZ', 'product_id' => $saved_product_id])->one();
                                if (empty($ProductAbbrivation_check)):
                                    $CategoryAbbrivation = new ProductAbbrivation();
                                    $CategoryAbbrivation->channel_abb_id = 'LZ';
                                    $CategoryAbbrivation->product_id = $saved_product_id;
                                    $CategoryAbbrivation->channel_accquired = $channel_type;
                                    $CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
                                    $CategoryAbbrivation->save(false);
                                endif;

                                $ProductChannel_check = ProductChannel::find()->Where(['channel_id' => $channel_id, 'product_id' => $saved_product_id])->one();
                                if (empty($ProductAbbrivation_check)):
                                    //Product Channel/Store Table Entry
                                    $productChannelModel = new ProductChannel;
                                    $productChannelModel->channel_id = $channel_id;
                                    $productChannelModel->product_id = $saved_product_id;
                                    $productChannelModel->status = 'yes';
                                    $productChannelModel->created_at = date('Y-m-d H:i:s', time());
                                    $productChannelModel->save(false);
                                endif;
                            endif;
                        }
//                        echo'saved';
//                        die;
                    }
                }
            }


            $parameters = array(
                // The user ID for which we are making the call.
                'UserID' => 'info@fauxfreckles.com',
                // The API version. Currently must be 1.0
                'Version' => '1.0',
                // The API method to call.
                'Action' => 'GetOrders',
                // The format of the result.
                'Format' => 'JSON',
                // The current time formatted as ISO8601
                'Timestamp' => date('c')
//    'Timestamp' => $utc 
            );

// Sort parameters by name.
            ksort($parameters);

// URL encode the parameters.
            $encoded = array();
            foreach ($parameters as $name => $value) {
                $encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
            }

// Concatenate the sorted and URL encoded parameters into a string.
            $concatenated = implode('&', $encoded);

// The API key for the user as generated in the Seller Center GUI.
// Must be an API key associated with the UserID parameter.
            $api_key = 'JeVfOHaBm0RdndrqR1HsZGyL20xHwGSxi3gngcsvOaCnPE_1nsf2Yb7f';

// Compute signature and add it to the parameters.
            $parameters['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, $api_key, false));

//            echo'<pre>';
//            print_R($parameters);


            $url = "https://api.sellercenter.lazada.com.my/";

// Build Query String
            $queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);

// Open cURL connection
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url . "?" . $queryString);

// Save response to the variable $data
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            $data = curl_exec($ch);

// Close Curl connection
            curl_close($ch);
            $order_response_decoded = json_decode($data);
            $orders = @$order_response_decoded->SuccessResponse->Body->Orders;
//echo"<pre>";print_R($order_response_decoded);
//echo'heer';
//print_R($orders);die;
            if (isset($orders) and ! empty($orders)) {
                foreach ($orders as $orders_data) {
                    //get BigCommerce Order id
                    $lazada_id = $orders_data->OrderId;
                    //Give prefix big Commerce Order id
                    $prefix_order_id = 'LZ' . $lazada_id;
                    //Check If Order duplicacy
                    $db_orders_check = Orders::find()->Where(['channel_abb_id' => $prefix_order_id])->one();
                    if (empty($db_orders_check)) {
                        if (!empty($prefix_order_id)):
                            $customer_db_data = CustomerUser::find()->Where(['channel_abb_id' => $prefix_order_id])->one();
                            if (empty($customer_db_data)):
                                //Save New customers 
                                $Customers_order_model = new CustomerUser();
                                $Customers_order_model->channel_abb_id = $prefix_order_id;
                                $Customers_order_model->first_name = isset($orders_data->CustomerFirstName) ? $orders_data->CustomerFirstName : "";
                                $Customers_order_model->last_name = isset($orders_data->CustomerLastName) ? $orders_data->CustomerLastName : "";
                                $Customers_order_model->email = isset($orders_data->AddressBilling->CustomerEmail) ? $orders_data->AddressBilling->CustomerEmail : "";
                                $Customers_order_model->channel_acquired = $channel_type;
                                $Customers_order_model->date_acquired = date('Y-m-d H:i:s', strtotime($orders_data->CreatedAt));
                                $Customers_order_model->street_1 = isset($orders_data->AddressBilling->Address1) ? $orders_data->AddressBilling->Address1 : "";
                                $Customers_order_model->street_2 = isset($orders_data->AddressBilling->Address2) ? $orders_data->AddressBilling->Address2 : "";
                                $Customers_order_model->city = isset($orders_data->AddressBilling->City) ? $orders_data->AddressBilling->City : "";
                                $Customers_order_model->country = isset($orders_data->AddressBilling->Country) ? $orders_data->AddressBilling->Country : "";
                                $Customers_order_model->country_iso = "";
                                $Customers_order_model->state = "";
                                $Customers_order_model->zip = isset($orders_data->AddressBilling->PostCode) ? $orders_data->AddressBilling->PostCode : "";
                                $Customers_order_model->phone_number = isset($orders_data->AddressBilling->Phone) ? $orders_data->AddressBilling->Phone : "";
                                $Customers_order_model->created_at = date('Y-m-d H:i:s', strtotime($orders_data->CreatedAt));
                                $Customers_order_model->updated_at = date('Y-m-d H:i:s', strtotime($orders_data->UpdatedAt));
                                //Save Elliot User id
                                $Customers_order_model->elliot_user_id = $user->id;
                                $Customers_order_model->save(false);
                            endif;
                        endif;
//                        $customer_email = $orders_data->billing_address->email;
                        //get Customer Id
                        $customer_db1 = CustomerUser::find()->Where(['channel_abb_id' => $prefix_order_id])->one();
                        $customer_id = $customer_db1->customer_ID;
                        $order_customer_id = $customer_id;
                        $order_status = $orders_data->Statuses[0];
                        switch (strtolower($order_status)) {
                            case "pending":
                                $order_status = 'Pending';
                                break;
                            case "delivered":
                                $order_status = "Completed";
                                break;
                            case "canceled":
                                $order_status = "Cancel";
                                break;
                            case "closed":
                                $order_status = "Closed";
                                break;
                            case "fraud":
                                $order_status = "Cancel";
                                break;
                            case "holded":
                                $order_status = "On Hold";
                                break;
                            case "payment_review":
                                $order_status = "Pending";
                                break;
                            case "return_shipped_by_customer":
                                $order_status = "Cancel";
                                break;
                            case "returned":
                                $order_status = "Cancel";
                                break;
                            case "return_waiting_for_approval":
                                $order_status = "Cancel";
                                break;
                            case "return_rejected":
                                $order_status = "Cancel";
                                break;
                            case "failed":
                                $order_status = "Cancel";
                                break;
                            case "pending_payment":
                                $order_status = "Pending";
                                break;
                            case "pending_paypal":
                                $order_status = "Pending";
                                break;
                            case "processing":
                                $order_status = "In Transit";
                                break;
                            case "shipped":
                                $order_status = "In Transit";
                                break;
                            default:
                                $order_status = "Pending";
                        }
                        $product_qauntity = $orders_data->ItemsCount;
                        //billing Address
                        $billing_add1 = isset($orders_data->AddressBilling->Address1) ? $orders_data->AddressBilling->Address1 : "" . ',' . isset($orders_data->AddressBilling->Address2) ? $orders_data->AddressBilling->Address2 : "";
                        $billing_add2 = isset($orders_data->AddressBilling->City) ? $orders_data->AddressBilling->City : "" . ',' .
                                ',' . isset($orders_data->AddressBilling->PostCode) ? $orders_data->AddressBilling->PostCode : "" . ',' . isset($orders_data->AddressBilling->Country) ? $orders_data->AddressBilling->Country : "";
                        $billing_address = $billing_add1 . ',' . $billing_add2;

                        //billing Address
                        $bill_street_1 = isset($orders_data->AddressBilling->Address1) ? $orders_data->AddressBilling->Address1 : '';
                        $bill_street_2 = isset($orders_data->AddressBilling->Address2) ? $orders_data->AddressBilling->Address2 : '';
                        $bill_city = isset($orders_data->AddressBilling->City) ? $orders_data->AddressBilling->City : '';
                        $bill_state = '';
                        $bill_zip = isset($orders_data->AddressBilling->PostCode) ? $orders_data->AddressBilling->PostCode : '';
                        $bill_country = isset($orders_data->AddressBilling->Country) ? $orders_data->AddressBilling->Country : '';
                        $bill_country_iso = '';

                        //Shipping Address
                        $ship_street_1 = isset($orders_data->AddressShipping->Address1) ? $orders_data->AddressShipping->Address1 : $orders_data->AddressBilling->Address1;
                        $ship_street_2 = isset($orders_data->AddressShipping->Address2) ? $orders_data->AddressShipping->Address2 : $orders_data->AddressBilling->Address2;
                        $ship_city = isset($orders_data->AddressShipping->City) ? $orders_data->AddressShipping->City : $orders_data->AddressBilling->City;
                        $ship_state = "";
                        $ship_zip = isset($orders_data->AddressShipping->PostCode) ? $orders_data->AddressShipping->PostCode : $orders_data->AddressBilling->PostCode;
                        $ship_country = isset($orders_data->AddressShipping->Country) ? $orders_data->AddressShipping->Country : $orders_data->AddressBilling->Country;
                        $ship_country_iso = '';

                        $shipping_address = $ship_street_1 . ',' . $ship_street_2 . ',' . $ship_city . ',' . $ship_state . ',' . $ship_zip . ',' . $ship_country;

                        $total_amount = $orders_data->Price;
                        $base_shipping_cost = '';
                        $shipping_cost_tax = '';
                        $base_handling_cost = '';
                        $handling_cost_tax = '';
                        $base_wrapping_cost = '';
                        $wrapping_cost_tax = '';
                        $payment_method = $orders_data->PaymentMethod;
                        $payment_provider_id = '';
                        $payment_status = '';
                        $refunded_amount = '';
                        $discount_amount = '';
                        $coupon_discount = '';
                        $order_date = $orders_data->CreatedAt;
                        $order_last_modified_date = $orders_data->UpdatedAt;
                        //Save Order Model details
                        $order_model = new Orders();
                        $order_model->customer_id = $customer_id;
                        $order_model->channel_abb_id = $prefix_order_id;
                        $order_model->order_status = $order_status;
                        $order_model->product_qauntity = $product_qauntity;
                        $order_model->bill_street_1 = $bill_street_1;
                        $order_model->bill_street_2 = $bill_street_2;
                        $order_model->bill_city = $bill_city;
                        $order_model->bill_state = $bill_state;
                        $order_model->bill_zip = $bill_zip;
                        $order_model->bill_country = $bill_country;
                        $order_model->bill_country_iso = $bill_country_iso;
                        $order_model->ship_street_1 = $ship_street_1;
                        $order_model->ship_street_2 = $bill_street_2;
                        $order_model->ship_city = $ship_city;
                        $order_model->ship_state = $ship_state;
                        $order_model->ship_zip = $ship_zip;
                        $order_model->ship_country = $ship_country;
                        $order_model->ship_country_iso = $ship_country_iso;
                        $order_model->shipping_address = $shipping_address;
                        $order_model->billing_address = $billing_address;
                        $order_model->base_shipping_cost = $base_shipping_cost;
                        $order_model->shipping_cost_tax = $shipping_cost_tax;
                        $order_model->base_handling_cost = $base_handling_cost;
                        $order_model->handling_cost_tax = $handling_cost_tax;
                        $order_model->base_wrapping_cost = $base_wrapping_cost;
                        $order_model->wrapping_cost_tax = $wrapping_cost_tax;
                        $order_model->payment_method = $payment_method;
                        $order_model->payment_provider_id = $payment_provider_id;
                        $order_model->payment_status = $payment_status;
                        $order_model->refunded_amount = $refunded_amount;
                        $order_model->discount_amount = $discount_amount;
                        $order_model->coupon_discount = $coupon_discount;
                        $order_model->total_amount = $total_amount;
                        $order_model->order_date = $order_date;
                        $order_model->updated_at = date('Y-m-d H:i:s', strtotime($order_last_modified_date));
                        $order_model->created_at = date('Y-m-d H:i:s', strtotime($order_date));
                        //Save Elliot User id
                        $order_model->elliot_user_id = $user->id;
                        if ($order_model->save(false)) {


                            $parameters = array(
                                // The user ID for which we are making the call.
                                'UserID' => 'info@fauxfreckles.com',
                                // The API version. Currently must be 1.0
                                'Version' => '1.0',
                                // The API method to call.
                                'Action' => 'GetOrderItems',
                                // The format of the result.
                                'Format' => 'JSON',
                                // The current time formatted as ISO8601
                                'Timestamp' => date('c')
                                , 'OrderId' => $lazada_id
//    'Timestamp' => $utc 
                            );

// Sort parameters by name.
                            ksort($parameters);

// URL encode the parameters.
                            $encoded = array();
                            foreach ($parameters as $name => $value) {
                                $encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
                            }

// Concatenate the sorted and URL encoded parameters into a string.
                            $concatenated = implode('&', $encoded);

// The API key for the user as generated in the Seller Center GUI.
// Must be an API key associated with the UserID parameter.
                            $api_key = 'JeVfOHaBm0RdndrqR1HsZGyL20xHwGSxi3gngcsvOaCnPE_1nsf2Yb7f';

// Compute signature and add it to the parameters.
                            $parameters['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, $api_key, false));



                            $url = "https://api.sellercenter.lazada.com.my/";

// Build Query String
                            $queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);

// Open cURL connection
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $url . "?" . $queryString);

// Save response to the variable $data
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                            $data = curl_exec($ch);

// Close Curl connection
                            curl_close($ch);
                            $response_decoded = json_decode($data);
                            if (isset($response_decoded) and ! empty($response_decoded)) {
                                $order_products_lazada = $response_decoded->SuccessResponse->Body->OrderItems;
//echo"<pre>";print_R($order_products_lazada);
                                if (isset($order_products_lazada) and ! empty($order_products_lazada)) {


                                    foreach ($order_products_lazada as $product_dtails_data) {
                                        $product_details_check = Products::find()->Where(['product_name' => $product_dtails_data->Name])->one();
//                                        print_R($product_details_check);
                                        if (!empty($product_details_check)) {
                                            /* if product already exist in db */
                                            $product_id = $product_details_check->id;
                                        } else {
                                            //Check for custom product and save a custom product in product table
                                            $product_details_custom = Products::find()->where(['product_name' => $product_dtails_data->Name, 'SKU' => $product_dtails_data->Sku])->one();
                                            if (empty($product_details_custom)) :
                                                //save custom product details 
                                                $product_custom_Model = new Products();
                                                $product_custom_Model->channel_abb_id = "LZ";
                                                $custom_product_name = $product_dtails_data->Name;
                                                $custom_product_sku = isset($product_dtails_data->Sku) ? $product_dtails_data->Sku : "";
                                                $product_custom_Model->product_name = $custom_product_name;
                                                $product_custom_Model->SKU = $custom_product_sku;
                                                $product_custom_Model->UPC = "";
                                                $product_custom_Model->EAN = "";
                                                $product_custom_Model->Jan = "";
                                                $product_custom_Model->ISBN = "";
                                                $product_custom_Model->MPN = "";
                                                $product_custom_Model->brand = "";
                                                $product_custom_Model->product_status = 'in_active';
                                                //Save Elliot User id 
                                                $product_custom_Model->elliot_user_id = $user->id;

                                                if ($product_custom_Model->save(false)):
                                                    $product_id = $product_custom_Model->id;
                                                    /* Save Product Abbrivation Id */
                                                    $CategoryAbbrivation = new ProductAbbrivation();
                                                    $CategoryAbbrivation->channel_abb_id = 'LZ';
                                                    $CategoryAbbrivation->product_id = $product_custom_Model->id;
                                                    $CategoryAbbrivation->channel_accquired = $channel_type;
                                                    $CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
                                                    $CategoryAbbrivation->save(false);
                                                endif;

                                                //Product Channel/Store Table Entry
                                                $custom_productChannelModel = new ProductChannel;
                                                $custom_productChannelModel->channel_id = $channel_id;
                                                $custom_productChannelModel->product_id = $product_custom_Model->id;
                                                $custom_productChannelModel->status = 'no';
                                                $custom_productChannelModel->created_at = date('Y-m-d H:i:s', time());
                                                $custom_productChannelModel->save(false);
                                            /* If order Product Already exist in database */
                                            else:

                                                $product_id = $product_custom_Model->id;
                                                /* Save Product Abbrivation Id */
                                                $ProductChannel_check = ProductChannel::find()->Where(['channel_id' => $channel_id, 'product_id' => $product_custom_Model->id])->one();
                                                if (empty($ProductChannel_check)):

                                                    $CategoryAbbrivation = new ProductAbbrivation();
                                                    $CategoryAbbrivation->channel_abb_id = 'LZ';
                                                    $CategoryAbbrivation->product_id = $product_custom_Model->id;
                                                    $CategoryAbbrivation->channel_accquired = $channel_type;
                                                    $CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
                                                    $CategoryAbbrivation->save(false);

                                                    //Product Channel/Store Table Entry
                                                    $custom_productChannelModel = new ProductChannel;
                                                    $custom_productChannelModel->channel_id = $channel_id;
                                                    $custom_productChannelModel->product_id = $product_custom_Model->id;
                                                    $custom_productChannelModel->status = 'no';
                                                    $custom_productChannelModel->created_at = date('Y-m-d H:i:s', time());
                                                    $custom_productChannelModel->save(false);
                                                endif;
                                            endif;
                                        }
                                        $product_name = $product_dtails_data->Name;
                                        $product_qty = "";
                                        $order_product_sku = $product_dtails_data->Sku;
                                        //$product_details = Products::find()->Where(['product_name' => $product_name])->one();
                                        //DB order Id
                                        $db_order_id = $order_model->order_ID;

                                        $product_order_check = OrdersProducts::find()->Where(['order_Id' => $db_order_id, 'product_Id' => $product_id, 'order_product_sku' => $order_product_sku])->one();

                                        if (empty($product_order_check)) {
                                            //Save Order Products Details
                                            $order_products_model = new OrdersProducts;
                                            $order_products_model->order_Id = $db_order_id;
                                            $order_products_model->product_Id = $product_id;
                                            $order_products_model->qty = $product_qty;
                                            $order_products_model->order_product_sku = $order_product_sku;
                                            $order_products_model->created_at = $order_model->created_at;
                                            //Save Elliot User id
                                            $order_products_model->elliot_user_id = $user->id;
                                            $order_products_model->save(false);
                                        }
                                    }
                                }
                            }


                            $order_channels_model = new OrderChannel();
                            $order_channels_model->order_id = $db_order_id;
                            $order_channels_model->elliot_user_id = $user->id;
                            $order_channels_model->channel_id = $channel_id;
                            $order_channels_model->created_at = $order_model->created_at;
                            $order_channels_model->save(false);
                        }
                    }
                }


                $get_rec = \backend\models\ChannelConnection::find()->Where(['user_id' => $user->id, 'channel_id' => $channel_id])->one();
                $get_rec->import_status = 'Completed';
                $get_rec->save(false);
                $notif_type = $channel_type;
                $notif_db = \backend\models\Notification::find()->Where(['notif_type' => $notif_type])->one();
                if (empty($notif_db)):
                    $notification_model = new \backend\models\Notification();
                    $notification_model->user_id = $user->id;
                    $notification_model->notif_type = $notif_type;
                    $notification_model->notif_description = 'Your ' . $notif_type . ' data has been successfully imported.';
                    $notification_model->created_at = date('Y-m-d h:i:s', time());
                    $notification_model->save(false);
                endif;
                $msg = 'Your ' . $notif_type . ' data has been imported in the Elliot';
                $sub = 'Success, Your ' . $notif_type . ' Store is Now Connected';
                $company_name = ucfirst($user->company_name);
                $ml2 = Yii::$app->mailer->compose()
                        //->getSwiftMessage()->getHeaders()->addTextHeader('name', 'value')
                        ->setFrom('elliot@helloiamelliot.com')
                        ->setTo($user->email)
                        ->setSubject($sub)
                        ->setHtmlBody(
                                '<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="background-color:#fcfcfc;padding: 60px 10px">
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="1" cellspacing="0" width="600" id="emailContainer" style="background-color:#fff;padding:25px;-webkit-box-shadow: 0px 5px 30px 0px rgba(50, 50, 50, 0.2);-moz-box-shadow:0px 5px 30px 0px rgba(50, 50, 50, 0.2);box-shadow:0px 5px 30px 0px rgba(50, 50, 50, 0.2);">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailHeader">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <center><img src="http://beta.brstdev.com/adrianna/elliot-logo.jpg" alt="Logo13" title="Logo" style="display:block; width: 100px;"/></center>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellpadding="1" cellspacing="0" width="100%" id="emailBody">
                                                <tr>
                                                    <td valign="top">
                                                        <p style="color: #000000;text-align: left;font-family: OpenSans;font-size: 14.0px;font-style: normal;font-stretch: normal;font-weight: 400;">
                                                            ' . $company_name . ',
                                                       </p>                                                    
                                                       </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                        <p style="color: #000000;text-align: left;font-family: OpenSans;font-size: 14.0px;font-style: normal;font-stretch: normal;font-weight: 400;line-height:25px;">
                                                        ' . $msg . '</p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellpadding="1" cellspacing="0" width="100%" id="emailFooter">
                                                    <tr>
                                                        <tdvalign="top">
                                                           <p>Thank you,<br>
                                                            <b>Elliot</b></p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>'
                        )
                        ->send();
            }
        }
        return $this->render('lazada', [
                    'type' => $type,
        ]);
    }

    function actionLazadaupdate($type = null) {
        if (empty($_GET['type']))
            die('Not Allowed');

//this is file for put lazada api xml format
//                require_once $_SERVER['DOCUMENT_ROOT'].'/backend/web/';


        $parameters = array(
            // The user ID for which we are making the call.
            'UserID' => 'info@fauxfreckles.com',
            // The API version. Currently must be 1.0
            'Version' => '1.0',
            // The API method to call.
            'Action' => 'CreateProduct',
            // The format of the result.
            'Format' => 'JSON',
            // The current time formatted as ISO8601
            'Timestamp' => date('c')
        );
// Sort parameters by name.
        ksort($parameters);
//        echo'<pre>';
//        print_r($parameters);
// URL encode the parameters.
        $encoded = array();
        foreach ($parameters as $name => $value) {
            $encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
        }
        print_r($encoded);
        $request = ['Request' => ['PrimaryCategory' => 3,
                'Attributes' => [
                    'name' => 'api create product test sample',
                    'model' => '2017',
                    'short_description' => 'updated description',
                ],
                'Skus' => [
                    'Available' => '20',
                    'SellerSku' => 'test01'
                ]]
        ];



//                ksort($request);
//
//        $fields_string = http_build_query($request);
//        print_r($fields_string);
//        die;
// Concatenate the sorted and URL encoded parameters into a string.
        $concatenated = implode('&', $encoded);
//        $new=$concatenated.'&'.$fields_string;
//        print_r($concatenated);die;
// Compute signature and add it to the parameters.
        $parameters['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, 'JeVfOHaBm0RdndrqR1HsZGyL20xHwGSxi3gngcsvOaCnPE_1nsf2Yb7f', false));

// Build Query String
//        echo'<br>';
        $queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);






        $input_request = "<Request>
    <Product>
        <PrimaryCategory>10000671</PrimaryCategory>
        <SPUId></SPUId>
        <Attributes>
            <name>api create product test sample</name>
  <name_ms>api create product test sample</name_ms>
  <warranty_type>Local Manufacturer Warranty</warranty_type>
            <short_description>This is a nice product</short_description>
            <brand>Remark</brand>
            <model>asdf</model>
            <kid_years>Kids (6-10yrs)</kid_years>
        </Attributes>
        <Skus>
            <Sku>
                <SellerSku>api-create-test-2</SellerSku>
  <tax_class>default</tax_class>
                <color_family>Green</color_family>
                <size>40</size>
                <quantity>1</quantity>
                <price>388.50</price>
                <package_length>11</package_length>
                <package_height>22</package_height>
                <package_weight>33</package_weight>
                <package_width>44</package_width>
                <package_content>this is what's in the box</package_content>
                <Images>
                    <Image>http://sg.s.alibaba.lzd.co/original/59046bec4d53e74f8ad38d19399205e6.jpg</Image>
                    <Image>http://sg.s.alibaba.lzd.co/original/179715d3de39a1918b19eec3279dd482.jpg</Image>
                </Images>
            </Sku>
          
        </Skus>
    </Product>
</Request>";


        $target = 'https://api.sellercenter.lazada.com.my/?' . $queryString;
//        echo $target;die;
        $tmpFile = $_SERVER['DOCUMENT_ROOT'] . '/backend/web/test.xml';
///SENDING DATA TO LAZADA API
        $curl = curl_init();
//TRUE to HTTP PUT a file. The file to PUT must be set with CURLOPT_INFILE and CURLOPT_INFILESIZE.
        curl_setopt($curl, CURLOPT_PUT, 1);
//display headers
        curl_setopt($curl, CURLOPT_HEADER, true);
//The name of a file holding one or more certificates to verify the peer with. This only makes sense when used in combination with CURLOPT_SSL_VERIFYPEER.
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
//  A directory that holds multiple CA certificates. Use this option alongside CURLOPT_SSL_VERIFYPEER.
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
//  TRUE to HTTP PUT a file. The file to PUT must be set with CURLOPT_INFILE and CURLOPT_INFILESIZE.
        curl_setopt($curl, CURLOPT_INFILESIZE, filesize($tmpFile));
//The expected size, in bytes, of the file when uploading a file to a remote site.
        curl_setopt($curl, CURLOPT_INFILE, ($in = fopen($tmpFile, 'r')));
//A custom request method to use instead of "GET" or "HEAD" when doing a HTTP request. 
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
//An array of HTTP header fields to set,
        curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/xml']);
//The URL to fetch. 
        curl_setopt($curl, CURLOPT_URL, $target);
//TRUE to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($curl, CURLOPT_POSTFIELDS, "xmlRequest=" . $input_request);

//        $str = http_build_query($request);
////        echo $str;die;
//        curl_setopt($curl, CURLOPT_POSTFIELDS, $str);
//executing code for connection
        $result = curl_exec($curl);
//closing connection
        curl_close($curl);
//closing the open file CURLOPT_INFILE
        fclose($in);
//printing data for user
        print_r($result);
        DIE;













//        print_r($queryString);
        $url = "https://api.sellercenter.lazada.com.my/";

// Open cURL connection
        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, $url . "?" . $queryString);
        curl_setopt($ch, CURLOPT_URL, $url . "?" . $queryString);

// Save response to the variable $data
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $str = http_build_query($request);
//        echo $str;die;
        curl_setopt($ch, CURLOPT_POSTFIELDS, $str);
        $data = curl_exec($ch);

// Close Curl connection
        curl_close($ch);
        $response = json_decode($data, true);
        echo'<pre>';
        print_r($response);
        die;

        if (isset($_POST) and ! empty($_POST)) {
            $request_data = Yii::$app->request->post();
//            echo'<pre>';
//            print_r($request_data);die;
            date_default_timezone_set("UTC");
            $channel_connected = \backend\models\ChannelConnection::find()->where(['channel_ID' => $request_data['channel_id']])->one();
//                        print_r($channel_connected);
//die;
            if (empty($channel_connected)) {
                $user_email = $request_data['lazada_user_email'];
                $url = $request_data['lazada_api_url'];
                $api_key = $request_data['lazada_api_key'];
            } else {
                $user_email = $channel_connected->lazada_user_email;
                $url = $channel_connected->lazada_api_url;
                $api_key = $channel_connected->lazada_api_key;
            }
            $parameters = array(
                // The user ID for which we are making the call.
                'UserID' => $user_email,
                // The API version. Currently must be 1.0
                'Version' => '1.0',
                // The API method to call.
                'Action' => 'GetProducts',
                // The format of the result.
                'Format' => 'JSON',
                // The current time formatted as ISO8601
                'Timestamp' => date('c')
//    'Timestamp' => $utc 
            );

// Sort parameters by name.
            ksort($parameters);

// URL encode the parameters.
            $encoded = array();
            foreach ($parameters as $name => $value) {
                $encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
            }

// Concatenate the sorted and URL encoded parameters into a string.
            $concatenated = implode('&', $encoded);


// Compute signature and add it to the parameters.
            $parameters['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, $api_key, false));

// Build Query String
            $queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);

// Open cURL connection
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url . "?" . $queryString);

// Save response to the variable $data
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            $data = curl_exec($ch);

// Close Curl connection
            curl_close($ch);
            $response = json_decode($data, true);
//              print_r($response);
//            die;  
            if (empty($response) or ( isset($response) and ! empty($response) and isset($response['ErrorResponse']))) {
                echo'error';
                die;
            } else {
                $channel_connection = new \backend\models\ChannelConnection;
//                echo'<pre>';
//                print_r($channel_connection);
//                die;
                if (empty($channel_connected)) {
                    $channel_connection->elliot_user_id = Yii::$app->user->identity->id;
                    $channel_connection->user_id = Yii::$app->user->identity->id;
                    $channel_connection->channel_id = $request_data['channel_id'];
                    $channel_connection->lazada_user_email = $request_data['lazada_user_email'];
                    $channel_connection->lazada_api_key = $request_data['lazada_api_key'];
                    $channel_connection->lazada_api_url = $request_data['lazada_api_url'];
                    $channel_connection->connected = 'yes';
                    $channel_connection->save();
                }
                if (isset($response['SuccessResponse']) and ! empty($response['SuccessResponse']) and isset($response['SuccessResponse']['Body']) and ! empty($response['SuccessResponse']['Body']['Products'])) {
                    $products = $response['SuccessResponse']['Body']['Products'];
                    print_r($products);
                    die;
                }
            }
        }
        return $this->render('lazada', [
                    'type' => $type,
        ]);
    }

    function actionLazada1($type = null) {
//        if (empty($_GET['type']))
//            die;
//        if (isset($_POST) and ! empty($_POST)) {
//            $request_data = Yii::$app->request->post();
//            echo'<pre>';
//            print_r($request_data);
//            die;
        date_default_timezone_set("UTC");

        $parameters = array(
            // The user ID for which we are making the call.
            'UserID' => 'info@fauxfreckles.com',
            // The API version. Currently must be 1.0
            'Version' => '1.0',
            // The API method to call.
            'Action' => 'GetOrders',
            // The format of the result.
            'Format' => 'JSON',
            // The current time formatted as ISO8601
            'Timestamp' => date('c')
//    'Timestamp' => $utc 
        );

// Sort parameters by name.
        ksort($parameters);

// URL encode the parameters.
        $encoded = array();
        foreach ($parameters as $name => $value) {
            $encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
        }

// Concatenate the sorted and URL encoded parameters into a string.
        $concatenated = implode('&', $encoded);

// The API key for the user as generated in the Seller Center GUI.
// Must be an API key associated with the UserID parameter.
        $api_key = 'JeVfOHaBm0RdndrqR1HsZGyL20xHwGSxi3gngcsvOaCnPE_1nsf2Yb7f';

// Compute signature and add it to the parameters.
        $parameters['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, $api_key, false));

        echo'<pre>';
        print_R($parameters);


        $url = "https://api.sellercenter.lazada.com.my/";

// Build Query String
        $queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);

// Open cURL connection
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url . "?" . $queryString);

// Save response to the variable $data
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $data = curl_exec($ch);

// Close Curl connection
        curl_close($ch);
        print_r(json_decode($data));
        die;
//        }
        return $this->render('lazada', [
                    'type' => $type,
        ]);
    }

    function actionLazada2($type = null) {
//        if (empty($_GET['type']))
//            die;
//        if (isset($_POST) and ! empty($_POST)) {
//            $request_data = Yii::$app->request->post();
//            echo'<pre>';
//            print_r($request_data);
//            die;
        date_default_timezone_set("UTC");

        $parameters = array(
            // The user ID for which we are making the call.
            'UserID' => 'info@fauxfreckles.com',
            // The API version. Currently must be 1.0
            'Version' => '1.0',
            // The API method to call.
            'Action' => 'GetOrderItems',
            // The format of the result.
            'Format' => 'JSON',
            // The current time formatted as ISO8601
            'Timestamp' => date('c')
            , 'OrderId' => '109121557'
//    'Timestamp' => $utc 
        );

// Sort parameters by name.
        ksort($parameters);

// URL encode the parameters.
        $encoded = array();
        foreach ($parameters as $name => $value) {
            $encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
        }

// Concatenate the sorted and URL encoded parameters into a string.
        $concatenated = implode('&', $encoded);

// The API key for the user as generated in the Seller Center GUI.
// Must be an API key associated with the UserID parameter.
        $api_key = 'JeVfOHaBm0RdndrqR1HsZGyL20xHwGSxi3gngcsvOaCnPE_1nsf2Yb7f';

// Compute signature and add it to the parameters.
        $parameters['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, $api_key, false));

        echo'<pre>';
        print_R($parameters);


        $url = "https://api.sellercenter.lazada.com.my/";

// Build Query String
        $queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);

// Open cURL connection
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url . "?" . $queryString);

// Save response to the variable $data
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $data = curl_exec($ch);

// Close Curl connection
        curl_close($ch);
        print_r(json_decode($data));
        die;
//        }
        return $this->render('lazada', [
                    'type' => $type,
        ]);
    }

    function actionFeed($u_id) {
//header('Content-Type: application/xml');

        if (empty($u_id)) {
            echo 'Invalid User Id';
            die;
        }
        $u_id = base64_decode(base64_decode($u_id));
//        echo $u_id;
        $user = User::find()->where(['id' => $u_id])->one();
//        echo'<pre>';
//        print_r($user);
//        die;
        if (empty($user)) {
            echo 'Invalid User';
            die;
        }
        $config = yii\helpers\ArrayHelper::merge(
                        require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $user->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);

        $products = Products::find()->with('productImages', 'productCategories.category', 'productChannels', 'productVariations')->asArray()->all();
//        echo"<pre>";        print_r($products);die;
        $items_xml = '';
        if (isset($products) and ! empty($products)) {
            foreach ($products as $single_product) {
                $items_xml .= '<item>
  <g:id>' . $single_product['id'] . '</g:id> 
  <g:title>' . str_replace("&", "and", $single_product['product_name']) . '</g:title> 
  <g:description>' . str_replace("&", "and", $single_product['product_name']) . '</g:description> 
  <g:link>http://www.example.com/bowls/db-1.html</g:link>';
                if (isset($single_product["brand"]) and ! empty($single_product["brand"])) {
                    $items_xml .= '<g:brand>' . $single_product["brand"] . '</g:brand>';
                } else {
                    $items_xml .= '<g:brand>Ravi</g:brand>';
                }
                $items_xml .= '<g:condition>new</g:condition> 
  <g:availability>' . $single_product["stock_level"] . '</g:availability> 
  <g:price>' . $single_product["price"] . '</g:price> 
  <g:google_product_category>Animals > Test Category</g:google_product_category> 
  <g:custom_label_0>Made in Waterford, IE</g:custom_label_0> ';
                if (isset($single_product["productImages"][0])) {
                    $items_xml .= ' <g:image_link>' . $single_product["productImages"][0]["link"] . '</g:image_link> ';
                } else {
                    $items_xml .= ' <g:image_link>http://google.com</g:image_link> ';
                }
                $items_xml .= '</item>';
            }
        }
//header('Content-Type: text/xml');
//        $input_request = "<rss xmlns:g='http://base.google.com/ns/1.0' version='2.0'>
//- <channel>
//  <title>Test Store</title> 
//  <link>http://www.example.com</link> 
//  <description>An example item from the feed</description> 
//- <item>
//  <g:id>DB_1</g:id> 
//  <g:title>Dog Bowl In Blue</g:title> 
//  <g:description>Solid plastic Dog Bowl in marine blue color</g:description> 
//  <g:link>http://www.example.com/bowls/db-1.html</g:link> 
//  <g:image_link>http://images.example.com/DB_1.png</g:image_link> 
//  <g:brand>Example</g:brand> 
//  <g:condition>new</g:condition> 
//  <g:availability>in stock</g:availability> 
//  <g:price>9.99 GBP</g:price> 
//- <g:shipping>
//  <g:country>UK</g:country> 
//  <g:service>Standard</g:service> 
//  <g:price>4.95 GBP</g:price> 
//  </g:shipping>
//  <g:google_product_category>Animals > Pet Supplies</g:google_product_category> 
//  <g:custom_label_0>Made in Waterford, IE</g:custom_label_0> 
//  </item>
//  </channel>
//  </rss>";
        $BASE_URL = Yii::$app->params['BASE_URL'];
// $note = <<<XML
//<rss xmlns:g='http://base.google.com/ns/1.0' version='2.0'>
//- <channel>
//  <title>Test Store</title>
//      <link>$BASE_URL</link> 
//  <description>An item from the feed</description>
//                $items_xml
//  </channel>
//  </rss>
//XML;

        $note = <<<XML
<rss xmlns:g='http://base.google.com/ns/1.0' version='2.0'>
- <channel>
  <title>Test Store</title> 
  <link>$BASE_URL</link> 
  <description>An item from the feed</description> 
XML;
        $note .= <<<XML
                $items_xml
  </channel>
  </rss>
XML;

        $xml = new \SimpleXMLElement($note);
        echo $xml->asXML();
//echo $note;die;
//header('Content-Type: application/xml');
//$output = "<root><name>sample_name</name></root>";
//print ($output);
// echo 'hdfas';
//   die;
    }

    function actionSavechannel() {
        if (isset($_POST) and ! empty($_POST) and isset($_POST['feed'])) {
            $id = Yii::$app->user->identity->id;
            $facebook = \backend\models\Channels::find()->where(['channel_name' => 'Facebook'])->one();
//echo'<pre>';
//print_r($lazada);die;
            $channel_id = $facebook->channel_ID;
//echo $channel_id;die;
            $checkConnection = \backend\models\ChannelConnection::find()->Where(['user_id' => Yii::$app->user->identity->id, 'channel_id' => $channel_id])->one();
            if ($_POST['feed'] == 'yes') {
                if (empty($checkConnection)) {
                    $checkConnection = new \backend\models\ChannelConnection();
                    $checkConnection->channel_id = $channel_id;
                    $checkConnection->user_id = $id;
                    $checkConnection->elliot_user_id = $id;
                    $checkConnection->import_status = 'Completed';
                }
                $checkConnection->connected = 'yes';
            }
            else {
                $checkConnection->connected = 'no';
            }
            $checkConnection->save(false);
            $channel_connection_id = $checkConnection->channel_connection_id;
            $channel_details_check = StoreDetails::find()->where(['channel_connection_id' => $channel_connection_id])->one();
            if(empty($channel_details_check)){
                $save_store_details = new StoreDetails();
                $save_store_details->channel_connection_id = $channel_connection_id;
                $save_store_details->store_name = 'Facebook';
                $save_store_details->store_url = '';
                $save_store_details->country = 'United States';
                $save_store_details->country_code = 'US';
                $save_store_details->currency = 'USD';
                $save_store_details->currency_symbol = '$';
                $save_store_details->channel_accquired = 'Facebook';
                $save_store_details->created_at = date('Y-m-d H:i:s', time());
                $save_store_details->save(false);
            }
        }
    }
    
    function actionTestCall() {

         $connection = \Yii::$app->db;
         $currency_symbols_array = array(
	'AED' => '&#1583;.&#1573;', // ?
	'AFN' => '&#65;&#102;',
	'ALL' => '&#76;&#101;&#107;',
	'AMD' => '',
	'ANG' => '&#402;',
	'AOA' => '&#75;&#122;', // ?
	'ARS' => '&#36;',
	'AUD' => '&#36;',
	'AWG' => '&#402;',
	'AZN' => '&#1084;&#1072;&#1085;',
	'BAM' => '&#75;&#77;',
	'BBD' => '&#36;',
	'BDT' => '&#2547;', // ?
	'BGN' => '&#1083;&#1074;',
	'BHD' => '.&#1583;.&#1576;', // ?
	'BIF' => '&#70;&#66;&#117;', // ?
	'BMD' => '&#36;',
	'BND' => '&#36;',
	'BOB' => '&#36;&#98;',
	'BRL' => '&#82;&#36;',
	'BSD' => '&#36;',
	'BTN' => '&#78;&#117;&#46;', // ?
	'BWP' => '&#80;',
	'BYR' => '&#112;&#46;',
	'BZD' => '&#66;&#90;&#36;',
	'CAD' => '&#36;',
	'CDF' => '&#70;&#67;',
	'CHF' => '&#67;&#72;&#70;',
	'CLF' => '', // ?
	'CLP' => '&#36;',
	'CNY' => '&#165;',
	'COP' => '&#36;',
	'CRC' => '&#8353;',
	'CUP' => '&#8396;',
	'CVE' => '&#36;', // ?
	'CZK' => '&#75;&#269;',
	'DJF' => '&#70;&#100;&#106;', // ?
	'DKK' => '&#107;&#114;',
	'DOP' => '&#82;&#68;&#36;',
	'DZD' => '&#1583;&#1580;', // ?
	'EGP' => '&#163;',
	'ETB' => '&#66;&#114;',
	'EUR' => '&#8364;',
	'FJD' => '&#36;',
	'FKP' => '&#163;',
	'GBP' => '&#163;',
	'GEL' => '&#4314;', // ?
	'GHS' => '&#162;',
	'GIP' => '&#163;',
	'GMD' => '&#68;', // ?
	'GNF' => '&#70;&#71;', // ?
	'GTQ' => '&#81;',
	'GYD' => '&#36;',
	'HKD' => '&#36;',
	'HNL' => '&#76;',
	'HRK' => '&#107;&#110;',
	'HTG' => '&#71;', // ?
	'HUF' => '&#70;&#116;',
	'IDR' => '&#82;&#112;',
	'ILS' => '&#8362;',
	'INR' => '&#8377;',
	'IQD' => '&#1593;.&#1583;', // ?
	'IRR' => '&#65020;',
	'ISK' => '&#107;&#114;',
	'JEP' => '&#163;',
	'JMD' => '&#74;&#36;',
	'JOD' => '&#74;&#68;', // ?
	'JPY' => '&#165;',
	'KES' => '&#75;&#83;&#104;', // ?
	'KGS' => '&#1083;&#1074;',
	'KHR' => '&#6107;',
	'KMF' => '&#67;&#70;', // ?
	'KPW' => '&#8361;',
	'KRW' => '&#8361;',
	'KWD' => '&#1583;.&#1603;', // ?
	'KYD' => '&#36;',
	'KZT' => '&#1083;&#1074;',
	'LAK' => '&#8365;',
	'LBP' => '&#163;',
	'LKR' => '&#8360;',
	'LRD' => '&#36;',
	'LSL' => '&#76;', // ?
	'LTL' => '&#76;&#116;',
	'LVL' => '&#76;&#115;',
	'LYD' => '&#1604;.&#1583;', // ?
	'MAD' => '&#1583;.&#1605;.', //?
	'MDL' => '&#76;',
	'MGA' => '&#65;&#114;', // ?
	'MKD' => '&#1076;&#1077;&#1085;',
	'MMK' => '&#75;',
	'MNT' => '&#8366;',
	'MOP' => '&#77;&#79;&#80;&#36;', // ?
	'MRO' => '&#85;&#77;', // ?
	'MUR' => '&#8360;', // ?
	'MVR' => '.&#1923;', // ?
	'MWK' => '&#77;&#75;',
	'MXN' => '&#36;',
	'MYR' => '&#82;&#77;',
	'MZN' => '&#77;&#84;',
	'NAD' => '&#36;',
	'NGN' => '&#8358;',
	'NIO' => '&#67;&#36;',
	'NOK' => '&#107;&#114;',
	'NPR' => '&#8360;',
	'NZD' => '&#36;',
	'OMR' => '&#65020;',
	'PAB' => '&#66;&#47;&#46;',
	'PEN' => '&#83;&#47;&#46;',
	'PGK' => '&#75;', // ?
	'PHP' => '&#8369;',
	'PKR' => '&#8360;',
	'PLN' => '&#122;&#322;',
	'PYG' => '&#71;&#115;',
	'QAR' => '&#65020;',
	'RON' => '&#108;&#101;&#105;',
	'RSD' => '&#1044;&#1080;&#1085;&#46;',
	'RUB' => '&#1088;&#1091;&#1073;',
	'RWF' => '&#1585;.&#1587;',
	'SAR' => '&#65020;',
	'SBD' => '&#36;',
	'SCR' => '&#8360;',
	'SDG' => '&#163;', // ?
	'SEK' => '&#107;&#114;',
	'SGD' => '&#36;',
	'SHP' => '&#163;',
	'SLL' => '&#76;&#101;', // ?
	'SOS' => '&#83;',
	'SRD' => '&#36;',
	'STD' => '&#68;&#98;', // ?
	'SVC' => '&#36;',
	'SYP' => '&#163;',
	'SZL' => '&#76;', // ?
	'THB' => '&#3647;',
	'TJS' => '&#84;&#74;&#83;', // ? TJS (guess)
	'TMT' => '&#109;',
	'TND' => '&#1583;.&#1578;',
	'TOP' => '&#84;&#36;',
	'TRY' => '&#8356;', // New Turkey Lira (old symbol used)
	'TTD' => '&#36;',
	'TWD' => '&#78;&#84;&#36;',
	'TZS' => '',
	'UAH' => '&#8372;',
	'UGX' => '&#85;&#83;&#104;',
	'USD' => '&#36;',
	'UYU' => '&#36;&#85;',
	'UZS' => '&#1083;&#1074;',
	'VEF' => '&#66;&#115;',
	'VND' => '&#8363;',
	'VUV' => '&#86;&#84;',
	'WST' => '&#87;&#83;&#36;',
	'XAF' => '&#70;&#67;&#70;&#65;',
	'XCD' => '&#36;',
	'XDR' => '',
	'XOF' => '',
	'XPF' => '&#70;',
	'YER' => '&#65020;',
	'ZAR' => '&#82;',
	'ZMK' => '&#90;&#75;', // ?
	'ZWL' => '&#90;&#36;',
);
         
         foreach($currency_symbols_array as $key => $value) {
//             echo strtolower($key) . $value;
//             echo '<br>';
    $insert_symbol = $connection->createCommand('INSERT INTO currency_symbols (name, symbol) VALUES("'. strtolower($key) .'", "'. $value .'")');
    $query = $insert_symbol->query();
           
         }
         
         
         
    }

}
