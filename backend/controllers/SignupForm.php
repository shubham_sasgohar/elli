<?php
namespace backend\models;

use yii\base\Model;
use common\models\User;
use common\models\CustomFunction;


/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $company_name;
    public $email;
    public $password;
    public $confirm_password;
    public $new_password;
    public $old_password;
    public $new_confirm_password;
    public $remember;

const SCENARIO_CREATE = 'changepassword';
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'checkUsername'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            
            ['company_name', 'trim'],
            ['company_name', 'required'],
            ['company_name', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This Company has already been taken.'],
            ['company_name', 'string', 'min' => 2, 'max' => 255],
            

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],
            ['email', 'checkEmail'],
         

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            
            [['old_password'], 'required', 'on' => self::SCENARIO_CREATE],
            [['old_password'], 'string','min' => 6, 'on' => self::SCENARIO_CREATE],
//            ['old_password', 'myvalidation', 'on' => self::SCENARIO_CREATE],
            
             // an inline validator defined as an anonymous function
            
            
            
            [['new_password'], 'required', 'on' => self::SCENARIO_CREATE],
            [['new_password'], 'string','min' => 6, 'on' => self::SCENARIO_CREATE],
            
            [['new_confirm_password'], 'required', 'on' => self::SCENARIO_CREATE],
            [['new_confirm_password'], 'string','min' => 6, 'on' => self::SCENARIO_CREATE],
            [['new_confirm_password'], 'compare', 'compareAttribute'=>'new_password', 'message'=>"Passwords don't match" , 'on' => self::SCENARIO_CREATE],
            
            ['confirm_password', 'required'],
            ['confirm_password', 'string', 'min' => 6],
            ['confirm_password', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],
            
            array('remember', 'compare', 'compareValue' => 1, 'message' => 'You should accept term to use our service'),
        ];
    }
    
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['changepassword'] = ['new_password','new_confirm_password','old_password'];//Scenario Values Only Accepted
        return $scenarios;
    }


    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->company_name = $this->company_name;
        $user->email = $this->email;
        $domain =CustomFunction::getemaildomain($this->email);
        $user->domain_name =$domain;
        $user->setPassword($this->password);
        $user->generateAuthKey();
  

        return $user->save() ? $user : null;
    }
    
        public function checkEmail($attribute, $params)
    {
            $new_user_email=$this->email;
            /*explode a user new email*/
            $explode_new_user_email=explode("@",$new_user_email);
            $new_user_email=$explode_new_user_email[1];
            $match_new_user_email='@'.$new_user_email;
            $users_data = User::find()->where('email LIKE :query') ->addParams([':query'=>'%'.$match_new_user_email])->andWhere(['role'=>'merchant'])->all();
            if(!empty($users_data)) {
                // no real check at the moment to be sure that the error is triggered
                $this->addError('email','This Email Domain has been already exist');                
                 // $this->addError($attribute, Yii::t('user', 'You entered an invalid date format.'));
            }
    }
    

    
//    public function myvalidation($attribute, $params)
//    {
//        die('here');
//            $old_password=$this->old_password;
//            echo $old_password;
//            die;
//            $return=$this->validatePassword($old_password);
//            echo'<pre>';
//            print_r($return);
//            die;
//            
//    }
    

    
        public function checkUsername($attribute, $params)
    {
            
            $new_user_username=$this->username;
            
            $validate_username = strpos($new_user_username, '.');
            if($validate_username == true)
            {
                $this->addError('username','Invalid Username'); 
            }
           

    }
    
    
}
