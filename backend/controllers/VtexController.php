<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;    
use backend\models\User;
use backend\models\StoresConnection;
use backend\models\Stores;
use backend\models\Categories;
use backend\models\Variations;
use backend\models\VariationsItemList;
use backend\models\Products;
use backend\models\OrdersProducts;
use backend\models\OrderChannel;
use backend\models\CustomerUser;
use backend\models\Notification;
use backend\models\ProductCategories;
use backend\models\ProductVariation;
use backend\models\ProductImages;
use backend\models\ProductChannel;
use backend\models\Orders;
use backend\models\Channels;
use backend\models\MagentoStores;
use common\models\CustomFunction;
use backend\models\ProductAbbrivation;
use backend\models\CategoryAbbrivation;
use backend\models\CronTasks;
use backend\controllers\ChannelsettingController;
use yii\web\Session;
use backend\models\Countries;
use backend\models\StoreDetails;
use backend\models\CustomerAbbrivation;
use backend\models\Channelsetting;

class VtexController extends \yii\web\Controller
{
    /**
    * @inheritdoc
    */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','login', 'test', 'vtex-importing', 'get-vtex-warehouses', 'change-product-price', 'change-vtex-product-qty', 'vtex-cron-importing', 'vtex-cron-importing-auth'],
                'rules' => [
                        [
                        /* action hit without log-in */
                        'actions' => ['login', 'test', 'vtex-importing', 'get-vtex-warehouses', 'change-product-price', 'change-vtex-product-qty', 'vtex-cron-importing', 'vtex-cron-importing-auth'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                        [
                        /* action hit only with log-in */
                        'actions' => ['index', 'test', 'vtex-importing', 'get-vtex-warehouses', 'change-product-price', 'change-vtex-product-qty', 'vtex-cron-importing', 'vtex-cron-importing-auth'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /**
     * @return wizard form of vtex
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    /**
    * Authentication of Vtex form
    */
    public function actionAuthVtex(){
        //echo "<pre>"; print_r($_POST); die('End here!');
        $array_msg = array();
        $vtex_account = $_POST['vtex_account'];
        $vtex_app_key = $_POST['vtex_app_key'];
        $vtex_app_token = $_POST['vtex_app_token'];
        $user_id = $_POST['user_id'];
        $vtex_country = $_POST['vtex_country'];
        
        $country = Countries::countryDetails($vtex_country);
        
        //$url  = 'http://'.$vtex_account.'.com.br/api/catalog_system/pub/category/tree/1000';
        //$url  = 'http://'.$vtex_account.'.com.br/api/catalog_system/pvt/category/10';
        $url  = 'http://'.$vtex_account.'.com.br/api/catalog_system/pvt/brand/list';
        
        $response = $this->vtexCurl($url, $vtex_app_key, $vtex_app_token);
        if(!is_scalar($response)){
            //echo "<pre>"; print_r($response); echo "<pre>";
            if(isset($response['error']['message'])){
                $array_msg['api_error'] = "Your App Key and App Token is not correct. It's giving Unauthorized access. Please check and try again.";
                return json_encode($array_msg);
            }
            else{
                
                $check_api_key = StoresConnection::find()->where(['vtex_account_name' => $vtex_account, 'vtex_app_key' => $vtex_app_key, 'vtex_app_token' => $vtex_app_token])->one();
                if (!empty($check_api_key)) {
                    $array_msg['api_error'] = 'This Vtex shop is already integrated with Elliot. Please use another API details to integrate with Elliot.';
                    return json_encode($array_msg);
                }
                $check_country = StoreDetails::find()->where(['country' => $country->name, 'channel_accquired' => 'VTEX'])->one();
                if (!empty($check_country)) {
                    $array_msg['api_error'] = 'This Vtex shop with same country <b>' . $country->name . '</b> is already integrated with Eliiot.';
                    return json_encode($array_msg);
                }
                
                $user = User::find()->where(['id' => $user_id])->one();
                $user_domain = $user->domain_name;
                $config = yii\helpers\ArrayHelper::merge(
                    require($_SERVER['DOCUMENT_ROOT'].'/common/config/main.php'),
                    require($_SERVER['DOCUMENT_ROOT'].'/common/users_config/' . $user_domain . '/main-local.php'),
                    require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main.php'),
                    require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main-local.php')
                );
                $application = new yii\web\Application($config);
                $store = Stores::find()->select('store_id')->where(['store_name' => 'VTEX'])->one();
                $vtex_store_id = $store->store_id;
                //$checkConnection = StoresConnection::find()->where(['store_id' => $vtex_store_id, 'user_id' => $user_id])->one();
                //if (empty($checkConnection)){
                    $connectionModel = new StoresConnection();
                    $connectionModel->store_id = $vtex_store_id;
                    $connectionModel->user_id = $user_id;
                    $connectionModel->connected = 'Yes';
                    $connectionModel->vtex_account_name = $vtex_account;
                    $connectionModel->vtex_app_key = $vtex_app_key;
                    $connectionModel->vtex_app_token = $vtex_app_token;
                    $created_date = date('Y-m-d h:i:s', time());
                    $connectionModel->created = $created_date;
                    if ($connectionModel->save()){
                        $array_msg['success'] = "Your Vtex shop has been connected successfully. Importing is started. Once importing is done you will get a nofiy message.";
                        $array_msg['store_connection_id'] = $connectionModel->stores_connection_id;
                        return json_encode($array_msg);
                    }
                    else{
                        $array_msg['api_error'] = "Error Something went wrong. Please try again";
                        return json_encode($array_msg);
                    }
                //}
            }
        }
        else{
            $array_msg['api_error'] = "It's look like you have enter wrong Vtex account name. Please check and try again.";
            return json_encode($array_msg);
        }
    }
    
    /**
    * Curl function for vtex
    */
    
    public function vtexCurl($url, $appkey, $apptoken) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => FALSE,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/json",
                "x-vtex-api-appkey: " . $appkey,
                "x-vtex-api-apptoken: " . $apptoken
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #: " . $err;
        } else {
            return json_decode($response, true);
        }
    }
    /***
     * Vtex Importing.
     */
    public function actionVtexImporting(){
        $user_id = $_GET['user_id'];
        $store_connection_id = $_GET['store_connection_id'];
        $vtex_country = $_GET['vtex_country'];
        $get_users = User::find()->select('domain_name,company_name,email')->where(['id' => $user_id])->one();
        $email = $get_users->email;
        $company_name = $get_users->company_name;
        $config = yii\helpers\ArrayHelper::merge(
            require($_SERVER['DOCUMENT_ROOT'].'/common/config/main.php'),
            require($_SERVER['DOCUMENT_ROOT'].'/common/users_config/' . $get_users->domain_name . '/main-local.php'), 
            require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main.php'), 
            require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);
        $get_vtex_id = Stores::find()->select('store_id')->where(['store_name' => 'VTEX'])->one();
        $vtex_store_id = $get_vtex_id->store_id;
        $store_vtex = StoresConnection::find()->where(['store_id' => $vtex_store_id, 'user_id' => $user_id, 'stores_connection_id' => $store_connection_id])->one();
        if(!empty($store_vtex)){
            $vtex_account = $store_vtex->vtex_account_name;
            $vtex_app_key = $store_vtex->vtex_app_key;
            $vtex_app_token = $store_vtex->vtex_app_token;
            $country_detail = Countries::countryDetails($vtex_country);
            
            $category_url = 'http://'.$vtex_account.'.com.br/api/catalog_system/pub/category/tree/1000';
            $category_response = $this->vtexCurl($category_url, $vtex_app_key, $vtex_app_token);
            if(!is_scalar($category_response)){
                //echo "<pre>"; print_r($category_response); die;
                $this->vtexStoresDetails($vtex_account, $store_connection_id, $country_detail);
                $this->vtexCategoryImporting($category_response, $user_id, $store_connection_id);
                $parent = '';
                $this->vtexParentCategoryAssign($category_response, $parent, $user_id, $store_connection_id);
                $this->vtexProductImporting($vtex_account, $vtex_app_key, $vtex_app_token, $user_id, $store_connection_id, $country_detail);
                $this->vtexCustomerImporting($vtex_account, $vtex_app_key, $vtex_app_token, $user_id, $store_connection_id);
                $this->vtexOrderImporing($vtex_account, $vtex_app_key, $vtex_app_token, $user_id, $store_connection_id, $country_detail, $vtex_store_id);
                $this->vtexCurrencydetails($user_id, $store_connection_id, $country_detail, 'Vtex', $vtex_store_id);
                
                /* Send Mail for conformation of Importing done */
                $email_message = 'Success, Your VTEX Store is Now Connected';
                $email = 'noor.mohamad@brihaspatitech.com';
                $send_email_notif = CustomFunction::ConnectBigCommerceEmail($email, $company_name, $email_message);

                //Drop-Down Notification for User
                $notif_type = 'VTEX';
                $notif_db = Notification::find()->Where(['notif_type' => $notif_type])->one();
                if (empty($notif_db)){
                    $notification_model = new Notification();
                    $notification_model->user_id = $user_id;
                    $notification_model->notif_type = $notif_type;
                    $notification_model->notif_description = 'Your VTEX store data has been successfully imported.';
                    $notification_model->created_at = date('Y-m-d h:i:s', time());
                    $notification_model->save(false);
                }
                //Sticky Notification :StoresConnection Model Entry for Import Status
                $get_rec = StoresConnection::find()->Where(['user_id' => $user_id, 'store_id' => $vtex_store_id, 'stores_connection_id' => $store_connection_id])->one();
                $get_rec->import_status = 'Completed';
                $get_rec->save(false);
                
                $cron_task_vtex = CronTasks::find()->where(['elliot_user_id' => $user_id, 'task_source' => 'Vtex'])->one();
                if(empty($cron_task_vtex)){
                    $CronTasks = new CronTasks();
                    $CronTasks->elliot_user_id = $user_id;
                    $CronTasks->task_name = 'import';
                    $CronTasks->task_source = 'Vtex';
                    $CronTasks->status = 'Completed';
                    $CronTasks->created_at = date('Y-m-d h:i:s', time());
                    $CronTasks->updated_at = date('Y-m-d h:i:s', time());
                    $CronTasks->save();
                }
            }
        }
    }
    
    public function vtexCurrencydetails($user_id, $store_connection_id, $country_detail, $store_name, $vtex_store_id) {
        $currency_channel_exists = Channelsetting::find()->Where(['mul_store_id' => $store_connection_id, 'setting_key' => 'currency', 'user_id' => $user_id])->one();
        if (empty($currency_channel_exists)) {
            $channel_setting = new Channelsetting();
            $channel_setting->store_id = $vtex_store_id;
            $channel_setting->user_id = $user_id;
            $channel_setting->channel_name = $store_name;
            $channel_setting->setting_key = 'currency';
            $channel_setting->setting_value = $country_detail->currency_code;
            $channel_setting->mul_store_id = $store_connection_id;
            $channel_setting->created_at = date('Y-m-d H:i:s');
            $channel_setting->save(false);
        }
    }
    
    public function vtexStoresDetails($vtex_account, $store_connection_id, $country_detail) {
        $check_store_detail = StoreDetails::find()->where(['store_connection_id' => $store_connection_id])->one();
        if (empty($check_store_detail)) {
            $save_store_details = new StoreDetails();
            $save_store_details->store_connection_id = $store_connection_id;
            $save_store_details->store_name = '';
            $save_store_details->store_url = $vtex_account;
            $save_store_details->country = $country_detail->name;
            $save_store_details->country_code = $country_detail->sortname;
            $save_store_details->currency = $country_detail->currency_code;
            $save_store_details->currency_symbol = $country_detail->currency_code;
            $save_store_details->others = '';
            $save_store_details->channel_accquired = 'VTEX';
            $save_store_details->created_at = date('Y-m-d H:i:s', time());
            $save_store_details->save(false);
        }
    }
    /**
     * Insert category from Vtex store
     * @param array $category_list
     * @param int $user_id
     */
    public function vtexCategoryImporting($category_list, $user_id, $store_connection_id){
        foreach($category_list as $cat){
            $cat_id = $cat['id'];
            $cat_name = $cat['name'];
            
            $category_data = array(
                'category_id'=>$cat_id,     // Give category id of Store/channels
                'parent_id'=>0,     // Give Category parent id of Elliot if null then give 0
                'name'=>$cat_name,   // Give category name
                'channel_store_name'=>'VTEX',   // Give Channel/Store name
                'channel_store_prefix'=>'VT',  // Give Channel/Store prefix id
                'mul_store_id' => $store_connection_id, // store connection id
                'mul_channel_id' => '', // channel connectin id
                'elliot_user_id'=>$user_id,     // Give Elliot user id
                'created_at'=>date('Y-m-d H:i:s'),  // Give Created at date if null then give current date format date('Y-m-d H:i:s')
                'updated_at'=>date('Y-m-d H:i:s'),  // Give Updated at date if null then give current date format date('Y-m-d H:i:s')
            );
            Stores::categoryImportingCommon($category_data);    // Then call store modal function and give $castegory data
            $child = $cat['children'];
            if(count($child)>0){
                $this->vtexCategoryImporting($child, $user_id, $store_connection_id);
            }
            
            /*$checkCategoryName = Categories::find()->where(['category_name' => $cat_name])->one();
            if(empty($checkCategoryName))
            {
                    $categoryModel = new Categories();
                    $categoryModel->channel_abb_id = '';
                    $categoryModel->category_name = $cat_name;
                    $categoryModel->parent_category_ID = 0;
                    $categoryModel->elliot_user_id = $user_id;
                    $created_date = date('Y-m-d h:i:s', time());
                    $categoryModel->created_at = $created_date;
                    if($categoryModel->save())
                    {
                        $CategoryAbbrivation = new CategoryAbbrivation();
                        $CategoryAbbrivation->channel_abb_id = 'VT'.$cat_id;
                        $CategoryAbbrivation->category_ID = $categoryModel->category_ID;
                        $CategoryAbbrivation->channel_accquired = 'VTEX';
                        $CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
                        $CategoryAbbrivation->save(false);
                    }
            }
            else
            {
                $CategoryAbbrivation = new CategoryAbbrivation();
                $CategoryAbbrivation->channel_abb_id = 'VT'.$cat_id;
                $CategoryAbbrivation->category_ID = $checkCategoryName->category_ID;
                $CategoryAbbrivation->channel_accquired = 'VTEX';
                $CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
                $CategoryAbbrivation->save(false);
            }
            $child = $cat['children'];*/
        }
    }
    /**
     * Parent category assign
     * @param array $category_list
     * @param int $parent_id
     * @param int $user_id
     */
    public function vtexParentCategoryAssign($category_list, $parent_id, $user_id, $store_connection_id){
        foreach($category_list as $cat){
            $cat_id = $cat['id'];
            $cat_name = $cat['name'];
            if($parent_id!=''){
                $checkCatModel = CategoryAbbrivation::find()->where(['channel_abb_id' => 'VT'.$cat_id, 'mul_store_id' => $store_connection_id])->one();
                if(!empty($checkCatModel)):
                    $elliot_cat_id = $checkCatModel->category_ID;
                    $checkParentModel = CategoryAbbrivation::find()->where(['channel_abb_id' => 'VT'.$parent_id, 'mul_store_id' => $store_connection_id])->one();
                    if(!empty($checkParentModel)):
                        $categoey_model = Categories::find()->where(['category_ID' => $elliot_cat_id])->one();
                        $elliot_parent_id = $checkParentModel->category_ID;
                        $categoey_model->parent_category_ID = $elliot_parent_id;
                        $categoey_model->save();
                    endif;
                endif;
            }
            $child = $cat['children'];
            if(count($child)>0)
            {
                $this->vtexParentCategoryAssign($child, $cat_id, $user_id, $store_connection_id);
            }
        }
    }
    
    /**
     * Vtex product importing
     * @param string $vtex_account
     * @param string $vtex_app_key
     * @param string $vtex_app_token
     * @param int $user_id
     */
    public function vtexProductImporting($vtex_account, $vtex_app_key, $vtex_app_token, $user_id, $store_connection_id, $country_detail) {
        $category_list = CategoryAbbrivation::find()->select('channel_abb_id')->where(['channel_accquired'=>'VTEX', 'mul_store_id' => $store_connection_id])->all();
        $count = 1;
        $product_ids = array();
        foreach($category_list as $cat){
            $cat_id = substr($cat->channel_abb_id, 2);
            $_from = 1;
            $_to = 50;
            $url = "http://".$vtex_account.".com.br/api/catalog_system/pvt/products/GetProductAndSkuIds?categoryId=".$cat_id."&_from=".$_from."&_to=".$_to;
            $product_data = $this->vtexCurl($url,$vtex_app_key,$vtex_app_token);
            //echo "<pre>"; print_r($product_data); die;
            $total_product_count = $product_data['range']['total'];
            $total_loop = ceil($total_product_count/50);
            for($pro=1;$pro<=$total_loop;$pro++){
                $url = "http://".$vtex_account.".com.br/api/catalog_system/pvt/products/GetProductAndSkuIds?categoryId=".$cat_id."&_from=".$_from."&_to=".$_to;
                $product_data = $this->vtexCurl($url,$vtex_app_key,$vtex_app_token);
                foreach($product_data['data'] as $key => $value){
                    if(!in_array($key, $product_ids)){
                        $product_ids[] = $key;
                    }
                }
                $_from = $_from+50;
                $_to = $_to+50;
            }
        }
        $brand_list = $this->getVtexBrandList($vtex_account, $vtex_app_key, $vtex_app_token);
        $this->productInsert($vtex_account, $vtex_app_key, $vtex_app_token, $product_ids, $brand_list, $user_id, $store_connection_id, $country_detail);
    }
    
    public function productInsert($vtex_account, $vtex_app_key, $vtex_app_token, $product_ids, $brand_list, $user_id, $store_connection_id, $country_detail){
        sort($product_ids);
        $cc= 1;
        foreach($product_ids as $pid){
            $product_url = 'http://'.$vtex_account.'.com.br/api/catalog_system/pvt/products/ProductGet/'.$pid;
            $product_variation_url = 'http://'.$vtex_account.'.com.br/api/catalog_system/pub/products/variations/'.$pid;
            $product_data = $this->vtexCurl($product_url, $vtex_app_key, $vtex_app_token);
            $product_variation = $this->vtexCurl($product_variation_url, $vtex_app_key, $vtex_app_token);
            //echo "<pre>"; print_r($product_data); print_r($product_variation);
            $product_id = $product_data['Id'];
            $name = $product_data['Name'];
            $url_link_id = $product_data['LinkId'];
            $category_id = $product_data['CategoryId'];
            $brand_id = $product_data['BrandId'];
            $description = $product_data['Description'];
            $created_at = date("Y-m-d H:i:s", strtotime($product_data['ReleaseDate']));
            $product_status = ($product_data['IsVisible']==1) ? $product_data['IsVisible'] : 0;
            $product_active_status = $product_data['IsActive'];
            $brand_name = '';
            
            if(!is_scalar($product_variation)){
                foreach($brand_list as $_brand){
                    if($_brand['id']==$brand_id){
                        $brand_name = $_brand['name'];
                    }
                }

                $count=1;
                if(isset($product_variation['skus'])){
                    $product_sku_data = $product_variation['skus'];
                    foreach($product_sku_data as $_product_sku){
                        $sku_id = $_product_sku['sku'];
                        $sku_name = $_product_sku['skuname'];
                        $product_price = $_product_sku['bestPriceFormated'];
                        $product_price = substr($product_price, 1);
                        $product_price = str_replace(',', '',$product_price);
                        $product_image_url = $_product_sku['image'];
                        $product_stock = ($_product_sku['available']==1) ? $_product_sku['available'] : 0;
                        $product_qty = $_product_sku['availablequantity'];
                        $product_weight = $_product_sku['measures']['cubicweight'];

                        if($count==1){
                            $product_image_data = array();
                            if($product_image_url!=''){
                                $product_image_data[] = array(
                                    'image_url'=>$product_image_url,
                                    'label'=>'',
                                    'position'=>1,
                                    'base_img'=>$product_image_url,
                                );
                            }

                            $store_id = Stores::find()->select('store_id')->where(['store_name' => 'VTEX'])->one();
                            $product_data = array(
                                'product_id'=>$product_id, // Stores/Channel product ID
                                'name'=>$name,  // Product name
                                'sku'=>$sku_id,    // Product SKU
                                'description'=>$description,    // Product Description
                                'product_url_path'=>$vtex_account.".com.br/".$url_link_id."/p",   // Product url if null give blank value
                                'weight'=>$product_weight,  // Product weight if null give blank value
                                'status'=>$product_status,  // Product status (visible or hidden). Give in 0 or 1 form (0 = hidden, 1 = visible).
                                'price'=>$product_price,    // Porduct price
                                'sale_price'=>$product_price,   // Product sale price if null give Product price value
                                'qty'=>$product_qty,    //Product quantity 
                                'stock_status'=>$product_stock,  // Product stock status ("in stock" or "out of stock"). 
                                                                // Give in 0 or 1 form (0 = out of stock, 1 = in stock)
                                'websites'=>array(),    //This is for only magento give only and blank array
                                'brand'=>$brand_name,    // Product brand if any
                                'low_stock_notification'=>5,    // Porduct low stock notification if any otherwise give default 5 value
                                'created_at'=>$created_at,  // Product created at date format date('Y-m-d H:i:s')
                                'updated_at'=>$created_at,  // Product updated at date format date('Y-m-d H:i:s')
                                'channel_store_name'=>'VTEX',   // Channel or Store name
                                'channel_store_prefix'=>'VT',      // Channel or store prefix id
                                'mul_store_id' => $store_connection_id, //Give multiple store id
                                'mul_channel_id' => '', // Give multiple channel id
                                'elliot_user_id'=>$user_id,         // Elliot user id
                                'store_id'=>$store_id->store_id,    // if your are importing store give store id
                                'channel_id'=>'',   // if your are importing channel give channel id
                                'country_code' => $country_detail->sortname,
                                'currency_code' => $country_detail->currency_code,
                                'barcode'=>'',  // Product barcode if any
                                'ean'=>'',  // Product ean if any
                                'jan'=>'',  // Product jan if any
                                'isban'=>'',    // Product isban if any
                                'mpn'=>'',  // Product mpn if any
                                'categories'=>array($category_id), // Product categroy array. If null give a blank array 
                                'images'=>$product_image_data,  // Product images data
                            );
                            $product_check = ProductAbbrivation::find()->Where(['channel_abb_id' => 'VT'.$product_id, 'mul_store_id' => $store_connection_id])->one();
                            if(empty($product_check)){
                                Stores::productImportingCommon($product_data);
                            }
                            else{
                                Stores::producUpdateCommon($product_data);
                            }
                            
                        }
                        $count++;
                    }
                }
            }
        }
    }
    
    public function getVtexBrandList($vtex_account, $vtex_app_key, $vtex_app_token){
        $product_brand_url = 'http://'.$vtex_account.'.com.br/api/catalog_system/pvt/brand/list';
        $brand_list = $this->vtexCurl($product_brand_url, $vtex_app_key, $vtex_app_token);
        return $brand_list;
    }
    /**
     * Vtex Order Importing
     * @param str $vtex_account
     * @param str $vtex_app_key
     * @param str $vtex_app_token
     * @param int $user_id
     */
    public function vtexOrderImporing($vtex_account, $vtex_app_key, $vtex_app_token, $user_id, $store_connection_id, $country_detail, $vtex_store_id) { 
        $order_list_url = "http://".$vtex_account.".com.br/api/oms/pvt/orders?per_page=100";
        $order_collection = $this->vtexCurl($order_list_url, $vtex_app_key, $vtex_app_token);
        $total_pages = $order_collection['paging']['pages'];
        for($i=1; $i<=$total_pages;$i++){
            $order_list_url = "http://".$vtex_account.".com.br/api/oms/pvt/orders?per_page=100&page=".$i;
            $order_collection = $this->vtexCurl($order_list_url, $vtex_app_key, $vtex_app_token);
            //echo count($order_collection_list['list']);
            $order_collection_list = $order_collection['list'];
            foreach($order_collection_list as $order_data){
                $order_id = $order_data['orderId'];
                $order_data_url = "http://".$vtex_account.".com.br/api/oms/pvt/orders/".$order_id;
                $order_collection = $this->vtexCurl($order_data_url, $vtex_app_key, $vtex_app_token);
                //echo "<pre>"; print_r($order_collection); die('End herere!!');
                $order_id = $order_collection['orderId'];
                $origin = $order_collection['origin'];
                $salesChannel = $order_collection['salesChannel'];
                $order_status = $order_collection['status'];
                $order_total = $this->vtexPriceFormat($order_collection['value']);
                $order_created_at = date("Y-m-d h:i:s", strtotime($order_collection['creationDate']));
                $order_updated_at = date("Y-m-d h:i:s", strtotime($order_collection['lastChange']));
                $group_price = $order_collection['totals'];
                
                switch (strtolower($order_status)) {
                    case "handling":
                        $order_status_2 = 'In Transit';
                        break;
                    case "ready-for-handling":
                        $order_status_2 = "In Transit";
                        break;
                    case "invoiced":
                        $order_status_2 = "In Transit";
                        break;
                    case "payment-pending":
                        $order_status_2 = "Pending";
                        break;
                    case "order-completed":
                        $order_status_2 = "Completed";
                        break;
                    case "payment-approved":
                        $order_status_2 = "In Transit";
                        break;
                    case "cancel":
                        $order_status_2 = "Cancel";
                        break;
                    case "cancellation-requested":
                        $order_status_2 = "Cancel";
                        break;
                    default:
                        $order_status_2 = "Pending";
                }
                
                $discount_price = '';
                $shipping_price = '';
                $tax_price = '';
                foreach($group_price as $_price)
                {
                    $price_type = $_price['id'];
                    if($price_type=='Discounts')
                    {
                        $discount_price = $this->vtexPriceFormat($_price['value']);
                    }
                    if($price_type=='Shipping')
                    {
                        $shipping_price = $this->vtexPriceFormat($_price['value']);
                    }
                    if($price_type=='Tax')
                    {
                        $tax_price = $this->vtexPriceFormat($_price['value']);
                    }
                }
                
                $payment_method = $order_collection['paymentData']['transactions'][0]['payments'][0]['paymentSystemName'];
                
                $customer_data = $order_collection['clientProfileData'];
                $email = $customer_data['email'];
                $firstName = $customer_data['firstName'];
                $lastName = $customer_data['lastName'];
                $phone = $customer_data['phone'];
                $vtex_customer_profile_id = $customer_data['userProfileId'];
                
                $shipping_data = $order_collection['shippingData'];
                $receiver_name = $shipping_data['address']['receiverName'];
                $zip_code = $shipping_data['address']['postalCode'];
                $city = $shipping_data['address']['city'];
                $state = $shipping_data['address']['state'];
                $country = $shipping_data['address']['country'];
                $street = $shipping_data['address']['street'];
                
                $shipping_address = $street.' '.$city.' '.$state.' '.$zip_code.' '.$country;
                $billing_address = $street.' '.$city.' '.$state.' '.$zip_code.' '.$country;
                
                $order_items = $order_collection['items'];
                $total_quantity_ordered = '';
                foreach($order_items as $_items){
                    $total_quantity_ordered += $_items['quantity'];
                }
                
                $order_items = $order_collection['items'];
                $order_product_data = array();
                foreach($order_items as $_items){
                    $product_id = $_items['productId'];
                    $quantity_ordered = $_items['quantity'];
                    $product_name = $_items['name'];
                    $product_price = $this->vtexPriceFormat($_items['price']);
                    $product_detail_url = 'http://'.$vtex_account.'.com.br/api/catalog_system/pub/products/variations/'.$product_id;
                    $product_detail = $this->vtexCurl($product_detail_url, $vtex_app_key, $vtex_app_token);
                    $product_sku = '';
                    if(!is_scalar($product_detail)){
                        $product_sku = $product_detail['skus'][0]['sku'];
                    }
                    $order_product_data[] = array(
                        'product_id'=> $product_id,
                        'name'=> $product_name,
                        'sku'=> $product_sku,
                        'price'=> $product_price,
                        'qty_ordered'=> $quantity_ordered,
                        'weight'=> '',
                    );
                }
                
                if($vtex_customer_profile_id==''){
                    $vtex_customer_profile_id = 0;
                }
                
                $customerCheckModel = CustomerAbbrivation::find()->Where(['channel_abb_id' => 'VT'.$vtex_customer_profile_id, 'mul_store_id' => $store_connection_id])->one();
                if(!empty($customerCheckModel)){
                    $customerCheckModel->bill_street_1 = $street;
                    $customerCheckModel->bill_street_2 = '';
                    $customerCheckModel->bill_city = $city;
                    $customerCheckModel->bill_state = $state;
                    $customerCheckModel->bill_zip = $zip_code;
                    $customerCheckModel->bill_country = $country;
                    $customerCheckModel->bill_country_iso = $country;
                    $customerCheckModel->ship_street_1 = $street;
                    $customerCheckModel->ship_street_2 = '';
                    $customerCheckModel->ship_city = $city;
                    $customerCheckModel->ship_state = $state;
                    $customerCheckModel->ship_zip = $zip_code;
                    $customerCheckModel->ship_country = $country;
                    $customerCheckModel->save(false);
                }
                
                
                $order_data = array(
                    'order_id'=> $order_id,
                    'status'=> $order_status_2,
                    'magento_store_id'=> '',
                    'order_grand_total'=>$order_total,
                    'customer_id'=> $vtex_customer_profile_id,
                    'customer_email'=> $email,
                    'order_shipping_amount'=> $shipping_price,
                    'order_tax_amount'=> $tax_price,
                    'total_qty_ordered'=> $total_quantity_ordered,
                    'created_at'=> $order_created_at,
                    'updated_at'=> $order_updated_at,
                    'payment_method'=> $payment_method,
                    'refund_amount'=> '',
                    'discount_amount'=> $discount_price,
                    'channel_store_name'=> 'VTEX',
                    'channel_store_prefix'=> 'VT',
                    'mul_store_id' => $store_connection_id,
                    'mul_channel_id' => '',
                    'elliot_user_id'=> $user_id,
                    'store_id'=>$vtex_store_id,
                    'channel_id'=>'',
                    'currency_code' => $country_detail->currency_code,
                    'shipping_address'=>array(
                        'street_1'=> $street,
                        'street_2'=> '',
                        'state'=> $state,
                        'city'=> $city,
                        'country'=> $country,
                        'country_id'=> $country,
                        'postcode'=> $zip_code,
                        'email'=> $email,
                        'telephone'=> $phone,
                        'firstname'=> $firstName,
                        'lastname'=> $lastName,
                    ),
                    'billing_address'=>array(
                        'street_1'=> $street,
                        'street_2'=> '',
                        'state'=> $state,
                        'city'=> $city,
                        'country'=> $country,
                        'country_id'=> $country,
                        'postcode'=> $zip_code,
                        'email'=> $email,
                        'telephone'=> $phone,
                        'firstname'=> $firstName,
                        'lastname'=> $lastName,
                    ),
                    'items'=>$order_product_data,
                );
                
                $check_order = Orders::find()->Where(['channel_abb_id' => 'VT'.$order_id, 'mul_store_id' => $store_connection_id])->one();
                if(empty($check_order)){
                    Stores::orderImportingCommon($order_data);
                }
                else{
                    if($order_status!='Completed' && $order_status!='Cancel'){
                        Stores::orderUpdateCommon($order_data);  
                    }
                }
                
                
                
//                $customerCheckModel = CustomerUser::find()->Where(['channel_abb_id' => 'VT'.$vtex_customer_profile_id])->one();
//                if(empty($customerCheckModel))
//                {
//                    if($vtex_customer_profile_id=='')
//                    {
//                       $vtex_customer_profile_id = '0000'; 
//                    }
//                    $customerCheckAnotherModel = CustomerUser::find()->Where(['channel_abb_id' => 'VT'.$vtex_customer_profile_id, 'email'=>$email])->one();
//                    if(empty($customerCheckAnotherModel))
//                    {
//                        $Customers_create_model = new CustomerUser();
//                        $Customers_create_model->channel_abb_id = 'VT'.$vtex_customer_profile_id;
//                        $Customers_create_model->first_name = $firstName;
//                        $Customers_create_model->last_name = $lastName;
//                        $Customers_create_model->email = $email;
//                        $Customers_create_model->channel_acquired = 'VTEX';
//                        $Customers_create_model->date_acquired = '';
//                        $Customers_create_model->created_at = date('Y-m-d h:i:s', time());
//                        $Customers_create_model->elliot_user_id = $user_id;
//                        $Customers_create_model->save(false);
//                        $elliot_customer_id = $Customers_create_model->customer_ID;
//                    }
//                    else
//                    {
//                        $elliot_customer_id = $customerCheckAnotherModel->customer_ID;
//                    }
//                }
//                else
//                {
//                    $customerCheckModel->street_1 = $street;
//                    $customerCheckModel->street_2 = '';
//                    $customerCheckModel->city = $city;
//                    $customerCheckModel->country = $country;
//                    $customerCheckModel->state = $state;
//                    $customerCheckModel->zip = $zip_code;
//                    $customerCheckModel->phone_number = $phone;
//                    $customerCheckModel->ship_street_1 = $street;
//                    $customerCheckModel->ship_street_2 = '';
//                    $customerCheckModel->ship_city = $city;
//                    $customerCheckModel->ship_state = $state;
//                    $customerCheckModel->ship_zip = $zip_code;
//                    $customerCheckModel->ship_country = $country;
//                    $customerCheckModel->save(false);
//                    $elliot_customer_id = $customerCheckModel->customer_ID;
//                }
//                
//                $check_order = Orders::find()->Where(['channel_abb_id' => 'VT'.$order_id])->one();
//                if(empty($check_order)) 
//                {
//                    $order_model = new Orders();
//                    $order_model->elliot_user_id = $user_id;
//                    $order_model->channel_abb_id = 'VT' . $order_id;
//                    $order_model->customer_id = $elliot_customer_id;
//                    $order_model->order_status = $order_status_2;
//                    $order_model->product_qauntity = $total_quantity_ordered;
//                    $order_model->shipping_address = $shipping_address;
//                    $order_model->ship_street_1 = $street;
//                    $order_model->ship_street_2 = '';
//                    $order_model->ship_city = $city;
//                    $order_model->ship_state = $state;
//                    $order_model->ship_zip = $zip_code;
//                    $order_model->ship_country = $country;
//                    $order_model->billing_address = $billing_address;
//                    $order_model->bill_street_1 = $street;
//                    $order_model->bill_street_2 = '';
//                    $order_model->bill_city = $city;
//                    $order_model->bill_state = $state;
//                    $order_model->bill_zip = $zip_code;
//                    $order_model->bill_country = $country;
//                    $order_model->base_shipping_cost = $shipping_price;
//                    $order_model->shipping_cost_tax = $tax_price;
//                    $order_model->payment_method = $payment_method;
//                    $order_model->payment_status = $order_status_2;
//                    $order_model->refunded_amount = '';
//                    $order_model->discount_amount = $discount_price;
//                    $order_model->total_amount = $order_total;
//                    $order_model->order_date = $order_created_at;
//                    $order_model->updated_at = $order_updated_at;
//                    //$order_model->created_at = date('Y-m-d h:i:s', time());
//                    $order_model->created_at = $order_created_at;
//                    $order_model->save(false);
//                    $last_order_id = $order_model->order_ID;                    
//
//                    $vtex_store = Stores::find()->where(['store_name' => 'VTEX'])->one();
//                    $vtex_store_id = $vtex_store->store_id;
//                    $order_channels_model = new OrderChannel();
//                    $order_channels_model->elliot_user_id = $user_id;
//                    $order_channels_model->order_id = $last_order_id;
//                    $order_channels_model->store_id = $vtex_store_id;
//                    $order_channels_model->created_at = date('Y-m-d h:i:s', time());
//                    $order_channels_model->save(false);
//                    $order_items = $order_collection['items'];
//                    foreach($order_items as $_items)
//                    {
//                        $product_id = $_items['productId'];
//                        $quantity_ordered = $_items['quantity'];
//                        $product_name = $_items['name'];
//                        $product_price = $this->vtexPriceFormat($_items['price']);
//                        $product_detail_url = 'http://'.$vtex_account.'.com.br/api/catalog_system/pub/products/variations/'.$product_id;
//                        $product_detail = $this->vtexCurl($product_detail_url, $vtex_app_key, $vtex_app_token);
//                        $product_sku = '';
//                        if(!is_scalar($product_detail))
//                        {
//                            $product_sku = $product_detail['skus'][0]['sku'];
//                        }
//                        $checkProductId = Products::find()->where(['product_name' => $product_name, 'sku' => $product_sku])->one();
//                        if(empty($checkProductId))
//                        {
//                            if($product_id=='')
//                            {
//                                $product_id = '0000';
//                            }
//                            $productModel = new Products ();
//                            $productModel->channel_abb_id = '';
//                            $productModel->product_name = $product_name;
//                            $productModel->SKU = $product_sku;
//                            $productModel->UPC = '';
//                            $productModel->EAN = '';
//                            $productModel->Jan = '';
//                            $productModel->ISBN = '';
//                            $productModel->MPN = '';
//                            $productModel->description = '';
//                            $productModel->adult = 'no';
//                            $productModel->age_group = NULL;
//                            $productModel->gender = 'Unisex';
//                            $productModel->brand = '';
//                            $productModel->stock_quantity = 1;
//                            $productModel->availability = 'Out of Stock';
//                            $productModel->stock_level = 'Out of Stock';
//                            $productModel->stock_status = 'Visible';
//                            $productModel->price = $product_price;
//                            $productModel->sales_price = $product_price;
//                            $productModel->elliot_user_id = $user_id;
//                            $productModel->save(false);
//                            $last_pro_id = $productModel->id;
//
//                            $custom_productChannelModel = new ProductChannel;
//                            $store_id = Stores::find()->select('store_id')->where(['store_name' => 'VTEX'])->one();
//                            $custom_productChannelModel->store_id = $store_id->store_id;
//                            $custom_productChannelModel->product_id = $last_pro_id;
//                            $custom_productChannelModel->elliot_user_id = $user_id;
//                            $custom_productChannelModel->status = 'no';
//                            $custom_productChannelModel->created_at = date('Y-m-d h:i:s', time());
//                            $custom_productChannelModel->save(false);
//
//                            /* Save Product Abbrivation Id */
//                            $product_abberivation = new ProductAbbrivation();
//                            $product_abberivation->channel_abb_id = 'VT'.$product_id;
//                            $product_abberivation->product_id = $last_pro_id;
//                            $product_abberivation->channel_accquired = 'VTEX';
//                            $product_abberivation->created_at = date('Y-m-d H:i:s', time());
//                            $product_abberivation->save(false);
//
//                        }
//                        else
//                        {
//                            $last_pro_id = $checkProductId->id;
//                        }
//
//                        $order_products_model = new OrdersProducts;
//                        $order_products_model->order_Id = $last_order_id;
//                        $order_products_model->product_Id = $last_pro_id;
//                        $order_products_model->qty = $quantity_ordered;
//                        $order_products_model->order_product_sku = $product_sku;
//                        $order_products_model->created_at = date('Y-m-d h:i:s', time());
//                        $order_products_model->elliot_user_id = $user_id;
//                        $order_products_model->save(false);
//                    }
//                }
            }
        }
    }
    
    /**
     * Vtex Order Importing
     * @param str $vtex_account
     * @param str $vtex_app_key
     * @param str $vtex_app_token
     * @param int $user_id
     */
    public function vtexCustomerImporting($vtex_account, $vtex_app_key, $vtex_app_token, $user_id, $store_connection_id) {
        $customer_data_url = 'http://api.vtex.com/'.$vtex_account.'/dataentities/CL/scroll';
        $customer_data = $this->vtexCurl($customer_data_url, $vtex_app_key, $vtex_app_token);
        if(count($customer_data)==4 && array_key_exists('ExceptionMessage', $customer_data)){
            return false;
        }
        else{
			if(count($customer_data)==1 && array_key_exists('Message', $customer_data)){
				return;
			}
            foreach($customer_data as $customer_list){
                //$email = $customer_list['email'];
                $id = $customer_list['id'];
                $url = 'http://api.vtex.com/'.$vtex_account.'/dataentities/CL/documents/'.$id.'?_fields=_all';
                $_customer = $this->vtexCurl($url, $vtex_app_key, $vtex_app_token);
                $phone = $_customer['homePhone'];
                $frist_name = $_customer['firstName'];
                $last_name = $_customer['lastName'];
                $email = $_customer['email'];
                $vtext_customer_profile_id = $_customer['id'];
                $vtext_customer_id = $_customer['userId'];
                $customer_created_at = date("Y-m-d h:i:s", strtotime($_customer['createdIn']));
                $customer_updated_at = date("Y-m-d h:i:s", strtotime($_customer['updatedIn']));
                if($vtext_customer_id==''){
                    $vtext_customer_id = '0000';
                }
                
                $customer_data = array(
                    'customer_id'=>$vtext_customer_id,
                    'elliot_user_id'=>$user_id,
                    'first_name'=>$frist_name,
                    'last_name'=>$last_name,
                    'email'=>$email,
                    'channel_store_name'=>'VTEX',
                    'channel_store_prefix'=>'VT',
                    'mul_store_id' => $store_connection_id,
                    'mul_channel_id' => '',
                    'customer_created_at'=>$customer_created_at,
                    'customer_updated_at'=>$customer_updated_at,
                    'billing_address'=>array(
                        'street_1'=>'',
                        'street_2'=>'',
                        'city'=>'',
                        'state'=>'',
                        'country'=>'',
                        'country_iso'=>'',
                        'zip'=>'',
                        'phone_number'=>$phone,
                        'address_type'=>'Default',
                    ),
                    'shipping_address' => array(
                        'street_1'=>'',
                        'street_2'=>'',
                        'city'=>'',
                        'state'=>'',
                        'country'=>'',
                        'zip'=>'',
                    ),
                );
                Stores::customerImportingCommon($customer_data);
                
                /*$checkCustomerModel = CustomerUser::find()->where(['channel_abb_id' => 'VT' . $vtext_customer_id, 'email'=>$email])->one();
                if(empty($checkCustomerModel)){
                    $Customers_model = new CustomerUser();
                    $Customers_model->channel_abb_id = 'VT'.$vtext_customer_id;
                    $Customers_model->first_name = $frist_name;
                    $Customers_model->last_name = $last_name;
                    $Customers_model->email = $email;
                    $Customers_model->channel_acquired = 'VTEX';
                    $Customers_model->date_acquired = $customer_created_at;
                    $Customers_model->created_at = $customer_created_at;
                    $Customers_model->updated_at = $customer_updated_at;
                    $Customers_model->elliot_user_id = $user_id;
                    $Customers_model->save(false);
                }*/
            }
        }
    }
    /**
     * vtex Price format 
     * @param int $price
     * @return Formated price (25.21)
     */
    public function vtexPriceFormat($price) {
       $price_format = $price/100;
       $final_price = number_format($price_format, 2);
       return $final_price;
    }
    /**
     * Get Vtex warehoses
     * @return array warehouses
     */
    public function getVtexWarehouses($vtex_account, $vtex_app_key, $vtex_app_token){
        $url = 'http://logistics.'.$vtex_account.'.com.br/api/logistics/pvt/configuration/warehouses?an='.$vtex_account;
        $warehouses_result = self::vtexCurl($url, $vtex_app_key, $vtex_app_token);
        $warehouses_ids = array();
        foreach($warehouses_result as $_warehouses){
            $warehouses_ids[] = $_warehouses['id'];
        }
        return $warehouses_ids;
    }
    /**
     * Change Product Price To oVtex
     */
    public function vtexChangeProductPrice($vtex_account, $vtex_app_key, $vtex_app_token, $sku, $price){
        $url = 'http://rnb.'.$vtex_account.'.com.br/api/pricing/pvt/price-sheet/?an='.$vtex_account;
        $method = "POST";
        
        $price_data = array(
            '0'=> array(
                'itemId'=>$sku,
                'salesChannel'=>'1',
                'sellerId'=>null,
                'price'=>$price,
                'listPrice'=>'1',
                'validFrom'=>'',
                'validTo'=>'',
            ),
        );
        
        $data = json_encode($price_data);
        $result = self::vtexPostCurl($url, $method, $data, $vtex_app_key, $vtex_app_token);
        //echo "<pre>"; print_r($result); echo "</pre>";
    }
    
    public function vtexChangeProductQty($vtex_account, $vtex_app_key, $vtex_app_token, $sku, $qty){
        $warehouses = self::getVtexWarehouses($vtex_account, $vtex_app_key, $vtex_app_token);
        //echo "<pre>"; print_r($warehouses);
        foreach($warehouses as $_warehouses){
            $url = 'http://logistics.'.$vtex_account.'.com.br/api/logistics/pvt/inventory/skus/'.$sku.'/warehouses/'.$_warehouses.'?an='.$vtex_account;
            $method = 'PUT';
            $data = array(
                'unlimitedQuantity'=>FALSE,
                'dateUtcOnBalanceSystem'=>null,
                'quantity'=>$qty,
            );
            $post_data = json_encode($data);
            $result = self::vtexPostCurl($url, $method, $post_data, $vtex_app_key, $vtex_app_token);
            //echo "<pre>"; print_r($result);
            //echo "<br>";
        }
    }

    public function actionTest() {
        //$this->actionChangeProductPrice();
        //$this->actionChangeVtexProductQty();
        $pId = 87;
        $store_name = 'VTEX';
        $store_connection_id = 4;
        $product_data = ProductAbbrivation::find()->where(['product_id' => $pId, 'channel_accquired' => $store_name, 'mul_store_id' => $store_connection_id])->with('product')->one();
        echo "<pre>"; print_r($product_data); die;
    }
    
    public function vtexPostCurl($url, $method, $post_data, $app_key, $app_token)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => false,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => $method,
          CURLOPT_POSTFIELDS => $post_data,
          CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: 81d2076b-d156-edc3-034c-407fd3366941",
            "x-vtex-api-appkey: ".$app_key,
            "x-vtex-api-apptoken: ".$app_token,
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          $data =  "cURL Error #:" . $err;
        } else {
          $data = json_decode($response, true);
        }
        return $data;
    }
    public function actionVtexCronImportingAuth(){
        $post = Yii::$app->request->post();
        $id = $post['user_id'];
        $task_source = $post['task_source'];
        $array_msg = array();
        $CronTasks_data = CronTasks::find()->where(['elliot_user_id' => $id, 'task_source' => $task_source])->one();
        if (empty($CronTasks_data)) {
            $CronTasks = new CronTasks();
            $CronTasks->elliot_user_id = $id;
            $CronTasks->task_name = $post['task'];
            $CronTasks->task_source = $task_source;
            $CronTasks->status = 'Pending';
            $CronTasks->created_at = date('Y-m-d h:i:s', time());
            $CronTasks->updated_at = date('Y-m-d h:i:s', time());
            $CronTasks->save();
        } else {
            if ($CronTasks_data->status == "Completed") {
                $CronTasks_data->status = 'Pending';
                $CronTasks_data->save();
                return 1;
            } else {
                return 0;
            }
        }
    }
    
    public function actionVtexCronImporting(){
        $user_id = $_GET['user_id'];
        $channel = $_GET['channel'];
        
        $get_users = User::find()->select('domain_name,company_name,email')->where(['id' => $user_id])->one();
        $email = $get_users->email;
        $company_name = $get_users->company_name;
        $config = yii\helpers\ArrayHelper::merge(
            require($_SERVER['DOCUMENT_ROOT'].'/common/config/main.php'),
            require($_SERVER['DOCUMENT_ROOT'].'/common/users_config/' . $get_users->domain_name . '/main-local.php'), 
            require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main.php'), 
            require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);
        $cron_tasks_data = CronTasks::find()->where(['elliot_user_id' => $user_id, 'task_source' => 'Vtex', 'status' => 'Completed'])->one();
        if(!empty($cron_tasks_data)){
            $cron_tasks_data->status = 'Pending';
            $cron_tasks_data->updated_at = date('Y-m-d h:i:s', time());
            $cron_tasks_data->save();
        }
        
        $get_vtex_id = Stores::find()->select('store_id')->where(['store_name' => 'VTEX'])->one();
        $vtex_store_id = $get_vtex_id->store_id;
        $store_vtex = StoresConnection::find()->where(['store_id' => $vtex_store_id, 'user_id' => $user_id])->all();
        foreach($store_vtex as $_store_vtex){
            $vtex_account = $_store_vtex->vtex_account_name;
            $vtex_app_key = $_store_vtex->vtex_app_key;
            $vtex_app_token = $_store_vtex->vtex_app_token;
            $store_connection_id = $_store_vtex->stores_connection_id;
            $check_store_detail = StoreDetails::find()->where(['store_connection_id' => $store_connection_id])->one();
            $vtex_country_code = $check_store_detail->country_code;
            $country_detail = Countries::countryDetails($vtex_country_code);
            try{
                $category_url = 'http://'.$vtex_account.'.com.br/api/catalog_system/pub/category/tree/1000';
                $category_response = $this->vtexCurl($category_url, $vtex_app_key, $vtex_app_token);
                if(!is_scalar($category_response)){
                    $this->vtexCategoryImporting($category_response, $user_id, $store_connection_id);
                    $parent = '';
                    $this->vtexParentCategoryAssign($category_response, $parent, $user_id, $store_connection_id);
                    $this->vtexProductImporting($vtex_account, $vtex_app_key, $vtex_app_token, $user_id, $store_connection_id, $country_detail);
                    //$this->vtexCustomerImporting($vtex_account, $vtex_app_key, $vtex_app_token, $user_id, $store_connection_id);
                    //$this->vtexOrderImporing($vtex_account, $vtex_app_key, $vtex_app_token, $user_id, $store_connection_id);
                    $this->vtexOrderImporing($vtex_account, $vtex_app_key, $vtex_app_token, $user_id, $store_connection_id, $country_detail, $vtex_store_id);

                    $cron_tasks_data = CronTasks::find()->where(['elliot_user_id' => $user_id, 'task_source' => 'Vtex', 'status' => 'Pending'])->one();
                    if(!empty($cron_tasks_data)){
                        $cron_tasks_data->status = 'Completed';
                        $cron_tasks_data->updated_at = date('Y-m-d h:i:s', time());
                        $cron_tasks_data->save();
                    }
                }
            } catch (Exception $ex) {
                error_log ("Error while importing data through cron msg is => ".$ex->getMessage()." ---- ", 3, 'vtex_cron_error.log');
            }
        }
    }
	
	
}      