<?php

namespace backend\controllers;
use Yii;
use yii\web\Controller;
use yii\helpers\Html;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\Content;
//use backend\models\ChannelConnection;

class ContentController extends \yii\web\Controller {
    
    
     public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create','update'],
                'rules' => [
                        [
                        'actions' => [''],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                        [
                        'actions' => ['index', 'create', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex() {
         $user_id = Yii::$app->user->identity->id;    
        $pages_all = Content::find()->where(['elliot_user_id' => $user_id])->asArray()->all();
        return $this->render('index', [
            'view_all' => $pages_all
        ]);
    }
//    
    public function actionGetpages() {
        $user_id = Yii::$app->user->identity->id;
        $post = Yii::$app->request->get();
         $start = $post['start'];
        $length = $post['length'];
        $draw = $post['draw'];
        $search = $post['search']['value'] ? $post['search']['value'] : '';
        $orderby = $post['order']['0']['column'] ? $post['order']['0']['column'] : '';
        $sortbyvalue = true;
        $orderby_str = 'page_title';
        if($orderby == 0) {
            $orderby_str = 'page_title';
        } else {
            $orderby_str = 'page_ID';
        }
        
        $asc = $post['order']['0']['dir'] ? $post['order']['0']['dir'] : '';
        
        $count = Content::find()->where(['elliot_user_id' => $user_id])->count();
        $pages_all = Content::find()
                ->limit($length)->offset($start)
                ->Where(['elliot_user_id' => $user_id])
                ->andFilterWhere([
                    'or',
                        ['like', 'page_title', $search],
                ])
                ->orderBy($orderby_str . " " . $asc)
                ->asArray()
                ->all();
        $response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count);
        $final_response = array();
        if(!empty($pages_all)) {
            foreach($pages_all as $pages) {
                
                $page_title =  $pages['page_title'];
                $page_delete = Html::a('', ['/content/delete', 'id' => $pages['page_ID']], [
                                       'class' => 'mdi mdi-delete',
                                       'data' => [
                                           'confirm' => 'Are you sure you want to delete this page?',
                                           'method' => 'post',
                                       ],
                                   ]);
                $page_update = Html::a('', ['/content/update/', 'id' => $pages['page_ID']], [
                                       'class' => 'mdi mdi-edit',
                                   ]);
                
                
                $final_response[0] = $page_title;
                $final_response[1] = $page_delete . ' ' . $page_update;
                $data_arr[] = $final_response; 
            }

            $response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count, "data" => $data_arr);
            echo json_encode($response_arr);
        } else {
              $response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count, "data" => array());
            echo json_encode($response_arr);
        }
    }
    
    public function actionCreate() {
        $model = new Content();
        $user_id = Yii::$app->user->identity->id;
        
        if (Yii::$app->request->post()) {
            
         $page_title = $_POST['page_title'];
         $page_description = $_POST['page_desc'];
         $created_at = date('Y-m-d h:i:s', time());
         
         $model->elliot_user_id = $user_id;
         $model->page_title = $page_title;
         $model->page_description = $page_description;
         $model->created_at = $created_at;
         
         if($model->save(false)) {
             
             Yii::$app->session->setFlash('success', 'Success! Page has been created.');
                return $this->redirect(['/content/index']);
         }

            
    } else {
        return $this->render('create', [
            'model' => $model
        ]);
    }
       return $this->render('create', [
            'model' => $model
        ]); 
        
    }
    
    
    public function actionUpdate($id) {
 
        $model = Content::find()->where(['page_ID' => $id])->one();
        return $this->render('update', [
            'model' => $model
        ]);
        
    }
    
    public function actionUpdatepage() {
        
        if(Yii::$app->request->post()) {
            $page_id = $_POST['page_id'];
            $get_record = Content::find()->where(['page_ID' => $page_id])->one();
            $get_record->page_title = $_POST['page_title'];
            $get_record->page_description = $_POST['page_desc'];
            if($get_record->save()) {
                Yii::$app->session->setFlash('success', 'Success! Page has been updated.');
                 return $this->redirect(['/content/index']);
                
            } else {
                return $this->render('update', [
                        'model' => $model
            ]);
            }      
        }
        
    }
    
        public function actionDelete($id) {
            $model = Content::find()->where(['page_ID' => $id])->one()->delete();
            
//        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    
    public function actionTestCall() {
        $tzlist = \DateTimeZone::listIdentifiers();
       echo '<pre>';
        print_r($tzlist);
    }
    

}
