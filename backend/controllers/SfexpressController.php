<?php

namespace backend\controllers;

use backend\models\ChannelConnection;
use backend\models\Channels;
use backend\models\StoresConnection;
use Bigcommerce\Api\Connection;
use backend\models\ProductImages;
use backend\controllers\ChannelsController;
use backend\models\Categories;
use backend\models\Products;
use backend\models\Variations;
use backend\models\VariationsItemList;
use backend\models\OrdersProducts;
use backend\models\OrderChannel;
use backend\models\CustomerUser;
use backend\models\Notification;
use backend\models\ProductCategories;
use backend\models\ProductVariation;
use backend\models\ProductChannel;
use backend\models\Orders;
use backend\models\MerchantProducts;
use backend\models\User;
use backend\models\GoogleProductCategories;
use backend\models\Stores;
use yii\filters\AccessControl;
use Yii;
use backend\models\Fulfillment;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class SfexpressController extends \yii\web\Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','sforders'],
                'rules' => [
                        [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                        [
                        'actions' => ['logout','index','sforders'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
      /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex() {
        return $this->render('index');
    }
	public function actionPrice() {
        return $this->render('price');
    }

    public function actionSforders() {
        $order = Orders::find()->all();
		$sf_orders_data=OrderChannel::find()->Where(['store_id'=>'1'])->all();
		//echo '<pre>'; print_r($sf_orders_data); echo '</pre>'; die('sdfa');
		$orderJsonArrayData = array();
		//$KEYJsonArrayData = array();
		   $url = 'https://sit.api.sf-express-us.com/api/orderservice/submitorder';
		foreach($sf_orders_data as $sf):
		  $order_id=$sf->order_id;
		  $sf_orders_data_all=Orders::find()->Where(['order_ID'=>$order_id])->with('customer')->with('ordersProducts')->one();
			//echo '<pre>'; print_r($sf_orders_data_all); echo '</pre>'; die('sdfa');
			$product_data = $sf_orders_data_all->ordersProducts;
			$customer_data = $sf_orders_data_all->customer;
			$orderIdForSF = $sf_orders_data_all->order_ID.'-'.$sf_orders_data_all->customer_id;
			$orderJsonArrayData['CneeContactName'] = $customer_data->first_name;
			$orderJsonArrayData['CneeCompany'] = 'Sf Express';
			$orderJsonArrayData['CneeAddress'] = $customer_data->street_1;
			$orderJsonArrayData['CneeCity'] = $customer_data->city;
			$orderJsonArrayData['CneeProvince'] = $customer_data->state;
			$orderJsonArrayData['CneeCountry'] = $customer_data->country;
			$orderJsonArrayData['CneePostCode'] = $customer_data->ship_zip;
			$orderJsonArrayData['CneePhone'] = $customer_data->phone_number;
			$orderJsonArrayData['ReferenceNo1'] = $orderIdForSF;
			$orderJsonArrayData['ReferenceNo2'] = 'IZIEF4342324324D23434';
			$orderJsonArrayData['ExpressType'] = '101';
			$orderJsonArrayData['ParcelQuantity'] = '1';
			$orderJsonArrayData['PayMethod'] = '3';
			$orderJsonArrayData['TaxPayType'] = '2';
			$orderJsonArrayData['Currency'] = 'USD';
			
			$i=0;
				foreach($product_data as $prodata):
				$id = $prodata->product_Id;
				$productData = Products::find()->Where(['id'=>$id])->one();
				if(!empty($productData->product_name)){
				$orderJsonArrayData['Items'][$i]['Name'] = $productData->product_name;
				} else{
					$orderJsonArrayData['Items'][$i]['Name'] = 'Testing';
				}
				if(!empty($productData->stock_quantity)){
				$orderJsonArrayData['Items'][$i]['Count'] = str_replace(',','',number_format($productData->stock_quantity,0));
				} else{
					$orderJsonArrayData['Items'][$i]['Count'] = '1';
				}
				$orderJsonArrayData['Items'][$i]['Unit'] = 'pcs';
				if(!empty($productData->price)){
				$orderJsonArrayData['Items'][$i]['Amount'] = $productData->price;
				} else {
					$orderJsonArrayData['Items'][$i]['Amount'] = '0';
				}
				$orderJsonArrayData['Items'][$i]['SourceArea'] = 'US';
				if(!empty($productData->price)){
				$orderJsonArrayData['Items'][$i]['Weight'] = $productData->weight;
				} else{
					$orderJsonArrayData['Items'][$i]['Weight'] = '1.0';
				}
				
				$i++;
				endforeach; 
			//$KEYJsonArrayData['Gateway'] = 'JFK';
			//$KEYJsonArrayData = array ( 'UserName' => 'info@helloiamelliot.com','Password' => 'sfexpress0035', );
			$KEYJsonArrayData = array ( 'UserName' => '90000006','Password' => '!p96PytJkBxJQUg', );
			//echo '<pre>'; print_r($orderJsonArrayData); echo '</pre>'; die('sdfa');
			//endforeach;
			 $order_data_json = array (
          'Order' =>$orderJsonArrayData,
          'Gateway' => 'JFK',
          'NetworkCredential' =>$KEYJsonArrayData,
          );

         $data_string = json_encode($order_data_json); 
		 $data_string = '{

"Order": {

  "CneeContactName": "Philipe",

  "CneeCompany": "Sf Express",

  "CneeAddress": "4000 HOLLYWOOD BLVD",

  "CneeCity": "HOLLYWOOD",

  "CneeProvince": "BEIJING",

  "CneeCountry": "CN",

  "CneePostCode": "100052",

  "CneePhone": "+19549970137",

  "ReferenceNo1": "12-923",

  "ReferenceNo2": "IZIEF4342324324D23434",

  "ExpressType": "101",

  "ParcelQuantity": "1",

  "PayMethod": "3",

  "TaxPayType": "2",

  "Currency": "USD",

  "Items": [{

   "Name": "White T-shirt XG",

   "Count": "99999",

   "Unit": "pcs",

   "Amount": 99,

   "SourceArea": "US",

   "Weight": "0.0002"

  }]

},

"GateWay": "LAX",

"NetworkCredential": {

  "UserName": "90000006",

  "Password": "=!p96PytJkBxJQUg"

}

}';
		// echo  $data_string;
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_SSL_VERIFYPEER => false,
		  CURLOPT_URL => "https://sit.api.sf-express-us.com/api/orderservice/submitorder",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_SSL_VERIFYHOST => FALSE,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $data_string,
		  CURLOPT_HTTPHEADER => array(
			"cache-control: no-cache",
			"charset: utf-8",
			"content-type: application/json"
		  ),
		));

          $result = curl_exec($curl);
          $err = curl_error($curl);
          //echo '<pre>'; print_r($ch); echo '</pre>';
          //echo '<pre>'; print_r($orderJsonArrayData); echo '</pre>';
          echo '<pre>'; print_r(json_decode($result)); echo '</pre>';// die('dsfa');
		  $orderData = json_decode($result);
		  //$orderData['Success'];
		    // echo $orderData->Success;
		    // print_r($orderData->Data);
		
		  $sf_orders_data_all->sf_data = $orderData;
		  $sf_orders_data_all->save(false);
		  
		//  echo '##########################################################################';
         // die('sdfa'); 
		endforeach;
         

       
         
       // return $this->render('sforders');
    }
	
	public function actionRoute(){
		$data_string = '{
					"Routes":[{
					"SfWaybillNo":"070035721724",
					"TrackingType":"1"
					}],
						"NetworkCredential":
							{
							"UserName":"90000006",
							"Password":"=!p96PytJkBxJQUg"
							}
				}';
				$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_SSL_VERIFYPEER => false,
		  CURLOPT_URL => "https://sit.api.sf-express-us.com/api/routeservice/query",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_SSL_VERIFYHOST => FALSE,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => $data_string,
		  CURLOPT_HTTPHEADER => array(
			"cache-control: no-cache",
			"charset: utf-8",
			"content-type: application/json"
		  ),
		));

          $result = curl_exec($curl);
          $err = curl_error($curl);
          //echo '<pre>'; print_r($ch); echo '</pre>';
          //echo '<pre>'; print_r($orderJsonArrayData); echo '</pre>';
          echo '<pre>'; print_r(json_decode($result)); echo '</pre>';// die('dsfa');
		  $orderData = json_decode($result);
	}
	
    public function actionSave() {
        //echo '<pre>'; print_r($_POST); echo '</pre>'; die('sdf');

        $user_id = Yii::$app->user->identity->id;
        $username = $_POST['username'];
        $password = $_POST['password'];
        $modelFulfillment = new Fulfillment();
        $modelFulfillment->elliot_user_id = $user_id;
        $modelFulfillment->name = 'SF Express';
        $modelFulfillment->fulfillment_link = '/sfexpress/sforders';
        $modelFulfillment->fulfillment_id = '5';
        $modelFulfillment->key_data = 'username';
        $modelFulfillment->value_data = $username;
        $modelFulfillment->created_at = date('Y-m-d H:i:s');
        $modelFulfillment->save();

        $modelFulfillment = new Fulfillment();
        $modelFulfillment->elliot_user_id = $user_id;
        $modelFulfillment->name = 'SF Express';
        $modelFulfillment->fulfillment_link = '/sfexpress/sforders';
        $modelFulfillment->fulfillment_id = '5';
        $modelFulfillment->key_data = 'password';
        $modelFulfillment->value_data = $password;
        $modelFulfillment->created_at = date('Y-m-d H:i:s');
        if ($modelFulfillment->save(false)) {
            echo 'success';
        } else {
            echo 'error';
        }
    }
	public function actionDefault() {
        //echo '<pre>'; print_r($_POST); echo '</pre>'; die('sdf');
		$feedcheck = $_POST['feed'];
			if($feedcheck == 'yes'){
			$user_id = Yii::$app->user->identity->id;
			$username = '90000006';
			$password = '=!p96PytJkBxJQUg';
			$modelFulfillment = new Fulfillment();
			$modelFulfillment->elliot_user_id = $user_id;
			$modelFulfillment->name = 'SF Express';
			$modelFulfillment->fulfillment_link = '/sfexpress/sforders';
			$modelFulfillment->fulfillment_id = '5';
			$modelFulfillment->key_data = 'username';
			$modelFulfillment->value_data = $username;
			$modelFulfillment->created_at = date('Y-m-d H:i:s');
			$modelFulfillment->save();

			$modelFulfillment = new Fulfillment();
			$modelFulfillment->elliot_user_id = $user_id;
			$modelFulfillment->name = 'SF Express';
			$modelFulfillment->fulfillment_link = '/sfexpress/sforders';
			$modelFulfillment->fulfillment_id = '5';
			$modelFulfillment->key_data = 'password';
			$modelFulfillment->value_data = $password;
			$modelFulfillment->created_at = date('Y-m-d H:i:s');
			if ($modelFulfillment->save(false)) {
				//echo 'success';
				header('location: /sfexpress');
				exit;
				
			} else {
				echo 'error';
			}
		} else{
			$user_id = Yii::$app->user->identity->id;
			$Fulfillment_check = Fulfillment::find()->where(['name' => 'SF Express','elliot_user_id' => $user_id])->all();
			//echo '<per>'; print_r($Fulfillment_check); die();
			if (!empty($Fulfillment_check)) {
				foreach($Fulfillment_check as $data){
					$id = $data->id;
					$Fulfillment_check_del = Fulfillment::find()->where(['name' => 'SF Express','id' => $id])->one();
					$Fulfillment_check_del->delete();
				}
			}
			
		}
    }
	public function actionDeletesf() {
		$key = $_POST['keyid'];
		$user_id = Yii::$app->user->identity->id;
		$Fulfillment_check = Fulfillment::find()->where(['name' => 'SF Express','elliot_user_id' => $user_id])->all();
		//echo '<per>'; print_r($Fulfillment_check); die();
		if (!empty($Fulfillment_check)) {
			foreach($Fulfillment_check as $data){
				$id = $data->id;
				$Fulfillment_check_del = Fulfillment::find()->where(['name' => 'SF Express','id' => $id])->one();
				$Fulfillment_check_del->delete();
			}
		}
		/* if (!empty($Fulfillment_check)) {
                        $Fulfillment_check->delete();
                    } */
		
		
	}

}
