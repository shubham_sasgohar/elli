<?php

namespace backend\controllers;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use backend\models\User;
use backend\models\StoresConnection;
use backend\models\Stores;
use backend\models\Categories;
use backend\models\Variations;
use backend\models\VariationsItemList;
use backend\models\Products;
use backend\models\OrdersProducts;
use backend\models\OrderChannel;
use backend\models\CustomerUser;
use backend\models\Notification;
use backend\models\ProductCategories;
use backend\models\ProductVariation;
use backend\models\ProductImages;
use backend\models\ProductChannel;
use backend\models\Orders;
use backend\models\Channels;
use common\models\CustomFunction;
use backend\models\ChannelConnection;
use backend\models\Integrations;
use backend\models\ProductAbbrivation;
use backend\models\CategoryAbbrivation;
use backend\models\CronTasks;
use backend\models\StoreDetails;

class JetController extends \yii\web\Controller {
    /**
     * @inheritdoc
     */
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'test', 'jet-auth', 'jet-importing'],
                'rules' => [
                    [
                        /* action hit without log-in */
                        'actions' => ['login', 'test', 'jet-importing'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        /* action hit only with log-in */
                        'actions' => ['index', 'test', 'jet-auth', 'jet-importing'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function actionIndex() {
        return $this->render('index');
    }
    
    public function actionJetAuth(){
        //echo "<pre>"; print_r($_POST); echo "</pre>";
        $jet_api_user = $_POST['jet_api_user'];
        $jet_secret_key = $_POST['jet_secret_key'];
        $jet_merchant_id = $_POST['jet_merchant_id'];
        $user_id = $_POST['user_id'];
        $array_msg = array();
        $access_token = $this->getJetAccessToken($jet_api_user, $jet_secret_key);
        if($access_token==0){
            $array_msg['api_error'] = 'Your API credentials is not working. Please check your API data and try again';
            return json_encode($array_msg);
        }
        elseif (array_key_exists('errors', $access_token)) {
            $array_msg['api_error'] = 'Your API credentials is not working. Please check your API data and try again.';
            return json_encode($array_msg);
        }
        else {
            $user = User::find()->where(['id' => $user_id])->one();
            $user_domain = $user->domain_name;
            $config = yii\helpers\ArrayHelper::merge(
                require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'),
                require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $user_domain . '/main-local.php'),
                require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'),
                require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
            );
            $application = new yii\web\Application($config);
            $channel_data = Channels::find()->where(['channel_name' => 'Jet'])->one();
            $jet_channel_id = $channel_data->channel_ID;
            $jet_channel_data = ChannelConnection::find()->where(['elliot_user_id' => $user_id, 'channel_id' => $jet_channel_id])->one();
            if(empty($jet_channel_data)){
                $channel_model = new ChannelConnection();
                $channel_model->elliot_user_id = $user_id;
                $channel_model->channel_id = $jet_channel_id;
                $channel_model->user_id = $user_id;
                $channel_model->connected = 'yes';
                $channel_model->jet_api_user = $jet_api_user;
                $channel_model->jet_secret_key = $jet_secret_key;
                $channel_model->jet_merchant_id = $jet_merchant_id;
                $channel_model->created = date("Y-m-d h:i:s", time());
                $channel_model->updated = date("Y-m-d h:i:s", time());
                if($channel_model->save(false)){
                    $array_msg['success'] = "Your Jet channle has been connected successfully. Importing is started. Once importing is done you will get a nofiy message.";
                }
                else{
                    $array_msg['error'] = "Error Something went wrong. Please try again";
                }
                return json_encode($array_msg);
            }
        }
    }
     /**
      * Jet Importing start
      */
    public function actionJetImporting(){
        $user_id = $_GET['user_id'];
        $get_users = User::find()->select('domain_name,company_name,email')->where(['id' => $user_id])->one();
        $email = $get_users->email;
        $company_name = $get_users->company_name;
        $config = yii\helpers\ArrayHelper::merge(
            require($_SERVER['DOCUMENT_ROOT'].'/common/config/main.php'),
            require($_SERVER['DOCUMENT_ROOT'].'/common/users_config/'.$get_users->domain_name.'/main-local.php'), 
            require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main.php'), 
            require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);
        $channel_data = Channels::find()->where(['channel_name' => 'Jet'])->one();
        $jet_channel_id = $channel_data->channel_ID;
        $jet_details = ChannelConnection::find()->where(['user_id' => $user_id, 'channel_id' => $jet_channel_id])->one();
        if(!empty($jet_details)){
            $channel_connection_id = $jet_details->channel_connection_id;
            $jet_api_user = $jet_details->jet_api_user;
            $jet_secret_key = $jet_details->jet_secret_key;
            $access_token_data = $this->getJetAccessToken($jet_api_user, $jet_secret_key);
            $access_token = $access_token_data['id_token'];
            
            $this->jetChannelImporting($channel_connection_id);
            
            $skus_list = $this->jetGetCurl($access_token, 'merchant-skus');
            $this->jetProductImporting($access_token, $skus_list, $user_id, $jet_channel_id, $channel_connection_id);
            
            $order_list = $this->jetGetCurl($access_token, 'orders/complete');
            $this->jetOrderImporting($access_token, $order_list, $user_id, $jet_channel_id, $channel_connection_id);
            
            $order_created_list = $this->jetGetCurl($access_token, 'orders/created');
            $this->jetOrderImporting($access_token, $order_created_list, $user_id, $jet_channel_id, $channel_connection_id);
            
            $order_ready_list = $this->jetGetCurl($access_token, 'orders/ready');
            $this->jetOrderImporting($access_token, $order_ready_list, $user_id, $jet_channel_id, $channel_connection_id);
            
            $order_acknowledged_list = $this->jetGetCurl($access_token, 'orders/acknowledged');
            $this->jetOrderImporting($access_token, $order_acknowledged_list, $user_id, $jet_channel_id, $channel_connection_id);
            
            $order_inprogress_list = $this->jetGetCurl($access_token, 'orders/inprogress');
            $this->jetOrderImporting($access_token, $order_inprogress_list, $user_id, $jet_channel_id, $channel_connection_id);
            
            /* SEnd Email Magento */
            $email_message = 'Success, Your JET Store is Now Connected';
            $email = 'noor.mohamad@brihaspatitech.com';
            $send_email_notif = CustomFunction::ConnectBigCommerceEmail($email, $company_name, $email_message);

            //Drop-Down Notification for User
            $notif_type = 'JET';
            $notif_db = Notification::find()->Where(['notif_type' => $notif_type])->one();
            if (empty($notif_db)):
                $notification_model = new Notification();
                $notification_model->user_id = $user_id;
                $notification_model->notif_type = $notif_type;
                $notification_model->notif_description = 'Your JET store data has been successfully imported.';
                $notification_model->created_at = date('Y-m-d H:i:s', time());
                $notification_model->save(false);
            endif;
            //Sticky Notification :StoresConnection Model Entry for Import Status
            $get_rec = ChannelConnection::find()->Where(['user_id' => $user_id, 'channel_id' => $jet_channel_id])->one();
            $get_rec->import_status = 'Completed';
            $get_rec->save(false);
            
            
        }
    }
    /** Jet Channel Imporiting **/
    public function jetChannelImporting($channel_connection_id) {
        $channel_details = StoreDetails::find()->Where(['channel_connection_id' => $channel_connection_id])->one();
        if(!empty($channel_details)){
            $save_store_details = new StoreDetails();
            $save_store_details->channel_connection_id = $channel_connection_id;
            $save_store_details->store_name = 'Jet';
            $save_store_details->store_url = '';
            $save_store_details->country = 'United State';
            $save_store_details->country_code = 'US';
            $save_store_details->currency = 'USD';
            $save_store_details->currency_symbol = '$';
            $save_store_details->others = '';
            $save_store_details->channel_accquired = 'Jet';
            $save_store_details->created_at = date('Y-m-d H:i:s', time());
            $save_store_details->save(false);
        }
    }
    /** Jet Product Importing **/
    public function jetProductImporting($access_token, $skus_list, $user_id, $jet_channel_id, $channel_connection_id) {
        if(isset($skus_list['sku_urls']) && count($skus_list['sku_urls'])>0){
            $skus_urls = $skus_list['sku_urls'];
            foreach($skus_urls as $sku_url){
                $product = $this->jetGetCurl($access_token, $sku_url);
                $product_id = isset($product['merchant_sku']) ? $product['merchant_sku'] : 0;
                $product_img = (isset($product['alternate_images']))?$product['alternate_images']:array();
                $main_image_url = isset($product['main_image_url']) ? $product['main_image_url'] : '';
                $brand = isset($product['brand']) ? $product['brand'] : '';
                $updated_at = isset($product['price_last_update']) ? strtotime($product['price_last_update']) : time();
                $price = isset($product['price']) ? $product['price'] : 0;
                $target_price = isset($product['target_price']) ? $product['target_price'] : '';
                $description = isset($product['product_description']) ? $product['product_description'] : '';
                $jet_sku = isset($product['jet_sku']) ? $product['jet_sku'] : '';
                $title = isset($product['product_title']) ? $product['product_title'] : '';
                $sku = isset($product['merchant_sku_id']) ? $product['merchant_sku_id'] : '';
                $status = isset($product['status']) ? $product['status'] : '';
                $product_qty = isset($product['inventory_by_fulfillment_node']) ? $product['inventory_by_fulfillment_node'][0]['quantity'] : 0;
                
                $jet_node_ids = array();
                $cat1 = isset($product['jet_browse_node_id']) ? $product['jet_browse_node_id'] : '';
                $cat2 = isset($product['jet_browse_node_id_mapped_level_0']) ? $product['jet_browse_node_id_mapped_level_0'] : '';
                $cat3 = isset($product['jet_browse_node_id_mapped_level_1']) ? $product['jet_browse_node_id_mapped_level_1'] : '';
                $cat4 = isset($product['jet_browse_node_id_mapped_level_2']) ? $product['jet_browse_node_id_mapped_level_2'] : '';
                if($cat1!=''){
                    $jet_node_ids[] = $cat1;
                }
                if($cat2!=''){
                    $jet_node_ids[] = $cat2;
                }
                if($cat3!=''){
                    $jet_node_ids[] = $cat3;
                }
                if($cat4!=''){
                    $jet_node_ids[] = $cat4;
                }
                foreach($jet_node_ids as $node_id){
                    $cate_url = "taxonomy/nodes/".$node_id;
                    $cat_result = $this->jetGetCurl($access_token, $cate_url);
                    if($cat_result=='' || isset($cat_result['Message']) && $cat_result['Message']=='The request is invalid.'){
                        continue;
                    }else{
                        $cat_name = $cat_result['jet_node_name'];
                        $cat_id = $cat_result['jet_node_id'];
                        $category_data = array(
                            'category_id'=>$cat_id,     // Give category id of Store/channels
                            'parent_id'=>0,     // Give Category parent id of Elliot if null then give 0
                            'name'=>$cat_name,   // Give category name
                            'channel_store_name'=>'Jet',   // Give Channel/Store name
                            'channel_store_prefix'=>'JET',  // Give Channel/Store prefix id
                            'mul_store_id'=> '',  // Give Channel/Store prefix id
                            'mul_channel_id'=>$channel_connection_id,  // Give Channel/Store prefix id
                            'elliot_user_id'=>$user_id,     // Give Elliot user id
                            'created_at'=> date('Y-m-d H:i:s'),  // Give Created at date if null then give current date format date('Y-m-d H:i:s')
                            'updated_at'=> date('Y-m-d H:i:s'),  // Give Updated at date if null then give current date format date('Y-m-d H:i:s')
                        );
                        Stores::categoryImportingCommon($category_data);    // Then call store modal function and give $category data
                    }
                }
                
                
                $upc = $ean = $isbn = $mpn = $jan = '';
                $product_url = 'https://jet.com/search?term='.$jet_sku;
                if(isset($product['standard_product_codes'])){
                    $product_codes = $product['standard_product_codes'];
                    foreach($product_codes as $_product_code){
                        if($_product_code['standard_product_code_type']=='UPC'){
                            $upc = $_product_code['standard_product_code'];
                        }
                        if($_product_code['standard_product_code_type']=='EAN'){
                            $ean = $_product_code['standard_product_code'];
                        }
                        if($_product_code['standard_product_code_type']=='ISBN'){
                            $isbn = $_product_code['standard_product_code'];
                        }
                        if($_product_code['standard_product_code_type']=='MPN'){
                            $mpn = $_product_code['standard_product_code'];
                        }
                        if($_product_code['standard_product_code_type']=='JAN'){
                            $jan = $_product_code['standard_product_code'];
                        }
                    }
                }

                $product_status = ($status=='Available for Purchase' || $status='Under Jet Review') ? 1 : 0;
                $stock_status = ($product_qty>0) ? 1 : 0;

                $product_image_data = array();
                if(count($product_img)>0){
                    foreach($product_img as $_image){
                        $img_url = $_image['image_url'];
                        $position = $_image['image_slot_id'];
                        if($main_image_url == $img_url){
                            $base_img = $img_url;
                        }
                        $product_image_data[] = array(
                            'image_url'=>$img_url,
                            'label'=>'',
                            'position'=>$position,
                            'base_img'=>$base_img,
                        );
                    }
                }

                $product_data = array(
                    'product_id'=> $product_id, // Stores/Channel product ID
                    'name'=> $title,  // Product name
                    'sku'=> $sku,    // Product SKU
                    'description'=> $description,    // Product Description
                    'product_url_path'=> $product_url,   // Product url if null give blank value
                    'weight'=> '',  // Product weight if null give blank value
                    'status'=> $product_status,  // Product status (visible or hidden). Give in 0 or 1 form (0 = hidden, 1 = visible).
                    'price'=> $price,    // Porduct price
                    'sale_price'=> $price,   // Product sale price if null give Product price value
                    'qty'=> $product_qty,    //Product quantity 
                    'stock_status'=> $stock_status,  // Product stock status ("in stock" or "out of stock"). 
                                                    // Give in 0 or 1 form (0 = out of stock, 1 = in stock)
                    'websites'=> array(),    //This is for only magento give only and blank array
                    'brand'=> $brand,    // Product brand if any
                    'low_stock_notification'=> 5,    // Porduct low stock notification if any otherwise give default 5 value
                    'created_at'=> $updated_at,  // Product created at date format date('Y-m-d H:i:s')
                    'updated_at'=> $updated_at,  // Product updated at date format date('Y-m-d H:i:s')
                    'channel_store_name'=> 'Jet',   // Channel or Store name
                    'channel_store_prefix'=> 'JET',      // Channel or store prefix id
                    'country_code' => 'USD',
                    'mul_store_id' => '', //Give multiple store id
                    'mul_channel_id' => $channel_connection_id, // Give multiple channel id
                    'elliot_user_id'=> $user_id,         // Elliot user id
                    'store_id'=> '',    // if your are importing store give store id
                    'channel_id'=> $jet_channel_id,   // if your are importing channel give channel id
                    'barcode'=> $upc,  // Product barcode if any
                    'ean'=> $ean,  // Product ean if any
                    'jan'=> $jan,  // Product jan if any
                    'isban'=> $isbn,    // Product isban if any
                    'mpn'=> $mpn,  // Product mpn if any
                    'categories'=> $jet_node_ids, // Product categroy array. If null give a blank array 
                    'images'=>$product_image_data,  // Product images data
                );
                Stores::productImportingCommon($product_data);
            }
        }
    }
    
    /** Jet order importing **/
    public function jetOrderImporting($access_token, $order_list, $user_id, $jet_channel_id, $channel_connection_id) {
        if(isset($order_list['order_urls']) && count($order_list['order_urls'])>0){
            $orders_urls = $order_list['order_urls'];
            foreach ($orders_urls as $order_url){
                $order = $this->jetGetCurl($access_token, $order_url);
                
                $order_id = $order['merchant_order_id'];
                $customer_email = $order['hash_email'];
                $customer_id = $order['customer_reference_order_id'];
                $order_created_at = $order['order_placed_date'];
                $order_updated_at = isset($order['order_ready_date']) ? $order['order_ready_date'] : date('Y-m-d H:i:s');
                $order_status = $order['status'];
                $total_product_qty  = count($order['order_items']);
                
                $base_price = isset($order['order_totals']['item_price']['base_price']) ? $order['order_totals']['item_price']['base_price'] : 0;
                $item_tax = isset($order['order_totals']['item_price']['item_tax']) ? $order['order_totals']['item_price']['item_tax'] : 0;
                $item_shipping_cost = isset($order['order_totals']['item_price']['item_shipping_cost']) ? $order['order_totals']['item_price']['item_shipping_cost'] : 0;
                $item_shipping_tax = isset($order['order_totals']['item_price']['item_shipping_tax']) ? $order['order_totals']['item_price']['item_shipping_tax'] : 0;
                $item_fulfillment_cost = isset($order['order_totals']['item_price']['item_fulfillment_cost']) ? $order['order_totals']['item_price']['item_fulfillment_cost'] : 0;
                
                $order_total = $base_price+$item_tax+$item_shipping_cost+$item_shipping_tax+$item_fulfillment_cost;
                $ship_name = isset($order['shipping_to']['recipient']['name']) ? $order['shipping_to']['recipient']['name'] : '';
                $ship_phone = isset($order['shipping_to']['recipient']['phone_number']) ? $order['shipping_to']['recipient']['phone_number'] : '';
                $ship_address_1 = isset($order['shipping_to']['address']['address1']) ? $order['shipping_to']['address']['address1'] : '';
                $ship_address_2 = isset($order['shipping_to']['address']['address2']) ? $order['shipping_to']['address']['address2'] : '';
                $ship_city = isset($order['shipping_to']['address']['city']) ? $order['shipping_to']['address']['city'] : '';
                $ship_state = isset($order['shipping_to']['address']['state']) ? $order['shipping_to']['address']['state'] : '';
                $ship_zip = isset($order['shipping_to']['address']['zip_code']) ? $order['shipping_to']['address']['zip_code'] : '';
                $country_name = '';
                $country_id = '';
                if ($ship_zip != '') {
                    $country_data = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.$ship_zip.'&sensor=true');
                    $country_result = json_decode($country_data, true);
                    $add = $country_result['results'][0]['address_components'];
                    foreach ($add as $_add) {
                        if ($_add['types'][0] == 'country') {
                            $country_name = $_add['long_name'];
                            $country_id = $_add['short_name'];
                        }
                    }
                }
                
                switch (strtolower($order_status)) {
                    case "created":
                        $order_status_2 = 'Pending';
                        break;
                    case "ready":
                        $order_status_2 = "In Transit";
                        break;
                    case "acknowledged":
                        $order_status_2 = "In Transit";
                        break;
                    case "inprogress":
                        $order_status_2 = "In Transit";
                        break;
                    case "complete":
                        $order_status_2 = "Completed";
                        break;
                    default:
                        $order_status_2 = "Pending";
                }
                
                $customer_data = array(
                    'customer_id'=>$customer_id,
                    'elliot_user_id'=>$user_id,
                    'first_name'=>$ship_name,
                    'last_name'=>'',
                    'email'=>$customer_email,
                    'channel_store_name'=>'Jet',
                    'channel_store_prefix'=>'JET',
                    'mul_store_id' => '',
                    'mul_channel_id' => $channel_connection_id,
                    'customer_created_at'=>$order_created_at,
                    'customer_updated_at'=>$order_updated_at,
                    'billing_address'=>array(
                        'street_1'=>$ship_address_1,
                        'street_2'=>$ship_address_2,
                        'city'=>$ship_city,
                        'state'=>$ship_state,
                        'country'=>$country_name,
                        'country_iso'=>$country_id,
                        'zip'=>$ship_zip,
                        'phone_number'=>$ship_phone,
                        'address_type'=>'Default',
                    ),
                    'shipping_address' => array(
                        'street_1'=>$ship_address_1,
                        'street_2'=>$ship_address_2,
                        'city'=>$ship_city,
                        'state'=>$ship_state,
                        'country'=>$country_name,
                        'zip'=>$ship_zip,
                    ),
                );
                
                Stores::customerImportingCommon($customer_data);
                
                $order_product_data = array();
                $order_items = $order['order_items'];
                foreach($order_items as $_items){
                    $product_id = $_items['merchant_sku'];
                    $product_sku = '';
                    $product_data = $this->jetGetCurl($access_token, 'merchant-skus/'.$product_id);
                    if($product_data!=''){
                        $product_sku = isset($product_data['merchant_sku_id']) ? $product_data['merchant_sku_id'] : '';
                    }
                    $title = $_items['product_title'];
                    $price = $_items['item_price']['base_price'];
                    $quantity = $_items['request_order_quantity'];
                    $product_weight = 0;
                    $order_product_data[] = array(
                        'product_id'=> $product_id,
                        'name'=> $title,
                        'sku'=> $product_sku,
                        'price'=> $price,
                        'qty_ordered'=> $quantity,
                        'weight'=> $product_weight,
                    );
                }

                $order_data = array(
                    'order_id'=> $order_id,
                    'status'=> $order_status_2,
                    'magento_store_id'=> '',
                    'order_grand_total'=>$order_total,
                    'customer_id'=> $customer_id,
                    'customer_email'=> $customer_email,
                    'order_shipping_amount'=> $item_shipping_cost,
                    'order_tax_amount'=> $item_shipping_tax,
                    'total_qty_ordered'=> $total_product_qty,
                    'created_at'=> $order_created_at,
                    'updated_at'=> $order_updated_at,
                    'payment_method'=> 'Debit card',
                    'refund_amount'=> '',
                    'discount_amount'=> '',
                    'channel_store_name'=> 'Jet',
                    'channel_store_prefix'=> 'JET',
                    'mul_store_id' => '',
                    'mul_channel_id' => $channel_connection_id,
                    'elliot_user_id'=> $user_id,
                    'store_id'=>'',
                    'channel_id'=>$jet_channel_id,
                    'shipping_address'=>array(
                        'street_1'=> $ship_address_1,
                        'street_2'=> $ship_address_2,
                        'state'=> $ship_state,
                        'city'=> $ship_city,
                        'country'=> $country_name,
                        'country_id'=> $country_id,
                        'postcode'=> $ship_zip,
                        'email'=> $customer_email,
                        'telephone'=> $ship_phone,
                        'firstname'=> $ship_name,
                        'lastname'=> '',
                    ),
                    'billing_address'=>array(
                        'street_1'=> $ship_address_1,
                        'street_2'=> $ship_address_2,
                        'state'=> $ship_state,
                        'city'=> $ship_city,
                        'country'=> $country_name,
                        'country_id'=> $country_id,
                        'postcode'=> $ship_zip,
                        'email'=> $customer_email,
                        'telephone'=> $ship_phone,
                        'firstname'=> $ship_name,
                        'lastname'=> '',
                    ),
                    'items'=>$order_product_data,
                );
                Stores::orderImportingCommon($order_data);
            }
        }
    }
    
    /** get jet access tokenv **/
    public function getJetAccessToken($api_user, $secret_key){
        $post_data_array = array(
            'user'=>$api_user,
            'pass'=>$secret_key
        );
        $post_data = json_encode($post_data_array);
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://merchant-api.jet.com/api/token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_SSL_VERIFYHOST=> false,
            CURLOPT_SSL_VERIFYPEER=> false,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $post_data,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
                "postman-token: 1db42c75-d1c4-aa18-6b63-6a804c20522e"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return 0;
        } else {
            return json_decode($response, TRUE);
        }
    }
    
    public function jetGetCurl($accessToken, $url){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://merchant-api.jet.com/api/".$url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => false,
          CURLOPT_SSL_VERIFYHOST=> false,
          CURLOPT_SSL_VERIFYPEER=> false,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "authorization: bearer $accessToken",
            "cache-control: no-cache",
            "content-type: application/json",
            "postman-token: 327a4332-4d67-659a-3d90-634a861cce0c"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          //echo "cURL Error #:" . $err;
            return array();
        } else {
          $response_data = json_decode($response, true);
          return $response_data;
        }
    }
    
    /** Test action for test **/
    public function actionTest(){
//        $prdouct_pricing = ProductAbbrivation::find()->Where(['product_id' => 9, 'channel_accquired' => 'Jet'])->one();
//        if(!empty($prdouct_pricing)){
//            echo "Yes<br>";
//            echo "<pre>"; print_r($prdouct_pricing); echo "</pre>";
//        }
//        else{
//            echo "Nullll";
//        }
//        $node_id = 10000061;
//        $jet_api_user = 'E9FCE9C46BB1F97B42705EF52F8B78D4943D1B00';
//        $jet_secret_key = 'LPRELivOZNt/woo6SW6gVJaphCxoa7xa8UxKtM6ImZZS';
//        $access_token_data = $this->getJetAccessToken($jet_api_user, $jet_secret_key);
//        $access_token = $access_token_data['id_token'];
//        $cate_url = "taxonomy/nodes/10000061";
//        $cat_result = $this->jetGetCurl($access_token, $cate_url);
//        echo "<pre>"; print_r($cat_result);
        
        $bill_phone = '+14027898627';
        $channel_name = 'Shopify';
        $store_connection_id = '2';
        $check_customer_phone = CustomerUser::find()->Where(['phone_number' => $bill_phone])
            ->with(['customerabbrivation' => function($query) use ($channel_name, $store_connection_id) {
                $query->andWhere(['channel_accquired' => $channel_name]);
                $query->andWhere(['mul_store_id' => $store_connection_id]);
            }])->asArray()->one();
        
        echo "<pre>"; print_r($check_customer_phone); echo "</pre>";
            
    }
}
