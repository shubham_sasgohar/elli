<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use backend\models\Products;
use backend\models\ProductsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Bigcommerce\Api\Connection;
use Bigcommerce\Api\Client as Bigcommerce;
use backend\models\GoogleProductCategories;
use backend\models\ProductCategories;
use backend\models\CategoryAbbrivation;
use backend\models\Categories;
use backend\models\ProductImages;
use yii\imagine\Image;
use backend\models\StoresConnection;
use backend\models\ChannelConnection;
use backend\models\Stores;
use backend\models\Channels;
use backend\models\ProductChannel;
use backend\models\Variations;
use backend\models\VariationsItemList;
use backend\models\VariationsSet;
use backend\models\ProductVariation;
use backend\models\Attributes;
use backend\models\AttributeType;
use backend\models\OrdersProducts;
use Bigcommerce\Api\ShopifyClient as Shopify;
use SoapClient;
use SoapFault;
use Automattic\WooCommerce\Client as Woocommerce;
use Automattic\WooCommerce\HttpClient\HttpClientException;
use backend\models\ProductAbbrivation;
use backend\models\StoreDetails;
use backend\models\CurrencyConversion;

/**
 * ProductsController implements the CRUD actions for Products model.
 */
class ProductsController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
	return [
	    'access' => [
		'class' => AccessControl::className(),
		'only' => ['index', 'view', 'create', 'index', 'update', 'google-product-category', 'update-product', 'product-image-upload', 'create-product-temp', 'create-product', 'save-product-variations', 'remove-product-variations', 'add-product-variations-row', 'get-opset-variations', 'get-attributes', 'ajax-product-delete', 'connected-products', 'test', 'inactive-products'],
		'rules' => [
			[
			'actions' => ['signup', 'login'],
			'allow' => true,
			'roles' => ['?'],
		    ],
			[
			'actions' => ['logout', 'index', 'view', 'create', 'index', 'update', 'google-product-category', 'update-product', 'product-image-upload', 'create-product-temp', 'create-product', 'save-product-variations', 'remove-product-variations', 'add-product-variations-row', 'get-opset-variations', 'get-attributes', 'ajax-product-delete', 'connected-products', 'test', 'inactive-products'],
			'allow' => true,
			'roles' => ['@'],
		    ],
		],
	    ],
	    'verbs' => [
		'class' => VerbFilter::className(),
		'actions' => [
		    ' delete' => ['POST'],
		],
	    ],
	];
    }

    /**
     * Disable CSRF Validation
     * @param type $action
     * @return type
     */
    public function beforeAction($action) {
	$this->enableCsrfValidation = false;
	return parent::beforeAction($action);
    }

    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex() {
	$products = Products::find()->Where(['elliot_user_id' => Yii::$app->user->identity->id])->all();
	$searchModel = new ProductsSearch();
	$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
	return $this->render('index', [
		    'searchModel' => $searchModel,
		    'dataProvider' => $dataProvider,
		    'products' => $products,
	]);
    }

    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionConnectedProducts() {
	return $this->render('connectedproducts');
    }

    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
	$attribute_types = AttributeType::find()->where(['elliot_user_id' => Yii::$app->user->identity->id])->all();
	return $this->render('view', [
		    'product_model' => $this->findModel($id),
		    'attribute_types' => $attribute_types,
	]);
    }

    public function actionGetchannel() {
	$users_Id = Yii::$app->user->identity->id;
	$store_connection = StoresConnection::find()->where(['user_id' => $users_Id])->with('storesDetails')->all();


	$channel_connection = ChannelConnection::find()->where(['user_id' => $users_Id])->all();
	$connections = array();
	foreach ($store_connection as $con) {
	    $store_connection_id = $con->stores_connection_id;
	    $store_detail_country = @$con->storesDetails->country;
	    if (!empty($store_detail_country)) {
		$store_country = $con->storesDetails->country;
	    } else {
		$store_country = '';
	    }
	    $store = Stores::find()->select(['store_name', 'store_image'])->where(['store_id' => $con->store_id])->one();
	    $arr[] = array("value" => $store_connection_id, "text" => trim($store->store_name . ' ' . $store_country));
	}
	foreach ($channel_connection as $con1) {
	    $channel = Channels::find()->select(['channel_name', 'channel_image', 'parent_name'])->where(['channel_id' => $con1->channel_id])->one();
	    $p_name = explode("_", $channel->parent_name);
	    $name = ucwords(str_replace("-", " ", $p_name[1]));
	    if ($name == 'Lazada') {
		$name = $channel->channel_name;
	    }
	    $arr[] = array("value" => $name, "text" => $name);
	}
	echo json_encode($arr);
	die;
    }

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
	$model = new Products();
	$users_Id = Yii::$app->user->identity->id;
	$store_connection = StoresConnection::find()->where(['user_id' => $users_Id])->all();
	$channel_connection = ChannelConnection::find()->where(['user_id' => $users_Id])->all();
	$connections = array();
	foreach ($store_connection as $con) {
	    $store = Stores::find()->select(['store_name', 'store_image'])->where(['store_id' => $con->store_id])->one();
	    $connections[] = $store->store_name;
	}
	foreach ($channel_connection as $con1) {
	    $channel = Channels::find()->select(['channel_name'])->where(['channel_ID' => $con1->channel_id])->one();
	    $connections[] = $channel->channel_name;
	}
	$attribute_types = AttributeType::find()->where(['elliot_user_id' => Yii::$app->user->identity->id])->all();
	if ($model->load(Yii::$app->request->post()) && $model->save()) {
	    return $this->redirect(['view', 'id' => $model->id]);
	} else {
	    return $this->render('create', [
			'model' => $model,
			'connections' => $connections,
			'attribute_types' => $attribute_types,
	    ]);
	}
    }

    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
	$model = $this->findModel($id);

	if ($model->load(Yii::$app->request->post()) && $model->save()) {
	    return $this->redirect(['view', 'id' => $model->id]);
	} else {
	    return $this->render('update', [
			'model' => $model,
	    ]);
	}
    }

    /**
     * Deletes an existing Products model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
	$this->findModel($id)->delete();

	return $this->redirect(['index']);
    }

    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
	if (($model = Products::findOne($id)) !== null) {
	    return $model;
	} else {
	    throw new NotFoundHttpException('The requested page does not exist.');
	}
    }

    /* Function for Google Products Categories */

    public function actionGoogleProductCategory() {
	$query = isset($_REQUEST['query']) ? $_REQUEST['query'] : $_REQUEST['term'];
	$sql = "SELECT google_category_name FROM google_product_categories WHERE google_category_name LIKE '{$query}%'";
	$google_categories = GoogleProductCategories::findBySql($sql)->all();
	$cat_array = array();
	foreach ($google_categories as $cat) {
	    $cat_array[] = $cat->google_category_name;
	}
	//RETURN JSON ARRAY
	return json_encode($cat_array);
    }

    /* Update the Single Product Details */

    public function actionUpdateProduct($pId) {

	$post_data = Yii::$app->request->post();

	$product_model = $this->findModel($pId);
	//General Tab
	$product_name = $_POST['pName'];
	$product_sku = $_POST['pSKU'];
	$product_upc = $_POST['pUPC'];
	$product_desc = $_POST['pDescription'];
	$product_wt = $_POST['pWeight'];
	$availabilty = $_POST['pAvail'];
	$Google_Cat1 = $_POST['Google_Cat1'];
	$Google_Cat2 = $_POST['Google_Cat2'];

	$Google_Cat_id = $_POST['Google_Cat_id'];
	if ($availabilty == 'In Stock' || $availabilty == 'preorder'):
	    $avail = 'available';
	else:
	    $avail = 'disabled';
	endif;

	$condition = $_POST['pCond'];
	//$salePrice = $_POST['pSale_price'];
	$salePrice = preg_replace('/[^a-zA-Z0-9_.]/', '', $_POST['pSale_price']);
	$price = preg_replace('/[^a-zA-Z0-9_.]/', '', $_POST['pPrice']);

	// Price Conversion as Per the Currency Selected( NOT IN CASE OFUSD) By User Starts 
	$user = Yii::$app->user->identity;
	$conversion_rate = 1;
	if (isset($user->currency)) {
	    $conversion_rate=Stores::getDbConversionRate($user->currency);
	   
//	    $username = Yii::$app->params['xe_account_id'];
//	    $password = Yii::$app->params['xe_account_api_key'];
//	    $URL = Yii::$app->params['xe_base_url'] . 'convert_from.json/?from=' . $user->currency . '&to=USD&amount=1';
//	    $ch = curl_init();
//	    curl_setopt($ch, CURLOPT_URL, $URL);
//	    curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds 
//	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//	    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
//	    curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
//	    $result = curl_exec($ch);
//	    $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
//	    curl_close($ch);
//	    $result = json_decode($result, true);
//	    if (isset($result) and ! empty($result) and isset($result['to']) and isset($result['to'][0]) and isset($result['to'][0]['quotecurrency'])) {
//		$conversion_rate = $result['to'][0]['mid'];
//		$no_without_decimal = number_format(str_replace('.', '', $conversion_rate), 5, '', '');
//		if (strstr($conversion_rate, 'E-')) {
//		    $pos = strpos($conversion_rate, 'E-');
//		    $number_to_be_divided = substr($conversion_rate, $pos + 2);
//		    $output = substr($conversion_rate, 0, strrpos($conversion_rate, 'E-'));
//		    $string_pos = strpos($output, '.');
//		    $no_of_zeros = $number_to_be_divided - $string_pos;
//		    $zeros = '.';
//		    for ($i = 0; $i < $no_of_zeros; $i++) {
//			$zeros .= 0;
//		    }
//		    $conversion_rate = $zeros . $no_without_decimal;
//		}
//	    }
	}
	$price = $price * $conversion_rate;
	$salePrice = $salePrice * $conversion_rate;
	$price = number_format((float) $price, 2, '.', '');
	$salePrice = number_format((float) $salePrice, 2, '.', '');

	$post_data['pSale_price'] = $post_data['pSale_price'] * $conversion_rate;
	$post_data['pPrice'] = $post_data['pPrice'] * $conversion_rate;
	$post_data['pSale_price'] = number_format((float) $post_data['pSale_price'], 2, '.', '');
	$post_data['pPrice'] = number_format((float) $post_data['pPrice'], 2, '.', '');

	$connected_price = isset($_POST['Connected_price']) ? $_POST['Connected_price'] : '';

	if (!empty($connected_price)):
	    foreach ($connected_price as $cp_key_price => $cp) :
		$_POST['Connected_price'][$cp_key_price] = number_format($cp * $conversion_rate, 2, '.', '');
	    endforeach;
	endif;

	$connected_sale_price = isset($_POST['Connected_sale_price']) ? $_POST['Connected_sale_price'] : '';
	if (!empty($connected_sale_price)):
	    foreach ($connected_sale_price as $cp_key_sale_price => $csp) :
		$_POST['Connected_sale_price'][$cp_key_sale_price] = number_format($csp * $conversion_rate, 2, '.', '');

	    endforeach;
	endif;

	// Price Conversion as Per the Currency Selected( NOT IN CASE OFUSD) By User Ends
	$stock_qty = $_POST['pStk_qty'];
	$low_stc_ntf = $_POST['plow_stk_ntf'];
	if ($_POST['plow_stk_ntf'] == 'Empty') :
	    $low_stc_ntf = NULL;
	endif;

	$pstk_status = $_POST['pStk_status'];

	if ($pstk_status == "Visible") :
	    $update_pstk_status = "simple";

	else :
	    $update_pstk_status = "none";
	endif;

	$pcat = isset($_POST['pcat']) ? $_POST['pcat'] : '';
	//Delete All product categories acc to product id

	if (!empty($pcat)) :
	    $p_cat_data = ProductCategories::deleteAll('product_ID = :product_ID', [':product_ID' => $pId]);
	    foreach ($pcat as $pcat_data) :
                $pcat_data = str_replace("&amp;","&",$pcat_data);
		$cat_data = Categories::find()->Where(['category_name' => $pcat_data])->one();
		$cat_id = $cat_data->category_ID;
		$product_category_model = new ProductCategories();
		$product_category_model->elliot_user_id = Yii::$app->user->identity->id;
		$product_category_model->category_ID = $cat_id;
		$product_category_model->product_ID = $pId;
		$product_category_model->created_at = date('Y-m-d H:i:s', time());
		$product_category_model->save(false);
	    endforeach;
	endif;
	/* Allocate Inventorey */
	$elli_allocate_inv = $_POST['elli_allocate_inv'];

	$channel_sale = isset($_POST['channel_sale']) ? $_POST['channel_sale'] : '';


	/* Starts Code If Channel Manager untick selected channel */
	$update_stock_connected_channels = Stores::update_stock_connected_channels($channel_sale, $pId, $post_data);

	/* END Code If Channel Manager untick selected channel */

	$product_model->product_name = $_POST['pName'];
	$product_model->SKU = $_POST['pSKU'];
	$product_model->UPC = $_POST['pUPC'];
	$product_model->EAN = $_POST['pEAN'];
	$product_model->Jan = $_POST['pJAN'];
	$product_model->ISBN = $_POST['pISBN'];
	$product_model->MPN = $_POST['pMPN'];
	$product_model->Google_cat1 = $_POST['Google_Cat1'];
	$product_model->Google_cat2 = $_POST['Google_Cat2'];
	$product_model->Google_cat_id = $_POST['Google_Cat_id'];
	$product_model->description = $_POST['pDescription'];
	if ($_POST['pAdult'] == 'Empty') :
	    $_POST['pAdult'] = 'no';
	endif;
	$product_model->adult = $_POST['pAdult'];
	if ($_POST['pAgeGroup'] == 'Empty') :
	    $_POST['pAgeGroup'] = NULL;
	endif;
	$product_model->age_group = $_POST['pAgeGroup'];
	if ($_POST['pAvail'] == 'Empty') :
	    $_POST['pAvail'] = 'Out of Stock';
	endif;
	$product_model->availability = $_POST['pAvail'];
	$product_model->brand = $_POST['pBrand'];
	if ($_POST['pCond'] == 'Empty') :
	    $_POST['pCond'] = 'New';
	endif;
	$product_model->condition = $_POST['pCond'];
	if ($_POST['pGender'] == 'Empty') :
	    $_POST['pGender'] = 'Unisex';
	endif;
	$product_model->gender = $_POST['pGender'];
	$product_model->weight = $_POST['pWeight'];

	//Inventory Management Tab
	$product_model->stock_quantity = $_POST['pStk_qty'];
	$product_model->allocate_inventory = $elli_allocate_inv;
	//$product_model->stock_level = $_POST['pStk_lvl'];
	$product_model->stock_status = $_POST['pStk_status'];
	$product_model->low_stock_notification = $low_stc_ntf;
	//Pricing Tab
	$product_model->price = $price;
	$product_model->sales_price = $salePrice;
	$schedule_date = $_POST['pSchedule_date'];
	$schedule_date_time = date('Y-m-d h:i:s', strtotime($schedule_date));
	$product_model->schedules_sales_date = $schedule_date_time;

	/* Check for update Product in Stores and channels */
	if (!empty($channel_sale)):
	    $Update_products = Stores::update_product($pId, $post_data, $channel_sale);
	endif;

	if ($product_model->save(false)) {

	    /* Create Product Of selected Channel or store */
	    if (!empty($channel_sale)):
		$create_product = Stores::channel_manager_create_product($channel_sale, $pId, $post_data);
		/* Status Active of Product Channel Product */
		foreach ($channel_sale as $ch_sale_update) {

		    $channel_name_explode = explode(" ", $ch_sale_update);
		    $channel_name_check = $channel_name_explode[0];
		    $check_valid_store = Stores::find()->where(['store_name' => $channel_name_check])->one();
		    /* For Stores */
		    if (!empty($check_valid_store)) {
			$fin_channel = $channel_name_check;
			$channel_space_pos = strpos($ch_sale_update, ' ');
			$country_sub_str = substr($ch_sale_update, $channel_space_pos);
			$store_country = trim($country_sub_str);
			$store_details_data = StoreDetails::find()->Where(['channel_accquired' => $fin_channel, 'country' => $store_country])->with('storeConnection')->one();
			if (!empty($store_details_data)) {
			    $store_connection_id = $store_details_data->store_connection_id;
			    echo $store_connection_id;
			    $ProductChannel = ProductChannel::find()->where(['product_id' => $pId, 'store_id' => $store_connection_id])->one();
			    if (!empty($ProductChannel)) {
				$ProductChannel->status = 'yes';
				$ProductChannel->save(false);
			    }
			}
			/* For Channels */
		    } else {
			$fin_channel = $ch_sale_update;

			if (strpos($ch_sale_update, 'Lazada') !== false) {
			    $channel_lazada = Channels::find()->where(['channel_name' => $ch_sale_update])->one();
			    $channel_id = $channel_lazada->channel_ID;
			} else {
			    $ch_sale_update1 = ucwords(str_replace(" ", "-", $ch_sale_update));
			    //get wechat Service account id 
			    $get_big_id = Channels::find()->where(['parent_name' => 'channel_' . $ch_sale_update1])->all();
			    $channel_ids = '';
			    foreach ($get_big_id as $ids) {
				//$arr_id[] = $ids->channel_ID;
				$channel_ids .= "'" . $ids->channel_ID . "',";
				$ids->channel_name;
			    }
			    //$channel_ids = implode(",", $arr_id);
			    $channel_ids = rtrim($channel_ids, ',');
			    $con_details = ChannelConnection::find()->where('channel_id IN(' . $channel_ids . ')')->one();
			    $channel_id = $con_details->channel_id;
			}
			/* For Status inactive in product channel */
			$ProductChannel = ProductChannel::find()->where(['product_id' => $pId, 'channel_id' => $channel_id])->one();
			if (isset($ProductChannel) and ! empty($ProductChannel)) {
			    $ProductChannel->status = 'yes';
			    $ProductChannel->save(false);
			}
		    }
//                    $stores = Stores::find()->where(['store_name' => $ch_sale_update])->one();
//                    if (!empty($stores)) {
//                        $store_id = $stores->store_id;
//                        /* For Status inactive in product channel */
//                        $ProductChannel = ProductChannel::find()->where(['product_id' => $pId, 'store_id' => $store_id])->one();
//                        if (!empty($ProductChannel)) {
//                            $ProductChannel->status = 'yes';
//                            $ProductChannel->save(false);
//                        }
//                    } else {
//                        if (strpos($ch_sale_update, 'Lazada') !== false) {
//                            $channel_lazada = Channels::find()->where(['channel_name' => $ch_sale_update])->one();
//                            $channel_id = $channel_lazada->channel_ID;
//                        } else {
//                            $ch_sale_update1 = ucwords(str_replace(" ", "-", $ch_sale_update));
//                            //get wechat Service account id 
//                            $get_big_id = Channels::find()->where(['parent_name' => 'channel_' . $ch_sale_update1])->all();
//                            $channel_ids = '';
//                            foreach ($get_big_id as $ids) {
//                                //$arr_id[] = $ids->channel_ID;
//                                $channel_ids .= "'" . $ids->channel_ID . "',";
//                                $ids->channel_name;
//                            }
//                            //$channel_ids = implode(",", $arr_id);
//                            $channel_ids = rtrim($channel_ids, ',');
//                            $con_details = ChannelConnection::find()->where('channel_id IN(' . $channel_ids . ')')->one();
//                            $channel_id = $con_details->channel_id;
//                        }
//                        /* For Status inactive in product channel */
//                        $ProductChannel = ProductChannel::find()->where(['product_id' => $pId, 'channel_id' => $channel_id])->one();
//                        if (isset($ProductChannel) and ! empty($ProductChannel)) {
//                            $ProductChannel->status = 'yes';
//                            $ProductChannel->save(false);
//                        }
//                    }
		}

	    endif;
	    Yii::$app->session->setFlash('success', 'Success! Product has been updated.');
	} else {
	    Yii::$app->session->setFlash('danger', 'Error! Product has not been updated.');
	}

	return $this->redirect(['view', 'id' => $product_model->id]);
    }

    /* Temporary Saving or Creating the Single Product **/

    public function actionCreateProductTemp() {
	$product_model = new Products;
	//General Tab
	$product_model->elliot_user_id = Yii::$app->user->identity->id;
	$product_model->product_name = $_POST['pName'];
	$product_model->SKU = $_POST['pSKU'];
	$product_model->UPC = $_POST['pUPC'];
	$product_model->EAN = $_POST['pEAN'];
	$product_model->Jan = $_POST['pJAN'];
	$product_model->ISBN = $_POST['pISBN'];
	$product_model->MPN = $_POST['pMPN'];
	$product_model->description = $_POST['pDes'];
	if ($_POST['pAdult'] == 'Please Select') :
	    $_POST['pAdult'] = 'no';
	endif;
	$product_model->adult = $_POST['pAdult'];
	if ($_POST['pAgeGroup'] == 'Please Select') :
	    $_POST['pAgeGroup'] = NULL;
	endif;
	$product_model->age_group = $_POST['pAgeGroup'];
	if ($_POST['pAvail'] == 'Please Select') :
	    $_POST['pAvail'] = 'Out of Stock';
	endif;
	$product_model->availability = $_POST['pAvail'];
	$product_model->brand = $_POST['pBrand'];
	if ($_POST['pCond'] == 'Please Select') :
	    $_POST['pCond'] = 'New';
	endif;
	$product_model->condition = $_POST['pCond'];
	if ($_POST['pGender'] == 'Please Select') :
	    $_POST['pGender'] = 'Unisex';
	endif;
	$product_model->gender = $_POST['pGender'];
	$product_model->weight = $_POST['pWeight'];
	$created_date = date('Y-m-d h:i:s', time());
	$product_model->created_at = $created_date;

	$product_model->save(false);


	return $product_model->id;
    }

    /* Upload Product Images - Dropzone Form Action */

    public function actionProductImageUpload() {
	$basedir = Yii::getAlias('@basedir');
	$ds = DIRECTORY_SEPARATOR;
	$storeFolder = $basedir . '/img/product_images';
	$error_code = $_FILES['file']['error'];
	$product_id = $_POST['pid'];
	if (!empty($_FILES) && $error_code == 0) {
	    $tempFile = $_FILES['file']['tmp_name'];
	    $imageName = $_FILES['file']['name'];
	    $imageNameArray = explode('.', $imageName);
	    $imageLabel = $imageNameArray[0];
	    $RandomImageId = uniqid();
	    //Generate Unique Name for each Image Uploaded
	    $new_imageName = $RandomImageId . '_' . $imageName;
	    //Path Setting
	    $targetPath = $storeFolder . $ds;
	    $targetFile = $targetPath . $new_imageName;
	    //Save the Uploaded File
	    move_uploaded_file($tempFile, $targetFile);
	    //Image Thumbnail 
	    Image::thumbnail(Yii::getAlias('@product_images/' . $new_imageName), 71, 71)
		    ->save(Yii::getAlias('@product_images/thumbnails/thumb_' . $new_imageName), ['quality' => 100]);
	    //Temp entry into Product_images Table 
	    $pImageModel = new ProductImages;
	    $pImageModel->product_ID = $product_id;
	    $product_image_name = $new_imageName;
	    $product_image_link = Yii::$app->params['BASE_URL'] . 'img/product_images/' . $product_image_name;
	    $pImageModel->link = $product_image_link;
	    $pImageModel->label = $imageLabel;
	    $pImageModel->image_status = 0;
	    $created = date('Y-m-d H:i:s', time());
	    $pImageModel->created_at = $created;
	    if ($pImageModel->save(false)) {
		$imgId = $pImageModel->image_ID;
		$imgLabel = $pImageModel->label;
		$response = ['status' => 'success', 'imgId' => $imgId, 'imgLabel' => $imgLabel, 'data' => 'Product image has been uploaded successfully'];
	    } else {
		$response = ['status' => 'error', 'data' => 'Failed to upload Product Image'];
	    }
	    echo json_encode($response);
	    exit;
	}
    }

    public function actionUploadProductImageVideo360() {
	$basedir = Yii::getAlias('@basedir');
	$ds = DIRECTORY_SEPARATOR;
	$storeFolder = $basedir . '/img/product_videos';
	$error_code = $_FILES['file']['error'];
	$image_id = $_GET['imgId'];
	if (!empty($_FILES) && $error_code == 0) {
	    $tempFile = $_FILES['file']['tmp_name'];
	    $videoName = $_FILES['file']['name'];
	    $videoNameArray = explode('.', $videoName);
	    $videoLabel = $videoNameArray[0];
	    $RandomVideoId = uniqid();
	    //Generate Unique Name for each Image Uploaded
	    $new_videoName = $RandomVideoId . '_' . $videoName;
	    //Path Setting
	    $targetPath = $storeFolder . $ds;
	    $targetFile = $targetPath . $new_videoName;
	    //Save the Uploaded File
	    move_uploaded_file($tempFile, $targetFile);
	    //Temp entry into Product_images Table for 360 video link
	    $pImageModel = ProductImages::find()->where(['image_ID' => $image_id])->one();
	    $product_image_video_name = $new_videoName;
	    $product_image_video_link = Yii::$app->params['BASE_URL'] . 'img/product_videos/' . $product_image_video_name;
	    $pImageModel->_360_degree_video_link = $product_image_video_link;
	    if ($pImageModel->save(false)) {
		$imgId = $pImageModel->image_ID;
		$imgLabel = $pImageModel->label;
		$response = $product_image_video_link;
	    } else {
		$response = '';
	    }
	    return $response;
	}
    }

    /* Save Product Images - Media Step (Product Create) */

    public function actionSaveProductImage() {

	$img_id = isset($_POST['img_Id']) ? $_POST['img_Id'] : '';
	$img_default = isset($_POST['img_default']) ? $_POST['img_default'] : '';
	$img_label = isset($_POST['img_label']) ? $_POST['img_label'] : '';
	$img_alt = isset($_POST['img_alt']) ? $_POST['img_alt'] : '';
	$img_html_video = isset($_POST['img_html_video']) ? $_POST['img_html_video'] : '';
//    $img_360_video = isset($_POST['img_360_video']) ? $_POST['img_360_video'] : '';
	if ($img_label == 'Empty'):
	    $img_label = NULL;
	endif;
	if ($img_alt == 'Empty'):
	    $img_alt = NULL;
	endif;
	if ($img_html_video == 'Empty'):
	    $img_html_video = NULL;
	endif;
//    if ($img_360_video == 'Empty'):
//      $img_360_video = NULL;
//    endif;
	//Update the Image Fields in Product_images Table
	$pImageModel = ProductImages::find()->where(['image_ID' => $img_id])->one();
	$pImageModel->label = $img_label;
	$pImageModel->tag = $img_alt;
	$pImageModel->html_video_link = $img_html_video;
//    $pImageModel->_360_degree_video_link = $img_360_video;
	$pImageModel->save(false);
	return 'success';
    }

    /* Update Product Image Order */

    public function actionUpdateProductImageOrder() {
	$img_id = isset($_POST['ImgId']) ? $_POST['ImgId'] : '';
	$img_order = isset($_POST['ImgOrder']) ? $_POST['ImgOrder'] : '';
	$img_default = isset($_POST['ImgDefault']) ? $_POST['ImgDefault'] : '';
	if ($img_default == 1):
	    $img_default = 'Yes';
	else:
	    $img_default = 'No';
	endif;
	//Update the Image Fields in Product_images Table
	$pImageModel = ProductImages::find()->where(['image_ID' => $img_id])->one();
	$pImageModel->priority = $img_order;
	$pImageModel->default_image = $img_default;
	$pImageModel->save(false);
	return 'success';
    }

    /* Create the Single Product */

    public function actionCreateProduct() {

	$product_id = $_REQUEST['pID'];
	$channel_name = $_POST['vals'];

//        echo'<pre>';
//        print_r($_POST);
//        die;

	$product_model = Products::find()->where(['id' => $product_id])->one();
	//General Tab
	if (!empty($product_model)):
	    $product_model->product_name = $_POST['pName'];
	    $product_model->SKU = $_POST['pSKU'];
	    $product_model->UPC = $_POST['pUPC'];
	    $product_model->EAN = $_POST['pEAN'];
	    $product_model->Jan = $_POST['pJAN'];
	    $product_model->ISBN = $_POST['pISBN'];
	    $product_model->MPN = $_POST['pMPN'];
	    $product_model->Google_cat_id = $_POST['Google_Cat_id'];
	    $product_model->Google_cat1 = $_POST['Google_Cat1'];
	    $product_model->Google_cat2 = $_POST['Google_Cat2'];
	    $product_model->description = $_POST['pDes'];
	    if ($_POST['pAdult'] == 'Please Select') :
		$_POST['pAdult'] = 'no';
	    endif;
	    $product_model->adult = $_POST['pAdult'];
	    if ($_POST['pAgeGroup'] == 'Please Select') :
		$_POST['pAgeGroup'] = NULL;
	    endif;
	    $product_model->age_group = $_POST['pAgeGroup'];
	    if ($_POST['pAvail'] == 'Please Select') :
		$_POST['pAvail'] = 'Out of Stock';
	    endif;
	    $product_model->availability = $_POST['pAvail'];
	    $product_model->brand = $_POST['pBrand'];
	    if ($_POST['pCond'] == 'Please Select') :
		$_POST['pCond'] = 'New';
	    endif;
	    $product_model->condition = $_POST['pCond'];
	    if ($_POST['pGender'] == 'Please Select') :
		$_POST['pGender'] = 'Unisex';
	    endif;
	    $product_model->gender = $_POST['pGender'];
	    $product_model->weight = $_POST['pWeight'];
	    //Attribution Tab
	    $product_model->occasion = $_POST['pOccasion'];
	    $product_model->weather = $_POST['pWeather'];
	    //Inventory Management Tab
	    $product_model->stock_quantity = $_POST['pStk_qty'];
	    if ($_POST['pStk_lvl'] == 'Please Select') :
		$_POST['pStk_lvl'] = 'Out of Stock';
	    endif;
	    $product_model->stock_level = $_POST['pStk_lvl'];
	    if ($_POST['pStk_status'] == 'Please Select') :
		$_POST['pStk_status'] = NULL;
	    endif;
	    $product_model->stock_status = $_POST['pStk_status'];
	    $product_model->low_stock_notification = $_POST['plow_stk_ntf'];
	    // connected_store=
	    //Pricing Tab
	    $product_model->price = $_POST['pPrice'];
	    $product_model->sales_price = $_POST['pSale_price'];
	    $schedule_date = $_POST['pSchedule_date'];
	    $schedule_date_time = date('Y-m-d h:i:s', strtotime($schedule_date));
	    $product_model->schedules_sales_date = $schedule_date_time;
	    $created_date = date('Y-m-d h:i:s', time());
	    $product_model->created_at = $created_date;

	    if ($product_model->save(false)) {
		//Connect Market Place
		if (!empty($_POST['pConnectMarketPlace'])) :
		    $connect_marketplace = $_POST['pConnectMarketPlace'];
		    $stores_data = Stores::find()->Where(['store_name' => $connect_marketplace])->one();
		    if (!empty($stores_data)) :
			$store_id = $stores_data->store_id;
			$product_channel = new ProductChannel();
			$product_channel->store_id = $store_id;
			$product_channel->product_id = $product_model->id;
			$product_channel->created_at = date('Y-m-d h:i:s', time());
			$product_channel->save(false);
		    endif;

		endif;
		/* Make the Status of Uploaded Product Images to 1(Uploaded in Media Step) */
		$product_images = ProductImages::find()->where(['product_ID' => $product_model->id])->all();
		foreach ($product_images as $product_image):
		    $product_image->image_status = 1;
		    $product_image->save();
		endforeach;

		// Create product in Store And Channels
		$product_id = $product_model->id;
		//explode channel Name
		$explode_channel_name = explode(",", $channel_name);

		foreach ($explode_channel_name as $ch):
		    if ($channel_name == 'BigCommerce') :
			// Create Product in bigcommerce
			// pass the object for update product fields
			$pstk_status = $_POST['pStk_status'];
			if ($pstk_status == "Visible") :
			    $update_pstk_status = "simple";
			else :
			    $update_pstk_status = "none";
			endif;

			$object = array(
			    "name" => $_POST['pName'],
			    "sku" => $_POST['pSKU'],
			    "upc" => $_POST['pUPC'],
			    "condition" => $_POST['pCond'],
			    "type" => 'physical',
			    "description" => $_POST['pDes'],
			    "sale_price" => $_POST['pSale_price'],
			    "price" => $_POST['pPrice'],
			    "categories" => [18],
			    "inventory_level" => $_POST['pStk_qty'],
			    "availability" => 'available',
			    "inventory_warning_level" => $_POST['plow_stk_ntf'],
			    "inventory_tracking" => $update_pstk_status,
			    "is_visible" => true,
			    "weight" => $_POST['pWeight']
			);

			$create_product_BigCommerce = Stores:: bigcommerce_create_product($object, $product_id);


		    endif;
		    //Product create Wechat Service Account
		    if ($channel_name == 'Service Account') :

			// Fuction Call In Store Moldel create product in Wechat service account
			$product_images = ProductImages::find()->where(['product_ID' => $product_id, 'default_image' => 'Yes'])->one();
			$thumb_image = $product_images->link;
			$wechat_object = array(
			    "title" => $_POST['pName'],
			    "description" => $_POST['pDes'],
			    "sku" => $_POST['pSKU'],
			    "barcode" => $_POST['pUPC'],
			    "price" => $_POST['pPrice'],
			    "salesPrice" => $_POST['pSale_price'],
			    "unit" => $_POST['pStk_qty'],
			    "thumbnail" => $thumb_image,
			    "weight" => $_POST['pWeight']
			);
			$create_product_wechat = Stores::wechat_create_product($product_id, $wechat_object);

		    endif;
		endforeach;

		$click = '<a href="view/' . $product_model->id . '">Click here</a>';
		Yii::$app->session->setFlash('success', 'Success! Product has been Created.' . $click . ' to view the Product Details.');
	    }
	    else {
		Yii::$app->session->setFlash('danger', 'Error! Product has not been Created.');
	    }
	else:
	    Yii::$app->session->setFlash('danger', 'Error! Product has not been Created.');
	endif;
	return $this->redirect(['create']);
    }

    /* Get Variation Items */

    public function actionGetVariantItems() {
	$var_Id = $_POST['pVar_Id'];
	$var_Name = $_POST['pVar_Name'];
	$var_items = VariationsItemList::find()->where(['variation_id' => $var_Id])->all();
	$items_array = array();
	foreach ($var_items as $var_item):
	    $var_item_id = $var_item->variations_item_list_id;
	    $var_item_name = $var_item->item_name;
	    $items_array[$var_item_id] = $var_item_name;
	endforeach;
	return json_encode($items_array);
    }

    /* Get List of Categories-Elliot */

    public function actionGetCats() {
	$users_Id = Yii::$app->user->identity->id;
	$cats = Categories::find()->where(['elliot_user_id' => $users_Id])->all();
	$connections = array();
	foreach ($cats as $cat) {
	    $arr[] = array("value" => $cat->category_ID, "text" => $cat->category_name);
	}
	echo json_encode($arr);
	die;
    }

    /* Create the Single Product */

    public function actionGetVaritems($var_name) {
	$var_object = Variations::find()->select('variations_ID')->where(['variation_name' => $var_name])->one();
	$var_Id = $var_object->variations_ID;
	$var_items = VariationsItemList::find()->where(['variation_id' => $var_Id])->all();
	$items_array = array();
	foreach ($var_items as $var_item):
	    $var_item_id = $var_item->variations_item_list_id;
	    $var_item_name = $var_item->item_name;
	    $items_array[$var_item_id] = $var_item_name;
	endforeach;
	return json_encode($items_array);
    }

    /* Save Product Variations */

    public function actionSaveProductVariations() {

	$pid = $_POST['pID'];
	$headerArr = isset($_POST['varHeaderArr']) ? $_POST['varHeaderArr'] : array();
	$rowArr = $_POST['varRowArr'];
	$rowid = isset($_POST['row_id']) ? $_POST['row_id'] : '';

	$varSKU = isset($_POST['varSKU']) ? $_POST['varSKU'] : '';
	$varOptionNamesArr = isset($_POST['varOptionNamesArr']) ? $_POST['varOptionNamesArr'] : array();
	$varOptionValuesArr = isset($_POST['varOptionValuesArr']) ? $_POST['varOptionValuesArr'] : array();

	$sku_id = preg_replace("/[^0-9]/", "", $rowid);
	$invetory = isset($rowArr[0]) ? $rowArr[0] : '';
	$price = isset($rowArr[1]) ? $rowArr[1] : '';
	$Weight = isset($rowArr[2]) ? $rowArr[2] : '';

	$object = array(
	    "price" => $price,
	    "cost_price" => $price,
	    "weight" => $Weight,
	    "inventory_level" => $invetory
	);
	$Products_data = Products::find()->select('channel_abb_id')->where(['id' => $pid])->one();
	$channel_abb_id = $Products_data->channel_abb_id;
	$Big_product_id = preg_replace("/[^0-9]/", "", $channel_abb_id);

	$update_sku_bigCommerce = Stores::bigcommerce_update_sku($Big_product_id, $sku_id, $object);

	$varRow_Model = ProductVariation::find()->where(['store_variation_id' => $rowid, 'product_id' => $pid, 'elliot_user_id' => Yii::$app->user->identity->id])->all();
	if (empty($varRow_Model)):
	    //Save SKU
	    $varRowSKU_Model = new ProductVariation;
	    $varRowSKU_Model->product_id = $pid;
	    $varRowSKU_Model->elliot_user_id = Yii::$app->user->identity->id;
	    $varRowSKU_Model->item_name = 'SKU';
	    $varRowSKU_Model->item_value = $varSKU;
	    $varRowSKU_Model->store_variation_id = $rowid;
	    $created_date = date('Y-m-d h:i:s', time());
	    $varRowSKU_Model->created_at = $created_date;
	    $varRowSKU_Model->save(false);
	    //Save Product Variants(Options)
	    foreach ($varOptionNamesArr as $key => $opval):
		$item_name_colon = $opval;
		$item_val = $varOptionValuesArr[$key];
		$item_name = str_replace(":", "", $item_name_colon);
		$get_variation = Variations::find()->select('variations_ID')->where(['variation_name' => $item_name, 'elliot_user_id' => Yii::$app->user->identity->id])->one();
		$varOp_Model = new ProductVariation;
		$varOp_Model->product_id = $pid;
		$varOp_Model->elliot_user_id = Yii::$app->user->identity->id;
		$varOp_Model->variation_id = $get_variation->variations_ID;
		$varOp_Model->item_name = $item_name;
		if ($item_val == 'Empty'):
		    $item_val = '';
		endif;
		$varOp_Model->item_value = $item_val;
		$varOp_Model->store_variation_id = $rowid;
		$created_date = date('Y-m-d h:i:s', time());
		$varOp_Model->created_at = $created_date;
		$varOp_Model->save(false);
	    endforeach;
	    //Save Inventory,Price,Weight
	    foreach ($headerArr as $key => $hval):
		$item_name = $hval;
		if ($item_name == 'Price' || $item_name == 'Weight'):
		    $num = $rowArr[$key];
		    $real_num = str_replace(',', '', $num);
		    $item_val = $real_num;
		else:
		    $item_val = $rowArr[$key];
		endif;
		$varRowItem_Model = new ProductVariation;
		if ($item_val == 'Empty'):
		    $item_val = '';
		endif;
		$varRowItem_Model->product_id = $pid;
		$varRowItem_Model->elliot_user_id = Yii::$app->user->identity->id;
		$varRowItem_Model->item_name = $item_name;
		$varRowItem_Model->item_value = $item_val;
		$varRowItem_Model->store_variation_id = $rowid;
		$created_date = date('Y-m-d h:i:s', time());
		$varRowItem_Model->created_at = $created_date;
		$varRowItem_Model->save(false);
	    endforeach;
	else:
	    //Save SKU
	    $varRowSKU_Model = ProductVariation::find()->where(['store_variation_id' => $rowid, 'product_id' => $pid, 'item_name' => 'SKU', 'elliot_user_id' => Yii::$app->user->identity->id])->one();
	    $varRowSKU_Model->item_value = $varSKU;
	    $varRowSKU_Model->save(false);
	    //Save Product Variants(Options)
	    foreach ($varOptionNamesArr as $key => $opval):
		$item_name_colon = $opval;
		$item_val = $varOptionValuesArr[$key];
		$item_name = rtrim($item_name_colon, ':');
		$varOp_Model = ProductVariation::find()->where(['store_variation_id' => $rowid, 'product_id' => $pid, 'item_name' => $item_name, 'elliot_user_id' => Yii::$app->user->identity->id])->one();
		if (!empty($varOp_Model)):
		    if ($item_val == 'Empty'):
			$item_val = '';
		    endif;
		    $varOp_Model->item_value = $item_val;
		    $varOp_Model->save(false);
		endif;
	    endforeach;
	    //Save Inventory,Price,Weight
	    foreach ($headerArr as $key => $hval):
		$item_name = $hval;
		if ($item_name == 'Price' || $item_name == 'Weight'):
		    $num = $rowArr[$key];
		    $real_num = str_replace(',', '', $num);
		    $item_val = $real_num;
		else:
		    $item_val = $rowArr[$key];
		endif;
		$varRowItem_Model = ProductVariation::find()->where(['store_variation_id' => $rowid, 'product_id' => $pid, 'item_name' => $item_name, 'elliot_user_id' => Yii::$app->user->identity->id])->one();
		if (!empty($varRowItem_Model)):
		    if ($item_val == 'Empty'):
			$item_val = '';
		    endif;
		    $varRowItem_Model->item_value = $item_val;
		    $varRowItem_Model->save(false);
		endif;
	    endforeach;
	endif;
	return 'success';
    }

    /* Remove Product Variations */

    public function actionRemoveProductVariations() {
	$pid = $_POST['pID'];
	$rowid = $_POST['tr_id'];
	if (!empty($rowid)):
	    $var_Model = ProductVariation::find()->where(['store_variation_id' => $rowid, 'product_id' => $pid])->all();
	    if (!empty($var_Model)):
		foreach ($var_Model as $var):
		    $var->delete();
		endforeach;
	    endif;
	endif;
	return 'success';
    }

    /* Get Variations Set */

    public function actionGetVarSet() {
	$var_sets = VariationsSet::find()->where(['elliot_user_id' => Yii::$app->user->identity->id])->all();
	$sets_array = array();
	foreach ($var_sets as $var_set):
	    $var_set_id = $var_set->variations_set_id;
	    $var_set_name = $var_set->variations_set_name;
	    $sets_array[$var_set_id] = $var_set_name;
	endforeach;
	return json_encode($sets_array);
    }

    //Product Variants Combination 
    public function actionAddProductVariationsRow() {
	$var_set = $_POST['var_set'];
	$op_set_object = VariationsSet::find()->where(['elliot_user_id' => Yii::$app->user->identity->id, 'variations_set_name' => $var_set])->one();
	$op_ids_arr = array();
	if (!empty($op_set_object)):
	    $op_ids_arr = explode(",", $op_set_object->variations_set_options_ids);
	endif;
	$var_name_arr = array();
	foreach ($op_ids_arr as $key => $val):
	    $var_obj = Variations::find()->where(['variations_ID' => $val])->one();
	    array_push($var_name_arr, $var_obj->variation_name);
	endforeach;
	return json_encode($var_name_arr);
    }

    //Get option Set Variations
    public function actionGetOpsetVariations() {
	$var_set_id = $_POST['opset_id'];
	$op_set_object = VariationsSet::find()->where(['variations_set_id' => $var_set_id, 'elliot_user_id' => Yii::$app->user->identity->id])->one();
	$op_set = '';
	$op_ids_arr = array();
	if (!empty($op_set_object)):
	    $op_set = $op_set_object->variations_set_name;
	    $op_ids_arr = explode(",", $op_set_object->variations_set_options_ids);
	endif;
	$var_name_arr = array();
	foreach ($op_ids_arr as $key => $val):
	    $var_obj = Variations::find()->where(['variations_ID' => $val])->one();
	    array_push($var_name_arr, $var_obj->variation_name);
	endforeach;
	return json_encode($var_name_arr);
    }

    //Get Attributes related to Attribute Type
    public function actionGetAttributes() {
	$attr_type_id = $_GET['attr_type'];
	$attrs = Attributes::find()->where(['attribute_type' => $attr_type_id, 'elliot_user_id' => Yii::$app->user->identity->id])->all();
	$attr_arr = array();
	foreach ($attrs as $key => $attr_data):
	    $attr_name = $attr_data->attribute_name;
	    $attr_arr[$key][$attr_data->attribute_id] = $attr_name;
	endforeach;

	echo json_encode($attr_arr);
    }

    /*
     * showing graph 
     * ajax hit from custom_graph.js
     */

    public function actionDonutchartonproducts() {
	if (!empty($_POST['data'])) {
	    $post = $_POST['data'];
	}

	$data_id = '';
	$btn_month_disable = '';
	$btn_quarter_disable = '';
	/*	 * ***************************FOR TODAY******************************************** */
	if ($post == 'donutcharttoday' || $post == 'donutcharttodaymob') {
	    $currentdate = date('Y-m-d');
	    $connection = \Yii::$app->db;
	    $orders_data = $connection->createCommand('SELECT * from orders_products Where date(created_at)= "' . $currentdate . '"');
	    $model = $orders_data->queryAll();
	    $categoryname = Categories::find()->where(['elliot_user_id' => Yii::$app->user->identity->id])->all();
	}
	/*	 * ***************************FOR WEEK******************************************** */
	if ($post == 'donutchartweek' || $post == 'donutchartweekmob') {
	    $Week_previous_date = date('Y-m-d', strtotime('-7  days'));
	    $currentdate = date('Y-m-d');
	    $connection = \Yii::$app->db;
	    $orders_data = $connection->createCommand('SELECT * from orders_products Where date(created_at) BETWEEN "' . $Week_previous_date . '" AND "' . $currentdate . '"');
	    $model = $orders_data->queryAll();
	    $categoryname = Categories::find()->where(['elliot_user_id' => Yii::$app->user->identity->id])->all();
	}
	/*	 * ***************************FOR MONTH******************************************** */
	if ($post == 'donutchartmonth' || $post == 'donutchartmonthmob') {
	    $currentmonth = date('m');
	    $currentyear = date('Y');
	    $connection = \Yii::$app->db;
	    $orders_data = $connection->createCommand('SELECT * from orders_products Where  month(created_at)="' . $currentmonth . '" AND year(created_at)="' . $currentyear . '"');
	    $model = $orders_data->queryAll();
	    $categoryname = Categories::find()->where(['elliot_user_id' => Yii::$app->user->identity->id])->all();
	}
	/*	 * ***************************FOR QUARTER******************************************** */
	if ($post == 'donutchartQuarter' || $post == 'donutchartQuartermob' || $data_id == 'donutchartQuarter') {

	    $currentmonth = date('m');
	    $val = $currentmonth - 3;
	    $currentmonth_pre = date("m", mktime(0, 0, 0, $val, 10));
	    $currentyear = date('Y');
	    $connection = \Yii::$app->db;
	    $orders_data = $connection->createCommand('SELECT * from orders_products Where  month(created_at) BETWEEN "' . $currentmonth_pre . '" AND "' . $currentyear . '" AND year(created_at)="' . $currentyear . '"');
	    $model = $orders_data->queryAll();
	    $categoryname = Categories::find()->where(['elliot_user_id' => Yii::$app->user->identity->id])->all();
	}
	/*	 * ***************************FOR YEAR******************************************** */
	if ($post == 'donutchartyear' || $post == 'donutchartyearmob') {
	    $currentyear = date('Y');
	    $lasttenyear = date('Y', strtotime('-10 years'));
	    $connection = \Yii::$app->db;
	    //$orders_data = $connection->createCommand('SELECT * from orders_products Where year(created_at) BETWEEN "' . $lasttenyear . '" AND "' . $currentyear . '"');
	    $orders_data = $connection->createCommand('SELECT * from orders_products Where  year(created_at)="' . $currentyear . '"');
	    $model = $orders_data->queryAll();

	    $categoryname = Categories::find()->where(['elliot_user_id' => Yii::$app->user->identity->id])->all();
//             echo '<pre>';
//            print_r($model);
	}

	if (!empty($model)) {
	    foreach ($model as $models) {
		$categoryid[] = ProductCategories::find()->select('product_ID,category_ID')->where(['product_ID' => $models['product_Id']])->groupBy('category_ID')->all();
	    }
	} else {
	    $data = 'invalid';
	    return \yii\helpers\Json::encode($data);
	}

	$count = 0;
	$customarr = array();
	foreach ($categoryid as $cate) {
	    foreach ($cate as $ctegory) {
		if (count($ctegory) == 0) {
		    $count += count($ctegory);
		} else {
		    $count += count($ctegory);
		    $catID = $ctegory['category_ID'];
		    if (array_key_exists($catID, $customarr)) {
			$customarr[$catID][] = array(
			    'productID' => $ctegory['product_ID'],
			    'categoryID' => $ctegory['category_ID'],
			);
		    } else {
			$customarr[$catID][] = array(
			    'productID' => $ctegory['product_ID'],
			    'categoryID' => $ctegory['category_ID'],
			);
		    }
		}
	    }
	}
	if ($count == 0) {
	    $data = 'invalid';
	    return \yii\helpers\Json::encode($data);
	}

	$arr3 = array();
	foreach ($customarr as $key => $val) {
	    $arr3[$key] = count($val);
	}

	$array5 = array();
	foreach ($categoryname as $catename) {
	    $array4 = array();
	    if (array_key_exists($catename->category_ID, $arr3)) {
		$array4['label'] = $catename->category_name;
		$array4['value'] = $arr3[$catename->category_ID];
	    } else {
		$array4['label'] = $catename->category_name;
		$array4['value'] = 0;
	    }
	    $array5[] = $array4;
	}

	$count = '';
	foreach ($array5 as $cheknull) {
	    $count += $cheknull['value'];
	}
	$colorarr = array(
	    0 => '#0091ea',
	    1 => '#00b0ff',
	    2 => '#40c4ff',
	    3 => '#80d8ff',
	    4 => '#01579b',
	    5 => '#0277bd',
	    6 => '#039be5',
	    7 => '#03a9f4',
	    8 => '#b3e5fc',
	    9 => '#81d4fa',
	    10 => '#29b6f6',
	    11 => '#e1f5fe',
	    12 => '#b5af79',
	    13 => '#914f84',
	    14 => '#ce6d87',
	    15 => '#e04a72',
	    16 => '#e3f2fd',
	    17 => '#bbdefb',
	    18 => '#304ffe',
	    19 => '#3d5afe',
	    20 => '#536dfe',
	    21 => '#8c9eff',
	    22 => '#1a237e',
	    23 => '#283593',
	    24 => '#303f9f',
	    25 => '#3949ab',
	    26 => '#3f51b5',
	    27 => '#5c6bc0',
	    28 => '#7986cb',
	    29 => '#9fa8da',
	    30 => '#c5cae9',
	    31 => '#e8eaf6',
	    32 => '#e8eaf6',
	    33 => '#0000ff',
	    34 => '#00bfff',
	    35 => '#0000e5',
	    36 => '#19c5ff',
	    37 => '#0000cc',
	    38 => '#32cbff',
	    39 => '#0000b2',
	    40 => '#4cd2ff',
	    41 => '#0086b3',
	    42 => '#0099cc',
	    43 => '#00ace6',
	    44 => '#00bfff',
	    45 => '#4dd2ff',
	    46 => '#33ccff',
	    47 => '#1ac5ff',
	    48 => '#bc8f8f',
	    49 => '#cd5c5c',
	    50 => '#8b4513',
	    51 => '#a0522d',
	    52 => '#cd853f',
	    53 => '#deb887',
	    54 => '#f5f5dc',
	    55 => '#d2b48c',
	    56 => '#e9967a',
	    57 => '#fa8072',
	    58 => '#ffa07a',
	    59 => '#0099cc',
	    60 => '#00ace6',
	    61 => '#00bfff',
	    62 => '#4dd2ff',
	    63 => '#33ccff',
	    64 => '#1ac5ff',
	    65 => '#bc8f8f',
	    66 => '#cd5c5c',
	    67 => '#8b4513',
	    67 => '#a0522d',
	    68 => '#cd853f',
	    69 => '#0091ea',
	    70 => '#00b0ff',
	    71 => '#40c4ff',
	    72 => '#80d8ff',
	    73 => '#01579b',
	    74 => '#0277bd',
	    75 => '#039be5',
	    76 => '#03a9f4',
	    77 => '#b3e5fc',
	    78 => '#81d4fa',
	    79 => '#29b6f6',
	    80 => '#e1f5fe',
	    81 => '#b5af79',
	    82 => '#914f84',
	    83 => '#ce6d87',
	    84 => '#e04a72',
	    85 => '#e3f2fd'
	);
	$randcolor = array();
	$randcolor_2 = array();
	$c = 0;

	foreach ($array5 as $val) {
	    $randcolor[] = $colorarr[$c++];
	}

	$randcolor_2 = $randcolor;
	if ($count > 0) {
	    $data = array('data' => json_encode($array5), 'colors' => json_encode($randcolor_2), 'id' => json_encode($post), 'btn_month' => json_encode($btn_month_disable), 'btn_quarter' => json_encode($btn_quarter_disable));
	    return \yii\helpers\Json::encode($data);
	} else {
	    $data = 'invalid';
	    return \yii\helpers\Json::encode($data);
	}
    }

    public function actionAjaxProductDelete() {

	if (!isset($_POST['productIds']) || count($_POST['productIds']) == 0) {
	    return FALSE;
	}
	$product_ids = $_POST['productIds'];
	$count_value = '';
	$product_stk_qty = 0;
	$all_products_channel_status = 'no';
	foreach ($product_ids as $_product_id) {
	    $delete_product = Products::updateAll(array('product_status' => 'in_active'), 'id = ' . $_product_id);
	    $count_value += $delete_product;
	    $ProductChannel = ProductAbbrivation::find()->where(['product_id' => $_product_id])->all();
	    ProductAbbrivation::updateAll(array('stock_quantity' => 0, 'stock_level' => 'Out of Stock'), 'product_id = ' . $_product_id);
	    if (!empty($ProductChannel)) {
		foreach ($ProductChannel as $_productChannel) {
		    $channel_abb_id = $_productChannel->channel_abb_id;
		    $product_channel_name = $_productChannel->channel_accquired;
		    $stores_name = $this->getAllStoreName();
		    if (in_array($product_channel_name, $stores_name)) {
			$multi_store_id = $_productChannel->mul_store_id;
			if ($product_channel_name == 'BigCommerce') {
			    $store_details_data = StoreDetails::find()->Where(['channel_accquired' => 'BigCommerce', 'store_connection_id' => $multi_store_id])->with('storeConnection')->one();
			    $this->removeQtyBigcommerce($_product_id, $channel_abb_id, $product_stk_qty, $all_products_channel_status, $store_details_data);
			}
			if ($product_channel_name == 'Shopify') {
			    $store_name = 'Shopify';
			    $store_details_data = StoreDetails::find()->Where(['channel_accquired' => $store_name, 'store_connection_id' => $multi_store_id])->with('storeConnection')->one();
			    $this->removeQtyShopify($_product_id, $product_stk_qty, $all_products_channel_status, $store_details_data, $store_name);
			}
			if ($product_channel_name == 'ShopifyPlus') {
			    $store_name = 'ShopifyPlus';
			    $store_details_data = StoreDetails::find()->Where(['channel_accquired' => $store_name, 'store_connection_id' => $multi_store_id])->with('storeConnection')->one();
			    $this->removeQtyShopify($_product_id, $product_stk_qty, $all_products_channel_status, $store_details_data, $store_name);
			}
			if ($product_channel_name == 'VTEX') {
			    $store_name = 'VTEX';
			    $store_details_data = StoreDetails::find()->Where(['channel_accquired' => $store_name, 'store_connection_id' => $multi_store_id])->with('storeConnection')->one();
			    $this->removeQtyVtex($_product_id, $product_stk_qty, $all_products_channel_status, $store_details_data, $store_name);
			}
			if ($product_channel_name == 'Magento') {
			    $store_details_data = StoreDetails::find()->Where(['channel_accquired' => 'Magento', 'store_connection_id' => $multi_store_id])->with('storeConnection')->one();
			    $this->removeQtyMagento($_product_id, $product_stk_qty, $all_products_channel_status, $store_details_data);
			}
			if ($product_channel_name == 'Magento2') {
			    $store_details_data = StoreDetails::find()->Where(['channel_accquired' => 'Magento2', 'store_connection_id' => $multi_store_id])->with('storeConnection')->one();
			    $this->removeQtyMagento2($_product_id, $product_stk_qty, $all_products_channel_status, $store_details_data);
			}
		    } else {
			if ($product_channel_name == 'WeChat') {
			    $this->removeQtyWechat($_product_id, $channel_abb_id, $product_stk_qty, 'WeChat', $all_products_channel_status);
			}
			if ($product_channel_name == 'Google Shopping') {
			    /* For Status inactive in product channel */
			    $channel_data = Channels::find()->Where(['channel_name' => 'Google Shopping'])->one();
			    $channel_id = $channel_data->channel_ID;
			    $ProductChannel = ProductChannel::find()->where(['product_id' => $_product_id, 'channel_id' => $channel_id])->one();
			    $ProductChannel->status = 'no';
			    $ProductChannel->save(false);
			}
			if ($product_channel_name == 'Facebook') {
			    /* For Status inactive in product channel */
			    $channel_data = Channels::find()->Where(['channel_name' => 'Facebook'])->one();
			    $channel_id = $channel_data->channel_ID;
			    $ProductChannel = ProductChannel::find()->where(['product_id' => $_product_id, 'channel_id' => $channel_id])->one();
			    $ProductChannel->status = 'no';
			    $ProductChannel->save(false);
			}
			if ($product_channel_name == 'Square') {
			    $this->removeQtySquare($_product_id, $product_stk_qty, $all_products_channel_status);
			}
			if ($product_channel_name == 'Lazada Malaysia') {
			    $this->removeQtyLazadaMalaysia($_product_id, $product_stk_qty, $all_products_channel_status);
			}
		    }
		}
	    }
	}
	// $record = ($count_value == 1) ? "record" : "records";
	// Yii::$app->session->setFlash('success', 'Total of ' . $count_value . ' ' . $record . '  deleted!');
	// return;
    }

    public function actionAjaxInactiveProductDelete() {

	//$post = Yii::$app->request->get();
	if (!isset($_POST['checkedProductID']) || count($_POST['checkedProductID']) == 0) {
	    return false;
	}
	$product_ids = $_POST['checkedProductID'];
	$count_value = '';
	$product_stk_qty = 1;
	$all_products_channel_status = 'yes';
	foreach ($product_ids as $_product_id) {
	    $undo_delete_product = Products::updateAll(array('product_status' => 'active'), 'id = ' . $_product_id);
	    $count_value += $undo_delete_product;
	    $ProductChannel = ProductAbbrivation::find()->where(['product_id' => $_product_id])->all();
	    ProductAbbrivation::updateAll(array('stock_quantity' => 1, 'stock_level' => 'In Stock'), 'product_id = ' . $_product_id);
	    if (!empty($ProductChannel)) {
		foreach ($ProductChannel as $_productChannel) {
		    $channel_abb_id = $_productChannel->channel_abb_id;
		    $product_channel_name = $_productChannel->channel_accquired;

		    $stores_name = $this->getAllStoreName();
		    if (in_array($product_channel_name, $stores_name)) {
			$multi_store_id = $_productChannel->mul_store_id;
			if ($product_channel_name == 'BigCommerce') {
			    $store_details_data = StoreDetails::find()->Where(['channel_accquired' => 'BigCommerce', 'store_connection_id' => $multi_store_id])->with('storeConnection')->one();
			    $this->removeQtyBigcommerce($_product_id, $channel_abb_id, $product_stk_qty, $all_products_channel_status, $store_details_data);
			}
			if ($product_channel_name == 'Shopify') {
			    $store_name = 'Shopify';
			    $store_details_data = StoreDetails::find()->Where(['channel_accquired' => 'Shopify', 'store_connection_id' => $multi_store_id])->with('storeConnection')->one();
			    $this->removeQtyShopify($_product_id, $product_stk_qty, $all_products_channel_status, $store_details_data, $store_name);
			}
			if ($product_channel_name == 'ShopifyPlus') {
			    $store_name = 'ShopifyPlus';
			    $store_details_data = StoreDetails::find()->Where(['channel_accquired' => $store_name, 'store_connection_id' => $multi_store_id])->with('storeConnection')->one();
			    $this->removeQtyShopify($_product_id, $product_stk_qty, $all_products_channel_status, $store_details_data, $store_name);
			}
			if ($product_channel_name == 'VTEX') {
			    $store_name = 'VTEX';
			    $store_details_data = StoreDetails::find()->Where(['channel_accquired' => $store_name, 'store_connection_id' => $multi_store_id])->with('storeConnection')->one();
			    $this->removeQtyVtex($_product_id, $product_stk_qty, $all_products_channel_status, $store_details_data, $store_name);
			}
			if ($product_channel_name == 'Magento') {
			    $store_details_data = StoreDetails::find()->Where(['channel_accquired' => 'Magento', 'store_connection_id' => $multi_store_id])->with('storeConnection')->one();
			    $this->removeQtyMagento($_product_id, $product_stk_qty, $all_products_channel_status, $store_details_data);
			}
			if ($product_channel_name == 'Magento2') {
			    $store_details_data = StoreDetails::find()->Where(['channel_accquired' => 'Magento2', 'store_connection_id' => $multi_store_id])->with('storeConnection')->one();
			    $this->removeQtyMagento2($_product_id, $product_stk_qty, $all_products_channel_status, $store_details_data);
			}
		    } else {
			if ($product_channel_name == 'WeChat') {
			    $this->removeQtyWechat($_product_id, $channel_abb_id, $product_stk_qty, 'WeChat', $all_products_channel_status);
			}
			if ($product_channel_name == 'Google Shopping') {
			    /* For Status inactive in product channel */
			    $channel_data = Channels::find()->Where(['channel_name' => 'Google Shopping'])->one();
			    $channel_id = $channel_data->channel_ID;
			    $ProductChannel = ProductChannel::find()->where(['product_id' => $_product_id, 'channel_id' => $channel_id])->one();
			    $ProductChannel->status = $all_products_channel_status;
			    $ProductChannel->save(false);
			}
			if ($product_channel_name == 'Facebook') {
			    /* For Status inactive in product channel */
			    $channel_data = Channels::find()->Where(['channel_name' => 'Facebook'])->one();
			    $channel_id = $channel_data->channel_ID;
			    $ProductChannel = ProductChannel::find()->where(['product_id' => $_product_id, 'channel_id' => $channel_id])->one();
			    $ProductChannel->status = $all_products_channel_status;
			    $ProductChannel->save(false);
			}
			if ($product_channel_name == 'Square') {
			    $this->removeQtySquare($_product_id, $product_stk_qty, $all_products_channel_status);
			}
			if ($product_channel_name == 'Lazada Malaysia') {
			    $this->removeQtyLazadaMalaysia($_product_id, $product_stk_qty, $all_products_channel_status);
			}
		    }
		}
	    }
	}
	// $record = ($count_value == 1) ? "product" : "products";
	// Yii::$app->session->setFlash('success', 'Total of ' . $count_value . ' ' . $record . '  restored!');
	// return;
    }

    /**
     * Product qty 0 in BigCommerce
     * @param type $product_id
     * @param type $channel_abb_id
     * @param type $product_stk_qty
     */
    public function removeQtyBigcommerce($product_id, $channel_abb_id, $product_stk_qty, $all_products_channel_status, $store_details_data) {
	$users_Id = Yii::$app->user->identity->id;
	if (!empty($store_details_data)) {
	    $store_connection_id = $store_details_data->store_connection_id;
	    $big_client_id = $store_details_data->storeConnection->big_client_id;
	    $big_access_token = $store_details_data->storeConnection->big_access_token;
	    $big_store_hash = $store_details_data->storeConnection->big_store_hash;

	    //BigCommerce Configuration Object
	    Bigcommerce::configure(array(
		'client_id' => $big_client_id,
		'auth_token' => $big_access_token,
		'store_hash' => $big_store_hash
	    ));
	    /* Get channel Abbrivation Id */
	    $bgc_id = preg_replace("/[^0-9,]/", "", $channel_abb_id);

	    $Bigcommerce_object = array("inventory_level" => $product_stk_qty);
	    $products_res = Bigcommerce::updateResource('/products/' . $bgc_id, $Bigcommerce_object);

	    /* For Status inactive in product channel */
	    $ProductChannel = ProductChannel::find()->where(['product_id' => $product_id, 'store_id' => $store_connection_id])->one();
	    $ProductChannel->status = $all_products_channel_status;
	    $ProductChannel->save(false);
	}
    }

    /**
     * Product qty 0 in WeChat
     * @param type $_product_id
     * @param type $channel_abb_id
     * @param type $product_stk_qty
     * @param type $channel
     */
    public function removeQtyWechat($_product_id, $channel_abb_id, $product_stk_qty, $channel, $all_products_channel_status) {
	//get wechat Service account id 
	$get_wechat_id = Channels::find()->where(['parent_name' => 'channel_' . $channel])->all();
	foreach ($get_wechat_id as $ids) {
	    $arr_id[] = $ids->channel_ID;
	    $arr_name[] = $ids->channel_name;
	    $ids->channel_name;
	}
	//$channel_ids = implode(",", $arr_id);
	$channel_ids = join("','", $arr_id);
	$channel_ids = "'" . $channel_ids . "'";
	$con_details = ChannelConnection::find()->where("channel_id IN(" . $channel_ids . ")")->one();
	$token = $con_details->token;
	$project_id = $con_details->wechat_project_id;
	$user_id = $con_details->user_id;
	$channel_id = $con_details->channel_id;

	$product_data = ProductAbbrivation::find()->where(['product_id' => $_product_id, 'channel_accquired' => 'WeChat'])->one();
	$wechat_product_id = substr($product_data->channel_abb_id, 2);
	$wechat_id = $product_data->wechat_id;

	/* For Status inactive in product channel */
	$ProductChannel = ProductChannel::find()->where(['product_id' => $_product_id, 'channel_id' => $channel_id])->one();
	$ProductChannel->status = $all_products_channel_status;
	$ProductChannel->save(false);

	$url = 'https://cms-api.walkthechat.com/products/groups/' . $wechat_product_id;
	$c_cat = curl_init();
	$method = "GET";
	curl_setopt_array($c_cat, array(
	    CURLOPT_URL => "$url",
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_ENCODING => "",
	    CURLOPT_MAXREDIRS => 10,
	    CURLOPT_TIMEOUT => FALSE,
	    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	    CURLOPT_CUSTOMREQUEST => $method,
	    CURLOPT_POSTFIELDS => "category=",
	    CURLOPT_HTTPHEADER => array(
		"x-access-token: $token",
		"x-id-project: $project_id"
	    ),
	));
	$rs = curl_exec($c_cat);
	$d_rs = json_decode($rs);
	$wechat_product_exists = isset($d_rs->products) ? $d_rs->products : '';

	if (!empty($wechat_product_exists)) :
	    $product_data = $d_rs->products->en->product;
	    $product_data->unit = $product_stk_qty;
	    //unset($product_data->categoryGroupId);
	    $product_data_arr = (array) $product_data;
	    $url = 'https://cms-api.walkthechat.com/products/' . $wechat_id;
	    $c_cat = curl_init();
	    $method = "PUT";
	    curl_setopt_array($c_cat, array(
		CURLOPT_URL => "$url",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => FALSE,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => $method,
		CURLOPT_POSTFIELDS => http_build_query($product_data_arr),
		CURLOPT_HTTPHEADER => array(
		    "x-access-token: $token",
		    "x-id-project: $project_id"
		),
	    ));
	    $rs1 = curl_exec($c_cat);
	    $d_rs1 = json_decode($rs1);

	endif;
    }

    /** Product qty 0 in Shopify  */
    public function removeQtyShopify($product_id, $product_stk_qty, $all_products_channel_status, $store_details_data, $store_name) {
	if (!empty($store_details_data)) {
	    $multi_store_id = $store_details_data->store_connection_id;
	    stores::shopifyProductStockNull($product_id, $product_stk_qty, $store_details_data, $store_name);
	    $ProductChannel = ProductChannel::find()->where(['product_id' => $product_id, 'store_id' => $multi_store_id])->one();
	    $ProductChannel->status = $all_products_channel_status;
	    $ProductChannel->save(false);
	}
    }

    /** Product qty 0 in Vtex  */
    public function removeQtyVtex($product_id, $product_stk_qty, $all_products_channel_status, $store_details_data, $store_name) {
	if (!empty($store_details_data)) {
	    $multi_store_id = $store_details_data->store_connection_id;
	    stores::vtexProductStockNull($product_id, $product_stk_qty, $store_details_data, $store_name);
	    $ProductChannel = ProductChannel::find()->where(['product_id' => $product_id, 'store_id' => $multi_store_id])->one();
	    $ProductChannel->status = $all_products_channel_status;
	    $ProductChannel->save(false);
	}
    }

    /** Product qty 0 in Magento  */
    public function removeQtyMagento($product_id, $product_stk_qty, $all_products_channel_status, $store_details_data) {
	if (!empty($store_details_data)) {
	    $multi_store_id = $store_details_data->store_connection_id;
	    stores::magentoProductStockNull($product_id, $product_stk_qty, $store_details_data);
	    $get_magento_data = Stores::find()->select('store_id')->where(['store_name' => 'Magento'])->one();
	    $magento_store_id = $get_magento_data->store_id;
	    $ProductChannel = ProductChannel::find()->where(['product_id' => $product_id, 'store_id' => $multi_store_id])->one();
	    $ProductChannel->status = $all_products_channel_status;
	    $ProductChannel->save(false);
	}
    }

    /** Product qty 0 in Magento 2  */
    public function removeQtyMagento2($product_id, $product_stk_qty, $all_products_channel_status, $store_details_data) {
	if (!empty($store_details_data)) {
	    $multi_store_id = $store_details_data->store_connection_id;
	    stores::magento2ProductStockNull($product_id, $product_stk_qty, $store_details_data);
	    $ProductChannel = ProductChannel::find()->where(['product_id' => $product_id, 'store_id' => $multi_store_id])->one();
	    $ProductChannel->status = $all_products_channel_status;
	    $ProductChannel->save(false);
	}
    }

    /** Product qty 0 in Square */
    public function removeQtySquare($product_id, $product_stk_qty, $all_products_channel_status) {
	stores::squareProductStockNull($product_id, $product_stk_qty);
	$channel_data = Channels::find()->where(['channel_name' => 'Square'])->one();
	$square_channel_id = $channel_data->channel_ID;
	$ProductChannel = ProductChannel::find()->where(['product_id' => $product_id, 'channel_id' => $square_channel_id])->one();
	if (!empty($ProductChannel)) {
	    $ProductChannel->status = $all_products_channel_status;
	    $ProductChannel->save(false);
	}
    }

    /** Product qty 0 in Lazada Malaysia  */
    public function removeQtyLazadaMalaysia($product_id, $product_stk_qty, $all_products_channel_status) {
	stores::lazadaMalaysiaProductStockNull($product_id, 'Lazada Malaysia', $product_stk_qty);
	$channel_data = Channels::find()->where(['channel_name' => 'Lazada Malaysia'])->one();
	$channel_id = $channel_data->channel_ID;
	$ProductChannel = ProductChannel::find()->where(['product_id' => $product_id, 'channel_id' => $channel_id])->one();
	if (!empty($ProductChannel)) {
	    $ProductChannel->status = $all_products_channel_status;
	    $ProductChannel->save(false);
	}
    }

    public function actionTest() {
	echo Stores::getCurrencyConversionRate('USD', 'INR');
    }

    /*
     * ajax hit from custom_graph.js for pie chart data
     */

    public function actionPiechartonproduct() {
	$post = $_POST['data'];
	$connectedchannel = ChannelConnection::find()->where(['user_id' => Yii::$app->user->identity->id])->all();
	$connectedstore = StoresConnection::find()->where(['user_id' => Yii::$app->user->identity->id])->all();
	$connecteduser = array_merge($connectedchannel, $connectedstore);
	if (!empty($connecteduser)) {
	    $arr = array();
	    $namearr = array();

	    foreach ($connecteduser as $connectedstore) {
		if (!empty($connectedstore->channel_id)) {
		    $channelname = Channels::find()->where(['channel_ID' => $connectedstore->channel_id])->one();
		    $fieldname = 'channel_id';
		    if (($channelname->channel_name) == 'Facebook' || ($channelname->channel_name) == 'Google shopping') {
			continue;
		    } elseif (($channelname->channel_name) == 'Service Account' || ($channelname->channel_name) == 'Subscription Account') {
			$name = 'WeChat';
			$store_channel_id = $connectedstore->channel_id;
		    } else {
			$store_channel_id = $connectedstore->channel_id;
			$name = $channelname->channel_name;
		    }
		} elseif (!empty($connectedstore->store_id)) {
		    $fieldname = 'store_id';
		    $store_channel_id = $connectedstore->store_id;
		    $storename = Stores::find()->where(['store_id' => $connectedstore->store_id])->one();
		    $name = $storename->store_name;
		}
		/* for today */
		if (!empty($fieldname) && !empty($store_channel_id) && !empty($name) && $post == 'piecharttoday' || $post == 'piecharttodaymob') {
		    $currentdate = date('Y-m-d');
		    $connection = \Yii::$app->db;
		    $orders_data = $connection->createCommand('SELECT * from product_channel Where ' . $fieldname . '="' . $store_channel_id . '"AND date(created_at) = "' . $currentdate . '"');
		    $model = count($orders_data->queryAll());
		    $arr[] = array($model);
		    $namearr[] = $name;
		}
		/* for week */
		if (!empty($fieldname) && !empty($store_channel_id) && !empty($name) && $post == 'piechartweek' || $post == 'piechartweekmob') {
		    $currentmonth = date('m');
		    $date_check = date('m');
		    for ($i = 1; $i <= 7; $i++) {

			if ($date_check == $currentmonth) {
			    $previous_day = date('Y-m-d', strtotime('-' . $i . 'days'));
			    $date_check = date('m', strtotime($previous_day));
			}
		    }

		    $week_previous_day = date($previous_day, strtotime('+1  days'));
		    $Week_previous_date = date('Y-m-d', strtotime('-7  days'));
		    $currentdate = date('Y-m-d');
		    // $date_check = date('Y-m-d', strtotime($previous_hour));                   
		    $connection = \Yii::$app->db;
		    $orders_data = $connection->createCommand('SELECT * from product_channel Where ' . $fieldname . '="' . $store_channel_id . '"AND date(created_at) BETWEEN "' . $Week_previous_date . '" AND "' . $currentdate . '"');
		    $model = count($orders_data->queryAll());
		    $arr[] = array($model);
		    $namearr[] = $name;
		}

		/* for month */
		if (!empty($fieldname) && !empty($store_channel_id) && !empty($name) && $post == 'piechartmonth' || $post == 'piechartmonthmob') {
		    //$Month_previous_date = date('Y-m-d', strtotime('-30  days'));
		    $currentdate = date('Y-m-d');
		    $currentyear = date('Y');
		    $currentmonth = date('m');
		    $connection = \Yii::$app->db;
		    $orders_data = $connection->createCommand('SELECT * from product_channel Where ' . $fieldname . '="' . $store_channel_id . '"AND month(created_at)= "' . $currentmonth . '" AND year(created_at)="' . $currentyear . '"');
		    $model = count($orders_data->queryAll());
		    //$model = count(ProductChannel::find()->where([$fieldname => $store_channel_id])->all());
		    $arr[] = array($model);
		    $namearr[] = $name;
		}
		/* for quarter */
		if (!empty($fieldname) && !empty($store_channel_id) && !empty($name) && $post == 'piechartQuarter' || $post == 'piechartQuartermob') {
		    $currentmonth = date('m');
		    $currentyear = date('Y');
		    $val = $currentmonth - 3;
		    $currentmonth_pre = date("m", mktime(0, 0, 0, $val, 10));
		    $Month_previous_date = date('Y-m-d', strtotime('-30  days'));
		    $currentdate = date('Y-m-d');
		    $connection = \Yii::$app->db;
		    $orders_data = $connection->createCommand('SELECT * from product_channel Where ' . $fieldname . '="' . $store_channel_id . '"AND month(created_at) BETWEEN "' . $currentmonth_pre . '" AND "' . $currentmonth . '" AND year(created_at)="' . $currentyear . '"');
		    $model = count($orders_data->queryAll());
		    //$model = count(ProductChannel::find()->where([$fieldname => $store_channel_id])->all());
		    $arr[] = array($model);
		    $namearr[] = $name;
		}
		/* for year */
		if (!empty($fieldname) && !empty($store_channel_id) && !empty($name) && $post == 'piechartyear' || $post == 'piechartyearmob') {
		    $currentyear = date('Y');
		    $connection = \Yii::$app->db;
		    $orders_data = $connection->createCommand('SELECT * from product_channel Where ' . $fieldname . '="' . $store_channel_id . '"AND year(created_at)= "' . $currentyear . '"');
		    $model = count($orders_data->queryAll());
		    // $model = count(ProductChannel::find()->where([$fieldname => $store_channel_id])->all());
		    $arr[] = array($model);
		    $namearr[] = $name;
		}
	    }
	    $namearr2 = $namearr;
	}

	$colorarr = array(
	    0 => '#0091ea',
	    1 => '#00b0ff',
	    2 => '#40c4ff',
	    3 => '#80d8ff',
	    4 => '#01579b',
	    5 => '#0277bd',
	    6 => '#039be5',
	    7 => '#03a9f4',
	    8 => '#b3e5fc',
	    9 => '#81d4fa',
	    10 => '#29b6f6',
	    11 => '#e1f5fe',
	    12 => '#b5af79',
	    13 => '#914f84',
	    14 => '#ce6d87',
	    15 => '#e04a72',
	    16 => '#0000ff',
	    17 => '#00bfff',
	    18 => '#0000e5',
	    19 => '#19c5ff',
	    20 => '#0000cc',
	    21 => '#32cbff',
	    22 => '#0000b2',
	    23 => '#4cd2ff',
	    24 => '#0086b3',
	    25 => '#0099cc',
	    26 => '#00ace6',
	    27 => '#00bfff',
	    28 => '#4dd2ff',
	    29 => '#33ccff',
	    30 => '#1ac5ff',
	    31 => '#bc8f8f',
	    32 => '#cd5c5c',
	    33 => '#8b4513',
	    34 => '#a0522d',
	    35 => '#cd853f',
	    36 => '#deb887',
	    37 => '#f5f5dc',
	    38 => '#d2b48c',
	    39 => '#e9967a',
	    40 => '#fa8072',
	    41 => '#ffa07a',
	);
	$randcolor = array();
	$randcolor_2 = array();
	$c = 0;
	foreach ($arr as $array1) {
	    $randcolor[] = $colorarr[$c++];
	}
	$randcolor_2 = $randcolor;
	$data = array('label' => json_encode($namearr), 'color' => json_encode($randcolor_2), 'data' => json_encode($arr));
	return \yii\helpers\Json::encode($data);
    }

    public function actionInactiveProducts() {
	return $this->render('inactiveproducts');
    }

    public function actionSmartingTranslationStatusEnable() {
	$post_data = Yii::$app->request->post();
	$data='';
	if (!empty($post_data)) {
	    $channel_acc = $post_data['channel_acc'];
	    $product_id = $post_data['product_id'];
	    $connection_id = $post_data['connection_id'];

	    $store_check = Stores::find()->Where(['store_name' => $channel_acc])->one();
	    if (!empty($store_check)) {

		$product_abbrivation_data = ProductAbbrivation::find()->Where(['product_id' => $product_id, 'mul_store_id' => $connection_id, 'channel_accquired' => $channel_acc])->one();
		$product_abbrivation_data->translation_status = 'yes';
		if ($product_abbrivation_data->save(false)) {
		    $data = "success";
		} else {
		    $data = "error";
		}
	    } else {
		$product_abbrivation_data = ProductAbbrivation::find()->Where(['channel_accquired' => $channel_acc])->one();
		$product_abbrivation_data->translation_status='yes';
		if ($product_abbrivation_data->save(false)) {
		    $data = "success";
		} else {
		    $data = "error";
		}
	    }
	}
	return $data;
    }
    
    public function actionSmartingTranslationStatusDisable() {
	$post_data = Yii::$app->request->post();
	$data='';
	if (!empty($post_data)) {
	    $channel_acc = $post_data['channel_acc'];
	    $product_id = $post_data['product_id'];
	    $connection_id = $post_data['connection_id'];

	    $store_check = Stores::find()->Where(['store_name' => $channel_acc])->one();
	    if (!empty($store_check)) {

		$product_abbrivation_data = ProductAbbrivation::find()->Where(['product_id' => $product_id, 'mul_store_id' => $connection_id, 'channel_accquired' => $channel_acc])->one();
		$product_abbrivation_data->translation_status = 'no';
		if ($product_abbrivation_data->save(false)) {
		    $data = "success";
		} else {
		    $data = "error";
		}
	    } else {
		$product_abbrivation_data = ProductAbbrivation::find()->Where(['channel_accquired' => $channel_acc])->one();
		$product_abbrivation_data->translation_status='no';
		if ($product_abbrivation_data->save(false)) {
		    $data = "success";
		} else {
		    $data = "error";
		}
	    }
	}
	return $data;
    }

    public function getAllStoreName() {
	$stores_names = Stores::find()->select('store_name')->asArray()->all();
	$store_array = array();
	foreach ($stores_names as $_store) {
	    $store_array[] = $_store['store_name'];
	}
	return $store_array;
    }

    public function rainbow($start, $end, $steps) {
	$s = $this->str_to_rgb($start);
	$e = $this->str_to_rgb($end);
	$out = array();
	$r = (integer) ($e['r'] - $s['r']) / $steps;
	$g = (integer) ($e['g'] - $s['g']) / $steps;
	$b = (integer) ($e['b'] - $s['b']) / $steps;
	for ($x = 0; $x < $steps; $x++) {
	    $out[] = '#' . rgb_to_str(
			    $s['r'] + (integer) ($r * $x), $s['g'] + (integer) ($g * $x), $s['b'] + (integer) ($b * $x));
	}
//            echo'<pre>';
//            print_r($out);
	return $out;
    }

    public function rgb_to_str($r, $g, $b) {
	return str_pad($r, 2, '0', STR_PAD_LEFT)
		. str_pad($g, 2, '0', STR_PAD_LEFT)
		. str_pad($b, 2, '0', STR_PAD_LEFT);
    }

    public function str_to_rgb($str) {
	return array(
	    'r' => hexdec(substr($str, 0, 2)),
	    'g' => hexdec(substr($str, 3, 2)),
	    'b' => hexdec(substr($str, 5, 2))
	);
    }

}
