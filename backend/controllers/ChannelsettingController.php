<?php

namespace backend\controllers;

use backend\models\ChannelConnection;
use backend\models\Channels;
use backend\models\User;
use backend\models\StoresConnection;
use Bigcommerce\Api\Connection;
use backend\models\ProductImages;
use backend\models\SmartlingPrice;
use backend\controllers\ChannelsController;
use backend\models\Categories;
use backend\models\Products;
use backend\models\Variations;
use backend\models\ProductAbbrivation;
use backend\models\VariationsItemList;
use backend\models\OrdersProducts;
use backend\models\OrderChannel;
use backend\models\CustomerUser;
use backend\models\Notification;
use backend\models\ProductCategories;
use backend\models\ProductVariation;
use backend\models\ProductChannel;
use backend\models\Orders;
use backend\models\Smartling;
use backend\models\MerchantProducts;
use backend\models\Channelsetting;
use backend\models\GoogleProductCategories;
use backend\models\Stores;
use backend\models\OrderFullfillment;
use yii\filters\AccessControl;
use Smartling\AuthApi;
use Bigcommerce\Api\ShopifyClient as Shopify;
use yii\filters\VerbFilter;
use Yii;
use Smartling\Exceptions\SmartlingApiException;
use Smartling\File\FileApi;
use Smartling\Jobs\JobsApi;
use Smartling\Jobs\Params\AddFileToJobParameters;
use Smartling\Jobs\Params\AddLocaleToJobParameters;
use Smartling\Jobs\Params\CancelJobParameters;
use backend\models\StoreDetails;
use backend\models\CategoryAbbrivation;
use backend\models\CustomerAbbrivation;

class ChannelsettingController extends \yii\web\Controller {

    public function behaviors() {
	return ['access' => [
		'class' => AccessControl::className(),
		'only' => ['index', 'tracking', 'save', 'sforders', 'channeldisable', 'translation', 'callback'],
		'rules' => [
			[
			'actions' => ['signup', 'callback'],
			'allow' => true,
			'roles' => ['?'],
		    ],
			[
			'actions' => ['index', 'tracking', 'save', 'sforders', 'channeldisable', 'translation', 'callback'],
			'allow' => true,
			'roles' => ['@'],
		    ],
		],
	    ],
	    'verbs' => [
		'class' => VerbFilter::className(),
		'actions' => [
		    'delete' => ['POST'],
		],
	    ],
	];
    }

    public function actionIndex() {
	return $this->render('index');
    }

    public function actionTracking() {



	$hawbs = base64_decode($_GET['hawbs']);
	if (!empty($hawbs)) {
	    $KEYJsonArrayData = array('UserName' => '90000001', 'Password' => 'b9rVZekQqY2bv6ds',);
	    $order_data_json = array(
		'Routes' => array(array('SfWaybillNo' => '070033836247', 'TrackingType' => '1',),),
		'NetworkCredential' => $KEYJsonArrayData,
	    );

	    $data_string = json_encode($order_data_json);
	    // echo  $data_string; die('dsfasd');
	    $curl = curl_init();

	    curl_setopt_array($curl, array(
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_URL => "https://sit.api.sf-express-us.com/api/routeservice/query",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_SSL_VERIFYHOST => FALSE,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => $data_string,
		CURLOPT_HTTPHEADER => array(
		    "cache-control: no-cache",
		    "charset: utf-8",
		    "content-type: application/json"
		),
	    ));

	    $result = curl_exec($curl);
	    $err = curl_error($curl);
	    echo '<pre>';
	    print_r(json_decode($result));
	    echo '</pre>';
	    die('dsfa');
	    return $this->render('track');
	} else {
	    return $this->render('index');
	}
    }

    public function actionSave() {
	$control = $_POST['control'];
	$channelID = $_POST['channelID'];
	$fulfillmentName = $_POST['fulfillmentName'];
	$fulfillmentID = $_POST['fulfillmentID'];
	$users_Id = Yii::$app->user->identity->id;

	$store_connection = Stores::find()->where(['store_id' => $channelID])->one();
	if (!empty($store_connection)) {
	    $name = $store_connection->store_name;
	} else {
	    $Channels = Channels::find()->where(['channel_id' => $channelID])->one();
	    $name = $Channels->channel_name;
	}
	if ($name == 'Service Account') {
	    $name = 'WeChat';
	}

	if ($control == 'no') {
	    $Channelsetting_delete = Channelsetting::find()->Where(['user_id' => $users_Id, 'channel_id' => $channelID, 'setting_key' => 'fulfillmentID'])->all();

	    if (!empty($Channelsetting_delete)) {
		foreach ($Channelsetting_delete as $Cs_delete) {
		    $Cs_delete->delete();
		    //echo '<pre>'; print_r($Channelsetting_delete); echo '</pre>'; 
		}
	    }
	} else {


	    $Channelsetting = new Channelsetting();
	    $Channelsetting->channel_id = $channelID;
	    $Channelsetting->channel_name = $name;
	    $Channelsetting->setting_key = 'fulfillmentID';
	    $Channelsetting->user_id = $users_Id;
	    $Channelsetting->setting_value = $fulfillmentID;
	    $Channelsetting->created_at = date('Y-m-d H:i:s');
	    if ($Channelsetting->save(false)) {
		echo 'success';
	    } else {
		echo 'error';
	    }
	    //$this->getSforders($channelID);
	}

	//echo $control.'---'.$channelID.'-----'.$fulfillmentName.'----'.$fulfillmentID;
    }

    public function actionSforders() {
	$storeid = $_POST['storeid'];
	$fulfillmentID = $_POST['fulfillmentID'];
	$order = Orders::find()->all();
	// $sf_orders_data = OrderChannel::find()->Where(['store_id' => $storeid])->all();
	//echo '<pre>'; print_r($order); echo '</pre>'; die('sdfa');

	/* $store_connection_data = StoresConnection::find()->Where(['store_id' => $storeid])->one();
	  $iduse = $store_connection_data->stores_connection_id; */
	$sf_orders_data = OrderChannel::find()->where(['store_id' => $storeid])->all();
	if (!empty($sf_orders_data)) {

	    $sf_orders_data = OrderChannel::find()->where(['store_id' => $iduse])->all();
	} else {
	    $sf_orders_data = OrderChannel::find()->where(['channel_id' => $storeid])->all();
	}

	//echo '<pre>'; print_r($sf_orders_data);  die;
	$orderJsonArrayData = array();
	//$KEYJsonArrayData = array();
	$url = 'https://sit.api.sf-express-us.com/api/orderservice/submitorder';
	foreach ($sf_orders_data as $sf):
	    $order_id = $sf['order_id'];
	    $sf_orders_data_all = Orders::find()->Where(['order_ID' => $order_id])->with('customer')->with('ordersProducts')->one();
	    $userData = Yii::$app->user->identity;
	    $CneeContactName = $userData->first_name;
	    $CneeCompany = $userData->general_company;
	    $CneeAddress = $userData->corporate_add_street1;
	    $CneeCity = $userData->corporate_add_city;
	    $CneeProvince = $userData->billing_address_state;
	    $CneeCountry = $userData->corporate_add_country;
	    $CneePostCode = $userData->corporate_add_zipcode;
	    $CneePhone = $userData->general_phone_number;
	    // echo '<pre>'; print_r($sf_orders_data_all); echo '</pre>'; die('sdfa');
	    $product_data = $sf_orders_data_all->ordersProducts;
	    $customer_data = $sf_orders_data_all->customer;
	    $orderIdForSF = $sf_orders_data_all->order_ID . '-' . $sf_orders_data_all->customer_id;
	    $orderJsonArrayData['CneeContactName'] = $CneeContactName;
	    $orderJsonArrayData['CneeCompany'] = $CneeCompany;
	    $orderJsonArrayData['CneeAddress'] = $CneeAddress;
	    $orderJsonArrayData['CneeCity'] = $CneeCity;
	    // $orderJsonArrayData['CneeProvince'] = $customer_data->state;
	    $orderJsonArrayData['CneeProvince'] = $CneeProvince;
	    //$orderJsonArrayData['CneeCountry'] = $customer_data->country;
	    $orderJsonArrayData['CneeCountry'] = $CneeCountry;
	    $orderJsonArrayData['CneePostCode'] = $CneePostCode;
	    $orderJsonArrayData['CneePhone'] = $CneePhone;
	    $orderJsonArrayData['ReferenceNo1'] = $orderIdForSF;
	    $orderJsonArrayData['ReferenceNo2'] = 'IZIEF4342324324D23434';
	    $orderJsonArrayData['ExpressType'] = '101';
	    $orderJsonArrayData['ParcelQuantity'] = '1';
	    $orderJsonArrayData['PayMethod'] = '3';
	    $orderJsonArrayData['TaxPayType'] = '2';
	    $orderJsonArrayData['Currency'] = 'USD';

	    $i = 0;
	    foreach ($product_data as $prodata):
		$id = $prodata->product_Id;
		$productData = Products::find()->Where(['id' => $id])->one();
		if (!empty($productData->product_name)) {
		    $orderJsonArrayData['Items'][$i]['Name'] = $productData->product_name;
		} else {
		    $orderJsonArrayData['Items'][$i]['Name'] = 'Testing';
		}
		if (!empty($productData->stock_quantity)) {
		    $orderJsonArrayData['Items'][$i]['Count'] = str_replace(',', '', number_format($productData->stock_quantity, 0));
		} else {
		    $orderJsonArrayData['Items'][$i]['Count'] = '1';
		}
		$orderJsonArrayData['Items'][$i]['Unit'] = 'pcs';
		if (!empty($productData->price)) {
		    $orderJsonArrayData['Items'][$i]['Amount'] = $productData->price;
		} else {
		    $orderJsonArrayData['Items'][$i]['Amount'] = '0';
		}
		$orderJsonArrayData['Items'][$i]['SourceArea'] = 'US';
		if (!empty($productData->price)) {
		    $orderJsonArrayData['Items'][$i]['Weight'] = $productData->weight;
		} else {
		    $orderJsonArrayData['Items'][$i]['Weight'] = '1.0';
		}

		$i++;
	    endforeach;
	    //$KEYJsonArrayData['Gateway'] = 'JFK';
	    //$KEYJsonArrayData = array ( 'UserName' => 'info@helloiamelliot.com','Password' => 'sfexpress0035', );
	    $KEYJsonArrayData = array('UserName' => '90000006', 'Password' => '=!p96PytJkBxJQUg',);
	    //echo '<pre>'; print_r($orderJsonArrayData); echo '</pre>'; die('sdfa');
	    //endforeach;
	    $order_data_json = array(
		'Order' => $orderJsonArrayData,
		'Gateway' => 'JFK',
		'NetworkCredential' => $KEYJsonArrayData,
	    );

	    $data_string = json_encode($order_data_json);
	    echo $data_string;
	    $curl = curl_init();

	    curl_setopt_array($curl, array(
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_URL => "https://sit.api.sf-express-us.com/api/orderservice/submitorder",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 100,
		CURLOPT_TIMEOUT => 900,
		CURLOPT_SSL_VERIFYHOST => FALSE,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => $data_string,
		CURLOPT_HTTPHEADER => array(
		    "cache-control: no-cache",
		    "charset: utf-8",
		    "content-type: application/json"
		),
	    ));

	    $result = curl_exec($curl);
	    $err = curl_error($curl);
	    //echo '<pre>'; print_r($ch); echo '</pre>';
	    echo '<pre>';
	    print_r($err);
	    echo '</pre>';
	    echo '<pre>';
	    print_r(json_decode($result));
	    echo '</pre>';
	    die('dsfa');
	    $id = Yii::$app->user->identity->id;
	    $orderData = json_decode($result);
	    $key_data = $orderData->Success;
	    $value_data = $orderData;
	    $OrderFullfillmentAlready = OrderFullfillment::find()->Where(['order_id' => $order_id])->one();
	    if (empty($OrderFullfillmentAlready)) {
		$OrderFullfillment = new OrderFullfillment();
		$OrderFullfillment->elliot_user_id = $id;
		$OrderFullfillment->order_id = $order_id;
		$OrderFullfillment->name = 'SF Express';
		$OrderFullfillment->fulfillment_id = $fulfillmentID;
		$OrderFullfillment->key_data = $key_data;
		$OrderFullfillment->value_data = $result;
		$OrderFullfillment->created_at = date('Y-m-d H:i:s');
		$OrderFullfillment->save(false);
	    }
	    //  echo '##########################################################################';
	    // die('sdfa'); 
	endforeach;




	// return $this->render('sforders');
    }

    public function actionChanneldisable() {
	$dt = Yii::$app->request->post();
	//echo "<pre>"; print_r($dt); die;
	$_SESSION['access_token'] = '';
	if (isset($dt['ch_id'])) {
	    if ($dt['ch_type'] == "store") {
		$store_big = StoresConnection::find()->where(['stores_connection_id' => $dt['ch_id']])->one();
		$store_details = StoreDetails::find()->where(['store_connection_id' => $dt['ch_id']])->one();
		$store_name = $store_details->channel_accquired;
		$store_connection_id = $dt['ch_id'];
		$this->deleteAllData($store_name, $store_connection_id);
		$this->deleteShopifyHooks($store_big, $store_connection_id);
		$this->deleteMagentoUrl($store_big);
		$store_details->delete();
		$store_big->delete();
		return;
	    } elseif ($dt['ch_type'] == "channel") {
		$channel_connection_details = ChannelConnection::find()->where(['channel_connection_id' => $dt['ch_id']])->one();
		$channel_details_delete = StoreDetails::find()->where(['channel_connection_id' => $dt['ch_id']])->one();
		if (!empty($channel_details_delete)) {
		    $channel_details_delete->delete();
		}

		$channel_id = $channel_connection_details->channel_id;
		$user_id = $channel_connection_details->user_id;
		$channel_details = Channels::find()->where(['channel_ID' => $channel_id])->one();
		$channel_name = $channel_details->channel_name;
		if ($channel_name == 'Service Account' || $channel_name == 'Subscription Account') {
		    $channel_name = 'WeChat';
		}
		$this->deleteAllDataChannel(trim($channel_name), $channel_id);
		$channel_connection_details->delete();
		if ($channel_name == 'Google shopping') {
		    $feed = 'no';
		    $user_data = User::find()->Where(['id' => $user_id])->one();
		    $user_data->google_feed = $feed;
		    if ($user_data->save(false)) {
			$config = yii\helpers\ArrayHelper::merge(
					require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
			);
			$application = new yii\web\Application($config);

			$user_data1 = User::find()->Where(['id' => $user_id])->one();
			$user_data1->google_feed = $feed;
			$user_data1->save(false);
		    }
		}
		return;
	    }
	}
//        $all_conntection = StoresConnection::find()->all();
//        $all_channels = ChannelConnection::find()->all();
//
//        $cnt = 0;
//        $arr1 = array();
//        foreach ($all_conntection as $conn) {
//            $arr1[$cnt]['connection_id'] = $conn->stores_connection_id;
//            $store = Stores::find()->select(['store_name', 'store_image'])->where(['store_id' => $conn->store_id])->one();
//
//            $arr1[$cnt]['store_name'] = $store->store_name;
//            $arr1[$cnt]['store_image'] = $store->store_image;
//        }
//
//        $cnt1 = 0;
//        $arr2 = array();
//        foreach ($all_channels as $channel) {
//            $arr2[$cnt]['connection_id'] = $channel->channel_connection_id;
//            $chnl = Channels::find()->select(['channel_name', 'channel_image', 'parent_name'])->where(['channel_id' => $channel->channel_id])->one();
//
//            $arr2[$cnt]['channel_name'] = $chnl->channel_name;
//            $arr2[$cnt]['channel_image'] = $chnl->channel_image;
//            $arr2[$cnt]['parent_name'] = $chnl->parent_name;
//        }
//
//        $arr = array("store" => $arr1, "channel" => $arr2);
//        return $this->render('channeldisable', [
//                    'connections' => $arr,
//        ]);
    }

    public function deleteShopifyHooks($store_big, $store_connection_id) {
	$user_id = $store_big->user_id;
	$store_id = $store_big->store_id;
	$get_shopify_id = Stores::find()->where(['store_id' => $store_id])->one();
	if (!empty($get_shopify_id)) {
	    $store_name = $get_shopify_id->store_name;
	    if ($store_name == 'Shopify' || $store_name == 'ShopifyPlus') {
		$shopify_shop = $store_big->url;
		$shopify_api_key = $store_big->api_key;
		$shopify_api_password = $store_big->key_password;
		$shopify_shared_secret = $store_big->shared_secret;
		$user_id = $store_big->user_id;
		$sc = new Shopify($shopify_shop, $shopify_api_password, $shopify_api_key, $shopify_shared_secret);
		$hooks = $sc->call('GET', '/admin/webhooks.json');
		foreach ($hooks as $_hook) {
		    if (in_array(Yii::$app->params['BASE_URL'] . "people/shopify-product-create?id=" . $user_id . "&connection_id=" . $store_connection_id . "&store_name=" . $store_name, $_hook)) {
			$sc->call('DELETE', '/admin/webhooks/' . $_hook['id'] . '.json');
		    }
		    if (in_array(Yii::$app->params['BASE_URL'] . "people/shopify-product-delete?id=" . $user_id . "&connection_id=" . $store_connection_id . "&store_name=" . $store_name, $_hook)) {
			$sc->call('DELETE', '/admin/webhooks/' . $_hook['id'] . '.json');
		    }
		    if (in_array(Yii::$app->params['BASE_URL'] . "people/shopify-product-update?id=" . $user_id . "&connection_id=" . $store_connection_id . "&store_name=" . $store_name, $_hook)) {
			$sc->call('DELETE', '/admin/webhooks/' . $_hook['id'] . '.json');
		    }
		    if (in_array(Yii::$app->params['BASE_URL'] . "people/shopify-order-create?id=" . $user_id . "&connection_id=" . $store_connection_id . "&store_name=" . $store_name, $_hook)) {
			$sc->call('DELETE', '/admin/webhooks/' . $_hook['id'] . '.json');
		    }
		    if (in_array(Yii::$app->params['BASE_URL'] . "people/shopify-order-update?id=" . $user_id . "&connection_id=" . $store_connection_id . "&store_name=" . $store_name, $_hook)) {
			$sc->call('DELETE', '/admin/webhooks/' . $_hook['id'] . '.json');
		    }
		}
	    }
	}
    }

    public function deleteMagentoUrl($store_big) {
	$user_id = $store_big->user_id;
	$store_id = $store_big->store_id;
	$get_magento_id = Stores::find()->select('store_id')->where(['store_name' => 'Magento'])->one();
	$magento_store_id = $get_magento_id->store_id;
	if ($store_id == $magento_store_id) {
	    $config = yii\helpers\ArrayHelper::merge(
			    require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
	    );
	    $application = new yii\web\Application($config);
	    $main_data = User::find()->where(['id' => $user_id])->one();
	    $main_data->magento_shop_url = '';
	    $main_data->save(false);
	}
    }

    public function actionTranslation() {

	/* $upload_docs_dir = Yii::getAlias('@upload_files');
	  echo $upload_docs_dir;
	  die; */

	//require_once('/var/www/html/smartling-api/vendor/composer/autoload_real.php'); 
	//$user_id = Yii::$app->user->identity->id;
	$ds = DIRECTORY_SEPARATOR;
	$basedir = Yii::getAlias('@basedir');


	$translateFile = $_FILES['smart-2'];
	$translate_filename = $translateFile['name'];
	$upload_docs_dir = Yii::getAlias('@upload_files');



	/* Starts buisness_file Document Upload */
	$buisness_tempFile = $translateFile['tmp_name'];
	$buisness_file_name = $translateFile['name'];
	$buisness_final_name = uniqid() . '_' . $buisness_file_name;
	$buisness_targetFile = $upload_docs_dir . $ds . $buisness_final_name;
	$buisness_file_upload = move_uploaded_file($buisness_tempFile, $buisness_targetFile);
	$filePath = $upload_docs_dir . '/' . $buisness_final_name;




	$options = [
	    'project-id' => '7289bfb87',
	    'user-id' => 'bhabaskqrnadgqgarxhuhawydiicml',
	    'secret-key' => '2anhc34el124ngi7pd46epc21kVM_ui3vgsmu1usvlshgfnva3viu6j',
	];


	if (
		!array_key_exists('project-id', $options) || !array_key_exists('user-id', $options) || !array_key_exists('secret-key', $options)
	) {
	    echo 'Missing required params.' . PHP_EOL;
	    exit;
	}

	$autoloader = $basedir . '/smartling-api/vendor/autoload.php';

	if (!file_exists($autoloader) || !is_readable($autoloader)) {
	    echo 'Error. Autoloader not found. Seems you didn\'t run:' . PHP_EOL . '    composer update' . PHP_EOL;
	    exit;
	} else {
	    require_once $basedir . '/smartling-api/vendor/autoload.php';
	}



	$projectId = $options['project-id'];
	$userIdentifier = $options['user-id'];
	$userSecretKey = $options['secret-key'];

	$fileName = $translate_filename;
	$fileUri = $filePath;
	$fileRealPath = realpath($fileUri);
	$fileType = 'xml';
	$newFileName = 'new_test_file.xml';
	$retrievalType = 'published';
	$content = file_get_contents(realpath($fileUri));
	$fileContentUri = $translate_filename;
	$translationState = 'PUBLISHED';
	$locale = 'zh-CN';
	$locales_array = [$locale];

	$this->resetFiles($userIdentifier, $userSecretKey, $projectId, [$fileName, $newFileName]);


	/**
	 * Upload file example
	 */
	try {
	    echo '::: File Upload Example :::' . PHP_EOL;

	    $authProvider = \Smartling\AuthApi\AuthTokenProvider::create($userIdentifier, $userSecretKey);

	    $fileApi = \Smartling\File\FileApi::create($authProvider, $projectId);
	    $uploadParams = new \Smartling\File\Params\UploadFileParameters();
	    $uploadParams->setCallbackUrl('https://s86.co/channelsetting/callback/');
	    $result = $fileApi->uploadFile($fileRealPath, $fileName, $fileType, $uploadParams);
	    echo 'File upload result:' . PHP_EOL;
	    echo var_export($result, true) . PHP_EOL . PHP_EOL;
	} catch (\Smartling\Exceptions\SmartlingApiException $e) {
	    $messageTemplate = 'Error happened while uploading file.' . PHP_EOL
		    . 'Response code: %s' . PHP_EOL
		    . 'Response message: %s' . PHP_EOL;

	    echo vsprintf(
		    $messageTemplate, [
		$e->getCode(),
		$e->getMessage(),
		    ]
	    );
	}


	/**
	 * Download file example
	 */
	try {
	    echo '::: File Download Example :::' . PHP_EOL;

	    $authProvider = \Smartling\AuthApi\AuthTokenProvider::create($userIdentifier, $userSecretKey);

	    $fileApi = \Smartling\File\FileApi::create($authProvider, $projectId);

	    $params = new \Smartling\File\Params\DownloadFileParameters();
	    $params->setRetrievalType($retrievalType);

	    $result = $fileApi->downloadFile('test.xml', 'zh-CN', $params);

	    echo 'File download result:' . PHP_EOL;
	    echo var_export($result, true) . PHP_EOL . PHP_EOL;
	} catch (\Smartling\Exceptions\SmartlingApiException $e) {
	    $messageTemplate = 'Error happened while downloading file.' . PHP_EOL
		    . 'Response code: %s' . PHP_EOL
		    . 'Response message: %s' . PHP_EOL;

	    echo vsprintf(
		    $messageTemplate, [
		$e->getCode(),
		$e->getMessage(),
		    ]
	    );
	}
	/**
	 * Getting file status example
	 */
	/*  try {
	  echo '::: Get File Status Example :::' . PHP_EOL;

	  $authProvider = \Smartling\AuthApi\AuthTokenProvider::create($userIdentifier, $userSecretKey);

	  $fileApi = \Smartling\File\FileApi::create($authProvider, $projectId);

	  $result = $fileApi->getStatus($fileName, $locale);

	  echo 'Get File Status result:' . PHP_EOL;
	  echo var_export($result, true) . PHP_EOL . PHP_EOL;
	  } catch (\Smartling\Exceptions\SmartlingApiException $e) {
	  $messageTemplate = 'Error happened while getting file status.' . PHP_EOL
	  . 'Response code: %s' . PHP_EOL
	  . 'Response message: %s' . PHP_EOL;

	  echo vsprintf(
	  $messageTemplate, [
	  $e->getCode(),
	  $e->getMessage(),
	  ]
	  );
	  } */

	/**
	 * Getting Authorized locales for file
	 */
	/* try {
	  echo '::: Get File Authorized Locales Example :::' . PHP_EOL;

	  $authProvider = \Smartling\AuthApi\AuthTokenProvider::create($userIdentifier, $userSecretKey);

	  $fileApi = \Smartling\File\FileApi::create($authProvider, $projectId);

	  $result = $fileApi->getAuthorizedLocales($fileName);

	  echo 'Get File Authorized Locales result:' . PHP_EOL;
	  echo var_export($result, true) . PHP_EOL . PHP_EOL;
	  } catch (\Smartling\Exceptions\SmartlingApiException $e) {
	  $messageTemplate = 'Error happened while getting file authorized locales.' . PHP_EOL
	  . 'Response code: %s' . PHP_EOL
	  . 'Response message: %s' . PHP_EOL;

	  echo vsprintf(
	  $messageTemplate, [
	  $e->getCode(),
	  $e->getMessage(),
	  ]
	  );
	  } */

	/**
	 * Listing Files
	 */
	/*  try {
	  echo '::: List Files Example :::' . PHP_EOL;

	  $authProvider = \Smartling\AuthApi\AuthTokenProvider::create($userIdentifier, $userSecretKey);

	  $fileApi = \Smartling\File\FileApi::create($authProvider, $projectId);

	  $params = new \Smartling\File\Params\ListFilesParameters();
	  $params
	  ->setFileTypes($fileType)
	  ->setUriMask('test')
	  ->setLimit(5);

	  $result = $fileApi->getList($params);

	  echo 'List Files result:' . PHP_EOL;
	  echo var_export($result, true) . PHP_EOL . PHP_EOL;
	  } catch (\Smartling\Exceptions\SmartlingApiException $e) {
	  $messageTemplate = 'Error happened while getting file list.' . PHP_EOL
	  . 'Response code: %s' . PHP_EOL
	  . 'Response message: %s' . PHP_EOL;

	  echo vsprintf(
	  $messageTemplate, [
	  $e->getCode(),
	  $e->getMessage(),
	  ]
	  );
	  } */

	/**
	 * Importing Files
	 */
	/*  try {
	  echo '::: File Import Example :::' . PHP_EOL;

	  $authProvider = \Smartling\AuthApi\AuthTokenProvider::create($userIdentifier, $userSecretKey);

	  $fileApi = \Smartling\File\FileApi::create($authProvider, $projectId);

	  $result = $fileApi->import($locale, $fileName, $fileType, $fileRealPath, $translationState, true);

	  echo 'File Import result:' . PHP_EOL;
	  echo var_export($result, true) . PHP_EOL . PHP_EOL;
	  } catch (\Smartling\Exceptions\SmartlingApiException $e) {
	  $messageTemplate = 'Error happened while importing file.' . PHP_EOL
	  . 'Response code: %s' . PHP_EOL
	  . 'Response message: %s' . PHP_EOL;

	  echo vsprintf(
	  $messageTemplate, [
	  $e->getCode(),
	  $e->getMessage(),
	  ]
	  );
	  } */

	/**
	 * Renaming Files
	 */
	/* try {
	  echo '::: Rename File Example :::' . PHP_EOL;

	  $authProvider = \Smartling\AuthApi\AuthTokenProvider::create($userIdentifier, $userSecretKey);

	  $fileApi = \Smartling\File\FileApi::create($authProvider, $projectId);

	  $result = $fileApi->renameFile($fileName, $newFileName);

	  echo 'Rename File result:' . PHP_EOL;
	  echo var_export($result, true) . PHP_EOL . PHP_EOL;
	  } catch (\Smartling\Exceptions\SmartlingApiException $e) {
	  $messageTemplate = 'Error happened while renaming files.' . PHP_EOL
	  . 'Response code: %s' . PHP_EOL
	  . 'Response message: %s' . PHP_EOL;

	  echo vsprintf(
	  $messageTemplate, [
	  $e->getCode(),
	  $e->getMessage(),
	  ]
	  );
	  } */
    }

    function resetFiles($userIdentifier, $userSecretKey, $projectId, $files = []) {
	$authProvider = \Smartling\AuthApi\AuthTokenProvider::create($userIdentifier, $userSecretKey);

	foreach ($files as $file) {
	    try {
		$fileApi = \Smartling\File\FileApi::create($authProvider, $projectId);
		$fileApi->deleteFile($file);
	    } catch (\Smartling\Exceptions\SmartlingApiException $e) {
		
	    }
	}
    }

    public function actionCallback() {

	$user = User::find()->where(['id' => 1453])->one();
	$user_domain = $user->domain_name;
	$config = yii\helpers\ArrayHelper::merge(
			require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $user_domain . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php'));
	$application = new yii\web\Application($config);


	$locale = $_GET['locale'];
	$fileUri = $_GET['fileUri'];
	echo $locale . '--' . $fileUri;
	$OrderFullfillmentAlready = OrderFullfillment::find()->Where(['id' => '529'])->one();

	$OrderFullfillmentAlready->data = $locale . '--' . $fileUri;
	if ($OrderFullfillmentAlready->save(false)) {
	    die('save');
	} else {
	    echo'<pre>';
	    print_r($OrderFullfillmentAlready->getErrors());
	}
	die('end here');
	// }
    }

    public function Workflowuid($projectId, $access_token) {
	$curl = curl_init();
	//$url = 'https://api.smartling.com/accounts-api/v2/accounts/'.$accountUid.'/projects';
	$url = ' https://api.smartling.com/workflows-api/v2/projects/' . $projectId . '/workflows';
	curl_setopt_array($curl, array(
	    CURLOPT_SSL_VERIFYPEER => false,
	    CURLOPT_URL => $url,
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_ENCODING => "",
	    CURLOPT_MAXREDIRS => 100,
	    CURLOPT_TIMEOUT => 900,
	    CURLOPT_SSL_VERIFYHOST => FALSE,
	    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	    CURLOPT_CUSTOMREQUEST => "GET",
	    /* CURLOPT_POSTFIELDS => $data_string, */
	    CURLOPT_HTTPHEADER => array(
		"cache-control: no-cache",
		"charset: utf-8",
		"Authorization: Bearer " . $access_token,
		"content-type: application/json"
	    ),
	));

	$result = curl_exec($curl);
	return $result;
    }

    public function actionTranslationchannelsettingdisable() {

	$domain_name = Yii::$app->user->identity->domain_name;
	$users_Id = Yii::$app->user->identity->id;
	$filenameAdd = $domain_name . '_' . $users_Id;
	//echo '<pre>'; print_r($_POST); echo '</per>'; die();
	$id = explode('-', $_POST['channel_id']);

	$channelID = $id[0];
	$enable = $_POST['enable'];
	$catenable = $_POST['smartlingyes'];
	$selectTranslation = $_POST['selectTranslation'];
	$Main_channel_id_translation = $_POST['Main_channel_id_translation'];
	$language = $_POST['language'];
	$users_Id = Yii::$app->user->identity->id;

	$store_connection_data = StoresConnection::find()->Where(['stores_connection_id' => $channelID])->with('storesDetails')->one();
	if (!empty($store_connection_data)) {
	    $iduse = $store_connection_data->store_id;

	    /* check vali Store */
	    $store_connection = Stores::find()->where(['store_id' => $iduse])->one();

	    //if (!empty($store_connection)) {
	    $country = @$store_connection_data->storesDetails->country;
	    $namestoreold = $store_connection->store_name;
	    $namestoreold = $store_connection->store_name . ' ' . $country;
	} else {
	    $Channels = Channels::find()->where(['channel_ID' => $channelID])->one();
	    $namestore = $Channels->parent_name;
	    $channel_name = $Channels->channel_name;
	    if ($namestore == 'channel_WeChat') {
		$namestoreold = 'WeChat';
	    }
	    if ($namestore == 'channel_Square') {
		$namestoreold = 'Square';
	    }
	    // $namestoreold = $Channels->channel_name;
	}


	$SmartlingProject = Smartling::find()->where(['elliot_user_id' => $users_Id, 'token' => $namestoreold])->one();
	$Enable_Smartling = Channelsetting::find()->where(['user_id' => $users_Id, 'channel_name' => $namestoreold, 'setting_key' => 'Enable_Smartling'])->one();
	if (!empty($Enable_Smartling)) {
	    $Enable_Smartling->setting_value = 'no';
	    $Enable_Smartling->save();
	}
	if (!empty($SmartlingProject)) {
	    $SmartlingProject->connected = 'no';
	    $SmartlingProject->save();
	}
	echo $namestoreold;
    }

    public function actionTranslationchannelcost() {
	$domain_name = Yii::$app->user->identity->domain_name;
	$users_Id = Yii::$app->user->identity->id;
	$filenameAdd = $domain_name . '_' . $users_Id;
	//echo '<pre>'; print_r($_POST); echo '</per>'; die();
	$id = explode('-', $_POST['channel_id']);

	$channelID = $id[0];
	$enable = $_POST['enable'];
	$catenable = $_POST['smartlingyes'];
	$selectTranslation = $_POST['selectTranslation'];
	$language = $_POST['language'];
	$users_Id = Yii::$app->user->identity->id;
	$arrayData = array($enable, $catenable);
	if ($selectTranslation == 'Google MT') {
	    $selectRate = 'rate2';
	    $SmartlingPrice = SmartlingPrice::find()->Where(['locale_id' => $language])->one();
	    $priceUnitCost = $SmartlingPrice->rate2;
	} else if ($selectTranslation == 'Translation with Edit') {
	    $selectRate = 'editing';
	    $SmartlingPrice = SmartlingPrice::find()->Where(['locale_id' => $language])->one();
	    $priceUnitCost = $SmartlingPrice->editing;
	} else {
	    $priceUnitCost = 0;
	}
	$priceUnitCost = str_replace('$', '', $priceUnitCost);
	$store_connection_data = StoresConnection::find()->Where(['stores_connection_id' => $channelID])->with('storesDetails')->one();

	if (!empty($store_connection_data)) {
	    $iduse = $store_connection_data->store_id;

	    /* check vali Store */
	    $store_connection = Stores::find()->where(['store_id' => $iduse])->one();

	    //if (!empty($store_connection)) {
	    $country = @$store_connection_data->storesDetails->country;
	    $namestoreold = $store_connection->store_name . ' ' . $country;
	    $namestore = $store_connection->store_name . ' ' . $country;
	} else {
	    $Channels = Channels::find()->where(['channel_ID' => $channelID])->one();
	    $namestore = $Channels->parent_name;
	    $channel_name = $Channels->channel_name;
	    if ($namestore == 'channel_WeChat') {
		$namestoreold = 'WeChat';
	    } else {
		$namestoreold = $Channels->channel_name;
	    }
	    //$namestoreold = $channel_name;
	}


	if ($id[1] == 'store') {
	    $store_connection = StoresConnection::find()->Where(['stores_connection_id' => $id[0]])->one();
	    if (!empty($store_connection)) {
		$store_connection->smartling_status = 'active';
		$store_connection->save(false);
	    }
	    $products_data = ProductAbbrivation::find()->Where(['mul_store_id' => $id[0]])->with('product')->all();

	    $arrayNew = array();
	    foreach ($products_data as $product) {
		$name = $product->product->product_name;
		$elliot_user_id = $product->product->elliot_user_id;
		$proid = $product->product->id;
		$description = $product->product->description;
		$brand = $product->product->brand;
		$array = array(
		    'id' => $proid,
		    'user_id' => $elliot_user_id,
		    'name' => $name,
		    'description' => $description,
		    'brand' => $brand
		);
		$arrayNew[] = $array;
	    }

	    $data = json_encode($arrayNew);
	    $data = strip_tags($data);
	    $wordcount = str_word_count($data);
	    $cost = $priceUnitCost * $wordcount;
	    $endData = '
			<h5>Estimate Word Count : ' . $wordcount . '</h5>
			<h5>Tentative Cost : $' . $cost . '</h5>
			<button type="button" data-dismiss="modal" class="smartling_enble_controller btn btn-space btn-default  btn-primary btn-lg">Authenticate - $1/-';
	} else {
	    $channel_connection = ChannelConnection::find()->Where(['channel_id' => $channelID])->one();
	    if (!empty($channel_connection)) {
		$channel_connection->smartling_status = 'active';
		$channel_connection->save(false);
	    }
	    $channel_name = trim($channel_name);
	    //echo $namestoreold; 
	    $products_data = ProductAbbrivation::find()->Where(['channel_accquired' => $namestoreold])->with('product')->all();

	    $arrayNew = array();
	    foreach ($products_data as $product) {
		$name = $product->product->product_name;
		$elliot_user_id = $product->product->elliot_user_id;
		$proid = $product->product->id;
		$description = $product->product->description;
		$brand = $product->product->brand;
		$array = array(
		    'id' => $proid,
		    'user_id' => $elliot_user_id,
		    'name' => $name,
		    'description' => $description,
		    'brand' => $brand
		);
		$arrayNew[] = $array;
	    }
	    /*  */
	    $data = json_encode($arrayNew);
	    $data = strip_tags($data);
	    $wordcount = str_word_count($data);
	    $cost = $priceUnitCost * $wordcount;
	    $endData = '
			<h5>Estimate Word Count : ' . $wordcount . '</h5>
			<h5>Tentative Cost : $' . $cost . '</h5>
			<button type="button" data-dismiss="modal" class="smartling_enble_controller btn btn-space btn-default  btn-primary btn-lg">Authenticate - $1/-';
	}

	return $endData;
    }

    public function actionTranslationchannelsetting() {

	//echo '<pre>'; print_r($_POST); echo '</pre>'; die();
	$credit = $_POST['credit'];
	$users_Id = Yii::$app->user->identity->id;
	/* if($credit == '0'){
	  $creditemail = $_POST['creditemail'];
	  $creditcart = $_POST['creditcart'];
	  $creditdate = $_POST['creditdate'];
	  $creditcvc = $_POST['creditcvc'];

	  $userData = User::find()->Where(['id' => $users_Id])->one();
	  $userData->credit_email = $creditemail;
	  $userData->credit_cardno = $creditcart;
	  $userData->credit_date = $creditdate;
	  $userData->credit_cvc = $creditcvc;
	  $userData->save(false);
	  } */
	$domain_name = Yii::$app->user->identity->domain_name;

	$filenameAdd = $domain_name . '_' . $users_Id;
	//echo '<pre>'; print_r($_POST); echo '</per>'; die();
	$id = explode('-', $_POST['channel_id']);

	$channelID = $id[0];
	$enable = $_POST['enable'];
	$Main_channel_id_translation = $_POST['Main_channel_id_translation'];
	$catenable = $_POST['smartlingyes'];
	$selectTranslation = $_POST['selectTranslation'];
	$language = $_POST['language'];

	$arrayData = array($enable, $catenable);
	$store_connection_data = StoresConnection::find()->Where(['stores_connection_id' => $channelID])->with('storesDetails')->one();
	if (!empty($store_connection_data)) {
	    $iduse = $store_connection_data->store_id;

	    /* check vali Store */
	    $store_connection = Stores::find()->where(['store_id' => $iduse])->one();

	    //if (!empty($store_connection)) {
	    $country = @$store_connection_data->storesDetails->country;
	    $namestoreold = $store_connection->store_name . ' ' . $country;
	    $namestore = $store_connection->store_name . ' ' . $country;
	} else {
	    $Channels = Channels::find()->where(['channel_ID' => $channelID])->one();
	    $namestore = $Channels->parent_name;
	    $channel_name = $Channels->channel_name;
	    if ($namestore == 'channel_WeChat') {
		$namestoreold = 'WeChat';
	    } else {
		$namestoreold = $Channels->channel_name;
	    }
	    $namestore = $Channels->channel_name;
	    //$namestoreold = $channel_name;
	}
	$namestoreold;
	if ($id[1] == 'store') {
	    $store_connection = StoresConnection::find()->Where(['stores_connection_id' => $id[0]])->one();
	    if (!empty($store_connection)) {
		$store_connection->smartling_status = 'active';
		$store_connection->save(false);
	    }
	    $products_data = ProductAbbrivation::find()->Where(['mul_store_id' => $id[0]])->with('product')->all();
	    /*  echo '<pre>';
	      print_r($products_data);
	      echo '<pre>'; */
	    $arrayNew = array();

	    if ($selectTranslation == 'Google MT') {
		$selectRate = 'rate2';
		$SmartlingPrice = SmartlingPrice::find()->Where(['locale_id' => $language])->one();
		$priceUnitCost = $SmartlingPrice->rate2;
	    } else if ($selectTranslation == 'Translation with Edit') {
		$selectRate = 'editing';
		$SmartlingPrice = SmartlingPrice::find()->Where(['locale_id' => $language])->one();
		$priceUnitCost = $SmartlingPrice->editing;
	    } else {
		$priceUnitCost = 0;
	    }
	    $priceUnitCost = str_replace('$', '', $priceUnitCost);
	    foreach ($products_data as $product) {
		$name = $product->product->product_name;
		$elliot_user_id = $product->product->elliot_user_id;
		$proid = $product->product->id;
		$description = $product->product->description;
		$brand = $product->product->brand;
		$array = array(
		    'id' => $proid,
		    'user_id' => $elliot_user_id,
		    'name' => $name,
		    'description' => $description,
		    'brand' => $brand
		);
		$arrayNew[] = $array;
	    }
	    $ds = DIRECTORY_SEPARATOR;
	    $basedir = Yii::getAlias('@basedir');
	    $path = $basedir . '/img/uploaded_files/smartling_files/' . $filenameAdd;
	    $filemmmmae = $namestore . '.json';
	    $pathFileName = $basedir . '/img/uploaded_files/smartling_files/' . $filenameAdd . '/' . $namestore . '.json';
	    if (!file_exists($path)) {
		mkdir($path, 0777, true);
	    }
	    $data = json_encode($arrayNew);
	    $data = strip_tags($data);
	    file_put_contents($pathFileName, $data);
	    /*  echo '<per>';
	      print_r(json_encode($arrayNew));
	      echo '</pre>'; */

	    if ($selectTranslation == 'Google MT') {
		$selectRate = 'rate2';
		$SmartlingPrice = SmartlingPrice::find()->Where(['locale_id' => $language])->one();
		$priceUnitCost = $SmartlingPrice->rate2;
	    } else if ($selectTranslation == 'Translation with Edit') {
		$selectRate = 'editing';
		$SmartlingPrice = SmartlingPrice::find()->Where(['locale_id' => $language])->one();
		$priceUnitCost = $SmartlingPrice->editing;
	    } else {
		$priceUnitCost = 0;
	    }


	    $wordcount = str_word_count($data);
	    $wordcount = $priceUnitCost * $wordcount;
	    //echo '<per>'; print_r($products_data); echo '</pre>'; die();
	} else {
	    $channel_connection = ChannelConnection::find()->Where(['channel_id' => $channelID])->one();
	    if (!empty($channel_connection)) {
		$channel_connection->smartling_status = 'active';
		$channel_connection->save(false);
	    }
	    $channel_name = trim($channel_name);
	    $products_data = ProductAbbrivation::find()->Where(['channel_accquired' => $namestoreold])->with('product')->all();

	    $arrayNew = array();
	    foreach ($products_data as $product) {
		$name = $product->product->product_name;
		$elliot_user_id = $product->product->elliot_user_id;
		$proid = $product->product->id;
		$description = $product->product->description;
		$brand = $product->product->brand;
		$array = array(
		    'id' => $proid,
		    'user_id' => $elliot_user_id,
		    'name' => $name,
		    'description' => $description,
		    'brand' => $brand
		);
		$arrayNew[] = $array;
	    }
	    $ds = DIRECTORY_SEPARATOR;
	    $basedir = Yii::getAlias('@basedir');
	    $path = $basedir . '/img/uploaded_files/smartling_files/' . $filenameAdd;
	    $filemmmmae = $namestore . '.json';
	    $pathFileName = $basedir . '/img/uploaded_files/smartling_files/' . $filenameAdd . '/' . $namestore . '.json';
	    if (!file_exists($path)) {
		mkdir($path, 0777, true);
	    }
	    $data = json_encode($arrayNew);
	    $data = strip_tags($data);
	    file_put_contents($pathFileName, $data);
	    /*  echo '<per>';
	      print_r(json_encode($arrayNew));
	      echo '</pre>'; */
	    if ($selectTranslation == 'Google MT') {
		$selectRate = 'rate2';
		$SmartlingPrice = SmartlingPrice::find()->Where(['locale_id' => $language])->one();
		$priceUnitCost = $SmartlingPrice->rate2;
	    } else if ($selectTranslation == 'Translation with Edit') {
		$selectRate = 'editing';
		$SmartlingPrice = SmartlingPrice::find()->Where(['locale_id' => $language])->one();
		$priceUnitCost = $SmartlingPrice->editing;
	    } else {
		$priceUnitCost = 0;
	    }


	    $wordcount = str_word_count($data);
	    $wordcount = $priceUnitCost * $wordcount;

	    //echo '<per>'; print_r($products_data); echo '</pre>'; die('sdfsdfa');
	}

	/* File Create End */
	$domain_name = Yii::$app->user->identity->domain_name;
	$account_name = $domain_name . '+' . $users_Id . '-Elliot-A';
	$SmartlingAccount = Smartling::find()->where(['elliot_user_id' => $users_Id, 'account_name' => $account_name])->one();

	if (empty($SmartlingAccount)) {

	    $Smartling = new Smartling();

	    $account_name = $domain_name . '+' . $users_Id . '-Elliot-A';
	    $data_string = '{
                                "accountName": "' . $account_name . '",
                                "productUid": "cbd55c56a878"
                            }';
	    $curl = curl_init();

	    curl_setopt_array($curl, array(
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_URL => "https://api.smartling.com/accounts-api/v2/affiliated-accounts",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 100,
		CURLOPT_TIMEOUT => 900,
		CURLOPT_SSL_VERIFYHOST => FALSE,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => $data_string,
		CURLOPT_HTTPHEADER => array(
		    "cache-control: no-cache",
		    "charset: utf-8",
		    "X-Auth-Token: BfcqrhanfJghxYZQe8PtDhUMuQHaPtqqRuQZtpZE&jkfnkCfeRBVXLsiBqFdXDaB",
		    "content-type: application/json"
		),
	    ));

	    $result = curl_exec($curl);
	    //echo '<pre>'; print_r(json_decode($result)); die();
	    $data = json_decode($result);
	    $accountUid = $data->response->data->accountUid;
	    $userIdentifier = $data->response->data->apiCredentials->userIdentifier;
	    $userSecret = $data->response->data->apiCredentials->userSecret;
	    $Smartling->elliot_user_id = $users_Id;
	    $Smartling->sm_user_id = $userIdentifier;
	    //$Smartling->token = $namestore;
	    $Smartling->account_id = $accountUid;
	    $Smartling->connected = 'yes';
	    $Smartling->account_name = $account_name;
	    $Smartling->secret_key = $userSecret;
	    $Smartling->save();

	    // $err = curl_error($curl);

	    /* StartProject Added */

	    $SmartlingProject = Smartling::find()->where(['elliot_user_id' => $users_Id])->andWhere(['<>', 'token', $namestore])->one();
	    if (empty($SmartlingProject)) {
		$Smartling = new Smartling();
		$Smartling->elliot_user_id = $users_Id;
		$Smartling->sm_user_id = $userIdentifier;
		$Smartling->token = $namestoreold;
		$Smartling->account_id = $accountUid;
		$Smartling->account_name = $account_name;
		$Smartling->secret_key = $userSecret;
		$accountUid = $accountUid;

		/* Account Athentication */
		$data_string = '{
						  "userIdentifier": "' . $userIdentifier . '",
						  "userSecret": "' . $userSecret . '"
						}';
		$curl = curl_init();
		$url = 'https://api.smartling.com/auth-api/v2/authenticate';
		curl_setopt_array($curl, array(
		    CURLOPT_SSL_VERIFYPEER => false,
		    CURLOPT_URL => $url,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_ENCODING => "",
		    CURLOPT_MAXREDIRS => 100,
		    CURLOPT_TIMEOUT => 900,
		    CURLOPT_SSL_VERIFYHOST => FALSE,
		    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		    CURLOPT_CUSTOMREQUEST => "POST",
		    CURLOPT_POSTFIELDS => $data_string,
		    CURLOPT_HTTPHEADER => array(
			"cache-control: no-cache",
			"charset: utf-8",
			"X-Auth-Token: BfcqrhanfJghxYZQe8PtDhUMuQHaPtqqRuQZtpZE&jkfnkCfeRBVXLsiBqFdXDaB",
			"content-type: application/json"
		    ),
		));

		$resultAuth = curl_exec($curl);

		//echo '<pre>'; print_r(json_decode($resultAuth)); die();
		$authData = json_decode($resultAuth);

		$access_token = $authData->response->data->accessToken;

		/* Account Athentication */





		$domain_name = Yii::$app->user->identity->domain_name;
		$Project_name = $domain_name . '+' . $users_Id . '-Elliot-P' . $namestore;
		$data_string = '{
                                    "projectName": "' . $Project_name . '",
                                    "projectTypeCode": "APPLICATION_RESOURCES",
                                    "sourceLocaleId": "en",
                                    "targetLocaleIds": ["' . $language . '"]
                                }';
		$curl = curl_init();
		$url = 'https://api.smartling.com/accounts-api/v2/accounts/' . $accountUid . '/projects';
		curl_setopt_array($curl, array(
		    CURLOPT_SSL_VERIFYPEER => false,
		    CURLOPT_URL => $url,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_ENCODING => "",
		    CURLOPT_MAXREDIRS => 100,
		    CURLOPT_TIMEOUT => 900,
		    CURLOPT_SSL_VERIFYHOST => FALSE,
		    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		    CURLOPT_CUSTOMREQUEST => "POST",
		    CURLOPT_POSTFIELDS => $data_string,
		    CURLOPT_HTTPHEADER => array(
			"cache-control: no-cache",
			"charset: utf-8",
			"Authorization: Bearer " . $access_token,
			"content-type: application/json"
		    ),
		));

		$result = curl_exec($curl);
		//echo '<pre>'; print_r(json_decode($result)); die();
		$data = json_decode($result);
		$projectId = $data->response->data->projectId;
		$projectTypeCode = $data->response->data->projectTypeCode;
		$projectTypeDisplayValue = $data->response->data->projectTypeDisplayValue;
		$targetLocales = $data->response->data->targetLocales;

		//$Smartling->elliot_user_id = $users_Id;
		$Smartling->project_name = $Project_name;
		$Smartling->project_id = $projectId;
		$Smartling->project_type = $projectTypeCode;
		$Smartling->project_type_value = $projectTypeDisplayValue;
		$Smartling->connected = 'yes';
		//$Smartling->target_locale = $targetLocales;
		/* 	$Smartling->secret_key = $userSecret; */
		//echo '<pre>'; print_r($Smartling); echo '</pre>';
		$Smartling->save();


		//$this->Workflowuid($projectId);
		$data = $this->Workflowuid($projectId, $access_token);
		/* echo '<pre>';
		  print_r($data);
		  echo '</pre>'; */
		$token = $namestore;

		$curl = curl_init();
		//$url = 'https://api.smartling.com/accounts-api/v2/accounts/'.$accountUid.'/projects';
		$url = 'https://api.smartling.com/workflows-api/v2/projects/' . $projectId . '/workflows';
		curl_setopt_array($curl, array(
		    CURLOPT_SSL_VERIFYPEER => false,
		    CURLOPT_URL => $url,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_ENCODING => "",
		    CURLOPT_MAXREDIRS => 100,
		    CURLOPT_TIMEOUT => 900,
		    CURLOPT_SSL_VERIFYHOST => FALSE,
		    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		    CURLOPT_CUSTOMREQUEST => "GET",
		    CURLOPT_HTTPHEADER => array(
			"cache-control: no-cache",
			"charset: utf-8",
			"Authorization: Bearer " . $access_token,
			"content-type: application/json"
		    ),
		));

		$result1 = curl_exec($curl);
		$data1 = json_decode($result1);
		$itemsData = $data1->response->data->items;
		if ($selectTranslation == 'Google MT') {
		    $workflowUid = $itemsData['1']->workflowUid;
		} else if ($selectTranslation == 'Translation with Edit') {
		    $workflowUid = $itemsData['2']->workflowUid;
		} else {
		    $workflowUid = $itemsData['3']->workflowUid;
		}








		$this->Jobcreate($projectId, $userIdentifier, $userSecret, $Project_name, $token, $filemmmmae, $pathFileName, $workflowUid, $access_token, $language);
		//echo $access_token; echo '</br>';
		/* $curl = curl_init();
		  //$url = 'https://api.smartling.com/accounts-api/v2/accounts/'.$accountUid.'/projects';
		  echo $url = ' https://api.smartling.com/workflows-api/v2/projects/'.$projectId.'/workflows';
		  curl_setopt_array($curl, array(
		  CURLOPT_SSL_VERIFYPEER => false,
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 100,
		  CURLOPT_TIMEOUT => 900,
		  CURLOPT_SSL_VERIFYHOST => FALSE,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",

		  CURLOPT_HTTPHEADER => array(
		  "cache-control: no-cache",
		  "charset: utf-8",
		  "Authorization: Bearer".$access_token,
		  "content-type: application/json"
		  ),
		  ));

		  echo $result = curl_exec($curl);
		  echo json_decode($result);

		  echo '<pre>'; print_r(json_decode($result)); die('df11111sas');
		 */
	    }


	    /* StartProject Added */
	} else {
	    if ($namestoreold == 'WeChat') {
		$namestore = 'WeChat';
	    }
	    //echo $namestore; 
	    $SmartlingProject = Smartling::find()->where(['elliot_user_id' => $users_Id, 'token' => $namestore])->one();
	    //echo '<pre>'; print_r($SmartlingProject); echo '</per>';
	    if (empty($SmartlingProject)) {
		//echo $namestore; die('sd');
		$Smartling = new Smartling();
		$Smartling->elliot_user_id = $users_Id;
		$Smartling->sm_user_id = $SmartlingAccount->sm_user_id;
		$Smartling->token = $namestoreold;
		$Smartling->account_id = $SmartlingAccount->account_id;
		$Smartling->account_name = $SmartlingAccount->account_name;
		$Smartling->secret_key = $SmartlingAccount->secret_key;
		$accountUid = $SmartlingAccount->account_id;

		/* Account Athentication */
		$data_string = '{
						  "userIdentifier": "' . $SmartlingAccount->sm_user_id . '",
						  "userSecret": "' . $SmartlingAccount->secret_key . '"
						}';
		$curl = curl_init();
		$url = 'https://api.smartling.com/auth-api/v2/authenticate';
		curl_setopt_array($curl, array(
		    CURLOPT_SSL_VERIFYPEER => false,
		    CURLOPT_URL => $url,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_ENCODING => "",
		    CURLOPT_MAXREDIRS => 100,
		    CURLOPT_TIMEOUT => 900,
		    CURLOPT_SSL_VERIFYHOST => FALSE,
		    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		    CURLOPT_CUSTOMREQUEST => "POST",
		    CURLOPT_POSTFIELDS => $data_string,
		    CURLOPT_HTTPHEADER => array(
			"cache-control: no-cache",
			"charset: utf-8",
			"X-Auth-Token: BfcqrhanfJghxYZQe8PtDhUMuQHaPtqqRuQZtpZE&jkfnkCfeRBVXLsiBqFdXDaB",
			"content-type: application/json"
		    ),
		));

		$resultAuth = curl_exec($curl);

		//echo '<pre>'; print_r(json_decode($resultAuth)); die();
		$authData = json_decode($resultAuth);

		$access_token = $authData->response->data->accessToken;

		/* Account Athentication */





		$domain_name = Yii::$app->user->identity->domain_name;
		$Project_name = $domain_name . '+' . $users_Id . '-Elliot-P' . $namestore;
		$data_string = '{
						  "projectName": "' . $Project_name . '",
						  "projectTypeCode": "APPLICATION_RESOURCES",
						  "sourceLocaleId": "en",
						  "targetLocaleIds": ["' . $language . '"]
						}';
		$curl = curl_init();
		$url = 'https://api.smartling.com/accounts-api/v2/accounts/' . $accountUid . '/projects';
		curl_setopt_array($curl, array(
		    CURLOPT_SSL_VERIFYPEER => false,
		    CURLOPT_URL => $url,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_ENCODING => "",
		    CURLOPT_MAXREDIRS => 100,
		    CURLOPT_TIMEOUT => 900,
		    CURLOPT_SSL_VERIFYHOST => FALSE,
		    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		    CURLOPT_CUSTOMREQUEST => "POST",
		    CURLOPT_POSTFIELDS => $data_string,
		    CURLOPT_HTTPHEADER => array(
			"cache-control: no-cache",
			"charset: utf-8",
			"Authorization: Bearer " . $access_token,
			"content-type: application/json"
		    ),
		));

		$result = curl_exec($curl);
		//echo '<pre>'; print_r(json_decode($result)); die();
		$data = json_decode($result);
		$projectId = $data->response->data->projectId;
		$projectTypeCode = $data->response->data->projectTypeCode;
		$projectTypeDisplayValue = $data->response->data->projectTypeDisplayValue;
		$targetLocales = $data->response->data->targetLocales;

		//$Smartling->elliot_user_id = $users_Id;
		$Smartling->project_name = $Project_name;
		$Smartling->project_id = $projectId;
		$Smartling->project_type = $projectTypeCode;
		$Smartling->project_type_value = $projectTypeDisplayValue;
		$Smartling->connected = 'yes';
		//$Smartling->target_locale = $targetLocales;
		/* 	$Smartling->secret_key = $userSecret; */
		/*  echo '<pre>';
		  print_r($Smartling);
		  echo '</pre>'; */
		$Smartling->save();


		$data = $this->Workflowuid($projectId, $access_token);
		/*  echo '<pre>';
		  print_r($data);
		  echo '</pre>'; */
		$projectId = $projectId;
		$userIdentifier = $SmartlingAccount->sm_user_id;
		$userSecret = $SmartlingAccount->secret_key;
		$Project_name = $Project_name;
		$token = $SmartlingAccount->token;

		$curl = curl_init();
		//$url = 'https://api.smartling.com/accounts-api/v2/accounts/'.$accountUid.'/projects';
		$url = 'https://api.smartling.com/workflows-api/v2/projects/' . $projectId . '/workflows';
		curl_setopt_array($curl, array(
		    CURLOPT_SSL_VERIFYPEER => false,
		    CURLOPT_URL => $url,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_ENCODING => "",
		    CURLOPT_MAXREDIRS => 100,
		    CURLOPT_TIMEOUT => 900,
		    CURLOPT_SSL_VERIFYHOST => FALSE,
		    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		    CURLOPT_CUSTOMREQUEST => "GET",
		    CURLOPT_HTTPHEADER => array(
			"cache-control: no-cache",
			"charset: utf-8",
			"Authorization: Bearer " . $access_token,
			"content-type: application/json"
		    ),
		));

		$result1 = curl_exec($curl);
		$data1 = json_decode($result1);
		//echo '<pre>'; print_r($data1);
		$itemsData = $data1->response->data->items;
		$selectTranslation;
		if ($selectTranslation == 'Google MT') {
		    $workflowUid = $itemsData['0']->workflowUid;
		} else if ($selectTranslation == 'Translation with Edit') {
		    $workflowUid = $itemsData['1']->workflowUid;
		} else {
		    $workflowUid = $itemsData['2']->workflowUid;
		}
		//echo $workflowUid;
		$this->Jobcreate($projectId, $userIdentifier, $userSecret, $Project_name, $token, $filemmmmae, $pathFileName, $workflowUid, $access_token, $language);
	    }
	    //echo $namestore;
	}

	/* $store_connection = Stores::find()->where(['store_id' => $channelID])->one();
	  if (empty($store_connection)) { */
	/*  $check_abbrivation = ProductAbbrivation::find()->where(['channel_accquired' => $namestoreold])->all();
	  //echo '<pre>'; print_r($check_abbrivation); die();
	  $i = 0;
	  $user_id = $users_Id;
	  $productArray = array();
	  foreach ($check_abbrivation as $data) {

	  $id = $data['id'];
	  $productData = Products::find()->where(['id' => $id])->one();
	  //	echo '<pre>'; print_r($productData); echo '</pre>';
	  $id = $id;
	  $name = @$productData->product_name;
	  $description = @$productData->description;
	  $condition = @$productData->condition;
	  $gender = @$productData->gender;
	  $stock_level = @$productData->stock_level;
	  $product_status = @$productData->product_status;
	  $productArray[$i]['id'] = $id;
	  $productArray[$i]['name'] = $name;
	  $productArray[$i]['description'] = $description;
	  $productArray[$i]['condition'] = $condition;
	  $productArray[$i]['gender'] = $gender;
	  $productArray[$i]['stock_level'] = $stock_level;
	  $productArray[$i]['product_status'] = $product_status;
	  $i++;
	  }
	  //echo '<pre>'; print_r($productArray); echo '</pre>';
	  $jsonProductData = json_encode($productArray); */




	/*  $fileName = $namestore . '--id--' . $user_id . '.json';
	  $pathfile = '/var/www/html/img/uploaded_files/' . $fileName;
	  $myfile = fopen($pathfile, "w");
	  if (file_exists($pathfile)) {
	  fwrite($myfile, $jsonProductData);
	  } else {
	  fwrite($pathfile, $jsonProductData);
	  } */
	// }

	for ($i = 0; $i < 5; $i++) {
	    if ($i == 0) {
		$setting_key = 'Enable_Smartling';
		$fulfillmentID = $enable;
	    } else if ($i == 1) {
		$setting_key = 'connected';
		$fulfillmentID = $catenable;
	    } else if ($i == 2) {

		$setting_key = 'Price';
		$fulfillmentID = $wordcount;
	    } else if ($i == 3) {

		$setting_key = 'Translationtype';
		$fulfillmentID = $selectTranslation;
	    } else {
		$setting_key = 'Language';
		$fulfillmentID = $language;
	    }

	    $SmartlingProject = Smartling::find()->where(['elliot_user_id' => $users_Id, 'token' => $namestoreold])->one();
	    $SmartlingProject->connected = $enable;
	    $SmartlingProject->save();
	    $channelsetting = Channelsetting::find()->where(['channel_id' => $channelID, 'user_id' => $users_Id, 'setting_key' => $setting_key])->one();
	    if (!empty($channelsetting)) {
		$Channelsetting = $channelsetting;
	    } else {
		$Channelsetting = new Channelsetting();
	    }

	    $Channelsetting->channel_id = $Main_channel_id_translation;
	    $Channelsetting->channel_name = $namestoreold;
	    $Channelsetting->setting_key = $setting_key;
	    $Channelsetting->user_id = $users_Id;
	    $Channelsetting->mul_store_id = $channelID;
	    $Channelsetting->setting_value = $fulfillmentID;
	    $Channelsetting->created_at = date('Y-m-d H:i:s');
	    if ($Channelsetting->save(false)) {
		// echo 'success';
	    } else {
		// echo 'error';
	    }
	}

	if ($enable == 'yes') {
	    $active = 'active';
	} else {
	    $active = 'in_active';
	}

	$config = yii\helpers\ArrayHelper::merge(
			require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php'));

	$application = new yii\web\Application($config);

	$Elli_user = User::find()->where(['id' => $users_Id])->one();
	if (!empty($Elli_user)) {
	    $Elli_user->smartling_status = $active;
	    $Elli_user->save(false);
	}

	echo 'You have initiated a translations job for ' . $namestoreold . ' in ' . $language . '.';
    }

    public function Jobcreate($projectId, $userIdentifier, $userSecret, $Project_name, $token, $filemmmmae, $pathFileName, $workflowUid, $access_token, $language) {

	$SmartlingProject = Smartling::find()->where(['project_id' => $projectId])->one();
	$project_id = $projectId;
	$sm_user_id = $userIdentifier;
	$secret_key = $userSecret;
	$project_name = $Project_name;
	$ds = DIRECTORY_SEPARATOR;
	$basedir = Yii::getAlias('@basedir');
	///echo '<pre>'; print_r($_POST); echo '</pre>';
	$options = [
	    'project-id' => '7289bfb87',
	    'user-id' => 'bhabaskqrnadgqgarxhuhawydiicml',
	    'secret-key' => '2anhc34el124ngi7pd46epc21kVM_ui3vgsmu1usvlshgfnva3viu6j',
	];

	if (!array_key_exists('project-id', $options) || !array_key_exists('user-id', $options) || !array_key_exists('secret-key', $options)
	) {
	    echo 'Missing required params.' . PHP_EOL;
	    exit;
	}

	$autoloader = $basedir . '/example-jobs-api/vendor/autoload.php';

	if (!file_exists($autoloader) || !is_readable($autoloader)) {
	    echo 'Error. Autoloader not found. Seems you didn\'t run:' . PHP_EOL . '    composer update' . PHP_EOL;
	    exit;
	} else {
	    /** @noinspection UntrustedInclusionInspection */
	    require_once $basedir . '/example-jobs-api/vendor/autoload.php';
	}

	$projectId = $project_id;
	$userIdentifier = $sm_user_id;
	$userSecretKey = $secret_key;
	$authProvider = \Smartling\AuthApi\AuthTokenProvider::create($userIdentifier, $userSecretKey);
	/* echo '<pre>';
	  print_r($authProvider);
	  echo '</pre>'; */
	/**
	 * Recommended flow.
	 *
	 * 0. Upload needed file with a help of FileAPI.
	 * 1. Create a job without locales.
	 * 2. Add needed locales to a job.
	 * 3. Attach file to a job.
	 * 4. Authorize a job.
	 * 5. Cancel job when it's needed.
	 *
	 * For more demos see jobs-sdk-php/example.php.
	 */
	$jobsAPI = JobsApi::create($authProvider, $projectId);
	$fileAPI = FileApi::create($authProvider, $projectId);

	try {
	    $localeId = $language;
	    $fileUri = $pathFileName;
	    $fileName = $filemmmmae;

	    // Upload file. 
	    $rs = $fileAPI->uploadFile($fileUri, $fileName, 'json');
	    
	    // Create a job without locales.
	    $createParams = new \Smartling\Jobs\Params\CreateJobParameters();
	    $createParams->setName($project_name . "--Job--" . time());
	    $createParams->setDescription($project_name . "--Job--" . time());
	    //$createParams->setDueDate('Jan 02, 2020 12:49 AM');

	    $job = $jobsAPI->createJob($createParams);

	    $SmartlingProject->job_translationJobUid = $job['translationJobUid'];
	    $SmartlingProject->job_jobName = $job['jobName'];
	    $SmartlingProject->job_referenceNumber = $job['referenceNumber'];
	    $SmartlingProject->job_targetLocaleIds = $language;
	    $SmartlingProject->target_locale = $language;
	    $SmartlingProject->job_callbackUrl = $filemmmmae;
	    $SmartlingProject->job_callbackMethod = $job['callbackMethod'];
	    $SmartlingProject->job_createdByUserUid = $job['createdByUserUid'];
	    $SmartlingProject->job_jobStatus = $job['jobStatus'];
	    $SmartlingProject->save();
	    /* Amit Sir Code Add To File Sync Commented Use New Api Method
	      $addLocaleParams = new AddLocaleToJobParameters();
	      $addLocaleParams->setSyncContent(false);
	      $jobsAPI->addLocaleToJobSync($job['translationJobUid'], $localeId, $addLocaleParams);

	      $addFileParams = new AddFileToJobParameters();
	      $addFileParams->setTargetLocales([$localeId]);
	      $addFileParams->setFileUri($fileName);

	      $fileupload = $jobsAPI->addFileToJobSync($job['translationJobUid'], $addFileParams);
	      End Amit Sir Code Add To File Sync Commented Use New Api Method */
	    
	    /* New FileAdd Sync by Curl Using fumnction */
	    $translationJobUid = $job['translationJobUid'];
	    $file_response_status = $this->customAddFileToJobSync($fileName, $localeId, $projectId, $translationJobUid, $access_token);
	    
	    $data = array(
		"localeWorkflows" => [array(
		"targetLocaleId" => $localeId,
		"workflowUid" => $workflowUid,
		    )],
	    );

	    $data_string = json_encode($data);
	    $curl = curl_init();
	    //​jobs-api/v3/projects/{projectId}/jobs/{translationJobUid}/authorize
	    $url = 'https://api.smartling.com/jobs-api/v3/projects/' . $projectId . '/jobs/' . $job['translationJobUid'] . '/authorize';
	    curl_setopt_array($curl, array(
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 100,
		CURLOPT_TIMEOUT => 900,
		CURLOPT_SSL_VERIFYHOST => FALSE,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => $data_string,
		CURLOPT_HTTPHEADER => array(
		    "cache-control: no-cache",
		    "charset: utf-8",
		    "Authorization: Bearer " . $access_token,
		    "content-type: application/json"
		),
	    ));

	    $result1 = curl_exec($curl);

	    $contentType = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	    
	    // Authorize job.
	    //$jobsAPI->authorizeJob($job['translationJobUid']);

	    $data_string = '{ "tags": ["test1", "test2"]}';
	    $curl = curl_init();
	    $url = 'https://api.smartling.com/estimates-api/v2/projects/' . $projectId . '/jobs/' . $job['translationJobUid'] . '/reports/fuzzy';
	    curl_setopt_array($curl, array(
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 100,
		CURLOPT_TIMEOUT => 900,
		CURLOPT_SSL_VERIFYHOST => FALSE,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => $data_string,
		CURLOPT_HTTPHEADER => array(
		    "cache-control: no-cache",
		    "charset: utf-8",
		    "Authorization: Bearer " . $access_token,
		    "content-type: application/json"
		),
	    ));

	    $result1 = curl_exec($curl);
	    $datareport = json_decode($result1);
	    $reportUid = $datareport->response->data->reportUid;


	    $SmartlingProject->project_type_value = $reportUid;
	    $SmartlingProject->save(false);
	    if ($SmartlingProject->save(false)) {
		//   echo 'success';
	    } else {
		// echo 'error';
	    }
	} catch (SmartlingApiException $e) {
	    echo"catch exception";
	    echo'<pre>';
	    print_r($e);
	    die('catch end 1877');
	    // echo '<pre>'; print_r($e); echo '</pre>';
	}
    }

    public static function customAddFileToJobSync($fileName, $localeId, $projectId, $translationJobUid, $access_token) {

	/* File add through job by curl for testing */

	$file_data = array(
	    'fileUri' => $fileName,
	    'targetLocaleIds' => [$localeId],
	);
	$file_data_encode = json_encode($file_data);
	$curl = curl_init();
	$url = 'https://api.smartling.com/jobs-api/v3/projects/' . $projectId . '/jobs/' . $translationJobUid . '/file/add';
	curl_setopt_array($curl, array(
	    CURLOPT_SSL_VERIFYPEER => false,
	    CURLOPT_URL => $url,
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_ENCODING => "",
	    CURLOPT_MAXREDIRS => 100,
	    CURLOPT_TIMEOUT => 900,
	    CURLOPT_SSL_VERIFYHOST => FALSE,
	    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	    CURLOPT_CUSTOMREQUEST => "POST",
	    CURLOPT_POSTFIELDS => $file_data_encode,
	    CURLOPT_HTTPHEADER => array(
		"cache-control: no-cache",
		"charset: utf-8",
		"Authorization: Bearer " . $access_token,
		"content-type: application/json"
	    ),
	));

	$result1 = curl_exec($curl);
	$result = json_decode($result1, true);
	$response=true;
	if(isset($result['response']['errors'])){
	    $response=false;
	    $error_msg=$result['response']['errors'][0]['message'];
	    $explde_error_msg= explode(':', $error_msg);
	    if(strtolower($explde_error_msg[0])== strtolower('File locked')){
		$response=false;
	    }
	}
	
	if($response!=true){
	    sleep(100);
	    self::customAddFileToJobSync($fileName, $localeId, $projectId, $translationJobUid, $access_token);
	}
	//echo $response;
	return  $response;
    }

    public function actionJobsetting() {
	$token = $_POST['token'];
	$SmartlingProject = Smartling::find()->where(['token' => $token])->one();
	//echo '<pre>'; print_r($SmartlingProject); echo '</pre>';
	$project_id = $SmartlingProject->project_id;
	$sm_user_id = $SmartlingProject->sm_user_id;
	$secret_key = $SmartlingProject->secret_key;
	$project_name = $SmartlingProject->project_name;
	$ds = DIRECTORY_SEPARATOR;
	$basedir = Yii::getAlias('@basedir');
	///echo '<pre>'; print_r($_POST); echo '</pre>';
	$options = [
	    'project-id' => '7289bfb87',
	    'user-id' => 'bhabaskqrnadgqgarxhuhawydiicml',
	    'secret-key' => '2anhc34el124ngi7pd46epc21kVM_ui3vgsmu1usvlshgfnva3viu6j',
	];

	if (!array_key_exists('project-id', $options) || !array_key_exists('user-id', $options) || !array_key_exists('secret-key', $options)
	) {
	    echo 'Missing required params.' . PHP_EOL;
	    exit;
	}

	$autoloader = $basedir . '/example-jobs-api/vendor/autoload.php';

	if (!file_exists($autoloader) || !is_readable($autoloader)) {
	    echo 'Error. Autoloader not found. Seems you didn\'t run:' . PHP_EOL . '    composer update' . PHP_EOL;
	    exit;
	} else {
	    /** @noinspection UntrustedInclusionInspection */
	    require_once $basedir . '/example-jobs-api/vendor/autoload.php';
	}

	$projectId = $project_id;
	$userIdentifier = $sm_user_id;
	$userSecretKey = $secret_key;
	$authProvider = \Smartling\AuthApi\AuthTokenProvider::create($userIdentifier, $userSecretKey);

	/**
	 * Recommended flow.
	 *
	 * 0. Upload needed file with a help of FileAPI.
	 * 1. Create a job without locales.
	 * 2. Add needed locales to a job.
	 * 3. Attach file to a job.
	 * 4. Authorize a job.
	 * 5. Cancel job when it's needed.
	 *
	 * For more demos see jobs-sdk-php/example.php.
	 */
	$jobsAPI = JobsApi::create($authProvider, $projectId);
	$fileAPI = FileApi::create($authProvider, $projectId);

	try {
	    $localeId = 'zh-CN';
	    $fileUri = 'file.xml';
	    //  $fileName = 'file.xml';
	    // Upload file.
	    // $fileAPI->uploadFile($fileUri, $fileName, 'xml');
	    // Create a job without locales.
	    $createParams = new \Smartling\Jobs\Params\CreateJobParameters();
	    $createParams->setName($project_name . "--Job--" . time());
	    $createParams->setDescription($project_name . "--Job--" . time());
	    //$createParams->setDueDate('Jan 02, 2020 12:49 AM');

	    $job = $jobsAPI->createJob($createParams);


	    // Add locale to a job.
	    $addLocaleParams = new AddLocaleToJobParameters();
	    $addLocaleParams->setSyncContent(false);
	    $jobsAPI->addLocaleToJobSync($job['translationJobUid'], $localeId, $addLocaleParams);
	    echo '<pre>';
	    print_r($job);
	    echo '</pre>';
	    $SmartlingProject->job_translationJobUid = $job['translationJobUid'];
	    $SmartlingProject->job_jobName = $job['jobName'];
	    $SmartlingProject->job_referenceNumber = $job['referenceNumber'];
	    $SmartlingProject->job_callbackUrl = $job['callbackUrl'];
	    $SmartlingProject->job_callbackMethod = $job['callbackMethod'];
	    $SmartlingProject->job_createdByUserUid = $job['createdByUserUid'];
	    $SmartlingProject->job_jobStatus = $job['jobStatus'];
	    if ($SmartlingProject->save(false)) {
		echo 'success';
	    } else {
		echo 'error';
	    }
	    // Attach uploaded file to a job.
	    /*  $addFileParams = new AddFileToJobParameters();
	      $addFileParams->setTargetLocales([$localeId]);
	      $addFileParams->setFileUri($fileName);
	      $jobsAPI->addFileToJobSync($job['translationJobUid'], $addFileParams); */

	    // Authorize job.
	    // $jobsAPI->authorizeJob($job['translationJobUid']);
	    // Cancel job.
	    /*  $cancelParams = new CancelJobParameters();
	      $cancelParams->setReason('Some reason to cancel');
	      $jobsAPI->cancelJobSync($job['translationJobUid'], $cancelParams); */
	} catch (SmartlingApiException $e) {

	    var_dump($e);
	}
    }

    public function actionTranslationsave() {
	$connected = $_POST['smartlingyes'];
	$translation_type = $_POST['translation_type'];
	$proidval = $_POST['proidval'];
	$SmartlingProject = Smartling::find()->where(['token' => $proidval])->one();

	$user_id = Yii::$app->user->identity->id;
	$smartlingData = Smartling::find()->Where(['elliot_user_id' => $user_id])->one();
	if (!empty($smartlingData)) {
	    $smartlingData->elliot_user_id = $user_id;
	    $smartlingData->connected = $connected;
	    $smartlingData->translation_type = $translation_type;
	    $smartlingData->created_at = date('Y-m-d H:i:s');
	    $smartlingData->save(false);
	    echo 'success';
	} else {

	    $smartlingData = new Smartling();
	    $smartlingData->elliot_user_id = $user_id;
	    $smartlingData->connected = $connected;
	    $smartlingData->translation_type = $translation_type;
	    $smartlingData->created_at = date('Y-m-d H:i:s');
	    $smartlingData->save(false);
	    echo 'success';
	}
	$options = [
	    'project-id' => '7289bfb87',
	    'user-id' => 'bhabaskqrnadgqgarxhuhawydiicml',
	    'secret-key' => '2anhc34el124ngi7pd46epc21kVM_ui3vgsmu1usvlshgfnva3viu6j',
	];
	$ds = DIRECTORY_SEPARATOR;
	$basedir = Yii::getAlias('@basedir');
	$autoloader = $basedir . '/example-jobs-api/vendor/autoload.php';

	if (!file_exists($autoloader) || !is_readable($autoloader)) {
	    echo 'Error. Autoloader not found. Seems you didn\'t run:' . PHP_EOL . '    composer update' . PHP_EOL;
	    exit;
	} else {
	    /** @noinspection UntrustedInclusionInspection */
	    require_once $basedir . '/example-jobs-api/vendor/autoload.php';
	}
	$project_id = $SmartlingProject->project_id;
	$sm_user_id = $SmartlingProject->sm_user_id;
	$secret_key = $SmartlingProject->secret_key;
	$job_translationJobUid = $SmartlingProject->job_translationJobUid;
	$projectId = $project_id;
	$userIdentifier = $sm_user_id;
	$userSecretKey = $secret_key;
	$authProvider = \Smartling\AuthApi\AuthTokenProvider::create($userIdentifier, $userSecretKey);
//echo '<pre>'; print_r($authProvider); echo '</pre>';

	$jobsAPI = JobsApi::create($authProvider, $projectId);
	$fileAPI = FileApi::create($authProvider, $projectId);

	try {
	    $localeId = 'zh-CN';
	    $fileUri = $basedir . '/example-jobs-api/file.xml';
	    $fileName = 'file.xml';

	    // Upload file. 
	    $fileAPI->uploadFile($fileUri, $fileName, 'xml');

	    // Create a job without locales.
	    /* $createParams = new \Smartling\Jobs\Params\CreateJobParameters();
	      $createParams->setName("NewFile " . time());
	      $createParams->setDescription("NewFile" . time());
	      //$createParams->setDueDate(DateTime::createFromFormat('Y-m-d H:i:s', '2020-01-01 19:19:17', new DateTimeZone('UTC')));

	      $job = $jobsAPI->createJob($createParams);
	      echo '<pre>'; print_r($job); echo '</pre>'; */

	    // Add locale to a job.
	    /* $addLocaleParams = new AddLocaleToJobParameters();
	      $addLocaleParams->setSyncContent(false);
	      $jobsAPI->addLocaleToJobSync($job['translationJobUid'], $localeId, $addLocaleParams); */

	    // Attach uploaded file to a job.
	    $addFileParams = new AddFileToJobParameters();
	    $addFileParams->setTargetLocales([$localeId]);
	    $addFileParams->setFileUri($fileName);
	    echo '<pre>';
	    print_r($addFileParams);
	    echo '</pre>';

	    $jobsAPI->addFileToJobSync($job['translationJobUid'], $addFileParams);

	    // Authorize job.
	    $jobsAPI->authorizeJob($job['translationJobUid']);

	    // Cancel job.
	    /*  $cancelParams = new CancelJobParameters();
	      $cancelParams->setReason('Some reason to cancel');
	      $jobsAPI->cancelJobSync($job['translationJobUid'], $cancelParams); */
	} catch (SmartlingApiException $e) {
	    echo '<pre>';
	    print_r($e);
	}
    }

    public function deleteAllData($store_name, $store_connection_id) {
	$this->deleteCategoryData($store_name, $store_connection_id);
	$this->deleteProductData($store_name, $store_connection_id);
	$this->deleteCustomerData($store_name, $store_connection_id);
	$this->deleteOrderData($store_name, $store_connection_id);
	$this->deleteStoreChannelSetting($store_name, $store_connection_id);
    }

    /** Delete Category data * */
    public function deleteCategoryData($store_name, $store_connection_id) {
	$category_ids = CategoryAbbrivation::find()->where(['mul_store_id' => $store_connection_id, 'channel_accquired' => $store_name])->all();
	if (!empty($category_ids)) {
	    foreach ($category_ids as $_cat) {
		$check_cat = CategoryAbbrivation::find()->where(['category_ID' => $_cat->category_ID])->count();
		if (!empty($check_cat)) {
		    if ($check_cat == 1) {
			Categories::find($_cat->category_ID)->one()->delete();
		    } else {
			CategoryAbbrivation::deleteAll(['category_ID' => $_cat->category_ID, 'channel_accquired' => $store_name, 'mul_store_id' => $store_connection_id]);
		    }
		}
	    }
	}
    }

    /** Delete Product data * */
    public function deleteProductData($store_name, $store_connection_id) {
	$product_ids = ProductAbbrivation::find()->where(['mul_store_id' => $store_connection_id, 'channel_accquired' => $store_name])->all();
	if (!empty($product_ids)) {
	    foreach ($product_ids as $_product) {
		$check_product = ProductAbbrivation::find()->where(['product_id' => $_product->product_id])->count();
		if ($check_product == 1) {
		    Products::deleteAll(['id' => $_product->product_id]);
		} else {
		    ProductAbbrivation::deleteAll(['product_id' => $_product->product_id, 'channel_accquired' => $store_name, 'mul_store_id' => $store_connection_id]);
		    ProductChannel::deleteAll(['product_id' => $_product->product_id, 'store_id' => $store_connection_id]);
		}
	    }
	}
    }

    /** Delete Customer data * */
    public function deleteCustomerData($store_name, $store_connection_id) {
	$customer_ids = CustomerAbbrivation::find()->where(['mul_store_id' => $store_connection_id, 'channel_accquired' => $store_name])->all();
	if (!empty($customer_ids)) {
	    foreach ($customer_ids as $_customer) {
		$check_customer = CustomerAbbrivation::find()->where(['customer_id' => $_customer->customer_id])->count();
		if ($check_customer == 1) {
		    CustomerUser::deleteAll(['customer_ID' => $_customer->customer_id]);
		} else {
		    CustomerAbbrivation::deleteAll(['customer_id' => $_customer->customer_id, 'channel_accquired' => $store_name, 'mul_store_id' => $store_connection_id]);
		}
	    }
	}
    }

    /** Delete Order data * */
    public function deleteOrderData($store_name, $store_connection_id) {
	$order_ids = Orders::deleteAll(['mul_store_id' => $store_connection_id, 'channel_accquired' => $store_name]);
    }

    /** Delete Channel Setting  * */
    public function deleteStoreChannelSetting($store_name, $store_connection_id) {
	Channelsetting::deleteAll(['mul_store_id' => $store_connection_id]);
    }

    public function deleteAllDataChannel($channel_name, $channel_id) {
	$this->deleteChannelCategoryData($channel_name);
	$this->deleteChannelProductData($channel_name, $channel_id);
	$this->deleteChannelCustomerData($channel_name);
	$this->deleteChannelOrderData($channel_name);
	$this->deleteChannelSetting($channel_name);
    }

    public function deleteChannelCategoryData($channel_name) {
	$category_ids = CategoryAbbrivation::find()->where(['channel_accquired' => $channel_name])->all();
	if (!empty($category_ids)) {
	    foreach ($category_ids as $_cat) {
		$check_cat = CategoryAbbrivation::find()->where(['category_ID' => $_cat->category_ID])->count();
		if (!empty($check_cat)) {
		    if ($check_cat == 1) {
			Categories::deleteAll(['category_ID' => $_cat->category_ID]);
		    } else {
			CategoryAbbrivation::deleteAll(['category_ID' => $_cat->category_ID, 'channel_accquired' => $channel_name]);
		    }
		}
	    }
	}
    }

    public function deleteChannelProductData($channel_name, $channel_id) {
	$product_ids = ProductAbbrivation::find()->where(['channel_accquired' => $channel_name])->all();
	if (!empty($product_ids)) {
	    foreach ($product_ids as $_product) {
		$check_product = ProductAbbrivation::find()->where(['product_id' => $_product->product_id])->count();
		if ($check_product == 1) {
		    Products::deleteAll(['id' => $_product->product_id]);
		} else {
		    ProductAbbrivation::deleteAll(['product_id' => $_product->product_id, 'channel_accquired' => $channel_name]);
		    ProductChannel::deleteAll(['product_id' => $_product->product_id, 'channel_id' => $channel_id]);
		}
	    }
	}
    }

    public function deleteChannelCustomerData($channel_name) {
	$customer_ids = CustomerAbbrivation::find()->where(['channel_accquired' => $channel_name])->all();
	if (!empty($customer_ids)) {
	    foreach ($customer_ids as $_customer) {
		$check_customer = CustomerAbbrivation::find()->where(['customer_id' => $_customer->customer_id])->count();
		if ($check_customer == 1) {
		    CustomerUser::deleteAll(['customer_id' => $_customer->customer_id]);
		} else {
		    CustomerAbbrivation::deleteAll(['customer_id' => $_customer->customer_id, 'channel_accquired' => $channel_name]);
		}
	    }
	}
    }

    public function deleteChannelOrderData($channel_name) {
	$order_ids = Orders::deleteAll(['channel_accquired' => $channel_name]);
    }

    public function deleteChannelSetting($channel_name) {
	Channelsetting::deleteAll(['channel_name' => $channel_name]);
    }

    public function actionNewtest() {
	$data_string = '{
						  "userIdentifier": "stjdueerybhpalqozmoebbrrqmoknb",
						  "userSecret": "dcr887ov0o93nh07lpeni0mukqTR.74gqc3ihbfofuodcsbm4862gsj"
						}';
	$curl = curl_init();
	$url = 'https://api.smartling.com/auth-api/v2/authenticate';
	curl_setopt_array($curl, array(
	    CURLOPT_SSL_VERIFYPEER => false,
	    CURLOPT_URL => $url,
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_ENCODING => "",
	    CURLOPT_MAXREDIRS => 100,
	    CURLOPT_TIMEOUT => 900,
	    CURLOPT_SSL_VERIFYHOST => FALSE,
	    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	    CURLOPT_CUSTOMREQUEST => "POST",
	    CURLOPT_POSTFIELDS => $data_string,
	    CURLOPT_HTTPHEADER => array(
		"cache-control: no-cache",
		"charset: utf-8",
		"X-Auth-Token: BfcqrhanfJghxYZQe8PtDhUMuQHaPtqqRuQZtpZE&jkfnkCfeRBVXLsiBqFdXDaB",
		"content-type: application/json"
	    ),
	));

	$resultAuth = curl_exec($curl);

	//echo '<pre>'; print_r(json_decode($resultAuth)); die();
	$authData = json_decode($resultAuth);

	echo $access_token = $authData->response->data->accessToken;








	echo $data_string = '{ "tags": ["test1", "test2"]}';
	$curl = curl_init();
	$url = 'https://api.smartling.com/estimates-api/v2/projects/2366dc7d8/jobs/jzets8je6qaz/reports/fuzzy';
	curl_setopt_array($curl, array(
	    CURLOPT_SSL_VERIFYPEER => false,
	    CURLOPT_URL => $url,
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_ENCODING => "",
	    CURLOPT_MAXREDIRS => 100,
	    CURLOPT_TIMEOUT => 900,
	    CURLOPT_SSL_VERIFYHOST => FALSE,
	    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	    CURLOPT_CUSTOMREQUEST => "POST",
	    CURLOPT_POSTFIELDS => $data_string,
	    CURLOPT_HTTPHEADER => array(
		"cache-control: no-cache",
		"charset: utf-8",
		"Authorization: Bearer " . $access_token,
		"content-type: application/json"
	    ),
	));

	$result1 = curl_exec($curl);
	echo '<pre>';
	print_r(json_decode($result1));
	$datareport = json_decode($result1);
	$reportUid = $datareport->response->data->reportUid;






	//​/​/estimates-api/v2/projects/{projectId}/reports/{reportUid}
	//​/estimates-api/v2/projects/{projectId}/reports/{reportUid}/status
	/* 	$curl = curl_init();
	  $url = 'https://api.smartling.com/estimates-api/v2/projects/5ecfd37f7/reports/7pcbcl422dol/status';
	  curl_setopt_array($curl, array(
	  CURLOPT_SSL_VERIFYPEER => false,
	  CURLOPT_URL => $url,
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 100,
	  CURLOPT_TIMEOUT => 900,
	  CURLOPT_SSL_VERIFYHOST => FALSE,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",

	  CURLOPT_HTTPHEADER => array(
	  "cache-control: no-cache",
	  "charset: utf-8",
	  "Authorization: Bearer ".$access_token,
	  "content-type: application/json"
	  ),
	  ));

	  $result1 = curl_exec($curl);
	  //$reportUid = $result1->response->data->reportUid;
	  echo '<pre>'; print_r(json_decode($result1));

	 */

	$curl = curl_init();
	$url = 'https://api.smartling.com/estimates-api/v2/projects/2366dc7d8/reports/' . $reportUid;
	curl_setopt_array($curl, array(
	    CURLOPT_SSL_VERIFYPEER => false,
	    CURLOPT_URL => $url,
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_ENCODING => "",
	    CURLOPT_MAXREDIRS => 100,
	    CURLOPT_TIMEOUT => 900,
	    CURLOPT_SSL_VERIFYHOST => FALSE,
	    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	    CURLOPT_CUSTOMREQUEST => "GET",
	    CURLOPT_HTTPHEADER => array(
		"cache-control: no-cache",
		"charset: utf-8",
		"Authorization: Bearer " . $access_token,
		"content-type: application/json"
	    ),
	));

	$result2 = curl_exec($curl);
	//$reportUid = $result1->response->data->reportUid;
	echo '<pre>';
	print_r(json_decode($result2));
    }

}
