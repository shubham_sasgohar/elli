<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use backend\models\SignupForm;
use backend\models\CurrencySymbols;
use backend\models\PasswordResetRequestForm;
use backend\models\ResetPasswordForm;
use backend\models\Countries;
use backend\models\States;
use backend\models\Cities;
use backend\models\Languages;
use backend\models\Smartling;
use common\models\CustomFunction;
use backend\models\Stores;
use backend\models\User;
use backend\models\TrialPeriod;
use backend\models\UserSubscription;
use yii\db\Query;
use yii\db\ActiveQuery;
use yii\db\QueryBuilder;
use yii\web\UploadedFile;
use yii\imagine\Image;
use backend\models\PasswordForm;
use backend\models\Notification;
use backend\models\StoresConnection;
use backend\models\StoreDetails;
use backend\models\Orders;
use backend\models\OrderChannel;
use backend\models\OrdersProducts;
use Bigcommerce\Api\Connection;
use Bigcommerce\Api\Client as Bigcommerce;
use backend\models\Categories;
use backend\models\Products;
use backend\models\ProductCategories;
use backend\models\ProductChannel;
use backend\models\ProductImages;
use backend\models\CustomerUser;
use backend\models\CustomerAbbrivation;
use backend\models\ChannelConnection;
use backend\models\CorporateDocuments;
use backend\models\BillingInvoice;
use backend\models\Channels;
//use backend\models\ChannelConnection;
use yii\web\Session;
use yii\helpers\ArrayHelper;
use backend\models\CurrencyConversion;

/**
 * Site controller
 */
class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
	return [
	    'access' => [
		'class' => AccessControl::className(),
		'only' => ['logout', 'profile', 'signup', 'index', 'error', 'user-subscription', 'order', 'cron-account-status', 'subscriptionpayment', 'profile', 'saveprofile', 'delete',
		    'export-user', 'general', 'destroysession', 'get-countries', 'get-states', 'get-cities', 'save-cover-image', 'save-profile-image', 'save-general-info', 'get-languages',
		    'get-timezone', 'translations', 'request-password-reset', 'reset-password', 'request-password-reset-token', 'change-password', 'save-smartling-api', 'integrate-transaltion',
		    'clear-notification', 'dashboard-graph', 'dashboard-graph-orders', 'ship-station', 'testoauth', 'createhook', 'dashboard-graph-product-sold', 'dashboard-graph-average-order-value',
		    'help-page', 'system-updates', 'system-status', 'terms-conditions', 'corporate-documents', 'upload-documents', 'newdashboard', 'updatedashboard', 'areachartondashboard'],
		'rules' => [
			[
			'actions' => ['signup', 'request-password-reset', 'reset-password', 'request-password-reset-token', 'testoauth', 'testload', 'createhook'],
			'allow' => true,
			'roles' => ['?'],
		    ],
			[
			'actions' => ['logout', 'profile', 'index', 'error', 'user-subscription', 'order', 'cron-account-status', 'subscriptionpayment', 'profile', 'saveprofile', 'delete',
			    'export-user', 'general', 'destroysession', 'get-countries', 'get-states', 'get-cities', 'save-cover-image', 'save-profile-image', 'save-general-info'
			    , 'get-languages', 'get-timezone', 'translations', 'change-password', 'save-smartling-api', 'integrate-transaltion', 'clear-notification', 'dashboard-graph',
			    'dashboard-graph-orders', 'ship-station', 'testoauth', 'dashboard-graph-product-sold', 'dashboard-graph-average-order-value', 'help-page', 'system-updates',
			    'system-status', 'corporate-documents', 'terms-conditions', 'upload-documents', 'newdashboard', 'updatedashboard', 'areachartondashboard'],
			'allow' => true,
			'roles' => ['@'],
		    ],
		],
	    ],
	    'verbs' => [
		'class' => VerbFilter::className(),
		'actions' => [
		    'logout' => ['post'],
		],
	    ],
	];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
	return [
	    'error' => [
		'class' => 'yii\web\ErrorAction',
	    ],
	];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action) {
	$this->enableCsrfValidation = false;
	return parent::beforeAction($action);
    }

    public function actionUpdatedashboard() {
	//New dashboard carousel
	$user = Yii::$app->user->identity;
	$user_id = $user->id;
	$currency = $user->currency;
	$currency = strtolower($currency);
	$symbol = CurrencySymbols::find()->select(['symbol'])->where(['name' => $currency])->one();
	$store_details_data = StoreDetails::find()->asArray()->all();
	$products_by_volume = OrdersProducts::find()
		->select(['sum(qty) AS product_quantity', 'product_Id'])
		->where(['elliot_user_id' => $user_id])
		->groupBy(['product_id'])
		->orderBy(['product_quantity' => SORT_DESC])
		->with(['product' => function($query) {
			$query->where(['not like', 'product_status', 'in_active']);
			$query->select('id, price, product_name');
		    },
		    'product.productImages' => function($query) {
			$query->select('product_ID,link');
		    }])
//                    ->limit(20)
		->asArray()
		->all();

	$products_by_revenue = OrdersProducts::find()
		->select(['sum(price) AS product_price', 'product_Id'])
		->where(['elliot_user_id' => $user_id])
		->groupBy(['product_id'])
		->orderBy(['product_price' => SORT_DESC])
		->with(['product' => function($query) {
			$query->where(['not like', 'product_status', 'in_active']);
			$query->select('id, price, product_name,country_code');
		    },
		    'product.productImages' => function($query) {
			$query->select('product_ID,link');
		    }])
//                    ->limit(20)
		->asArray()
		->all();

	$countries = \backend\models\StoreDetails::find()->select(['id', 'country_code', 'country'])->distinct()->asArray()->all();
	$countries = ArrayHelper::map($countries, 'country_code', 'country');
	//        echo"<pre>";            print_R($countries);die;
	if (isset($_GET['$$']) and ! empty($_GET['$$'])) {
	    echo "<pre>";
	    print_r($products_by_volume);
	    print_r($products_by_revenue);
	    print_r($countries);
	    print_R($store_details_data);
	    die;
	}

	$products_by_revenue_count = 0;
	if (isset($products_by_revenue) and ! empty($products_by_revenue)) {
	    foreach ($products_by_revenue as $single_revenue) {
		if (isset($single_revenue['product']) and ! empty($single_revenue['product'])) {
		    $products_by_revenue_count++;
		}
	    }
	}
	$products_by_volume_count = 0;
	if (isset($products_by_volume) and ! empty($products_by_volume)) {
	    foreach ($products_by_volume as $single_volume) {
		if (isset($single_volume['product']) and ! empty($single_volume['product'])) {
		    $products_by_volume_count++;
		}
	    }
	}
	return $this->render('updateDashboard', [
		    'products_by_volume' => $products_by_volume,
		    'products_by_revenue' => $products_by_revenue,
		    'products_by_volume_count' => $products_by_volume_count,
		    'products_by_revenue_count' => $products_by_revenue_count,
		    'countries' => $countries,
		    'user' => $user,
		    'symbol' => $symbol->symbol,
		    'store_details_data' => $store_details_data
			]
	);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {

	$role = Yii::$app->user->identity->role;

	if ($role != 'superadmin') {
	    $basedir = Yii::getAlias('@basedir');
	    //include stripe init file//
	    require $basedir . '/stripe/init.php';
	    \Stripe\Stripe::setApiKey("sk_test_2GPaBt5mT2k0yyTA9ho3CPIQ");
	    //get subscription plan through stripe//
	    $collection_plans = \Stripe\Plan::all(array("limit" => 100));

	    $stripe_plans = $collection_plans->__toArray(true);
	    $stripe_plan_data = $stripe_plans['data'];

	    foreach ($stripe_plan_data as $data) {
		$db_stripe_channel_data = Channels::find()->Where(['channel_name' => $data['name']])->one();
		if (empty($db_stripe_channel_data)) {
		    $meta = @$data['metadata']['image_url'];
		    $id = $data['id'];
		    $prefix_channel = substr($id, 0, 7);
		    if ($prefix_channel == 'channel') {
			$stripe_Channel = new Channels();
			$stripe_Channel->stripe_Channel_id = stripslashes($data['id']);
			$stripe_Channel->amount = stripslashes($data['amount'] / 100);
			$stripe_Channel->currency = stripslashes($data['currency']);
			$stripe_Channel->channel_image = stripslashes(@$data['metadata']['image_url']);
			$stripe_Channel->channel_name = stripslashes($data['name']);
			$stripe_Channel->parent_name = stripslashes(@$data['metadata']['parent_name']);
			$stripe_Channel->trial_period_days = stripslashes($data['trial_period_days']);
			$stripe_Channel->save(false);
		    }
		} else {
		    $meta = @$data['metadata']['image_url'];
		    $id = $data['id'];
		    $prefix_channel = substr($id, 0, 7);
		    if ($prefix_channel == 'channel') {
			$stripe_Channel = Channels::find()->Where(['channel_name' => $data['name']])->one();
			$stripe_Channel->stripe_Channel_id = stripslashes($data['id']);
			$stripe_Channel->amount = stripslashes($data['amount'] / 100);
			$stripe_Channel->currency = stripslashes($data['currency']);
			$stripe_Channel->channel_image = stripslashes(@$data['metadata']['image_url']);
			$stripe_Channel->channel_name = stripslashes($data['name']);
			$stripe_Channel->parent_name = stripslashes(@$data['metadata']['parent_name']);
			$stripe_Channel->trial_period_days = stripslashes($data['trial_period_days']);
			$stripe_Channel->save(false);
		    }
		}
	    }
	}
	//New dashboard carousel
	$user = Yii::$app->user->identity;
	$user_id = $user->id;
	$currency = $user->currency;
	$currency = strtolower($currency);
	$symbol = CurrencySymbols::find()->select(['symbol'])->where(['name' => $currency])->one();

	$store_details_data = StoreDetails::find()->asArray()->all();
	$current_date = date('Y-m-d');
	$previous_date = date('Y-m-d', strtotime('-30 days'));
	$products_by_volume = OrdersProducts::find()
		->select(['sum(qty) AS product_quantity', 'product_Id'])
		->where(['elliot_user_id' => $user_id])
		->andFilterWhere(['between', 'date(created_at)', $previous_date, $current_date])
		->groupBy(['product_id'])
		->orderBy(['product_quantity' => SORT_DESC])
		->with(['product' => function($query) {
			$query->where(['not like', 'product_status', 'in_active']);
			$query->select('id, price, product_name');
		    },
		    'product.productImages' => function($query) {
			$query->select('product_ID,link');
		    }])
//                    ->limit(20)
		->asArray()
		->all();


	$products_by_revenue = OrdersProducts::find()
		->select(['sum(price) AS product_price', 'product_Id'])
		->where(['elliot_user_id' => $user_id])
		->andFilterWhere(['between', 'date(created_at)', $previous_date, $current_date])
		->groupBy(['product_id'])
		->orderBy(['product_price' => SORT_DESC])
		->with(['product' => function($query) {
			$query->where(['not like', 'product_status', 'in_active']);
			$query->select('id, price, product_name,country_code');
		    },
		    'product.productImages' => function($query) {
			$query->select('product_ID,link');
		    }])
//                    ->limit(20)
		->asArray()
		->all();

	$countries = \backend\models\StoreDetails::find()->select(['id', 'country_code', 'country'])->distinct()->asArray()->all();
	$countries = ArrayHelper::map($countries, 'country_code', 'country');
	//        echo"<pre>";            print_R($countries);die;
	if (isset($_GET['$$']) and ! empty($_GET['$$'])) {
	    echo "<pre>";
	    print_r($products_by_volume);
	    print_r($products_by_revenue);
	    print_r($countries);
	    print_R($store_details_data);
	    die;
	}

	$products_by_revenue_count = 0;
	if (isset($products_by_revenue) and ! empty($products_by_revenue)) {
	    foreach ($products_by_revenue as $single_revenue) {
		if (isset($single_revenue['product']) and ! empty($single_revenue['product'])) {
		    $products_by_revenue_count++;
		}
	    }
	}
	$products_by_volume_count = 0;
	if (isset($products_by_volume) and ! empty($products_by_volume)) {
	    foreach ($products_by_volume as $single_volume) {
		if (isset($single_volume['product']) and ! empty($single_volume['product'])) {
		    $products_by_volume_count++;
		}
	    }
	}
	return $this->render('updateDashboard', [
		    'products_by_volume' => $products_by_volume,
		    'products_by_revenue' => $products_by_revenue,
		    'products_by_volume_count' => $products_by_volume_count,
		    'products_by_revenue_count' => $products_by_revenue_count,
		    'countries' => $countries,
		    'user' => $user,
		    'symbol' => $symbol->symbol,
		    'store_details_data' => $store_details_data
			]
	);



//        return $this->render('index');
//        $check_subscription = CustomFunction::checkSubscriptionPlan();
//
//        if ($check_subscription == 'false') {
//            return $this->redirect('user-subscription');
//        } else {
	//      return $this->render('index');
//        }
    }

    /**
     * Displays Profile.
     *
     * @return string
     */
    public function actionProfile() {
	return $this->render('profile');
    }

    /**
     * Displays User Subscription Page.
     *
     * @return string
     */
    public function actionUserSubscription() {
	return $this->render('usersubscription');
    }

    /**
     * Displays General Page.
     *
     * @return string
     */
    public function actionGeneral() {

	return $this->render('general');
    }

    /**
     * Displays Genral Page.
     *
     * @return string
     */
    public function actionHelpPage() {
	return $this->render('help');
    }

    public function actionSystemUpdates() {
	return $this->render('systemupdate');
    }

    public function actionSystemStatus() {
	return $this->render('systemstatus');
    }

    public function actionTermsConditions() {
	return $this->render('termscondition');
    }

    /* For Important Documents */

    public function actionCorporateDocuments() {
	return $this->render('importdocument');
    }

    public function actionUploadDocuments() {
	$user_id = Yii::$app->user->identity->id;
	$ds = DIRECTORY_SEPARATOR;
	$basedir = Yii::getAlias('@basedir');

	$maxsize = 26214400;
	/* For validation */
	$tax_val = $_POST['tax_id'];
	$channel_name = $_POST['channel_name'];
	$buisness_file = $_FILES['file-2'];
	$incorporation_file = $_FILES['file-3'];

	$array_msg = array();
	$error_msg = '';
	$validate = TRUE;
	if ($tax_val == '' || $tax_val == "Empty"):
	    $error_msg .= '<li>Please Fill Your Tax ID</li>';
	    $validate = FALSE;
	endif;
	if ($channel_name == ''):
	    $error_msg .= '<li>Please Fill Your Channel Name</li>';
	    $validate = FALSE;
	endif;

	/* Buisness_file Type check */
	$buisnees_allowed = array('pdf', 'jpg', 'png');
	$buisnees_filename = $buisness_file['name'];
	$buisnees_ext = pathinfo($buisnees_filename, PATHINFO_EXTENSION);
	if ($buisnees_filename == "") {
	    $error_msg .= '<li>Please Upload Business License</li>';
	    $validate = FALSE;
	} else {
	    if (!in_array($buisnees_ext, $buisnees_allowed)) {
		$error_msg .= '<li>Extension is not valid</li>';
		$validate = FALSE;
	    }
	    if (($buisness_file['size'] >= $maxsize) || ($buisness_file["size"] == 0)) {
		$error_msg .= '<li>File too large. File must be less than 25 MB.</li>';
		$validate = FALSE;
	    }
	}
	/* End  Buisness_file Type check */

	/* incorporation_file Type check */
	$incorporation_allowed = array('pdf', 'jpg', 'png');
	$incorporation_filename = $incorporation_file['name'];
	$incorporation_filename_ext = pathinfo($incorporation_filename, PATHINFO_EXTENSION);
	if ($incorporation_filename == "") {
	    $error_msg .= '<li>Please Upload Business Incorporation Papers</li>';
	    $validate = FALSE;
	} else {
	    if (!in_array($incorporation_filename_ext, $incorporation_allowed)) {
		$error_msg .= '<li>Extension is not valid</li>';
		$validate = FALSE;
	    }
	    if (($incorporation_file['size'] >= $maxsize) || ($incorporation_file["size"] == 0)) {
		$error_msg .= '<li>File too large. File must be less than 25 MB.</li>';
		$validate = FALSE;
	    }
	}
	if ($validate == FALSE) {
	    $array_msg['error_msg'] = "<ul>" . $error_msg . "</ul>";
	    return json_encode($array_msg);
	}
	/* If validation is true then upload document and save value in database */
	if ($validate == TRUE) {
	    /* Base path of corporate Documents */
	    $upload_docs_dir = Yii::getAlias('@important_documents');

	    /* Starts buisness_file Document Upload */
	    $buisness_tempFile = $buisness_file['tmp_name'];
	    $buisness_file_name = $buisness_file['name'];
	    $buisness_final_name = uniqid() . '_' . $buisness_file_name;
	    $buisness_targetFile = $upload_docs_dir . $ds . $buisness_final_name;
	    $buisness_file_upload = move_uploaded_file($buisness_tempFile, $buisness_targetFile);
	    /* End Starts buisness_file Document Upload */

	    /* Starts buisness_file Document Upload */
	    $incorporation_file_tempFile = $incorporation_file['tmp_name'];
	    $incorporation_file_name = $incorporation_file['name'];
	    $incorporation_final_name = uniqid() . '_' . $incorporation_file_name;
	    $incorporation_targetFile = $upload_docs_dir . $ds . $incorporation_final_name;
	    $incorporation_file_upload = move_uploaded_file($incorporation_file_tempFile, $incorporation_targetFile);
	    /* End Starts buisness_file Document Upload */

	    /* Save Value in corporate Documents */
	    $corporate_data = CorporateDocuments::find()->Where(['elliot_user_id' => $user_id, 'channel' => $channel_name])->one();
	    if (!empty($corporate_data)):

		$file_delete1 = unlink($basedir . '/' . $corporate_data->Business_License);
		$file_delete2 = unlink($basedir . '/' . $corporate_data->Business_Papers);

		$corporate_data->Business_License = 'img/important_documents/' . $buisness_final_name;
		$corporate_data->Business_Papers = 'img/important_documents/' . $incorporation_final_name;
		$corporate_data->channel = $channel_name;
		$corporate_data->Tax_ID = $tax_val;
		$corporate_data->save(false);

	    else:
		$CorporateDocuments = new CorporateDocuments();
		$CorporateDocuments->elliot_user_id = Yii::$app->user->identity->id;
		$CorporateDocuments->Business_License = 'img/important_documents/' . $buisness_final_name;
		$CorporateDocuments->Business_Papers = 'img/important_documents/' . $incorporation_final_name;
		$CorporateDocuments->channel = $channel_name;
		$CorporateDocuments->Tax_ID = $tax_val;
		$CorporateDocuments->created_at = date('Y-m-d h:i:s', time());
		$CorporateDocuments->save(false);
	    endif;

	    $array_msg['success_msg'] = "Document Upload SuccesFully";
	    return json_encode($array_msg);
	}
    }

    /**
     * Displays Translations Page.
     *
     * @return string
     */
    public function actionTranslations() {
	return $this->render('translations');
    }

    /**
     * Displays Orders Page.
     *
     * @return string
     */
    public function actionOrder() {
	return $this->render('order');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin() {
	$this->layout = "loginmain";

	if (!Yii::$app->user->isGuest) {
	    $uname = Yii::$app->user->identity->domain_name;
	    //use function get email domain

	    $role = Yii::$app->user->identity->role;
	    $parent_id = Yii::$app->user->identity->parent_id;
	    if ($role == 'merchant') {
		$uname = Yii::$app->user->identity->domain_name;
		$url = Yii::$app->params['PROTOCOL'] . $uname . '.' . Yii::$app->params['DOMAIN_NAME'];
		return Yii::$app->getResponse()->redirect($url);
	    } else {
		$user_parent_data = User::find()->Where(['id' => $parent_id])->one();
		$uname = $user_parent_data->domain_name;
		$url = Yii::$app->params['PROTOCOL'] . $uname . '.' . Yii::$app->params['DOMAIN_NAME'];
		return Yii::$app->getResponse()->redirect($url);
	    }
	} else {
	    $model = new LoginForm();
	    if ($model->load(Yii::$app->request->post()) && $model->login()) {

		$user_data = User::find()->Where(['id' => Yii::$app->user->identity->id])->one();
		$user_data->date_last_login = date('Y-m-d H:i:s');
		$parent_id = $user_data->parent_id;
		$role = $user_data->role;
		if ($user_data->save(false)) {
		    $session = new Session;
		    $session->open();
		    $session['wechat_import_start'] = 'yes';
		    if ($role == 'merchant' || $role == 'superadmin') {

			if ($role == 'superadmin') {
			    $curr_url = explode(".", "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
			    $uname = $curr_url[0];
			} else {
			    $uname = Yii::$app->user->identity->domain_name;
			}
			$url = Yii::$app->params['PROTOCOL'] . $uname . '.' . Yii::$app->params['DOMAIN_NAME'];
			/* This Flash is used used For show notification one time */
			$session = Yii::$app->session;
			$session->setFlash('insert', 'You have successfully registered.');

			return Yii::$app->getResponse()->redirect($url);
		    } else {
			$user_parent_data = User::find()->Where(['id' => $parent_id])->one();
			$uname = $user_parent_data->domain_name;
			$url = Yii::$app->params['PROTOCOL'] . $uname . '.' . Yii::$app->params['DOMAIN_NAME'];
			/* This Flash is used used For show notification one time */
			$session = Yii::$app->session;
			$session->setFlash('insert', 'You have successfully registered.');
			return Yii::$app->getResponse()->redirect($url);
		    }
		} else {
		    echo'<pre>';
		    print_r($user_data->getErrors());
		}
	    } else {
		return $this->render('login', [
			    'model' => $model,
		]);
	    }
	}
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup() {

	/* Use loginmain layout  */
	$this->layout = "loginmain";
	$datauser = Yii::$app->request->post();
	$model = new SignupForm();
	if ($model->load(Yii::$app->request->post())) {
	    $new_user_email = $model->email;
	    $company_name = $model->company_name;

	    //get email domain for new sub domain
	    //$new_user_username is a new email domain the name is not change because It's used in a lot of space.
	    $new_user_username = CustomFunction::getemaildomain($new_user_email);

	    $username = $model->username;
	    $password = $model->password;
	    if ($user = $model->signup()) {

		/* Sending For welcome email mail */
		$send_welcome_mail = CustomFunction::SendWelcomeEmail($new_user_email, $company_name);

		/* use the function for create main-local.php for each user */
		$create_main_local = CustomFunction::createMainLocal($new_user_username);

		$create_merchant_domain = CustomFunction::createMerchantDomain($new_user_username);

		if ($create_main_local == true && $create_merchant_domain == true) {
		    /* use the function for create new database for each merchant */
		    $db_create = CustomFunction::createdb($new_user_username);

		    if ($db_create == 'true') {
			/* use the function for import sql for create databse tables */
			$creat_db_tables = CustomFunction::creat_db_tables($new_user_username);
		    }

		    /* Insert the Signup Merchant Detail to Merchant DB User Table */
		    $merchant_config = CustomFunction::createMerchantDBConfig($new_user_username, $new_user_email);
		    $data = [
			'email' => $new_user_email,
			'status' => 'subscribed'
		    ];

		    $add_user_to_mailchimp = CustomFunction::syncMailchimp($data);


		    if (Yii::$app->getUser()->login($user)) {

			/* This Flash is used used For show notification one time */
			$session = Yii::$app->session;
			$session->setFlash('insert', 'You have successfully registered.');

			/* Redirect to Merchant Dashboard - Subdomain */
			/* get current user login id */
			$url = Yii::$app->params['PROTOCOL'] . $new_user_username . '.' . Yii::$app->params['DOMAIN_NAME'];
			return Yii::$app->getResponse()->redirect($url);
		    }
		}
	    }
	}

	return $this->render('signup', [
		    'model' => $model,
	]);
    }

    /**
     * Subscription payment action.
     *
     * @return string
     */
    public function actionSubscriptionpay() {
	/* get current user login id */
	$user_id = '1983';
	$user_domain = 'smart61';
	//use for base directory//

	$customer_token = 'cus_BY5DiKgNeZr2VE';

	echo $invoice_name = $user_id . '_' . $user_domain . '_' . $customer_token;

	//   $name = $request->post('name');
	$basedir = Yii::getAlias('@basedir');
	//use for baseurl//
	$baseurl = Yii::getAlias('@baseurl');
	$stripe_init_file = $basedir . '/stripe/init.php';

	require $stripe_init_file;
	//   $name = $request->post('name');
	\Stripe\Stripe::setApiKey("sk_test_2GPaBt5mT2k0yyTA9ho3CPIQ");
	try {


	    // Create a Customer:
	    /* $customer = \Stripe\Customer::create(array(
	      "email" => 'brstdev13@gmail.com',
	      "source" => $customer_token,
	      )); */

	    // Charge the Customer instead of the card:
	    $charge = \Stripe\Charge::create(array(
			"amount" => 1000,
			"currency" => "usd",
			"customer" => $customer_token
	    ));
	    echo $stripe_payment_Status = $charge->status;
	} catch (Exception $e) {
	    var_dump($e->getMessage());
	}
    }

    public function actionSubscriptionpaymentcustom() {

	$creditemail = $_POST['creditemail'];
	$creditcart = $_POST['creditcart'];
	$creditdate = $_POST['creditdate'];
	$monyear = explode('/', $creditdate);
	$creditcvc = $_POST['creditcvc'];

	/* get current user login id */
	$user_id = Yii::$app->user->identity->id;
	$user_domain = Yii::$app->user->identity->domain_name;
	//use for base directory//
	$basedir = Yii::getAlias('@basedir');
	//use for baseurl//
	$baseurl = Yii::getAlias('@baseurl');
	$stripe_init_file = $basedir . '/stripe/init.php';

	require $stripe_init_file;
	//   $name = $request->post('name');
	\Stripe\Stripe::setApiKey("sk_test_2GPaBt5mT2k0yyTA9ho3CPIQ");
	try {

	    $token = \Stripe\Token::create(array(
			"card" => array(
			    "number" => $creditcart,
			    "exp_month" => $monyear[0],
			    "exp_year" => $monyear[1],
			    "cvc" => $creditcvc
			)
	    ));
	    $customer = \Stripe\Customer::create(array(
			'email' => $creditemail,
			'source' => $token->id,
			'customer' => $user_domain . '_' . $user_id,
			"metadata" => array("order_id" => $user_id)
	    ));

	    //Charge Payment
	    $charge = \Stripe\Charge::create(array(
			"amount" => '100', // amount in cents, again
			"currency" => "usd",
			"customer" => $customer->id,
			"description" => $user_domain . '_' . $user_id,
			"metadata" => array("order_id" => $user_id)
	    ));
	    $customerId = $customer->id;
	    $users_Id = Yii::$app->user->identity->id;


	    $userData = User::find()->Where(['id' => $users_Id])->one();
	    $userData->customer_token = $customerId;
	    $userData->save(false);

	    $config = yii\helpers\ArrayHelper::merge(
			    require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php'));

	    $application = new yii\web\Application($config);

	    $Elli_user = User::find()->where(['id' => $users_Id])->one();
	    if (!empty($Elli_user)) {
		$Elli_user->customer_token = $customerId;
		$Elli_user->save(false);
	    }




	    return $stripe_payment_Status = $charge->status;
	}

	/* echo $token->id;
	  echo '<pre>'; print_r($customer); echo '</pre>';
	  echo '<pre>'; print_r($charge); */ catch (Exception $e) {
	    var_dump($e->getMessage());
	}

	//Create Customer
    }

    public function actionSubscriptionpayment() {

	$request = Yii::$app->request;
	/* get current user login id */
	$user_id = Yii::$app->user->identity->id;
	$user_domain = Yii::$app->user->identity->domain_name;
	//use for base directory//
	$basedir = Yii::getAlias('@basedir');
	//use for baseurl//
	$baseurl = Yii::getAlias('@baseurl');
	$stripe_init_file = $basedir . '/stripe/init.php';

	require $stripe_init_file;
	$name = $request->post('name');
	\Stripe\Stripe::setApiKey("sk_test_2GPaBt5mT2k0yyTA9ho3CPIQ");

	try {
	    //Create Customer
	    $customer = \Stripe\Customer::create(array(
			'email' => $request->post('stripeEmail'),
			'source' => $request->post('stripeToken'),
			'plan' => $request->post('plan_name'),
			'customer' => '12345',
			"metadata" => array("order_id" => "6735")
	    ));

	    //Charge Payment
	    $charge = \Stripe\Charge::create(array(
			"amount" => $request->post('amount'), // amount in cents, again
			"currency" => "usd",
			"customer" => $customer->id,
			"description" => $request->post('email'),
			"metadata" => array("order_id" => "6735")
	    ));

	    $stripe_payment_Status = $charge->status;
	    $stripe_plan_id = $charge->id;
	    if ($stripe_payment_Status == 'succeeded') {

		/* Starts Save Billing Invoice */
		$billing_invoice_model = new BillingInvoice();
		$billing_invoice_model->elliot_user_id = $user_id;
		$billing_invoice_model->stripe_id = $stripe_plan_id;
		$billing_invoice_model->customer_email = $customer->email;
		$billing_invoice_model->invoice_name = $request->post('plan_name');
		$billing_invoice_model->amount = $request->post('amount');
		$billing_invoice_model->status = $stripe_payment_Status;
		$billing_invoice_model->created_at = date('Y-m-d h:i:s', time());
		$billing_invoice_model->save(false);
		/* End Save Billing Invoice */


		$user_data = User::find()->where(['id' => $user_id])->one();
		$user_data->trial_period_status = CustomFunction::trial_status_deactivate;
		$user_data->subscription_plan_id = $request->post('plan_name');
		$user_data->plan_status = CustomFunction::plan_status_activate;


		$config = yii\helpers\ArrayHelper::merge(
				require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php'));


		$application = new yii\web\Application($config);


		//change user status acc to subscription plan
		$user_data_main = User::find()->where(['id' => $user_id])->one();
		$user_data_main->trial_period_status = CustomFunction::trial_status_deactivate;
		$user_data_main->subscription_plan_id = $request->post('plan_name');
		$user_data_main->plan_status = CustomFunction::plan_status_activate;

		$user_data_main->save(false);

		$get_big_id_users = User::find()->select('domain_name,company_name,email')->where(['id' => $user_id])->one();
		$config_one = yii\helpers\ArrayHelper::merge(
				require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_big_id_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php'));


		if ($user_data->save(false)) {
		    Yii::$app->session->setFlash('success', 'Success! ' . $request->post('plan_name') . ' Subscription Plan has been activated in both.');


		    /* get current user login id */

		    $url = Yii::$app->params['PROTOCOL'] . $user_domain . '.' . Yii::$app->params['DOMAIN_NAME'] . 'user-subscription';
		    // $url = 'user-subscription';
		    return Yii::$app->getResponse()->redirect($url);
		} else {
		    Yii::$app->session->setFlash('danger', 'Error! Subscription plan is not subscripe please contact Support.');
		}
	    }
	} catch (Exception $e) {

	    echo "Customer Creation Failed";
	    error_log("unable to sign up customer:" . $_POST['stripeEmail'] .
		    ", error:" . $e->getMessage());
	}
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout() {
	Yii::$app->user->logout();
	return $this->goHome();
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
	$this->findModel($id)->delete();

	return $this->redirect(['profile']);
    }

    /**
     * For Change Password
     */
    public function actionChangePassword() {

	$model = new PasswordForm;
	$modeluser = User::find()->where(['id' => Yii::$app->user->identity->id])->one();

	if ($model->load(Yii::$app->request->post())) {


	    if ($model->validate()) {




		$passHash = $model->setPassword($model->newpass);
		$authkey = $model->generateAuthKey();

		$modeluser->password_hash = $passHash;
		$modeluser->auth_key = $authkey;



		if ($modeluser->save(false)) {

		    $save_pass = CustomFunction::update_pass_main_db($passHash);
		    Yii::$app->session->setFlash('success', 'Password Change SuccessFully.');
		    return $this->render('changePassword', [
				'model' => $model
		    ]);
		}
	    } else {
		return $this->render('changePassword', [
			    'model' => $model
		]);
	    }
	} else {
	    return $this->render('changePassword', [
			'model' => $model
	    ]);
	}
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset() {
	$this->layout = "loginmain";
	$model = new PasswordResetRequestForm();
	if ($model->load(Yii::$app->request->post()) && $model->validate()) {

	    if ($model->sendEmail()) {

		Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

		return $this->goHome();
	    } else {
		Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
	    }
	}

	return $this->render('requestPasswordResetToken', [
		    'model' => $model,
	]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token) {

	$this->layout = "loginmain";
	try {
	    $model = new ResetPasswordForm($token);
	} catch (InvalidParamException $e) {
	    throw new BadRequestHttpException($e->getMessage());
	}

	if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
	    Yii::$app->session->setFlash('success', 'New password saved.');

	    return $this->goHome();
	}

	return $this->render('resetPassword', [
		    'model' => $model,
	]);
    }

    /**
     * Cron for checking User Account Status.
     */
    public function actionCronAccountStatus() {

	/* get All users data */
	$users_data = User::find()->All();
	foreach ($users_data as $user) {

	    $user_id = $user->id;

	    $user_create_date = $user->created_at;

	    $trial_days_data = TrialPeriod::find()->one();
	    $trial_period_days = $trial_days_data->trial_days;

	    /* convert trial period days to hour */
	    $trial_days_hours = $trial_period_days * 24;

	    /* get current date */
	    $current_date = date("Y-m-d h:i:s");

	    /* Date Diff */
	    $diff = ((strtotime($current_date) - strtotime($user_create_date)) / 3600);

	    /* Update User Trail Period Status */
	    if ($diff > $trial_days_hours) {
		$user->trial_period_status = 'deactivate';
		$user->save(false);
		echo 'User Id : ' . $user_id . ' - Trail Period has been expired.<br>';
	    }
	}
    }

    /**
     * Save account details through ajax.
     *
     * @return string
     */
    public function actionSaveprofile() {

	$request = Yii::$app->request;
	$userid = Yii::$app->user->identity->id;
	$emp_param = Yii::$app->params['emp_val'];

	if (Yii::$app->request->post()) {


	    $userdata = User::find()->Where(['id' => $userid])->one();
	    $userdata->first_name = (Yii::$app->request->post('profile_first_name') == $emp_param) ? '' : Yii::$app->request->post('profile_first_name');
	    $userdata->last_name = (Yii::$app->request->post('profile_last_name') == $emp_param) ? '' : Yii::$app->request->post('profile_last_name');
	    $userdata->email = (Yii::$app->request->post('profile_email_add') == $emp_param) ? '' : Yii::$app->request->post('profile_email_add');
	    $userdata->dob = (Yii::$app->request->post('profile_dob') == $emp_param) ? '' : Yii::$app->request->post('profile_dob');
	    $userdata->gender = Yii::$app->request->post('gender');
	    (Yii::$app->request->post('profile_email_add') == $emp_param) ? '' : Yii::$app->request->post('profile_email_add');
	    $userdata->phoneno = (Yii::$app->request->post('profile_Phone_no') == $emp_param) ? '' : Yii::$app->request->post('profile_Phone_no');
	    $userdata->timezone = (Yii::$app->request->post('profile_timezone') == $emp_param) ? '' : Yii::$app->request->post('profile_timezone');
	    $userdata->corporate_add_street1 = (Yii::$app->request->post('corporate_street1') == $emp_param) ? '' : Yii::$app->request->post('corporate_street1');
	    $userdata->corporate_add_street2 = (Yii::$app->request->post('corporate_street2') == $emp_param) ? '' : Yii::$app->request->post('corporate_street2');
	    $userdata->corporate_add_city = (Yii::$app->request->post('corporate_city') == $emp_param) ? '' : Yii::$app->request->post('corporate_city');
	    $userdata->corporate_add_state = (Yii::$app->request->post('corporate_state') == $emp_param) ? '' : Yii::$app->request->post('corporate_state');
	    $userdata->corporate_add_zipcode = Yii::$app->request->post('corporate_zip');
	    (Yii::$app->request->post('profile_email_add') == $emp_param) ? '' : Yii::$app->request->post('profile_email_add');
	    $userdata->corporate_add_country = (Yii::$app->request->post('corporate_country') == $emp_param) ? '' : Yii::$app->request->post('corporate_country');
	    $userdata->billing_address_street1 = (Yii::$app->request->post('ship_street1') == $emp_param) ? '' : Yii::$app->request->post('ship_street1');
	    $userdata->billing_address_street2 = (Yii::$app->request->post('ship_street2') == $emp_param) ? '' : Yii::$app->request->post('ship_street2');
	    $userdata->billing_address_city = (Yii::$app->request->post('ship_city') == $emp_param) ? '' : Yii::$app->request->post('ship_city');
	    $userdata->billing_address_state = (Yii::$app->request->post('ship_state') == $emp_param) ? '' : Yii::$app->request->post('ship_state');
	    $userdata->billing_address_zipcode = (Yii::$app->request->post('ship_zip') == $emp_param) ? '' : Yii::$app->request->post('ship_zip');
	    $userdata->billing_address_country = (Yii::$app->request->post('ship_country') == $emp_param) ? '' : Yii::$app->request->post('ship_country');


	    if ($userdata->save(false)) {
		$response = ['status' => 'success', 'data' => 'profile info saved'];
		Yii::$app->session->setFlash('success', 'Success! Profile info has been updated.');
	    } else {

		$response = ['status' => 'error', 'data' => 'profile info not saved'];
		Yii::$app->session->setFlash('danger', 'Error! Profile info has not been updated.');
	    }
	}
	echo json_encode($response);
	exit;
    }

    /**
     * Save General setting account details through ajax.
     *
     * @return string
     */
    public function actionSaveGeneralInfo() {

	$request = Yii::$app->request;
	$userid = Yii::$app->user->identity->id;
	if (Yii::$app->request->post()) {
	    $userdata = User::find()->Where(['id' => $userid])->one();
	    $userdata->email = Yii::$app->request->post('AccountOwner');
	    $userdata->corporate_add_street1 = Yii::$app->request->post('general_corporate_street1');
	    $userdata->corporate_add_street2 = Yii::$app->request->post('general_corporate_street2');
	    $userdata->general_phone_number = Yii::$app->request->post('general_phone_number');
	    $userdata->general_company = Yii::$app->request->post('general_company');
	    $userdata->corporate_add_country = Yii::$app->request->post('general_corporate_country');
	    $userdata->corporate_add_state = Yii::$app->request->post('general_corporate_state');
	    $userdata->corporate_add_city = Yii::$app->request->post('general_corporate_city');
	    $userdata->corporate_add_zipcode = Yii::$app->request->post('general_corporate_zip');
	    $userdata->billing_address_street1 = Yii::$app->request->post('subscription_billing_street1');
	    $userdata->billing_address_street2 = Yii::$app->request->post('subscription_billing_street2');
	    $userdata->billing_address_country = Yii::$app->request->post('subscription_billing_country');
	    $userdata->billing_address_state = Yii::$app->request->post('subscription_billing_state');
	    $userdata->billing_address_city = Yii::$app->request->post('subscription_billing_city');
	    $userdata->billing_address_zipcode = Yii::$app->request->post('subscription_billing_zip');
	    $userdata->tax_rate = Yii::$app->request->post('general_TaxRate');
	    $userdata->default_language = Yii::$app->request->post('general_Language');
	    $userdata->default_weight_preference = Yii::$app->request->post('general_WeightPreference');
	    $userdata->timezone = Yii::$app->request->post('general_Timezone');

	    if ($userdata->save(false)) {
		$response = ['status' => 'success', 'data' => 'General info saved'];
		Yii::$app->session->setFlash('success', 'Success! General info has been updated.');
	    } else {
		$response = ['status' => 'error', 'data' => 'profile info not saved'];
		Yii::$app->session->setFlash('danger', 'Error! General info has not been updated.');
	    }
	}
	echo json_encode($response);
	exit;
    }

    /**
     * Save General setting account details through ajax.
     *
     * @return string
     */
    public function actionSaveSmartlingApi() {

	$request = Yii::$app->request;
	$userid = Yii::$app->user->identity->id;
	if (Yii::$app->request->post()) {
	    $smartling = new Smartling();
	    $smartling->project_id = Yii::$app->request->post('smrt_project_id');
	    $smartling->sm_user_id = Yii::$app->request->post('sm_user_id');
	    $smartling->secret_key = Yii::$app->request->post('secret_key');
	    $smartling->created_at = date('Y-m-d h:i:s', time());

	    if ($smartling->save(false)) {
		$response = ['status' => 'success', 'data' => 'Smartling Api Saved'];
	    } else {
		$response = ['status' => 'error', 'data' => 'Smartling Api  not Saved'];
	    }
	}
	echo json_encode($response);
	exit;
    }

    public function actionIntegrateTransaltion() {

	return $this->render('smartlingTranslate');
    }

    /**
     * Export MErchant user Action.
     *
     * @return string
     */
    public function actionExportUser() {

	$user_id = Yii::$app->user->identity->id;
	$merchant_user = User::find()->where(['parent_id' => $user_id, 'role' => User::role_merchant_user])->all();


	$html = '<table>
                <tr>
                     <th>Name</th>
                     <th>User Role</th>
                     <th>Date Created</th>
                     <th>Date Last Logged In</th>
                  </tr>';

	foreach ($merchant_user as $users_data) {

	    $html .= '<tr>';
	    $html .= '<td>';
	    $html .= $users_data->first_name . '' . $users_data->last_name;
	    $html .= '</td>';


	    $html .= '<td>';
	    $html .= $users_data->role;
	    $html .= '</td>';

	    $html .= '<td>';
	    $html .= $users_data->created_at;
	    $html .= '</td>';

	    $html .= '<td>';
	    $html .= $users_data->date_last_login;
	    $html .= '</td>';
	}

	$html .= '</table>';


	$file = 'Users_Reports-' . date('d/m/Y') . '.xls';
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=$file");
	echo $html;
    }

    /**
     * Destroy a session.
     */
    public function actionDestroysession() {

	$session = Yii::$app->session;
	// destroys all data registered to a session.
	$session->destroy();
    }

    /* Action for Get Countries */

    public function actionGetCountries() {
	$query = $_REQUEST['query'];
	$sql = "SELECT name FROM countries WHERE name LIKE '{$query}%'";
	$countries = Countries::findBySql($sql)->all();
	$cat_array = array();
	foreach ($countries as $country_name) {
	    $country_array[] = $country_name->name;
	}
	//RETURN JSON ARRAY
	return json_encode($country_array);
    }

    /* ACTION for Get States */

    public function actionGetStates() {
	$query = $_REQUEST['query'];
	$sql = "SELECT name FROM states WHERE name LIKE '{$query}%'";
	$states = Countries::findBySql($sql)->all();
	$cat_array = array();
	foreach ($states as $states_name) {
	    $states_array[] = $states_name->name;
	}
	//RETURN JSON ARRAY
	return json_encode($states_array);
    }

    /* Action for Get States */

    public function actionGetCities() {
	$query = $_REQUEST['query'];
	$sql = "SELECT name FROM cities WHERE name LIKE '{$query}%'";
	$cities = Countries::findBySql($sql)->all();
	$cat_array = array();
	foreach ($cities as $cities_name) {
	    $cities_array[] = $cities_name->name;
	}
	//RETURN JSON ARRAY
	return json_encode($cities_array);
    }

    /* Action for Get languages */

    public function actionGetLanguages() {
	$query = $_REQUEST['query'];
	$sql = "SELECT name FROM languages WHERE name LIKE '{$query}%'";
	$languages = Languages::findBySql($sql)->all();
	$cat_array = array();
	foreach ($languages as $languages_name) {
	    $languages_array[] = $languages_name->name;
	}
	//RETURN JSON ARRAY
	return json_encode($languages_array);
    }

    /* Action for Get Timezone */

    public function actionGetTimezone() {
	$zones_array = array();
	$timestamp = time();
	foreach (timezone_identifiers_list() as $key => $zone) {
	    date_default_timezone_set($zone);
	    $zones_array[$key]['zone'] = $zone;
	    $zones_array[$key]['diff_from_GMT'] = 'UTC/GMT ' . date('P', $timestamp);
	}
	//$a="{'M': 'male', 'F': 'female'}";
	echo json_encode($zones_array);
    }

    /* save cover iamge action */

    public function actionSaveCoverImage() {
	$basedir = Yii::getAlias('@basedir');
	$basepath_cover_images = Yii::getAlias('@cover_images');
	$ds = DIRECTORY_SEPARATOR;
	$storeFolder = $basepath_cover_images;
	$tempFile = $_FILES['file']['tmp_name'];
	$imageName = $_FILES['file']['name'];
	$RandomImageId = uniqid();
	//Generate Unique Name for each Image Uploaded
	$new_imageName = $RandomImageId . '_' . $imageName;
	//Path Setting
	$targetPath = $storeFolder . $ds;
	$targetFile = $targetPath . $new_imageName;
	//Save the Uploaded File
	$imageFile = move_uploaded_file($tempFile, $targetFile);

	//Image Thumbnail 
	Image::thumbnail(Yii::getAlias('@cover_images/' . $new_imageName), 300, 300)
		->save(Yii::getAlias('@cover_images/thumbnails/thumb_' . $new_imageName), ['quality' => 100]);

	//Temp entry into Product_images Table 
	$userid = Yii::$app->user->identity->id;
	$userdata = User::find()->Where(['id' => $userid])->one();
	$userdata->cover_img = $new_imageName;
	if ($userdata->save(false)) {
	    $response = ['status' => 'success', 'data' => 'Cover image has been changed successfully'];
	    Yii::$app->session->setFlash('success', 'Success! Cover image has been updated.');
	} else {
	    $response = ['status' => 'success', 'data' => 'Failed to update Cover Image'];
	    Yii::$app->session->setFlash('danger', 'Error! Failed to update Cover Image.');
	}
	echo json_encode($response);
	exit;
    }

    /* save profile iamge action */

    public function actionSaveProfileImage() {
	$basedir = Yii::getAlias('@basedir');
	$basepath_profile_images = Yii::getAlias('@profile_images');
	$ds = DIRECTORY_SEPARATOR;
	$storeFolder = $basepath_profile_images;
	$tempFile = $_FILES['file']['tmp_name'];
	$imageName = $_FILES['file']['name'];
	$RandomImageId = uniqid();
	//Generate Unique Name for each Image Uploaded
	$new_imageName = $RandomImageId . '_' . $imageName;
	//Path Setting
	$targetPath = $storeFolder . $ds;
	$targetFile = $targetPath . $new_imageName;
	//Save the Uploaded File
	$imageFile = move_uploaded_file($tempFile, $targetFile);

	//Image Thumbnail 
	Image::thumbnail(Yii::getAlias('@profile_images/' . $new_imageName), 71, 71)
		->save(Yii::getAlias('@profile_images/thumbnails/thumb_' . $new_imageName), ['quality' => 100]);

	//Temp entry into Product_images Table 
	$userid = Yii::$app->user->identity->id;
	$userdata = User::find()->Where(['id' => $userid])->one();
	$userdata->profile_img = $new_imageName;
	if ($userdata->save(false)) {
	    $response = ['status' => 'success', 'data' => 'Profile image has been changed successfully'];
	    Yii::$app->session->setFlash('success', 'Success! Profile image has been updated.');
	} else {
	    $response = ['status' => 'success', 'data' => 'Failed to update Profile Image'];
	    Yii::$app->session->setFlash('danger', 'Error! Failed to update Profile Image.');
	}
	echo json_encode($response);
	exit;
    }

    //Action Show graph by order base
    public function actionDashboardGraphOrders() {
	$products_count = count(Products::find()->all());
	$user_id = Yii::$app->user->identity->id;

	$filtered_channel_connection_id = [];
	$filtered_store_connection_id = [];
	$connectedstore = [];
	$connectedchannel = [];
	$channel_name = '';
	if (isset($_POST['connection_id']) and ! empty($_POST['connection_id'])) {

	    if (isset($_POST['type']) and ! empty($_POST['type'])) {
		if ($_POST['type'] == 'store') {
		    $filtered_store_connection_id = $_POST['connection_id'];
		} elseif ($_POST['type'] == 'channel') {
		    $filtered_channel_connection_id = $_POST['connection_id'];
		    $channel_connection_data = ChannelConnection::find()->where(['channel_connection_id' => $filtered_channel_connection_id])->with('storesDetails')->one();
		    $channel_name = $channel_connection_data->storesDetails->channel_accquired;
		}
	    }
	}

	$order_ids = [];
	if (!empty($filtered_channel_connection_id)) {
	    $connectedchannel = Orders::find()->select('order_ID')->andFilterWhere([
			'and',
			    ['=', 'channel_accquired', $channel_name],
		    ])->andWhere(['elliot_user_id' => $user_id])->asArray()->all();
	    if (isset($connectedchannel) and ! empty($connectedchannel)) {
		foreach ($connectedchannel as $single) {
		    $order_ids[] = $single['order_ID'];
		}
	    }
	} elseif (!empty($filtered_store_connection_id)) {
	    $connectedstore = Orders::find()->select('order_ID')->andFilterWhere([
			'and',
			    ['=', 'mul_store_id', $filtered_store_connection_id],
		    ])->andWhere(['elliot_user_id' => $user_id])->asArray()->all();
	    if (isset($connectedstore) and ! empty($connectedstore)) {
		foreach ($connectedstore as $single) {
		    $order_ids[] = $single['order_ID'];
		}
	    }
	}
	$andWhere = 'andFilterWhere';
	if ((!empty($filtered_channel_connection_id) or ! empty($filtered_store_connection_id))) {
	    $andWhere = 'andWhere';
	    if (isset($order_ids) and ! empty($order_ids)) {
		$andWhere = 'andFilterWhere';
	    }
	}
	$order_ids_query = implode(",", $order_ids);
	//$order_ids_query="'".$order_ids_query."'";
	/* Show Data According to Month */
	$whole_revenue_amount = 0;
	if (Yii::$app->request->post('data') == 'month') :
	    //current date
	    $current_date = date('Y-m-d h:i:s', time());
	    //6 days ago date
	    $Week_previous_date = date('Y-m-d h:i:s', strtotime('-30 days'));
	    $current_m = date('Y-m');
	    $currentmonth = date('m');
	    $currentyear = date('Y');
	    $connection_db = \Yii::$app->db;
	    $orders_Data = $connection_db->createCommand('SELECT * from orders Where  month(created_at)="' . $currentmonth . '"AND year(created_at)="' . $currentyear . '" ');
	    if (isset($order_ids) and ! empty($order_ids)) {
		$orders_Data = $connection_db->createCommand('SELECT * from orders Where  month(created_at)="' . $currentmonth . '"AND year(created_at)="' . $currentyear . '" and order_ID in  (' . $order_ids_query . ')');
	    }
	    $orders_data = $orders_Data->queryAll();
	    $count_orders_data = count($orders_Data->queryAll());
	    /* new revnue earned */

	    $new_arr = array();
	    for ($j = 0; $j <= 30; $j++) {
		$previous_date = date('Y-m-d', strtotime("-" . $j . " days"));
		$check_month = date('Y-m', strtotime($previous_date));
		//get previous date
		if ($current_m == $check_month) {
		    $orders_data_current = Orders::find()
			    ->andWhere(['=', 'date(created_at)', $previous_date])
			    ->andWhere(['=', 'month(created_at)', $currentmonth])
			    ->$andWhere([
				'and',
				    ['in', 'order_ID', $order_ids],
			    ])
			    ->asArray()
			    ->all();
		    $single_count = 0;
		    if (isset($orders_data_current) and ! empty($orders_data_current)) {
			foreach ($orders_data_current as $single) {
			    $single_count += $single['total_amount'];
			}
		    }
		    $new_arr[] = array('count' => $single_count);
		    $whole_revenue_amount += $single_count;
		}
	    }

	    //for cornerdata percent
	    if ($count_orders_data != 0 && !empty($count_orders_data)):
		$Value = $count_orders_data * 100 / $products_count;
		$Value = number_format($Value, 0, '', ',');
	    else:
		$Value = 0;
	    endif;
	    $new_orders = array_column($new_arr, 'count');

	    $new_array = array('data' => $new_orders, 'ordercount' => '$' . number_format($whole_revenue_amount, 2), 'ordercountsales' => $Value);
	    return json_encode($new_array);
	endif;

	/* Show Data According to Week */
	if (Yii::$app->request->Post('data') == 'week'):
	    //current date
	    $current_date = date('Y-m-d h:i:s', time());
	    //6 days ago date
	    $Week_previous_date = date('Y-m-d h:i:s', strtotime('-7 days'));
	    $orders_data = Orders::find()->Where(['between', 'created_at', $Week_previous_date, $current_date])->$andWhere([
			'and',
			    ['in', 'order_ID', $order_ids],
		    ])->all();
	    $count_orders_data = count(Orders::find()->Where(['between', 'created_at', $Week_previous_date, $current_date])->$andWhere([
			'and',
			    ['in', 'order_ID', $order_ids],
		    ])->all());
	    $new_arr = array();

	    for ($j = 0; $j <= 6; $j++) {
		//get previous date
		$previous_date = date('Y-m-d', strtotime("-" . $j . " days"));

		$orders_data_current = Orders::find()
			->andWhere(['=', 'date(created_at)', $previous_date])
			->$andWhere([
			    'and',
				['in', 'order_ID', $order_ids],
			])
			->asArray()
			->all();
//                print_R($orders_data_current);

		$single_count = 0;
		if (isset($orders_data_current) and ! empty($orders_data_current)) {
		    foreach ($orders_data_current as $single) {
			$single_count += $single['total_amount'];
		    }
		}

		$new_arr[] = array('count' => $single_count);
		$whole_revenue_amount += $single_count;
	    }
	    //for cornerdata percent
	    if ($count_orders_data != 0 && !empty($count_orders_data)):
		$Value = $count_orders_data * 100 / $products_count;
		$Value = number_format($Value, 0, '', ',');
	    else:
		$Value = 0;
	    endif;
	    $new_orders = array_column($new_arr, 'count');
	    $new_array = array('data' => $new_orders, 'ordercount' => '$' . number_format($whole_revenue_amount, 2), 'ordercountsales' => $Value);
	    return json_encode($new_array);
	endif;


	/* Show Data According to Today */
	if (Yii::$app->request->post('data') == 'today'):
	    //get orders data acc to channel
	    $new_arr = array();
	    $current_date = date('Y-m-d', time());
	    $j = 0;
	    $new_arr = array();
	    for ($i = 1; $i <= 24; $i++) {

		if ($i == 1) {
		    $current_date_hour = date('Y-m-d h:i:s', time());
		} else {
		    $current_date_hour = date('Y-m-d h:i:s', strtotime('-' . $j . ' hour'));
		}
		$previous_hour = date('Y-m-d h:i:s', strtotime('-' . $i . ' hour'));
		$date_check = date('Y-m-d', strtotime($previous_hour));
		if ($date_check == $current_date) {
		    $orders_data_current = Orders::find()
			    ->andWhere(['between', 'created_at', $previous_hour, $current_date_hour])
			    ->$andWhere([
				'and',
				    ['in', 'order_ID', $order_ids],
			    ])
			    ->asArray()
			    ->all();
		    $single_count = 0;
		    if (isset($orders_data_current) and ! empty($orders_data_current)) {
			foreach ($orders_data_current as $single) {
			    $single_count += $single['total_amount'];
			}
		    }
		    $new_arr[] = array('count' => $single_count);
		    $whole_revenue_amount += $single_count;
		}
		$j++;
	    }
	    //for data count at the corner of the graph
	    $current_date_today = date('Y-m-d', time());
	    $connection = \Yii::$app->db;
	    $orders_data_query = $connection->createCommand('select * FROM orders WHERE date(created_at)="' . $current_date_today . '" ');
	    if (isset($order_ids) and ! empty($order_ids)) {
		$orders_data_query = $connection->createCommand('select * FROM orders WHERE date(created_at)="' . $current_date_today . '" and order_ID in  (' . $order_ids_query . ')');
	    }
	    $orders_data = $orders_data_query->queryAll();
	    $count_orders_data = count($orders_data);
	    if ($count_orders_data != 0 && !empty($count_orders_data)):
		$Value = $count_orders_data * 100 / $products_count;
		$Value = number_format($Value, 0, '', ',');
	    else:
		$Value = 0;
	    endif;
	    $new_orders = array_column($new_arr, 'count');
	    $new_orders_count = count($new_orders);
	    $new_array = array('data' => $new_orders, 'ordercount' => '$' . number_format($whole_revenue_amount, 2), 'ordercountsales' => $Value);
	    $i++;
	    return json_encode($new_array);
	endif;
	/* Show Data According to Year */
	if (Yii::$app->request->post('data') == 'year'):

	    $current_d = date('m');
	    $current_y = date('Y');
	    $month = (int) $current_d;
	    $new_arr = array();
	    for ($month; $month >= 1; $month--) {

		$orders_data_current = Orders::find()
			->andWhere(['=', 'month(created_at)', $month])
			->andWhere(['=', 'year(created_at)', $current_y])
			->$andWhere([
			    'and',
				['in', 'order_ID', $order_ids],
			])
			->asArray()
			->all();
		$single_count = 0;
		if (isset($orders_data_current) and ! empty($orders_data_current)) {
		    foreach ($orders_data_current as $single) {
			$single_count += $single['total_amount'];
		    }
		}
		$new_arr[] = array('count' => $single_count);
		$whole_revenue_amount += $single_count;
	    }
// for count data at the corner of the graph
	    $current_d = date('m');
	    $current_y = date('Y');
	    $month = (int) $current_d;
	    $connection = \Yii::$app->db;
	    $orders_data = $connection->createCommand('SELECT * from orders WHERE YEAR(created_at)="' . $current_y . '"  ');
	    if (isset($order_ids) and ! empty($order_ids)) {
		$orders_data = $connection->createCommand('SELECT * from orders WHERE YEAR(created_at)="' . $current_y . '"and order_ID in  (' . $order_ids_query . ')');
	    }
	    $orders_count = count($orders_data->queryAll());
	    if ($orders_count != 0 && !empty($orders_count)):
		$Value = $orders_count * 100 / $products_count;
		$Value = number_format($Value, 0, '', ',');
	    else:
		$Value = 0;
	    endif;
	    $new_orders = array_column($new_arr, 'count');
	    $new_orders_count = count($new_orders);
	    $new_array = array('data' => $new_orders, 'ordercount' => '$' . number_format($whole_revenue_amount, 2), 'ordercountsales' => $Value);
	    return json_encode($new_array);
	endif;

	if (Yii::$app->request->post('data') == 'quarter') {
	    $enddate = date('Y-m-d');
	    $current_date = date('Y-m-d', strtotime('-3 months'));
	    $datediff = strtotime($enddate) - strtotime($current_date);
	    $numOfdays = floor($datediff / (60 * 60 * 24));
	    $loop_val = floor($numOfdays / 7);
	    $prev_year = date('Y');
//                echo $loop_val;
	    $final_new_customer = [];
	    $final_repeat_customer = [];
	    for ($i = 1; $i <= $loop_val; $i++) {
		$days_array_current_year = [];
		$days_array_last_year = [];
		if ($i == 1) {
		    $date_current = date('W', strtotime($current_date));
		    $year_current = date('Y', strtotime($current_date));
		} else {
		    $date_current = date('W', strtotime('+' . $i * 7 . 'days', strtotime($current_date)));
		    $year_current = date('Y', strtotime('+' . $i * 7 . 'days', strtotime($current_date)));
		}

		$orders_data_current = Orders::find()
			->andWhere(['=', 'WEEK(created_at)', $date_current])
			->andWhere(['=', 'year(created_at)', $year_current])
			->$andWhere([
			    'and',
				['in', 'order_ID', $order_ids],
			])
			->asArray()
			->all();
		$single_count = 0;
		if (isset($orders_data_current) and ! empty($orders_data_current)) {
		    foreach ($orders_data_current as $single) {
			$single_count += $single['total_amount'];
		    }
		}
		$new_arr[] = array('count' => $single_count);
		$whole_revenue_amount += $single_count;
	    }

	    $connection = \Yii::$app->db;

	    $orders_data = $connection->createCommand('SELECT * from orders WHERE date(created_at) BETWEEN "' . $current_date . '" AND "' . $enddate . '"  ');
	    if (isset($order_ids) and ! empty($data)) {
		$orders_data = $connection->createCommand('SELECT * from orders WHERE date(created_at) BETWEEN "' . $current_date . '" AND "' . $enddate . '" and order_ID in  (' . $order_ids_query . ')');
	    }
	    $orders_count = count($orders_data->queryAll());
	    if ($orders_count != 0 && !empty($orders_count)):
		$Value = $orders_count * 100 / $products_count;
		$Value = number_format($Value, 0, '', ',');
	    else:
		$Value = 0;
	    endif;
	    $new_orders = array_column($new_arr, 'count');
	    $new_orders_count = count($new_orders);
	    $new_array = array('data' => $new_orders, 'ordercount' => '$' . number_format($whole_revenue_amount, 2), 'ordercountsales' => $Value);
	    return json_encode($new_array);
	}

	if (!empty($_POST['daterange'])) {
	    $daterange = $_POST['daterange'];
	}
	// for daterange
	if (Yii::$app->request->post('data') == 'dateRange' and ! empty($daterange)) {
	    $date = explode('-', $daterange);
	    $startdate = date('Y-m-d', strtotime($date[0]));
	    $enddate = date('Y-m-d', strtotime($date[1]));
	    $datediff = strtotime($enddate) - strtotime($startdate);
	    $numOfdays = floor($datediff / (60 * 60 * 24));
	    if ($numOfdays < 30) {

		$current_date = date('d');
		$prev_year = date('Y');

		for ($i = $numOfdays; $i >= 0; $i--) {
		    $days_array_current_year = [];
		    $days_array_last_year = [];
		    $date_current = date('Y-m-d', strtotime('-' . $i . ' days', strtotime($enddate)));



		    $orders_data_current = Orders::find()
			    ->andWhere(['=', 'date(created_at)', $date_current])
			    ->$andWhere([
				'and',
				    ['in', 'order_ID', $order_ids],
			    ])
			    ->asArray()
			    ->all();
		    $single_count = 0;
		    if (isset($orders_data_current) and ! empty($orders_data_current)) {
			foreach ($orders_data_current as $single) {
			    $single_count += $single['total_amount'];
			}
		    }
		    $new_arr[] = array('count' => $single_count);
		    $whole_revenue_amount += $single_count;
		}
	    }
	    if ($numOfdays > 30 && $numOfdays < 150) {
		$loop_val = floor($numOfdays / 7);
		$current_date = $startdate;
		for ($i = 1; $i <= $loop_val; $i++) {
		    if ($i == 1) {
			$date_current = date('W', strtotime($current_date));
			$year_current = date('Y', strtotime($current_date));
		    } else {
			$date_current = date('W', strtotime('+' . $i * 7 . 'days', strtotime($current_date)));
			$year_current = date('Y', strtotime('+' . $i * 7 . 'days', strtotime($current_date)));
		    }

		    $orders_data_current = Orders::find()
			    ->andWhere(['=', 'week(created_at)', $date_current])
			    ->andWhere(['=', 'year(created_at)', $year_current])
			    ->$andWhere([
				'and',
				    ['in', 'order_ID', $order_ids],
			    ])
			    ->asArray()
			    ->all();
		    $single_count = 0;
		    if (isset($orders_data_current) and ! empty($orders_data_current)) {
			foreach ($orders_data_current as $single) {
			    $single_count += $single['total_amount'];
			}
		    }
		    $new_arr[] = array('count' => $single_count);
		    $whole_revenue_amount += $single_count;
		}
	    }


	    $connection = \Yii::$app->db;

	    $orders_data = $connection->createCommand('SELECT * from orders WHERE date(created_at) BETWEEN "' . $startdate . '" AND "' . $enddate . '"  ');
	    if (isset($order_ids) and ! empty($data)) {
		$orders_data = $connection->createCommand('SELECT * from orders WHERE date(created_at) BETWEEN "' . $startdate . '" AND "' . $enddate . '" and order_ID in  (' . $order_ids_query . ')');
	    }
	    $orders_count = count($orders_data->queryAll());
	    if ($orders_count != 0 && !empty($orders_count)):
		$Value = $orders_count * 100 / $products_count;
		$Value = number_format($Value, 0, '', ',');
	    else:
		$Value = 0;
	    endif;
	    $new_orders = array_column($new_arr, 'count');
	    $new_orders_count = count($new_orders);
	    $new_array = array('data' => $new_orders, 'ordercount' => '$' . number_format($whole_revenue_amount, 2), 'ordercountsales' => $Value);
	    return json_encode($new_array);
	}
    }

    //Action Show graph for product Sold
    public function actionDashboardGraphProductSold() {
	$user_id = Yii::$app->user->identity->id;
	$filtered_channel_connection_id = [];
	$filtered_store_connection_id = [];
	$connectedstore = [];
	$connectedchannel = [];
	$channel_name = '';
	if (isset($_POST['connection_id']) and ! empty($_POST['connection_id'])) {

	    if (isset($_POST['type']) and ! empty($_POST['type'])) {
		if ($_POST['type'] == 'store') {
		    $filtered_store_connection_id = $_POST['connection_id'];
		} elseif ($_POST['type'] == 'channel') {
		    $filtered_channel_connection_id = $_POST['connection_id'];
		    $channel_connection_data = ChannelConnection::find()->where(['channel_connection_id' => $filtered_channel_connection_id])->with('storesDetails')->one();
		    $channel_name = $channel_connection_data->storesDetails->channel_accquired;
		}
	    }
	}

	$order_ids = [];
	if (!empty($filtered_channel_connection_id)) {
	    $connectedchannel = Orders::find()->select('order_ID')->andFilterWhere([
			'and',
			    ['=', 'channel_accquired', $channel_name],
		    ])->andWhere(['elliot_user_id' => $user_id])->asArray()->all();
	    if (isset($connectedchannel) and ! empty($connectedchannel)) {
		foreach ($connectedchannel as $single) {
		    $order_ids[] = $single['order_ID'];
		}
	    }
	} elseif (!empty($filtered_store_connection_id)) {
	    $connectedstore = Orders::find()->select('order_ID')->andFilterWhere([
			'and',
			    ['=', 'mul_store_id', $filtered_store_connection_id],
		    ])->andWhere(['elliot_user_id' => $user_id])->asArray()->all();
	    if (isset($connectedstore) and ! empty($connectedstore)) {
		foreach ($connectedstore as $single) {
		    $order_ids[] = $single['order_ID'];
		}
	    }
	}
	$andWhere = 'andFilterWhere';
	if ((!empty($filtered_channel_connection_id) or ! empty($filtered_store_connection_id))) {
	    $andWhere = 'andWhere';
	    if (isset($order_ids) and ! empty($order_ids)) {
		$andWhere = 'andFilterWhere';
	    }
	}
	$order_ids_query = implode(",", $order_ids);
	//$order_ids_query="'".$order_ids_query."'";
	/* Show Data According to Month */
	$whole_revenue_amount = 0;
	/* Show Data According to Week */

	if (Yii::$app->request->Post('data') == 'week'):
	    //current date
	    $current_date = date('Y-m-d h:i:s', time());
	    //6 days ago date
	    $Week_previous_date = date('Y-m-d h:i:s', strtotime('-7 days'));

	    $new_arr = array();

	    for ($j = 0; $j <= 6; $j++) {
		//get previous date
		$previous_date = date('Y-m-d', strtotime("-" . $j . " days"));

		$orders_data_current = OrdersProducts::find()
			->andWhere(['=', 'date(created_at)', $previous_date])
			->$andWhere([
			    'and',
				['in', 'order_Id', $order_ids],
			])
			->count();
		$single_count = 0;
		$single_count = $orders_data_current;

		$new_arr[] = array('count' => $single_count);
		$whole_revenue_amount += $single_count;
	    }
	    $new_orders = array_column($new_arr, 'count');
	    $new_array = array('data' => $new_orders, 'ordercount' => $whole_revenue_amount);
	    return json_encode($new_array);
	endif;

	if (Yii::$app->request->post('data') == 'month') :
	    //current date
	    $current_date = date('Y-m-d h:i:s', time());
	    //6 days ago date
	    $Week_previous_date = date('Y-m-d h:i:s', strtotime('-30 days'));
	    $currentmonth = date('m');
	    $current_m = date('Y-m');
	    $currentyear = date('Y');

	    /* new revnue earned */

	    $new_arr = array();
	    for ($j = 0; $j <= 30; $j++) {
		//get previous date
		$previous_date = date('Y-m-d', strtotime("-" . $j . " days"));
		$check_month = date('Y-m', strtotime($previous_date));

		if ($current_m == $check_month) {
		    $orders_data_current = OrdersProducts::find()
			    ->andWhere(['=', 'date(created_at)', $previous_date])
			    ->andWhere(['=', 'month(created_at)', $currentmonth])
			    ->$andWhere([
				'and',
				    ['in', 'order_Id', $order_ids],
			    ])
			    ->count();
		    $single_count = 0;
		    $single_count = $orders_data_current;
		    $new_arr[] = array('count' => $single_count);
		    $whole_revenue_amount += $single_count;
		}
	    }
	    $new_orders = array_column($new_arr, 'count');
	    $new_array = array('data' => $new_orders, 'ordercount' => $whole_revenue_amount);
	    return json_encode($new_array);
	endif;

	if (Yii::$app->request->post('data') == 'today'):
	    //get orders data acc to channel
	    $new_arr = array();
	    $current_date = date('Y-m-d', time());
	    $j = 0;
	    $new_arr = array();
	    for ($i = 1; $i <= 24; $i++) {

		if ($i == 1) {
		    $current_date_hour = date('Y-m-d h:i:s', time());
		} else {
		    $current_date_hour = date('Y-m-d h:i:s', strtotime('-' . $j . ' hour'));
		}
		$previous_hour = date('Y-m-d h:i:s', strtotime('-' . $i . ' hour'));
		$date_check = date('Y-m-d', strtotime($previous_hour));
		if ($date_check == $current_date) {
		    $orders_data_current = OrdersProducts::find()
			    ->andWhere(['between', 'created_at', $previous_hour, $current_date_hour])
			    ->$andWhere([
				'and',
				    ['in', 'order_Id', $order_ids],
			    ])
			    ->count();
		    $single_count = 0;
		    $single_count = $orders_data_current;
		    $new_arr[] = array('count' => $single_count);
		    $whole_revenue_amount += $single_count;
		}
		$j++;
	    }
	    //for data count at the corner of the graph
	    $new_orders = array_column($new_arr, 'count');
	    $new_orders_count = count($new_orders);
	    $new_array = array('data' => $new_orders, 'ordercount' => $whole_revenue_amount);
	    $i++;
	    return json_encode($new_array);
	endif;

	/* Show Data According to Year */

	if (Yii::$app->request->post('data') == 'year'):
	    $current_d = date('m');
	    $current_y = date('Y');
	    $month = (int) $current_d;
	    $new_arr = array();
	    for ($month; $month >= 1; $month--) {

		$orders_data_current = OrdersProducts::find()
			->andWhere(['=', 'month(created_at)', $month])
			->andWhere(['=', 'year(created_at)', $current_y])
			->$andWhere([
			    'and',
				['in', 'order_Id', $order_ids],
			])
			->count();
		$single_count = 0;
		$single_count = $orders_data_current;

		$new_arr[] = array('count' => $single_count);
		$whole_revenue_amount += $single_count;
	    }
	    // for count data at the corner of the graph

	    $new_orders = array_column($new_arr, 'count');
	    $new_orders_count = count($new_orders);
	    $new_array = array('data' => $new_orders, 'ordercount' => $whole_revenue_amount);
	    return json_encode($new_array);
	endif;

	if (Yii::$app->request->post('data') == 'quarter') {
	    $enddate = date('Y-m-d');
	    $current_date = date('Y-m-d', strtotime('-3 months'));
	    $datediff = strtotime($enddate) - strtotime($current_date);
	    $numOfdays = floor($datediff / (60 * 60 * 24));
	    $loop_val = floor($numOfdays / 7);
	    $prev_year = date('Y');
//                echo $loop_val;
	    $final_new_customer = [];
	    $final_repeat_customer = [];
	    for ($i = 1; $i <= $loop_val; $i++) {
		$days_array_current_year = [];
		$days_array_last_year = [];
		if ($i == 1) {
		    $date_current = date('W', strtotime($current_date));
		    $year_current = date('Y', strtotime($current_date));
		} else {
		    $date_current = date('W', strtotime('+' . $i * 7 . 'days', strtotime($current_date)));
		    $year_current = date('Y', strtotime('+' . $i * 7 . 'days', strtotime($current_date)));
		}

		$orders_data_current = OrdersProducts::find()
			->andWhere(['=', 'WEEK(created_at)', $date_current])
			->andWhere(['=', 'year(created_at)', $year_current])
			->$andWhere([
			    'and',
				['in', 'order_Id', $order_ids],
			])
			->count();
		$single_count = 0;
		$single_count = $orders_data_current;
		$new_arr[] = array('count' => $single_count);
		$whole_revenue_amount += $single_count;
	    }


	    $new_orders = array_column($new_arr, 'count');
	    $new_orders_count = count($new_orders);
	    $new_array = array('data' => $new_orders, 'ordercount' => $whole_revenue_amount);
	    return json_encode($new_array);
	}

	if (!empty($_POST['daterange'])) {
	    $daterange = $_POST['daterange'];
	}
	// for daterange
	if (Yii::$app->request->post('data') == 'dateRange' and ! empty($daterange)) {
	    $date = explode('-', $daterange);
	    $startdate = date('Y-m-d', strtotime($date[0]));
	    $enddate = date('Y-m-d', strtotime($date[1]));
	    $datediff = strtotime($enddate) - strtotime($startdate);
	    $numOfdays = floor($datediff / (60 * 60 * 24));
	    if ($numOfdays < 30) {

		$current_date = date('d');
		$prev_year = date('Y');

		for ($i = $numOfdays; $i >= 0; $i--) {
		    $days_array_current_year = [];
		    $days_array_last_year = [];
		    $date_current = date('Y-m-d', strtotime('-' . $i . ' days', strtotime($enddate)));



		    $orders_data_current = OrdersProducts::find()
			    ->andWhere(['=', 'date(created_at)', $date_current])
			    ->$andWhere([
				'and',
				    ['in', 'order_Id', $order_ids],
			    ])
			    ->count();
		    $single_count = 0;
		    $single_count = $orders_data_current;
		    $new_arr[] = array('count' => $single_count);
		    $whole_revenue_amount += $single_count;
		}
	    }
	    if ($numOfdays > 30 && $numOfdays < 150) {
		$loop_val = floor($numOfdays / 7);
		$current_date = $startdate;
		for ($i = 1; $i <= $loop_val; $i++) {
		    if ($i == 1) {
			$date_current = date('W', strtotime($current_date));
			$year_current = date('Y', strtotime($current_date));
		    } else {
			$date_current = date('W', strtotime('+' . $i * 7 . 'days', strtotime($current_date)));
			$year_current = date('Y', strtotime('+' . $i * 7 . 'days', strtotime($current_date)));
		    }

		    $orders_data_current = Orders::find()
			    ->andWhere(['=', 'week(created_at)', $date_current])
			    ->andWhere(['=', 'year(created_at)', $year_current])
			    ->$andWhere([
				'and',
				    ['in', 'order_Id', $order_ids],
			    ])
			    ->count();
		    $single_count = 0;
		    $single_count = $orders_data_current;
		    $new_arr[] = array('count' => $single_count);
		    $whole_revenue_amount += $single_count;
		}
	    }



	    $new_orders = array_column($new_arr, 'count');
	    $new_orders_count = count($new_orders);
	    $new_array = array('data' => $new_orders, 'ordercount' => $whole_revenue_amount);
	    return json_encode($new_array);
	}
    }

    //Action Show graph for Average Order Value
    public function actionDashboardGraphAverageOrderValue() {

	$user_id = Yii::$app->user->identity->id;


	$filtered_channel_connection_id = [];
	$filtered_store_connection_id = [];
	$connectedstore = [];
	$connectedchannel = [];
	$channel_name = '';
	if (isset($_POST['connection_id']) and ! empty($_POST['connection_id'])) {

	    if (isset($_POST['type']) and ! empty($_POST['type'])) {
		if ($_POST['type'] == 'store') {
		    $filtered_store_connection_id = $_POST['connection_id'];
		} elseif ($_POST['type'] == 'channel') {
		    $filtered_channel_connection_id = $_POST['connection_id'];
		    $channel_connection_data = ChannelConnection::find()->where(['channel_connection_id' => $filtered_channel_connection_id])->with('storesDetails')->one();
		    $channel_name = $channel_connection_data->storesDetails->channel_accquired;
		}
	    }
	}

	$order_ids = [];
	if (!empty($filtered_channel_connection_id)) {
	    $connectedchannel = Orders::find()->select('order_ID')->andFilterWhere([
			'and',
			    ['=', 'channel_accquired', $channel_name],
		    ])->andWhere(['elliot_user_id' => $user_id])->asArray()->all();
	    if (isset($connectedchannel) and ! empty($connectedchannel)) {
		foreach ($connectedchannel as $single) {
		    $order_ids[] = $single['order_ID'];
		}
	    }
	} elseif (!empty($filtered_store_connection_id)) {
	    $connectedstore = Orders::find()->select('order_ID')->andFilterWhere([
			'and',
			    ['=', 'mul_store_id', $filtered_store_connection_id],
		    ])->andWhere(['elliot_user_id' => $user_id])->asArray()->all();
	    if (isset($connectedstore) and ! empty($connectedstore)) {
		foreach ($connectedstore as $single) {
		    $order_ids[] = $single['order_ID'];
		}
	    }
	}
	$andWhere = 'andFilterWhere';
	if ((!empty($filtered_channel_connection_id) or ! empty($filtered_store_connection_id))) {
	    $andWhere = 'andWhere';
	    if (isset($order_ids) and ! empty($order_ids)) {
		$andWhere = 'andFilterWhere';
	    }
	}
	$order_ids_query = implode(",", $order_ids);
	//$order_ids_query="'".$order_ids_query."'";
	/* Show Data According to Month */
	$whole_revenue_amount = 0;

	if (Yii::$app->request->Post('data') == 'week'):
	    //current date
	    $current_date = date('Y-m-d h:i:s', time());
	    //6 days ago date
	    $Week_previous_date = date('Y-m-d h:i:s', strtotime('-7 days'));

	    $new_arr = array();

	    for ($j = 0; $j <= 6; $j++) {
		//get previous date

		if ($j == 0): $current_date = date('Y-m-d', time());
		else: $current_date = date('Y-m-d', strtotime('-' . $j . ' days'));
		endif;
		$connection = \Yii::$app->db;
		$orders_average_data = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE date(created_at) ="' . $current_date . '"');
		if (isset($order_ids) and ! empty($order_ids)) {
		    $orders_average_data = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE date(created_at) ="' . $current_date . '" and order_ID in  (' . $order_ids_query . ')');
		}

		$orders_average = $orders_average_data->queryAll();
		$count_orders_average = count($orders_average_data->queryAll());
		$new_arr[] = array('average1' => $orders_average);
	    }

	    $average_orders = array_column($new_arr, 'average1');
	    $average_o = array_column($average_orders, 0);
	    $new_avg = array();
	    foreach ($average_o as $avg):

		if (empty($avg['average'])):
		    $new_avg[] = $avg['average'] = 0;
		else:
		    $new_avg[] = number_format($avg['average'], 2);
		endif;

	    endforeach;
	    // for avg count corner of graph data
	    $current_date = date('Y-m-d h:i:s', time());
	    $month_previous_date_orders = date('Y-m-d h:i:s', strtotime('-6 days'));
	    $connection = \Yii::$app->db;
	    $orders_average_data = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE created_at BETWEEN "' . $month_previous_date_orders . '" AND "' . $current_date . '"');
	    if (isset($order_ids) and ! empty($order_ids)) {
		$orders_average_data = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE created_at BETWEEN "' . $month_previous_date_orders . '" AND "' . $current_date . '" and order_ID in  (' . $order_ids_query . ')');
	    }
	    $orders_average = $orders_average_data->queryAll();
	    if (!empty($orders_average)):
		$orders_average[0]['average'] = number_format($orders_average[0]['average'], 0, '', ',');
	    endif;
	    $new_orders_count = count($new_avg);
	    $new_array = array('data' => $new_avg, 'ordercount' => '$' . $orders_average[0]['average']);
	    return json_encode($new_array);
	endif;

	/* Show Data According to Month */
	if (Yii::$app->request->post('data') == 'month') :

	    $current_m = date('Y-m');
	    $current_month = date('m');
	    $current_year = date('Y');
	    $new_arr = array();
	    for ($i = 0; $i <= 30; $i++):
		if ($i == 0): $current_date = date('Y-m-d', time());
		else:
		    $current_date = date('Y-m-d', strtotime('-' . $i . ' days'));
		endif;

		$check_month = date('Y-m', strtotime($current_date));

		if ($current_m == $check_month) :

		    $connection = \Yii::$app->db;
		    $orders_average_data = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE date(created_at) = "' . $current_date . '" AND year(created_at)="' . $current_year . '"');
		    if (isset($order_ids) and ! empty($order_ids)) :
			$orders_average_data = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE date(created_at) = "' . $current_date . '" AND year(created_at)="' . $current_year . '" and order_ID in  (' . $order_ids_query . ')');
		    endif;
		    $orders_average = $orders_average_data->queryAll();
		    $count_orders_average = count($orders_average_data->queryAll());
		    $new_arr[] = array('average1' => $orders_average);
		endif;
	    endfor;

	    $average_orders = array_column($new_arr, 'average1');
	    $average_o = array_column($average_orders, 0);
	    $new_avg = array();
	    foreach ($average_o as $avg):
		if (empty($avg['average'])):
		    $new_avg[] = $avg['average'] = 0;
		else:
		    $new_avg[] = number_format($avg['average'], 2);
		endif;

	    endforeach;
	    //for avg data count 
	    $current_month = date('m');
	    $current_year = date('Y');
	    $month_previous_date_orders = date('Y-m-d h:i:s', strtotime('-30 days'));
	    $connection = \Yii::$app->db;
	    //$orders_average_data = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE created_at BETWEEN "' . $month_previous_date_orders . '" AND "' . $current_date . '"');
	    $orders_average_data = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE month(created_at) = "' . $current_month . '" AND year(created_at)="' . $current_year . '"');
	    if (isset($order_ids) and ! empty($order_ids)) {
		$orders_average_data = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE month(created_at) = "' . $current_month . '" AND year(created_at)="' . $current_year . '"and order_ID in  (' . $order_ids_query . ')');
	    }
	    $orders_average = $orders_average_data->queryAll();

	    if (!empty($orders_average)):
		$orders_average[0]['average'] = number_format($orders_average[0]['average'], 0, '', ',');
	    endif;

	    /* start for compare to previous month value */
//	    $last_month = date('ms', strtotime($month_previous_date_orders));
//	    $last_year = date('Y', strtotime($month_previous_date_orders));
//	    $orders_average_data_last_month = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE month(created_at) = "' . $last_month . '" AND year(created_at)="' . $last_year . '"');
//	    if (isset($order_ids) and ! empty($order_ids)) {
//		$orders_average_data_last_month = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE month(created_at) = "' . $last_month . '" AND year(created_at)="' . $last_year . '"and order_ID in  (' . $order_ids_query . ')');
//	    }
//	    $orders_average_last_month = $orders_average_data_last_month->queryAll();
//
//
//	    if (!empty($orders_average_last_month)):
//		$orders_average_last_month[0]['average'] = number_format($orders_average_last_month[0]['average'], 0, '', ',');
//	    endif;
//	    $up = false;
//	    if ($orders_average_last_month[0]['average'] <= $orders_average_last_month[0]['average']) {
//		$up = true;
//	    }
	    /* End  for compare to previous month value */

	    $new_orders_count = count($new_avg);
	    $new_array = array('data' => $new_avg, 'ordercount' => '$' . $orders_average[0]['average']);

	    return json_encode($new_array);

	endif;


	/* Show Data According to Today */
	if (Yii::$app->request->post('data') == 'today') :

	    //get orders data acc to channel
	    $new_arr = array();
	    $current_date = date('Y-m-d', time());
	    $j = 0;
	    $new_arr = array();
	    for ($i = 1; $i <= 24; $i++) {

		if ($i == 1) {
		    $current_date_hour = date('Y-m-d h:i:s', time());
		} else {
		    $current_date_hour = date('Y-m-d h:i:s', strtotime('-' . $j . ' hour'));
		}

		$previous_hour = date('Y-m-d h:i:s', strtotime('-' . $i . ' hour'));
		$date_check = date('Y-m-d', strtotime($previous_hour));
		if ($date_check == $current_date) {
		    $connection = \Yii::$app->db;
		    $orders_data_query = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE created_at BETWEEN ("' . $previous_hour . '") AND ("' . $current_date_hour . '") AND date(created_at)="' . $current_date . '" ');
		    if (isset($order_ids) and ! empty($order_ids)) {
			$orders_data_query = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE created_at BETWEEN ("' . $previous_hour . '") AND ("' . $current_date_hour . '") AND date(created_at)="' . $current_date . '" and order_ID in  (' . $order_ids_query . ')');
		    }
		    $orders_average = $orders_data_query->queryAll();
		    $new_arr[] = array('date' => $current_date_hour, 'average' => $orders_average);
		}

		$j++;
	    }

	    $new_orders = array_column($new_arr, 'average');
	    $average_o = array_column($new_orders, 0);
	    $new_avg = array();
	    foreach ($average_o as $avg):

		if (empty($avg['average'])):

		    $new_avg[] = $avg['average'] = 0;
		else:
		    $new_avg[] = number_format($avg['average'], 2);
		endif;

	    endforeach;
// for avg count corner of graph data
	    $current_date = date('Y-m-d', time());
	    $connection = \Yii::$app->db;
	    // $orders_data_query = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE date(created_at)="' . $current_date . '" GROUP BY created_at');
	    $orders_data_query = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE date(created_at)="' . $current_date . '" ');
	    if (isset($order_ids) and ! empty($order_ids)) {
		$orders_data_query = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE date(created_at)="' . $current_date . '" and order_ID in  (' . $order_ids_query . ')');
	    }
	    $orders_average = $orders_data_query->queryAll();
	    $count_orders_average = count($orders_average);
	    if (!empty($orders_average)):
		$orders_average[0]['average'] = number_format($orders_average[0]['average'], 0, '', ',');
	    endif;
	    $new_orders_count = count($new_avg);
	    $new_array = array('data' => $new_avg, 'ordercount' => '$' . $orders_average[0]['average']);
	    $i++;

	    return json_encode($new_array);

	endif;

	/* Show Data According to Year */
	if (Yii::$app->request->post('data') == 'year'):

	    $current_d = date('m');
	    $current_y = date('Y');
	    $month = (int) $current_d;
	    $new_arr = array();
	    for ($month; $month >= 1; $month--) {
		$connection = \Yii::$app->db;
		$orders_average_data = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE MONTH(created_at)="' . $month . '"  AND YEAR(created_at)="' . $current_y . '"');
		if (isset($order_ids) and ! empty($order_ids)) {
		    $orders_average_data = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE MONTH(created_at)="' . $month . '"  AND YEAR(created_at)="' . $current_y . '"and order_ID in  (' . $order_ids_query . ')');
		}
		$orders_average = $orders_average_data->queryAll();
		$count_orders_average = count($orders_average_data->queryAll());
		$new_arr[] = array('average1' => $orders_average);
	    }

	    $average_orders = array_column($new_arr, 'average1');

	    $average_o = array_column($average_orders, 0);
	    $new_avg = array();
	    foreach ($average_o as $avg):

		if (empty($avg['average'])):

		    $new_avg[] = $avg['average'] = 0;
		else:
		    $new_avg[] = number_format($avg['average'], 2);
		endif;

	    endforeach;
// for avg count corner of graph data

	    $current_d = date('m');
	    $current_y = date('Y');
	    $month = (int) $current_d;
	    $connection = \Yii::$app->db;
	    $orders_average_data = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE YEAR(created_at)="' . $current_y . '"');
	    if (isset($order_ids) and ! empty($order_ids)) {
		$orders_average_data = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE YEAR(created_at)="' . $current_y . '"and order_ID in  (' . $order_ids_query . ')');
	    }
	    $orders_average = $orders_average_data->queryAll();
	    if (!empty($orders_average)):
		$orders_average[0]['average'] = number_format($orders_average[0]['average'], 0, '', ',');
	    endif;

	    $new_orders_count = count($new_avg);
	    $new_array = array('data' => $new_avg, 'ordercount' => '$' . $orders_average[0]['average']);
	    return json_encode($new_array);

	endif;


	if (Yii::$app->request->post('data') == 'quarter') {
	    $enddate = date('Y-m-d');
	    $current_date = date('Y-m-d', strtotime('-3 months'));
	    $datediff = strtotime($enddate) - strtotime($current_date);
	    $numOfdays = floor($datediff / (60 * 60 * 24));
	    $loop_val = floor($numOfdays / 7);
	    $prev_year = date('Y');
//                echo $loop_val;
	    $final_new_customer = [];
	    $final_repeat_customer = [];
	    for ($i = 1; $i <= $loop_val; $i++) {
		$days_array_current_year = [];
		$days_array_last_year = [];
		if ($i == 1) {
		    $date_current = date('W', strtotime($current_date));
		    $year_current = date('Y', strtotime($current_date));
		} else {
		    $date_current = date('W', strtotime('+' . $i * 7 . 'days', strtotime($current_date)));
		    $year_current = date('Y', strtotime('+' . $i * 7 . 'days', strtotime($current_date)));
		}

		$connection = \Yii::$app->db;
		$orders_average_data = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE week(created_at)="' . $date_current . '"  AND YEAR(created_at)="' . $year_current . '"');
		if (isset($order_ids) and ! empty($order_ids)) {
		    $orders_average_data = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE week(created_at)="' . $date_current . '"  AND YEAR(created_at)="' . $year_current . '"and order_ID in  (' . $order_ids_query . ')');
		}
		$orders_average = $orders_average_data->queryAll();
		$count_orders_average = count($orders_average_data->queryAll());
		$new_arr[] = array('average1' => $orders_average);
	    }

	    $average_orders = array_column($new_arr, 'average1');
	    $average_o = array_column($average_orders, 0);
	    $new_avg = array();
	    foreach ($average_o as $avg):

		if (empty($avg['average'])):

		    $new_avg[] = $avg['average'] = 0;
		else:
		    $new_avg[] = number_format($avg['average'], 2);
		endif;

	    endforeach;
	    // for avg count corner of graph data

	    $current_d = date('m');
	    $current_y = date('Y');
	    $month = (int) $current_d;
	    $connection = \Yii::$app->db;
	    $orders_average_data = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE date(created_at)Between("' . $current_date . '") and ("' . $enddate . '")');

	    if (isset($order_ids) and ! empty($order_ids)) {
		$orders_average_data = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE date(created_at) Between ("' . $current_date . '") and ("' . $enddate . '") and order_ID in  (' . $order_ids_query . ')');
	    }
	    $orders_average = $orders_average_data->queryAll();
	    if (!empty($orders_average)):
		$orders_average[0]['average'] = number_format($orders_average[0]['average'], 0, '', ',');
	    endif;

	    $new_orders_count = count($new_avg);
	    $new_array = array('data' => $new_avg, 'ordercount' => '$' . $orders_average[0]['average']);
	    return json_encode($new_array);
	}

	if (!empty($_POST['daterange'])) {
	    $daterange = $_POST['daterange'];
	}
	// for daterange
	if (Yii::$app->request->post('data') == 'dateRange' and ! empty($daterange)) {
	    $date = explode('-', $daterange);
	    $startdate = date('Y-m-d', strtotime($date[0]));
	    $enddate = date('Y-m-d', strtotime($date[1]));
	    $datediff = strtotime($enddate) - strtotime($startdate);
	    $numOfdays = floor($datediff / (60 * 60 * 24));
	    if ($numOfdays < 30) {

		$current_date = date('d');
		$prev_year = date('Y');
		$current_y = date('Y');

		for ($i = $numOfdays; $i >= 0; $i--) {
		    $days_array_current_year = [];
		    $days_array_last_year = [];
		    $date_current = date('Y-m-d', strtotime('-' . $i . ' days', strtotime($enddate)));

		    $connection = \Yii::$app->db;
		    $orders_average_data = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE date(created_at)="' . $date_current . '"  AND YEAR(created_at)="' . $current_y . '"');
		    if (isset($order_ids) and ! empty($order_ids)) {
			$orders_average_data = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE date(created_at)="' . $date_current . '"  AND YEAR(created_at)="' . $current_y . '"and order_ID in  (' . $order_ids_query . ')');
		    }
		    $orders_average = $orders_average_data->queryAll();
		    $count_orders_average = count($orders_average_data->queryAll());
		    $new_arr[] = array('average1' => $orders_average);
		}

		$average_orders = array_column($new_arr, 'average1');

		$average_o = array_column($average_orders, 0);
		$new_avg = array();
		foreach ($average_o as $avg):

		    if (empty($avg['average'])):

			$new_avg[] = $avg['average'] = 0;
		    else:
			$new_avg[] = number_format($avg['average'], 2);
		    endif;

		endforeach;
// for avg count corner of graph data

		$current_d = date('m');
		$current_y = date('Y');
		$month = (int) $current_d;
		$connection = \Yii::$app->db;
		$orders_average_data = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE date(created_at)Between("' . $startdate . '") and ("' . $enddate . '")');

		if (isset($order_ids) and ! empty($order_ids)) {
		    $orders_average_data = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE date(created_at) Between ("' . $startdate . '") and ("' . $enddate . '") and order_ID in  (' . $order_ids_query . ')');
		}
		$orders_average = $orders_average_data->queryAll();
		if (!empty($orders_average)):
		    $orders_average[0]['average'] = number_format($orders_average[0]['average'], 0, '', ',');
		endif;

		$new_orders_count = count($new_avg);
		$new_array = array('data' => $new_avg, 'ordercount' => '$' . $orders_average[0]['average']);
		return json_encode($new_array);
	    }
	    if ($numOfdays > 30 && $numOfdays < 150) {
		$loop_val = floor($numOfdays / 7);
		$current_date = $startdate;
		for ($i = 1; $i <= $loop_val; $i++) {
		    if ($i == 1) {
			$date_current = date('W', strtotime($current_date));
			$year_current = date('Y', strtotime($current_date));
		    } else {
			$date_current = date('W', strtotime('+' . $i * 7 . 'days', strtotime($current_date)));
			$year_current = date('Y', strtotime('+' . $i * 7 . 'days', strtotime($current_date)));
		    }

		    $connection = \Yii::$app->db;
		    $orders_average_data = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE week(created_at)="' . $date_current . '"  AND YEAR(created_at)="' . $year_current . '"');
		    if (isset($order_ids) and ! empty($order_ids)) {
			$orders_average_data = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE week(created_at)="' . $date_current . '"  AND YEAR(created_at)="' . $year_current . '"and order_ID in  (' . $order_ids_query . ')');
		    }
		    $orders_average = $orders_average_data->queryAll();
		    $count_orders_average = count($orders_average_data->queryAll());
		    $new_arr[] = array('average1' => $orders_average);
		}

		$average_orders = array_column($new_arr, 'average1');

		$average_o = array_column($average_orders, 0);
		$new_avg = array();
		foreach ($average_o as $avg):

		    if (empty($avg['average'])):

			$new_avg[] = $avg['average'] = 0;
		    else:
			$new_avg[] = number_format($avg['average'], 2);
		    endif;

		endforeach;
// for avg count corner of graph data

		$current_d = date('m');
		$current_y = date('Y');
		$month = (int) $current_d;
		$connection = \Yii::$app->db;

		$orders_average_data = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE date(created_at)Between("' . $startdate . '") and ("' . $enddate . '")');

		if (isset($order_ids) and ! empty($order_ids)) {
		    $orders_average_data = $connection->createCommand('SELECT AVG(total_amount) as average FROM orders WHERE date(created_at) Between ("' . $startdate . '") and ("' . $enddate . '") and order_ID in  (' . $order_ids_query . ')');
		}
		$orders_average = $orders_average_data->queryAll();
		if (!empty($orders_average)):
		    $orders_average[0]['average'] = number_format($orders_average[0]['average'], 0, '', ',');
		endif;

		$new_orders_count = count($new_avg);
		$new_array = array('data' => $new_avg, 'ordercount' => '$' . $orders_average[0]['average']);
		return json_encode($new_array);
	    }
	}
    }

    /* For Customer View Graphs */


    /* Action Show graph by order base */

    public function actionCustomerGraphOrders() {

	$customer_id = Yii::$app->request->post('customer_view_id');

	$current_d = date('m');
	$current_y = date('Y');
	$month = (int) $current_d;
	$new_arr = array();
	for ($month; $month >= 1; $month--) {
	    $connection = \Yii::$app->db;
	    $orders_data = $connection->createCommand('SELECT * from orders Where customer_id="' . $customer_id . '" AND MONTH(created_at)="' . $month . '"  AND YEAR(created_at)="' . $current_y . '"  ');
	    $orders_count = count($orders_data->queryAll());
	    $new_arr[] = array('count' => $orders_count);
	}

	$new_orders = array_column($new_arr, 'count');
	$new_orders_count = count($new_orders);
	$new_array = array('data' => $new_orders, 'ordercount' => $new_orders_count);
	return json_encode($new_array);
    }

    /* Action Show graph by order base */

    public function actionCustomerGraphReturns() {

	$customer_id = Yii::$app->request->post('customer_view_id');
	$current_d = date('m');
	$current_y = date('Y');
	$month = (int) $current_d;
	$new_arr = array();
	for ($month; $month >= 1; $month--) {
	    $connection = \Yii::$app->db;
	    $orders_data = $connection->createCommand('SELECT * from orders  Where customer_id="' . $customer_id . '" AND  MONTH(created_at)="' . $month . '"  AND YEAR(created_at)="' . $current_y . '"  AND  order_status IN("Cancel","Refunded","Returned")');
	    $orders_count = count($orders_data->queryAll());
	    $new_arr[] = array('count' => $orders_count);
	}

	$new_orders = array_column($new_arr, 'count');
	$new_orders_count = count($new_orders);
	$new_array = array('data' => $new_orders, 'ordercount' => $new_orders_count);
	return json_encode($new_array);
    }

    /* Action Show graph by customer item purchase */

    public function actionCustomerGraphItemPurchase() {

	$customer_id = Yii::$app->request->post('customer_view_id');
	$current_d = date('m');
	$current_y = date('Y');
	$month = (int) $current_d;
//  $order_count_1='';
	$new_arr = array();
	for ($month; $month >= 1; $month--) {
	    $connection = \Yii::$app->db;
// $orders_data = $connection->createCommand('SELECT * from orders Where customer_id="' . $customer_id . '" AND  MONTH(created_at)="' . $month . '"  AND YEAR(created_at)="' . $current_y . '"  ');
	    $orders_data = $connection->createCommand('SELECT SUM(product_qauntity) as value from orders Where customer_id="' . $customer_id . '" AND MONTH(created_at)="' . $month . '"  AND YEAR(created_at)="' . $current_y . '" GROUP BY product_qauntity');
	    $orders_count = $orders_data->queryAll();
	    if (count($orders_count) > 0) {
		$order_count_1 = $orders_count[0]['value'];
	    } else {
		$order_count_1 = 0;
	    }
//$orders_count = count($orders_data->queryAll());

	    $new_arr[] = array('count' => $order_count_1);
	}



	$new_orders = array_column($new_arr, 'count');
	$new_orders_count = count($new_orders);
	$new_array = array('data' => $new_orders, 'ordercount' => $new_orders_count);
	return json_encode($new_array);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
	if (($model = User::findOne($id)) !== null) {
	    return $model;
	} else {
	    throw new NotFoundHttpException('The requested page does not exist.');
	}
    }

    /* Clears the Notification & Mark the status as Read */

    public function actionClearNotification() {
	$notif_id = $_POST['notif_id'];
	$get_notf = Notification::find()->Where(['id' => $notif_id])->one();
	if (!empty($get_notf)):
	    $get_notf->notif_status = 'Read';
	    $get_notf->save(false);
	endif;
	return 'success';
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionTestoauth() {

	Bigcommerce::configure(array(
	    'client_id' => 'j9cwuqcvr7px4e8husdzp2zz0dnj9wp',
	    'auth_token' => 'ivg4lm2owkjlu67ja8lb8hzq85s3470',
	    'store_hash' => '9c4sjk3b'
	));

	$ping = Bigcommerce::getTime();
    }

    public function actionSavecurrency() {
	if (isset($_POST) and ! empty($_POST) and isset($_POST['currency']) and ! empty($_POST['currency'])) {
	    $user = Yii::$app->user->identity;
	    $user->currency = $_POST['currency'];
	    $user->save(false);
	}


	$selected_currency = \backend\models\CurrencySymbols::find()->where(['name' => strtolower($user->currency)])->select(['id', 'symbol'])->asArray()->one();
	if (isset($selected_currency) and ! empty($selected_currency)) {
	    $currency_symbol = $selected_currency['symbol'];
	}

	$conversion_rate = 1;
	if (isset($user->currency) and $user->currency != 'USD') {
	    $username = Yii::$app->params['xe_account_id'];
	    $password = Yii::$app->params['xe_account_api_key'];
	    $URL = Yii::$app->params['xe_base_url'] . 'convert_from.json/?from=USD&to=' . $user->currency . '&amount=1';

	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $URL);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
	    curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
	    $result = curl_exec($ch);
	    $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
	    curl_close($ch);
//echo'<pre>';
	    $result = json_decode($result, true);
	    if (isset($result) and ! empty($result) and isset($result['to']) and isset($result['to'][0]) and isset($result['to'][0]['quotecurrency'])) {
		$conversion_rate = $result['to'][0]['mid'];
		$conversion_rate = number_format((float) $conversion_rate, 2, '.', '');
	    }
	}


	/* Save Currency conversion */
	if ($user->currency != '') {

	    $conversion_rate = Stores::getCurrencyConversionRate($user->currency, 'USD');
	    $currency_check = CurrencyConversion::find()->Where(['to_currency' => $user->currency])->one();
	    if (empty($currency_check)) {
		$store_currency_conversion = new CurrencyConversion();
		$store_currency_conversion->from_currency = 'USD';
		$store_currency_conversion->from_value = 1;
		$store_currency_conversion->to_currency = $user->currency;
		$store_currency_conversion->to_value = $conversion_rate;
		$store_currency_conversion->created_at = date('Y-m-d H:i:s', time());
		$store_currency_conversion->save(false);
	    } else {
		$currency_check->from_currency = 'USD';
		$currency_check->from_value = 1;
		$currency_check->to_currency = $user->currency;
		$currency_check->to_value = $conversion_rate;
		$currency_check->save(false);
	    }
	}


	$array = ['annual_revenue' => $user->annual_revenue * $conversion_rate, 'currency_symbol' => $currency_symbol];
	echo json_encode($array);
	die;
    }

    public function actionSavechannelcurrency() {
	if (isset($_POST) and ! empty($_POST) and isset($_POST['currency']) and ! empty($_POST['currency'])) {
	    $referer = $_SERVER['HTTP_REFERER'];
	    $data = substr($referer, strpos($referer, "=") + 1);
	    $data = explode('&', $data);
	    $id = $data[0];
	    $type = $data[1];
	    $type = substr($type, strpos($type, "=") + 1);
	    $user_id = Yii::$app->user->identity->id;
	    if (isset($data[2]) and ! empty($data[2])) {
		$mul_store_id = @$data[2];
		$mul_store_id = @substr($mul_store_id, strpos($mul_store_id, "=") + 1);
	    }
	    if (isset($id) and ! empty($id)) {
		if ($type == 'store') {
		    $result = Stores::find()->where(['store_id' => $id])->asArray()->one();
		} elseif ($type == 'channel') {
		    $result = Channels::find()->where(['channel_ID' => $id])->asArray()->one();
		}

		if (isset($result) and ! empty($result)) {
		    if (isset($result['store_id']) and ! empty($result['store_id'])) {
			$channel_setting = \backend\models\Channelsetting::find()->where(['store_id' => $id, 'user_id' => Yii::$app->user->identity->id, 'setting_key' => 'currency', 'mul_store_id' => $mul_store_id])->one();
			$store_connection_exists = StoresConnection::find()->where(['stores_connection_id' => $mul_store_id, 'user_id' => $user_id])->one();
			if (empty($store_connection_exists)) {
			    die('Not Allowed');
			}
			if (empty($channel_setting)) {
			    $channel_setting = new \backend\models\Channelsetting();
			}
			$channel_setting->store_id = $id;
			$channel_setting->user_id = Yii::$app->user->identity->id;
			$channel_setting->channel_name = $result['store_name'];
			$channel_setting->setting_key = 'currency';
			$channel_setting->setting_value = $_POST['currency'];
			$channel_setting->mul_store_id = $mul_store_id;
			$channel_setting->created_at = date('Y-m-d H:i:s');
			$channel_setting->save(false);
			echo'saved';
		    } elseif (isset($result['channel_ID']) and ! empty($result['channel_ID'])) {
			$channel_setting = \backend\models\Channelsetting::find()->where(['channel_id' => $id, 'user_id' => Yii::$app->user->identity->id, 'setting_key' => 'currency'])->one();

			if (empty($channel_setting)) {
			    $channel_setting = new \backend\models\Channelsetting();
			}
			$channel_setting->channel_id = $id;
			$channel_setting->user_id = Yii::$app->user->identity->id;
			$channel_setting->channel_name = (($result['parent_name'] == 'channel_WeChat') or ( $result['parent_name'] == 'channel_WeChat')) ? 'WeChat' : $result['channel_name'];
			$channel_setting->setting_key = 'currency';
			$channel_setting->setting_value = $_POST['currency'];
			$channel_setting->created_at = date('Y-m-d H:i:s');

			$channel_setting->save(false);
			echo'saved';
		    }
		}
	    }
	    die;
	}
    }

    public function actionSaverevenue() {
	if (isset($_POST) and ! empty($_POST) and isset($_POST['revenue'])) {
	    $user = Yii::$app->user->identity;
//            print_r($user);die;  
	    $value = $_POST['revenue'];
	    if (strpos($value, ',') !== false) {
		$value = str_replace(',', '', $_POST['revenue']);
	    } else {
		$value = $value;
	    }
//            if (preg_match("/^[0-9,]+$/", $value))
//                $value = str_replace(',', '', $_POST['revenue']);
	    $conversion_rate = 1;
	    if (isset($user->currency) and $user->currency != 'USD') {
		$username = Yii::$app->params['xe_account_id'];
		$password = Yii::$app->params['xe_account_api_key'];
		$URL = Yii::$app->params['xe_base_url'] . 'convert_from.json/?from=' . $user->currency . '&to=USD&amount=1';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $URL);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
		$result = curl_exec($ch);
		$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
		curl_close($ch);
//echo'<pre>';
		$result = json_decode($result, true);
//                    print_r($result);
		if (isset($result) and ! empty($result) and isset($result['to']) and isset($result['to'][0]) and isset($result['to'][0]['quotecurrency'])) {
		    $conversion_rate = $result['to'][0]['mid'];
		    $no_without_decimal = number_format(str_replace('.', '', $conversion_rate), 5, '', '');
		    if (strstr($conversion_rate, 'E-')) {
			$pos = strpos($conversion_rate, 'E-');
			$number_to_be_divided = substr($conversion_rate, $pos + 2);
			$output = substr($conversion_rate, 0, strrpos($conversion_rate, 'E-'));
			$string_pos = strpos($output, '.');
			$no_of_zeros = $number_to_be_divided - $string_pos;
			$zeros = '.';
			for ($i = 0; $i < $no_of_zeros; $i++) {
			    $zeros .= 0;
			}
			$conversion_rate = $zeros . $no_without_decimal;

//                        $conversion_rate=(float)$output/(pow(10,$number_to_be_divided));
		    }

		    $value = $value * $conversion_rate;
//                    $value = number_format((float) $value, 2, '.', '');
		}
	    }
	    $user->annual_revenue = $value;
	    $user->save(false);
	}
	die;
    }

    public function actionSaveorderrevenue() {
	if (isset($_POST) and ! empty($_POST) and isset($_POST['revenue'])) {
	    $user = Yii::$app->user->identity;
//            print_r($user);die;  
	    $value = $_POST['revenue'];
	    if (strpos($value, ',') !== false) {
		$value = str_replace(',', '', $_POST['revenue']);
	    } else {
		$value = $value;
	    }
//            if (preg_match("/^[0-9,]+$/", $value))
//                $value = str_replace(',', '', $_POST['revenue']);
	    $conversion_rate = 1;
	    if (isset($user->currency) and $user->currency != 'USD') {
		$username = Yii::$app->params['xe_account_id'];
		$password = Yii::$app->params['xe_account_api_key'];
		$URL = Yii::$app->params['xe_base_url'] . 'convert_from.json/?from=' . $user->currency . '&to=USD&amount=1';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $URL);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
		$result = curl_exec($ch);
		$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
		curl_close($ch);
//echo'<pre>';
		$result = json_decode($result, true);
//                    print_r($result);
		if (isset($result) and ! empty($result) and isset($result['to']) and isset($result['to'][0]) and isset($result['to'][0]['quotecurrency'])) {
		    $conversion_rate = $result['to'][0]['mid'];
		    $no_without_decimal = number_format(str_replace('.', '', $conversion_rate), 5, '', '');
		    if (strstr($conversion_rate, 'E-')) {
			$pos = strpos($conversion_rate, 'E-');
			$number_to_be_divided = substr($conversion_rate, $pos + 2);
			$output = substr($conversion_rate, 0, strrpos($conversion_rate, 'E-'));
			$string_pos = strpos($output, '.');
			$no_of_zeros = $number_to_be_divided - $string_pos;
			$zeros = '.';
			for ($i = 0; $i < $no_of_zeros; $i++) {
			    $zeros .= 0;
			}
			$conversion_rate = $zeros . $no_without_decimal;

//                        $conversion_rate=(float)$output/(pow(10,$number_to_be_divided));
		    }

		    $value = $value * $conversion_rate;
//                    $value = number_format((float) $value, 2, '.', '');
		}
	    }
	    $user->annual_order_target = $value;
	    $user->save(false);
	}
	die;
    }

    public function actionSingleProductGraph() {

	$graph_type = Yii::$app->request->post('id');
	$product_id = Yii::$app->request->post('product_id');
	//get user id
	$user_id = Yii::$app->user->identity->id;
	/*	 * *******************Starts Graph For Dashboard********************* */
	/* Show MAIN GRAPH Data According to Week */

	//current date
	$current_date = date('Y-m-d h:i:s', time());
	$Curr_date = date('Y-m-d h:i:s', strtotime('+1 days'));
	//6 days ago date
	$currentmonth = date('m');
	$currentyear = date('Y');
	//get store connection data
	$get_feed_channels = Channels::find()->select(['channel_ID'])->Where(['in', 'channel_name', ['Google shopping', 'Facebook']])->asArray()->all();
	$feed_ids = [];
	if (isset($get_feed_channels) and ! empty($get_feed_channels)) {
	    foreach ($get_feed_channels as $key => $value) {
		$feed_ids[] = $value['channel_ID'];
	    }
	}
	$store_connection_data = StoresConnection::find()->Where(['user_id' => $user_id])->with('stores')->all();
	$channels_data = ChannelConnection::find()->Where(['user_id' => $user_id])->where(['not in', 'channel_id', $feed_ids])->with('channels')->all();
	/* Define Blank array */

	$ch_arr = $arr = $stores_arr = $channel_arr = array();

	if (!empty($store_connection_data)) :
	    $i = 0;
	    foreach ($store_connection_data as $store_data) :
		$stores_arr[] = $store_data->stores->store_name;
		//get orders data acc to channel
	    endforeach;
	endif;

	if (!empty($channels_data)):
	    $i = 0;
	    foreach ($channels_data as $channels_d) :
		$channel_arr[] = $channels_d->channels[0]->channel_name;

	    endforeach;

	endif;
	//die;
	/* Data Merge Connected Store And  conneceted channel */
	$names = array_merge($stores_arr, $channel_arr);



	$connecteduser = array_merge($channels_data, $store_connection_data);
//echo"<pre>";
//print_r($connecteduser);die;
	if (!empty($connecteduser)) {
	    $arr = array();
	    $country = '';
	    foreach ($connecteduser as $connectedstore) {
		if (!empty($connectedstore->channel_id)) {
		    $storename = Channels::find()->where(['channel_ID' => $connectedstore->channel_id])->one();
		    $channel_id = $storename->channel_ID;
		    $name = $storename->channel_name;
		    if (($connectedstore->channels[0]->parent_name) == 'channel_WeChat') {
			$name = 'WeChat';
			$checkParent = 'parent_name';
		    } else {
			$checkParent = 'channel_name';
		    }
		} else if (!empty($connectedstore->store_id)) {

		    $storename = StoreDetails::find()->where(['store_connection_id' => $connectedstore->stores_connection_id])->with('storeConnection')->one();
		    $name = $storename->channel_accquired;
		    $country = $storename->country;
		    $store_id = $storename->storeConnection->store_id;
		}
//                echo $store_id.'<br>';
//                echo $store_id.'<br>';

		$fin_name = trim($name . ' ' . $country);

		if ($graph_type == 'day') {
		    $Week_previous_date = date('Y-m-d');
		    $connection = \Yii::$app->db;

		    if (!empty($connectedstore->channel_id)) {
			$orders_data = $connection->createCommand('SELECT * FROM orders_products join order_channel on orders_products.order_Id=order_channel.order_id WHERE  orders_products.product_Id="' . $product_id . '" AND date(orders_products.created_at) ="' . $Week_previous_date . '" and order_channel.channel_id="' . $channel_id . '"');
		    } else {
			$orders_data = $connection->createCommand('SELECT * FROM orders_products join order_channel on orders_products.order_Id=order_channel.order_id WHERE  orders_products.product_Id="' . $product_id . '" AND date(orders_products.created_at) ="' . $Week_previous_date . '" and order_channel.store_id="' . $connectedstore->stores_connection_id . '"');
		    }


//                    $orders_data = $connection->createCommand('SELECT * FROM orders_products join order_channel on orders_products.order_Id=order_channel.order_id WHERE  orders_products.product_Id="' . $product_id . '" AND date(orders_products.created_at) = "' . $Week_previous_date . '" AND ( order_channel.channel_id IN(SELECT channel_ID FROM channels WHERE channel_name="' . $name . '") OR order_channel.store_id in (SELECT store_id from stores WHERE store_name="' . $name . '")) ');
		    $orders_count = count($orders_data->queryAll());

		    $arr[] = array('name' => $fin_name, 'count' => $orders_count, 'date' => $Week_previous_date);
		}
		if ($graph_type == 'week') {
		    for ($j = 7; $j >= 1; $j--):
			$Week_previous_date = date('Y-m-d', strtotime('-' . $j . ' days'));
			$connection = \Yii::$app->db;


			if (!empty($connectedstore->channel_id)) {
			    $orders_data = $connection->createCommand('SELECT * FROM orders_products join order_channel on orders_products.order_Id=order_channel.order_id WHERE  orders_products.product_Id="' . $product_id . '" AND date(orders_products.created_at) ="' . $Week_previous_date . '" and order_channel.channel_id="' . $channel_id . '"');
			} else {
			    $orders_data = $connection->createCommand('SELECT * FROM orders_products join order_channel on orders_products.order_Id=order_channel.order_id WHERE  orders_products.product_Id="' . $product_id . '" AND date(orders_products.created_at) ="' . $Week_previous_date . '" and order_channel.store_id="' . $connectedstore->stores_connection_id . '"');
			}


//                        $orders_data = $connection->createCommand('SELECT * FROM orders_products join order_channel on orders_products.order_Id=order_channel.order_id WHERE  orders_products.product_Id="' . $product_id . '" AND date(orders_products.created_at) = "' . $Week_previous_date . '" AND ( order_channel.channel_id IN(SELECT channel_ID FROM channels WHERE channel_name="' . $name . '") OR order_channel.store_id in (SELECT store_id from stores WHERE store_name="' . $name . '")) ');
			$orders_count = count($orders_data->queryAll());

			$arr[] = array('name' => $fin_name, 'count' => $orders_count, 'date' => $Week_previous_date);
		    endfor;
		}
//die;
		/*		 * *********************** FOR MONTHLY************************ */
		if ($graph_type == 'month') {
		    $curr_year = date('Y');
		    for ($j = ($currentmonth - 1); $j >= 0; $j--):
			$Week_previous_date = date('m', strtotime('-' . $j . ' month'));
			$Week_pre_date = date('M', strtotime('-' . $j . ' month'));

			$connection = \Yii::$app->db;

			if (!empty($connectedstore->channel_id)) {
			    $orders_data = $connection->createCommand('SELECT * FROM orders_products join order_channel on orders_products.order_Id=order_channel.order_id WHERE  orders_products.product_Id="' . $product_id . '" AND month(orders_products.created_at) ="' . $Week_previous_date . '" AND year(orders_products.created_at) ="' . $curr_year . '" and order_channel.channel_id="' . $channel_id . '"');
			} else {
			    $orders_data = $connection->createCommand('SELECT * FROM orders_products join order_channel on orders_products.order_Id=order_channel.order_id WHERE  orders_products.product_Id="' . $product_id . '" AND month(orders_products.created_at) ="' . $Week_previous_date . '" AND year(orders_products.created_at) ="' . $curr_year . '" and order_channel.store_id="' . $connectedstore->stores_connection_id . '"');
			}

//                        $orders_data = $connection->createCommand('SELECT * FROM orders_products join order_channel on orders_products.order_Id=order_channel.order_id WHERE  orders_products.product_Id="' . $product_id . '" AND month(orders_products.created_at) = "' . $Week_previous_date . '" AND ( order_channel.channel_id IN(SELECT channel_ID FROM channels WHERE channel_name="' . $name . '") OR order_channel.store_id in (SELECT store_id from stores WHERE store_name="' . $name . '"))');

			$orders_count = count($orders_data->queryAll());


			$arr[] = array('name' => $fin_name, 'count' => $orders_count, 'date' => $Week_pre_date . ' ' . $currentyear);
		    endfor;
		}


		if ($graph_type == 'quarter') {
		    $iterating_month_number = 9;
		    $past_year_date = date('Y-m-d', strtotime('-1 year'));
//                    echo $past_year_date;die;
		    for ($j = 12; $j >= 3; $j -= 3):
			$current_month = date('m', strtotime('-' . $iterating_month_number . ' month'));
			$current_month_text = date('M', strtotime('-' . $iterating_month_number . ' month'));
			$current_month_text_year = date('Y', strtotime('-' . $iterating_month_number . ' month'));

			$Week_previous_date = date('m', strtotime('-' . $j . ' month'));
			$Week_pre_date_text = date('M', strtotime('-' . $j + 1 . ' month'));
			$Week_pre_date_text_year = date('Y', strtotime('-' . $j . ' month'));
			$connection = \Yii::$app->db;

			if (!empty($connectedstore->channel_id)) {
			    $orders_data = $connection->createCommand('SELECT * FROM orders_products join order_channel on orders_products.order_Id=order_channel.order_id WHERE  orders_products.product_Id="' . $product_id . '" AND (month(orders_products.created_at) > "' . $Week_previous_date . '" and  month(orders_products.created_at) <="' . $current_month . '") AND  year(orders_products.created_at)="' . $currentyear . '" and order_channel.channel_id="' . $channel_id . '"');
			} else {
			    $orders_data = $connection->createCommand('SELECT * FROM orders_products join order_channel on orders_products.order_Id=order_channel.order_id WHERE  orders_products.product_Id="' . $product_id . '" AND (month(orders_products.created_at) > "' . $Week_previous_date . '" and  month(orders_products.created_at) <="' . $current_month . '") AND  year(orders_products.created_at)="' . $currentyear . '" and order_channel.store_id="' . $connectedstore->stores_connection_id . '"');
			}



//                        $orders_data = $connection->createCommand('SELECT * FROM orders_products join order_channel on orders_products.order_Id=order_channel.order_id WHERE  orders_ated_at) <="' . $current_month . '") AND  year(orders_products.created_at)="' . $currentyear . '" and ( order_channel.channel_id IN(SELECT channel_ID FROM channels WHERE channel_name="' . $name . '") OR order_channel.store_id in (SELECT store_id from stores WHERE store_name="' . $name . 'products.product_Id="' . $product_id . '" AND (month(orders_products.created_at) > "' . $Week_previous_date . '" and  month(orders_products.cre"))');
//echo 'SELECT * FROM orders_products join order_channel on orders_products.order_Id=order_channel.order_id WHERE  orders_products.product_Id="' . $product_id . '" AND (month(orders_products.created_at) > "' . $Week_previous_date . '" and  month(orders_products.created_at) <="' . $current_month . '") AND ( order_channel.channel_id IN(SELECT channel_ID FROM channels WHERE channel_name="' . $name . '") OR order_channel.store_id in (SELECT store_id from stores WHERE store_name="' . $name . '"))';
//echo "<br>";
			$orders_count = count($orders_data->queryAll());

			$arr[] = array('name' => $fin_name, 'count' => $orders_count, 'date' => $Week_pre_date_text . ' ' . $Week_pre_date_text_year . ' to ' . $current_month_text . ' ' . $current_month_text_year);
			$iterating_month_number = $iterating_month_number - 3;
		    endfor;
		}
		/*		 * *********************** FOR YEARLY************************ */
		if ($graph_type == 'year') {
		    for ($j = 9; $j >= 0; $j--):
			$Week_previous_date = date('Y', strtotime('-' . $j . ' years'));
			$connection = \Yii::$app->db;
//                        $orders_data = $connection->createCommand('SELECT * FROM orders_products join order_channel on orders_products.order_Id=order_channel.order_id WHERE  orders_products.product_Id="' . $product_id . '" AND year(orders_products.created_at) = "' . $Week_previous_date . '"');
			if (!empty($connectedstore->channel_id)) {
			    $orders_data = $connection->createCommand('SELECT * FROM orders_products join order_channel on orders_products.order_Id=order_channel.order_id WHERE  orders_products.product_Id="' . $product_id . '" AND year(orders_products.created_at) ="' . $Week_previous_date . '" and order_channel.channel_id="' . $channel_id . '"');
			} else {
			    $orders_data = $connection->createCommand('SELECT * FROM orders_products join order_channel on orders_products.order_Id=order_channel.order_id WHERE  orders_products.product_Id="' . $product_id . '" AND year(orders_products.created_at) ="' . $Week_previous_date . '" and order_channel.store_id="' . $connectedstore->stores_connection_id . '"');
			}




//      $connection = \Yii::$app->db;
//    $orders_data = $connection->createCommand('SELECT * FROM orders_products join order_channel on orders_products.order_Id=order_channel.order_id WHERE orders_products.product_Id=' . $product_id . ' AND year(orders_products.created_at) = ' . $Week_previous_date . ' AND (SELECT channel_ID FROM channels WHERE channel_ID=' . $channel_id . ') OR(SELECT store_id from stores WHERE store_id=' . $store_id . ')');
			//  $orders_count = count($orders_data->queryAll());
//                         $orders_data = $connection->createCommand('SELECT * FROM orders_products join order_channel on orders_products.order_Id=order_channel.order_id WHERE  orders_products.product_Id="' . $product_id . '" AND year(orders_products.created_at) = "' . $Week_previous_date . '"');


			$orders_count = count($orders_data->queryAll());

			$arr[] = array('name' => $fin_name, 'count' => $orders_count, 'date' => $Week_previous_date);
		    endfor;
		}
	    }
	}
//          echo'<pre>';          print_r($arr);die;
//        if ($graph_type == 'year') {
//            
//            echo'<pre>';
//            print_r($arr);
//            die('end here');
//        }

	$array1 = array();
	foreach ($arr as $a):
	    /* Check if key exists or not */
	    $dates = $a['date'];
	    if (array_key_exists($dates, $array1)) {
		$array1[$dates][] = array(
		    'name' => $a['name'],
		    'count' => $a['count'],
		);
	    } else {
		$array1[$dates][] = array(
		    'name' => $a['name'],
		    'count' => $a['count'],
		);
	    }
	endforeach;
	//print_r($array1);die;

	$data_val_2 = array();
	$data_val = array();
	foreach ($array1 as $_data => $value) {
	    foreach ($value as $_value) {
		$data_val['period'] = $_data;
		$data_val[$_value['name']] = $_value['count'];
	    }
	    $data_val_2[] = $data_val;
	}
	$keyname = array();
	foreach ($data_val as $key => $val) {
	    if ($key != 'period') {
		$keyname[] = $key;
	    }
	}
	//print_r($data_val_2);die;

	$colorarr = array(
	    0 => '#0091ea',
	    1 => '#00b0ff',
	    2 => '#40c4ff',
	    3 => '#80d8ff',
	    4 => '#01579b',
	    5 => '#0277bd',
	    6 => '#039be5',
	    7 => '#03a9f4',
	    8 => '#b3e5fc',
	    9 => '#81d4fa',
	    10 => '#29b6f6',
	    11 => '#e1f5fe',
	    12 => '#b5af79',
	    13 => '#914f84',
	    14 => '#ce6d87',
	    15 => '#e04a72',
	);
	$randcolor = array();
	$c = 0;
	$cc = 0;
	$showcolorhtml = '';
	foreach ($keyname as $color) {
// $randcolor[] = '#' . random_color();
	    $randcolor[] = $colorarr[$c++];
	    // $strname = explode(' ', $color);
	    $showcolorhtml .= '<li><span data-color="main-chart-color" class="BigCommerce connected_store_name" style="background-color: ' . $colorarr[$cc++] . ';"></span>' . $color . '</li>';
	}
	$data = array('data' => json_encode($data_val_2), 'ykeyslabels' => json_encode($keyname), 'linecolors' => json_encode($randcolor), 'store_channel_names' => $names, 'showcolorhtml' => $showcolorhtml);
	return \yii\helpers\Json::encode($data);
    }

    /*
     * Action Show Connected Stores In dashboard
     * ajax hit from custom_graph.js 
     */

    //  public function actionAreachartondashboard() {
    public function actionAreachartondashboard() {

	$user_id = Yii::$app->user->identity->id;
	if (!empty($_POST['data'])) {
	    $post = $_POST['data'];
	}
	$currentmonth = date('m');
	$currentyear = date('Y');
	$current_date = date('Y-m-d', time());
	$filtered_channel_connection_id = [];
	$filtered_store_connection_id = [];
	$connectedstore = [];
	$connectedchannel = [];
	if (isset($_POST['connection_id']) and ! empty($_POST['connection_id'])) {

	    if (isset($_POST['type']) and ! empty($_POST['type'])) {
		if ($_POST['type'] == 'store') {
		    $filtered_store_connection_id = $_POST['connection_id'];
		} elseif ($_POST['type'] == 'channel') {
		    $filtered_channel_connection_id = $_POST['connection_id'];
		}
	    }
	}

	$connectedstore = StoresConnection::find()->andFilterWhere([
		    'and',
			['in', 'stores_connection_id', $filtered_store_connection_id],
		])->andWhere(['user_id' => $user_id])->all();




	$connectedchannel = ChannelConnection::find()->where(['user_id' => $user_id])->andFilterWhere([
		    'and',
			['in', 'channel_connection_id', $filtered_channel_connection_id],
		])->andWhere(['user_id' => $user_id])->all();

	if (empty($filtered_store_connection_id) and empty($filtered_channel_connection_id)) {
	    $connecteduser = array_merge($connectedchannel, $connectedstore);
	} elseif (!empty($filtered_store_connection_id) and empty($filtered_channel_connection_id)) {
	    $connecteduser = $connectedstore;
	} elseif (empty($filtered_store_connection_id) and ! empty($filtered_channel_connection_id)) {
	    $connecteduser = $connectedchannel;
	}

	if (!empty($connecteduser)) {
	    $arr = array();
	    $today = array();
	    foreach ($connecteduser as $connectedstore) {
		if (!empty($connectedstore->channel_id)) {
		    $channelname = Channels::find()->where(['channel_ID' => $connectedstore->channel_id])->one();
		    $fieldname = 'channel_id';
		    if (($channelname->channel_name) == 'Facebook' || ($channelname->channel_name) == 'Google shopping') {
			continue;
		    } elseif (($channelname->channel_name) == 'Service Account' || ($channelname->channel_name) == 'Subscription Account') {
			$name = 'WeChat';
			$store_channel_id = $connectedstore->channel_id;
		    } else {
			$store_channel_id = $connectedstore->channel_id;
			$name = $channelname->channel_name;
		    }
		} elseif (!empty($connectedstore->store_id)) {
		    $fieldname = 'store_id';
		    $storename = StoresConnection::find()->where(['stores_connection_id' => $connectedstore->stores_connection_id])->with('storesDetails')->one();
		    $store_channel_id = $connectedstore->stores_connection_id;

		    $name = $storename->storesDetails->channel_accquired;
		    $country = $storename->storesDetails->country;
		    $name = trim($name . ' ' . $country);
		}

		/*		 * *********************** FOR WEEKLY************************ */
		if ($post == 'dashboardchartweek' || $post == 'dashboardchartweekmob') {
		    for ($j = 7; $j >= 1; $j--):
			$Week_previous_date = date('Y-m-d', strtotime('-' . $j . ' days'));
			$connection = \Yii::$app->db;
			$orders_data = $connection->createCommand('SELECT * from order_channel Where ' . $fieldname . '="' . $store_channel_id . '" AND date(created_at)="' . $Week_previous_date . '"  ');
			$orders_count = count($orders_data->queryAll());
			$arr[] = array('name' => $name, 'user' => $orders_count, 'date' => $Week_previous_date);
		    endfor;
		}

		/*		 * *********************** FOR MONTHLY************************ */
		if ($post == 'dashboardchartmonth' || $post == 'dashboardchartmonthmob') {
		    for ($j = ($currentmonth - 1); $j >= 0; $j--):
			$Week_previous_date = date('m', strtotime('-' . $j . ' month'));
			$Week_pre_date = date('M', strtotime('-' . $j . ' month'));
			$connection = \Yii::$app->db;
			$orders_data = $connection->createCommand('SELECT * from order_channel Where ' . $fieldname . '="' . $store_channel_id . '" AND month(created_at)="' . $Week_previous_date . '" AND year(created_at)="' . $currentyear . '"');
			$orders_count = count($orders_data->queryAll());

			$arr[] = array('name' => $name, 'user' => $orders_count, 'date' => $Week_pre_date . ' ' . $currentyear);
// $arr[] = array('name' => $name, 'user' => $orders_count, 'date' => $Week_pre_date);
		    endfor;
		}
		/*		 * *********************** FOR YEARLY************************ */
		if ($post == 'dashboardchartyear' || $post == 'dashboardchartyearmob') {
		    for ($j = 3; $j >= 0; $j--):
			$Week_previous_date = date('Y', strtotime('-' . $j . ' years'));
			$connection = \Yii::$app->db;
			$orders_data = $connection->createCommand('SELECT * from order_channel Where ' . $fieldname . '="' . $store_channel_id . '" AND year(created_at)="' . $Week_previous_date . '"  ');
			$orders_count = count($orders_data->queryAll());
			$arr[] = array('name' => $name, 'user' => $orders_count, 'date' => $Week_previous_date);
		    endfor;
		}
		/*		 * *********************** FOR TODAY************************ */
		if ($post == 'dashboardcharttoday' || $post == 'dashboardcharttodaymob') {
		    $current_date = date('Y-m-d', time());
		    $j = 0;
		    for ($i = 1; $i <= 24; $i++):
			if ($i == 1) {
			    $current_date_hour = date('Y-m-d h:i:s', time());
			} else {
			    $current_date_hour = date('Y-m-d h:i:s', strtotime('-' . $j . ' hour'));
			}
			$previous_hour = date('Y-m-d h:i:s', strtotime('-' . $i . ' hour'));
			$previous_hour1 = date('h:i:s', strtotime('-' . $i . ' hour'));
			$date_check = date('Y-m-d', strtotime($previous_hour));
			if ($date_check == $current_date):
			    $current_time = date('Y-m-d');
			    $connection = \Yii::$app->db;
			    //$orders_data = $connection->createCommand('SELECT * from order_channel Where ' . $fieldname . '="' . $store_channel_id . '" AND date(created_at)="' . $current_date . '"  ');
			    $orders_data = $connection->createCommand('select * from order_channel WHERE ' . $fieldname . '="' . $store_channel_id . '"  AND created_at BETWEEN ("' . $previous_hour . '") AND ("' . $current_date_hour . '") AND date(created_at)="' . $current_date . '" ');
			    $orders_count = count($orders_data->queryAll());
			    $today[] = array('name' => $name, 'user' => $orders_count, 'date' => $previous_hour1);
			// $arr=array_reverse($arr);
			endif;
			$j++;
		    endfor;
		}


		/*		 * *********************** FOR TODAY************************ */
		if ($post == 'dashboardchartquarter' || $post == 'dashboardchartquartermob') {
		    $current_date = date('Y-m-d', time());


		    $enddate = date('Y-m-d');
		    $current_date = date('Y-m-d', strtotime('-3 months'));
		    $datediff = strtotime($enddate) - strtotime($current_date);
		    $numOfdays = floor($datediff / (60 * 60 * 24));
		    $loop_val = floor($numOfdays / 7);
		    $prev_year = date('Y');
//                echo $loop_val;
		    $final_new_customer = [];
		    $final_repeat_customer = [];
		    for ($i = 1; $i <= $loop_val; $i++) {
			if ($i == 1) {
			    $date_current = date('W', strtotime($current_date));
			    $year_current = date('Y', strtotime($current_date));
			} else {
			    $date_current = date('W', strtotime('+' . $i * 7 . 'days', strtotime($current_date)));
			    $year_current = date('Y', strtotime('+' . $i * 7 . 'days', strtotime($current_date)));
			}

			$connection = \Yii::$app->db;
			$orders_data = $connection->createCommand('SELECT * from order_channel Where ' . $fieldname . '="' . $store_channel_id . '" AND week(created_at)="' . $date_current . '" AND year(created_at)="' . $year_current . '"');
			$orders_count = count($orders_data->queryAll());

			$arr[] = array('name' => $name, 'user' => $orders_count, 'date' => 'Week ' . $i);
		    }
		}


		if (!empty($_POST['daterange'])) {
		    $daterange = $_POST['daterange'];
		}
		// for daterange
		if ($post == 'dateRange' and ! empty($daterange)) {
		    $date = explode('-', $daterange);
		    $startdate = date('Y-m-d', strtotime($date[0]));
		    $enddate = date('Y-m-d', strtotime($date[1]));
		    $datediff = strtotime($enddate) - strtotime($startdate);
		    $numOfdays = floor($datediff / (60 * 60 * 24));
		    if ($numOfdays < 30) {

			$current_date = date('d');
			$prev_year = date('Y');

			for ($i = $numOfdays; $i >= 0; $i--) {
			    $days_array_current_year = [];
			    $days_array_last_year = [];
			    $date_current = date('Y-m-d', strtotime('-' . $i . ' days', strtotime($enddate)));
			    $date_current_text = date('M-d', strtotime('-' . $i . ' days', strtotime($enddate)));


			    $connection = \Yii::$app->db;
			    $orders_data = $connection->createCommand('SELECT * from order_channel Where ' . $fieldname . '="' . $store_channel_id . '" AND date(created_at)="' . $date_current . '" AND year(created_at)="' . $currentyear . '"');
			    $orders_count = count($orders_data->queryAll());

			    $arr[] = array('name' => $name, 'user' => $orders_count, 'date' => $date_current_text . ' ' . $currentyear);
			}
		    }
		    if ($numOfdays > 30 && $numOfdays < 150) {
			$loop_val = floor($numOfdays / 7);
			$current_date = $startdate;
			for ($i = 1; $i <= $loop_val; $i++) {
			    if ($i == 1) {
				$date_current = date('W', strtotime($current_date));
				$year_current = date('Y', strtotime($current_date));
			    } else {
				$date_current = date('W', strtotime('+' . $i * 7 . 'days', strtotime($current_date)));
				$year_current = date('Y', strtotime('+' . $i * 7 . 'days', strtotime($current_date)));
			    }

			    $connection = \Yii::$app->db;
			    $orders_data = $connection->createCommand('SELECT * from order_channel Where ' . $fieldname . '="' . $store_channel_id . '" AND week(created_at)="' . $date_current . '" AND year(created_at)="' . $year_current . '"');
			    $orders_count = count($orders_data->queryAll());

			    $arr[] = array('name' => $name, 'user' => $orders_count, 'date' => 'Week ' . $i);
			}
		    }
		}
	    }
	}
	if (!empty($today)) {
	    $arr = array_reverse($today);
	}
	$array1 = array();
	if (!empty($arr)) {
	    foreach ($arr as $a):
		/* Check if key exists or not */
		$dates = $a['date'];
		if (array_key_exists($dates, $array1)) {
		    $array1[$dates][] = array(
			'name' => $a['name'],
			'user' => $a['user'],
		    );
		} else {
		    $array1[$dates][] = array(
			'name' => $a['name'],
			'user' => $a['user'],
		    );
		}
	    endforeach;
	}


	$data_val_2 = array();
	foreach ($array1 as $_data => $value) {
	    $data_val = array();
	    foreach ($value as $_value) {
		$data_val['period'] = $_data;
//$data_val[$_value['name']]=$_value['name'];
		$data_val[$_value['name'] . "  Orders"] = $_value['user'];
	    }
	    $data_val_2[] = $data_val;
	}



	$keyname = array();
	$keyname_2 = array();
	if (!empty($data_val)) {
	    foreach ($data_val as $key => $val) {
		if ($key == 'period') {
		    
		} else {
		    $keyname[] = $key;
		}
	    }
	}

	$keyname_2 = $keyname;

	/*
	 * array to store hex color 
	 */
	$colorarr = array(
	    0 => '#0091ea', #e04a72
	    1 => '#00b0ff', #83ed1a
	    2 => '#40c4ff',
	    3 => '#80d8ff',
	    4 => '#01579b',
	    5 => '#0277bd',
	    6 => '#039be5',
	    7 => '#03a9f4',
	    8 => '#b3e5fc',
	    9 => '#81d4fa',
	    10 => '#29b6f6',
	    11 => '#e1f5fe',
	    12 => '#b5af79',
	    13 => '#914f84',
	    14 => '#ce6d87',
	    15 => '#e04a72',
	);
	$randcolor = array();
	$randcolor_2 = array();
	$showcolorhtml = '';
	$c = 0;
	$cc = 0;
	foreach ($keyname as $color) {
	    // $randcolor[] = '#' . random_color();
	    $randcolor[] = $colorarr[$c++];
	    $strname = explode(' ', $color);
	    unset($strname[count($strname) - 1]);
	    $strname = implode(' ', $strname);

	    $showcolorhtml .= '<li><span data-color="main-chart-color" class="BigCommerce connected_store_name" style="background-color: ' . $colorarr[$cc++] . ';"></span>' . $strname . '</li>';
	}

	$randcolor_2 = $randcolor;
	if (!empty($data_val_2)) {
	    $data = array('data' => json_encode($data_val_2), 'ykeyslabels' => json_encode($keyname_2), 'linecolors' => json_encode($randcolor_2), 'showcolorhtml' => $showcolorhtml);
	} else {
	    $data = 'Invalid';
	}
	return \yii\helpers\Json::encode($data);
    }

/////////////////////////////////////////////////////////////
//new dashboard data start here
    public function actionNewdashboard() {
	$user = Yii::$app->user->identity;
	$user_id = $user->id;

	$products_by_volume = OrdersProducts::find()
		->select(['sum(qty) AS product_quantity', 'product_Id'])
		->where(['elliot_user_id' => $user_id])
		->groupBy(['product_id'])
		->orderBy(['product_quantity' => SORT_DESC])
		->with(['product' => function($query) {
			$query->where(['not like', 'product_status', 'in_active']);
			$query->select('id, price, product_name');
		    },
		    'product.productImages' => function($query) {
			$query->select('product_ID,link');
		    }])
		->asArray()
		->all();


	$products_by_revenue = OrdersProducts::find()
		->select(['sum(price) AS product_price', 'product_Id'])
		->where(['elliot_user_id' => $user_id])
		->groupBy(['product_id'])
		->orderBy(['product_price' => SORT_DESC])
		->with(['product' => function($query) {
			$query->where(['not like', 'product_status', 'in_active']);
			$query->select('id, price, product_name,country_code');
		    },
		    'product.productImages' => function($query) {
			$query->select('product_ID,link');
		    }])
		->asArray()
		->all();

	$countries = \backend\models\StoreDetails::find()->select(['id', 'country_code', 'country'])->distinct()->asArray()->all();
	$countries = ArrayHelper::map($countries, 'country_code', 'country');
//        echo"<pre>";            print_R($countries);die;
	if (isset($_GET['$$']) and ! empty($_GET['$$'])) {
	    echo "<pre>";
	    print_r($products_by_volume);
	    print_r($products_by_revenue);
	    print_r($countries);
	    die;
	}

	return $this->render('newdashboard', [
		    'products_by_volume' => $products_by_volume,
		    'products_by_revenue' => $products_by_revenue,
		    'countries' => $countries,
		    'user' => $user
			]
	);

	// return $this->render('newdashboard');
    }

    /*
     * ajax for orders graph on new dashboard page from custom_graph.js
     */

    public function actionNewdashordersgraph() {
	if (!empty($_POST['data'])) {
	    $post = preg_replace('/\s+/', '', $_POST['data']);
	}
	if (!empty($_POST['daterange'])) {
	    $daterange = $_POST['daterange'];
	}
	$data_2017 = 0;
	$data_2016 = 0;
	// for daterange
	if ($post == 'dateRange' && !empty($daterange)) {
	    $date = explode('-', $daterange);
	    $startdate = date('Y-m-d', strtotime($date[0]));
	    $enddate = date('Y-m-d', strtotime($date[1]));
	    $datediff = strtotime($enddate) - strtotime($startdate);
	    $numOfdays = floor($datediff / (60 * 60 * 24));
	    if ($numOfdays < 30) {
		$prev_year = date('Y');
		for ($i = $numOfdays; $i >= 0; $i--) {
		    $Week_previous_date1 = date('Y-m-d', strtotime('-' . $i . ' days', strtotime($enddate)));
		    $Week_previous_date_1 = date('M-d', strtotime($Week_previous_date1));
		    $Week_previous_date2 = date('Y-m-d', strtotime('-1 years', strtotime($Week_previous_date1)));
		    $Week_previous_date_2 = date('M-d', strtotime($Week_previous_date2));
		    //for 2017
		    $connection1 = \Yii::$app->db;
		    $orders_data1 = $connection1->createCommand('SELECT * from orders Where date(created_at)="' . $Week_previous_date1 . '"  ');
		    $orders_count1 = count($orders_data1->queryAll());
		    $data_2017 += $orders_count1;
		    //            $arr1[] = array('2017N' => '2017', '2017D' => $orders_count1, 'date' => $Week_previous_date_1);
		    //for 2016
		    $connection2 = \Yii::$app->db;
		    $orders_data2 = $connection2->createCommand('SELECT * from orders Where date(created_at)="' . $Week_previous_date2 . '"  ');
		    $orders_count2 = count($orders_data2->queryAll());
		    $data_2016 += $orders_count2;
		    $arr[] = array('2017D' => $orders_count1, '2016D' => $orders_count2, 'date' => $Week_previous_date_2);
		}
	    }
	    if ($numOfdays > 30 && $numOfdays < 150) {
		$loop_val = floor($numOfdays / 7);
		$current_date = date('d');
		$prev_year = date('Y');
		$j = 0;
		for ($i = 1; $i <= $loop_val; $i++) {
		    $start_Date = date('Y-m-d', strtotime('+' . $j * 7 . 'days', strtotime($startdate)));
		    $start_Date2 = date('Y-m-d', strtotime('-1 years', strtotime($start_Date)));
		    $start_date = date('Y-m-d', strtotime('+' . $i * 7 . 'days', strtotime($startdate)));
		    $start_date2 = date('Y-m-d', strtotime('-1 years', strtotime($start_date)));
		    $week_num = 'Week ' . $i;
		    $Week_previous_date1 = date('Y-m-d', strtotime('-' . $i . ' days'));
		    $Week_previous_date_1 = date('M-d', strtotime($Week_previous_date1));
		    $Week_previous_date2 = date('Y-m-d', strtotime('-1 years', strtotime($Week_previous_date1)));
		    $Week_previous_date_2 = date('M-d', strtotime($Week_previous_date2));
		    //for 2017
		    $connection1 = \Yii::$app->db;
		    $orders_data1 = $connection1->createCommand('SELECT * from orders Where date(created_at) BETWEEN"' . $start_Date . '" AND "' . $start_date . '"');
		    $orders_count1 = count($orders_data1->queryAll());
		    $data_2017 += $orders_count1;
		    //            $arr1[] = array('2017N' => '2017', '2017D' => $orders_count1, 'date' => $Week_previous_date_1);
		    //for 2016
		    $connection2 = \Yii::$app->db;
		    $orders_data2 = $connection2->createCommand('SELECT * from orders Where date(created_at) BETWEEN"' . $start_Date2 . '" AND"' . $start_date2 . '" ');
		    $orders_count2 = count($orders_data2->queryAll());
		    $data_2016 += $orders_count2;
		    $arr[] = array('2017D' => $orders_count1, '2016D' => $orders_count2, 'date' => $week_num);
		    $j++;
		}
	    }
	}

	// for month
	if ($post == 'month') {
	    $current_date = date('d');
	    $prev_year = date('Y');
	    for ($i = $current_date - 1; $i >= 0; $i--) {
		$Week_previous_date1 = date('Y-m-d', strtotime('-' . $i . ' days'));
		$Week_previous_date_1 = date('M-d', strtotime($Week_previous_date1));
		$Week_previous_date2 = date('Y-m-d', strtotime('-1 years', strtotime($Week_previous_date1)));
		$Week_previous_date_2 = date('M-d', strtotime($Week_previous_date2));
		//for 2017
		$connection1 = \Yii::$app->db;
		$orders_data1 = $connection1->createCommand('SELECT * from orders Where date(created_at)="' . $Week_previous_date1 . '"  ');
		$orders_count1 = count($orders_data1->queryAll());
		$data_2017 += $orders_count1;
		//            $arr1[] = array('2017N' => '2017', '2017D' => $orders_count1, 'date' => $Week_previous_date_1);
		//for 2016
		$connection2 = \Yii::$app->db;
		$orders_data2 = $connection2->createCommand('SELECT * from orders Where date(created_at)="' . $Week_previous_date2 . '"  ');
		$orders_count2 = count($orders_data2->queryAll());
		$data_2016 += $orders_count2;
		$arr[] = array('2017D' => $orders_count1, '2016D' => $orders_count2, 'date' => $Week_previous_date_2);
	    }
	}
	//for week
	if ($post == 'week') {
	    $current_date = date('d');
	    $prev_year = date('Y');
	    for ($i = 6; $i >= 0; $i--) {
		$Week_previous_date1 = date('Y-m-d', strtotime('-' . $i . ' days'));
		$Week_previous_date_1 = date('M-d', strtotime($Week_previous_date1));
		$Week_previous_date2 = date('Y-m-d', strtotime('-1 years', strtotime($Week_previous_date1)));
		$Week_previous_date_2 = date('M-d', strtotime($Week_previous_date2));
		//for 2017
		$connection1 = \Yii::$app->db;
		$orders_data1 = $connection1->createCommand('SELECT * from orders Where date(created_at)="' . $Week_previous_date1 . '"  ');
		$orders_count1 = count($orders_data1->queryAll());
		$data_2017 += $orders_count1;
//            $arr1[] = array('2017N' => '2017', '2017D' => $orders_count1, 'date' => $Week_previous_date_1);
		//for 2016
		$connection2 = \Yii::$app->db;
		$orders_data2 = $connection2->createCommand('SELECT * from orders Where date(created_at)="' . $Week_previous_date2 . '"  ');
		$orders_count2 = count($orders_data2->queryAll());
		$data_2016 += $orders_count2;
		$arr[] = array('2017D' => $orders_count1, '2016D' => $orders_count2, 'date' => $Week_previous_date_2);
	    }
	}
	//for quarter
	if ($post == 'Quarter') {
	    $enddate = date('Y-m-d');
	    $startdate = date('Y-m-d', strtotime('-3 months'));
	    $datediff = strtotime($enddate) - strtotime($startdate);
	    $numOfdays = floor($datediff / (60 * 60 * 24));
	    $loop_val = floor($numOfdays / 7);
	    $prev_year = date('Y');
	    $j = 0;
	    for ($i = 1; $i <= $loop_val; $i++) {
		$start_Date = date('Y-m-d', strtotime('+' . $j * 7 . 'days', strtotime($startdate)));
		$start_Date2 = date('Y-m-d', strtotime('-1 years', strtotime($start_Date)));
		$start_date = date('Y-m-d', strtotime('+' . $i * 7 . 'days', strtotime($startdate)));
		$start_date2 = date('Y-m-d', strtotime('-1 years', strtotime($start_date)));
		$week_num = 'Week ' . $i;
		$Week_previous_date1 = date('Y-m-d', strtotime('-' . $i . ' days'));
		$Week_previous_date_1 = date('M-d', strtotime($Week_previous_date1));
		$Week_previous_date2 = date('Y-m-d', strtotime('-1 years', strtotime($Week_previous_date1)));
		$Week_previous_date_2 = date('M-d', strtotime($Week_previous_date2));
		//for 2017
		$connection1 = \Yii::$app->db;
		$orders_data1 = $connection1->createCommand('SELECT * from orders Where date(created_at) BETWEEN"' . $start_Date . '" AND "' . $start_date . '"');
		$orders_count1 = count($orders_data1->queryAll());
		$data_2017 += $orders_count1;
//            $arr1[] = array('2017N' => '2017', '2017D' => $orders_count1, 'date' => $Week_previous_date_1);
		//for 2016
		$connection2 = \Yii::$app->db;
		$orders_data2 = $connection2->createCommand('SELECT * from orders Where date(created_at) BETWEEN"' . $start_Date2 . '" AND "' . $start_date2 . '"');
		$orders_count2 = count($orders_data2->queryAll());
		$data_2016 += $orders_count2;
		$arr[] = array('2017D' => $orders_count1, '2016D' => $orders_count2, 'date' => $week_num);
		$j++;
	    }
	}
	//for today
	if ($post == 'day') {
	    $current_date1 = date('Y-m-d', time());
	    $current_date2 = date('Y-m-d', strtotime('-1 years', strtotime($current_date1)));
	    $j = 0;
	    for ($i = 1; $i <= 24; $i++) {
		if ($i == 1) {
		    $current_date_hour1 = date('Y-m-d h:i:s', time());
		    $current_date_hour2 = date('Y-m-d h:i:s', strtotime('-1 years', strtotime($current_date_hour1)));
		} else {
		    $current_date_hour1 = date('Y-m-d h:i:s', strtotime('-' . $j . ' hour'));
		    $current_date_hour2 = date('Y-m-d h:i:s', strtotime('-1 years', strtotime($current_date_hour1)));
		}
		$previous_hour1 = date('Y-m-d h:i:s', strtotime('-' . $i . ' hour'));
		$previous_hour_1 = date('Y-m-d h:i:s', strtotime($previous_hour1));
		$previous_hour2 = date('Y-m-d h:i:s', strtotime('-1 years', strtotime($previous_hour1)));
		$date_check = date('Y-m-d', strtotime($previous_hour1));
		if ($date_check == $current_date1):
		    $current_time = date('Y-m-d');
		    $connection1 = \Yii::$app->db;
		    $orders_data1 = $connection1->createCommand('select * from orders Where  created_at BETWEEN ("' . $previous_hour1 . '") AND ("' . $current_date_hour1 . '") AND date(created_at)="' . $current_date1 . '" ');
		    $orders_count1 = count($orders_data1->queryAll());
		    $data_2017 += $orders_count1;
		    $connection2 = \Yii::$app->db;
		    $orders_data2 = $connection2->createCommand('select * from orders Where  created_at BETWEEN ("' . $previous_hour1 . '") AND ("' . $current_date_hour1 . '") AND date(created_at)="' . $current_date1 . '" ');
		    $orders_count2 = count($orders_data2->queryAll());
		    $data_2016 += $orders_count2;
		    $today[] = array('2017D' => $orders_count1, '2016D' => $orders_count2, 'date' => $previous_hour_1);
		endif;
		$j++;
	    }
	}
	if (!empty($today)) {
	    $arr = array_reverse($today);
	}
	if (!empty($arr_data)) {
	    $arr = array_reverse($arr_data);
	}
	//echo $data_2017;echo'klklk';echo $data_2016;
	if ($data_2017 > 0) {

	    $data_up_down = round((($data_2017 - $data_2016) / $data_2017) * 100);
	} else {
	    $data_up_down = 0;
	}
	$array1 = array();
	foreach ($arr as $a):
	    /* Check if key exists or not */
	    $dates = $a['date'];
	    if (array_key_exists($dates, $array1)) {
		$array1[$dates][] = array(
		    '2017D' => $a['2017D'],
		    '2016D' => $a['2016D'],
		);
	    } else {
		$array1[$dates][] = array(
		    '2017D' => $a['2017D'],
		    '2016D' => $a['2016D'],
		);
	    }
	endforeach;
	$data_val_2 = array();
	foreach ($array1 as $_data => $value) {
	    $data_val = array();
	    foreach ($value as $_value) {
		$data_val['period'] = $_data;
		//$data_val[$_value['name']]=$_value['name'];
		$data_val['2017'] = $_value['2017D'];
		$data_val['2016'] = $_value['2016D'];
	    }
	    $data_val_2[] = $data_val;
	}

	$keyname = array();
	$keyname_2 = array();
	if (!empty($data_val)) {
	    foreach ($data_val as $key => $val) {
		if ($key == 'period') {
		    
		} else {
		    $keyname[] = $key;
		}
	    }
	}
	$keyname_2 = $keyname;
	/*
	 * array to store hex color 
	 */
	$colorarr = array(
	    0 => '#0091ea', #e04a72
	    1 => '#00b0ff', #83ed1a
	    2 => '#40c4ff',
	    3 => '#80d8ff',
	    4 => '#01579b',
	    5 => '#0277bd',
	    6 => '#039be5',
	    7 => '#03a9f4',
	    8 => '#b3e5fc',
	    9 => '#81d4fa',
	    10 => '#29b6f6',
	    11 => '#e1f5fe',
	    12 => '#b5af79',
	    13 => '#914f84',
	    14 => '#ce6d87',
	    15 => '#e04a72',
	);
	$randcolor = array();
	$randcolor_2 = array();
	$showcolorhtml = '';
	$c = 0;
	$cc = 0;
	foreach ($keyname as $color) {
	    // $randcolor[] = '#' . random_color();
	    $randcolor[] = $colorarr[$c++];
	    $strname = explode(' ', $color);
	    //$showcolorhtml .= '<li><span data-color="main-chart-color" class="BigCommerce" style="background-color: ' . $colorarr[$cc++] . ';"></span>' . $strname[0] . '</li>';
	    $showcolorhtml .= '<li><span data-color="main-chart-color" class="BigCommerce" style="background-color: ' . $colorarr[$cc++] . ';"></span><span style="position:relative;top:-3px;width:35px;background-color:#fff">' . $strname[0] . '</span></li>';
	}
	$randcolor_2 = $randcolor;
	$data = array('data' => json_encode($data_val_2), 'ykeyslabels' => json_encode($keyname_2), 'linecolors' => json_encode($randcolor_2), 'up_down' => json_encode($data_up_down), 'showcolorhtml' => $showcolorhtml);
	// $data = array('data' => json_encode($data_val_2));
	return \yii\helpers\Json::encode($data);
    }

    /*
     * ajax for REVENUE graph on new dashboard page from custom_graph.js
     */

    public function actionNewdashrevenuegraph() {
	if (!empty($_POST['data'])) {
	    $post = preg_replace('/\s+/', '', $_POST['data']);
	}
	if (!empty($_POST['daterange'])) {
	    $daterange = $_POST['daterange'];
	}
	$data_2016 = 0;
	$data_2017 = 0;
	// for date range
	if ($post == 'dateRange' && !empty($daterange)) {
	    $date = explode('-', $daterange);
	    $startdate = date('Y-m-d', strtotime($date[0]));
	    $enddate = date('Y-m-d', strtotime($date[1]));
	    $datediff = strtotime($enddate) - strtotime($startdate);
	    $numOfdays = floor($datediff / (60 * 60 * 24));
	    if ($numOfdays < 30) {
		$current_date = date('d');
		$prev_year = date('Y');
		for ($i = $numOfdays; $i >= 0; $i--) {
		    $Week_previous_date1 = date('Y-m-d', strtotime('-' . $i . ' days', strtotime($enddate)));
		    $Week_previous_date_1 = date('M-d', strtotime($Week_previous_date1));
		    $Week_previous_date2 = date('Y-m-d', strtotime('-1 years', strtotime($Week_previous_date1)));
		    $Week_previous_date_2 = date('M-d', strtotime($Week_previous_date2));
		    //for 2017
		    $connection1 = \Yii::$app->db;
		    $orders_data1 = $connection1->createCommand('SELECT * from orders Where date(created_at)="' . $Week_previous_date1 . '" AND order_status = "Completed"');
		    $orders_count1 = count($orders_data1->queryAll());
		    $data_2017 += $orders_count1;
		    //for 2016
		    $connection2 = \Yii::$app->db;
		    $orders_data2 = $connection2->createCommand('SELECT * from orders Where date(created_at)="' . $Week_previous_date2 . '"  AND order_status = "Completed" ');
		    $orders_count2 = count($orders_data2->queryAll());
		    $data_2016 += $orders_count2;
		    $arr[] = array('2017D' => $orders_count1, '2016D' => $orders_count2, 'date' => $Week_previous_date_2);
		}
	    }
	    if ($numOfdays > 30 && $numOfdays < 150) {
		$loop_val = floor($numOfdays / 7);
		$current_date = date('d');
		$prev_year = date('Y');
		$j = 0;
		for ($i = 1; $i <= $loop_val; $i++) {
		    $start_Date = date('Y-m-d', strtotime('+' . $j * 7 . 'days', strtotime($startdate)));
		    $start_Date2 = date('Y-m-d', strtotime('-1 years', strtotime($start_Date)));
		    $start_date = date('Y-m-d', strtotime('+' . $i * 7 . 'days', strtotime($startdate)));
		    $start_date2 = date('Y-m-d', strtotime('-1 years', strtotime($start_date)));
		    $week_num = 'Week ' . $i;
		    $Week_previous_date1 = date('Y-m-d', strtotime('-' . $i . ' days'));
		    $Week_previous_date_1 = date('M-d', strtotime($Week_previous_date1));
		    $Week_previous_date2 = date('Y-m-d', strtotime('-1 years', strtotime($Week_previous_date1)));
		    $Week_previous_date_2 = date('M-d', strtotime($Week_previous_date2));
		    //for 2017
		    $connection1 = \Yii::$app->db;
		    $orders_data1 = $connection1->createCommand('SELECT * from orders Where date(created_at) BETWEEN"' . $start_Date . '" AND "' . $start_date . '" AND order_status = "Completed"');
		    $orders_count1 = count($orders_data1->queryAll());
		    $data_2017 += $orders_count1;
		    //            $arr1[] = array('2017N' => '2017', '2017D' => $orders_count1, 'date' => $Week_previous_date_1);
		    //for 2016
		    $connection2 = \Yii::$app->db;
		    $orders_data2 = $connection2->createCommand('SELECT * from orders Where date(created_at) BETWEEN"' . $start_Date2 . '" AND "' . $start_date2 . '" AND order_status = "Completed"');
		    $orders_count2 = count($orders_data2->queryAll());
		    $data_2016 += $orders_count2;
		    $arr[] = array('2017D' => $orders_count1, '2016D' => $orders_count2, 'date' => $week_num);
		    $j++;
		}
	    }
	}
	// for month
	if ($post == 'month') {
	    $current_date = date('d');
	    $prev_year = date('Y');
	    for ($i = $current_date - 1; $i >= 0; $i--) {
		$Week_previous_date1 = date('Y-m-d', strtotime('-' . $i . ' days'));
		$Week_previous_date_1 = date('M-d', strtotime($Week_previous_date1));
		$Week_previous_date2 = date('Y-m-d', strtotime('-1 years', strtotime($Week_previous_date1)));
		$Week_previous_date_2 = date('M-d', strtotime($Week_previous_date2));
		//for 2017
		$connection1 = \Yii::$app->db;
		$orders_data1 = $connection1->createCommand('SELECT * from orders Where date(created_at)="' . $Week_previous_date1 . '" AND order_status = "Completed"');
		$orders_count1 = count($orders_data1->queryAll());
		$data_2017 += $orders_count1;
		//for 2016
		$connection2 = \Yii::$app->db;
		$orders_data2 = $connection2->createCommand('SELECT * from orders Where date(created_at)="' . $Week_previous_date2 . '"  AND order_status = "Completed" ');
		$orders_count2 = count($orders_data2->queryAll());
		$data_2016 += $orders_count2;
		$arr[] = array('2017D' => $orders_count1, '2016D' => $orders_count2, 'date' => $Week_previous_date_2);
	    }
	}
	//for week
	if ($post == 'week') {
	    $current_date = date('d');
	    $prev_year = date('Y');
	    for ($i = 6; $i >= 0; $i--) {
		$Week_previous_date1 = date('Y-m-d', strtotime('-' . $i . ' days'));
		$Week_previous_date_1 = date('M-d', strtotime($Week_previous_date1));
		$Week_previous_date2 = date('Y-m-d', strtotime('-1 years', strtotime($Week_previous_date1)));
		$Week_previous_date_2 = date('M-d', strtotime($Week_previous_date2));
		//for 2017
		$connection1 = \Yii::$app->db;
		$orders_data1 = $connection1->createCommand('SELECT * from orders Where date(created_at)="' . $Week_previous_date1 . '"  AND order_status = "Completed" ');
		$orders_count1 = count($orders_data1->queryAll());
		$data_2017 += $orders_count1;
//            $arr1[] = array('2017N' => '2017', '2017D' => $orders_count1, 'date' => $Week_previous_date_1);
		//for 2016
		$connection2 = \Yii::$app->db;
		$orders_data2 = $connection2->createCommand('SELECT * from orders Where date(created_at)="' . $Week_previous_date2 . '"  AND order_status = "Completed" ');
		$orders_count2 = count($orders_data2->queryAll());
		$data_2016 += $orders_count2;
		$arr[] = array('2017D' => $orders_count1, '2016D' => $orders_count2, 'date' => $Week_previous_date_2);
	    }
	}
	//for quarter
	if ($post == 'Quarter') {
	    $enddate = date('Y-m-d');
	    $startdate = date('Y-m-d', strtotime('-3 months'));
	    $datediff = strtotime($enddate) - strtotime($startdate);
	    $numOfdays = floor($datediff / (60 * 60 * 24));
	    $loop_val = floor($numOfdays / 7);
	    $prev_year = date('Y');
	    $j = 0;
	    for ($i = 1; $i <= $loop_val; $i++) {
		$start_Date = date('Y-m-d', strtotime('+' . $j * 7 . 'days', strtotime($startdate)));
		$start_Date2 = date('Y-m-d', strtotime('-1 years', strtotime($start_Date)));
		$start_date = date('Y-m-d', strtotime('+' . $i * 7 . 'days', strtotime($startdate)));
		$start_date2 = date('Y-m-d', strtotime('-1 years', strtotime($start_date)));
		$week_num = 'Week ' . $i;
		$Week_previous_date1 = date('Y-m-d', strtotime('-' . $i . ' days'));
		$Week_previous_date_1 = date('M-d', strtotime($Week_previous_date1));
		$Week_previous_date2 = date('Y-m-d', strtotime('-1 years', strtotime($Week_previous_date1)));
		$Week_previous_date_2 = date('M-d', strtotime($Week_previous_date2));
		//for 2017
		$connection1 = \Yii::$app->db;
		$orders_data1 = $connection1->createCommand('SELECT * from orders Where date(created_at) BETWEEN"' . $start_Date . '" AND "' . $start_date . '" AND order_status = "Completed"  ');
		$orders_count1 = count($orders_data1->queryAll());
		$data_2017 += $orders_count1;
//            $arr1[] = array('2017N' => '2017', '2017D' => $orders_count1, 'date' => $Week_previous_date_1);
		//for 2016
		$connection2 = \Yii::$app->db;
		$orders_data2 = $connection2->createCommand('SELECT * from orders Where date(created_at) BETWEEN"' . $start_Date2 . '" AND "' . $start_date2 . '" AND order_status = "Completed"  ');
		$orders_count2 = count($orders_data2->queryAll());
		$data_2016 += $orders_count2;
		$arr[] = array('2017D' => $orders_count1, '2016D' => $orders_count2, 'date' => $week_num);
		$j++;
	    }
	}
	//for today
	if ($post == 'day') {

	    $current_date1 = date('Y-m-d', time());
	    $current_date2 = date('Y-m-d', strtotime('-1 years', strtotime($current_date1)));
	    $j = 0;
	    for ($i = 1; $i <= 24; $i++) {
		if ($i == 1) {
		    $current_date_hour1 = date('Y-m-d h:i:s', time());
		    $current_date_hour2 = date('Y-m-d h:i:s', strtotime('-1 years', strtotime($current_date_hour1)));
		} else {
		    $current_date_hour1 = date('Y-m-d h:i:s', strtotime('-' . $j . ' hour'));
		    $current_date_hour2 = date('Y-m-d h:i:s', strtotime('-1 years', strtotime($current_date_hour1)));
		}
		$previous_hour1 = date('Y-m-d h:i:s', strtotime('-' . $i . ' hour'));
		$previous_hour_1 = date('Y-m-d h:i:s', strtotime($previous_hour1));
		$previous_hour2 = date('Y-m-d h:i:s', strtotime('-1 years', strtotime($previous_hour1)));
		$date_check = date('Y-m-d', strtotime($previous_hour1));
		if ($date_check == $current_date1):
		    $current_time = date('Y-m-d');
		    $connection1 = \Yii::$app->db;
		    $orders_data1 = $connection1->createCommand('select * from orders Where  created_at BETWEEN ("' . $previous_hour1 . '") AND ("' . $current_date_hour1 . '") AND date(created_at)="' . $current_date1 . '" AND order_status = "Completed" ');
		    $orders_count1 = count($orders_data1->queryAll());
		    $data_2017 += $orders_count1;
		    $connection2 = \Yii::$app->db;
		    $orders_data2 = $connection2->createCommand('select * from orders Where  created_at BETWEEN ("' . $previous_hour1 . '") AND ("' . $current_date_hour1 . '") AND date(created_at)="' . $current_date1 . '"  AND order_status = "Completed"');
		    $orders_count2 = count($orders_data2->queryAll());
		    $data_2016 += $orders_count2;
		    $today[] = array('2017D' => $orders_count1, '2016D' => $orders_count2, 'date' => $previous_hour_1);
		endif;
		$j++;
	    }
	}
	if (!empty($today)) {
	    $arr = array_reverse($today);
	}
	if (!empty($arr_data)) {
	    $arr = array_reverse($arr_data);
	}
	if ($data_2017 > 0) {
	    $data_up_down = round((($data_2017 - $data_2016) / $data_2017) * 100);
	} else {
	    $data_up_down = 0;
	}
	$array1 = array();
	foreach ($arr as $a):
	    /* Check if key exists or not */
	    $dates = $a['date'];
	    if (array_key_exists($dates, $array1)) {
		$array1[$dates][] = array(
		    '2017D' => $a['2017D'],
		    '2016D' => $a['2016D'],
		);
	    } else {
		$array1[$dates][] = array(
		    '2017D' => $a['2017D'],
		    '2016D' => $a['2016D'],
		);
	    }
	endforeach;
	$data_val_2 = array();
	foreach ($array1 as $_data => $value) {
	    $data_val = array();
	    foreach ($value as $_value) {
		$data_val['period'] = $_data;
		//$data_val[$_value['name']]=$_value['name'];
		$data_val['2017'] = $_value['2017D'];
		$data_val['2016'] = $_value['2016D'];
	    }
	    $data_val_2[] = $data_val;
	}

	$keyname = array();
	$keyname_2 = array();
	if (!empty($data_val)) {
	    foreach ($data_val as $key => $val) {
		if ($key == 'period') {
		    
		} else {
		    $keyname[] = $key;
		}
	    }
	}
	$keyname_2 = $keyname;
	/*
	 * array to store hex color 
	 */
	$colorarr = array(
	    0 => '#0091ea', #e04a72
	    1 => '#00b0ff', #83ed1a
	    2 => '#40c4ff',
	    3 => '#80d8ff',
	    4 => '#01579b',
	    5 => '#0277bd',
	    6 => '#039be5',
	    7 => '#03a9f4',
	    8 => '#b3e5fc',
	    9 => '#81d4fa',
	    10 => '#29b6f6',
	    11 => '#e1f5fe',
	    12 => '#b5af79',
	    13 => '#914f84',
	    14 => '#ce6d87',
	    15 => '#e04a72',
	);
	$randcolor = array();
	$randcolor_2 = array();
	$showcolorhtml = '';
	$c = 0;
	$cc = 0;
	foreach ($keyname as $color) {
	    // $randcolor[] = '#' . random_color();
	    $randcolor[] = $colorarr[$c++];
	    $strname = explode(' ', $color);
	    //$showcolorhtml .= '<li><span data-color="main-chart-color" class="BigCommerce" style="background-color: ' . $colorarr[$cc++] . ';"></span>' . $strname[0] . '</li>';
	    $showcolorhtml .= '<li><span data-color="main-chart-color" class="BigCommerce" style="background-color: ' . $colorarr[$cc++] . ';"></span><span style="position:relative;top:-3px;width:35px;background-color:#fff">' . $strname[0] . '</span></li>';
	}
	$randcolor_2 = $randcolor;
	$data = array('data' => json_encode($data_val_2), 'ykeyslabels' => json_encode($keyname_2), 'linecolors' => json_encode($randcolor_2), 'up_down' => json_encode($data_up_down), 'showcolorhtml' => $showcolorhtml);
	// $data = array('data' => json_encode($data_val_2));
	return \yii\helpers\Json::encode($data);
    }

    /*
     * ajax hit from custom_graph.js for pie chart data
     */

    public function actionNewdashrevenuebychannel() {
	if (!empty($_POST['data'])) {
	    $post = preg_replace('/\s+/', '', $_POST['data']);
	}
	if (!empty($_POST['daterange'])) {
	    $daterange = $_POST['daterange'];
	}
	$connectedchannel = ChannelConnection::find()->where(['user_id' => Yii::$app->user->identity->id])->all();
	$connectedstore = StoresConnection::find()->where(['user_id' => Yii::$app->user->identity->id])->all();
	$connecteduser = array_merge($connectedchannel, $connectedstore);
	if (!empty($connecteduser)) {
	    $arr = array();
	    $arr2 = array();
	    $namearr = array();
//channel_connection_id
	    foreach ($connecteduser as $connectedstore) {
		if (!empty($connectedstore->channel_id)) {
		    $channelname = Channels::find()->where(['channel_ID' => $connectedstore->channel_id])->one();
		    $fieldname = 'channel_id';
		    $store_channel_id = $connectedstore->channel_connection_id;
//                    if (($channelname->channel_name) == 'Facebook' || ($channelname->channel_name) == 'Google shopping') {
//                        continue;
//                    } elseif (($channelname->channel_name) == 'Service Account' || ($channelname->channel_name) == 'Subscription Account') {
//                        $name = 'WeChat';
//                        $store_channel_id = $connectedstore->channel_id;
//                    } else {
//                        $store_channel_id = $connectedstore->channel_id;
//                        $name = $channelname->channel_name;
//                    }
		} elseif (!empty($connectedstore->store_id)) {
		    $fieldname = 'store_id';
		    $store_channel_id = $connectedstore->stores_connection_id;
		    $storename = Stores::find()->where(['store_id' => $connectedstore->store_id])->one();
		    $name = $storename->store_name;
		}
		/* for date range */
		if (!empty($fieldname) && !empty($store_channel_id) && !empty($name) && $post == 'dateRange' && !empty($daterange)) {
		    $date = explode('-', $daterange);
		    $startdate = date('Y-m-d', strtotime($date[0]));
		    $enddate = date('Y-m-d', strtotime($date[1]));
		    $currentdate = date('Y-m-d');
		    $model = 0;
		    $model2 = OrderChannel::find()->where([$fieldname => $store_channel_id])->all();
		    foreach ($model2 as $val) {
			$connection = \Yii::$app->db;
			$orders_data = $connection->createCommand('SELECT * from orders Where  order_ID="' . $val->order_id . '"AND date(created_at) BETWEEN "' . $startdate . '" AND "' . $enddate . '"AND order_status = "Completed"');
			$data = $orders_data->queryOne();
			$model += intval($data['total_amount']);
		    }
		    $arr2[$name] = array_key_exists($name, $arr2) ? $arr2[$name] + $model : $model;
		}
		/* for today */
		if (!empty($fieldname) && !empty($store_channel_id) && !empty($name) && $post == 'day') {
		    $currentdate = date('Y-m-d');
		    $model = 0;
		    $model2 = OrderChannel::find()->where([$fieldname => $store_channel_id])->all();
		    foreach ($model2 as $val) {
			$connection = \Yii::$app->db;
			$orders_data = $connection->createCommand('SELECT * from orders Where  order_ID="' . $val->order_id . '"AND date(created_at) = "' . $currentdate . '"AND order_status = "Completed"');
			$data = $orders_data->queryOne();
			$model += intval($data['total_amount']);
		    }

		    $arr2[$name] = array_key_exists($name, $arr2) ? $arr2[$name] + $model : $model;
		}
		/* for week */
		if (!empty($fieldname) && !empty($store_channel_id) && !empty($name) && $post == 'week') {
		    $currentmonth = date('m');
		    $date_check = date('m');
		    for ($i = 1; $i <= 7; $i++) {

			if ($date_check == $currentmonth) {
			    $previous_day = date('Y-m-d', strtotime('-' . $i . 'days'));
			    $date_check = date('m', strtotime($previous_day));
			}
		    }

		    $week_previous_day = date($previous_day, strtotime('+1  days'));
		    $Week_previous_date = date('Y-m-d', strtotime('-7  days'));
		    $currentdate = date('Y-m-d');
		    $model = 0;
		    $model2 = OrderChannel::find()->where([$fieldname => $store_channel_id])->all();
		    foreach ($model2 as $val) {
			$connection = \Yii::$app->db;
			$orders_data = $connection->createCommand('SELECT * from orders Where  order_ID="' . $val->order_id . '"AND date(created_at) BETWEEN "' . $Week_previous_date . '" AND "' . $currentdate . '"AND order_status = "Completed"');
			$data = $orders_data->queryOne();
			$model += intval($data['total_amount']);
		    }

		    $arr2[$name] = array_key_exists($name, $arr2) ? $arr2[$name] + $model : $model;
		}

		/* for month */
		if (!empty($fieldname) && !empty($store_channel_id) && !empty($name) && $post == 'month') {
		    //$Month_previous_date = date('Y-m-d', strtotime('-30  days'));
		    $currentdate = date('Y-m-d');
		    $currentyear = date('Y');
		    $currentmonth = date('m');
		    $model = 0;
		    $model2 = OrderChannel::find()->where([$fieldname => $store_channel_id])->all();
		    foreach ($model2 as $val) {
			$connection = \Yii::$app->db;
			$orders_data = $connection->createCommand('SELECT * from orders Where  order_ID="' . $val->order_id . '"AND month(created_at)= "' . $currentmonth . '" AND year(created_at)="' . $currentyear . '"AND order_status = "Completed"');
			$data = $orders_data->queryOne();
			$model += intval($data['total_amount']);
		    }

		    $arr2[$name] = array_key_exists($name, $arr2) ? $arr2[$name] + $model : $model;
		}
		/* for quarter */
		if (!empty($fieldname) && !empty($store_channel_id) && !empty($name) && $post == 'Quarter') {
		    $currentmonth = date('m');
		    $currentyear = date('Y');
		    $model = 0;
		    $val = $currentmonth - 3;
		    $currentmonth_pre = date("m", mktime(0, 0, 0, $val, 10));
		    $Month_previous_date = date('Y-m-d', strtotime('-30  days'));
		    $currentdate = date('Y-m-d');
		    $model2 = OrderChannel::find()->where([$fieldname => $store_channel_id])->all();
		    foreach ($model2 as $val) {
			$connection = \Yii::$app->db;
			$orders_data = $connection->createCommand('SELECT * from orders Where  order_ID="' . $val->order_id . '"AND month(created_at) BETWEEN "' . $currentmonth_pre . '" AND "' . $currentmonth . '" AND year(created_at)="' . $currentyear . '"AND order_status = "Completed"');
			$data = $orders_data->queryOne();
			$model += intval($data['total_amount']);
		    }

		    $arr2[$name] = array_key_exists($name, $arr2) ? $arr2[$name] + $model : $model;
		}
		/* for year */
		if (!empty($fieldname) && !empty($store_channel_id) && !empty($name) && $post == 'piechartyear' || $post == 'piechartyearmob') {
		    $currentyear = date('Y');
		    $connection = \Yii::$app->db;
		    $orders_data = $connection->createCommand('SELECT * from product_channel Where  order_ID="' . $val->order_id . '"AND year(created_at)= "' . $currentyear . '"AND order_status = "Completed"');
		    $model = count($orders_data->queryAll());
		    $arr2[$name] = array_key_exists($name, $arr2) ? $arr2[$name] + $model : $model;
		}
	    }
	    $namearr2 = $namearr;
	}
	$namearr = array_keys($arr2);
	$arr = array_values($arr2);
	$sum = 0;
	foreach ($arr as $val) {
	    $sum += $val;
	}
	if ($sum == 0) {
	    $data = 'invalid';
	    return \yii\helpers\Json::encode($data);
	}
	$colorarr = array(
	    0 => '#0091ea',
	    1 => '#00b0ff',
	    2 => '#40c4ff',
	    3 => '#80d8ff',
	    4 => '#01579b',
	    5 => '#0277bd',
	    6 => '#039be5',
	    7 => '#03a9f4',
	    8 => '#b3e5fc',
	    9 => '#81d4fa',
	    10 => '#29b6f6',
	    11 => '#e1f5fe',
	    12 => '#b5af79',
	    13 => '#914f84',
	    14 => '#ce6d87',
	    15 => '#e04a72',
	);
	$randcolor = array();
	$randcolor_2 = array();
	$c = 0;
	foreach ($arr as $array1) {
	    $randcolor[] = $colorarr[$c++];
	}
	$randcolor_2 = $randcolor;
	$data = array('label' => json_encode($namearr), 'color' => json_encode($randcolor_2), 'data' => json_encode($arr));
	return \yii\helpers\Json::encode($data);
    }

    /*
     * ajax hit from custom_graph.js for pie chart data
     */

    public function actionNewdashrevenuebyregion() {
	if (!empty($_POST['data'])) {
	    $post = preg_replace('/\s+/', '', $_POST['data']);
	}
	if (!empty($_POST['daterange'])) {
	    $daterange = $_POST['daterange'];
	}
	$connectedchannel = ChannelConnection::find()->where(['user_id' => Yii::$app->user->identity->id])->all();
	$connectedstore = StoresConnection::find()->where(['user_id' => Yii::$app->user->identity->id])->all();
	$connecteduser = array_merge($connectedchannel, $connectedstore);
	if (!empty($connecteduser)) {
	    $arr = array();
	    $arr2 = array();
	    $namearr = array();
	    foreach ($connecteduser as $connectedstore) {
		if (!empty($connectedstore->channel_id)) {
		    $channelname = Channels::find()->where(['channel_ID' => $connectedstore->channel_id])->one();
		    $country_name = StoreDetails::find()->where(['channel_connection_id' => $connectedstore->channel_connection_id])->one();
		    $name = $country_name->country;
		    $fieldname = 'channel_id';
		    $store_channel_id = $connectedstore->channel_connection_id;
		} elseif (!empty($connectedstore->store_id)) {
		    $fieldname = 'store_id';
		    $store_channel_id = $connectedstore->stores_connection_id;
		    $storename = Stores::find()->where(['store_id' => $connectedstore->store_id])->one();
		    $country_name = StoreDetails::find()->where(['store_connection_id' => $connectedstore->stores_connection_id])->one();
		    $name = $country_name->country;
//                     echo 'in_store '.$store_channel_id.'country_name'.$name;
		    // $name = $storename->store_name;
		}
		/* for date range */
		if (!empty($fieldname) && !empty($store_channel_id) && !empty($name) && $post == 'dateRange' && !empty($daterange)) {
		    $date = explode('-', $daterange);
		    $startdate = date('Y-m-d', strtotime($date[0]));
		    $enddate = date('Y-m-d', strtotime($date[1]));
		    $currentdate = date('Y-m-d');
		    $model = 0;
		    $model2 = OrderChannel::find()->where([$fieldname => $store_channel_id])->all();
		    foreach ($model2 as $val) {
			$connection = \Yii::$app->db;
			$orders_data = $connection->createCommand('SELECT * from orders Where  order_ID="' . $val->order_id . '"AND date(created_at) BETWEEN "' . $startdate . '" AND "' . $enddate . '"AND order_status = "Completed"');
			$data = $orders_data->queryOne();
			$model += intval($data['total_amount']);
		    }
		    $arr2[$name] = array_key_exists($name, $arr2) ? $arr2[$name] + $model : $model;
		}
		/* for today */
		if (!empty($fieldname) && !empty($store_channel_id) && !empty($name) && $post == 'day') {
		    $currentdate = date('Y-m-d');
		    $model = 0;
		    $model2 = OrderChannel::find()->where([$fieldname => $store_channel_id])->all();
		    foreach ($model2 as $val) {
			$connection = \Yii::$app->db;
			$orders_data = $connection->createCommand('SELECT * from orders Where  order_ID="' . $val->order_id . '"AND date(created_at) = "' . $currentdate . '"AND order_status = "Completed"');
			$data = $orders_data->queryOne();
			$model += intval($data['total_amount']);
		    }
		    $arr2[$name] = array_key_exists($name, $arr2) ? $arr2[$name] + $model : $model;
		}
		/* for week */
		if (!empty($fieldname) && !empty($store_channel_id) && !empty($name) && $post == 'week') {
		    $currentmonth = date('m');
		    $date_check = date('m');
		    for ($i = 1; $i <= 7; $i++) {
			if ($date_check == $currentmonth) {
			    $previous_day = date('Y-m-d', strtotime('-' . $i . 'days'));
			    $date_check = date('m', strtotime($previous_day));
			}
		    }
		    $week_previous_day = date($previous_day, strtotime('+1  days'));
		    $Week_previous_date = date('Y-m-d', strtotime('-7  days'));
		    $currentdate = date('Y-m-d');
		    $model = 0;
		    $model2 = OrderChannel::find()->where([$fieldname => $store_channel_id])->all();
		    foreach ($model2 as $val) {
			$connection = \Yii::$app->db;
			$orders_data = $connection->createCommand('SELECT * from orders Where order_ID="' . $val->order_id . '"AND date(created_at) BETWEEN "' . $Week_previous_date . '" AND "' . $currentdate . '"AND order_status = "Completed"');
			$data = $orders_data->queryOne();
			$model += intval($data['total_amount']);
		    }
		    $arr2[$name] = array_key_exists($name, $arr2) ? $arr2[$name] + $model : $model;
		}

		/* for month */
		if (!empty($fieldname) && !empty($store_channel_id) && !empty($name) && $post == 'month') {
		    //$Month_previous_date = date('Y-m-d', strtotime('-30  days'));
		    $currentdate = date('Y-m-d');
		    $currentyear = date('Y');
		    $currentmonth = date('m');
		    $model = 0;
		    $model2 = OrderChannel::find()->where([$fieldname => $store_channel_id])->all();
		    if (empty($model2)) {
			continue;
		    }
		    foreach ($model2 as $val) {
			$connection = \Yii::$app->db;
			$orders_data = $connection->createCommand('SELECT * from orders Where order_ID="' . $val->order_id . '" AND month(created_at)= "' . $currentmonth . '" AND year(created_at)="' . $currentyear . '" AND order_status = "Completed"');
			$data = $orders_data->queryOne();
			$model = $model + intval($data['total_amount']);
		    }
		    $arr2[$name] = array_key_exists($name, $arr2) ? $arr2[$name] + $model : $model;
		}
		/* for quarter */
		if (!empty($fieldname) && !empty($store_channel_id) && !empty($name) && $post == 'Quarter') {
		    $currentmonth = date('m');
		    $currentyear = date('Y');
		    $val = $currentmonth - 3;
		    $currentmonth_pre = date("m", mktime(0, 0, 0, $val, 10));
		    $Month_previous_date = date('Y-m-d', strtotime('-30  days'));
		    $currentdate = date('Y-m-d');
		    // $orders_data = $connection->createCommand('SELECT * from order_channel Where ' . $fieldname . '="' . $store_channel_id . '"AND month(created_at) BETWEEN "' . $currentmonth_pre . '" AND "' . $currentmonth . '" AND year(created_at)="' . $currentyear . '"');
		    $model = 0;
		    $model2 = OrderChannel::find()->where([$fieldname => $store_channel_id])->all();
		    foreach ($model2 as $val) {
			$connection = \Yii::$app->db;
			$orders_data = $connection->createCommand('SELECT * from orders Where order_ID="' . $val->order_id . '"AND month(created_at) BETWEEN "' . $currentmonth_pre . '" AND "' . $currentmonth . '" AND year(created_at)="' . $currentyear . '"AND order_status = "Completed"');
			$data = $orders_data->queryOne();
			$model += intval($data['total_amount']);
		    }

		    $arr2[$name] = array_key_exists($name, $arr2) ? $arr2[$name] + $model : $model;
		}
		/* for year */
		if (!empty($fieldname) && !empty($store_channel_id) && !empty($name) && $post == 'piechartyear' || $post == 'piechartyearmob') {
		    $currentyear = date('Y');
		    $connection = \Yii::$app->db;
		    $orders_data = $connection->createCommand('SELECT * from product_channel Where ' . $fieldname . '="' . $store_channel_id . '"AND year(created_at)= "' . $currentyear . '"');
		    $model = count($orders_data->queryAll());
		    // $model = count(ProductChannel::find()->where([$fieldname => $store_channel_id])->all());
		    //$arr[$name] = in_array($name,$namearr)?$arr[$name]+$model:array($model);
		    //$namearr[] = !in_array($name,$namearr)?$name:'';
		}
	    }
	    $namearr2 = $namearr;
	}


	$namearr = array_keys($arr2);
	$arr = array_values($arr2);
	$sum = 0;
	foreach ($arr as $val) {
	    $sum += $val;
	}
	if ($sum == 0) {
	    $data = 'invalid';
	    return \yii\helpers\Json::encode($data);
	}
	$colorarr = array(
	    0 => '#0091ea',
	    1 => '#00b0ff',
	    2 => '#40c4ff',
	    3 => '#80d8ff',
	    4 => '#01579b',
	    5 => '#0277bd',
	    6 => '#039be5',
	    7 => '#03a9f4',
	    8 => '#b3e5fc',
	    9 => '#81d4fa',
	    10 => '#29b6f6',
	    11 => '#e1f5fe',
	    12 => '#b5af79',
	    13 => '#914f84',
	    14 => '#ce6d87',
	    15 => '#e04a72',
	);
	$randcolor = array();
	$randcolor_2 = array();
	$c = 0;
	foreach ($arr as $array1) {
	    $randcolor[] = $colorarr[$c++];
	}
	$randcolor_2 = $randcolor;


	$data = array('label' => json_encode($namearr), 'color' => json_encode($randcolor_2), 'data' => json_encode($arr));
	return \yii\helpers\Json::encode($data);
    }

    /*
     * ajax hit from custom graph.js for average order value graph 
     */

    public function actionNewdashavgorderval() {
	if (!empty($_POST['data'])) {
	    $post = preg_replace('/\s+/', '', $_POST['data']);
	}
	if (!empty($_POST['daterange'])) {
	    $daterange = $_POST['daterange'];
	}
	$sum = 0;
	$connectedchannel = ChannelConnection::find()->where(['user_id' => Yii::$app->user->identity->id])->all();
	$connectedstore = StoresConnection::find()->where(['user_id' => Yii::$app->user->identity->id])->all();
	$connecteduser = array_merge($connectedchannel, $connectedstore);
	if (!empty($connecteduser)) {
	    $arr = array();
	    $arr2 = array();
	    $namearr = array();
	    $ARRAY = array();

	    $names_array = [];
	    foreach ($connecteduser as $connectedstore) {
		$sum = 0;
		$count = 0;
		if (!empty($connectedstore->channel_id)) {
		    $channelname = Channels::find()->where(['channel_ID' => $connectedstore->channel_id])->one();
		    $fieldname = 'channel_id';
		    if (($channelname->channel_name) == 'Facebook' || ($channelname->channel_name) == 'Google shopping') {
			continue;
		    } elseif (($channelname->channel_name) == 'Service Account' || ($channelname->channel_name) == 'Subscription Account') {
			$name = 'WeChat';
			$store_channel_id = $connectedstore->channel_id;
		    } else {
			$store_channel_id = $connectedstore->channel_id;
			$name = $channelname->channel_name;
		    }
		} elseif (!empty($connectedstore->store_id)) {
		    $fieldname = 'store_id';
		    $store_channel_id = $connectedstore->stores_connection_id;
		    $storename = StoresConnection::find()->where(['stores_connection_id' => $store_channel_id])->with('storesDetails')->one();
		    $name = $storename->storesDetails->channel_accquired;
//                    $storename = Stores::find()->where(['store_id' => $connectedstore->store_id])->one();
//                    $name = $storename->store_name;
		}
		$names_array[] = $name;

		/* for date range */
		if (!empty($fieldname) && !empty($store_channel_id) && !empty($name) && $post == 'dateRange' && !empty($daterange)) {
		    $date = explode('-', $daterange);
		    $startdate = date('Y-m-d', strtotime($date[0]));
		    $enddate = date('Y-m-d', strtotime($date[1]));
		    $currentdate = date('Y-m-d');
		    $connection1 = \Yii::$app->db;
		    $orders_data1 = $connection1->createCommand('SELECT * from order_channel Where ' . $fieldname . '="' . $store_channel_id . '"');
		    $model = $orders_data1->queryAll();
		    if (!empty($model)) {
			$count = count($model);
			foreach ($model as $modeldata) {
			    $connection2 = \Yii::$app->db;
			    $orders_data2 = $connection2->createCommand('SELECT * from orders Where  order_ID="' . $modeldata['order_id'] . '" AND date(created_at) BETWEEN"' . $startdate . '" AND "' . $enddate . '"');
			    $model2 = $orders_data2->queryOne();
			    $sum += intval($model2['total_amount']);
			}
			$arr2[$name] = array_key_exists($name, $arr2) ? $arr2[$name] + intval($sum / $count) : intval($sum / $count);
//                        $arr[] = intval($sum / $count);
		    }
		}
		/* for today */
		if (!empty($fieldname) && !empty($store_channel_id) && !empty($name) && $post == 'day') {
		    $currentdate = date('Y-m-d');
		    $connection1 = \Yii::$app->db;
		    $orders_data1 = $connection1->createCommand('SELECT * from order_channel Where ' . $fieldname . '="' . $store_channel_id . '"');
		    $model = $orders_data1->queryAll();
		    if (!empty($model)) {
			$count = count($model);
			foreach ($model as $modeldata) {
			    $connection2 = \Yii::$app->db;
			    $orders_data2 = $connection2->createCommand('SELECT * from orders Where  order_ID="' . $modeldata['order_id'] . '" AND date(created_at)="' . $currentdate . '"');
			    $model2 = $orders_data2->queryOne();
			    $sum += intval($model2['total_amount']);
			}
			$arr2[$name] = array_key_exists($name, $arr2) ? $arr2[$name] + intval($sum / $count) : intval($sum / $count);
//                        $arr[] = intval($sum / $count);
//                        $namearr[] = $name;
		    }
		}
		/* for week */
		if (!empty($fieldname) && !empty($store_channel_id) && !empty($name) && $post == 'week') {
		    $currentmonth = date('m');
		    $date_check = date('m');
		    for ($i = 1; $i <= 7; $i++) {
			if ($date_check == $currentmonth) {
			    $previous_day = date('Y-m-d', strtotime('-' . $i . 'days'));
			    $date_check = date('m', strtotime($previous_day));
			}
		    }
		    $week_previous_day = date($previous_day, strtotime('+1  days'));
		    $Week_previous_date = date('Y-m-d', strtotime('-7  days'));
		    $currentdate = date('Y-m-d');
		    // $date_check = date('Y-m-d', strtotime($previous_hour));                   
		    $connection = \Yii::$app->db;
		    $orders_data = $connection->createCommand('SELECT * from order_channel Where ' . $fieldname . '="' . $store_channel_id . '"');
		    $model = $orders_data->queryAll();
		    if (!empty($model)) {
			$count = count($model);
			foreach ($model as $modeldata) {
			    $connection2 = \Yii::$app->db;
			    $orders_data2 = $connection2->createCommand('SELECT * from orders Where  order_ID="' . $modeldata['order_id'] . '" AND date(created_at) BETWEEN "' . $Week_previous_date . '" AND "' . $currentdate . '"');
			    $model2 = $orders_data2->queryOne();
			    $sum += intval($model2['total_amount']);
			}
			if ($sum == 0) {
			    continue;
			}
			$arr2[$name] = array_key_exists($name, $arr2) ? $arr2[$name] + intval($sum / $count) : intval($sum / $count);
//                        $arr[] = intval($sum / $count);
//                        $namearr[] = $name;
		    }
		}

		/* for month */
		if (!empty($fieldname) && !empty($store_channel_id) && !empty($name) && $post == 'month') {
		    //$Month_previous_date = date('Y-m-d', strtotime('-30  days'));
		    $final_result = 0;
		    $currentdate = date('Y-m-d');
		    $currentyear = date('Y');
		    $currentmonth = date('m');

		    $connection = \Yii::$app->db;
		    $orders_data = $connection->createCommand('SELECT * from order_channel Where ' . $fieldname . '="' . $store_channel_id . '"');
		    $model = $orders_data->queryAll();

		    if (!empty($model)) {
			$count = count($model);
			foreach ($model as $modeldata) {
			    $connection2 = \Yii::$app->db;
			    $orders_data2 = $connection2->createCommand('SELECT * from orders Where  order_ID="' . $modeldata['order_id'] . '" AND month(created_at)= "' . $currentmonth . '" AND year(created_at)="' . $currentyear . '"');
			    $model2 = $orders_data2->queryOne();
			    $sum += intval($model2['total_amount']);
			}
			$final_result = intval($sum / $count);
			$arr2[$name] = array_key_exists($name, $arr2) ? $arr2[$name] + $final_result : $final_result;

//                        $ARRAY = array('device' => $name, 'geekbench' => $final_result);
//                        $arr[] = intval($sum / $count);
//                        $namearr[] = $name;
		    }
		}
		/* for quarter */
		if (!empty($fieldname) && !empty($store_channel_id) && !empty($name) && $post == 'Quarter') {
		    $currentmonth = date('m');
		    $currentyear = date('Y');
		    $val = $currentmonth - 3;
		    $currentmonth_pre = date("m", mktime(0, 0, 0, $val, 10));
		    $Month_previous_date = date('Y-m-d', strtotime('-30  days'));
		    $currentdate = date('Y-m-d');
		    $connection = \Yii::$app->db;
		    $orders_data = $connection->createCommand('SELECT * from order_channel Where ' . $fieldname . '="' . $store_channel_id . '"');
		    $model = $orders_data->queryAll();
		    if (!empty($model)) {
			$count = count($model);
			foreach ($model as $modeldata) {
			    $connection2 = \Yii::$app->db;
			    $orders_data2 = $connection2->createCommand('SELECT * from orders Where  order_ID="' . $modeldata['order_id'] . '"AND month(created_at) BETWEEN "' . $currentmonth_pre . '" AND "' . $currentmonth . '" AND year(created_at)="' . $currentyear . '"');
			    // $orders_data2 = $connection2->createCommand('SELECT * from orders Where  order_ID="' . $modeldata['order_id'] . '"AND month(created_at)="' . $currentmonth . '"  AND year(created_at)="' . $currentyear . '"');
			    $model2 = $orders_data2->queryOne();
			    $sum += intval($model2['total_amount']);
			}
			$arr2[$name] = array_key_exists($name, $arr2) ? $arr2[$name] + intval($sum / $count) : intval($sum / $count);

//                        $arr[] = intval($sum / $count);
			$namearr[] = $name;
		    }
		}
		/* for year */
		if (!empty($fieldname) && !empty($store_channel_id) && !empty($name) && $post == 'piechartyear' || $post == 'piechartyearmob') {
		    $currentyear = date('Y');
		    $connection = \Yii::$app->db;
		    $orders_data = $connection->createCommand('SELECT * from product_channel Where ' . $fieldname . '="' . $store_channel_id . '"AND year(created_at)= "' . $currentyear . '"');
		    $model = count($orders_data->queryAll());
		    // $model = count(ProductChannel::find()->where([$fieldname => $store_channel_id])->all());
		    $arr[] = array($model);
		    $namearr[] = $name;
		}
	    }

	    $namearr2 = $namearr;
	}

	$arr = array();
	$sum = 0;
	foreach ($arr2 as $key => $val) {
//            $s_val= (string) '$'.$val;
	    $arr[] = array('device' => $key, 'AOV' => $val);
	    $sum += $val;
	}
	if (empty($arr)) {
	    $arr = [];
	    foreach (array_unique($names_array) as $key => $val) {
		$arr[] = array('device' => $val, 'AOV' => '0');
	    }
	}
	return \yii\helpers\Json::encode($arr);
//        if ($sum == 0) {
//            $data = 'invalid';
//            echo $data;die;
////            return \yii\helpers\Json::encode($data);
//        } else {
//            return \yii\helpers\Json::encode($arr);
//        }
	// $sum = 0;
	// foreach ($arr as $val) {
	// $sum += $val;
	// } 
	// if ($sum == 0) {
	// $data = 'invalid';
	// return \yii\helpers\Json::encode($data);
	// }
	$colorarr = array(
	    0 => '#0091ea',
	    1 => '#00b0ff',
	    2 => '#40c4ff',
	    3 => '#80d8ff',
	    4 => '#01579b',
	    5 => '#0277bd',
	    6 => '#039be5',
	    7 => '#03a9f4',
	    8 => '#b3e5fc',
	    9 => '#81d4fa',
	    10 => '#29b6f6',
	    11 => '#e1f5fe',
	    12 => '#b5af79',
	    13 => '#914f84',
	    14 => '#ce6d87',
	    15 => '#e04a72',
	);
	$randcolor = array();
	$randcolor_2 = array();
	$c = 0;
	foreach ($arr as $array1) {
	    $randcolor[] = $colorarr[$c++];
	}
	$randcolor_2 = $randcolor;
	$data = array('color' => json_encode($randcolor_2), 'data' => json_encode($arr));
	return \yii\helpers\Json::encode($data);
    }

    public function actionGetRecentOrders() {
	$user = Yii::$app->user->identity;
	$user_id = Yii::$app->user->identity->id;


	$filtered_channel_connection_id = [];
	$filtered_store_connection_id = [];
	$connectedstore = [];
	$connectedchannel = [];
	$channel_name = '';
	if (isset($_POST['connection_id']) and ! empty($_POST['connection_id'])) {

	    if (isset($_POST['type']) and ! empty($_POST['type'])) {
		if ($_POST['type'] == 'store') {
		    $filtered_store_connection_id = $_POST['connection_id'];
		} elseif ($_POST['type'] == 'channel') {
		    $filtered_channel_connection_id = $_POST['connection_id'];
		    $channel_connection_data = ChannelConnection::find()->where(['channel_connection_id' => $filtered_channel_connection_id])->with('storesDetails')->one();
		    $channel_name = $channel_connection_data->storesDetails->channel_accquired;
		}
	    }
	}

//        print_r($filtered_store_connection_id);


	$order_ids = [];
	if (!empty($filtered_channel_connection_id)) {
	    $connectedchannel = Orders::find()->select('order_ID')->andFilterWhere([
			'and',
			    ['=', 'channel_accquired', $channel_name],
		    ])->andWhere(['elliot_user_id' => $user_id])->asArray()->all();
	    if (isset($connectedchannel) and ! empty($connectedchannel)) {
		foreach ($connectedchannel as $single) {
		    $order_ids[] = $single['order_ID'];
		}
	    }
	} elseif (!empty($filtered_store_connection_id)) {
	    $connectedstore = Orders::find()->select('order_ID')->andFilterWhere([
			'and',
			    ['=', 'mul_store_id', $filtered_store_connection_id],
		    ])->andWhere(['elliot_user_id' => $user_id])->asArray()->all();
	    if (isset($connectedstore) and ! empty($connectedstore)) {
		foreach ($connectedstore as $single) {
		    $order_ids[] = $single['order_ID'];
		}
	    }
	}
	$andWhere = 'andFilterWhere';
	if ((!empty($filtered_channel_connection_id) or ! empty($filtered_store_connection_id))) {
	    $andWhere = 'andWhere';
	    if (isset($order_ids) and ! empty($order_ids)) {
		$andWhere = 'andFilterWhere';
	    }
	}
	$order_ids_query = implode(",", $order_ids);
	$data = $_POST['data'];
	if ($data == 'month') {
	    $orders_data_REcent = Orders::find()
		    ->andWhere(['elliot_user_id' => Yii::$app->user->identity->id])
		    ->andWhere(['=', 'month(created_at)', date('m')])
		    ->andWhere(['=', 'year(created_at)', date('Y')])
		    ->$andWhere([
			'and',
			    ['in', 'order_ID', $order_ids],
		    ])
		    ->with('customer')
		    ->orderBy(['order_ID' => SORT_DESC,])->limit(5)
		    ->all();
	}
	if ($data == 'week') {
	    $orders_data_REcent = Orders::find()
		    ->andWhere(['elliot_user_id' => Yii::$app->user->identity->id])
		    ->andWhere(['=', 'week(created_at)', date('W')])
		    ->andWhere(['=', 'year(created_at)', date('Y')])
		    ->$andWhere([
			'and',
			    ['in', 'order_ID', $order_ids],
		    ])
		    ->with('customer')
		    ->orderBy(['order_ID' => SORT_DESC,])->limit(5)
		    ->all();
	}
	if ($data == 'quarter') {
	    $orders_data_REcent = Orders::find()
		    ->andWhere(['elliot_user_id' => Yii::$app->user->identity->id])
		    ->andWhere(['between', 'created_at', date('Y-m-d', strtotime('-3 months')), date('Y-m-d')])
		    ->$andWhere([
			'and',
			    ['in', 'order_ID', $order_ids],
		    ])
		    ->with('customer')
		    ->orderBy(['order_ID' => SORT_DESC,])->limit(5)
		    ->all();
	}
	if ($data == 'year') {
	    $orders_data_REcent = Orders::find()
		    ->andWhere(['elliot_user_id' => Yii::$app->user->identity->id])
		    ->andWhere(['=', 'year(created_at)', date('Y')])
		    ->$andWhere([
			'and',
			    ['in', 'order_ID', $order_ids],
		    ])
		    ->with('customer')
		    ->orderBy(['order_ID' => SORT_DESC,])->limit(5)
		    ->all();
	}

	if (!empty($_POST['daterange'])) {
	    $daterange = $_POST['daterange'];
	}
	if ($data == 'dateRange' and ! empty($daterange)) {
	    $date = explode('-', $daterange);
	    $startdate = date('Y-m-d', strtotime($date[0]));
	    $enddate = date('Y-m-d', strtotime($date[1]));
	    $orders_data_REcent = Orders::find()
		    ->andWhere(['elliot_user_id' => Yii::$app->user->identity->id])
		    ->andWhere(['between', 'created_at', date('Y-m-d', strtotime($startdate)), date('Y-m-d', strtotime($enddate))])
		    ->$andWhere([
			'and',
			    ['in', 'order_ID', $order_ids],
		    ])
		    ->with('customer')
		    ->orderBy(['order_ID' => SORT_DESC,])->limit(5)
		    ->all();
	}
	if ($data == 'today') {
	    $orders_data_REcent = Orders::find()
		    ->andWhere(['elliot_user_id' => Yii::$app->user->identity->id])
		    ->andWhere(['=', 'date(created_at)', date('Y-m-d')])
		    ->$andWhere([
			'and',
			    ['in', 'order_ID', $order_ids],
		    ])
		    ->with('customer')
		    ->orderBy(['order_ID' => SORT_DESC,])->limit(5)
		    ->all();
	}

	return $this->renderAjax('recentorder', [
		    'orders_data_REcent' => $orders_data_REcent,
		    'user' => $user
			]
	);
    }

    public function actionGetLatestEnagagements() {


	$user = Yii::$app->user->identity;
	$user_id = Yii::$app->user->identity->id;
	$filtered_channel_connection_id = [];
	$filtered_store_connection_id = [];
	$connectedstore = [];
	$connectedchannel = [];
	$channel_name = '';
	if (isset($_POST['connection_id']) and ! empty($_POST['connection_id'])) {

	    if (isset($_POST['type']) and ! empty($_POST['type'])) {
		if ($_POST['type'] == 'store') {
		    $filtered_store_connection_id = $_POST['connection_id'];
		} elseif ($_POST['type'] == 'channel') {
		    $filtered_channel_connection_id = $_POST['connection_id'];
		    $channel_connection_data = ChannelConnection::find()->where(['channel_connection_id' => $filtered_channel_connection_id])->with('storesDetails')->one();
		    $channel_name = $channel_connection_data->storesDetails->channel_accquired;
		}
	    }
	}

	$order_ids = [];
	$product_ids = [];
	$customer_ids = [];
	if (!empty($filtered_channel_connection_id)) {
	    $connectedchannel = Orders::find()->select('order_ID')->andFilterWhere([
			'and',
			    ['=', 'channel_accquired', $channel_name],
		    ])->andWhere(['elliot_user_id' => $user_id])->asArray()->all();
	    if (isset($connectedchannel) and ! empty($connectedchannel)) {
		foreach ($connectedchannel as $single) {
		    $order_ids[] = $single['order_ID'];
		}
	    }
	    $connectedchannel_products = ProductChannel::find()->select('product_id')->andFilterWhere([
			'and',
			    ['=', 'channel_id', $channel_connection_data->channel_id],
		    ])->andWhere(['elliot_user_id' => $user_id])->asArray()->all();
	    if (isset($connectedchannel_products) and ! empty($connectedchannel_products)) {
		foreach ($connectedchannel_products as $single) {
		    $product_ids[] = $single['product_id'];
		}
	    }
	    $connectedchannel_customers = CustomerAbbrivation::find()->select('customer_id')->andFilterWhere([
			'and',
			    ['=', 'channel_accquired', $channel_name],
		    ])->asArray()->all();
	    if (isset($connectedchannel_customers) and ! empty($connectedchannel_customers)) {
		foreach ($connectedchannel_customers as $single) {
		    $customer_ids[] = $single['customer_id'];
		}
	    }
	} elseif (!empty($filtered_store_connection_id)) {
	    $connectedstore = Orders::find()->select('order_ID')->andFilterWhere([
			'and',
			    ['=', 'mul_store_id', $filtered_store_connection_id],
		    ])->andWhere(['elliot_user_id' => $user_id])->asArray()->all();
	    if (isset($connectedstore) and ! empty($connectedstore)) {
		foreach ($connectedstore as $single) {
		    $order_ids[] = $single['order_ID'];
		}
	    }

	    $connectedstore_products = ProductChannel::find()->select('product_id')->andFilterWhere([
			'and',
			    ['=', 'store_id', $filtered_store_connection_id],
		    ])->andWhere(['elliot_user_id' => $user_id])->asArray()->all();
	    if (isset($connectedstore_products) and ! empty($connectedstore_products)) {
		foreach ($connectedstore_products as $single) {
		    $product_ids[] = $single['product_id'];
		}
	    }
	    $connectedstore_customers = CustomerAbbrivation::find()->select('customer_id')->andFilterWhere([
			'and',
			    ['=', 'mul_store_id', $filtered_store_connection_id],
		    ])->asArray()->all();
	    if (isset($connectedstore_customers) and ! empty($connectedstore_customers)) {
		foreach ($connectedstore_customers as $single) {
		    $customer_ids[] = $single['customer_id'];
		}
	    }
	}
	$andWhere = 'andFilterWhere';
	if ((!empty($filtered_channel_connection_id) or ! empty($filtered_store_connection_id))) {
	    $andWhere = 'andWhere';
	    if (isset($order_ids) and ! empty($order_ids)) {
		$andWhere = 'andFilterWhere';
	    }
	}
	$order_ids_query = implode(",", $order_ids);
	$data = $_POST['data'];

	if ($data == 'month') {
	    $customer_eng_data = CustomerUser::find()
			    ->andWhere(['elliot_user_id' => Yii::$app->user->identity->id])
			    ->andWhere(['=', 'month(created_at)', date('m')])
			    ->andWhere(['=', 'year(created_at)', date('Y')])
			    ->$andWhere([
				'and',
				    ['in', 'customer_ID', $customer_ids],
			    ])->asArray()->all();
	    $products_eng_data = Products::find()->andWhere(['elliot_user_id' => Yii::$app->user->identity->id])
			    ->andWhere(['=', 'month(created_at)', date('m')])
			    ->andWhere(['=', 'year(created_at)', date('Y')])
			    ->$andWhere([
				'and',
				    ['in', 'id', $product_ids],
			    ])->asArray()->all();
	    $orders_eng_data = Orders::find()
		    ->andWhere(['elliot_user_id' => Yii::$app->user->identity->id])
		    ->andWhere(['=', 'month(created_at)', date('m')])
		    ->andWhere(['=', 'year(created_at)', date('Y')])
		    ->$andWhere([
			'and',
			    ['in', 'order_ID', $order_ids],
		    ])->asArray()
		    ->all();
	}
	if ($data == 'week') {

	    $customer_eng_data = CustomerUser::find()
			    ->andWhere(['elliot_user_id' => Yii::$app->user->identity->id])
			    ->andWhere(['=', 'week(created_at)', date('W')])
			    ->andWhere(['=', 'year(created_at)', date('Y')])
			    ->$andWhere([
				'and',
				    ['in', 'customer_ID', $customer_ids],
			    ])->asArray()->all();
	    $products_eng_data = Products::find()
			    ->andWhere(['elliot_user_id' => Yii::$app->user->identity->id])
			    ->andWhere(['=', 'week(created_at)', date('W')])
			    ->andWhere(['=', 'year(created_at)', date('Y')])
			    ->$andWhere([
				'and',
				    ['in', 'id', $product_ids],
			    ])->asArray()->all();
	    $orders_eng_data = Orders::find()
		    ->andWhere(['elliot_user_id' => Yii::$app->user->identity->id])
		    ->andWhere(['=', 'week(created_at)', date('W')])
		    ->andWhere(['=', 'year(created_at)', date('Y')])
		    ->$andWhere([
			'and',
			    ['in', 'order_ID', $order_ids],
		    ])->asArray()
		    ->all();
	}
	if ($data == 'quarter') {

	    $customer_eng_data = CustomerUser::find()
			    ->andWhere(['elliot_user_id' => Yii::$app->user->identity->id])
			    ->andWhere(['between', 'created_at', date('Y-m-d', strtotime('-3 months')), date('Y-m-d')])
			    ->$andWhere([
				'and',
				    ['in', 'customer_ID', $customer_ids],
			    ])->asArray()->all();
	    $products_eng_data = Products::find()
			    ->andWhere(['elliot_user_id' => Yii::$app->user->identity->id])
			    ->andWhere(['between', 'created_at', date('Y-m-d', strtotime('-3 months')), date('Y-m-d')])
			    ->$andWhere([
				'and',
				    ['in', 'id', $product_ids],
			    ])->asArray()->all();
	    $orders_eng_data = Orders::find()
		    ->andWhere(['elliot_user_id' => Yii::$app->user->identity->id])
		    ->andWhere(['between', 'created_at', date('Y-m-d', strtotime('-3 months')), date('Y-m-d')])
		    ->$andWhere([
			'and',
			    ['in', 'order_ID', $order_ids],
		    ])->asArray()
		    ->all();
	}
	if ($data == 'year') {
	    $customer_eng_data = CustomerUser::find()
			    ->andWhere(['elliot_user_id' => Yii::$app->user->identity->id])
			    ->andWhere(['=', 'year(created_at)', date('Y')])
			    ->$andWhere([
				'and',
				    ['in', 'customer_ID', $customer_ids],
			    ])->asArray()->all();
	    $products_eng_data = Products::find()
			    ->andWhere(['elliot_user_id' => Yii::$app->user->identity->id])
			    ->andWhere(['=', 'year(created_at)', date('Y')])
			    ->$andWhere([
				'and',
				    ['in', 'id', $product_ids],
			    ])->asArray()->all();
	    $orders_eng_data = Orders::find()
		    ->andWhere(['elliot_user_id' => Yii::$app->user->identity->id])
		    ->andWhere(['=', 'year(created_at)', date('Y')])
		    ->$andWhere([
			'and',
			    ['in', 'order_ID', $order_ids],
		    ])->asArray()
		    ->all();
	}

	if (!empty($_POST['daterange'])) {
	    $daterange = $_POST['daterange'];
	}
	if ($data == 'dateRange' and ! empty($daterange)) {
	    $date = explode('-', $daterange);
	    $startdate = date('Y-m-d', strtotime($date[0]));
	    $enddate = date('Y-m-d', strtotime($date[1]));
	    $customer_eng_data = CustomerUser::find()
			    ->andWhere(['elliot_user_id' => Yii::$app->user->identity->id])
			    ->andWhere(['between', 'created_at', date('Y-m-d', strtotime($startdate)), date('Y-m-d', strtotime($enddate))])
			    ->$andWhere([
				'and',
				    ['in', 'customer_ID', $customer_ids],
			    ])->asArray()->all();
	    $products_eng_data = Products::find()
			    ->andWhere(['elliot_user_id' => Yii::$app->user->identity->id])
			    ->andWhere(['between', 'created_at', date('Y-m-d', strtotime($startdate)), date('Y-m-d', strtotime($enddate))])
			    ->$andWhere([
				'and',
				    ['in', 'id', $product_ids],
			    ])->asArray()->all();
	    $orders_eng_data = Orders::find()
		    ->andWhere(['elliot_user_id' => Yii::$app->user->identity->id])
		    ->andWhere(['between', 'created_at', date('Y-m-d', strtotime($startdate)), date('Y-m-d', strtotime($enddate))])
		    ->$andWhere([
			'and',
			    ['in', 'order_ID', $order_ids],
		    ])->asArray()
		    ->all();
	}
	if ($data == 'today') {
	    $customer_eng_data = CustomerUser::find()
			    ->andWhere(['elliot_user_id' => Yii::$app->user->identity->id])
			    ->andWhere(['=', 'date(created_at)', date('Y-m-d')])
			    ->$andWhere([
				'and',
				    ['in', 'customer_ID', $customer_ids],
			    ])->asArray()->all();
	    $products_eng_data = Products::find()
			    ->andWhere(['elliot_user_id' => Yii::$app->user->identity->id])
			    ->andWhere(['=', 'date(created_at)', date('Y-m-d')])
			    ->$andWhere([
				'and',
				    ['in', 'id', $product_ids],
			    ])->asArray()->all();
	    $orders_eng_data = Orders::find()
		    ->andWhere(['elliot_user_id' => Yii::$app->user->identity->id])
		    ->andWhere(['=', 'date(created_at)', date('Y-m-d')])
		    ->$andWhere([
			'and',
			    ['in', 'order_ID', $order_ids],
		    ])->asArray()
		    ->all();
	}
	/* Latest Engagements */
	$late_engag_arr = array_merge($customer_eng_data, $products_eng_data, $orders_eng_data);

	$sort = array();
	foreach ($late_engag_arr as $k => $v) {
	    $sort['created_at'][$k] = $v['created_at'];
	}
	if (!empty($sort['created_at'])) {
	    array_multisort($sort['created_at'], SORT_DESC, $late_engag_arr);
	}

	$latest_engagements = array_slice($late_engag_arr, 0, 5);

	return $this->renderAjax('latestengagements', [
		    'lates_engagements' => $latest_engagements,
		    'user' => $user
			]
	);
    }

}
