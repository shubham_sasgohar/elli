<?php

namespace backend\controllers;

use Yii;
use backend\models\CronTasks;
use backend\models\CronTasksSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Stores;
use backend\models\User;
use common\models\CustomFunction;
use backend\models\Notification;
use backend\models\StoresConnection;
use backend\models\Smartling;
use Smartling\AuthApi;
use Smartling\Exceptions\SmartlingApiException;
use Smartling\File\FileApi;
use Smartling\Jobs\JobsApi;
use Smartling\Jobs\Params\AddFileToJobParameters;
use Smartling\Jobs\Params\AddLocaleToJobParameters;
use Smartling\Jobs\Params\CancelJobParameters;
use backend\models\BillingInvoice;
use backend\models\Channelsetting;
use backend\models\SmartlingPrice;

/**
 * CronTasksController implements the CRUD actions for CronTasks model.
 */
class CronTasksController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CronTasks models.
     * @return mixed
     */
    public function actionIndex() {
        $pending_tasks = CronTasks::find()->where(['status' => 'Pending'])->all();
        foreach ($pending_tasks as $task):
            $uid = $task->elliot_user_id;
            $user = User::find()->where(['id' => $uid])->one();
            $user_domain = $user->domain_name;
            $task_name = $task->task_name;
            $task_source = $task->task_source;
            $config = yii\helpers\ArrayHelper::merge(
                            require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $user_domain . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
            );
            $application = new yii\web\Application($config);
            if ($task_name == 'Import'):
                //Import Data
                if ($task_source == 'BigCommerce'):
                    //Import the BigCommerce Store Data          
                    $get_notify = Stores::bigcommerce_initial_import($user);
                    //If Import done successfully
                    if ($get_notify == 'true'):
                        //Send Notification Email
                        $email = $user->email;
                        $company_name = $user->company_name;
                        $send_email_notif = CustomFunction::ConnectBigCommerceEmail($email, $company_name);
                        //Drop-Down Notification for User
                        $notif_type = 'BigCommerce';
                        $notif_db = Notification::find()->Where(['notif_type' => $notif_type])->one();
                        if (empty($notif_db)):
                            $notification_model = new Notification();
                            $notification_model->user_id = $user->id;
                            $notification_model->notif_type = $notif_type;
                            $notification_model->notif_description = 'Your BigCommerce store data has been successfully imported.';
                            $notification_model->created_at = date('Y-m-d h:i:s', time());
                            $notification_model->save(false);
                        endif;
                        //Sticky Notification :StoresConnection Model Entry for Import Status
                        $get_rec = StoresConnection::find()->Where(['user_id' => $user->id, 'store_id' => 1])->one();
                        $get_rec->import_status = 'Completed';
                        $get_rec->save(false);
                    endif;
                    //Update the Status of Task in Main DB
                    $config = yii\helpers\ArrayHelper::merge(
                                    require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
                    );
                    $application = new yii\web\Application($config);
                    $get_task = CronTasks::find()->where(['task_id' => $task->task_id])->one();
                    if (!empty($get_task)):
                        $get_task->status = 'Completed';
                        $get_task->updated_at = date('Y-m-d h:i:s', time());
                        $get_task->save(false);
                    endif;
                endif;
            endif;

        endforeach;

//        $searchModel = new CronTasksSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//
//        return $this->render('index', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);
    }

    /**
     * Displays a single CronTasks model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CronTasks model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new CronTasks();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->task_id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CronTasks model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->task_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CronTasks model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CronTasks model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CronTasks the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = CronTasks::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /* SmartLing Task */

    public function actionSmartlingCron() {
        $basedir = Yii::getAlias('@basedir');
        //use for baseurl//
        $baseurl = Yii::getAlias('@baseurl');
        $stripe_init_file = $basedir . '/stripe/init.php';
		 require $stripe_init_file;
		\Stripe\Stripe::setApiKey("sk_test_2GPaBt5mT2k0yyTA9ho3CPIQ");
		
        $basedir = Yii::getAlias('@basedir');
        require_once $basedir . '/example-jobs-api/vendor/autoload.php';
        /* For Connecting The Elli Database */
        $config = yii\helpers\ArrayHelper::merge(
            require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'),
            require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main-local.php'),
            require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'),
            require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php'));

        /* Craete Config oBject */
        $application = new yii\web\Application($config);

        /* Get Elli All Users */
        $Elli_user = User::find()->Where(['smartling_status' => 'active'])->all();
		
		
        foreach ($Elli_user as $Elli_user_data) {
			
			$customer_token = $Elli_user_data->customer_token;
			$email = $Elli_user_data->email;
			$id = $Elli_user_data->id;
	
            $user_domain = $Elli_user_data->domain_name;
            $config = yii\helpers\ArrayHelper::merge(
                require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'),
                require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $user_domain . '/main-local.php'),
                require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'),
                require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
            );
            $application = new yii\web\Application($config);

            $smartling = Smartling::find()->Where(['<>', 'token', ''])
                        ->andWhere(['connected'=>'yes'])->all();
			
            
            foreach($smartling as $smartling_data){
                $user_id        =   $smartling_data->elliot_user_id;
				//if($user_id == '2016'){
                $token_name     =   $smartling_data->token;
                $folder_name    =   $user_domain.'_'.$user_id;
                $project_id     =   $smartling_data->project_id;
                $locale         =   $smartling_data->target_locale;
                $userSecretKey  =   $smartling_data->secret_key;
                $userIdentifier =   $smartling_data->sm_user_id;
                $fileName       =   $smartling_data->job_callbackUrl;
                $jobId       =   $smartling_data->job_translationJobUid;
                $project_type_value       =   $smartling_data->project_type_value;
                $Channelsetting = Channelsetting::find()->Where(['user_id'=>$user_id,'channel_name'=>$token_name,'setting_key'=>'Price'])->one();
				 $ChannelsettingCost = Channelsetting::find()->Where(['user_id'=>$user_id,'channel_name'=>$token_name,'setting_key'=>'Translationtype'])->one();
				$price = 1234;
				if(!empty($Channelsetting)){
				$price = $Channelsetting->setting_value;
				}
				if(!empty($ChannelsettingCost)){
				$PriceTaken = $ChannelsettingCost->setting_value;
				}
                 $projectId      = $project_id;
                $retrievalType  = 'published';
                $locale         = $locale;
                $userSecretKey  = $userSecretKey;
                $userIdentifier = $userIdentifier;
                $fileName       = $fileName; 
				/* $projectId      = '7289bfb87';
                $retrievalType  = 'pseudo';
                $locale         = 'zh-CN';
                $userSecretKey  = '2anhc34el124ngi7pd46epc21kVM_ui3vgsmu1usvlshgfnva3viu6j';
                $userIdentifier = 'bhabaskqrnadgqgarxhuhawydiicml';
                $fileName       = 'test2.json'; */
                
                
                $domain_folder_path = $basedir . '/img/uploaded_files/smartling_files' . '/' . $folder_name;
                if (is_dir($domain_folder_path)) {
                // is_dir - tells whether the filename is a directory
                    $create_token_folder=$domain_folder_path.'/'.$token_name;
                    $token_file=$create_token_folder . '/'.$token_name.'.json';
                    if (!file_exists($create_token_folder)) {
                        
                        mkdir($create_token_folder);
                        $create_token_file= fopen($token_file, "w") or die("Unable to open file!");
                        /*Call Smartling file Download Function*/
                        $this->SmartlingFileDownload($userIdentifier, $userSecretKey, $projectId, $retrievalType,$fileName,$locale,$token_file);
                        
                    }else{
                       $this->SmartlingFileDownload($userIdentifier, $userSecretKey, $projectId, $retrievalType,$fileName,$locale,$token_file);
                     
                    }
                   
                } 
				$smartling12 = Smartling::find()->Where(['project_id'=>$projectId])->one();
				$job_referenceNumber = $smartling12->job_referenceNumber;
				if(empty($job_referenceNumber)){
					if(!empty($project_type_value)){
						if($PriceTaken == 'Google MT'){
					$selectRate = 'rate2';
					$SmartlingPrice = SmartlingPrice::find()->Where(['locale_id' => $locale])->one();
					$priceUnitCost =  $SmartlingPrice->rate2;
					} else if($PriceTaken == 'Translation with Edit'){
						$selectRate = 'editing';
						$SmartlingPrice = SmartlingPrice::find()->Where(['locale_id' => $locale])->one();
						$priceUnitCost =  $SmartlingPrice->editing;
					}else{
						$priceUnitCost =  1;
					}
					$data = $price;
					$this->Subscriptionpaymentcustom($userIdentifier, $userSecretKey,$project_id, $jobId, $customer_token, $email, $user_domain, $id, $priceUnitCost, $project_type_value);
					}
					//$smartling12 = Smartling::find()->Where(['project_id'=>$projectId])->one();
					$smartling12->job_referenceNumber = 'Done';
					$smartling12->save(false);
				}
           // }
		}
            
           // $data = str_word_count($result); 
		
			echo $user_id; echo '</br>';
            
            //$this->SmartlingFileDownload($userIdentifier, $userSecretKey, $projectId, $retrievalType);
        }
    }
	
    /* Smartling file Download Function */

    public function SmartlingFileDownload($userIdentifier, $userSecretKey, $projectId, $retrievalType,$fileName,$locale,$token_file) {

        /**
         * Download file example
         */
        try {
            //echo '::: File Download Example :::' . PHP_EOL;

            $authProvider = \Smartling\AuthApi\AuthTokenProvider::create($userIdentifier, $userSecretKey);

            $fileApi = \Smartling\File\FileApi::create($authProvider, $projectId);

            $params = new \Smartling\File\Params\DownloadFileParameters();
            $params->setRetrievalType($retrievalType);

            $result = $fileApi->downloadFile($fileName, $locale, $params);
           // echo 'File download result:' . PHP_EOL;
            //echo var_export($result, true) . PHP_EOL . PHP_EOL;
            
            file_put_contents($token_file, $result);
			return $result;
        } catch (\Smartling\Exceptions\SmartlingApiException $e) {
            $messageTemplate = 'Error happened while downloading file.' . PHP_EOL
                    . 'Response code: %s' . PHP_EOL
                    . 'Response message: %s' . PHP_EOL;

            echo vsprintf(
                    $messageTemplate, [
                $e->getCode(),
                $e->getMessage(),
                    ]
            );
        }
    }
	
	public function Subscriptionpaymentcustom($userIdentifier, $userSecretKey, $project_id, $jobId, $customer_token, $email, $user_domain, $id, $priceUnitCost, $project_type_value) {
        /* get current user login id */
        $user_id = $id;
        $user_domain = $user_domain;
        //use for base directory//
		
		//echo $userIdentifier . '---'. $userSecretKey; die();
		
		
       $data =  $this->actionNewtest($userIdentifier, $userSecretKey, $project_id, $jobId, $project_type_value);
	    $priceUnitCost;
	    $data = $data*$priceUnitCost*100;
		echo  $data = intval($data); 
		echo '</br>';
		if(!empty($data)){
			 $invoice_name = $user_id.'_'.$user_domain.'_'.$customer_token;
		   
		 //   $name = $request->post('name');
			if(!empty($customer_token)){
			try {
				
			
			/* 	  // Create a Customer:
					$customer = \Stripe\Customer::create(array(
					  "email" => 'brstdev13@gmail.com',
					  "source" => $customer_token,
					)); */

					// Charge the Customer instead of the card:
					$charge = \Stripe\Charge::create(array(
					  "amount" => $data,
					  "currency" => "usd",
					  "customer" => $customer_token
					));
				 $stripe_payment_Status = $charge->status;
				echo $stripe_plan_id = $charge->id;
				if ($stripe_payment_Status == 'succeeded') {

					/* Starts Save Billing Invoice */
					$billing_invoice_model = new BillingInvoice();
					$billing_invoice_model->elliot_user_id = $user_id;
					$billing_invoice_model->stripe_id = $stripe_plan_id;
					$billing_invoice_model->customer_email = $email;
					$billing_invoice_model->invoice_name = $invoice_name;
					$billing_invoice_model->amount = $data;
					$billing_invoice_model->status = $stripe_payment_Status;
					$billing_invoice_model->created_at = date('Y-m-d h:i:s', time());
					$billing_invoice_model->save(false);
					/* End Save Billing Invoice */
					}
				/* echo $token->id;
				echo '<pre>'; print_r($customer); echo '</pre>';
				echo '<pre>'; print_r($charge); */
 
				
				} 
				catch (Exception $e) {
				var_dump($e->getMessage());
				}
				
				//Create Customer
			   

			}
			else{
				echo $invoice_name.'</br>';
			}
		}
	}
	public function actionNewtest($userIdentifier, $userSecretKey, $project_id, $jobId, $project_type_value){
		  $data_string = '{
						  "userIdentifier": "'.$userIdentifier.'",
						  "userSecret": "'.$userSecretKey.'"
						}'; 
						
                $curl = curl_init();
                $url = 'https://api.smartling.com/auth-api/v2/authenticate';
                curl_setopt_array($curl, array(
                    CURLOPT_SSL_VERIFYPEER => false,
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 100,
                    CURLOPT_TIMEOUT => 900,
                    CURLOPT_SSL_VERIFYHOST => FALSE,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => $data_string,
                    CURLOPT_HTTPHEADER => array(
                        "cache-control: no-cache",
                        "charset: utf-8",
                        "X-Auth-Token: BfcqrhanfJghxYZQe8PtDhUMuQHaPtqqRuQZtpZE&jkfnkCfeRBVXLsiBqFdXDaB",
                        "content-type: application/json"
                    ),
                ));

                $resultAuth = curl_exec($curl);

               // echo '<pre>'; print_r(json_decode($resultAuth)); die();
                $authData = json_decode($resultAuth);

                 $access_token = $authData->response->data->accessToken;
		
		//​/estimates-api/v2/projects/{projectId}/reports/{reportUid}/status
			$curl = curl_init();
    $url = 'https://api.smartling.com/estimates-api/v2/projects/'.$project_id.'/reports/'.$project_type_value; 
	//echo '</br>';
	//echo $url = 'https://api.smartling.com/estimates-api/v2/projects/5ecfd37f7/reports/7pcbcl422dol';
            curl_setopt_array($curl, array(
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 100,
                CURLOPT_TIMEOUT => 900,
                CURLOPT_SSL_VERIFYHOST => FALSE,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
              
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "charset: utf-8",
                    "Authorization: Bearer ".$access_token,
                    "content-type: application/json"
                ),
            ));

            $result2 = curl_exec($curl); 
			$datareport = json_decode($result2);
			// echo '<pre>'; print_r($datareport);
			/*die(); */
			 $totalWeightedWordCount = $datareport->response->data->totalWeightedWordCount; 
			return $totalWeightedWordCount;
		//echo '<pre>'; print_r(json_decode($result2));
	}

}
