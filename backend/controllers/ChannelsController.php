<?php

namespace backend\controllers;

use Yii;
use backend\models\User;
use backend\models\Channels;
use backend\models\Channelsetting;
use backend\models\ChannelsSearch;
use backend\models\ChannelConnection;
use backend\models\CustomerUser;
use backend\models\CustomerAbbrivation;
use yii\web\Controller;
use backend\controllers\IntegrationsController as Integrations;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\StoresConnection;
use backend\models\Stores;
use backend\models\Categories;
use backend\models\CategoryAbbrivation;
use backend\models\Products;
use backend\models\ProductImages;
use backend\models\ProductChannel;
use backend\models\MerchantProducts;
use backend\models\ProductAbbrivation;
use backend\models\Variations;
use backend\models\ProductVariation;
use backend\models\VariationsItemList;
use backend\models\ProductCategories;
use backend\models\Orders;
use backend\models\OrdersProducts;
use backend\models\OrderChannel;
use backend\models\Notification;
use backend\models\CronTasks;
use Bigcommerce\Api\Client as Bigcommerce;
use yii\helpers\Html;
use backend\models\MagentoStores;
use backend\models\Countries;
use backend\models\StoreDetails;
use backend\models\CurrencyConversion;

/**
 * ChannelsController implements the CRUD actions for Channels model.
 */
class ChannelsController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
	return [
	    'access' => [
		'class' => AccessControl::className(),
		'only' => ['index', 'view', 'create', 'update', 'wechat', 'flipkartconnect', 'wechatconnect', 'wechatimport', 'test'],
		'rules' => [
			[
			'actions' => ['signup', 'wechatimport', 'test'],
			'allow' => true,
			'roles' => ['?'],
		    ],
			[
			'actions' => ['logout', 'index', 'view', 'create', 'update', 'wechat', 'flipkartconnect', 'wechatconnect', 'wechatimport', 'test'],
			'allow' => true,
			'roles' => ['@'],
		    ],
		],
	    ],
	    'verbs' => [
		'class' => VerbFilter::className(),
		'actions' => [
		    'delete' => ['POST'],
		],
	    ],
	];
    }

    /**
     * Lists all Channels models.
     * @return mixed
     */
    public function actionIndex() {
	$searchModel = new ChannelsSearch();
	$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

	return $this->render('index', [
		    'searchModel' => $searchModel,
		    'dataProvider' => $dataProvider,
	]);
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action) {
	$this->enableCsrfValidation = false;
	return parent::beforeAction($action);
    }

    /**
     * Displays a single Channels model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
	return $this->render('view', [
		    'model' => $this->findModel($id),
	]);
    }

    /**
     * Creates a new Channels model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
	$model = new Channels();

	if ($model->load(Yii::$app->request->post()) && $model->save()) {
	    return $this->redirect(['view', 'id' => $model->channel_ID]);
	} else {
	    return $this->render('create', [
			'model' => $model,
	    ]);
	}
    }

    /**
     * Updates an existing Channels model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
	$model = $this->findModel($id);

	if ($model->load(Yii::$app->request->post()) && $model->save()) {
	    return $this->redirect(['view', 'id' => $model->channel_ID]);
	} else {
	    return $this->render('update', [
			'model' => $model,
	    ]);
	}
    }

    /**
     * Deletes an existing Channels model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
	$this->findModel($id)->delete();

	return $this->redirect(['index']);
    }

    /**
     * Finds the Channels model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Channels the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
	if (($model = Channels::findOne($id)) !== null) {
	    return $model;
	} else {
	    throw new NotFoundHttpException('The requested page does not exist.');
	}
    }

    /**
     * Intigrate flipkart with the system
     */
    public function actionFlipkartconnect() {
	return $this->render('flipkartconnect');
    }

    /**
     * Request for WeChat store
     */
    public function actionWechat() {

	$dt = Yii::$app->request->post();
	if (!empty($dt)) {

	    $storename = $dt['storename'];
	    $password = $dt['password'];
	    $wechat_type = isset($dt['wechat_type']) ? $dt['wechat_type'] : '';
	    $email = Yii::$app->user->identity->email;
	    $company_name = ucfirst(Yii::$app->user->identity->company_name);
	    $email2 = 'helloiamelliot@studio86.co';
	    $ml1 = Yii::$app->mailer->compose()
		    ->setFrom('elliot@helloiamelliot.com')
		    ->setTo($email)
		    ->attach('img/email_docs/Aus_NZ_Dinpay_intro_email_v.pdf')
		    ->attach('img/email_docs/Dinpay_Cross_border_Ecommerce_Application_Form_for_Merchant.pdf')
		    ->attach('img/email_docs/Dinpay_Merchant_Agreement_cross_border_ecommerce.docx')
		    ->setSubject('Next steps to complete your Elliot/WeChat integration')
		    ->setHtmlBody(
			    '<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="background-color:#fcfcfc;padding: 60px 10px">
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="1" cellspacing="0" width="600" id="emailContainer" style="background-color:#fff;padding:25px;-webkit-box-shadow: 0px 5px 30px 0px rgba(50, 50, 50, 0.2);-moz-box-shadow:0px 5px 30px 0px rgba(50, 50, 50, 0.2);box-shadow:0px 5px 30px 0px rgba(50, 50, 50, 0.2);">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailHeader">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <center><img src="http://beta.brstdev.com/adrianna/elliot-logo.jpg" alt="Logo13" title="Logo" style="display:block; width: 100px;"/></center>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellpadding="1" cellspacing="0" width="100%" id="emailBody">
                                                <tr>
                                                    <td valign="top">
                                                        <p style="color: #000000;text-align: left;font-family: OpenSans;font-size: 14.0px;font-style: normal;font-stretch: normal;font-weight: 400;">
                                                            ' . $company_name . ',
                                                       </p>                                                    
                                                       </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                        <p style="color: #000000;text-align: left;font-family: OpenSans;font-size: 14.0px;font-style: normal;font-stretch: normal;font-weight: 400;line-height:25px;">
                                                            Thank you for subscribing to WeChat Stores via Elliot. An Elliot representative will be in touch with you within 24 hours to complete your on-boarding. In the mean time, please view and 
                                                            complete the attached documents. *Attach the Dinpay agreements attached in this email
                                                        </p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellpadding="1" cellspacing="0" width="100%" id="emailFooter">
                                                    <tr>
                                                        <tdvalign="top">
                                                           <p>Thank you,<br>
                                                            <b>Elliot</b></p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>'
		    )
		    ->send();

	    $ml2 = Yii::$app->mailer->compose()
		    ->setFrom('elliot@helloiamelliot.com')
		    ->setTo($email2)
		    ->setSubject('Request For Elliot/WeChat integration')
		    ->setHtmlBody(
			    '<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="background-color:#fcfcfc;padding: 60px 10px">
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="1" cellspacing="0" width="600" id="emailContainer" style="background-color:#fff;padding:25px;-webkit-box-shadow: 0px 5px 30px 0px rgba(50, 50, 50, 0.2);-moz-box-shadow:0px 5px 30px 0px rgba(50, 50, 50, 0.2);box-shadow:0px 5px 30px 0px rgba(50, 50, 50, 0.2);">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailHeader">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <center><img src="http://beta.brstdev.com/adrianna/elliot-logo.jpg" alt="Logo13" title="Logo" style="display:block; width: 100px;"/></center>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellpadding="1" cellspacing="0" width="100%" id="emailBody">
                                                <tr>
                                                    <td valign="top">
                                                        <p style="color: #000000;text-align: left;font-family: OpenSans;font-size: 14.0px;font-style: normal;font-stretch: normal;font-weight: 400;">
                                                            ' . $company_name . ',
                                                       </p>                                                    
                                                       </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                        <p style="color: #000000;text-align: left;font-family: OpenSans;font-size: 14.0px;font-style: normal;font-stretch: normal;font-weight: 400;line-height:25px;">
                                                            <p>There is a new request to create store on walkthechat.com from user ' . $email . ' . Following are the details</p>Suggested Store Name: ' . $storename . ' <br>Suggested Password: ' . $password . ' <br>Store Type: ' . $wechat_type . '"
                                                        </p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellpadding="1" cellspacing="0" width="100%" id="emailFooter">
                                                    <tr>
                                                        <tdvalign="top">
                                                           <p>Thank you,<br>
                                                            <b>Elliot</b></p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>'
		    )
		    ->send();

	    if ($ml1) {
		$store_type = Channels::find()->where(['channel_name' => $wechat_type])->one();
		$channel_connection = new ChannelConnection();
		$channel_connection->user_id = Yii::$app->user->identity->id;
		$channel_connection->channel_id = $store_type->channel_ID;
		$channel_connection->save();

		echo true;
		die;
	    } else {
		echo false;
		die;
	    }
	}
	return $this->render('wechat');
    }

    /**
     * Intigrate wechat store with the system
     */
    public function actionWechatconnect() {



	$dt = Yii::$app->request->post();

	/* Merchant DB Config Object */
	if (!empty($dt)) {
	    if (isset($dt['already'])) {
		$channel_big = Channels::find()->where(['channel_name' => $dt['type']])->one();
		if (!empty($channel_big)) {
		    $dt['type'] = $channel_big->channel_ID;
		}
	    }
	    //$dt['email']=isset($dt['already'])?$email=Yii::$app->user->identity->email:$dt['email'];
	    $curr_url = explode(".", "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
	    $uname = $curr_url[0];
	    $con = User::find()->where(['domain_name' => $uname])->one();
	    $email_current = $con->email;
	    $dt['email'] = isset($dt['already']) ? $email = Yii::$app->user->identity->email : $email_current;
	    $db = explode("@", $dt['email']);
	    $db_name = explode(".", $db[1]);
	    $dbnm = $uname;
//               $new_db_name = $db_name[0] . '_elliot';
	    $new_db_name = $uname . '_elliot';
	    //        $new_db_name = 'ravim3_elliot';
	    $dir_name = $_SERVER['DOCUMENT_ROOT'] . '/subdomains/' . $dbnm;

	    if (file_exists($dir_name)) {
		$config = [
		    'class' => 'yii\db\Connection',
		    'dsn' => 'mysql:host=127.0.0.1;dbname=' . $new_db_name . '',
		    'username' => Yii::$app->params['DB_USERNAME'],
		    'password' => Yii::$app->params['DB_PASSWORD'],
		    'charset' => 'utf8',
		];


		$db_merchant = Yii::createObject($config);
		$data = $db_merchant->createCommand("SELECT id,company_name from user WHERE email='" . $dt['email'] . "'")->queryAll();

		$id = $data[0]['id'];
		$company_name = ucfirst($data[0]['company_name']);
		$data1 = $db_merchant->createCommand("SELECT * from channel_connection WHERE user_id=" . $id . " and (channel_id=122 or channel_id=123)")->queryOne();

		$post = [
		    'username' => $dt['user'],
		    'password' => $dt['password']
		];

		$ch = curl_init('https://cms-api.walkthechat.com/login/admin');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 300);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));

		// execute!
		$response = curl_exec($ch);

		// close the connection, release resources used
		//print_r(json_decode($response)); die;
		curl_close($ch);
//                      if(isset(json_decode($response)->token->token)){

		$token = isset(json_decode($response)->token->token) ? json_decode($response)->token->token : '';


		if (!$token && !isset($dt['already'])) {
//                        Yii::$app->session->setFlash('danger', 'Error! Please provide Valid WeChat Details');
		    echo "error";
		    die;
		}
		$curl = curl_init();

		curl_setopt_array($curl, array(
		    CURLOPT_URL => "https://cms-api.walkthechat.com/projects/",
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_ENCODING => "",
		    CURLOPT_MAXREDIRS => 10,
		    CURLOPT_TIMEOUT => 30,
		    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		    CURLOPT_CUSTOMREQUEST => "GET",
		    CURLOPT_HTTPHEADER => array(
			"x-access-token: $token"
		    ),
		));
		$responseid = curl_exec($curl);
		$projectid = isset(json_decode($responseid)->projects[0]->_id) ? json_decode($responseid)->projects[0]->_id : '';
		$user_data = User::find()->Where(['id' => Yii::$app->user->identity->id])->one();
		$email_match = $user_data->email;
		$is_superadmin = false;
		if ($dt['email'] == $email_match) {
		    $admin_approved = 'no';
		} else {
		    $admin_approved = 'yes';
		    $is_superadmin = true;
		}
		if (isset($dt['already'])) {
		    $u = $dt['user'];
		    $p = $dt['password'];
		}
		$to = $dt['email'];
		if (isset($data1) && !empty($data1)) {

		    //update gere
		    $db_merchant->createCommand()->update('channel_connection', [
			'channel_id' => $dt['type'],
			'username' => $dt['user'],
			'password' => $dt['password'],
			'connected' => 'yes',
			'token' => $token,
			'wechat_project_id' => $projectid,
			'wechat_admin_approve' => $admin_approved,
			'created' => date('Y-m-d h:i:s', time()),
			    ], ["and", "user_id=$id", ['or', 'channel_id=122', 'channel_id=123']])->execute();

		    if (!isset($dt['already'])) {
			// $this->Wechatimport();
		    }
		    $email = $dt['email'];

		    if (isset($dt['already'])) {
			$msg = 'An Elliot team member will reach out to you within the next 24 hours regarding next steps.';
			$sub = 'Next steps to complete your Elliot/WeChat integration';

			$ml2 = Yii::$app->mailer->compose()
				//->getSwiftMessage()->getHeaders()->addTextHeader('name', 'value')
				->setFrom('elliot@helloiamelliot.com')
				->setTo($to)
				->setSubject($sub)
				->setHtmlBody(
					'<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="background-color:#fcfcfc;padding: 60px 10px">
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="1" cellspacing="0" width="600" id="emailContainer" style="background-color:#fff;padding:25px;-webkit-box-shadow: 0px 5px 30px 0px rgba(50, 50, 50, 0.2);-moz-box-shadow:0px 5px 30px 0px rgba(50, 50, 50, 0.2);box-shadow:0px 5px 30px 0px rgba(50, 50, 50, 0.2);">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailHeader">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <center><img src="http://beta.brstdev.com/adrianna/elliot-logo.jpg" alt="Logo13" title="Logo" style="display:block; width: 100px;"/></center>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellpadding="1" cellspacing="0" width="100%" id="emailBody">
                                                <tr>
                                                    <td valign="top">
                                                        <p style="color: #000000;text-align: left;font-family: OpenSans;font-size: 14.0px;font-style: normal;font-stretch: normal;font-weight: 400;">
                                                            ' . $company_name . ',
                                                       </p>                                                    
                                                       </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                        <p style="color: #000000;text-align: left;font-family: OpenSans;font-size: 14.0px;font-style: normal;font-stretch: normal;font-weight: 400;line-height:25px;">
                                                        ' . $msg . '</p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellpadding="1" cellspacing="0" width="100%" id="emailFooter">
                                                    <tr>
                                                        <tdvalign="top">
                                                           <p>Thank you,<br>
                                                            <b>Elliot</b></p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>'
				)
				->send();
		    }
		    if (!isset($dt['already'])) {
			echo true;
		    }
		    die;
		} else {
		    $db_merchant->createCommand()->insert('channel_connection', [
			'channel_id' => $dt['type'],
			'user_id' => $id,
			'connected' => 'yes',
			'username' => $dt['user'],
			'password' => $dt['password'],
			'token' => $token,
			'wechat_already' => "yes",
			'wechat_project_id' => $projectid,
			'wechat_admin_approve' => $admin_approved,
			'created' => date('Y-m-d h:i:s', time()),
		    ])->execute();


//                    Stores::find()->select('store_id')->where(['store_name' => 'BigCommerce'])->one();
		    $email = $dt['email'];

		    if (isset($dt['already'])) {
			$msg = 'An Elliot team member will reach out to you within the next 24 hours regarding next steps.';
			$sub = 'Next steps to complete your Elliot/WeChat integration';

			$ml2 = Yii::$app->mailer->compose()
				//->getSwiftMessage()->getHeaders()->addTextHeader('name', 'value')
				->setFrom('elliot@helloiamelliot.com')
				->setTo($to)
				->setSubject($sub)
				->setHtmlBody(
					'<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="background-color:#fcfcfc;padding: 60px 10px">
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="1" cellspacing="0" width="600" id="emailContainer" style="background-color:#fff;padding:25px;-webkit-box-shadow: 0px 5px 30px 0px rgba(50, 50, 50, 0.2);-moz-box-shadow:0px 5px 30px 0px rgba(50, 50, 50, 0.2);box-shadow:0px 5px 30px 0px rgba(50, 50, 50, 0.2);">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailHeader">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <center><img src="http://beta.brstdev.com/adrianna/elliot-logo.jpg" alt="Logo13" title="Logo" style="display:block; width: 100px;"/></center>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellpadding="1" cellspacing="0" width="100%" id="emailBody">
                                                <tr>
                                                    <td valign="top">
                                                        <p style="color: #000000;text-align: left;font-family: OpenSans;font-size: 14.0px;font-style: normal;font-stretch: normal;font-weight: 400;">
                                                            ' . $company_name . ',
                                                       </p>                                                    
                                                       </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                        <p style="color: #000000;text-align: left;font-family: OpenSans;font-size: 14.0px;font-style: normal;font-stretch: normal;font-weight: 400;line-height:25px;">
                                                        ' . $msg . '</p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellpadding="1" cellspacing="0" width="100%" id="emailFooter">
                                                    <tr>
                                                        <tdvalign="top">
                                                           <p>Thank you,<br>
                                                            <b>Elliot</b></p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>'
				)
				->send();
		    }


		    //Sending mail to admin with users wehcat store details is he already has the store

		    if (isset($dt['already']) && $dt['already']) {
			$msg = "There is a new request to create store on walkthechat.com from user $email . User already has the wechat atore and following are the details</p>Username: $u <br>Password: $p";
			$sub = 'Request For Elliot/WeChat integration';
			$to = "helloiamelliot@studio86.co";
		    }
		    $ml2 = Yii::$app->mailer->compose()
			    //->getSwiftMessage()->getHeaders()->addTextHeader('name', 'value')
			    ->setFrom('elliot@helloiamelliot.com')
			    ->setTo($to)
			    ->setSubject($sub)
			    ->setHtmlBody(
				    '<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="background-color:#fcfcfc;padding: 60px 10px">
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="1" cellspacing="0" width="600" id="emailContainer" style="background-color:#fff;padding:25px;-webkit-box-shadow: 0px 5px 30px 0px rgba(50, 50, 50, 0.2);-moz-box-shadow:0px 5px 30px 0px rgba(50, 50, 50, 0.2);box-shadow:0px 5px 30px 0px rgba(50, 50, 50, 0.2);">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailHeader">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <center><img src="http://beta.brstdev.com/adrianna/elliot-logo.jpg" alt="Logo13" title="Logo" style="display:block; width: 100px;"/></center>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellpadding="1" cellspacing="0" width="100%" id="emailBody">
                                                <tr>
                                                    <td valign="top">
                                                        <p style="color: #000000;text-align: left;font-family: OpenSans;font-size: 14.0px;font-style: normal;font-stretch: normal;font-weight: 400;">
                                                            ' . $company_name . ',
                                                       </p>                                                    
                                                       </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                        <p style="color: #000000;text-align: left;font-family: OpenSans;font-size: 14.0px;font-style: normal;font-stretch: normal;font-weight: 400;line-height:25px;">
                                                        ' . $msg . '</p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellpadding="1" cellspacing="0" width="100%" id="emailFooter">
                                                    <tr>
                                                        <tdvalign="top">
                                                           <p>Thank you,<br>
                                                            <b>Elliot</b></p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>'
			    )
			    ->send();

		    if (!isset($dt['already'])) {
//                    Yii::$app->session->setFlash('success', 'Success! WeChat account detailss saved successfully');
			echo true;
		    }

		    die;
		}
	    } else {
		echo "No Account found with this email address";
		Yii::$app->session->setFlash('danger', 'Error! No acccount found with this email address.');
		die;
	    }
	}
	return $this->render('wechatconnect');
    }

    //calling wechat api with curl
    public function wechatcurl($url, $token, $pid, $param = null, $method = null) {
	$c_cat = curl_init();
	if (!$method) {
	    $method = "GET";
	}
	curl_setopt_array($c_cat, array(
	    CURLOPT_URL => "$url",
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_ENCODING => "",
	    CURLOPT_MAXREDIRS => 10,
	    CURLOPT_TIMEOUT => 30,
	    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	    CURLOPT_CUSTOMREQUEST => $method,
	    CURLOPT_POSTFIELDS => "category=$param",
	    CURLOPT_HTTPHEADER => array(
		"x-access-token: $token",
		"x-id-project: $pid"
	    ),
	));
	return curl_exec($c_cat);
    }

    public function actionWechatimportonlogin() {



	$post = Yii::$app->request->post();
	$id = $post['user_id'];
	$task_source = $post['task_source'];

	$get_big_id_users = User::find()->select('domain_name,company_name,email')->where(['id' => $id])->one();

	$config = yii\helpers\ArrayHelper::merge(
			require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_big_id_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
	);
	$application = new yii\web\Application($config);

	$CronTasks_data = CronTasks::find()->where(['elliot_user_id' => $id, 'task_source' => $task_source])->one();
	if (empty($CronTasks_data)) {
	    $CronTasks = new CronTasks();
	    $CronTasks->elliot_user_id = $id;
	    $CronTasks->task_name = $post['task'];
	    $CronTasks->task_source = $task_source;
	    $CronTasks->status = 'Pending';
	    $CronTasks->created_at = date('Y-m-d h:i:s', time());
	    $CronTasks->updated_at = date('Y-m-d h:i:s', time());
	    $CronTasks->save();
	} else {
	    if ($CronTasks_data->status == "Completed") {
		$CronTasks_data->status = 'Pending';
		$CronTasks_data->save();
	    } else {
		echo "error";
		die;
	    }
	}
	$connection = ChannelConnection::find()->where(['wechat_admin_approve' => 'yes'])->one();
	if (!empty($connection)) {
	    echo $connection->channel_id;
	    die;
	}
    }

    /**
     * Importing data from wechat store
     */
    public function actionWechatimport($id, $type = null) {
	header('content-type: application/json; charset=utf-8');

	header("Access-Control-Allow-Origin: *");
	$dt = Yii::$app->request->get();

	if ($type != null) {
	    $dt['type'] = $type;
	} else {
	    $dt = Yii::$app->request->get();
	}

	$get_big_id_users = User::find()->select('domain_name,company_name,email')->where(['id' => $id])->one();
	$config = yii\helpers\ArrayHelper::merge(
			require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_big_id_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
	);
	$application = new yii\web\Application($config);


	//get wechat Service account id 
	$get_big_id = Channels::find()->select('channel_ID')->where(['channel_ID' => $dt['type']])->one();
	$channel_id = $get_big_id->channel_ID;

	//Get walkthechat token and project id
	$con_details = ChannelConnection::find()->where(['channel_id' => $channel_id])->one();
	$token = $con_details->token;
	$pid = $con_details->wechat_project_id;
	$user_id = $con_details->user_id;
	$channel_connection_id = $con_details->channel_connection_id;

	/* Save Channel details */
	$country_code = '';
	$detail_url = 'https://cms-api.walkthechat.com/contacts';
	$channel_details = $this->wechatcurl($detail_url, $token, $pid);
	$_channel_details = json_decode($channel_details);
        $channel_currency_code = 'CNY';
        $conversion_rate = '';
        if ($channel_currency_code != '') {
            $conversion_rate = Stores::getCurrencyConversionRate($channel_currency_code, 'USD');
            $currency_check = CurrencyConversion::find()->Where(['to_currency' => $channel_currency_code])->one();
            if (empty($currency_check)) {
                $store_currency_conversion = new CurrencyConversion();
                $store_currency_conversion->from_currency = 'USD';
                $store_currency_conversion->from_value = 1;
                $store_currency_conversion->to_currency = $channel_currency_code;
                $store_currency_conversion->to_value = $conversion_rate;
                $store_currency_conversion->created_at = date('Y-m-d H:i:s', time());
                $store_currency_conversion->save(false);
            } else {
                $currency_check->from_currency = 'USD';
                $currency_check->from_value = 1;
                $currency_check->to_currency = $channel_currency_code;
                $currency_check->to_value = $conversion_rate;
                $currency_check->save(false);
            }
        }
        
	if (!empty($_channel_details)) {
	    $country_name = $_channel_details->contacts[0]->country;
	    $country_data = Countries::find()->where(['name' => $country_name])->one();
	    if (!empty($country_data)) {
		$country_code = $country_data->sortname;
		$store_detail_check = StoreDetails::find()->Where(['channel_connection_id' => $channel_connection_id])->one();
		if (empty($store_detail_check)) {
		    $save_store_details = new StoreDetails();
		    $save_store_details->channel_connection_id = $channel_connection_id;
		    $save_store_details->store_name = $_channel_details->contacts[0]->company_name;
		    $save_store_details->store_url = '';
		    $save_store_details->country = $country_name;
		    $save_store_details->country_code = $country_code;
		    $save_store_details->currency = 'CNY';
		    $save_store_details->currency_symbol = 'CNY';
		    $save_store_details->channel_accquired = 'WeChat';
		    $save_store_details->created_at = date('Y-m-d H:i:s', time());
		    $save_store_details->save(false);
		}
	    }
	}

	//Importing Product Categories
	$url = 'https://cms-api.walkthechat.com/categories/product?page=1&limit=999999&sort=-modifed';
	$rs_cat = $this->wechatcurl($url, $token, $pid);
	$categories = json_decode($rs_cat);

	if (!empty($categories)) :
	    $dlt_array = array();
	    foreach ($categories->categoriesProduct as $category):
		$cat_name = $category->name;
		$create_date = $category->created;
		$store_category_id = $category->_id;
		$store_category_groupid = $category->groupId;
		$dlt_array[] = $store_category_groupid;
		//Check whelther category with the same id available
		$checkModel = Categories::find()->where(['category_name' => $cat_name])->one();

		if (empty($checkModel)):
		    //Create Model for each new category fetched
		    $categoryModel = new Categories();
		    $categoryModel->category_name = $cat_name;
		    $categoryModel->channel_abb_id = '';
		    $categoryModel->store_category_groupid = '';
		    $categoryModel->parent_category_ID = 0;
		    $created_date = date('Y-m-d H:i:s', $create_date);
		    $categoryModel->created_at = $created_date;
		    //Save Elliot User id
		    $categoryModel->elliot_user_id = $user_id;
		    $parent_id = $categoryModel->category_ID;
		    if ($categoryModel->save()):
			/* Save Category Id in Category Abbrivation table */
			$CategoryAbbrivation = new CategoryAbbrivation();
			$CategoryAbbrivation->channel_abb_id = "WC" . $store_category_id;
			$CategoryAbbrivation->store_category_groupid = $store_category_groupid;
			$CategoryAbbrivation->category_ID = $categoryModel->category_ID;
			$CategoryAbbrivation->channel_accquired = 'WeChat';
			$CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
			$CategoryAbbrivation->save(false);
		    endif;

		else:

		    /** If category is already exist in elliot then Save Category Id in Category Abbrivation table */
		    $categorytAbbrivation_check = CategoryAbbrivation::find()->Where(['channel_abb_id' => "WC" . $store_category_id])->one();
		    if (empty($categorytAbbrivation_check)):
			$CategoryAbbrivation = new CategoryAbbrivation();
			$CategoryAbbrivation->channel_abb_id = "WC" . $store_category_id;
			$CategoryAbbrivation->category_ID = $checkModel->category_ID;
			$CategoryAbbrivation->store_category_groupid = $store_category_groupid;
			$CategoryAbbrivation->channel_accquired = 'WeChat';
			$CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
			$CategoryAbbrivation->save(false);
		    endif;

//                    $parent_id = $checkModel->category_ID ? $checkModel->category_ID : 0;
//                    $checkModel->category_name = $cat_name;
//                    $checkModel->store_category_groupid = $store_category_groupid;
//                    $checkModel->save();
		endif;
		if (!empty($category->children)):
		    foreach ($category->children as $cat_child):
			$child_cat_name = $cat_child->name;
			$create_date = $cat_child->created;
			$store_category_id = $cat_child->_id;
			$store_category_groupid_child = $cat_child->groupId;
			$dlt_array[] = $store_category_groupid_child;
			//Check whelther category with the same name available
//                        $checkparent = Categories::find()->where(['parent_category_ID' => $parent_id, 'store_category_groupid' => $store_category_groupid_child])->one();
			$checkparent = Categories::find()->where(['parent_category_ID' => $parent_id, 'category_name' => $child_cat_name])->one();
			if (empty($checkparent)):
			    //Create Model for each new category fetched
			    $categoryModel = new Categories();
			    $categoryModel->category_name = $child_cat_name;
			    $categoryModel->channel_abb_id = '';
			    $categoryModel->store_category_groupid = '';
			    $categoryModel->parent_category_ID = $parent_id;
			    $created_date = date('Y-m-d H:i:s', $create_date);
			    $categoryModel->created_at = $created_date;
			    //Save Elliot User id
			    $categoryModel->elliot_user_id = $user_id;
			    if ($categoryModel->save(false)):

				/* Save Category Id in Category Abbrivation table */
				$CategoryAbbrivation = new CategoryAbbrivation();
				$CategoryAbbrivation->channel_abb_id = $store_category_id;
				$CategoryAbbrivation->store_category_groupid = $store_category_groupid_child;
				$CategoryAbbrivation->category_ID = $categoryModel->category_ID;
				$CategoryAbbrivation->channel_accquired = 'WeChat';
				$CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
				$CategoryAbbrivation->save(false);

			    endif;

			else:
			    /** If category is already exist in elliot then Save Category Id in Category Abbrivation table */
			    $categorytAbbrivation_check = CategoryAbbrivation::find()->Where(['channel_abb_id' => $store_category_id])->one();
			    if (empty($categorytAbbrivation_check)):
				$CategoryAbbrivation = new CategoryAbbrivation();
				$CategoryAbbrivation->channel_abb_id = $store_category_id;
				$CategoryAbbrivation->category_ID = $checkparent->category_ID;
				$CategoryAbbrivation->store_category_groupid = $store_category_groupid_child;
				$CategoryAbbrivation->channel_accquired = 'WeChat';
				$CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
				$CategoryAbbrivation->save(false);
			    endif;
//                            $checkparent->category_name = $child_cat_name;
//                            $checkparent->store_category_groupid = $store_category_groupid_child;
//                            $checkparent->parent_category_ID = $parent_id;
//                            $checkparent->save(false);

			endif;
		    endforeach;

		endif;
	    endforeach;
	endif;


	//Importing Users

	$url = 'https://cms-api.walkthechat.com/users?page=1&limit=1000000&sort=-created';
	$rs_products = $this->wechatcurl($url, $token, $pid);
	$users = json_decode($rs_products);

	if (!empty($users)) {
	    foreach ($users->users as $us) {
		$customer_abb_id = $us->_id;
		$Prefix_customer_abb_id = "WC" . $us->_id;
		// $channel = CustomerUser::find()->Where(['channel_abb_id' => "WC" . $us->_id])->one();
		$channel = CustomerAbbrivation::find()->Where(['channel_abb_id' => $Prefix_customer_abb_id])->one();

		if (empty($channel)) {
		    $user_custom_field = $custom_field_name = $custom_field_last_name = $custom_field_city = $custom_field_street_1 = $custom_field_phone = $custom_field_country = $custom_field_province = $custom_field_district = $custom_field_zipcode = "";
		    $email_eg = '';

		    $channel_name1 = 'WeChat';
		    $cus_first_name = isset($us->info->nickname) ? $us->info->nickname : "";
		    $date_accquired = date('Y-m-d h:i:s', $us->created);
		    $city = isset($us->info->city) ? $us->info->city : "";
		    $state = isset($us->info->province) ? $us->info->province : "";

		    if (isset($us->customerFields)) {

			$user_custom_field = json_decode($us->customerFields);
			$custom_field_name = $user_custom_field->address->name;
			$custom_field_last_name = $user_custom_field->address->surname;
			$custom_field_city = $user_custom_field->address->city;
			$custom_field_street_1 = $user_custom_field->address->address;
			$custom_field_phone = $user_custom_field->address->phone;
			$custom_field_country = $user_custom_field->address->country;
			$custom_field_province = $user_custom_field->address->province;
			$custom_field_district = $user_custom_field->address->district;
			$custom_field_zipcode = $user_custom_field->address->zipcode;
		    }

		    $first_name = (empty($cus_first_name)) ? $custom_field_name : $cus_first_name;
		    $last_name = (empty($custom_field_last_name)) ? "" : $custom_field_last_name;
		    $create_at = $us->created;
		    $update_at = date('Y-m-d H:i:s');
		    $customer_data = array(
			'customer_id' => $customer_abb_id,
			'elliot_user_id' => $user_id,
			'first_name' => $first_name,
			'last_name' => $last_name,
			'email' => '',
			'channel_store_name' => 'WeChat',
			'channel_store_prefix' => 'WC',
			'mul_store_id' => '',
			'mul_channel_id' => $channel_connection_id,
			'customer_created_at' => $create_at,
			'customer_updated_at' => $update_at,
			'billing_address' => array(
			    'street_1' => $custom_field_street_1,
			    'street_2' => '',
			    'city' => $custom_field_city,
			    'state' => $custom_field_province,
			    'country' => 'China',
			    'country_iso' => '',
			    'zip' => $custom_field_zipcode,
			    'phone_number' => $custom_field_phone,
			    'address_type' => 'Default',
			),
			'shipping_address' => array(
			    'street_1' => '',
			    'street_2' => '',
			    'city' => '',
			    'state' => '',
			    'country' => '',
			    'zip' => '',
			),
		    );

		    Stores::customerImportingCommon($customer_data);
		} else {
		    $db_customer_id = $channel->customer_id;
		    $channel_update = CustomerUser::find()->Where(['customer_ID' => $db_customer_id])->one();

		    $user_custom_field = $custom_field_name = $custom_field_last_name = $custom_field_city = $custom_field_street_1 = $custom_field_phone = $custom_field_country = $custom_field_province = $custom_field_district = $custom_field_zipcode = "";
		    $email_eg = '';

		    $channel_name1 = 'WeChat';
		    $first_name = isset($us->info->nickname) ? $us->info->nickname : "";
		    $date_accquired = date('Y-m-d h:i:s', $us->created);
		    $city = isset($us->info->city) ? $us->info->city : "";
		    $state = isset($us->info->province) ? $us->info->province : "";

		    if (isset($us->customerFields)) {
			$user_custom_field = json_decode($us->customerFields);
			$custom_field_name = $user_custom_field->address->name;
			$custom_field_last_name = $user_custom_field->address->surname;
			$custom_field_city = $user_custom_field->address->city;
			$custom_field_street_1 = $user_custom_field->address->address;
			$custom_field_phone = $user_custom_field->address->phone;
			$custom_field_country = $user_custom_field->address->country;
			$custom_field_province = $user_custom_field->address->province;
			$custom_field_district = $user_custom_field->address->district;
			$custom_field_zipcode = $user_custom_field->address->zipcode;
		    }

		    $channel_update->first_name = (empty($first_name)) ? $custom_field_name : $first_name;
		    $channel_update->last_name = (empty($custom_field_last_name)) ? "" : $custom_field_last_name;
		    $channel_update->channel_abb_id = $Prefix_customer_abb_id;
		    $channel_update->channel_acquired = $channel_name1;
		    $channel_update->date_acquired = date('Y-m-d h:i:s', $us->created);
		    $channel_update->street_1 = (empty($custom_field_street_1)) ? "" : $custom_field_street_1;
		    $channel_update->city = (empty($city)) ? $custom_field_city : $city;
		    $channel_update->country = 'China';
		    $channel_update->state = (empty($state)) ? $custom_field_province : $state;
		    $channel_update->zip = (empty($custom_field_zipcode)) ? "" : $custom_field_zipcode;
		    $channel_update->phone_number = (empty($custom_field_phone)) ? "" : $custom_field_phone;
		    $channel_update->save(false);
		}
	    }
	}

	//Importing products, products variants and others
	$url = 'https://cms-api.walkthechat.com/products/?page=1&limit=9999999&sort=-sort';
	$rs_pro = $this->wechatcurl($url, $token, $pid);
	$products = json_decode($rs_pro);

	if (!empty($products)) :
	    $dlt_array_pro = array();
	    foreach ($products->products as $pro):

		if ($pro->title):
		    $group_id = $pro->groupId;
		    //get product variants by product id
		    $url = "https://cms-api.walkthechat.com/products/$group_id/variants/";
		    $rs_pro_variants = $this->wechatcurl($url, $token, $pid);
		    $variants = json_decode($rs_pro_variants);
		    if (!empty($variants)):
			$dlt_array_pro[] = "WC" . $variants->variants->en->product->groupId;

			$product_id = $variants->variants->en->product->groupId;
			$sku = $variants->variants->en->product->sku;
			$product_name = $variants->variants->en->product->title;
			$upc = $variants->variants->en->product->barcode;
			$description = $variants->variants->en->product->description;
			$qty = ($variants->variants->en->product->unit > 0) ? $variants->variants->en->product->unit : 0;
			$stock = ($variants->variants->en->product->stock == '') ? 0 : 1;
			$status = ($variants->variants->en->product->stock == '') ? 0 : 1;
			$weight = $variants->variants->en->product->weight;
			$price = $variants->variants->en->product->price;
			$sale_price = $variants->variants->en->product->salesPrice;
			if ($sale_price == '' || $sale_price == Null || $sale_price == 0) {
			    $sale_price = $price;
			}
			$wechat_id = $variants->variants->en->product->_id;
			$created_at = date('Y-m-d H:i:s', $variants->variants->en->product->created);
			$updated_at = date('Y-m-d H:i:s', $variants->variants->en->product->modified);

			$wechat_product_cat_id = $variants->variants->en->product->categoryGroupId;
			$wechat_cat_id = array();
			if (count($wechat_product_cat_id) > 0) {
			    foreach ($wechat_product_cat_id as $_cat_id) {
				$get_category = CategoryAbbrivation::find()->where(['store_category_groupid' => $_cat_id])->one();
				if (!empty($get_category)) {
				    $wechat_cat_id[] = substr($get_category->channel_abb_id, 2);
				}
			    }
			}
			$product_image_data = array();
			if ($variants->variants->en->product->gallery) {
			    $group_id = $variants->variants->en->product->gallery;
			    $url = "https://cms-api.walkthechat.com/galleries/$group_id";
			    $rs_pro_img = $this->wechatcurl($url, $token, $pid);
			    $images = json_decode($rs_pro_img);
			    if (!empty($images)) {
				$count = 1;
				foreach ($images->galleries as $product_image) {
				    $image_url = $product_image->url;
				    $lable = $product_image->name;
				    $p_image_priority = '';
				    if ($variants->variants->en->product->thumbnail != '' && $count == 1) {
					$product_image_data[] = array(
					    'image_url' => $variants->variants->en->product->thumbnail,
					    'label' => '',
					    'position' => 1,
					    'base_img' => $variants->variants->en->product->thumbnail,
					);
				    }
				    $product_image_data[] = array(
					'image_url' => $image_url,
					'label' => $lable,
					'position' => $count,
					'base_img' => '',
				    );
				}
			    }
			}
			//$channel = Products::find()->where(['or', ['sku' => $variants->variants->en->product->sku]])->andWhere(['<>', 'sku', $variants->variants->en->product->sku])->one();
			/* If products name and sku already in elliot true if condition */
			//if (empty($channel)):
			$product_data = array(
			    'product_id' => $product_id, // Stores/Channel product ID
			    'wechat_id' => $wechat_id,
			    'name' => $product_name, // Product name
			    'sku' => $sku, // Product SKU
			    'description' => $description, // Product Description
			    'product_url_path' => '', // Product url if null give blank value
			    'weight' => $weight, // Product weight if null give blank value
			    'status' => $status, // Product status (visible or hidden). Give in 0 or 1 form (0 = hidden, 1 = visible).
			    'price' => $price, // Porduct price
			    'sale_price' => $sale_price, // Product sale price if null give Product price value
			    'qty' => $qty, //Product quantity 
			    'stock_status' => $stock, // Product stock status ("in stock" or "out of stock"). 
			    // Give in 0 or 1 form (0 = out of stock, 1 = in stock)
			    'websites' => array(), //This is for only magento give only and blank array
			    'brand' => '', // Product brand if any
			    'low_stock_notification' => 5, // Porduct low stock notification if any otherwise give default 5 value
			    'created_at' => $created_at, // Product created at date format date('Y-m-d H:i:s')
			    'updated_at' => $updated_at, // Product updated at date format date('Y-m-d H:i:s')
			    'channel_store_name' => 'WeChat', // Channel or Store name
			    'channel_store_prefix' => 'WC', // Channel or store prefix id
			    'mul_store_id' => '', //Give multiple store id
			    'mul_channel_id' => $channel_connection_id, // Give multiple channel id
			    'elliot_user_id' => $user_id, // Elliot user id
			    'store_id' => '', // if your are importing store give store id
			    'channel_id' => $channel_id, // if your are importing channel give channel id
			    'country_code' => $country_code, // if your are importing channel give channel id
			    'conversion_rate' => $conversion_rate,
			    'currency_code' => 'CNY',
			    'upc' => $upc, // Product upc if any
			    'jan' => '', // Product jan if any
			    'isban' => '', // Product isban if any
			    'mpn' => '', // Product mpn if any
			    'categories' => $wechat_cat_id, // Product categroy array. If null give a blank array 
			    'images' => $product_image_data, // Product images data
			);
			$checkProductModel = ProductAbbrivation::find()->where(['channel_abb_id' => 'WC' . $product_id])->one();
			if (empty($checkProductModel)) {
			    Stores::productImportingCommon($product_data);
			}
//                        else:
////                                UPDATE PRODUCTS IF ALREADY EXISTS
//                            $channel->channel_abb_id = "WC" . $variants->variants->en->product->groupId;
//                            $channel->product_name = $variants->variants->en->product->title;
//                            $channel->SKU = $variants->variants->en->product->sku ? $variants->variants->en->product->sku : '';
//                            $channel->UPC = $variants->variants->en->product->barcode ? $variants->variants->en->product->barcode : ''; // There is no UPC and EAN in response from wechat so we are using barcode here.
//                            $channel->EAN = '';
//                            $channel->Jan = '';
//                            $channel->ISBN = '';
//                            $channel->MPN = '';
//                            $channel->description = $variants->variants->en->product->description;
//                            $channel->adult = 'no';
//                            //Refrence to Google Product Search Categories
//                            //                            $productModel->age_group = NULL;
//                            $channel->gender = 'Unisex';
//                            //End
//                            if ($variants->variants->en->product->stock == true):
//                                $channel->availability = 'In Stock';
//                                $channel->stock_level = 'In Stock';
//
//                            else:
//                                $channel->availability = 'Out of Stock';
//                                $channel->stock_level = 'Out of Stock';
//                            endif;
//                            $channel->brand = '';
//                            //                            if ($p_condition == ''):
//                            $channel->condition = 'new';
//                            //                            else:
//                            //                              $productModel->condition = $p_condition;
//                            //                            endif;
//                            $channel->weight = $variants->variants->en->product->weight;
//                            //Inventory Fields Mapping
//                            $channel->stock_quantity = $variants->variants->en->product->unit;
//                            $channel->price = $variants->variants->en->product->price;
//                            $channel->sales_price = $variants->variants->en->product->salesPrice;
//                            $channel->created_at = date('Y-m-d H:i:s', $variants->variants->en->product->created);
//                            $channel->updated_at = date('Y-m-d H:i:s', $variants->variants->en->product->modified);
//                            //Save Elliot User id
//                            $channel->elliot_user_id = $user_id;
//                            $channel->save(false);
//                            if ($channel->save(false)):
//                                /* Save Product Abbrivation Id */
//                                $check_product_abbrevation = ProductAbbrivation::find()->Where(['channel_abb_id' => "WC" . $variants->variants->en->product->groupId])->one();
//                                if (empty($check_product_abbrevation)) :
//
//                                    $product_abberivation = new ProductAbbrivation();
//                                    $product_abberivation->channel_abb_id = "WC" . $variants->variants->en->product->groupId;
//                                    $product_abberivation->product_id = $channel->id;
//                                    $product_abberivation->channel_accquired = 'WeChat';
//                                    $product_abberivation->salePrice = $variants->variants->en->product->salesPrice;
//                                    $product_abberivation->price = $variants->variants->en->product->price;
//                                    $product_abberivation->schedules_sales_date = date('Y-m-d', time());
//                                    $product_abberivation->stock_quantity = $variants->variants->en->product->unit;
//                                    if ($variants->variants->en->product->stock == true):
//                                        $product_abberivation->stock_level = 'In Stock';
//                                    else:
//                                        $product_abberivation->stock_level = 'Out of Stock';
//                                    endif;
//                                    $product_abberivation->created_at = date('Y-m-d H:i:s', time());
//                                    $product_abberivation->save(false);
//
//                                    //Product Channel/Store Table Entry 
//                                    $productChannelModel = new ProductChannel;
//                                    $productChannelModel->channel_id = $channel_id;
//                                    $productChannelModel->product_id = $channel->id;
//                                    $productChannelModel->status = 'yes';
//                                    $productChannelModel->created_at = date('Y-m-d H:i:s', time());
//                                    $productChannelModel->save(false);
//                                endif;
//                            endif;
//                            $p_created_date = date('Y-m-d H:i:s', $variants->variants->en->product->created);
//                            $p_updated_date = date('Y-m-d H:i:s', $variants->variants->en->product->modified);
//
//                            $get_category = ProductCategories::find()->where(['product_ID' => $channel->id])->one();
//                            if (!empty($get_category)) {
//                                $get_category->delete();
//                            }
//                            if (isset($variants->variants->en->product->categoryGroupId) && !empty($variants->variants->en->product->categoryGroupId)):
//                                foreach ($variants->variants->en->product->categoryGroupId as $catid):
//                                    //$catid = $variants->variants->en->product->categoryGroupId[0];
//
//
//                                    $url = "https://cms-api.walkthechat.com/categories/product/groups/$catid";
//                                    $rs_cat_name = $this->wechatcurl($url, $token, $pid);
//
//                                    $cat_name = json_decode($rs_cat_name);
//
//
//                                    if (!empty($cat_name->categories)):
//                                        $cat_arr = $cat_name->categories;
//                                        $cname = '';
//                                        foreach ($cat_arr as $c_name):
//                                            if ($c_name->language == "en"):
//                                                $cname = $c_name->name;
//                                            else:
//                                                if ($cname == ''):
//                                                    $cname = $c_name->name;
//                                                endif;
//                                            endif;
//                                        endforeach;
//
//                                        $get_category = Categories::find()->select('category_ID')->where(['category_name' => $cname])->one();
//                                        $cat_id = !empty($get_category) ? $get_category->category_ID : '';
//                                        $check_category = ProductCategories::find()->where(['category_ID' => $cat_id, 'product_ID' => $channel->id])->one();
//                                        if (empty($check_category)):
//
//
//                                            //Create Model for Each new Product Category
//                                            $productCategoryModel = new ProductCategories;
//                                            $productCategoryModel->category_ID = $cat_id;
//                                            $productCategoryModel->product_ID = $channel->id;
//                                            $productCategoryModel->created_at = $p_created_date;
//                                            $productCategoryModel->updated_at = $p_updated_date;
//                                            //Save Elliot User id
//                                            $productCategoryModel->elliot_user_id = $user_id;
//                                            $productCategoryModel->save(false);
//                                        endif;
//                                    endif;
//                                endforeach;
//                            endif;
//
//                            $get_images = ProductImages::find()->where(['product_ID' => $channel->id])->one();
//                            if (!empty($get_images)):
//                                $get_images->delete();
//                            endif;
//                            if ($variants->variants->en->product->thumbnail):
//                                $productImageModel = new ProductImages;
//                                $productImageModel->product_ID = $channel->id;
//                                $productImageModel->label = '';
//                                $productImageModel->link = $variants->variants->en->product->thumbnail;
//
//                                $productImageModel->priority = '';
//                                $productImageModel->default_image = 'Yes';
//
//                                $productImageModel->image_status = 1;
//                                $productImageModel->created_at = date('Y-m-d H:i:s', $variants->variants->en->product->created);
//                                $productImageModel->updated_at = date('Y-m-d H:i:s', $variants->variants->en->product->modified);
//                                $productImageModel->save(false);
//                            endif;
//                            if ($variants->variants->en->product->gallery):
//                                $group_id = $variants->variants->en->product->gallery;
//
//                                $url = "https://cms-api.walkthechat.com/galleries/$group_id";
//                                $rs_pro_img = $this->wechatcurl($url, $token, $pid);
//                                $images = json_decode($rs_pro_img);
//
//                                if (!empty($images)):
//                                    foreach ($images->galleries as $product_image):
//                                        // echo '<Pre>';print_r*$product_image;
//                                        $p_image_link = $product_image->url;
//                                        $p_image_label = '';
//                                        $p_image_priority = '';
//                                        $p_image_created_date = date('Y-m-d H:i:s', $product_image->created);
//                                        $p_image_updated_date = date('Y-m-d H:i:s', $product_image->created);
//                                        //Create Model for Each new Product Image
//                                        $productImageModel = new ProductImages;
//                                        $productImageModel->product_ID = $channel->id;
//                                        $productImageModel->label = $p_image_label;
//                                        $productImageModel->link = $p_image_link;
//
//                                        $productImageModel->priority = '';
//                                        $productImageModel->default_image = '';
//
//                                        $productImageModel->image_status = 1;
//                                        $productImageModel->created_at = $p_image_created_date;
//                                        $productImageModel->updated_at = $p_image_updated_date;
//                                        $productImageModel->save(false);
//                                    endforeach;
//                                endif;
//                            endif;
//
//
//                            //will add variations code here;
//                            if (!empty($variants->variants->en->variants)) :
//                                foreach ($variants->variants->en->variants as $variation):
//                                    $variations_id = '';
//                                    foreach ($variation as $key => $varnt):
//                                        $get_var = ProductVariation::find()->where(['product_ID' => $channel->id])->one();
//                                        if (!empty($get_var)):
//                                            $get_var->delete();
//                                        endif;
//
//                                        if ($key == 'variant'):
//                                            foreach ($varnt as $k => $var):
//                                                $var_name = $k;
//                                                $checkVarModel = Variations::find()->where(['variation_name' => $var_name])->one();
//                                                if (empty($checkVarModel)):
//                                                    //Create Model for each new Variation(Product Option) fetched
//                                                    $variationModel = new Variations();
//                                                    $variationModel->variation_name = $var_name;
//                                                    $variationModel->variation_description = $var_name;
//                                                    //            $variationModel->variation_type = $var_type;
//                                                    $created_date = date('Y-m-d h:i:s', time());
//                                                    $variationModel->created_at = $p_created_date;
//                                                    //Save Elliot User id
//                                                    $variationModel->elliot_user_id = $user_id;
//
//                                                    if ($variationModel->save()):
//                                                        $variation_id = $variationModel->variations_ID;
//                                                        //Create Variation Item List(Product Options Values)  
//                                                        //                             $variation_items = $variation->attributes;
//                                                        //                             if (!empty($variation_items)):
//                                                        //                          foreach ($variation_items as $variation_item):
//                                                        $var_item_name = $var;
//                                                        //                                 $var_item_value = $variation_item->value;
//                                                        //Check whelther Variation Item with the same name and same Variation Id available
//                                                        $checkVarItemModel = VariationsItemList::find()->where(['variation_id' => $variation_id, 'item_name' => $var_item_name])->one();
//                                                        if (empty($checkVarItemModel)):
//                                                            //Create Model for each new Variation Item(Product Option Value) fetched                
//                                                            $variationItemModel = new VariationsItemList();
//                                                            $variationItemModel->variation_id = $variation_id;
//                                                            $variationItemModel->item_name = $var_item_name;
//                                                            $variationItemModel->item_value = '';
//                                                            $created_date = date('Y-m-d h:i:s', time());
//                                                            $variationItemModel->created_at = $p_created_date;
//                                                            //Save Elliot User id
//                                                            $variationItemModel->elliot_user_id = $user_id;
//                                                            $variationItemModel->save();
//                                                        endif;
//                                                    //endforeach;
//                                                    //endif;
//
//
//                                                    endif;
//                                                else:
//                                                    $variation_id = $checkVarModel->variations_ID;
//                                                    $var_item_name = $var;
//                                                    $checkVarItemModel = VariationsItemList::find()->where(['variation_id' => $variation_id, 'item_name' => $var_item_name])->one();
//                                                    if (empty($checkVarItemModel)):
//                                                        $variation_id = $checkVarModel->variations_ID;
//                                                        //Create Variation Item List(Product Options Values)      
//                                                        $variationItemModel = new VariationsItemList();
//                                                        $variationItemModel->variation_id = $variation_id;
//                                                        $variationItemModel->item_name = $var;
//                                                        $variationItemModel->item_value = '';
//                                                        $created_date = date('Y-m-d h:i:s', time());
//                                                        $variationItemModel->created_at = $p_created_date;
//                                                        //Save Elliot User id
//                                                        $variationItemModel->elliot_user_id = $user_id;
//                                                        $variationItemModel->save();
//                                                    endif;
//                                                endif;
//                                            endforeach;
//
//                                        else:
//                                            if ($key == '_id'):
//                                                $variations_id = $varnt;
//                                            endif;
//                                            $checkVarItemModel = ProductVariation::find()->where(['store_variation_id' => $variations_id, 'item_name' => $key, 'item_value' => $varnt])->one();
//                                            if (empty($checkVarItemModel)):
//                                                $productVariationModel = new ProductVariation;
//                                                $productVariationModel->variation_id = '';
//                                                $productVariationModel->product_id = $channel->id;
//                                                $productVariationModel->store_variation_id = $variations_id;
//                                                $productVariationModel->item_name = $key;
//                                                $productVariationModel->item_value = $varnt;
//                                                $created_date = date('Y-m-d h:i:s', time());
//                                                $productVariationModel->created_at = $p_created_date;
//                                                //            $productVariationModel->updated_at = $p_updated_date;
//                                                $productVariationModel->save(false);
//
//                                            endif;
//                                        endif;
//                                    endforeach;
//                                endforeach;
//                            endif;
//                        endif;
		    endif;
		endif;
	    endforeach;
	endif;
	//Import Orders 
	$url = 'https://cms-api.walkthechat.com/orders?page=1&limit=10&sort=-created';
	//echo "Token:".$token."project id". $pid; die();
	$rs_cat = $this->wechatcurl($url, $token, $pid);
	$orders = json_decode($rs_cat);
	// echo "<pre>";
	// print_r($orders); 
	/* Check for Product duplicacy */
	if (!empty($orders)) {
	    $order_product_data = array();
	    foreach ($orders->orders as $ord):
		// echo "1441 <br>";
		$order_cus_id = $ord->userId;
		// $order_id = $ord->shipmentDetails->custom->groupId;
		// $prefix_order_id = "WC" . $ord->shipmentDetails->custom->groupId;
		// if(isset($ord->shipmentDetails->custom)){
		// $order_id = $ord->shipmentDetails->custom->groupId;
		// $prefix_order_id = "WC" . $ord->shipmentDetails->custom->groupId;
		// }elseif(isset($ord->shippingRates->custom)){
		// $order_id = $ord->shippingRates->custom->groupId;
		// $prefix_order_id = "WC" . $ord->shippingRates->custom->groupId;
		// }elseif(isset($ord->shippingRates->groupId)){
		// $order_id = $ord->shippingRates->groupId;
		// $prefix_order_id = "WC" . $ord->shippingRates->groupId;
		// }else{
		// $order_id='';
		// $prefix_order_id='';
		// }

		if (isset($ord->_id)) {
		    $order_id = $ord->_id;
		    $prefix_order_id = "WC" . $ord->_id;
		} elseif (isset($ord->_id)) {
		    $order_id = $ord->_id;
		    $prefix_order_id = "WC" . $ord->_id;
		} elseif (isset($ord->_id)) {
		    $order_id = $ord->_id;
		    $prefix_order_id = "WC" . $ord->_id;
		} else {
		    $order_id = '';
		    $prefix_order_id = '';
		}

		// $status = $ord->status == 'waiting-for-shipment' ? 'Awaiting Fulfillment' : $ord->status;

		switch ($ord->status) {
		    case "waiting-for-shipment":
			$status = 'Pending';
			break;
		    case "Awaiting Fulfillment":
			$status = 'Pending';
			break;
		    case "authorized":
			$status = "In Transit";
			break;
		    case "partially_paid":
			$status = "Completed";
			break;
		    case "paid":
			$status = "Completed";
			break;
		    case "completed":
			$status = "Completed";
			break;
		    case "partially_refunded":
			$status = "Refunded";
			break;
		    case "refunded":
			$status = "Refunded";
			break;
		    case "voided":
			$status = "Cancel";
			break;
		    default:
			$status = "Pending";
		}


		$order_price = $ord->price;
		$product_order_data = json_decode($ord->products);
		// $total_product_count = $product_order_data[0]->quantity;
		if (isset($ord->productsDetails)) {
		    $productsDetails_order_data = $ord->productsDetails;
		}
		if (!empty($product_order_data)) {
		    $total_product_count = $product_order_data[0]->quantity;
		} elseif (!empty($productsDetails_order_data)) {
		    $total_product_count = '';
		    foreach ($productsDetails_order_data->en as $p_details) {
			$total_product_count += $p_details->cartProductQuantity;

			$product_group_id = $p_details->groupId;
			$title = $p_details->title;
			$description = $p_details->description;
			$product_thumbnail = $p_details->thumbnail;
			$pro_sku = $p_details->sku;
			$pro_price = $p_details->price;
			$pro_sale_price = $p_details->salesPrice;
			$pro_weight = $p_details->weight;
			$product_qty_ordered = $p_details->cartProductQuantity;
			$order_product_data[] = array(
			    'product_id' => $product_group_id,
			    'name' => $title,
			    'sku' => $pro_sku,
			    'price' => $pro_price,
			    'qty_ordered' => $product_qty_ordered,
			    'weight' => $pro_weight,
			);
		    }
		} else {
		    $total_product_count = '';
		}


		$product_order_custom_field = json_decode($ord->customerFields);
		$payment_gateway = $ord->paymentMethod;
		$order_created_at = date('Y-m-d H:i:s', $ord->created);
		$order_updated_at = date('Y-m-d H:i:s', $ord->modified);

		$ship_add = $product_order_custom_field->address->address;
		$city = $product_order_custom_field->address->city;
		$state = $product_order_custom_field->address->province;
		$zip = $product_order_custom_field->address->zipcode;
		$country = $product_order_custom_field->address->country;
		$phone = $product_order_custom_field->address->phone;
		$first_name = $product_order_custom_field->address->name;
		$last_name = $product_order_custom_field->address->surname;

		$order_product = $ord->products;
		$order_product_json = json_decode($order_product);

		if (!empty($order_product_json)) {
		    foreach ($order_product_json as $product_check) {
			$product_group_id = $product_check->products->cn->product->groupId;
			$title = $product_check->products->cn->product->title;
			$description = $product_check->products->cn->product->description;
			$product_thumbnail = $product_check->products->cn->product->thumbnail;
			$pro_sku = $product_check->products->cn->product->sku;
			$pro_price = $product_check->products->cn->product->price;
			$pro_sale_price = $product_check->products->cn->product->salesPrice;
			$pro_weight = $product_check->products->cn->product->weight;
			$product_qty_ordered = $product_check->quantity;
			$order_product_data[] = array(
			    'product_id' => $product_group_id,
			    'name' => $title,
			    'sku' => $pro_sku,
			    'price' => $pro_price,
			    'qty_ordered' => $product_qty_ordered,
			    'weight' => $pro_weight,
			);
		    }
		}

		$orders_check = Orders::find()->where(['channel_abb_id' => $prefix_order_id])->one();

		if (empty($orders_check)):
		    $order_data = array(
			'order_id' => $order_id,
			'status' => $status,
			'magento_store_id' => '',
			'order_grand_total' => $order_price,
			'customer_id' => $order_cus_id,
			'customer_email' => '',
			'order_shipping_amount' => '',
			'order_tax_amount' => '',
			'total_qty_ordered' => $total_product_count,
			'created_at' => $order_created_at,
			'updated_at' => $order_updated_at,
			'payment_method' => $payment_gateway,
			'refund_amount' => '',
			'discount_amount' => '',
			'channel_store_name' => 'WeChat',
			'channel_store_prefix' => 'WC',
			'mul_store_id' => '',
			'mul_channel_id' => $channel_connection_id,
			'elliot_user_id' => $user_id,
			'store_id' => '',
			'channel_id' => $channel_id,
			'currency_code' => 'CNY',
			'conversion_rate' => $conversion_rate,
			'shipping_address' => array(
			    'street_1' => $ship_add,
			    'street_2' => '',
			    'state' => $state,
			    'city' => $city,
			    'country' => 'China',
			    'country_id' => 'CN',
			    'postcode' => $zip,
			    'email' => '',
			    'telephone' => $phone,
			    'firstname' => $first_name,
			    'lastname' => $last_name,
			),
			'billing_address' => array(
			    'street_1' => $ship_add,
			    'street_2' => '',
			    'state' => $state,
			    'city' => $city,
			    'country' => 'China',
			    'country_id' => 'CN',
			    'postcode' => $zip,
			    'email' => '',
			    'telephone' => $phone,
			    'firstname' => $first_name,
			    'lastname' => $last_name,
			),
			'items' => $order_product_data,
		    );
		    // echo "<pre>";
		    // print_r($order_data); 
		    Stores::orderImportingCommon($order_data);

//                $cs_id = "WC" . $ord->userId;
//                //$con_details = CustomerUser::find()->where(['channel_abb_id' => $cs_id])->one();
//                $con_details = CustomerAbbrivation::find()->Where(['channel_abb_id' => $cs_id])->one();
//
//                $product_order_data = json_decode($ord->products);
//                $product_order_custom_field = json_decode($ord->customerFields);
//
//                $status = $ord->status == 'waiting-for-shipment' ? 'Awaiting Fulfillment' : $ord->status;
//                $order_model = new Orders();
//
//                $order_model->customer_id = $con_details->customer_id;
//                $order_model->channel_abb_id = $prefix_order_id;
//                $order_model->order_status = $status;
//                $order_model->product_qauntity = $product_order_data[0]->quantity;
//                $order_model->ship_street_1 = $product_order_custom_field->address->address;
//                $order_model->ship_street_2 = '';
//                $order_model->ship_city = $product_order_custom_field->address->city;
//                $order_model->ship_state = $product_order_custom_field->address->province;
//                $order_model->ship_zip = $product_order_custom_field->address->zipcode;
//                $order_model->ship_country = $product_order_custom_field->address->country;
//                $order_model->shipping_address = $product_order_custom_field->address->address;
//                $order_model->billing_address = $product_order_custom_field->address->address;
//                $order_model->base_shipping_cost = $ord->price;
//                $order_model->payment_method = $ord->paymentMethod;
//                $order_model->total_amount = $ord->price;
//                $order_model->order_date = date('Y-m-d H:i:s', $ord->created);
//                $order_model->updated_at = date('Y-m-d H:i:s', $ord->modified);
//                $order_model->created_at = date('Y-m-d H:i:s', $ord->created);
//                $create_date = date('Y-m-d H:i:s', $ord->created);
//                //Save Elliot User id
//                $order_model->elliot_user_id = $user_id;
//
//                if ($order_model->save(false)):
//                    /* For Order Products */
//                    foreach ($order_product_json as $product_check):
//
//
//                        $p_data_id = $product_check->products->cn->product->groupId;
//                        $wechat_id = $product_check->products->cn->product->_id;
//                        $Channel_abbr_id = "WC" . $p_data_id;
//                        $url_product = "https://cms-api.walkthechat.com/products/groups/$p_data_id";
//                        //$url_product = "https://cms-api.walkthechat.com/products/?page=1&limit=9999999&sort=-sort";
//                        $url_product_curl = $this->wechatcurl($url_product, $token, $pid);
//                        $Order_product_details = json_decode($url_product_curl);
//
//                        /* If Order Product not Exist in product table Then create a product */
//                        if (!isset($Order_product_details->products)) {
//
//                            //$product_exist_check = ProductAbbrivation::find()->Where(['channel_abb_id' => "WC" . $p_data_id])->one();
//                            $product_exist_check = Products::find()->where(['product_name' => $product_check->products->cn->product->title, 'sku' => $product_check->products->cn->product->sku])->one();
//
//                            if (empty($product_exist_check)):
//                                $productModel = new Products();
//                                $productModel->channel_abb_id = "";
//                                $productModel->product_name = $product_check->products->cn->product->title;
//                                $productModel->SKU = $product_check->products->cn->product->sku ? $product_check->products->cn->product->sku : '';
//                                $productModel->UPC = $product_check->products->cn->product->barcode ? $product_check->products->cn->product->barcode : ''; // There is no UPC and EAN in response from wechat so we are using barcode here.
//                                $productModel->EAN = '';
//                                $productModel->Jan = '';
//                                $productModel->ISBN = '';
//                                $productModel->MPN = '';
//                                $productModel->description = $product_check->products->cn->product->description;
//                                $productModel->adult = 'no';
//                                //Refrence to Google Product Search Categories
//                                //                            $productModel->age_group = NULL;
//                                $productModel->gender = 'Unisex';
//                                //End
//                                if ($product_check->products->cn->product->stock == true):
//                                    $productModel->stock_level = 'In Stock';
//                                    $productModel->availability = 'In Stock';
//                                else:
//                                    $productModel->stock_level = 'Out of Stock';
//                                    $productModel->availability = 'Out of Stock';
//                                endif;
//                                $productModel->brand = '';
//                                $productModel->condition = 'new';
//
//                                $productModel->weight = $product_check->products->cn->product->weight ? $product_check->products->cn->product->weight : 0;
//                                //Inventory Fields Mapping
//                                $productModel->stock_quantity = $product_check->products->cn->product->unit ? $product_check->products->cn->product->unit : 0;
//                                $productModel->price = $product_check->products->cn->product->price ? $product_check->products->cn->product->price : 0;
//                                $productModel->sales_price = $product_check->products->cn->product->salesPrice ? $product_check->products->cn->product->salesPrice : 0;
//                                $productModel->product_status = 'in_active';
//                                $productModel->permanent_hidden = 'in_active';
//                                $productModel->created_at = date('Y-m-d H:i:s', time());
//                                //Save Elliot User id
//                                $productModel->elliot_user_id = $user_id;
//                                $productModel->save(false);
//
//                                if ($productModel->save(false)):
//                                    $product_id = $productModel->id;
//                                    /* Save Product Abbrivation Id */
//                                    $product_abberivation = new ProductAbbrivation();
//                                    $product_abberivation->channel_abb_id = $Channel_abbr_id;
//                                    $product_abberivation->product_id = $productModel->id;
//                                    $product_abberivation->channel_accquired = 'WeChat';
//                                    $product_abberivation->wechat_id = $wechat_id;
//                                    $product_abberivation->created_at = date('Y-m-d H:i:s', time());
//                                    $product_abberivation->save(false);
//
//                                    //Product Channel/Store Table Entry
//                                    $custom_productChannelModel = new ProductChannel;
//                                    $custom_productChannelModel->channel_id = $channel_id;
//                                    $custom_productChannelModel->product_id = $productModel->id;
//                                    $custom_productChannelModel->status = 'no';
//                                    $custom_productChannelModel->created_at = date('Y-m-d H:i:s', time());
//                                    $custom_productChannelModel->save(false);
//
//                                endif;
//                            /* If order product is already exist in database */
//                            else:
//                                $product_id = $product_exist_check->id;
//                                /* Save Product Abbrivation Id */
//                                $ProductChannel_check = ProductChannel::find()->Where(['channel_id' => $channel_id, 'product_id' => $product_exist_check->id])->one();
//                                if (empty($ProductChannel_check)):
//
//                                    $product_abberivation = new ProductAbbrivation();
//                                    $product_abberivation->channel_abb_id = $Channel_abbr_id;
//                                    $product_abberivation->product_id = $product_exist_check->id;
//                                    $product_abberivation->channel_accquired = 'WeChat';
//                                    $product_abberivation->wechat_id = $wechat_id;
//                                    $product_abberivation->created_at = date('Y-m-d H:i:s', time());
//                                    $product_abberivation->save(false);
//
//                                    //Product Channel/Store Table Entry
//                                    $custom_productChannelModel = new ProductChannel;
//                                    $custom_productChannelModel->channel_id = $channel_id;
//                                    $custom_productChannelModel->product_id = $product_exist_check->id;
//                                    $custom_productChannelModel->status = 'no';
//                                    $custom_productChannelModel->created_at = date('Y-m-d H:i:s', time());
//                                    $custom_productChannelModel->save(false);
//
//                                endif;
//
//                            endif;
//                        }
//
//                        $db_order_id = $order_model->order_ID;
//                        $order_products_model = new OrdersProducts;
//
//                        $order_products_model->order_Id = $db_order_id;
//                        $order_products_model->product_Id = isset($product_id) ? $product_id : '';
//                        $order_products_model->qty = $product_order_data[0]->quantity;
//                        $order_products_model->order_product_sku = $product_order_data[0]->products->en->product->sku;
//                        $order_products_model->created_at = $create_date;
//
//                        //Save Elliot User id
//                        $order_products_model->elliot_user_id = $user_id;
//                        $order_products_model->save(false);
//
//                    endforeach;
//
//                    //Save Order channels
//                    $order_channels_model = new OrderChannel;
//                    $order_channels_model->order_id = $db_order_id;
//                    $order_channels_model->channel_id = $channel_id;
//                    $order_channels_model->created_at = $create_date;
//                    $order_channels_model->save(false);
//                endif;
		endif;
	    endforeach;
	    // die("uu");
	}
	if (!isset($dt['process'])) {
	    $get_rec = ChannelConnection::find()->Where(['user_id' => $id, 'channel_id' => $channel_id])->one();
	    $get_rec->import_status = 'Completed';
	    $get_rec->save(false);

	    $connection = \Yii::$app->db;
	    $add_we_chat_curr = $connection->createCommand()->insert('channelsetting', [
			'channel_id' => $channel_id,
			'user_id' => $id,
			'channel_name' => 'WeChat',
			'setting_key' => 'currency',
			'setting_value' => 'CNY',
			'created_at' => date('Y-m-d h:i:s', time()),
		    ])->execute();
	}
	$notif_type = 'WeChat';
	$notif_db = Notification::find()->Where(['notif_type' => $notif_type])->one();
	if (empty($notif_db)):
	    $notification_model = new Notification();
	    $notification_model->user_id = $id;
	    $notification_model->notif_type = $notif_type;
	    $notification_model->notif_description = 'Your WeChat data has been successfully imported.';
	    $notification_model->created_at = date('Y-m-d h:i:s', time());
	    $notification_model->save(false);
	endif;

	$currency_channel_exists = Channelsetting::find()->Where(['channel_id' => $channel_id, 'setting_key' => 'currency', 'user_id' => $user_id])->one();
	if (empty($currency_channel_exists)) {
	    $channel_setting = new Channelsetting();
	    $channel_setting->channel_id = $channel_id;
	    $channel_setting->user_id = $user_id;
	    $channel_setting->channel_name = 'WeChat';
	    $channel_setting->setting_key = 'currency';
	    $channel_setting->setting_value = 'CNY';
	    $channel_setting->mul_channel_id = $channel_connection_id;
	    $channel_setting->created_at = date('Y-m-d H:i:s');
	    $channel_setting->save(false);
	}

	if (!isset($dt['process'])) {
	    $msg = 'Your WeChat Store account has been created and data has been imported in the Elliot';
	    $sub = 'Success, Your WeChat Store is Now Connected';
	    $company_name = ucfirst($get_big_id_users->company_name);
	    $ml2 = Yii::$app->mailer->compose()
		    //->getSwiftMessage()->getHeaders()->addTextHeader('name', 'value')
		    ->setFrom('elliot@helloiamelliot.com')
		    ->setTo($get_big_id_users->email)
		    ->setSubject($sub)
		    ->setHtmlBody(
			    '<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="background-color:#fcfcfc;padding: 60px 10px">
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="1" cellspacing="0" width="600" id="emailContainer" style="background-color:#fff;padding:25px;-webkit-box-shadow: 0px 5px 30px 0px rgba(50, 50, 50, 0.2);-moz-box-shadow:0px 5px 30px 0px rgba(50, 50, 50, 0.2);box-shadow:0px 5px 30px 0px rgba(50, 50, 50, 0.2);">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailHeader">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <center><img src="http://beta.brstdev.com/adrianna/elliot-logo.jpg" alt="Logo13" title="Logo" style="display:block; width: 100px;"/></center>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellpadding="1" cellspacing="0" width="100%" id="emailBody">
                                                <tr>
                                                    <td valign="top">
                                                        <p style="color: #000000;text-align: left;font-family: OpenSans;font-size: 14.0px;font-style: normal;font-stretch: normal;font-weight: 400;">
                                                            ' . $company_name . ',
                                                       </p>                                                    
                                                       </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                        <p style="color: #000000;text-align: left;font-family: OpenSans;font-size: 14.0px;font-style: normal;font-stretch: normal;font-weight: 400;line-height:25px;">
                                                        ' . $msg . '</p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellpadding="1" cellspacing="0" width="100%" id="emailFooter">
                                                    <tr>
                                                        <tdvalign="top">
                                                           <p>Thank you,<br>
                                                            <b>Elliot</b></p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>'
		    )
		    ->send();
	}
	/* $storename = 'WeChat';
	  $file = 'WeChat_';
	  Integrations::Writefile($storename, $file, $id); */
	$CronTasks_data = CronTasks::find()->where(['elliot_user_id' => $id, 'task_source' => 'WeChat'])->one();
	if (!empty($CronTasks_data)) {
	    $CronTasks_data->status = "Completed";
	    $CronTasks_data->updated_at = date('Y-m-d h:i:s', time());
	    $CronTasks_data->save(false);
	} else {
	    // $CronTasks=new CronTasks;
	    // $CronTasks->task_source = "WeChat";
	    // $CronTasks->task_name = "Import";
	    // $CronTasks->status = "Completed";
	    // $CronTasks->updated_at = date('Y-m-d h:i:s', time());
	    // $CronTasks->save(false);

	    $CronTasks = new CronTasks();
	    $CronTasks->elliot_user_id = $user_id;
	    $CronTasks->task_name = 'import';
	    $CronTasks->task_source = 'WeChat';
	    $CronTasks->status = 'Completed';
	    $CronTasks->created_at = date('Y-m-d h:i:s', time());
	    $CronTasks->updated_at = date('Y-m-d h:i:s', time());
	    $CronTasks->save();
	}

	return $this->redirect(['/']);

	return $this->render('Wechatimport');
    }

    public function actionFlipkartcallback() {
	echo "testing";

	$c_cat = curl_init();

	$method = "GET";

	$url = "https://sandbox-api.flipkart.net/oauth-service/oauth/token?grant_type=client_credentials&scope=Seller_Api";
	curl_setopt_array($c_cat, array(
	    CURLOPT_URL => "$url",
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_ENCODING => "",
	    CURLOPT_MAXREDIRS => 10,
	    CURLOPT_TIMEOUT => 30,
	    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	    CURLOPT_CUSTOMREQUEST => $method,
	    CURLOPT_POSTFIELDS => "",
	    CURLOPT_HTTPHEADER => array(
		"appid: 8b34173ba688886918893092905991060a39",
		"app-secret: 24418775faaf8e50312cd6a6207f6759c"
	    ),
	));

	$rs = curl_exec($c_cat);
	print_r($rs);

	$action = 'https://sandbox-api.flipkart.net/oauth-service/oauth/token?grant_type=client_credentials&scope=Seller_Api';

	$headers = array();
	$headers[] = 'content-type: application/json';
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $action);
	curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
//        curl_setopt($curl, CURLOPT_USERPWD, "<id>:<key>");
	curl_setopt($curl, CURLOPT_USERPWD, '8b34173ba688886918893092905991060a39:24418775faaf8e50312cd6a6207f6759c');
	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
	curl_setopt($curl, CURLOPT_HEADER, 0);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
	$result = curl_exec($curl);
	echo '<pre>';
	print_r($result);
	curl_close($curl);
    }

    public function actionSkuupdate($id) {

	$users_Id = $id;
	$get_big_id_users = User::find()->where(['id' => $id])->one();
	$user_company = $get_big_id_users->company_name;
	$config = yii\helpers\ArrayHelper::merge(
			require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_big_id_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
	);
	$application = new yii\web\Application($config);
	$webhookContent = '';
	$webhook = fopen('php://input', 'rb');
	while (!feof($webhook)) {
	    $webhookContent .= fread($webhook, 4096);
	}

	$myfile = fopen($_SERVER['DOCUMENT_ROOT'] . "/bigcommerce_callback.php", "w") or die("Unable to open file!");
	fwrite($myfile, $webhookContent);
	fclose($myfile);

	$response = json_decode($webhookContent);

	$Stores_data = Stores::find()->Where(['store_name' => 'BigCommerce'])->with('storeconnection')->one();
	$bigcommerce_store_id = $Stores_data->store_id;
	$Stores_connection = Stores::find()->Where(['store_name' => 'BigCommerce'])->with('storeconnection')->one();
	$big_client_id = $Stores_connection->storeconnection->big_client_id;
	$auth_token = $Stores_connection->storeconnection->big_access_token;
	$big_store_hash = $Stores_connection->storeconnection->big_store_hash;

	Bigcommerce::configure(array(
	    'client_id' => $big_client_id,
	    'auth_token' => $auth_token,
	    'store_hash' => $big_store_hash
	));

//        Bigcommerce::deleteWebhook(12399043);
//        
//        $hookrs = Bigcommerce::createWebhook(array(
//                        'scope' => 'store/sku/*',
//                        'destination' => 'https://ravi1.s86.co/channels/skuupdate?id=' . $users_Id,
//                        'is_active' => true
//            ));
////        
//        $hks=Bigcommerce::listWebhooks();
//        echo "<pre>";
//        echo "uuu";
//        print_r($hks);
//        die;
	if ($response->data->type == "sku"):
	    $p_id = $response->data->sku->product_id;
	    $sku_id = $response->data->id;
	    $product_sku = Bigcommerce::getResource("/products/" . $p_id . "/skus/" . $sku_id);
	    $saved_product_id = '';
	    $checkVarItemModel = ProductVariation::find()->where(['store_variation_id' => 'BGC' . $product_sku->id])->all();
	    if (!empty($checkVarItemModel)):
		foreach ($checkVarItemModel as $chk):
		    $chk->delete();
		    $saved_product_id = $chk->product_id;
		endforeach;
	    endif;
	    $users_Id = $id;

	    if (isset($product_sku->sku)):
//                $get_variation_sku_al = ProductVariation::find()->where(['store_variation_id' => 'BGC' . $product_sku->id, 'item_name' => 'SKU'])->one();
		$get_variation_sku_al = ProductVariation::find()->where(['store_variation_id' => 'BGC72', 'item_name' => 'SKU'])->one();
		if (empty($get_variation_sku_al)):
		    $newP_Var_Model1 = new ProductVariation;
		    $newP_Var_Model1->product_id = $saved_product_id;
		    $newP_Var_Model1->elliot_user_id = $users_Id;
		    $newP_Var_Model1->item_name = 'SKU';
		    $newP_Var_Model1->item_value = $product_sku->sku;
		    $newP_Var_Model1->store_variation_id = 'BGC' . $product_sku->id;
		    $created_date = date('Y-m-d H:i:s', time());
		    $newP_Var_Model1->created_at = $created_date;
		    $newP_Var_Model1->save(false);
		endif;

	    endif;

	    //UPC
	    if (isset($product_sku->upc)):
		$newP_Var_Model2 = new ProductVariation;
		$newP_Var_Model2->product_id = $saved_product_id;
		$newP_Var_Model2->elliot_user_id = $users_Id;
		$newP_Var_Model2->item_name = 'UPC';
		$newP_Var_Model2->item_value = $product_sku->upc;
		$newP_Var_Model2->store_variation_id = 'BGC' . $product_sku->id;
		$created_date = date('Y-m-d H:i:s', time());
		$newP_Var_Model2->created_at = $created_date;
		$newP_Var_Model2->save(false);
	    endif;
	    //Price
	    if (isset($product_sku->adjusted_price)):
		$newP_Var_Model3 = new ProductVariation;
		$newP_Var_Model3->product_id = $saved_product_id;
		$newP_Var_Model3->elliot_user_id = $users_Id;
		$newP_Var_Model3->item_name = 'Price';
		$newP_Var_Model3->item_value = $product_sku->adjusted_price;
		$newP_Var_Model3->store_variation_id = 'BGC' . $product_sku->id;
		$created_date = date('Y-m-d H:i:s', time());
		$newP_Var_Model3->created_at = $created_date;
		$newP_Var_Model3->save(false);
	    endif;
	    //Inventory
	    if (isset($product_sku->inventory_level)):
		$newP_Var_Model4 = new ProductVariation;
		$newP_Var_Model4->product_id = $saved_product_id;
		$newP_Var_Model4->elliot_user_id = $users_Id;
		$newP_Var_Model4->item_name = 'Inventory';
		$newP_Var_Model4->item_value = $product_sku->inventory_level;
		$newP_Var_Model4->store_variation_id = 'BGC' . $product_sku->id;
		$created_date = date('Y-m-d H:i:s', time());
		$newP_Var_Model4->created_at = $created_date;
		$newP_Var_Model4->save(false);
	    endif;
	    //Weight
	    if (isset($product_sku->adjusted_weight)):
		$newP_Var_Model5 = new ProductVariation;
		$newP_Var_Model5->product_id = $saved_product_id;
		$newP_Var_Model5->elliot_user_id = $users_Id;
		$newP_Var_Model5->item_name = 'Weight';
		$newP_Var_Model5->item_value = $product_sku->adjusted_weight;
		$newP_Var_Model5->store_variation_id = 'BGC' . $product_sku->id;
		$created_date = date('Y-m-d H:i:s', time());
		$newP_Var_Model5->created_at = $created_date;
		$newP_Var_Model5->save(false);
	    endif;
	    //is_purchasing_disabled
	    if (isset($product_sku->is_purchasing_disabled)):
		$newP_Var_Model6 = new ProductVariation;
		$newP_Var_Model6->product_id = $saved_product_id;
		$newP_Var_Model6->elliot_user_id = $users_Id;
		$newP_Var_Model6->item_name = 'Purchase_disable';
		$newP_Var_Model6->item_value = $product_sku->is_purchasing_disabled;
		$newP_Var_Model6->store_variation_id = 'BGC' . $product_sku->id;
		$created_date = date('Y-m-d H:i:s', time());
		$newP_Var_Model6->created_at = $created_date;
		$newP_Var_Model6->save(false);
	    endif;
	    //is_purchasing_disabled_msg
	    if (isset($product_sku->purchasing_disabled_message)):
		$newP_Var_Model7 = new ProductVariation;
		$newP_Var_Model7->product_id = $saved_product_id;
		$newP_Var_Model7->elliot_user_id = $users_Id;
		$newP_Var_Model7->item_name = 'Purchase_disable_message';
		$newP_Var_Model7->item_value = $product_sku->purchasing_disabled_message;
		$newP_Var_Model7->store_variation_id = 'BGC' . $product_sku->id;
		$created_date = date('Y-m-d H:i:s', time());
		$newP_Var_Model7->created_at = $created_date;
		$newP_Var_Model7->save(false);
	    endif;
	    //image_file
	    if (isset($product_sku->image_file)):
		$newP_Var_Model8 = new ProductVariation;
		$newP_Var_Model8->product_id = $saved_product_id;
		$newP_Var_Model8->elliot_user_id = $users_Id;
		$newP_Var_Model8->item_name = 'Image';
		$newP_Var_Model8->item_value = $product_sku->image_file;
		$newP_Var_Model8->store_variation_id = 'BGC' . $product_sku->id;
		$created_date = date('Y-m-d H:i:s', time());
		$newP_Var_Model8->created_at = $created_date;
		$newP_Var_Model8->save(false);
	    endif;
	    //Variants 
	    if (isset($product_sku->options)):
		foreach ($product_sku->options as $option):
		    $var_op_id = $option->product_option_id;
		    $op_object = Bigcommerce::getProductOption($p_id, $var_op_id);
		    if (!empty($op_object)):
			$var_op_val_id = $option->option_value_id;
			$op_val_object = Bigcommerce::getOptionValue($op_object->option_id, $var_op_val_id);
			if (!empty($op_val_object)):
			    $Pop_object = Bigcommerce::getOption($op_object->option_id);
			    if (!empty($Pop_object)):
				$get_variation = Variations::find()->select('variations_ID')->where(['variation_name' => $Pop_object->name, 'elliot_user_id' => $users_Id])->one();
				if (!empty($get_variation)):
				    $get_variation_already = ProductVariation::find()->where(['store_variation_id' => 'BGC' . $product_sku->id, 'item_name' => $Pop_object->name])->one();
				    if (empty($get_variation_already)):
					$newP_Var_Model9 = new ProductVariation;
					$newP_Var_Model9->product_id = $saved_product_id;
					$newP_Var_Model9->elliot_user_id = $users_Id;
					$newP_Var_Model9->variation_id = $get_variation->variations_ID;
					$newP_Var_Model9->item_name = $Pop_object->name;
					$newP_Var_Model9->item_value = $op_val_object->label;
					$newP_Var_Model9->store_variation_id = 'BGC' . $product_sku->id;
					$created_date = date('Y-m-d H:i:s', time());
					$newP_Var_Model9->created_at = $created_date;
					$newP_Var_Model9->save(false);
				    endif;

				endif;
			    endif;
			endif;
		    endif;
		endforeach;
	    endif;
	endif;
    }

    public function actionConnectordersajax() {

	$post = Yii::$app->request->get();
	$orders_data = $post['orders'];
	$store_id = '';

	$channel_name_explode = explode(" ", $orders_data);
	$channel_name_check = $channel_name_explode[0];
	$check_valid_store = Stores::find()->where(['store_name' => $channel_name_check])->one();
	if (!empty($check_valid_store)) {
	    $fin_channel = trim($channel_name_check);
	} else {
	    $fin_channel = $orders_data;
	}

	$order_type = array("BigCommerce", "Magento", "Shopify", "WooCommerce", "VTEX", "Magento2", "ShopifyPlus");
	if (in_array($fin_channel, $order_type)) {

	    $channel_space_pos = strpos($orders_data, ' ');
	    $country_sub_str = substr($orders_data, $channel_space_pos);
	    $store_country = trim($country_sub_str);

	    $store_details_data = StoreDetails::find()->Where(['channel_accquired' => $fin_channel, 'country' => $store_country])->one();
	    $store_connection_id = $store_details_data->store_connection_id;
	    $store_id = $store_connection_id;
	    $store_id_channel_id = $store_connection_id;
	    $store_table_column = 'order_channel.store_id';
	} else {

	    if ($fin_channel == "WeChat") {
		$channels_data = Channels::find()->Where(['parent_name' => 'channel_' . $fin_channel])->one();
	    } else {
		$channels_data = Channels::find()->Where(['channel_name' => $fin_channel])->one();
	    }

	    $channel_id = $channels_data->channel_ID;
	    $store_id_channel_id = $channels_data->channel_ID;
	    $store_table_column = 'order_channel.channel_id';
	}


	$start = $post['start'];
	$length = $post['length'];
	$draw = $post['draw'];
	$search = $post['search']['value'] ? $post['search']['value'] : '';
	$orderby = $post['order']['0']['column'] ? $post['order']['0']['column'] : '';
	$sortbyvalue = true;
	if ($orderby == 1) {
	    $orderby_str = 'orders.channel_abb_id';
	    $sortbyvalue = true;
	} elseif ($orderby == 2) {
	    $orderby_str = 'Customer_User.first_name';
	} elseif ($orderby == 4) {
	    $orderby_str = 'orders.total_amount';
	    $sortbyvalue = true;
	} elseif ($orderby == 5) {
	    $orderby_str = 'orders.order_date';
	    $sortbyvalue = true;
	} elseif ($orderby == 6) {
	    $orderby_str = 'orders.total_amount';
	    $sortbyvalue = true;
	} else {
	    $orderby_str = 'orders.channel_abb_id';
	    $sortbyvalue = true;
	}
	$asc = $post['order']['0']['dir'] ? $post['order']['0']['dir'] : '';

//        $count = Orders::find()->Where(['elliot_user_id' => Yii::$app->user->identity->id])->with('customer')->count();

	$count = Orders::find()
		->joinWith('customer')
		->joinWith('orderChannels')
		->where(['and', ['orders.elliot_user_id' => Yii::$app->user->identity->id], ['orders.order_visible_status' => 'active']])
		->andWhere([$store_table_column => $store_id_channel_id])
		->count();
	$response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count);
	if (!empty($store_id)) {
	    $orders_data = Orders::find()
                ->joinWith('customer')
                ->joinWith('orderChannels')
                ->where(['and', ['orders.elliot_user_id' => Yii::$app->user->identity->id], ['orders.order_visible_status' => 'active']])
                ->limit($length)->offset($start)
                ->andFilterWhere([
                    'or',
                        ['like', 'orders.channel_abb_id', $search],
                        ['like', 'orders.total_amount', $search],
                        ['like', 'concat(Customer_User.first_name," ",Customer_User.last_name)', $search],
                        ['like', 'orders.order_date', $search],
                        ['like', 'concat(orders.ship_city,", ",orders.ship_country)', $search],
                ])
                ->andWhere(['order_channel.store_id' => $store_id])
                ->orderBy($orderby_str . " " . $asc)
                ->all();
            if($search){
                $count = Orders::find()
                    ->joinWith('customer')
                    ->joinWith('orderChannels')
                    ->where(['and', ['orders.elliot_user_id' => Yii::$app->user->identity->id], ['orders.order_visible_status' => 'active']])
                    ->andFilterWhere([
                        'or',
                            ['like', 'orders.channel_abb_id', $search],
                            ['like', 'orders.total_amount', $search],
                            ['like', 'concat(Customer_User.first_name," ",Customer_User.last_name)', $search],
                            ['like', 'orders.order_date', $search],
                            ['like', 'concat(orders.ship_city,", ",orders.ship_country)', $search],
                    ])
                    ->andWhere(['order_channel.store_id' => $store_id])
                    ->count();
            }
        }
        else{
	    $orders_data = Orders::find()
                ->joinWith('customer')
                ->joinWith('orderChannels')
                ->where(['and', ['orders.elliot_user_id' => Yii::$app->user->identity->id], ['orders.order_visible_status' => 'active']])
                ->limit($length)->offset($start)
                ->andFilterWhere([
                    'or',
                        ['like', 'orders.channel_abb_id', $search],
                        ['like', 'orders.total_amount', $search],
                        ['like', 'concat(Customer_User.first_name," ",Customer_User.last_name)', $search],
                        ['like', 'orders.order_date', $search],
                        ['like', 'concat(orders.ship_city,", ",orders.ship_country)', $search],
                ])
                ->andWhere(['order_channel.channel_id' => $channel_id])
                ->orderBy($orderby_str . " " . $asc)
                ->all();
            if($search!=''){
                $count = Orders::find()
                    ->joinWith('customer')
                    ->joinWith('orderChannels')
                    ->where(['and', ['orders.elliot_user_id' => Yii::$app->user->identity->id], ['orders.order_visible_status' => 'active']])
                    ->andFilterWhere([
                        'or',
                            ['like', 'orders.channel_abb_id', $search],
                            ['like', 'orders.total_amount', $search],
                            ['like', 'concat(Customer_User.first_name," ",Customer_User.last_name)', $search],
                            ['like', 'orders.order_date', $search],
                            ['like', 'concat(orders.ship_city,", ",orders.ship_country)', $search],
                    ])
                    ->andWhere(['order_channel.channel_id' => $channel_id])
                    ->count();
            }
        }

	if (!empty($orders_data)) :
	    $i = 1;
	    foreach ($orders_data as $orders_data_value) :

		$channel_abb_id = isset($orders_data_value->channel_abb_id) ? $orders_data_value->channel_abb_id : "";
		$channel_explode = explode('_', $channel_abb_id);
		$channel_name = $channel_explode[0];

		//magento store details
		$magento_store_id = isset($orders_data_value->magento_store_id) ? $orders_data_value->magento_store_id : "";
		$magento_store_name = '';
		if ($magento_store_id != "") {
		    $magento_store_detail = MagentoStores::find()->where(['magento_store_id' => $magento_store_id])->one();
		    $magento_store_name = $magento_store_detail->magento_store_name;
		}

		$store_channel_name = $orders_data_value->channel_accquired;
		$channel_img = $this->getStoreChannelImageByName($store_channel_name);

		$order_amount = isset($orders_data_value->total_amount) ? $orders_data_value->total_amount : 0;
		$order_value = number_format((float) $order_amount, 2, '.', ',');
		$firstname = isset($orders_data_value->customer->first_name) ? $orders_data_value->customer->first_name : '';
		$lname = isset($orders_data_value->customer->last_name) ? $orders_data_value->customer->last_name : '';
		$date_order = date('m/d/y h:i A', strtotime($orders_data_value->order_date));
		$city = $orders_data_value->ship_city ? $orders_data_value->ship_city : "";
		$country = $orders_data_value->ship_country ? $orders_data_value->ship_country : "";
		$ship = $city . ', ' . $country;
		$ship_to = preg_replace('/^([^a-zA-Z0-9])*/', '', $ship);



		$user = Yii::$app->user->identity;
		$conversion_rate = 1;
		if (isset($user->currency)) {
		    
		    $conversion_rate = Stores::getDbConversionRate($user->currency);
		    $order_value = $order_value * $conversion_rate;
		    $order_value = number_format((float) $order_value, 2, '.', ',');
		    
//		    $username = Yii::$app->params['xe_account_id'];
//		    $password = Yii::$app->params['xe_account_api_key'];
//		    $URL = Yii::$app->params['xe_base_url'] . 'convert_from.json/?from=USD&to=' . $user->currency . '&amount=1';
//
//		    $ch = curl_init();
//		    curl_setopt($ch, CURLOPT_URL, $URL);
//		    curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
//		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//		    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
//		    curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
//		    $result = curl_exec($ch);
//		    $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
//		    curl_close($ch);
////echo'<pre>';
//		    $result = json_decode($result, true);
//		    if (isset($result) and ! empty($result) and isset($result['to']) and isset($result['to'][0]) and isset($result['to'][0]['quotecurrency'])) {
//			$conversion_rate = $result['to'][0]['mid'];
//			$conversion_rate = number_format((float) $conversion_rate, 2, '.', '');
//			$order_value = $order_value * $conversion_rate;
//			$order_value = number_format((float) $order_value, 2, '.', ',');
//		    }
		}

		$order_status = $orders_data_value->order_status;
		$label = '';
		if ($order_status == 'Completed') :
		    $label = 'label-success';
		endif;

		if ($order_status == 'Returned' || $order_status == 'Refunded' || $order_status == 'Cancel') :
		    $label = 'label-danger';
		endif;

		if ($order_status == 'In Transit'):
		    $label = 'label-primary';
		endif;

		if ($order_status == 'Awaiting Fulfillment' || $order_status == 'Incomplete' || $order_status == 'waiting-for-shipment' || $order_status == 'Pending' || $order_status == 'Awaiting Payment' || $order_status == 'On Hold'):
		    $label = 'label-warning';
		endif;
		if ($order_status == 'Shipped'):
		    $label = 'label-primary';
		endif;

		$selected_currency = \backend\models\CurrencySymbols::find()->where(['name' => strtolower($user->currency)])->select(['id', 'symbol'])->asArray()->one();
		if (isset($selected_currency) and ! empty($selected_currency)) {
		    $currency_symbol = $selected_currency['symbol'];
		}

		$arr[0] = '<div class="be-checkbox"><input name="order_check" id="ck' . $i . '" value="' . $orders_data_value->order_ID . '" name="ck1" type="checkbox" data-parsley-multiple="groups" value="bar" data-parsley-mincheck="2" data-parsley-errors-container="#error-container1" class="order_row_check"><label class="getId" for="ck' . $i . '"></label></div>';
		$arr[1] = '<a href="/orders/view/' . $orders_data_value->order_ID . '">' . $channel_abb_id . '</a>';
		$arr[2] = $firstname . ' ' . $lname;
		$arr[3] = $channel_img . '<lable class="magento-store-name">' . $magento_store_name . '</lable>';
		$arr[4] = $currency_symbol . $order_value;
		$arr[5] = $date_order;
		$arr[6] = $ship_to;
		$arr[7] = '<span class="label  ' . $label . '">' . $order_status . '</span>';
		$data_arr[] = $arr;
		$i++;
	    endforeach;

	    if ($sortbyvalue) {

		if ($orderby == 5) {

		    if ($asc == 'asc') {
			usort($data_arr, function($a, $b) {
			    $a[5] = strtotime($a[5]);
			    $b[5] = strtotime($b[5]);
			    return $a[5] - $b[5];
			});
		    } else {
			usort($data_arr, function($a, $b) {
			    $a[5] = strtotime($a[5]);
			    $b[5] = strtotime($b[5]);
			    return $b[5] - $a[5];
			});
		    }
		}

		$data_arr1 = array_values($data_arr);
	    } else {
		$data_arr1 = array_values($data_arr);
	    }

	    $response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count, "data" => $data_arr1);
	    echo json_encode($response_arr);
	else:
	    $response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count, "data" => array());
	    echo json_encode($response_arr);

	endif;
    }

    public function actionTestCall() {
	$connection = \Yii::$app->db;
	$test = array();

	$get_order_query = $connection->createCommand('SELECT order_ID FROM orders WHERE customer_id=75');
	$get_order = $get_order_query->queryAll();

	for ($i = 0; $i < count($get_order); $i++) {
	    $stores = $connection->createCommand('SELECT order_id, store_id, channel_id FROM order_channel WHERE order_id=' . $get_order[$i]['order_ID'] . '');
	    $stores_query[] = $stores->queryAll();
	}

	for ($j = 0; $j < count($stores_query); $j++) {

	    for ($i = 0; $i < count($stores_query[$j]); $i++) {

		if ($stores_query[$j][$i]['store_id'] != '' && $stores_query[$j][$i]['channel_id'] != '') {
		    echo 'yes';
		}
	    }
	}

	//echo $stores_query[5]['store_id'];
	die('here');
    }

    public function actionCustomerajax() {
	$post = Yii::$app->request->get();
	$start = $post['start'];
	$length = $post['length'];
	$draw = $post['draw'];
	$search = $post['search']['value'] ? $post['search']['value'] : '';
	$orderby = $post['order']['0']['column'] ? $post['order']['0']['column'] : '';
	$sortbyvalue = true;
	$orderby_str = 'first_name';
	if ($orderby == 1) {
	    $orderby_str = 'first_name';
	    $sortbyvalue = false;
	} elseif ($orderby == 3) {
	    $orderby_str = 'city';
	    $sortbyvalue = true;
	} elseif ($orderby == 4) {
	    $sortbyvalue = true;
	} elseif ($orderby == 5) {
	    $sortbyvalue = true;
	} else {
	    $orderby_str = 'first_name';
	    $sortbyvalue = true;
	}

	$asc = $post['order']['0']['dir'] ? $post['order']['0']['dir'] : '';
	$user_id = Yii::$app->user->identity->id;
	$total_customer_count_for_per = CustomerUser::find()->Where(['elliot_user_id' => $user_id, 'people_visible_status' => 'active'])->count();
	$count = CustomerUser::find()->Where(['elliot_user_id' => $user_id, 'people_visible_status' => 'active'])->count();

	if ($orderby == 5) {
	    $connection = \Yii::$app->db;
	    $model = $connection->createCommand('SELECT `orderSum`.*,`Customer_User`.* FROM `Customer_User` Left JOIN 
			    (SELECT `customer_id`,count(customer_id) as order_count, SUM(total_amount) as order_total FROM `orders` GROUP BY `customer_id`)
			    `orderSum` ON orderSum.customer_id = Customer_User.customer_ID ORDER BY order_total ' . $asc . ' limit ' . $start . ', ' . $length);
	    $clv_data = $model->queryAll();
	    $json = json_encode($clv_data);
	    $customers_data = json_decode($json);
	    if ($search != '') {
		$customers_data = CustomerUser::find()
                    ->Where(['and', ['elliot_user_id' => $user_id], ['people_visible_status' => 'active']])
                    ->limit($length)->offset($start)
                    ->andFilterWhere([
                        'or',
                            ['like', 'concat(first_name," ",last_name)', $search],
                            ['like', 'email', $search],
                            ['like', 'concat(city,", ",country)', $search],
                    ])
                    ->all();
                $count = CustomerUser::find()
                    ->Where(['and', ['elliot_user_id' => $user_id], ['people_visible_status' => 'active']])
                    ->andFilterWhere([
                        'or',
                            ['like', 'concat(first_name," ",last_name)', $search],
                            ['like', 'email', $search],
                            ['like', 'concat(city,", ",country)', $search],
                    ])
                    ->count();
	    }
	} else {
	    $customers_data = CustomerUser::find()
                ->Where(['and', ['elliot_user_id' => $user_id], ['people_visible_status' => 'active']])
                ->limit($length)->offset($start)
                ->andFilterWhere([
                    'or',
                        ['like', 'concat(first_name," ",last_name)', $search],
                        ['like', 'email', $search],
                        ['like', 'concat(city,", ",country)', $search],
                ])
                ->orderBy($orderby_str . " " . $asc)
                ->all();
	    $count = CustomerUser::find()
                ->Where(['and', ['elliot_user_id' => $user_id], ['people_visible_status' => 'active']])
                ->limit($length)->offset($start)
                ->andFilterWhere([
                    'or',
                        ['like', 'concat(first_name," ",last_name)', $search],
                        ['like', 'email', $search],
                        ['like', 'concat(city,", ",country)', $search],
                ])
                ->orderBy($orderby_str . " " . $asc)
                ->count();
	}

	$clv_array = array();
	$ten_highest_clv = array();
	$cust_orderID = array();
	$orders_id_array = array();
	$order_IDS = array();
	$test = array();
	$stores_query = array();

	$response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count);

	if (!empty($customers_data)) :
	    $i = 1;
	    $customer_loop = 1;
	    foreach ($customers_data as $customers_data_value) :
		$customer_id = $customers_data_value->customer_ID;
		$channel_accuired = $customers_data_value->channel_acquired;
		$explode_channel_accuired = explode(",", $channel_accuired);

		$fname = isset($customers_data_value->first_name) ? $customers_data_value->first_name : "";
		$lname = isset($customers_data_value->last_name) ? $customers_data_value->last_name : "";
		$email = isset($customers_data_value->email) ? $customers_data_value->email : "";
		$city = isset($customers_data_value->city) ? $customers_data_value->city : "";
		$country = isset($customers_data_value->country) ? $customers_data_value->country : "";
		$ship_country = isset($customers_data_value->ship_country) ? $customers_data_value->ship_country : "";
		$country_codess = isset($customers_data_value->country_iso) ? $customers_data_value->country_iso : "";

		$total_img = $this->getCustomerStoreImageLogo($customer_id);

		if (strtolower($country) == 'country') {
		    $country = '';
		}

		$magento_store_id = isset($customers_data_value->magento_store_id) ? $customers_data_value->magento_store_id : "";
		$magento_store_name = '';
		if ($magento_store_id != "") {
		    $magento_store_detail = MagentoStores::find()->where(['magento_store_id' => $magento_store_id])->one();
		    if (!empty($magento_store_detail)) {
			$magento_store_name = $magento_store_detail->magento_store_name;
		    }
		}

		$city_country = $city . ' ' . $country;
		$date_acquired = date('m-d-y', strtotime($customers_data_value->date_acquired));

		//Total Orders
		$orders_data = Orders::find()->Where(['customer_id' => $customers_data_value->customer_ID])->all();
		$count_orders = count($orders_data);

		//*******code for country flags starts here******
		$connection = \Yii::$app->db;
		$check_if_country_in_order_query = $connection->createCommand("SELECT ship_country FROM orders WHERE customer_id='" . $customers_data_value->customer_ID . "'");
		$check_if_country_in_order = $check_if_country_in_order_query->queryAll();
		$exact_code_from_order = @$check_if_country_in_order[0]['ship_country'];
		$exact_code_from_order = strtolower($exact_code_from_order);

		if ($country_codess != '') {
		    $exact_code = $country_codess;
		    $exact_code = strtolower($exact_code);
		} else {
		    if ($country == '' && $ship_country == '' && $exact_code_from_order == '') {
			$exact_code = '';
		    } elseif ($country == '' && $ship_country != '' && $exact_code_from_order == '') {
			if ($ship_country == 'USA') {
			    $ship_country = 'United States';
			}
			if (strlen($ship_country) == 2) {
			    $exact_code = $ship_country;
			    $exact_code = strtolower($exact_code);
			} else {
			    if ($ship_country == '') {
				$orders_customer = Orders::find()->Where(['customer_id' => $customers_data_value->customer_ID])->orderBy(['order_date' => SORT_DESC])->one();
				if (!empty($orders_customer)) {
				    $ship_country = $orders_customer->ship_country;
				}
			    }
			    $country_code_query = $connection->createCommand("SELECT sortname, name FROM countries WHERE name= '" . $ship_country . "'");
			    $country_code = $country_code_query->queryAll();
			    $exact_code = @$country_code[0]['sortname'];
			    $exact_code = strtolower($exact_code);
			}
		    } elseif ($country == '' && $ship_country == '' && $exact_code_from_order != '') {
			if ($exact_code_from_order == 'usa') {
			    $exact_code = 'us';
			} else {
			    if ($ship_country == '') {
				$orders_customer = Orders::find()->Where(['customer_id' => $customers_data_value->customer_ID])->orderBy(['order_date' => SORT_DESC])->one();
				if (!empty($orders_customer)) :
				    $ship_country = $orders_customer->ship_country;
				endif;
			    }

			    $country_code_query = $connection->createCommand("SELECT sortname, name FROM countries WHERE name= '" . $ship_country . "'");
			    $country_code = $country_code_query->queryAll();
			    $exact_code = @$country_code[0]['sortname'];
			    $exact_code = strtolower($exact_code);
			}
		    }
		    elseif ($country == '' && $ship_country != '' && $exact_code_from_order != '') {
			$exact_code = strtolower($exact_code_from_order);
		    } else {
			if ($country == 'USA') {
			    $country = 'United States';
			}
			if (strlen($country) == 2) {
			    $exact_code = $country;
			    $exact_code = strtolower($exact_code);
			} else {
			    if ($country == '') {
				$orders_customer = Orders::find()->Where(['customer_id' => $customers_data_value->customer_ID])->orderBy(['order_date' => SORT_DESC])->one();
				if (!empty($orders_customer)) :
				    $country = $orders_customer->ship_country;
				endif;
			    }
			    $country_code_query = $connection->createCommand("SELECT sortname, name FROM countries WHERE name= '" . $country . "'");
			    $country_code = $country_code_query->queryAll();
			    $exact_code = @$country_code[0]['sortname'];
			    $exact_code = strtolower($exact_code);
			}
		    }
		}
		if (!empty($country)) {
		    if (!empty($city)) {
			$add_val = ucwords($city) . ',' . ' ' . ucwords($country);
		    } else {
			$add_val = ucwords($country);
		    }
		} else {
		    $orders_customer = Orders::find()->Where(['customer_id' => $customer_id])->orderBy(['order_date' => SORT_DESC])->one();
		    if (!empty($orders_customer)) {
			$ship_city = $orders_customer->ship_city;
			$ship_country = $orders_customer->ship_country;
			if (!empty($ship_city)) {

			    $add_val = $ship_city . ',' . ' ' . $ship_country;
			} else {
			    $add_val = $ship_country;
			}
		    } else {
			$add_val = "";
		    }
		}


		//*******code for country flags ends here******
		//$model1 = $connection->createCommand('SELECT DISTINCT parent_name stripe_Channel_id,channel_image,parent_name FROM channels');
		if ($orderby == 5 && $search == '') {
		    $clv = $customers_data_value->order_total;
		} else {
		    $order_sum_query = $connection->createCommand('SELECT SUM(total_amount), customer_id FROM orders WHERE customer_id=' . $customer_id . ' GROUP BY customer_id');
		    $order_sum = $order_sum_query->queryAll();
		    if (!empty($order_sum)) :
			// $clv = number_format((float) @$order_sum[0]['SUM(total_amount)'], 2, '.', ',');
			$clv = $order_sum[0]['SUM(total_amount)'];
		    else :
			$clv = '0.00';
		    endif;
		}
		/*		 * **** RATING SECTION CODE BEGINS HERE ***** */
		$star_rate=$this->getStartRating($customers_data_value->customer_ID, $email, $total_customer_count_for_per);
		/*		 * ********RATING SECTION CODE ENDS HERE*************** */

		$user = Yii::$app->user->identity;
		$conversion_rate = 1;
		if (isset($user->currency)) {
		    
		    $conversion_rate = Stores::getDbConversionRate($user->currency);
		    $clv = $clv * $conversion_rate;
		    $clv = number_format((float) $clv, 2, '.', '');
		  
		}
		$arr[0] = $customers_data_value->customer_ID;
		$arr[1] = '<a href="/people/view?id=' . $customers_data_value->customer_ID . '">' . $fname . ' ' . $lname . '</a>';
		//$arr[1] = $fname . ' ' . $lname;
		$arr[2] = '<div class="channel_scrolling">' . $total_img . '<lable class="magento-store-name">' . $magento_store_name . '</lable></div>';
		if ($exact_code != '') {
		    $arr[3] = $add_val . '!' . $exact_code;
		} else {
		    $arr[3] = '';
		}
		$arr[4] = $star_rate;
		$arr[5] = $clv;
		$arr[6] = $count_orders;
		$data_arr[] = $arr;
		$i++;
		$customer_loop++;
	    endforeach;



	    if ($sortbyvalue):
		if ($orderby == 6):
		    if ($asc == 'asc'):
			usort($data_arr, function($a, $b) {
			    return $a[6] - $b[6];
			});
		    else:
			usort($data_arr, function($a, $b) {
			    return $b[6] - $a[6];
			});
		    endif;
		else:
		    if ($asc == 'asc'):
			usort($data_arr, function($a, $b) {
			    return $a[5] - $b[5];
			});
		    else:
			usort($data_arr, function($a, $b) {
			    return $b[5] - $a[5];
			});
		    endif;
		endif;

		if ($orderby == 3) {
		    if ($asc == 'asc') {
			usort($data_arr, function($a, $b) {
			    return strcmp(strtoupper($a[3]), strtoupper($b[3]));
			});
		    } else {
			usort($data_arr, function($a, $b) {
			    return strcmp(strtoupper($b[3]), strtoupper($a[3]));
			});
		    }
		}

		if ($orderby == 4) {
		    if ($asc == 'asc') {
			usort($data_arr, function($a, $b) {
			    return $a[4] - $b[4];
			});
		    } else {
			usort($data_arr, function($a, $b) {
			    return $b[4] - $a[4];
			});
		    }
		}
		$data_arr1 = array_values($data_arr);
	    else:
		$data_arr1 = array_values($data_arr);
	    endif;
//	    if ($start == 0 && $search =='') {
//		$customer_loop = 1;
//		foreach ($data_arr1 as $key => $data_arr_val) {
//		    if ($customer_loop <= $total_customer_5_star) {
//			if ($data_arr_val[5] >= 5) {
//			    $data_arr1[$key][4] = 5;
//			}
//		    }
//		    $customer_loop++;
//		}
//	    }


	    if (isset($data_arr1) and ! empty($data_arr1)) {
		$selected_currency = \backend\models\CurrencySymbols::find()->where(['name' => strtolower($user->currency)])->select(['id', 'symbol'])->asArray()->one();
		if (isset($selected_currency) and ! empty($selected_currency)) {
		    $currency_symbol = $selected_currency['symbol'];
		}
		$x = 1;

		foreach ($data_arr1 as $single) {
		    $single_array = [];
		    $single_array = $single;
		    $single_array[0] = '<div class="be-checkbox"><input name="people_check" id="ck' . $x . '" value="' . $single_array[0] . '" name="ck1" type="checkbox" data-parsley-multiple="groups" value="bar" data-parsley-mincheck="2" data-parsley-errors-container="#error-container1" class="people_row_check"><label class="getId" for="ck' . $x . '"></label></div>';
		    $code_coming = substr($single_array[3], strpos($single_array[3], "!") + 1);
		    $single_array[3] = substr($single_array[3], 0, strpos($single_array[3], "!"));
		    $single_array[3] = '<span class="flag-icon flag-icon-' . $code_coming . '"></span><span class="country_names"> ' . $single_array[3] . '</span>';
		    if ($single_array[4] == 1) {
			$single_array[4] = '<span class="mdi mdi-star yellow"></span><span class="mdi mdi-star black"></span><span class="mdi mdi-star black"></span><span class="mdi mdi-star black"></span><span class="mdi mdi-star black"></span>';
		    } elseif ($single_array[4] == 2) {
			$single_array[4] = '<span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span><span class="mdi mdi-star black"></span><span class="mdi mdi-star black"></span><span class="mdi mdi-star black"></span>';
		    } elseif ($single_array[4] == 3) {
			$single_array[4] = '<span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span><span class="mdi mdi-star black"></span><span class="mdi mdi-star black"></span>';
		    } elseif ($single_array[4] == 4) {
			$single_array[4] = '<span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span><span class="mdi mdi-star black"></span>';
		    } else {
			$single_array[4] = '<span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span>';
		    }
		    $single_array[5] = $currency_symbol . number_format($single_array[5], 2);

		    $final_array[] = $single_array;

		    $x++;
		}
	    }
	    $response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count, "data" => $final_array);
	    echo json_encode($response_arr);
	else:
	    $response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count, "data" => array());
	    echo json_encode($response_arr);

	endif;
    }

    /*
     * ajax hit from new_custom_elliot.js to delete multiple select data from people
     */

    public function actionAjaxPeopleDelete() {
	if (!isset($_POST['peopleIds']) || count($_POST['peopleIds']) == 0) {
	    return FALSE;
	} else {
	    $product_ids = $_POST['peopleIds'];
	    foreach ($product_ids as $_product_id) {
		$delete_product = CustomerUser::updateAll(array('people_visible_status' => 'in_active'), 'customer_ID = ' . $_product_id);
	    }
	    return TRUE;
	}
    }

    /* Inactive people ajax start */

    public function actionAjaxPeopleUndoDelete() {
	if (!isset($_POST['Inactive_peopleIds']) || count($_POST['Inactive_peopleIds']) == 0) {
	    return false;
	} else {
	    $inactive_peopleID = $_POST['Inactive_peopleIds'];
	    foreach ($inactive_peopleID as $people) {
		$undo_delete_customer = CustomerUser::updateAll(array('people_visible_status' => 'active'), 'customer_ID = ' . $people);
	    }
	    return true;
	}
    }

    public function actionInactivecustomerajax() {
	$post = Yii::$app->request->get();
	$connection = $post['customer'];
	$store_id = '';

	$channel_name_explode = explode(" ", $connection);
	$channel_name_check = $channel_name_explode[0];
	$check_valid_store = Stores::find()->where(['store_name' => $channel_name_check])->one();
	if (!empty($check_valid_store)) {
	    $fin_channel = trim($channel_name_check);
	} else {
	    $fin_channel = $connection;
	}
	$start = $post['start'];
	$length = $post['length'];
	$draw = $post['draw'];
	$search = $post['search']['value'] ? $post['search']['value'] : '';
	$orderby = $post['order']['0']['column'] ? $post['order']['0']['column'] : '';
	$sortbyvalue = true;
	$orderby_str = 'first_name';
	if ($orderby == 1) {
	    $orderby_str = 'first_name';
	    $sortbyvalue = false;
	} elseif ($orderby == 3) {
	    $orderby_str = 'city';
	    $sortbyvalue = true;
	} elseif ($orderby == 4) {
	    $sortbyvalue = true;
	} elseif ($orderby == 5) {
	    $sortbyvalue = true;
	} else {
	    $orderby_str = 'first_name';
	    $sortbyvalue = true;
	}
	$asc = $post['order']['0']['dir'] ? $post['order']['0']['dir'] : '';
	$user_id = Yii::$app->user->identity->id;




	/* Listing Query Store And channels */
	$check_valid_store = Stores::find()->where(['store_name' => $fin_channel])->one();
	if (!empty($check_valid_store)) {
	    $channel_space_pos = strpos($connection, ' ');
	    $country_sub_str = substr($connection, $channel_space_pos);
	    $store_country = trim($country_sub_str);
	    $store_details_data = StoreDetails::find()->Where(['channel_accquired' => $fin_channel, 'country' => $store_country])->with('storeConnection')->one();
	    if (!empty($store_details_data)) {
		$store_connection_id = $store_details_data->store_connection_id;
	    }
	    $customers_data = CustomerUser::find()->Where(['and', ['elliot_user_id' => $user_id], ['people_visible_status' => 'in_active']])
		    ->limit($length)->offset($start)
		    ->joinWith('customerabbrivation')
		    ->andFilterWhere([
			'or',
			    ['like', 'concat(first_name," ",last_name)', $search],
			    ['like', 'email', $search],
			    ['like', 'concat(city,", ",country)', $search],
		    ])
		    ->andWhere(['channel_acquired' => $fin_channel])
		    ->andWhere(['customer_abbrivation.mul_store_id' => $store_connection_id])
		    ->orderBy($orderby_str . " " . $asc)
		    ->all();

	    $count = CustomerUser::find()->Where(['elliot_user_id' => $user_id, 'channel_acquired' => $channel_name_check, 'people_visible_status' => 'in_active'])
			    ->with(['customerabbrivation' => function($query) {
				    $query->andWhere(['mul_store_id' => $store_connection_id]);
				}
			    ])->count();
	    //$count = count($customers_data);
	} else {
	    $customers_data = CustomerUser::find()->Where(['and', ['elliot_user_id' => $user_id], ['people_visible_status' => 'in_active']])
		    ->limit($length)->offset($start)
		    ->andFilterWhere([
			'or',
			    ['like', 'concat(first_name," ",last_name)', $search],
			    ['like', 'email', $search],
			    ['like', 'concat(city,", ",country)', $search],
		    ])
		    ->andWhere(['channel_acquired' => $fin_channel])
		    ->orderBy($orderby_str . " " . $asc)
		    ->all();
	    $count = CustomerUser::find()->Where(['elliot_user_id' => $user_id, 'channel_acquired' => $fin_channel, 'people_visible_status' => 'in_active'])->count();
	}

	$response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count);
	$customers_data_clv = CustomerUser::find()->Where(['elliot_user_id' => $user_id])->all();

	$clv_array = array();
	$ten_highest_clv = array();
	$cust_orderID = array();
	$orders_id_array = array();
	$order_IDS = array();
	$test = array();
	$stores_query = array();

	if (!empty($customers_data_clv)) {
	    $connection = \Yii::$app->db;

	    foreach ($customers_data_clv as $value) {

		$customer_id = $value->customer_ID;
		$order_sum_query = $connection->createCommand('SELECT SUM(total_amount), customer_id FROM orders WHERE customer_id=' . $customer_id . ' GROUP BY customer_id');
		$order_sum = $order_sum_query->queryAll();
		if (!empty($order_sum)) :
		    // $clv = number_format((float) @$order_sum[0]['SUM(total_amount)'], 2, '.', ',');
		    $clv = $order_sum[0]['SUM(total_amount)'];


		else :
		    $clv = '0.00';
		endif;

		if ($clv > 5.00) {
		    $clv_array[] = $clv;
		}
		$get_order_query = $connection->createCommand('SELECT order_ID FROM orders WHERE customer_id=' . $customer_id . '');
		$get_order = $get_order_query->queryAll();
		foreach ($get_order as $val) {
		    $order_IDS[] = $val['order_ID'];
		}
	    }

	    $total_clv = count($clv_array);
	    $clv_counter = 1;
	    $max_results = (10 / 100) * $total_clv;
	    while ($clv_counter <= $total_clv - $max_results) {
		$clv_counter++;
		/* search for the min value of views
		  and removing it. */
		$key = array_search(min($clv_array), $clv_array);
		unset($clv_array[$key]);
	    }
	    foreach ($customers_data_clv as $array) {
		$customer_id = $array->customer_ID;
		$order_sum_query = $connection->createCommand('SELECT SUM(total_amount), customer_id FROM orders WHERE customer_id=' . $customer_id . ' GROUP BY customer_id');
		$order_sum = $order_sum_query->queryAll();
		if (!empty($order_sum)) :
		    // $clv = number_format((float) @$order_sum[0]['SUM(total_amount)'], 2, '.', ',');
		    $clv = $order_sum[0]['SUM(total_amount)'];

		else :
		    $clv = '0.00';
		endif;

		foreach ($clv_array as $key => $value) {
		    if ($clv == $value) {
			array_push($ten_highest_clv, $value);
		    }
		}
	    }
	}

	if (!empty($customers_data)) {

	    foreach ($customers_data as $customers_data_value) {
		$customer_id = $customers_data_value->customer_ID;
		$channel_accuired = $customers_data_value->channel_acquired;
		$explode_channel_accuired = explode(",", $channel_accuired);

		$fname = isset($customers_data_value->first_name) ? $customers_data_value->first_name : "";
		$lname = isset($customers_data_value->last_name) ? $customers_data_value->last_name : "";
		$email = isset($customers_data_value->email) ? $customers_data_value->email : "";
		$city = isset($customers_data_value->city) ? $customers_data_value->city : "";
		$country = isset($customers_data_value->country) ? $customers_data_value->country : "";
		$ship_country = isset($customers_data_value->ship_country) ? $customers_data_value->ship_country : "";
		$country_codess = isset($customers_data_value->country_iso) ? $customers_data_value->country_iso : "";
		if (strtolower($country) == 'country') {
		    $country = '';
		}

		$magento_store_id = isset($customers_data_value->magento_store_id) ? $customers_data_value->magento_store_id : "";
		$magento_store_name = '';
		if ($magento_store_id != "") {
		    $magento_store_detail = MagentoStores::find()->where(['magento_store_id' => $magento_store_id])->one();
		    if (!empty($magento_store_detail)) {
			$magento_store_name = $magento_store_detail->magento_store_name;
		    }
		}

		$city_country = $city . ' ' . $country;
		$date_acquired = date('m-d-y', strtotime($customers_data_value->date_acquired));

		//Total Orders
		$orders_data = Orders::find()->Where(['customer_id' => $customers_data_value->customer_ID])->all();
		$count_orders = count($orders_data);

		//***Code for country flags start here***//

		$connection = \Yii::$app->db;
		$check_if_country_in_order_query = $connection->createCommand("SELECT ship_country FROM orders WHERE customer_id='" . $customers_data_value->customer_ID . "'");
		$check_if_country_in_order = $check_if_country_in_order_query->queryAll();
		$exact_code_from_order = @$check_if_country_in_order[0]['ship_country'];
		$exact_code_from_order = strtolower($exact_code_from_order);
		if ($country_codess != '') {

		    $exact_code = $country_codess;
		    $exact_code = strtolower($exact_code);
		} else {
		    if ($country == '' && $ship_country == '' && $exact_code_from_order == '') {
			$exact_code = '';
		    } elseif ($country == '' && $ship_country != '' && $exact_code_from_order == '') {
			if ($ship_country == 'USA') {
			    $ship_country = 'United States';
			}

			if (strlen($ship_country) == 2) {
			    $exact_code = $ship_country;
			    $exact_code = strtolower($exact_code);
			} else {
			    if ($ship_country == '') {
				$orders_customer = Orders::find()->Where(['customer_id' => $customers_data_value->customer_ID])->orderBy(['order_date' => SORT_DESC])->one();
				if (!empty($orders_customer)) :
				    $ship_country = $orders_customer->ship_country;
				endif;
			    }

			    $country_code_query = $connection->createCommand("SELECT sortname, name FROM countries WHERE name= '" . $ship_country . "'");
			    $country_code = $country_code_query->queryAll();
			    $exact_code = @$country_code[0]['sortname'];
			    $exact_code = strtolower($exact_code);
			}
		    }
		    elseif ($country == '' && $ship_country == '' && $exact_code_from_order != '') {

			if ($exact_code_from_order == 'usa') {
			    $exact_code = 'us';
			} else {
			    if ($ship_country == '') {
				$orders_customer = Orders::find()->Where(['customer_id' => $customers_data_value->customer_ID])->orderBy(['order_date' => SORT_DESC])->one();
				if (!empty($orders_customer)) :
				    $ship_country = $orders_customer->ship_country;
				endif;
			    }

			    $country_code_query = $connection->createCommand("SELECT sortname, name FROM countries WHERE name= '" . $ship_country . "'");
			    $country_code = $country_code_query->queryAll();
			    $exact_code = @$country_code[0]['sortname'];
			    $exact_code = strtolower($exact_code);
			}
		    } elseif ($country == '' && $ship_country != '' && $exact_code_from_order != '') {
			$exact_code = strtolower($exact_code_from_order);
		    } else {
			if ($country == 'USA') {
			    $country = 'United States';
			}

			if (strlen($country) == 2) {

			    $exact_code = $country;
			    $exact_code = strtolower($exact_code);
			} else {
			    if ($country == '') {
				$orders_customer = Orders::find()->Where(['customer_id' => $customers_data_value->customer_ID])->orderBy(['order_date' => SORT_DESC])->one();
				if (!empty($orders_customer)) :
				    $country = $orders_customer->ship_country;
				endif;
			    }

			    $country_code_query = $connection->createCommand("SELECT sortname, name FROM countries WHERE name= '" . $country . "'");
			    $country_code = $country_code_query->queryAll();
			    $exact_code = @$country_code[0]['sortname'];
			    $exact_code = strtolower($exact_code);
			}
		    }
		}

		//***Code for country flags ends here***//
		if (!empty($country)) {
		    if (!empty($city)) {
			$add_val = ucwords($city) . ',' . ' ' . ucwords($country);
		    } else {
			$add_val = ucwords($country);
		    }
		} else {
		    $orders_customer = Orders::find()->Where(['customer_id' => $customer_id])->orderBy(['order_date' => SORT_DESC])->one();
		    if (!empty($orders_customer)) {
			$ship_city = $orders_customer->ship_city;
			$ship_country = $orders_customer->ship_country;
			if (!empty($ship_city)) {

			    $add_val = $ship_city . ',' . ' ' . $ship_country;
			} else {
			    $add_val = $ship_country;
			}
		    } else {
			$add_val = "";
		    }
		}

		$connection = \Yii::$app->db;
		//$model1 = $connection->createCommand('SELECT DISTINCT parent_name stripe_Channel_id,channel_image,parent_name FROM channels');
		$order_sum_query = $connection->createCommand('SELECT SUM(total_amount), customer_id FROM orders WHERE customer_id=' . $customer_id . ' GROUP BY customer_id');
		$order_sum = $order_sum_query->queryAll();
		if (!empty($order_sum)) :
		    $clv = number_format((float) @$order_sum[0]['SUM(total_amount)'], 2, '.', '');
		else :
		    $clv = '0.00';
		endif;

		/*		 * *****RATING SECTION CODE BEGINS HERE********* */
//		$star_array = array();
//		$star_count = 1;
//		array_push($star_array, $star_count); //default one star
//		$order_count_query = $connection->createCommand("SELECT COUNT(order_ID) AS total_orders FROM orders WHERE customer_id='" . $customers_data_value->customer_ID . "'");
//		$order_count_result = $order_count_query->queryAll();
//		$order_count = @$order_count_result[0]['total_orders'];
//		if ($order_count >= 1 && $order_count <= 10) {
//		    $star_count_order = 3;
//		    array_push($star_array, $star_count_order); // star 3 for 1-10 order count
//		}
//
//
//		//value stores customer email 2 stars
//		if ($email != '') {
//		    $star_count_email = 2;
//		    array_push($star_array, $star_count_email); // star 2 if customer has email
//		}
//
//
//		$get_order_query = $connection->createCommand('SELECT order_ID FROM orders WHERE customer_id=' . $customers_data_value->customer_ID . '');
//		$get_order = $get_order_query->queryAll();
//
//		for ($i = 0; $i < count($get_order); $i++) {
//		    $stores = $connection->createCommand('SELECT order_id, store_id, channel_id FROM order_channel WHERE order_id=' . $get_order[$i]['order_ID'] . '');
//		    $stores_query[] = $stores->queryAll();
//		}
//
//		for ($j = 0; $j < count($stores_query); $j++) {
//
//		    for ($i = 0; $i < count($stores_query[$j]); $i++) {
//
//			if ($stores_query[$j][$i]['store_id'] != '' && $stores_query[$j][$i]['channel_id'] != '') {
//
//			    array_push($orders_id_array, $stores_query[$j][$i]['order_id']);
//			}
//		    }
//		}
//
//		for ($x = 0; $x < count($get_order); $x++) {
//		    if (in_array($get_order[$x]['order_ID'], $orders_id_array) == true) {
//			$star_count_storess = 4;
//			array_push($star_array, $star_count_storess);
//		    }
//		}
//
//		if (in_array($clv, $ten_highest_clv) == true) {
//		    $star_count_clv = 5;
//		    array_push($star_array, $star_count_clv); // 5 star for top 10% customers as per clv
//		}
//
//		$html_code = max($star_array);

		$star_rate=$this->getStartRating($customers_data_value->customer_ID, $email, $count);

		/*		 * **********RATING SECTION CODE ENDS HERE*************** */

		foreach ($explode_channel_accuired as $ch) :

		    $stores = Stores::find()->Where(['store_name' => $ch])->one();
		    if (!empty($stores)) {
			$img = $stores->store_image;
		    } else {

			if ($ch == "WeChat") {
			    $channels = Channels::find()->Where(['parent_name' => 'channel_' . $ch])->one();
			} else {
			    $channels = Channels::find()->Where(['channel_name' => $ch])->one();
			}
			if ($channels->parent_name == "channel_WeChat") {
			    $img = '/img/marketplace_logos/elliot-wechat-connection-icon.png';
			} elseif ($channels->parent_name == "channel_Square") {
			    $img = '/img/square_logo_grid.png';
			} elseif ($channels->parent_name == "channel_Lazada") {
			    $img = '/img/marketplace_logos/lazada-listing-logo.png';
			} else {
			    $img = $channels->channel_image;
			}
		    }

		endforeach;

		$user = Yii::$app->user->identity;
		$conversion_rate = 1;
		if (isset($user->currency)) {
		    
		    $conversion_rate = Stores::getDbConversionRate($user->currency);
		    $clv = $clv * $conversion_rate;
		    $clv = number_format((float) $clv, 2, '.', '');
		    
//		    $username = Yii::$app->params['xe_account_id'];
//		    $password = Yii::$app->params['xe_account_api_key'];
//		    $URL = Yii::$app->params['xe_base_url'] . 'convert_from.json/?from=USD&to=' . $user->currency . '&amount=1';
//
//		    $ch = curl_init();
//		    curl_setopt($ch, CURLOPT_URL, $URL);
//		    curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
//		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//		    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
//		    curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
//		    $result = curl_exec($ch);
//		    $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
//		    curl_close($ch);
//		    $result = json_decode($result, true);
//		    if (isset($result) and ! empty($result) and isset($result['to']) and isset($result['to'][0]) and isset($result['to'][0]['quotecurrency'])) {
//			$conversion_rate = $result['to'][0]['mid'];
//			$conversion_rate = number_format((float) $conversion_rate, 2, '.', '');
//			$clv = $clv * $conversion_rate;
//			$clv = number_format((float) $clv, 2, '.', '');
//		    }
		}

		$arr[0] = $customers_data_value->customer_ID;
		$arr[1] = '<a href="javascript:">' . $fname . ' ' . $lname . '</a>';
		$arr[2] = '<div class="col-sm-6"><div class="col-sm-4"><img class="ch_img" src="' . $img . '" width="50" height="50" alt="Channel Image"><lable class="magento-store-name">' . $magento_store_name . '</lable></div></div>';
		if ($exact_code != '') {
		    $arr[3] = $add_val . '!' . $exact_code;
		} else {
		    $arr[3] = '';
		}
		$arr[4] = $star_rate;
		$arr[5] = $clv;
		$arr[6] = $count_orders;
		$data_arr[] = $arr;
	    }
	    if ($sortbyvalue) {

		if ($orderby == 6):
		    if ($asc == 'asc'):
			usort($data_arr, function($a, $b) {
			    return $a[6] - $b[6];
			});
		    else:
			usort($data_arr, function($a, $b) {
			    return $b[6] - $a[6];
			});
		    endif;
		else:
		    if ($asc == 'asc'):
			usort($data_arr, function($a, $b) {
			    return $a[5] - $b[5];
			});
		    else:
			usort($data_arr, function($a, $b) {
			    return $b[5] - $a[5];
			});
		    endif;
		endif;

		if ($orderby == 3) {
		    if ($asc == 'asc') {
			usort($data_arr, function($a, $b) {
			    return strcmp(strtoupper($a[3]), strtoupper($b[3]));
			});
		    } else {
			usort($data_arr, function($a, $b) {
			    return strcmp(strtoupper($b[3]), strtoupper($a[3]));
			});
		    }
		}

		if ($orderby == 4) {
		    if ($asc == 'asc') {
			usort($data_arr, function($a, $b) {
			    return $a[4] - $b[4];
			});
		    } else {
			usort($data_arr, function($a, $b) {
			    return $b[4] - $a[4];
			});
		    }
		}

		$data_arr1 = array_values($data_arr);
	    } else {
		$data_arr1 = array_values($data_arr);
	    }

	    if (isset($data_arr1) and ! empty($data_arr1)) {
		$selected_currency = \backend\models\CurrencySymbols::find()->where(['name' => strtolower($user->currency)])->select(['id', 'symbol'])->asArray()->one();
		if (isset($selected_currency) and ! empty($selected_currency)) {
		    $currency_symbol = $selected_currency['symbol'];
		}
		$x = 1;
		foreach ($data_arr1 as $single) {
		    $single_array = [];
		    $single_array = $single;
		    $single_array[0] = '<div class="be-checkbox"><input name="people_check" id="ck' . $x . '" value="' . $single_array[0] . '" name="ck1" type="checkbox" data-parsley-multiple="groups" value="bar" data-parsley-mincheck="2" data-parsley-errors-container="#error-container1" class="people_row_check"><label class="getId" for="ck' . $x . '"></label></div>';
		    $code_coming = substr($single_array[3], strpos($single_array[3], "!") + 1);
		    $single_array[3] = substr($single_array[3], 0, strpos($single_array[3], "!"));
		    $single_array[3] = '<span class="flag-icon flag-icon-' . $code_coming . '"></span><span class="country_names"> ' . $single_array[3] . '</span>';
		    if ($single_array[4] == 1) {
			$single_array[4] = '<span class="mdi mdi-star yellow"></span><span class="mdi mdi-star black"></span><span class="mdi mdi-star black"></span><span class="mdi mdi-star black"></span><span class="mdi mdi-star black"></span>';
		    } elseif ($single_array[4] == 2) {
			$single_array[4] = '<span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span><span class="mdi mdi-star black"></span><span class="mdi mdi-star black"></span><span class="mdi mdi-star black"></span>';
		    } elseif ($single_array[4] == 3) {
			$single_array[4] = '<span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span><span class="mdi mdi-star black"></span><span class="mdi mdi-star black"></span>';
		    } elseif ($single_array[4] == 4) {
			$single_array[4] = '<span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span><span class="mdi mdi-star black"></span>';
		    } else {
			$single_array[4] = '<span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span>';
		    }
		    $single_array[5] = $currency_symbol . number_format($single_array[5], 2);

		    $final_array[] = $single_array;

		    $x++;
		}
	    }

	    $response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count, "data" => $final_array);
	    echo json_encode($response_arr);
	} else {

	    $response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count, "data" => array());
	    echo json_encode($response_arr);
	}
    }

    /* Starts Connected customer ajax */

    public function actionConnectedcustomerajax() {
	$user_id = Yii::$app->user->identity->id;
	$post = Yii::$app->request->get();
	$orders_data = $post['customers'];
	$store_id = '';

	$channel_name_explode = explode(" ", $orders_data);
	$channel_name_check = $channel_name_explode[0];
	$check_valid_store = Stores::find()->where(['store_name' => $channel_name_check])->one();
	if (!empty($check_valid_store)) {
	    $fin_channel = trim($channel_name_check);
	} else {
	    $fin_channel = $orders_data;
	}

	$start = $post['start'];
	$length = $post['length'];
	$draw = $post['draw'];
	$search = $post['search']['value'] ? $post['search']['value'] : '';
	$orderby = $post['order']['0']['column'] ? $post['order']['0']['column'] : '';
	$sortbyvalue = true;
	$orderby_str = 'first_name';
	if ($orderby == 1) {
	    $orderby_str = 'first_name';
	    $sortbyvalue = false;
	} elseif ($orderby == 3) {
	    $orderby_str = 'city';
	    $sortbyvalue = true;
	} elseif ($orderby == 4) {
	    $sortbyvalue = true;
	} elseif ($orderby == 5) {
	    $sortbyvalue = true;
	} else {
	    $orderby_str = 'first_name';
	    $sortbyvalue = true;
	}

	$asc = $post['order']['0']['dir'] ? $post['order']['0']['dir'] : '';

	/* Listing Query Store And channels */
	$check_valid_store = Stores::find()->where(['store_name' => $fin_channel])->one();
	if (!empty($check_valid_store)) {

	    $channel_space_pos = strpos($orders_data, ' ');
	    $country_sub_str = substr($orders_data, $channel_space_pos);
	    $store_country = trim($country_sub_str);
	    $store_details_data = StoreDetails::find()->Where(['channel_accquired' => $fin_channel, 'country' => $store_country])->with('storeConnection')->one();
	    if (!empty($store_details_data)) {
		$store_connection_id = $store_details_data->store_connection_id;
	    }

	    if ($orderby == 5) {
		$connection = \Yii::$app->db;
		$model = $connection->createCommand('SELECT `orderSum`.*,`Customer_User`.* FROM `Customer_User` Left JOIN 
		(SELECT `customer_id`,count(customer_id) as order_count, SUM(total_amount) as order_total FROM `orders` GROUP BY `customer_id`)
		`orderSum` ON orderSum.customer_id = Customer_User.customer_ID where Customer_User.channel_acquired="' . $fin_channel . '" ORDER BY order_total ' . $asc . ' limit ' . $start . ', ' . $length);
		$clv_data = $model->queryAll();
		$json = json_encode($clv_data);
		$customers_data = json_decode($json);
                
                $count = CustomerUser::find()
                    ->Where(['and', ['elliot_user_id' => $user_id], ['people_visible_status' => 'active']])
                    ->joinWith('customerabbrivation')
                    ->andWhere(['customer_abbrivation.mul_store_id' => $store_connection_id])
                    ->andWhere(['channel_acquired' => $fin_channel])
                    ->count();
                
		if ($search != '') {
		    $customers_data = CustomerUser::find()
                        ->Where(['and', ['elliot_user_id' => $user_id], ['people_visible_status' => 'active']])
                        ->limit($length)->offset($start)
                        ->joinWith('customerabbrivation')
                        ->andFilterWhere([
                            'or',
                                ['like', 'concat(first_name," ",last_name)', $search],
                                ['like', 'email', $search],
                                ['like', 'concat(city,", ",country)', $search],
                        ])
                        ->andWhere(['customer_abbrivation.mul_store_id' => $store_connection_id])
                        ->andWhere(['channel_acquired' => $fin_channel])
                        ->all();
		    $count = CustomerUser::find()
                        ->Where(['and', ['elliot_user_id' => $user_id], ['people_visible_status' => 'active']])
                        ->joinWith('customerabbrivation')
                        ->andWhere(['customer_abbrivation.mul_store_id' => $store_connection_id])
                        ->andWhere(['channel_acquired' => $fin_channel])
                        ->andFilterWhere([
                            'or',
                                ['like', 'concat(first_name," ",last_name)', $search],
                                ['like', 'email', $search],
                                ['like', 'concat(city,", ",country)', $search],
                        ])
                        ->count();
		}
	    } else {
		$customers_data = CustomerUser::find()->Where(['and', ['elliot_user_id' => $user_id], ['people_visible_status' => 'active']])
			->limit($length)->offset($start)
			->joinWith('customerabbrivation')
			->andFilterWhere([
			    'or',
				['like', 'concat(first_name," ",last_name)', $search],
				['like', 'email', $search],
				['like', 'concat(city,", ",country)', $search],
			])
			->andWhere(['channel_acquired' => $fin_channel])
			->andWhere(['customer_abbrivation.mul_store_id' => $store_connection_id])
			->orderBy($orderby_str . " " . $asc)
			->all();
                 $count = CustomerUser::find()->Where(['elliot_user_id' => $user_id, 'channel_acquired' => $fin_channel, 'people_visible_status' => 'active'])
                ->with(['customerabbrivation' => function($query) {
                        $query->andWhere(['mul_store_id' => $store_connection_id]);
                    }
                ])->count();
	    }
        }
        else {
	    if ($orderby == 5) {
		$connection = \Yii::$app->db;
		$model = $connection->createCommand('SELECT `orderSum`.*,`Customer_User`.* FROM `Customer_User` Left JOIN 
		(SELECT `customer_id`,count(customer_id) as order_count, SUM(total_amount) as order_total FROM `orders` GROUP BY `customer_id`)
		`orderSum` ON orderSum.customer_id = Customer_User.customer_ID where Customer_User.channel_acquired="' . $fin_channel . '" ORDER BY order_total ' . $asc . ' limit ' . $start . ', ' . $length);
		$clv_data = $model->queryAll();
		$json = json_encode($clv_data);
		$customers_data = json_decode($json);
                $count = CustomerUser::find()
                    ->Where(['and', ['elliot_user_id' => $user_id], ['people_visible_status' => 'active']])
                    ->andWhere(['channel_acquired' => $fin_channel])
                    ->count();
		if ($search != '') {
		    $customers_data = CustomerUser::find()
                        ->Where(['and', ['elliot_user_id' => $user_id], ['people_visible_status' => 'active']])
                        ->limit($length)->offset($start)
                        ->andWhere(['channel_acquired' => $fin_channel])
                        ->andFilterWhere([
                            'or',
                                ['like', 'concat(first_name," ",last_name)', $search],
                                ['like', 'email', $search],
                                ['like', 'concat(city,", ",country)', $search],
                        ])
                        ->all();
		    $count = CustomerUser::find()
                        ->Where(['and', ['elliot_user_id' => $user_id], ['people_visible_status' => 'active']])
                        ->andWhere(['channel_acquired' => $fin_channel])
                        ->andFilterWhere([
                            'or',
                                ['like', 'concat(first_name," ",last_name)', $search],
                                ['like', 'email', $search],
                                ['like', 'concat(city,", ",country)', $search],
                        ])
                        ->count();
		}
	    } else {
		$customers_data = CustomerUser::find()->Where(['and', ['elliot_user_id' => $user_id], ['people_visible_status' => 'active']])
		    ->limit($length)->offset($start)
		    ->andFilterWhere([
			'or',
			    ['like', 'concat(first_name," ",last_name)', $search],
			    ['like', 'email', $search],
			    ['like', 'concat(city,", ",country)', $search],
		    ])
		    ->andWhere(['channel_acquired' => $fin_channel])
		    ->orderBy($orderby_str . " " . $asc)
		    ->all();
                $count = CustomerUser::find()->Where(['elliot_user_id' => $user_id, 'channel_acquired' => $fin_channel, 'people_visible_status' => 'active'])->count();
	    }
	}

	$response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count);
	$customers_data_clv = CustomerUser::find()->Where(['elliot_user_id' => $user_id])->all();
	$clv_array = array();
	$ten_highest_clv = array();
	$cust_orderID = array();
	$orders_id_array = array();
	$order_IDS = array();
	$test = array();
	$stores_query = array();

	$total_customer_5_star = ceil($length / 100 * 10);
	
	if (!empty($customers_data)) :
	    $i = 1;
	    $customer_loop = 1;
	    foreach ($customers_data as $customers_data_value) :
		$customer_id = $customers_data_value->customer_ID;
		$channel_accuired = $customers_data_value->channel_acquired;
		$explode_channel_accuired = explode(",", $channel_accuired);

		$fname = isset($customers_data_value->first_name) ? $customers_data_value->first_name : "";
		$lname = isset($customers_data_value->last_name) ? $customers_data_value->last_name : "";
		$email = isset($customers_data_value->email) ? $customers_data_value->email : "";
		$city = isset($customers_data_value->city) ? $customers_data_value->city : "";
		$country = isset($customers_data_value->country) ? $customers_data_value->country : "";
		$ship_country = isset($customers_data_value->ship_country) ? $customers_data_value->ship_country : "";
		$country_codess = isset($customers_data_value->country_iso) ? $customers_data_value->country_iso : "";

		if (strtolower($country) == 'country') {
		    $country = '';
		}

		$magento_store_id = isset($customers_data_value->magento_store_id) ? $customers_data_value->magento_store_id : "";
		$magento_store_name = '';
		if ($magento_store_id != "") {
		    $magento_store_detail = MagentoStores::find()->where(['magento_store_id' => $magento_store_id])->one();
		    if (!empty($magento_store_detail)) {
			$magento_store_name = $magento_store_detail->magento_store_name;
		    }
		}

		$city_country = $city . ' ' . $country;
		$date_acquired = date('m-d-y', strtotime($customers_data_value->date_acquired));

		//Total Orders
		$orders_data = Orders::find()->Where(['customer_id' => $customers_data_value->customer_ID])->all();
		$count_orders = count($orders_data);

		//***Code for country flags start here***//

		$connection = \Yii::$app->db;
		$check_if_country_in_order_query = $connection->createCommand("SELECT ship_country FROM orders WHERE customer_id='" . $customers_data_value->customer_ID . "'");
		$check_if_country_in_order = $check_if_country_in_order_query->queryAll();
		$exact_code_from_order = @$check_if_country_in_order[0]['ship_country'];
		$exact_code_from_order = strtolower($exact_code_from_order);
		if ($country_codess != '') {

		    $exact_code = $country_codess;
		    $exact_code = strtolower($exact_code);
		} else {
		    if ($country == '' && $ship_country == '' && $exact_code_from_order == '') {
			$exact_code = '';
		    } elseif ($country == '' && $ship_country != '' && $exact_code_from_order == '') {
			if ($ship_country == 'USA') {
			    $ship_country = 'United States';
			}

			if (strlen($ship_country) == 2) {
			    $exact_code = $ship_country;
			    $exact_code = strtolower($exact_code);
			} else {
			    if ($ship_country == '') {
				$orders_customer = Orders::find()->Where(['customer_id' => $customers_data_value->customer_ID])->orderBy(['order_date' => SORT_DESC])->one();
				if (!empty($orders_customer)) :
				    $ship_country = $orders_customer->ship_country;
				endif;
			    }

			    $country_code_query = $connection->createCommand("SELECT sortname, name FROM countries WHERE name= '" . $ship_country . "'");
			    $country_code = $country_code_query->queryAll();
			    $exact_code = @$country_code[0]['sortname'];
			    $exact_code = strtolower($exact_code);
			}
		    }
		    elseif ($country == '' && $ship_country == '' && $exact_code_from_order != '') {

			if ($exact_code_from_order == 'usa') {
			    $exact_code = 'us';
			} else {
			    if ($ship_country == '') {
				$orders_customer = Orders::find()->Where(['customer_id' => $customers_data_value->customer_ID])->orderBy(['order_date' => SORT_DESC])->one();
				if (!empty($orders_customer)) :
				    $ship_country = $orders_customer->ship_country;
				endif;
			    }

			    $country_code_query = $connection->createCommand("SELECT sortname, name FROM countries WHERE name= '" . $ship_country . "'");
			    $country_code = $country_code_query->queryAll();
			    $exact_code = @$country_code[0]['sortname'];
			    $exact_code = strtolower($exact_code);
			}
		    } elseif ($country == '' && $ship_country != '' && $exact_code_from_order != '') {
			$exact_code = strtolower($exact_code_from_order);
		    } else {
			if ($country == 'USA') {
			    $country = 'United States';
			}

			if (strlen($country) == 2) {

			    $exact_code = $country;
			    $exact_code = strtolower($exact_code);
			} else {
			    if ($country == '') {
				$orders_customer = Orders::find()->Where(['customer_id' => $customers_data_value->customer_ID])->orderBy(['order_date' => SORT_DESC])->one();
				if (!empty($orders_customer)) :
				    $country = $orders_customer->ship_country;
				endif;
			    }

			    $country_code_query = $connection->createCommand("SELECT sortname, name FROM countries WHERE name= '" . $country . "'");
			    $country_code = $country_code_query->queryAll();
			    $exact_code = @$country_code[0]['sortname'];
			    $exact_code = strtolower($exact_code);
			}
		    }
		}
		//***Code for country flags ends here***//
		if (!empty($country)) {
		    if (!empty($city)) {
			$add_val = ucwords($city) . ',' . ' ' . ucwords($country);
		    } else {
			$add_val = ucwords($country);
		    }
		} else {
		    $orders_customer = Orders::find()->Where(['customer_id' => $customer_id])->orderBy(['order_date' => SORT_DESC])->one();
		    if (!empty($orders_customer)) {
			$ship_city = $orders_customer->ship_city;
			$ship_country = $orders_customer->ship_country;
			if (!empty($ship_city)) {

			    $add_val = $ship_city . ',' . ' ' . $ship_country;
			} else {
			    $add_val = $ship_country;
			}
		    } else {
			$add_val = "";
		    }
		}

		$connection = \Yii::$app->db;
		//$model1 = $connection->createCommand('SELECT DISTINCT parent_name stripe_Channel_id,channel_image,parent_name FROM channels');
		 if($orderby==5 && $search==''){
		    $clv = $customers_data_value->order_total;
		 }
		 else{
		    $order_sum_query = $connection->createCommand('SELECT SUM(total_amount), customer_id FROM orders WHERE customer_id=' . $customer_id . ' GROUP BY customer_id');
		    $order_sum = $order_sum_query->queryAll();
		    if (!empty($order_sum)) :
			$clv = number_format((float) @$order_sum[0]['SUM(total_amount)'], 2, '.', '');
		    else :
			$clv = '0.00';
		    endif;
		 } 


		/*		 * ******RATING SECTION CODE BEGINS HERE********* */
		$star_rate=$this->getStartRating($customers_data_value->customer_ID, $email, $count);
/*		 * *********RATING SECTION CODE ENDS HERE*************** */

		foreach ($explode_channel_accuired as $ch) :

		    $stores = Stores::find()->Where(['store_name' => $ch])->one();
		    if (!empty($stores)) {
			$img = $stores->store_image;
		    } else {

			if ($ch == "WeChat") {
			    $channels = Channels::find()->Where(['parent_name' => 'channel_' . $ch])->one();
			} else {
			    $channels = Channels::find()->Where(['channel_name' => $ch])->one();
			}
			if ($channels->parent_name == "channel_WeChat") {
			    $img = '/img/marketplace_logos/elliot-wechat-connection-icon.png';
			} elseif ($channels->parent_name == "channel_Square") {
			    $img = '/img/square_logo_grid.png';
			} elseif ($channels->parent_name == "channel_Lazada") {
			    $img = '/img/marketplace_logos/lazada-listing-logo.png';
			} else {
			    $img = $channels->channel_image;
			}
		    }

		endforeach;

		$user = Yii::$app->user->identity;
		$conversion_rate = 1;
		if (isset($user->currency)) {
		    
		    $conversion_rate = Stores::getDbConversionRate($user->currency);
		    $clv = $clv * $conversion_rate;
		    $clv = number_format((float) $clv, 2, '.', '');
		}

		$arr[0] = $customers_data_value->customer_ID;
		$arr[1] = '<a href="/people/view?id=' . $customers_data_value->customer_ID . '">' . $fname . ' ' . $lname . '</a>';
		$arr[2] = '<div class="col-sm-6"><div class="col-sm-4"><img class="ch_img" src="' . $img . '" width="50" height="50" alt="Channel Image"><lable class="magento-store-name">' . $magento_store_name . '</lable></div></div>';
		if ($exact_code != '') {
		    $arr[3] = $add_val . '!' . $exact_code;
		} else {
		    $arr[3] = '';
		}
		$arr[4] = $star_rate;
		$arr[5] = $clv;
		$arr[6] = $count_orders;
		$data_arr[] = $arr;
		$i++;
		$customer_loop++;
	    endforeach;
	    if ($sortbyvalue):
		if ($orderby == 6):
		    if ($asc == 'asc'):
			usort($data_arr, function($a, $b) {
			    return $a[6] - $b[6];
			});
		    else:
			usort($data_arr, function($a, $b) {
			    return $b[6] - $a[6];
			});
		    endif;
		else:
		    if ($asc == 'asc'):
			usort($data_arr, function($a, $b) {
			    return $a[5] - $b[5];
			});
		    else:
			usort($data_arr, function($a, $b) {
			    return $b[5] - $a[5];
			});
		    endif;
		endif;

		if ($orderby == 3) {
		    if ($asc == 'asc') {
			usort($data_arr, function($a, $b) {
			    return strcmp(strtoupper($a[3]), strtoupper($b[3]));
			});
		    } else {
			usort($data_arr, function($a, $b) {
			    return strcmp(strtoupper($b[3]), strtoupper($a[3]));
			});
		    }
		}

		if ($orderby == 4) {
		    if ($asc == 'asc') {
			usort($data_arr, function($a, $b) {
			    return $a[4] - $b[4];
			});
		    } else {
			usort($data_arr, function($a, $b) {
			    return $b[4] - $a[4];
			});
		    }
		}

		$data_arr1 = array_values($data_arr);
	    else:
		$data_arr1 = array_values($data_arr);
	    endif;
	    
	    if ($start == 0) {
		$customer_loop = 1;
		foreach ($data_arr1 as $key => $data_arr_val) {
		    if ($customer_loop <= $total_customer_5_star) {
			if ($data_arr_val[5] >= 5) {
			    $data_arr1[$key][4] = 5;
			}
		    }
		    $customer_loop++;
		}
	    }
	    
	    if (isset($data_arr1) and ! empty($data_arr1)) {
		$selected_currency = \backend\models\CurrencySymbols::find()->where(['name' => strtolower($user->currency)])->select(['id', 'symbol'])->asArray()->one();
		if (isset($selected_currency) and ! empty($selected_currency)) {
		    $currency_symbol = $selected_currency['symbol'];
		}
		$x = 1;
		foreach ($data_arr1 as $single) {
		    $single_array = [];
		    $single_array = $single;
		    $single_array[0] = '<div class="be-checkbox"><input name="people_check" id="ck' . $x . '" value="' . $single_array[0] . '" name="ck1" type="checkbox" data-parsley-multiple="groups" value="bar" data-parsley-mincheck="2" data-parsley-errors-container="#error-container1" class="people_row_check"><label class="getId" for="ck' . $x . '"></label></div>';
		    $code_coming = substr($single_array[3], strpos($single_array[3], "!") + 1);
		    $single_array[3] = substr($single_array[3], 0, strpos($single_array[3], "!"));
		    $single_array[3] = '<span class="flag-icon flag-icon-' . $code_coming . '"></span><span class="country_names"> ' . $single_array[3] . '</span>';
		    if ($single_array[4] == 1) {
			$single_array[4] = '<span class="mdi mdi-star yellow"></span><span class="mdi mdi-star black"></span><span class="mdi mdi-star black"></span><span class="mdi mdi-star black"></span><span class="mdi mdi-star black"></span>';
		    } elseif ($single_array[4] == 2) {
			$single_array[4] = '<span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span><span class="mdi mdi-star black"></span><span class="mdi mdi-star black"></span><span class="mdi mdi-star black"></span>';
		    } elseif ($single_array[4] == 3) {
			$single_array[4] = '<span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span><span class="mdi mdi-star black"></span><span class="mdi mdi-star black"></span>';
		    } elseif ($single_array[4] == 4) {
			$single_array[4] = '<span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span><span class="mdi mdi-star black"></span>';
		    } else {
			$single_array[4] = '<span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span><span class="mdi mdi-star yellow"></span>';
		    }
		    $single_array[5] = $currency_symbol . number_format($single_array[5], 2);

		    $final_array[] = $single_array;

		    $x++;
		}
	    }


	    $response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count, "data" => $final_array);
	    echo json_encode($response_arr);
	else:
	    $response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count, "data" => array());
	    echo json_encode($response_arr);

	endif;
    }

    /* End Connected customer ajax */

    public function actionProductsajax() {
	$post = Yii::$app->request->get();
	$start = $post['start'];
	$length = $post['length'];
	$draw = $post['draw'];
	$search = $post['search']['value'] ? $post['search']['value'] : '';
	$orderby = $post['order']['0']['column'] ? $post['order']['0']['column'] : '';
	$sortbyvalue = true;
	$orderby_str = 'product_name';
	if ($orderby == 1) {
	    $orderby_str = 'product_name';
	    $sortbyvalue = false;
	} elseif ($orderby == 2) {
	    $orderby_str = 'SKU';
	    $sortbyvalue = false;
	} elseif ($orderby == 4) {
	    $orderby_str = 'price';
	    $sortbyvalue = false;
	} elseif ($orderby == 5) {
	    $sortbyvalue = true;
	} else {
	    $orderby_str = 'product_name';
	    $sortbyvalue = true;
	}

	$asc = $post['order']['0']['dir'] ? $post['order']['0']['dir'] : '';

	$count = Products::find()->Where(['elliot_user_id' => Yii::$app->user->identity->id, 'product_status' => 'active', 'permanent_hidden' => 'active'])->count();

	$products = Products::find()
            ->limit($length)->offset($start)
            ->joinWith('productChannels')
            ->Where(['products.elliot_user_id' => Yii::$app->user->identity->id, 'products.product_status' => 'active', 'products.permanent_hidden' => 'active'])
            ->andWhere(['product_channel.status' => 'yes'])
            ->orderBy($orderby_str . " " . $asc)
            ->all();
        
        if($search!=''){
            $products = Products::find()
                ->limit($length)->offset($start)
                ->joinWith('productChannels')
                ->Where(['products.elliot_user_id' => Yii::$app->user->identity->id, 'products.product_status' => 'active', 'products.permanent_hidden' => 'active'])
                ->andWhere(['product_channel.status' => 'yes'])
                ->andFilterWhere([
                    'or',
                        ['like', 'product_name', $search],
                        ['like', 'SKU', $search],
                        ['like', 'price', $search],
                ])
                ->orderBy($orderby_str . " " . $asc)
                ->all();
            
            $count = Products::find()
                ->joinWith('productChannels')
                ->Where(['products.elliot_user_id' => Yii::$app->user->identity->id, 'products.product_status' => 'active', 'products.permanent_hidden' => 'active'])
                ->andWhere(['product_channel.status' => 'yes'])
                ->andFilterWhere([
                    'or',
                        ['like', 'product_name', $search],
                        ['like', 'SKU', $search],
                        ['like', 'price', $search],
                ])
                ->count();
        }

	$response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count);
	$arr = array();

	if (!empty($products)) :
	    $i = 1;
	    foreach ($products as $product) :
		//Magento store details 
		$magento_store_id = $product->magento_store_id ? $product->magento_store_id : '';
		$magento_store_name = '';
		if ($magento_store_id != "") {
		    $magento_store_ids = explode(',', $magento_store_id);
		    $magento_store_detail = MagentoStores::find()->where(['in', 'magento_store_id', $magento_store_ids])->all();
		    if (!empty($magento_store_detail)) {
			foreach ($magento_store_detail as $magento_detail) {
			    $magento_store_name .= $magento_detail->magento_store_name . ', ';
			}
		    }
		}
		$Mulstore_id = array();
		$Mulchannel_id = array();

		$get_product_base = ProductAbbrivation::find()->select(['mul_store_id', 'mul_channel_id', 'channel_accquired'])->where(['product_id' => $product->id])->all();
		foreach ($get_product_base as $val) {
		    $Mulstore_id[] = $val->mul_store_id;
		    $Mulchannel_id[] = $val->mul_channel_id;
		}
		$names = [];

		$productChannelImage = ProductChannel::getProductChannelImage($product->id);
		$channelImages = '';

		// append all images
		foreach ($productChannelImage as $key => $value):
		    $imageUrl = $value;
			$Mulstore_id=array_unique($Mulstore_id);
		    if (isset($Mulstore_id) and ! empty($Mulstore_id)) {
			//means the multiple connection is with store
			foreach ($Mulstore_id as $store) {

			    $check = StoreDetails::find()->where(['store_connection_id' => $store])->one();
			    if (!empty($check) and isset($check)) {
				//echo $check->channel_accquired;
				if (strtolower($key) == strtolower($check->channel_accquired)) {
				    $names[] = $check->channel_accquired . ' ' . $check->country_code;
				}
			    }
			}
		    } else {
			$names[] = strtoupper($key);
		    }
		    if ($key == 'ShopifyPlus') {
			$img_height = '13';
		    } else {
			$img_height = '50';
		    }
		    $tooltip = implode(', ', $names);
		    $channelImages = $channelImages . Html::img($imageUrl, ['alt' => 'Channel Image', 'width' => '50', 'height' => $img_height, 'class' => 'ch_img', 'data-toggle' => "tooltip", 'data-placement' => "right", 'title' => $tooltip, 'data-original-title' => '', 'test_sttr' => '']);
		    $names = array();
		endforeach;
		//Total Orders
		$productOrders = OrdersProducts::find()->Where(['product_Id' => $product->id])->all();
		$price1 = isset($product->price) ? $product->price : 0;
		$price = number_format((float) @$price1, 2, '.', ''); //number_format($price1, 2);
		$countOrders = count($productOrders);
		$total_refunds = 0;

		$refundedOrders = OrdersProducts::find()->Where(['product_Id' => $product->id])->with(['order' => function($query) {
				$query->andWhere(['order_status' => ['Refunded', 'Cancel']]);
			    }])->all();

		if (isset($refundedOrders) and ! empty($refundedOrders)) {

		    foreach ($refundedOrders as $single_order) {
			$total_refunds = count($single_order->order);
		    }
		}
		$name_pro = $product->product_name;
		// strlen($in) > 50 ? substr($in,0,50)."..." : $in;
		// if (strlen($name_pro) > 20) {
		// $name_pro = substr($name_pro, 0, 20) . "...";
		// } else {
		// $name_pro = $name_pro;
		// }
		$arr[0] = '<div class="be-checkbox"><input name="product_check" id="ck' . $i . '" value="' . $product->id . '" name="ck1" type="checkbox" data-parsley-multiple="groups" value="bar" data-parsley-mincheck="2" data-parsley-errors-container="#error-container1" class="product_row_check"><label class="getId" for="ck' . $i . '"></label></div>';

		$arr[1] = '<div class="product_listing_title"><a href="products/' . $product->id . '">' . $name_pro . '</a></div>';
		$arr[2] = '<div class="channel_scrolling">' . $channelImages . '<label class="magento-store-name">' . rtrim($magento_store_name, ', ') . '</label></div>';
		$arr[3] = $product->SKU;
		$arr[4] = '$' . $price;
		$user = Yii::$app->user->identity;
		$conversion_rate = 1;
		if (isset($user->currency)) {
		    $conversion_rate = Stores::getDbConversionRate($user->currency);
		    $price = $price * $conversion_rate;
		    $price = number_format((float) $price, 2, '.', '');
		    
//		    $username = Yii::$app->params['xe_account_id'];
//		    $password = Yii::$app->params['xe_account_api_key'];
//		    $URL = Yii::$app->params['xe_base_url'] . 'convert_from.json/?from=USD&to=' . $user->currency . '&amount=1';
//
//		    $ch = curl_init();
//		    curl_setopt($ch, CURLOPT_URL, $URL);
//		    curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
//		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//		    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
//		    curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
//		    $result = curl_exec($ch);
//		    $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
//		    curl_close($ch);
//		    $result = json_decode($result, true);
//		    if (isset($result) and ! empty($result) and isset($result['to']) and isset($result['to'][0]) and isset($result['to'][0]['quotecurrency'])) {
//			$conversion_rate = $result['to'][0]['mid'];
//			$conversion_rate = number_format((float) $conversion_rate, 2, '.', '');
//			$price = $price * $conversion_rate;
//			$price = number_format((float) $price, 2, '.', '');
//		    }
		}

		$selected_currency = \backend\models\CurrencySymbols::find()->where(['name' => strtolower($user->currency)])->select(['id', 'symbol'])->asArray()->one();
		if (isset($selected_currency) and ! empty($selected_currency)) {
		    $currency_symbol = $selected_currency['symbol'];
		}
		$arr[4] = $currency_symbol . $price;

		$arr[5] = $countOrders;
		$arr[6] = $total_refunds;
		$data_arr[] = $arr;
		$i++;
	    endforeach;
	    // echo "<pre>";
	    // print_r($data_arr);
	    // echo json_encode($data_arr);

	    if ($sortbyvalue):
		if ($asc == 'asc'):
		    usort($data_arr, function($a, $b) {
			return $a[5] - $b[5];
		    });
		else:
		    usort($data_arr, function($a, $b) {
			return $b[5] - $a[5];
		    });
		endif;

		$data_arr1 = array_values($data_arr);
	    else:
		$data_arr1 = array_values($data_arr);
	    endif;

	    // echo "aaaa";
	    // echo json_encode($data_arr1);
// echo "afterarray";

	    $response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count, "data" => $data_arr1);
	    echo json_encode($response_arr);

	else:
	    $response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count, "data" => array());
	    echo json_encode($response_arr);
// die('215645656');
	endif;
    }

    //code for inactive products listings

    public function actionInactiveProducts() {
	$post = Yii::$app->request->get();
	$current_store_name = $post['product'];
	$store_id = '';
	$channel_id = '';
	$channel_name_explode = explode(" ", $current_store_name);
	$channel_name_check = $channel_name_explode[0];
	$check_valid_store = Stores::find()->where(['store_name' => $channel_name_check])->one();
	if (!empty($check_valid_store)) {
	    $fin_channel = trim($channel_name_check);
	} else {
	    $fin_channel = $current_store_name;
	}

	//$order_type = array("BigCommerce", "Magento", "Shopify", "WooCommerce", "VTEX", "Magento2", "ShopifyPlus");
	$stores_name = $this->getAllStoreName();
	if (in_array($fin_channel, $stores_name)) {
	    $store_country_name = substr(strstr($current_store_name, " "), 1);
	    $store_detail = StoreDetails::find()->where(['channel_accquired' => $fin_channel, 'country' => $store_country_name])->one();
	    //$Stores_data = Stores::find()->Where(['store_name' => $fin_channel])->one();
	    $store_id = $store_detail->store_connection_id;

	    $store_table_column = 'product_channel.store_id';
	} else {
	    if ($fin_channel == "WeChat") {
		$get_big_id = Channels::find()->where(['parent_name' => 'channel_' . $fin_channel])->all();
		foreach ($get_big_id as $ids) {
		    $arr_id[] = $ids->channel_ID;
		}
		$channel_ids = implode(",", $arr_id);
		$channels_data = ChannelConnection::find()->where('channel_id IN(' . $channel_ids . ')')->one();
		$channel_id = $channels_data->channel_id;
	    } else {
		$channels_data = Channels::find()->Where(['channel_name' => $fin_channel])->one();
		$channel_id = $channels_data->channel_ID;
	    }
	    $store_table_column = 'product_channel.channel_id';
	}

	$start = $post['start'];
	$length = $post['length'];
	$draw = $post['draw'];
	$search = $post['search']['value'] ? $post['search']['value'] : '';
	$orderby = $post['order']['0']['column'] ? $post['order']['0']['column'] : '';
	$sortbyvalue = true;
	$orderby_str = 'product_name';
	if ($orderby == 1) {
	    $orderby_str = 'product_name';
	    $sortbyvalue = false;
	} elseif ($orderby == 3) {
	    $orderby_str = 'SKU';
	    $sortbyvalue = false;
	} elseif ($orderby == 4) {
	    $orderby_str = 'price';
	    $sortbyvalue = false;
	} elseif ($orderby == 5) {
	    $sortbyvalue = true;
	} else {
	    $orderby_str = 'product_name';
	    $sortbyvalue = true;
	}

	$asc = $post['order']['0']['dir'] ? $post['order']['0']['dir'] : '';
	$user_id = Yii::$app->user->identity->id;

	if (!empty($store_id)):
	    $products = Products::find()
		    ->Where(['products.elliot_user_id' => Yii::$app->user->identity->id])
		    ->joinWith('productChannels')
		    ->limit($length)->offset($start)
		    ->andFilterWhere([
			'or',
			    ['like', 'product_name', $search],
			    ['like', 'SKU', $search],
			    ['like', 'price', $search],
		    ])
		    ->andWhere(['product_channel.store_id' => $store_id])
		    ->andWhere(['products.product_status' => 'in_active'])
		    ->andWhere(['products.permanent_hidden' => 'active'])
		    ->andWhere(['product_channel.status' => 'no'])
		    ->orderBy($orderby_str . " " . $asc)
		    ->all();
	    $count = Products::find()
		    ->Where(['products.elliot_user_id' => Yii::$app->user->identity->id])
		    ->joinWith('productChannels')
		    ->andWhere(['product_channel.store_id' => $store_id])
		    ->andWhere(['products.product_status' => 'in_active'])
		    ->andWhere(['products.permanent_hidden' => 'active'])
		    ->andWhere(['product_channel.status' => 'no'])
		    ->count();

	else:

	    $products = Products::find()
		    ->Where(['products.elliot_user_id' => Yii::$app->user->identity->id])
		    ->joinWith('productChannels')
		    ->limit($length)->offset($start)
		    ->andFilterWhere([
			'or',
			    ['like', 'product_name', $search],
			    ['like', 'SKU', $search],
			    ['like', 'price', $search],
		    ])
		    ->andWhere(['product_channel.channel_id' => $channel_id])
		    ->andWhere(['products.product_status' => 'in_active'])
		    ->andWhere(['products.permanent_hidden' => 'active'])
		    ->andWhere(['product_channel.status' => 'no'])
		    ->orderBy($orderby_str . " " . $asc)
		    ->all();
	    $count = Products::find()
		    ->Where(['products.elliot_user_id' => Yii::$app->user->identity->id])
		    ->joinWith('productChannels')
		    ->andWhere(['product_channel.channel_id' => $channel_id])
		    ->andWhere(['products.product_status' => 'in_active'])
		    ->andWhere(['products.permanent_hidden' => 'active'])
		    ->andWhere(['product_channel.status' => 'no'])
		    ->count();

	endif;

	$response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count);
	if (!empty($products)) {
	    $i = 1;
	    foreach ($products as $product) {
		//Magento store details
		$magento_store_id = $product->magento_store_id ? $product->magento_store_id : '';
		$magento_store_name = '';
		if ($magento_store_id != "") {
		    $magento_store_ids = explode(',', $magento_store_id);
		    $magento_store_detail = MagentoStores::find()->where(['in', 'magento_store_id', $magento_store_ids])->all();
		    if (!empty($magento_store_detail)) {
			foreach ($magento_store_detail as $magento_detail) {
			    $magento_store_name .= $magento_detail->magento_store_name . ', ';
			}
		    }
		}

		$Mulstore_id = array();
		$Mulchannel_id = array();

		$get_product_base = ProductAbbrivation::find()->select(['mul_store_id', 'mul_channel_id', 'channel_accquired'])->where(['product_id' => $product->id])->all();
		foreach ($get_product_base as $val) {
		    $Mulstore_id[] = $val->mul_store_id;
		    $Mulchannel_id[] = $val->mul_channel_id;
		}
		$names = [];



		$productChannelImage = ProductChannel::getHiddenProductChannelImage($product->id);
		$channelImages = '';
		// append all images
		foreach ($productChannelImage as $key => $value):
		    $url_val = parse_url($value);
		    $imageUrl = '';
		    if (isset($url_val['scheme']) && ($url_val['scheme'] == 'https' || $url_val['scheme'] == 'http')) {
			$imageUrl = $value;
		    } else {
			$imageUrl = '/' . $value;
		    }

		    if (isset($Mulstore_id) and ! empty($Mulstore_id)) {
			//means the multiple connection is with store
			foreach ($Mulstore_id as $store) {

			    $check = StoreDetails::find()->where(['store_connection_id' => $store])->one();
			    if (!empty($check) and isset($check)) {
				//echo $check->channel_accquired;
				if (strtolower($key) == strtolower($check->channel_accquired)) {
				    $names[] = $check->channel_accquired . ' ' . $check->country_code;
				}
			    }
			}
		    } else {
			$names[] = strtoupper($key);
		    }

		    $tooltip = implode(', ', $names);
		    if ($key == 'ShopifyPlus') {
			$img_height = '13';
		    } else {
			$img_height = '50';
		    }
		    $channelImages = $channelImages . Html::img($imageUrl, ['alt' => 'Channel Image', 'width' => '50', 'height' => $img_height, 'class' => 'ch_img', 'data-toggle' => "tooltip", 'data-placement' => "top", 'title' => $tooltip, 'data-original-title' => '', 'test_sttr' => '']);
		    $names = array();
		endforeach;
		//Total Orders
		$productOrders = OrdersProducts::find()->Where(['product_Id' => $product->id])->all();
		$price1 = isset($product->price) ? $product->price : 0;
		$price = number_format((float) @$price1, 2, '.', ''); //number_format($price1, 2);
		$countOrders = count($productOrders);
		$total_refunds = 0;

		$refundedOrders = OrdersProducts::find()->Where(['product_Id' => $product->id])->with(['order' => function($query) {
				$query->andWhere(['order_status' => ['Refunded', 'Cancel']]);
			    }])->all();

		if (isset($refundedOrders) and ! empty($refundedOrders)) {

		    foreach ($refundedOrders as $single_order) {
			$total_refunds = count($single_order->order);
		    }
		}

		$user = Yii::$app->user->identity;
		$conversion_rate = 1;
		if (isset($user->currency)) {
		    $conversion_rate = Stores::getDbConversionRate($user->currency);
		    $price = $price * $conversion_rate;
		    $price = number_format((float) $price, 2, '.', '');
		    
//		    $username = Yii::$app->params['xe_account_id'];
//		    $password = Yii::$app->params['xe_account_api_key'];
//		    $URL = Yii::$app->params['xe_base_url'] . 'convert_from.json/?from=USD&to=' . $user->currency . '&amount=1';
//
//		    $ch = curl_init();
//		    curl_setopt($ch, CURLOPT_URL, $URL);
//		    curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
//		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//		    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
//		    curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
//		    $result = curl_exec($ch);
//		    $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
//		    curl_close($ch);
//		    //echo'<pre>';
//		    $result = json_decode($result, true);
//		    if (isset($result) and ! empty($result) and isset($result['to']) and isset($result['to'][0]) and isset($result['to'][0]['quotecurrency'])) {
//			$conversion_rate = $result['to'][0]['mid'];
//			$conversion_rate = number_format((float) $conversion_rate, 2, '.', '');
//			$price = $price * $conversion_rate;
//			$price = number_format((float) $price, 2, '.', '');
//		    }
		}

		$selected_currency = \backend\models\CurrencySymbols::find()->where(['name' => strtolower($user->currency)])->select(['id', 'symbol'])->asArray()->one();
		if (isset($selected_currency) and ! empty($selected_currency)) {
		    $currency_symbol = $selected_currency['symbol'];
		}

		$name_pro = $product->product_name;
		// strlen($in) > 50 ? substr($in,0,50)."..." : $in;
		//if (strlen($name_pro) > 20) {
		//    $name_pro = substr($name_pro, 0, 20) . "...";
		//} else {
		//    $name_pro = $name_pro;
		//}

		$arr[0] = '<div class="be-checkbox"><input name="inactive_product_check" id="ck' . $i . '" value="' . $product->id . '" name="ck1" type="checkbox" data-parsley-multiple="groups" value="bar" data-parsley-mincheck="2" data-parsley-errors-container="#error-container1" class="product_row_check"><label class="getId" for="ck' . $i . '"></label></div>';
		$arr[1] = '<a class="testing" href="javascript:">' . $name_pro . '</a>';
		$arr[2] = '<div class="channel_scrolling">' . $channelImages . '<label class="magento-store-name">' . rtrim($magento_store_name, ', ') . '</label></div>';
		$arr[3] = $product->SKU;
		$arr[4] = $currency_symbol . $price;
		$arr[5] = $countOrders;
		$arr[6] = $total_refunds;
		$data_arr[] = $arr;
		$i++;
	    }
	    if ($sortbyvalue):
		if ($asc == 'asc'):
		    usort($data_arr, function($a, $b) {
			return $a[5] - $b[5];
		    });
		else:
		    usort($data_arr, function($a, $b) {
			return $b[5] - $a[5];
		    });
		endif;

		$data_arr1 = array_values($data_arr);
	    else:
		$data_arr1 = array_values($data_arr);
	    endif;

	    $response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count, "data" => $data_arr1);
	    echo json_encode($response_arr);
	} else {
	    $response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count, "data" => array());
	    echo json_encode($response_arr);
	}
    }

    //code for inactive products listing page ends here.

    public function actionConnectedProducts() {
	$post = Yii::$app->request->get();
	$orders_data = $post['product'];
	$store_id = '';
	$channel_name_explode = explode(" ", $orders_data);
	$channel_name_check = $channel_name_explode[0];
	$check_valid_store = Stores::find()->where(['store_name' => $channel_name_check])->one();
	if (!empty($check_valid_store)) {
	    $fin_channel = trim($channel_name_check);
	} else {
	    $fin_channel = $orders_data;
	}

	$order_type = array("BigCommerce", "Magento", "Shopify", "WooCommerce", "VTEX", "Magento2", "ShopifyPlus");
	if (in_array($fin_channel, $order_type)) {
	    $channel_space_pos = strpos($orders_data, ' ');
	    $country_sub_str = substr($orders_data, $channel_space_pos);
	    $store_country = trim($country_sub_str);
	    $store_details_data = StoreDetails::find()->Where(['channel_accquired' => $fin_channel, 'country' => $store_country])->one();
	    $store_connection_id = $store_details_data->store_connection_id;
	    $Stores_data = Stores::find()->Where(['store_name' => $fin_channel])->one();
	    $store_id = $store_connection_id;
	    $store_table_column = 'product_channel.store_id';
	} else {
	    if ($fin_channel == "WeChat") {

		$get_big_id = Channels::find()->where(['parent_name' => 'channel_' . $fin_channel])->all();
		foreach ($get_big_id as $ids) {
		    $arr_id[] = $ids->channel_ID;
		}
		$channel_ids = implode(",", $arr_id);
		$channels_data = ChannelConnection::find()->where('channel_id IN(' . $channel_ids . ')')->one();
		$channel_id = $channels_data->channel_id;
	    } else {
		$channels_data = Channels::find()->Where(['channel_name' => $fin_channel])->one();
		$channel_id = $channels_data->channel_ID;
	    }
	    $store_table_column = 'product_channel.channel_id';
	}

	$start = $post['start'];
	$length = $post['length'];
	$draw = $post['draw'];
	$search = $post['search']['value'] ? $post['search']['value'] : '';
	$orderby = $post['order']['0']['column'] ? $post['order']['0']['column'] : '';
	$sortbyvalue = true;
	$orderby_str = 'product_name';
	if ($orderby == 1) {
	    $orderby_str = 'product_name';
	    $sortbyvalue = false;
	} elseif ($orderby == 2) {
	    $orderby_str = 'SKU';
	    $sortbyvalue = false;
	} elseif ($orderby == 4) {
	    $orderby_str = 'price';
	    $sortbyvalue = false;
	} elseif ($orderby == 5) {
	    $sortbyvalue = true;
	} else {
	    $orderby_str = 'product_name';
	    $sortbyvalue = true;
	}

	$asc = $post['order']['0']['dir'] ? $post['order']['0']['dir'] : '';
	$user_id = Yii::$app->user->identity->id;
	// $count = Products::find()->Where(['elliot_user_id' =>$user_id ])->andWhere(['products.product_status' => 'active'])->count();
	if (!empty($store_id)){
            $products = Products::find()
                ->Where(['products.elliot_user_id' => $user_id])
                ->joinWith('productChannels')
                ->limit($length)->offset($start)
                ->andFilterWhere([
                    'or',
                        ['like', 'product_name', $search],
                        ['like', 'SKU', $search],
                        ['like', 'price', $search],
                ])
                ->andWhere(['product_channel.store_id' => $store_id])
                ->andWhere(['products.product_status' => 'active'])
                ->andWhere(['products.permanent_hidden' => 'active'])
                ->andWhere(['product_channel.status' => 'yes'])
                ->orderBy($orderby_str . " " . $asc)
                ->all();

	    $count = Products::find()
                ->Where(['products.elliot_user_id' => $user_id])
                ->joinWith('productChannels')
                ->andWhere(['product_channel.store_id' => $store_id])
                ->andWhere(['products.product_status' => 'active'])
                ->andWhere(['products.permanent_hidden' => 'active'])
                ->andWhere(['product_channel.status' => 'yes'])
                ->count();
            
            if($search!=''){
                $count = Products::find()
                    ->Where(['products.elliot_user_id' => $user_id])
                    ->joinWith('productChannels')
                    ->andFilterWhere([
                        'or',
                            ['like', 'product_name', $search],
                            ['like', 'SKU', $search],
                            ['like', 'price', $search],
                    ])
                    ->andWhere(['product_channel.store_id' => $store_id])
                    ->andWhere(['products.product_status' => 'active'])
                    ->andWhere(['products.permanent_hidden' => 'active'])
                    ->andWhere(['product_channel.status' => 'yes'])
                    ->count();
            }

        }
        else{
            $products = Products::find()
                ->Where(['products.elliot_user_id' => Yii::$app->user->identity->id])
                ->joinWith('productChannels')
                ->limit($length)->offset($start)
                ->andFilterWhere([
                    'or',
                        ['like', 'product_name', $search],
                        ['like', 'SKU', $search],
                        ['like', 'price', $search],
                ])
                ->andWhere(['product_channel.channel_id' => $channel_id])
                ->andWhere(['products.product_status' => 'active'])
                ->andWhere(['products.permanent_hidden' => 'active'])
                ->andWhere(['product_channel.status' => 'yes'])
                ->orderBy($orderby_str . " " . $asc)
                ->all();

	    $count = Products::find()
                ->Where(['products.elliot_user_id' => Yii::$app->user->identity->id])
                ->joinWith('productChannels')
                ->andWhere(['product_channel.channel_id' => $channel_id])
                ->andWhere(['products.product_status' => 'active'])
                ->andWhere(['products.permanent_hidden' => 'active'])
                ->andWhere(['product_channel.status' => 'yes'])
                ->count();
            
            if($search!=''){
                $count = Products::find()
                    ->Where(['products.elliot_user_id' => Yii::$app->user->identity->id])
                    ->joinWith('productChannels')
                    ->andFilterWhere([
                        'or',
                            ['like', 'product_name', $search],
                            ['like', 'SKU', $search],
                            ['like', 'price', $search],
                    ])
                    ->andWhere(['product_channel.channel_id' => $channel_id])
                    ->andWhere(['products.product_status' => 'active'])
                    ->andWhere(['products.permanent_hidden' => 'active'])
                    ->andWhere(['product_channel.status' => 'yes'])
                    ->count();
            }
            
            

        }

	$response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count);

	if (!empty($products)) :
	    $i = 1;
	    foreach ($products as $product) :
		//Magento store details
		$magento_store_id = $product->magento_store_id ? $product->magento_store_id : '';
		$magento_store_name = '';
		if ($magento_store_id != "") {
		    $magento_store_ids = explode(',', $magento_store_id);
		    $magento_store_detail = MagentoStores::find()->where(['in', 'magento_store_id', $magento_store_ids])->all();
		    if (!empty($magento_store_detail)) {
			foreach ($magento_store_detail as $magento_detail) {
			    $magento_store_name .= $magento_detail->magento_store_name . ', ';
			}
		    }
		}
		$Mulstore_id = array();
		$Mulchannel_id = array();

		$get_product_base = ProductAbbrivation::find()->select(['mul_store_id', 'mul_channel_id', 'channel_accquired'])->where(['product_id' => $product->id])->all();
		foreach ($get_product_base as $val) {
		    $Mulstore_id[] = $val->mul_store_id;
		    $Mulchannel_id[] = $val->mul_channel_id;
		}
		$names = [];


		$productChannelImage = ProductChannel::getProductChannelImage($product->id);
		$channelImages = '';
		// append all images
		foreach ($productChannelImage as $key => $value):
		    $url_val = parse_url($value);
		    $imageUrl = '';
		    if (isset($url_val['scheme']) && ($url_val['scheme'] == 'https' || $url_val['scheme'] == 'http')) {
			$imageUrl = $value;
		    } else {
			$imageUrl = '/' . $value;
		    }

		    if (isset($Mulstore_id) and ! empty($Mulstore_id)) {
			//means the multiple connection is with store
			foreach ($Mulstore_id as $store) {

			    $check = StoreDetails::find()->where(['store_connection_id' => $store])->one();
			    if (!empty($check) and isset($check)) {
				//echo $check->channel_accquired;
				if (strtolower($key) == strtolower($check->channel_accquired)) {
				    $names[] = $check->channel_accquired . ' ' . $check->country_code;
				}
			    }
			}
		    } else {
			$names[] = strtoupper($key);
		    }

		    $tooltip = implode(', ', $names);

//                    $channelImages = $channelImages . Html::img($imageUrl, ['alt' => 'Channel Image', 'width' => '50', 'height' => '50', 'class' => 'ch_img']);
		    if ($key == 'ShopifyPlus') {
			$img_height = '13';
		    } else {
			$img_height = '50';
		    }

		    $channelImages = $channelImages . Html::img($imageUrl, ['alt' => 'Channel Image', 'width' => '50', 'height' => $img_height, 'class' => 'ch_img', 'data-toggle' => "tooltip", 'data-placement' => "top", 'title' => $tooltip, 'data-original-title' => '']);
		    $names = array();
		endforeach;
		//Total Orders
		$productOrders = OrdersProducts::find()->Where(['product_Id' => $product->id])->all();
		$price1 = isset($product->price) ? $product->price : 0;
		$price = number_format((float) @$price1, 2, '.', ''); //number_format($price1, 2);
		$countOrders = count($productOrders);
		$total_refunds = 0;

		$refundedOrders = OrdersProducts::find()->Where(['product_Id' => $product->id])->with(['order' => function($query) {
				$query->andWhere(['order_status' => ['Refunded', 'Cancel']]);
			    }])->all();

		if (isset($refundedOrders) and ! empty($refundedOrders)) {

		    foreach ($refundedOrders as $single_order) {
			$total_refunds = count($single_order->order);
		    }
		}

		$user = Yii::$app->user->identity;
		$conversion_rate = 1;
		if (isset($user->currency)) {
		    $conversion_rate = Stores::getDbConversionRate($user->currency);
		    $price = $price * $conversion_rate;
		    $price = number_format((float) $price, 2, '.', '');
		    
//		    $username = Yii::$app->params['xe_account_id'];
//		    $password = Yii::$app->params['xe_account_api_key'];
//		    $URL = Yii::$app->params['xe_base_url'] . 'convert_from.json/?from=USD&to=' . $user->currency . '&amount=1';
//
//		    $ch = curl_init();
//		    curl_setopt($ch, CURLOPT_URL, $URL);
//		    curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
//		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//		    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
//		    curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
//		    $result = curl_exec($ch);
//		    $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
//		    curl_close($ch);
//		    //echo'<pre>';
//		    $result = json_decode($result, true);
//		    if (isset($result) and ! empty($result) and isset($result['to']) and isset($result['to'][0]) and isset($result['to'][0]['quotecurrency'])) {
//			$conversion_rate = $result['to'][0]['mid'];
//			$conversion_rate = number_format((float) $conversion_rate, 2, '.', '');
//			$price = $price * $conversion_rate;
//			$price = number_format((float) $price, 2, '.', '');
//		    }
		}

		$selected_currency = \backend\models\CurrencySymbols::find()->where(['name' => strtolower($user->currency)])->select(['id', 'symbol'])->asArray()->one();
		if (isset($selected_currency) and ! empty($selected_currency)) {
		    $currency_symbol = $selected_currency['symbol'];
		}

		$name_pro = $product->product_name;
		// strlen($in) > 50 ? substr($in,0,50)."..." : $in;
		//if (strlen($name_pro) > 20) {
		//    $name_pro = substr($name_pro, 0, 20) . "...";
		//} else {
		//    $name_pro = $name_pro;
		//}

		$arr[0] = '<div class="be-checkbox"><input name="inactive_product_check" id="ck' . $i . '" value="' . $product->id . '" name="ck1" type="checkbox" data-parsley-multiple="groups" value="bar" data-parsley-mincheck="2" data-parsley-errors-container="#error-container1" class="product_row_check"><label class="getId" for="ck' . $i . '"></label></div>';
		$arr[1] = '<div class="product_listing_title"><a class="testing" href="/products/' . $product->id . '">' . $name_pro . '</a></div>';
		//$arr[2] = $channelImages . '<label class="magento-store-name">' . rtrim($magento_store_name, ', ') . '</label>';

		$arr[2] = '<div class="channel_scrolling">' . $channelImages . '<label class="magento-store-name">' . rtrim($magento_store_name, ', ') . '</label></div>';
		$arr[3] = $product->SKU;
		$arr[4] = $currency_symbol . $price;
		$arr[5] = $countOrders;
		$arr[6] = $total_refunds;
		$data_arr[] = $arr;



		$i++;
	    endforeach;

	    if ($sortbyvalue):
		if ($asc == 'asc'):
		    usort($data_arr, function($a, $b) {
			return $a[5] - $b[5];
		    });
		else:
		    usort($data_arr, function($a, $b) {
			return $b[5] - $a[5];
		    });
		endif;

		$data_arr1 = array_values($data_arr);
	    else:
		$data_arr1 = array_values($data_arr);
	    endif;

	    $response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count, "data" => $data_arr);
	    echo json_encode($response_arr);
	else:
	    $response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count, "data" => array());
	    echo json_encode($response_arr);

	endif;
    }

    public function actionFacebook() {

	$dt = Yii::$app->request->post();
	if (!empty($dt)) {

	    $storename = $dt['storename'];
	    $password = $dt['password'];
	    $wechat_type = isset($dt['wechat_type']) ? $dt['wechat_type'] : '';
	    $email = Yii::$app->user->identity->email;
	    $company_name = ucfirst(Yii::$app->user->identity->company_name);
	    $email2 = 'helloiamelliot@studio86.co';
	    $ml1 = Yii::$app->mailer->compose()
		    ->setFrom('elliot@helloiamelliot.com')
		    ->setTo($email)
		    ->attach('img/email_docs/Aus_NZ_Dinpay_intro_email_v.pdf')
		    ->attach('img/email_docs/Dinpay_Cross_border_Ecommerce_Application_Form_for_Merchant.pdf')
		    ->attach('img/email_docs/Dinpay_Merchant_Agreement_cross_border_ecommerce.docx')
		    ->setSubject('Next steps to complete your Elliot/WeChat integration')
		    ->setHtmlBody(
			    '<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="background-color:#fcfcfc;padding: 60px 10px">
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="1" cellspacing="0" width="600" id="emailContainer" style="background-color:#fff;padding:25px;-webkit-box-shadow: 0px 5px 30px 0px rgba(50, 50, 50, 0.2);-moz-box-shadow:0px 5px 30px 0px rgba(50, 50, 50, 0.2);box-shadow:0px 5px 30px 0px rgba(50, 50, 50, 0.2);">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailHeader">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <center><img src="http://beta.brstdev.com/adrianna/elliot-logo.jpg" alt="Logo13" title="Logo" style="display:block; width: 100px;"/></center>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellpadding="1" cellspacing="0" width="100%" id="emailBody">
                                                <tr>
                                                    <td valign="top">
                                                        <p style="color: #000000;text-align: left;font-family: OpenSans;font-size: 14.0px;font-style: normal;font-stretch: normal;font-weight: 400;">
                                                            ' . $company_name . ',
                                                       </p>                                                    
                                                       </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                        <p style="color: #000000;text-align: left;font-family: OpenSans;font-size: 14.0px;font-style: normal;font-stretch: normal;font-weight: 400;line-height:25px;">
                                                            Thank you for subscribing to WeChat Stores via Elliot. An Elliot representative will be in touch with you within 24 hours to complete your on-boarding. In the mean time, please view and 
                                                            complete the attached documents. *Attach the Dinpay agreements attached in this email
                                                        </p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellpadding="1" cellspacing="0" width="100%" id="emailFooter">
                                                    <tr>
                                                        <tdvalign="top">
                                                           <p>Thank you,<br>
                                                            <b>Elliot</b></p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>'
		    )
		    ->send();

	    $ml2 = Yii::$app->mailer->compose()
		    ->setFrom('elliot@helloiamelliot.com')
		    ->setTo($email2)
		    ->setSubject('Request For Elliot/WeChat integration')
		    ->setHtmlBody(
			    '<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="background-color:#fcfcfc;padding: 60px 10px">
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="1" cellspacing="0" width="600" id="emailContainer" style="background-color:#fff;padding:25px;-webkit-box-shadow: 0px 5px 30px 0px rgba(50, 50, 50, 0.2);-moz-box-shadow:0px 5px 30px 0px rgba(50, 50, 50, 0.2);box-shadow:0px 5px 30px 0px rgba(50, 50, 50, 0.2);">
                                        <tr>
                                            <td align="center" valign="top">
                                                <table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailHeader">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <center><img src="http://beta.brstdev.com/adrianna/elliot-logo.jpg" alt="Logo13" title="Logo" style="display:block; width: 100px;"/></center>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellpadding="1" cellspacing="0" width="100%" id="emailBody">
                                                <tr>
                                                    <td valign="top">
                                                        <p style="color: #000000;text-align: left;font-family: OpenSans;font-size: 14.0px;font-style: normal;font-stretch: normal;font-weight: 400;">
                                                            ' . $company_name . ',
                                                       </p>                                                    
                                                       </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top">
                                                        <p style="color: #000000;text-align: left;font-family: OpenSans;font-size: 14.0px;font-style: normal;font-stretch: normal;font-weight: 400;line-height:25px;">
                                                            <p>There is a new request to create store on walkthechat.com from user ' . $email . ' . Following are the details</p>Suggested Store Name: ' . $storename . ' <br>Suggested Password: ' . $password . ' <br>Store Type: ' . $wechat_type . '"
                                                        </p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <table border="0" cellpadding="1" cellspacing="0" width="100%" id="emailFooter">
                                                    <tr>
                                                        <tdvalign="top">
                                                           <p>Thank you,<br>
                                                            <b>Elliot</b></p>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>'
		    )
		    ->send();

	    if ($ml1) {
		$store_type = Channels::find()->where(['channel_name' => $wechat_type])->one();
		$channel_connection = new ChannelConnection();
		$channel_connection->user_id = Yii::$app->user->identity->id;
		$channel_connection->channel_id = $store_type->channel_ID;
		$channel_connection->save();

		echo true;
		die;
	    } else {
		echo false;
		die;
	    }
	}
	return $this->render('facebook');
    }

    /* Use For Percentage */

//    function get_percentage($percentage, $of) {
//        //courtesy : http://stackoverflow.com/a/14880194
//        $percent = $percentage / $of;
//        return number_format($percent * 100, 2) . '%';
//    }

    public static function actionTestCallss() {



	$a = Yii::$app->mailer->compose()
		//->getSwiftMessage()->getHeaders()->addTextHeader('name', 'value')
		->setFrom('abc@mail.com')
		->setTo('shubham.sasgohar@brihaspatitech.com')
		->setSubject(' Welcome to Elliot, Your Admin Panel is Ready for you to Start')
		->setTextBody('Plain text content')
		->send();


	print_r($a);
	die;


	$connection = \Yii::$app->db;

	$model1 = $connection->createCommand('SELECT COUNT(parent_name), parent_name FROM channels GROUP BY parent_name');
	$channels = $model1->queryAll();
	$channels_data_value = array();

	foreach ($channels as $ch) {
	    $parent_name = $ch['parent_name'];
	    $channels_data_value[] = Channels::find()->Where(['parent_name' => $parent_name])->all();
	}
	// echo '<pre>';
	// print_r($channels_data_value);



	$orders_data_recent = Orders::find()->Where(['elliot_user_id' => Yii::$app->user->identity->id])->with('customer')->orderBy(['order_ID' => SORT_DESC])->limit(5)->all();

	echo '<pre>';
	echo 'dfgdfgd--------------------------------';



	usort($orders_data_recent, function($a, $b) {
	    $t1 = strtotime($a->order_date);
	    $t2 = strtotime($b->order_date);
	    return $t2 - $t1;
	});

	print_r($orders_data_recent);
    }

    public function getCustomerStoreImageLogo($customer_id) {
	$checkcustomer = CustomerAbbrivation::find()->Where(['customer_id' => $customer_id])->all();
	$image_data = '';
	$check = 0;
	if (!empty($checkcustomer)) {
	    $created_date_array = array();
	    foreach ($checkcustomer as $_customer) {
		$channel_name = $_customer->channel_accquired;
		if (count($checkcustomer) == 1) {
		    $image_data = $this->getStoreChannelImageByName($channel_name);
		    return $image_data;
		} else {
		    $check = 1;
		    $store_data = Stores::find()->where(['store_name' => $channel_name])->one();
		    if (!empty($store_data)) {
			$store_id = $store_data->store_id;
			$store_connection = StoresConnection::find()->where(['store_id' => $store_id])->one();
			if (!empty($store_connection)) {
			    $created_date_array[$channel_name] = strtotime($store_connection->created);
			}
		    } else {
			$channel_data = Channels::find()->where(['channel_name' => $channel_name])->one();
			if (!empty($channel_data)) {
			    $channel_data = $channel_data->channel_ID;
			    $channel_connection = ChannelConnection::find()->where(['channel_id' => $channel_data])->one();
			    if (!empty($channel_connection)) {
				$created_date_array[$channel_name] = strtotime($channel_connection->created);
			    }
			}
		    }
		}
	    }
	    if ($check == 1) {
		asort($created_date_array);
		if (count($created_date_array) > 0) {
		    $newArray = array_keys($created_date_array);
		    $_channel_name = $newArray[0];
		    $image_data = $this->getStoreChannelImageByName($_channel_name);
		    return $image_data;
		}
	    }
	}
    }

    public static function getStoreChannelImageByName($channel_name) {
        $image_data = '';
        if ($channel_name == 'WeChat') {
            $image_data = '<img class="ch_img" src="/img/marketplace_logos/elliot-wechat-connection-icon.png" width="50" height="50" alt="Channel Image">';
        }
        if ($channel_name == 'Square') {
            $image_data = '<img class="ch_img" src="/img/square_logo_grid.png" width="50" height="50" alt="Channel Image">';
        }
        if ($channel_name == 'BigCommerce') {
            $image_data = '<img class="ch_img" src="/img/bigcommerce.png" width="50" height="50" alt="Channel Image">';
        }
        if ($channel_name == 'Shopify') {
            $image_data = '<img class="ch_img" src="/img/shopify.png" width="50" height="50" alt="Channel Image">';
        }
        if ($channel_name == 'ShopifyPlus') {
            $image_data = '<img class="ch_img" src="/img/shopifyplus-listing.png" width="50" height="13" alt="Channel Image">';
        }
        if (strstr($channel_name, 'Lazada')) {
            $image_data = '<img class="ch_img" src="/img/marketplace_logos/lazada-listing-logo.png" width="50" height="50" alt="Channel Image">';
        }
        if ($channel_name == 'Magento') {
            $image_data = '<img class="ch_img" src="/img/magento.png" width="50" height="50" alt="Channel Image">';
        }
        if ($channel_name == 'Magento2') {
            $image_data = '<img class="ch_img" src="/img/magento2.png" width="50" height="50" alt="Channel Image">';
        }
        if ($channel_name == 'TMall') {
            $image_data = '<img class="ch_img" src="/img/marketplace_logos/tmall-icon-logo.png" width="50" height="50" alt="Channel Image">';
        }
        if ($channel_name == 'Jet') {
            $image_data = '<img class="ch_img" src="/img/marketplace_logos/elliot-jet-connection.png" width="50" height="50" alt="Channel Image">';
        }
        if ($channel_name == 'WooCommerce') {
            $image_data = '<img class="ch_img" src="/img/woocommerce.png" width="50" height="50" alt="Channel Image">';
        }
        if ($channel_name == 'VTEX') {
            $image_data = '<img class="ch_img" src="/img/vtex.png" width="50" height="50" alt="Channel Image">';
        }
        return $image_data;
    }

    public function actionTest() {
	$connection = \Yii::$app->db;
	$model = $connection->createCommand('SELECT `orderSum`.* FROM `Customer_User` Left JOIN 
			    (SELECT `customer_id`, SUM(total_amount) as order_total FROM `orders` GROUP BY `customer_id`)
			    `orderSum` ON orderSum.customer_id = Customer_User.customer_ID ORDER BY order_total DESC limit 0, 270');
	$clv_data = $model->queryAll();
	echo "<pre>"; print_r($clv_data); die('End here!');
	
//	$data = CustomerUser::find()->with('orders')->limit(10)->all();
////        $data=CustomerUser::find()
////                ->with(['orders' => function($query)
////                    {
////                        $query->andWhere->sum('total_amount');
////                       
////                    } 
////            ])->limit(10)->all();
//	echo'<pre>';
//	print_r($data);
//	die('end here');

//        $storename = 'WeChat';
//        $file = 'WeChat_';
//        Integrations::Writefile($storename, $file);
    }

    public function actionTesting() {

	$count = Products::find()->Where(['elliot_user_id' => Yii::$app->user->identity->id, 'product_status' => 'active', 'permanent_hidden' => 'active'])->count();

	echo '<pre>';
	print_r($count);
	echo '--';

	$products = Products::find()->Where(['products.elliot_user_id' => Yii::$app->user->identity->id])
		->joinWith('productChannels')
		->andWhere(['products.permanent_hidden' => 'active'])
		->andWhere(['products.product_status' => 'active'])
		->andWhere(['product_channel.status' => 'yes'])
		->all();

	echo count($products);
    }

    public function getWechatChannelInfo($param) {
	
    }

    public function getAllStoreName() {
	$stores_names = Stores::find()->select('store_name')->asArray()->all();
	$store_array = array();
	foreach ($stores_names as $_store) {
	    $store_array[] = $_store['store_name'];
	}
	return $store_array;
    }
    
    public function getStartRating($customer_id,$customer_email,$total_cus){
	$connection = \Yii::$app->db;
	$star_array = array();
	/*For One Star By default*/
	$star_count = 1;
	
	if($customer_email!=''){
	    $star_count=2;/*For One Star By default*/
	}
	$order_count=Orders::find()->Where(['customer_id'=>$customer_id])->count();
	if($order_count>=1 && $order_count<=10){
	    $star_count=3;
	}
	$order_distinct=Orders::find()->select('channel_accquired')->Where(['customer_id'=>$customer_id])->distinct()->count();
	if($order_distinct>1 || $order_count>10){
	    $star_count=4;
	}
	
	$total_customer_5_star = ceil($total_cus / 100 * 10);
	$model = $connection->createCommand('SELECT `orderSum`.* FROM `Customer_User` Left JOIN 
			    (SELECT `customer_id`, SUM(total_amount) as order_total FROM `orders` GROUP BY `customer_id`)
			    `orderSum` ON orderSum.customer_id = Customer_User.customer_ID ORDER BY order_total DESC limit 0, '.$total_customer_5_star);
	$clv_data = $model->queryAll();
	foreach($clv_data as $_clv_data){
	    if($_clv_data['customer_id']==$customer_id){
		$order_total = $_clv_data['order_total'];
		if($order_total>=5){
		    $star_count=5;
		}
		break;
	    }
	}
	return $star_count;
    }

}
