<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use backend\models\User;
use backend\models\StoresConnection;
use backend\models\Stores;
use backend\models\Categories;
use backend\models\Variations;
use backend\models\VariationsItemList;
use backend\models\Products;
use backend\models\OrdersProducts;
use backend\models\OrderChannel;
use backend\models\CustomerUser;
use backend\models\Notification;
use backend\models\ProductCategories;
use backend\models\ProductVariation;
use backend\models\ProductImages;
use backend\models\ProductChannel;
use backend\models\Orders;
use backend\models\Channels;
use common\models\CustomFunction;
use backend\models\ChannelConnection;
use backend\models\Integrations;
use backend\models\ProductAbbrivation;
use backend\models\CategoryAbbrivation;
use backend\models\CronTasks;
use backend\models\StoreDetails;
use backend\models\Countries;
use backend\models\Channelsetting;


class SquareController extends \yii\web\Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'test', 'square-auth', 'square-importing', 'square-webhook-response', 'square-cron-importing'],
                'rules' => [
                    [
                        /* action hit without log-in */
                        'actions' => ['login', 'test', 'square-auth', 'square-importing', 'square-webhook-response', 'square-cron-importing'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        /* action hit only with log-in */
                        'actions' => ['index', 'test', 'square-auth', 'square-importing', 'square-webhook-response', 'square-cron-importing'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex() {
        return $this->render('index');
    }

    /**
     * Square Auth Connection
     */
    public function actionSquareAuth() {
        //echo "<pre>"; print_r($_POST); die('End here');
        $square_application_id = $_POST['square_application_id'];
        $square_personal_access_token = $_POST['square_personal_access_token'];
        $user_id = $_POST['user_id'];
        $user = User::find()->where(['id' => $user_id])->one();
        $user_domain = $user->domain_name;
        $config = yii\helpers\ArrayHelper::merge(
            require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'),
            require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $user_domain . '/main-local.php'),
            require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'),
            require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);
        $channel_data = Channels::find()->where(['channel_name' => 'Square'])->one();
        $square_channel_id = $channel_data->channel_ID;
        $square_channel_data = ChannelConnection::find()->where(['elliot_user_id' => $user_id, 'channel_id' => $square_channel_id])->one();
        $array_msg = array();
        if(empty($square_channel_data))
        {
            $channel_model = new ChannelConnection();
            $channel_model->elliot_user_id = $user_id;
            $channel_model->channel_id = $square_channel_id;
            $channel_model->user_id = $user_id;
            $channel_model->connected = 'yes';
            $channel_model->square_application_id = $square_application_id;
            $channel_model->square_personal_access_token = $square_personal_access_token;
            $channel_model->created = date("Y-m-d h:i:s", time());
            $channel_model->updated = date("Y-m-d h:i:s", time());
            if($channel_model->save(false)){
                $array_msg['success'] = "Square has been successfully connected. Importing has started and you will be notified once completed.";
            }
            else{
                $array_msg['error'] = "Error Something went wrong. Please try again";
            }
            return json_encode($array_msg);
        }
    }
    /**
     * Start Square Importing
     */
    public function actionSquareImporting(){
        $user_id = $_GET['user_id'];
        $get_users = User::find()->select('domain_name,company_name,email')->where(['id' => $user_id])->one();
        $email = $get_users->email;
        $company_name = $get_users->company_name;
        $config = yii\helpers\ArrayHelper::merge(
            require($_SERVER['DOCUMENT_ROOT'].'/common/config/main.php'),
            require($_SERVER['DOCUMENT_ROOT'].'/common/users_config/'.$get_users->domain_name.'/main-local.php'), 
            require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main.php'), 
            require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);
        $channel_data = Channels::find()->where(['channel_name' => 'Square'])->one();
        $square_channel_id = $channel_data->channel_ID;
        $square_details = ChannelConnection::find()->where(['user_id' => $user_id, 'channel_id' => $square_channel_id])->one();
        if(!empty($square_details)){
            //echo "<pre>"; print_r($square_integration); die;
            //echo "<pre>"; print_r($square_details);
            $channel_connection_id = $square_details->channel_connection_id;
            require_once($_SERVER['DOCUMENT_ROOT'].'/backend/web/square/vendor/autoload.php');
            \SquareConnect\Configuration::getDefaultConfiguration()->setAccessToken($square_details->square_personal_access_token);
            $this->squareChannelImporting($channel_connection_id);
            $this->squareCustomerImporting($user_id, $channel_connection_id);
            $this->squareCategoryImporting($user_id,$channel_connection_id);
            $country_code = $this->getSquareCountryCode();
            $this->squareProductImporting($user_id, $channel_connection_id, $square_channel_id, $country_code);
            $this->squareCurrencyDetails($user_id, $channel_connection_id, $square_channel_id);
            //$this->squareGetSinglProduct($user_id);
            //$resul = $this->retireveSquareProductInventory($user_id);
            //echo "<pre>"; print_r($resul); echo "</pre>";
            //$this->squareProductCreate();
            //$this->squareCategoryCreate();
            //$this->getSquareListTransaction($user_id);
            //$this->squareOrderImporting($user_id);
            //$this->adjustSquareProductInventory();
            
            $cron_task_square = CronTasks::find()->where(['elliot_user_id' => $user_id, 'task_source' => 'Square'])->one();
            if(empty($cron_task_square)){
                $CronTasks = new CronTasks();
                $CronTasks->elliot_user_id = $user_id;
                $CronTasks->task_name = 'import';
                $CronTasks->task_source = 'Square';
                $CronTasks->status = 'Completed';
                $CronTasks->created_at = date('Y-m-d h:i:s', time());
                $CronTasks->updated_at = date('Y-m-d h:i:s', time());
                $CronTasks->save();
            }
            
            $email_message = 'Success, Your Square channel is Now Connected';
            $email = 'noor.mohamad@brihaspatitech.com';
            $send_email_notif = CustomFunction::ConnectBigCommerceEmail($email, $company_name, $email_message);
            
            //Drop-Down Notification for User
            $notif_type = 'Square';
            $notif_db = Notification::find()->Where(['notif_type' => $notif_type])->one();
            if (empty($notif_db)):
                $notification_model = new Notification();
                $notification_model->user_id = $user_id;
                $notification_model->notif_type = $notif_type;
                $notification_model->notif_description = 'Your Square Channel data has been successfully imported.';
                $notification_model->created_at = date('Y-m-d h:i:s', time());
                $notification_model->save(false);
            endif;
            //Sticky Notification :StoresConnection Model Entry for Import Status
            $get_rec = ChannelConnection::find()->Where(['user_id' => $user_id, 'channel_id' => $square_channel_id])->one();
            $get_rec->import_status = 'Completed';
            $get_rec->save(false);
        }
    }
    /** Square Currency Details **/
    public function squareCurrencyDetails($user_id, $channel_connection_id, $square_channel_id) {
        $currency_channel_exists = Channelsetting::find()->Where(['channel_id' => $square_channel_id, 'setting_key' => 'currency', 'user_id' => $user_id])->one();
        if (empty($currency_channel_exists)) {
            $channel_setting = new Channelsetting();
            $channel_setting->channel_id = $square_channel_id;
            $channel_setting->user_id = $user_id;
            $channel_setting->channel_name = 'Square';
            $channel_setting->setting_key = 'currency';
            $channel_setting->setting_value = 'USD';
            $channel_setting->mul_channel_id = $channel_connection_id;
            $channel_setting->created_at = date('Y-m-d H:i:s');
            $channel_setting->save(false);
        }
    }
    /** Square Location Importing **/
    public function getSquareLocationId(){
        try {
            $api = new \SquareConnect\Api\LocationsApi();
            $location_result = $api->listLocations();
            return $location_result['locations'][0]['id'];
        } catch (Exception $ex) {
            echo 'Exception when calling : '; echo  $e->getMessage();
        }
    }
    
    /** Square Location Importing **/
    public function getSquareCountryCode(){
        try {
            $api = new \SquareConnect\Api\LocationsApi();
            $location_result = $api->listLocations();
            return $location_result['locations'][0]['address']['country'];
        } catch (Exception $ex) {
            echo 'Exception when calling : '; echo  $e->getMessage();
        }
    }
    
    /** Square store importing **/
    
    public function squareChannelImporting($channel_connection_id){
        try {
            $api = new \SquareConnect\Api\LocationsApi();
            $location_result = $api->listLocations();
            $channel_name = $location_result['locations'][0]['name'];
            $address = $location_result['locations'][0]['address']['address_line_1'];
            $state = $location_result['locations'][0]['address']['administrative_district_level_1'];
            $zip_code = $location_result['locations'][0]['address']['postal_code'];
            $country_code = $location_result['locations'][0]['address']['country'];
            $first_name = $location_result['locations'][0]['address']['first_name'];
            $last_name = $location_result['locations'][0]['address']['last_name'];
            
            $others_details = array(
                "contact_name" => $first_name." ".$last_name,
                "company" => "",
                "address" => $address,
                "city" => $state,
                "province" => $state,
                "country" => 'United States',
                "post_code" => $zip_code,
                "phone" => '',
                "currency" => 'USD'
            );

            $others_details_serialized = serialize($others_details);
            
            $save_store_details = new StoreDetails();
            $save_store_details->channel_connection_id = $channel_connection_id;
            $save_store_details->store_name = $channel_name;
            $save_store_details->store_url = '';
            $save_store_details->country = 'United States';
            $save_store_details->country_code = $country_code;
            $save_store_details->currency = "USD";
            $save_store_details->currency_symbol = '$';
            $save_store_details->others = $others_details_serialized;
            $save_store_details->channel_accquired = 'Square';
            $save_store_details->created_at = date('Y-m-d H:i:s', time());
            $save_store_details->save(false);
        } catch (Exception $ex) {
            echo 'Exception when calling : '; echo  $e->getMessage();
        }
    }
    
    /** Square Category Importing **/
    public function squareCategoryImporting($user_id, $channel_connection_id)
    {
        try {
            $api = new \SquareConnect\Api\CatalogApi();
            $types = join(",", [
              "CATEGORY"
            ]);
            $cursor = null;
            $category_result = [];
            do {
                $apiResponse = $api->listCatalog($cursor, $types);
                $cursor = $apiResponse['cursor'];
                if ($apiResponse['objects'] != null) {
                  $category_result = array_merge($category_result, $apiResponse['objects']);
                }
            } while ($apiResponse['cursor']);
            //echo "<pre>"; print_r($category_result); die();
            foreach($category_result as $cat)
            {
                $cat_id = $cat['id'];
                $cat_name = $cat['category_data']['name'];
                $created_at = $cat['updated_at'];
                
                $category_data = array(
                    'category_id'=>$cat_id,     // Give category id of Store/channels
                    'parent_id'=>0,     // Give Category parent id of Elliot if null then give 0
                    'name'=>$cat_name,   // Give category name
                    'channel_store_name'=>'Square',   // Give Channel/Store name
                    'channel_store_prefix'=>'SQ',  // Give Channel/Store prefix id
                    'mul_store_id'=> '',  // Give Channel/Store prefix id
                    'mul_channel_id'=>$channel_connection_id,  // Give Channel/Store prefix id
                    'elliot_user_id'=>$user_id,     // Give Elliot user id
                    'created_at'=> date('Y-m-d H:i:s'),  // Give Created at date if null then give current date format date('Y-m-d H:i:s')
                    'updated_at'=> date('Y-m-d H:i:s'),  // Give Updated at date if null then give current date format date('Y-m-d H:i:s')
                );
                Stores::categoryImportingCommon($category_data);    // Then call store modal function and give $category data
                
//                $checkCategoryName = Categories::find()->where(['category_name' => $cat_name])->one();
//                if(empty($checkCategoryName)){
//                    $categoryModel = new Categories();
//                    $categoryModel->channel_abb_id = '';
//                    $categoryModel->category_name = $cat_name;
//                    $categoryModel->parent_category_ID = 0;
//                    $categoryModel->elliot_user_id = $user_id;
//                    $categoryModel->created_at = date("Y-m-d H:i:s", strtotime($created_at));
//                    if($categoryModel->save()){
//                        $CategoryAbbrivation = new CategoryAbbrivation();
//                        $CategoryAbbrivation->channel_abb_id = 'SQ'.$cat_id;
//                        $CategoryAbbrivation->category_ID = $categoryModel->category_ID;
//                        $CategoryAbbrivation->channel_accquired = 'Square';
//                        $CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
//                        $CategoryAbbrivation->save(false);
//                    }
//                }
//                else{
//                    $CategoryAbbrivation = new CategoryAbbrivation();
//                    $CategoryAbbrivation->channel_abb_id = 'SQ'.$cat_id;
//                    $CategoryAbbrivation->category_ID = $checkCategoryName->category_ID;
//                    $CategoryAbbrivation->channel_accquired = 'Square';
//                    $CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
//                    $CategoryAbbrivation->save(false);
//                }
            }
        } catch (Exception $e) {
            echo 'Exception when calling ', $e->getMessage(), PHP_EOL;
        }
    }
    
    /** Square Category Importing **/
    public function squareProductImporting($user_id, $channel_connection_id, $square_channel_id, $country_code){
        try {
            $api = new \SquareConnect\Api\CatalogApi();
            $types = join(",", [
              "ITEM",
            ]);
            $cursor = null;
            $product_result = [];
            do {
                $apiResponse = $api->listCatalog($cursor, $types);
                $cursor = $apiResponse['cursor'];
                if ($apiResponse['objects'] != null) {
                  $product_result = array_merge($product_result, $apiResponse['objects']);
                }
            } while ($apiResponse['cursor']);
            //return $product_result;
            //die('End here');
            //echo "<pre>"; print_r($product_result); die;
            if(count($product_result)>0){
                foreach ($product_result as $_product){
                    $product_id = $_product['id'];
                    $product_catalog_v1_ids = $_product['catalog_v1_ids'];
                    if(count($product_catalog_v1_ids)>0){
                        $product_id = $_product['catalog_v1_ids'][0]['catalog_v1_id'];
                    }
                    $created_at = $_product['updated_at'];
                    $name = $_product['item_data']['name'];
                    $description = $_product['item_data']['description'];
                    $category_id = $_product['item_data']['category_id'];
                    $image_url = isset($_product['item_data']['image_url']) ? $_product['item_data']['image_url'] : '';
                    $variations = $_product['item_data']['variations'];
                    $product_count = 1;
                    foreach($variations as $_variation){
                        $variation_id = $_variation['id'];
                        $variation_id_second = $_variation['catalog_v1_ids'];
                        if(count($variation_id_second)>0){
                            $variation_id = $variation_id_second[0]['catalog_v1_id'];
                        }
                        $variants_created_at = $_variation['updated_at'];
                        $variants_updated_at = $_variation['updated_at'];

                        $variation_item_id = $_variation['item_variation_data']['item_id'];
                        $variation_name = $_variation['item_variation_data']['name'];
                        $variation_sku = $_variation['item_variation_data']['sku'];
                        $variation_upc = ($_variation['item_variation_data']['upc']!='')?$_variation['item_variation_data']['upc']:'';
                        $variation_amount = $_variation['item_variation_data']['price_money']['amount'];
                        $variation_inventory_alert = $_variation['item_variation_data']['location_overrides'][0]['track_inventory'];
                        $variation_inventory_alert_amount = $_variation['item_variation_data']['location_overrides'][0]['inventory_alert_threshold'];
                        $inventory_data=$this->retireveSquareProductInventory($user_id);
                        $product_qty='';
                        foreach($inventory_data as $_inventory){
                            if($_inventory['variation_id']==$variation_id){
                                $product_qty = $_inventory['quantity_on_hand'];
                            }
                        }
                        $stock_status = ($product_qty>0) ? 1 : 0;
                        
                        if($product_count==1){
                            
                            $product_image_data = array();
                            if($image_url!=''){
                                foreach ($product_images as $_image) {
                                    $product_image_data[] = array(
                                        'image_url' => $image_url,
                                        'label' => '',
                                        'position' => 1,
                                        'base_img' => $image_url,
                                    );
                                }
                            }
                            
                            $product_data = array(
                                'product_id' => $product_id, // Stores/Channel product ID
                                'name' => $name, // Product name
                                'sku' => $variation_sku, // Product SKU
                                'description' => $description, // Product Description
                                'product_url_path' => '', // Product url if null give blank value
                                'weight' => 0, // Product weight if null give blank value
                                'status' => 1, // Product status (visible or hidden). Give in 0 or 1 form (0 = hidden, 1 = visible).
                                'price' => $variation_amount/100, // Porduct price
                                'sale_price' => $variation_amount/100, // Product sale price if null give Product price value
                                'qty' => $product_qty, //Product quantity 
                                'stock_status' => $stock_status, // Product stock status ("in stock" or "out of stock"). 
                                // Give in 0 or 1 form (0 = out of stock, 1 = in stock)
                                'websites' => array(), //This is for only magento give only and blank array
                                'brand' => '', // Product brand if any
                                'low_stock_notification' => $variation_inventory_alert_amount, // Porduct low stock notification if any otherwise give default 5 value
                                'created_at' => $variants_created_at, // Product created at date format date('Y-m-d H:i:s')
                                'updated_at' => $variants_updated_at, // Product updated at date format date('Y-m-d H:i:s')
                                'channel_store_name' => 'Square', // Channel or Store name
                                'channel_store_prefix' => 'SQ', // Channel or store prefix id
                                'mul_store_id' => '', //Give multiple store id
                                'mul_channel_id' => $channel_connection_id, // Give multiple channel id
                                'elliot_user_id' => $user_id, // Elliot user id
                                'store_id' => '', // if your are importing store give store id
                                'channel_id' => $square_channel_id, // if your are importing channel give channel id
                                'country_code' => $country_code,
                                'currency_code' => 'USD',
                                'upc' => $variation_upc, // Product upc if any
                                'jan' => '', // Product jan if any
                                'isban' => '', // Product isban if any
                                'mpn' => '', // Product mpn if any
                                'categories' => array($category_id), // Product categroy array. If null give a blank array 
                                'images' => $product_image_data, // Product images data
                            );
                            Stores::productImportingCommon($product_data);
                        }
                        $product_count++;
                    }
                }
            }
        } catch (Exception $e) {
            echo 'Exception when calling ', $e->getMessage(), PHP_EOL;
        }
    }
    
    /**
     * Square get single product details
     * @param str $product_id Description
     */
    public function retrieveSquareSingleProduct($product_id){
        $api_instance = new \SquareConnect\Api\V1ItemsApi();
        $location_id = $this->getSquareLocationId();
        try {
            $product_data = $api_instance->retrieveItem($location_id, $product_id);
            //echo "<pre>"; print_r($result); echo "</pre>";
            return $product_data;
        } catch (Exception $e) {
            echo 'Exception when calling V1ItemsApi->retrieveItem: ', $e->getMessage(), PHP_EOL;
        }
    }
    /**
     * Square get product inventory list
     */
    public function retireveSquareProductInventory() {
        $api_instance = new \SquareConnect\Api\V1ItemsApi();
        $location_id = $this->getSquareLocationId();
        $limit = 1000;
        try {
            $result = $api_instance->listInventory($location_id, $limit);
            return $result;
        } catch (Exception $e) {
            echo 'Exception when calling V1ItemsApi->listInventory: ', $e->getMessage(), PHP_EOL;
        }
    }
    /**
     * Square adjust product inventory
     */
    public function adjustSquareProductInventory(){
        $api_instance = new \SquareConnect\Api\V1ItemsApi();
        $location_id = $this->getSquareLocationId();
        $variation_id = "FR5XY57TPV6GF5GRCI2VKVEN"; 
        $catalog_v1_ids = "80e4dc6b-d623-4fe7-afeb-7bdf8d5c2386";
        $product_qty=786;
        $body = new \SquareConnect\Model\V1AdjustInventoryRequest();
        $inventory_list=$this->retireveSquareProductInventory();
        //echo "<pre>"; print_r($inventory_list); echo "</pre>";
        foreach($inventory_list as $_inventory)
        {
            if($_inventory['variation_id']==$variation_id){
                $qty_on_hand = $_inventory['quantity_on_hand'];
                if($product_qty>$qty_on_hand){
                    $final_product_qty=$product_qty-$qty_on_hand;
                    $body['quantity_delta'] = $final_product_qty;
                    $body['adjustment_type'] = '';
                    $body['memo'] = '';
                    try {
                        $result = $api_instance->adjustInventory($location_id, $variation_id, $body);
                    } catch (Exception $e) {
                        echo 'Exception when calling V1ItemsApi->adjustInventory: ', $e->getMessage();
                    }
                }
                if($product_qty<$qty_on_hand){
                    $final_product_qty=$qty_on_hand-$product_qty;
                    $body['quantity_delta'] = -$final_product_qty;
                    $body['adjustment_type'] = '';
                    $body['memo'] = '';
                    try {
                        $result = $api_instance->adjustInventory($location_id, $variation_id, $body);
                    } catch (Exception $e) {
                        echo 'Exception when calling V1ItemsApi->adjustInventory: ', $e->getMessage();
                    }
                }
                if($product_qty==$qty_on_hand){
                    $final_product_qty=$qty_on_hand-$product_qty;
                }
            }
            if($_inventory['variation_id']==$catalog_v1_ids){
                $qty_on_hand = $_inventory['quantity_on_hand'];
                if($product_qty>$qty_on_hand){
                    $final_product_qty=$product_qty-$qty_on_hand;
                    $body['quantity_delta'] = $final_product_qty;
                    $body['adjustment_type'] = '';
                    $body['memo'] = '';
                    try {
                        $result = $api_instance->adjustInventory($location_id, $catalog_v1_ids, $body);
                    } catch (Exception $e) {
                        echo 'Exception when calling V1ItemsApi->adjustInventory: ', $e->getMessage();
                    }
                }
                if($product_qty<$qty_on_hand){
                    $final_product_qty=$qty_on_hand-$product_qty;
                    $body['quantity_delta'] = -$final_product_qty;
                    $body['adjustment_type'] = '';
                    $body['memo'] = '';
                    try {
                        $result = $api_instance->adjustInventory($location_id, $catalog_v1_ids, $body);
                    } catch (Exception $e) {
                        echo 'Exception when calling V1ItemsApi->adjustInventory: ', $e->getMessage();
                    }
                }
                if($product_qty==$qty_on_hand){
                    $final_product_qty=$qty_on_hand-$product_qty;
                }
            }
        }
    }
    /**
     * Square Category Importing
     */
    public function squareCustomerImporting($user_id, $channel_connection_id)
    {
        try {
            $api = new \SquareConnect\Api\CustomersApi();
            $customer_result = $api->listCustomers();
            //echo "<pre>"; print_r($customer_result); die;
            foreach($customer_result['customers'] as $_customer){
                $customer_id = $_customer['id'];
                $created_at = $_customer['created_at'];
                $updated_at = $_customer['updated_at'];
                $updated_at = $_customer['updated_at'];
                $f_name = $_customer['given_name'];
                $l_name = $_customer['family_name'];
                $email = $_customer['email_address'];
                $phone = $_customer['phone_number'];
                $address = $_customer['address'];
                $add_1 = $add_2 = $city = $state = $zip = $country = '';
                if($address!=''){
                    $add_1 = $address['address_line_1'];
                    $add_2 = $address['address_line_2'];
                    $city = $address['locality'];
                    $state = $address['administrative_district_level_1'];
                    $zip = $address['postal_code'];
                    $country = $address['country'];
                }

                $customer_data = array(
                    'customer_id'=>$customer_id,
                    'elliot_user_id'=>$user_id,
                    'first_name'=>$f_name,
                    'last_name'=>$l_name,
                    'email'=>$email,
                    'channel_store_name'=>'Square',
                    'channel_store_prefix'=>'SQ',
                    'mul_store_id' => '',
                    'mul_channel_id' => $channel_connection_id,
                    'customer_created_at'=>$created_at,
                    'customer_updated_at'=>$updated_at,
                    'billing_address'=>array(
                        'street_1'=>$add_1,
                        'street_2'=>$add_2,
                        'city'=>$city,
                        'state'=>$state,
                        'country'=>$country,
                        'country_iso'=>'',
                        'zip'=>$zip,
                        'phone_number'=>$phone,
                        'address_type'=>'Default',
                    ),
                    'shipping_address' => array(
                        'street_1'=>'',
                        'street_2'=>'',
                        'city'=>'',
                        'state'=>'',
                        'country'=>'',
                        'zip'=>'',
                    ),
                );
                Stores::customerImportingCommon($customer_data);
            }
        } catch (Exception $e) {
            echo 'Exception when calling ', $e->getMessage(), PHP_EOL;
        }
    }
    
    /**
     * Create Product in Square
     */
    public function squareProductCreate() {
        $api_instance = new \SquareConnect\Api\V1ItemsApi();
        $location_id = $this->getSquareLocationId();
        $body = new \SquareConnect\Model\V1Item();
        $inventory = new \SquareConnect\Model\V1AdjustInventoryRequest();
        $category = new \SquareConnect\Model\V1Category();
        $product_qty = 78;
        $body = array(
            'name'=>'test-name',
            'description'=>'It has survived not only five centuries, but also the leap into electronic type setting.',
            'visibility'=>'PRIVATE',
            //'category_id'=>'S6LWFKWSAMDRBMNFEPF5XXLX',
            'category_id'=>'d1dc0ec3-5da0-418e-9b57-8c2af315009e',
            'variations'=>[array(
                'id'=>uniqid(),
                'name'=>'Regular',
                'pricing_type'=>'FIXED_PRICING',
                'price_money'=>array(
                    'currency_code'=>'USD',
                    'amount'=>7899,
                ),
                'sku'=>'test-name-001',
                'track_inventory'=>true,
                'inventory_alert_type'=>'LOW_QUANTITY',
                'inventory_alert_threshold'=>3,
            )],
        );
        try {
            $product_data = $api_instance->createItem($location_id, $body);
            echo "<pre>"; print_r($product_data);
            $variation = $product_data['variations'];
            foreach($variation as $_variation){
                $variation_id = $_variation['id'];
                $inventory['quantity_delta'] = $product_qty;
                $inventory['adjustment_type'] = '';
                $inventory['memo'] = '';
                try {
                    $inventory_data = $api_instance->adjustInventory($location_id, $variation_id, $inventory);
                    echo "<pre>"; print_r($inventory_data); 
                } catch (Exception $e) {
                    echo 'Exception when calling V1ItemsApi->adjustInventory: ', $e->getMessage();
                }
            }
        } catch (Exception $e) {
            echo 'Exception when calling V1ItemsApi->createItem: ', $e->getMessage(), PHP_EOL;
        }
    }
    
    /**
     * Create Category in SquareUp Channel
     */
    public function squareCategoryCreate() {
        $api_instance = new \SquareConnect\Api\V1ItemsApi();
        $location_id = $this->getSquareLocationId();
        $body = new \SquareConnect\Model\V1Category();
        $cat_array = array(
            'Soft Drinks',
            'Hard Drinks',
            'Bears',
        );
        foreach($cat_array as $_cat){
            $body = array(
                'name'=>$_cat,
            );
            try {
                $category_response = $api_instance->createCategory($location_id, $body);
                echo "<pre>"; print_r($category_response);
            } catch (Exception $e) {
                echo 'Exception when calling V1ItemsApi->createCategory: ', $e->getMessage(), PHP_EOL;
            }
        }
        
    }
    
    /**
     * Square Order Import
     */
    public function squareOrderImporting($user_id){
        $api_instance = new \SquareConnect\Api\V1TransactionsApi();
        $location_id = $this->getSquareLocationId();
        $order = "orders";
        $limit = 200; 
        try {
            $result = $api_instance->listOrders($location_id, $order, $limit);
            echo "<pre>"; print_r($result);
        } catch (Exception $e) {
            echo 'Exception when calling V1TransactionsApi->listOrders: ', $e->getMessage(), PHP_EOL;
        }
    }
    /**
     * Square get List of Trasactions
     */
    public function getSquareListTransaction($user_id){
        $api_instance = new \SquareConnect\Api\TransactionsApi();
        $curr_date = date('Y-m-d');
        $location_id = $this->getSquareLocationId();
        $begin_time = "2000-01-01";
        $end_time = $curr_date; 
        $sort_order = "ASC"; 
        $cursor = ""; 
        try {
            $result = $api_instance->listTransactions($location_id, $begin_time, $end_time, $sort_order, $cursor);
            echo "<pre>"; print_r($result);
        } catch (Exception $e) {
            echo 'Exception when calling TransactionsApi->listTransactions: ', $e->getMessage(), PHP_EOL;
        }
    }
    /**
    * Square webhooks response
    */
    public function actionSquareWebhookResponse(){
        //$webhookContent = "";
        //$webhook = fopen('php://input' , 'rb');
        //while (!feof($webhook)){
        //    $webhookContent .= fread($webhook, 4096);
        //}
        //$content=json_encode($webhookContent);
        $content = json_encode($_REQUEST);
        $myfile = fopen("square_webhook_response.txt", "w") or die("Unable to open file!");
        fwrite($myfile, $content);
        fclose($myfile);
    }
    
    public function actionSquareCronImporting() {
        $user_id = $_GET['user_id'];
        $channel = $_GET['channel'];
        $get_users = User::find()->select('domain_name,company_name,email')->where(['id' => $user_id])->one();
        $email = $get_users->email;
        $company_name = $get_users->company_name;
        $config = yii\helpers\ArrayHelper::merge(
            require($_SERVER['DOCUMENT_ROOT'].'/common/config/main.php'),
            require($_SERVER['DOCUMENT_ROOT'].'/common/users_config/' . $get_users->domain_name . '/main-local.php'), 
            require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main.php'), 
            require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);
        
        $cron_tasks_data = CronTasks::find()->where(['elliot_user_id' => $user_id, 'task_source' => 'Square', 'status' => 'Completed'])->one();
        if(!empty($cron_tasks_data)){
            $cron_tasks_data->status = 'Pending';
            $cron_tasks_data->updated_at = date('Y-m-d h:i:s', time());
            $cron_tasks_data->save();
        
            $channel_data = Channels::find()->where(['channel_name' => 'Square'])->one();
            $square_channel_id = $channel_data->channel_ID;
            $square_details = ChannelConnection::find()->where(['user_id' => $user_id, 'channel_id' => $square_channel_id])->one();
            if(!empty($square_details)){
                $channel_connection_id = $square_details->channel_connection_id;
                require_once($_SERVER['DOCUMENT_ROOT'].'/backend/web/square/vendor/autoload.php');
                \SquareConnect\Configuration::getDefaultConfiguration()->setAccessToken($square_details->square_personal_access_token);
                $this->squareCustomerImporting($user_id, $channel_connection_id);
                $this->squareCategoryImporting($user_id, $channel_connection_id);
                $country_code = $this->getSquareCountryCode();
                $this->squareProductImporting($user_id, $channel_connection_id, $square_channel_id, $country_code);

                $cron_tasks_data = CronTasks::find()->where(['elliot_user_id' => $user_id, 'task_source' => 'Square', 'status' => 'Pending'])->one();
                if(!empty($cron_tasks_data)){
                    $cron_tasks_data->status = 'Completed';
                    $cron_tasks_data->updated_at = date('Y-m-d h:i:s', time());
                    $cron_tasks_data->save();
                } 
            }
        }
    }
    
    public function squareGetSingleProduct(){
        $api_instance = new \SquareConnect\Api\V1ItemsApi();
        $location_id = $this->getSquareLocationId();
        $item_id = "BUOOCYRHGG7XLHSD2VNVPX4Z"; // string | The item's ID.

        try {
            $result = $api_instance->retrieveItem($location_id, $item_id);
            echo "<pre>"; print_r($result);
        } catch (Exception $e) {
            echo 'Exception when calling V1ItemsApi->retrieveItem: ', $e->getMessage(), PHP_EOL;
        }
    }
    
    public function SquareProductUpdate(){
        require_once($_SERVER['DOCUMENT_ROOT'].'/backend/web/square/vendor/autoload.php');
        \SquareConnect\Configuration::getDefaultConfiguration()->setAccessToken('sq0atp-vhSaMlFhSdZT5wbovp2QeA');
        $api_instance = new \SquareConnect\Api\V1ItemsApi();
        $location_id = $this->getSquareLocationId();
        //$item_id = "f615a642-12b5-4711-ada6-eab795d5877c";
        $item_id = "CFDLCYV4WU25Q2544KVOTPLN";
        $item_body = new \SquareConnect\Model\V1Item();
        $variation_body = new \SquareConnect\Model\V1Variation();
        $inventory_body = new \SquareConnect\Model\V1AdjustInventoryRequest();
        $product_qty = 65;
        try {
            $item_body = array(
                'name' => 'Tea special',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer',
                'visibility' => 'PRIVATE',
                'category_id' => 'NJG5FX2QN6FKC2NGFSK26IBN',
            );
            $product_result = $api_instance->updateItem($location_id, $item_id, $item_body);
            //echo "<pre>"; print_r($product_result); echo "</pre>";
            $variation = $product_result['variations'];
            foreach($variation as $_variation){
                $variation_id = $_variation['id'];
                $inventory_list=$this->retireveSquareProductInventory();
                foreach($inventory_list as $_inventory)
                {
                    if($_inventory['variation_id']==$variation_id){
                        $qty_on_hand = $_inventory['quantity_on_hand'];
                        if($product_qty>$qty_on_hand){
                            $final_product_qty=$product_qty-$qty_on_hand;
                            $inventory_body['quantity_delta'] = $final_product_qty;
                            $inventory_body['adjustment_type'] = '';
                            $inventory_body['memo'] = '';
                            try {
                                $result = $api_instance->adjustInventory($location_id, $variation_id, $inventory_body);
                            } catch (Exception $e) {
                                echo 'Exception when calling V1ItemsApi->adjustInventory: ', $e->getMessage();
                            }
                        }
                        if($product_qty<$qty_on_hand){
                            $final_product_qty=$qty_on_hand-$product_qty;
                            $inventory_body['quantity_delta'] = -$final_product_qty;
                            $inventory_body['adjustment_type'] = '';
                            $inventory_body['memo'] = '';
                            try {
                                $result = $api_instance->adjustInventory($location_id, $variation_id, $inventory_body);
                            } catch (Exception $e) {
                                echo 'Exception when calling V1ItemsApi->adjustInventory: ', $e->getMessage();
                            }
                        }
                    }
                }
            }
            try {
                $variation_body = array(
                    'name' => 'Regular',
                    'pricing_type' => 'FIXED_PRICING',
                    'price_money' => array(
                        'currency_code' => 'USD',
                        'amount' => 99*100,
                    ),
                    'sku' => 'tea-special-0001',
                    'track_inventory' => true,
                    'inventory_alert_type' => 'LOW_QUANTITY',
                    'inventory_alert_threshold' => 8,
                );
                $variation_result = $api_instance->updateVariation($location_id, $item_id, $variation_id, $variation_body);
                //echo "<pre>"; print_r($variation_result); echo "<pre>";
            } catch (Exception $e) {
                echo 'Exception when calling V1ItemsApi->updateVariation: ', $e->getMessage(), PHP_EOL;
            }
        } catch (Exception $e) {
            echo 'Exception when calling V1ItemsApi->updateItem: ', $e->getMessage(), PHP_EOL;
        }
    }
    
    
    /**
     * test action for testing perpose
     */
    public function actionTest() {
//        echo "Hello world!"; 
//        require_once($_SERVER['DOCUMENT_ROOT'].'/backend/web/square/vendor/autoload.php');
//        \SquareConnect\Configuration::getDefaultConfiguration()->setAccessToken('sq0atp-vhSaMlFhSdZT5wbovp2QeA'); 
//        $api_instance = new \SquareConnect\Api\V1ItemsApi();
//        $location_id = $this->getSquareLocationId();
//        $body = new \SquareConnect\Model\V1Category();
//        $body = array(
//            'name'=>'testing',
//        );
//        try {
//            $category_response = $api_instance->createCategory($location_id, $body);
//            echo "<pre>"; print_r($category_response);
//        } catch (Exception $e) {
//            echo 'Exception when calling V1ItemsApi->createCategory: ', $e->getMessage(), PHP_EOL;
//        }
//        $result = $this->squareProductImporting(1270);
//        foreach($result as $_result){
//            $p_id = $_result['id'];
//            echo "Product id herer => ".$p_id;
//            $p_catalog_ids = $_result['catalog_v1_ids'];
//            if(count($p_catalog_ids)>0){
//                echo "<br>";
//                echo "Product catlog ids here => ".$p_catalog_ids[0]['catalog_v1_id'];
//                echo "<br>";
//            }
//            echo "<br>";
//            echo "<br>";
//        }

echo 'Abe changes aa rhe hai!!!!!!!!!!!';
        
    }
}        