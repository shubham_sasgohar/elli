<?php

namespace backend\controllers;

use Yii;
use backend\models\CustomerUser;
use backend\models\ChannelConnection;
use backend\models\Channels;
use backend\models\User;
use backend\models\CustomerUserSearch;
use backend\models\CustomerAbbrivation;
use backend\models\Stores;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\Countries;
use backend\models\States;
use backend\models\Cities;
use backend\models\StoresConnection;
use backend\models\StoreDetails;
use Bigcommerce\Api\Client as Bigcommerce;
use backend\models\Orders;
use backend\models\OrdersProducts;
use backend\models\OrderChannel;
use backend\models\Products;
use backend\models\Categories;
use backend\models\CategoryAbbrivation;
use backend\models\ProductCategories;
use backend\models\ProductChannel;
use backend\models\ProductAbbrivation;
use backend\models\ProductImages;
use Bigcommerce\Api\ShopifyClient as Shopify;
use backend\models\ProductVariation;
use backend\models\VariationsSet;
use backend\models\Variations;
use backend\models\VariationsItemList;
use backend\models\MerchantProducts;
use Automattic\WooCommerce\Client as Woocommerce;
use Automattic\WooCommerce\HttpClient\HttpClientException;
use SoapClient;
use yii\db\Query;
use yii\db\ActiveQuery;
use yii\db\QueryBuilder;
use yii\data\ActiveDataProvider;
use backend\controllers\StoresController as StoresController;
use backend\controllers\ChannelsController as ChannelsController;

/**
 * CustomerUserController implements the CRUD actions for CustomerUser model.
 */
class PeopleController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'savecustomer', 'create-customer-hook', 'orderbigcommerce', 'orderhookcreate', 'producthooksbg', 'skuupdategb', 'productskuhooksbg',
                    'categoryhooksbg', 'shopify-product-create', 'shopify-product-update', 'shopify-product-delete', 'shopify-order-create', 'shopify-order-paid', 'shopify-order-update',
                    'shopify-customer-create', 'shopify-customer-update', 'magento-product-create', 'magento-order-create', 'magento-customer-create', 'magento-customer-address-update', 'connected-customer',
                    'inactive-customers', 'orderbigcommercecreate', 'orderbigcommerceupdate'],
                'rules' => [
                        [
                        'actions' => ['signup', 'create-customer-hook', 'orderbigcommerce', 'orderhookcreate', 'producthooksbg', 'skuupdategb', 'productskuhooksbg', 'categoryhooksbg',
                            'shopify-product-create', 'shopify-product-update', 'shopify-product-delete', 'shopify-order-create', 'shopify-order-paid', 'shopify-order-update',
                            'shopify-customer-create', 'shopify-customer-update', 'magento-product-create', 'magento-order-create', 'magento-customer-create', 'magento-customer-address-update', 'orderbigcommercecreate', 'orderbigcommerceupdate'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                        [
                        'actions' => ['logout', 'index', 'view', 'create', 'update', 'savecustomer', 'create-customer-hook', 'orderbigcommerce', 'orderhookcreate', 'producthooksbg',
                            'skuupdategb', 'productskuhooksbg', 'categoryhooksbg', 'shopify-product-create', 'shopify-product-update', 'shopify-product-delete', 'shopify-order-create',
                            'shopify-order-paid', 'shopify-order-update', 'shopify-customer-create', 'shopify-customer-update', 'sku', 'magento-product-create', 'magento-order-create',
                            'magento-customer-create', 'magento-customer-address-update', 'connected-customer', 'inactive-customers', 'orderbigcommercecreate', 'orderbigcommerceupdate'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CustomerUser models.
     * @return mixed
     */
    public function beforeAction($action) {

        $this->enableCsrfValidation = false;


        return parent::beforeAction($action);
    }

    public function actionIndex() {


        $searchModel = new CustomerUserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CustomerUser model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
	$data = $this->findModel($id);
	$user_id=Yii::$app->user->identity->id;
	$total_cus = CustomerUser::find()->Where(['elliot_user_id' => $user_id, 'people_visible_status' => 'active'])->count();

	$star_rate=ChannelsController::getStartRating($data->customer_ID,$data->email,$total_cus);
	
        return $this->render('view', [
                    'model' => $data,
                    'starRate' => $star_rate,
        ]);
    }

    /**
      For Connected Customer
     */
    public function actionConnectedCustomer() {

        return $this->render('connectedcustomer');
    }

    /**
     * Creates a new CustomerUser model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {


        $model = new CustomerUser();

        if (Yii::$app->request->post()) :
            /* $checkbox = $_POST['channel_accquired'];
              $channel = '';
              if (!empty($checkbox)) {
              foreach ($checkbox as $check) {
              $channel = $channel . ',' . $check;
              }
              }
              $channel_name = trim($channel, ","); */

            //shipping
            $ship1 = $_POST['cu-ship-Street-1'];
            $ship2 = $_POST['cu-ship-Street-2'];
            $ship_city = $_POST['cu-ship-City'];
            $ship_state = States::find()->where(['id' => $_POST['cu-ship-State']])->one();
            ;
            $ship_zip = $_POST['cu-ship-Zip'];
            $ship_country = $_POST['cu-ship-Country'];

            $model->elliot_user_id = Yii::$app->user->identity->id;
            $model->first_name = $_POST['cu-first-name'];
            $model->last_name = $_POST['cu-last-name'];
            $model->email = $_POST['cu-email'];
            $model->DOB = $_POST['cu-DOB'];
            $model->gender = $_POST['cu-Gender'];
            $model->phone_number = $_POST['cu-Phone-Number'];
            //$model->channel_acquired = $channel_name;
            $model->channel_acquired = 'BigCommerce';
            //billing
            //customer save billing address
            $model->street_1 = $_POST['cu-Street-1'];
            $model->street_2 = $_POST['cu-Street-2'];
            $model->city = $_POST['cu-City'];
            $state_name = States::find()->where(['id' => $_POST['cu-State']])->one();
            $model->state = $state_name;
            $model->zip = $_POST['cu-Zip'];
            $model->country = $_POST['cu-Country'];

            //shipping
            $model->ship_street_1 = $_POST['cu-ship-Street-1'];
            $model->ship_street_2 = $_POST['cu-ship-Street-2'];
            $model->ship_city = $_POST['cu-ship-City'];
            $ship_state = States::find()->where(['id' => $_POST['cu-ship-State']])->one();
            $model->ship_state = $ship_state;
            $model->ship_zip = $_POST['cu-ship-Zip'];
            $model->ship_country = $_POST['cu-ship-Country'];

            $model->shipping_address = $ship1 . ',' . $ship2 . ',' . ',' . $ship_city . ',' . $ship_state . ',' . $ship_zip . ',' . $ship_country;

            //Create customer in Big Commerce
            //explode channel Name
            //$explode_channel_name = explode(",", $channel_name);
//            foreach ($explode_channel_name as $ch):
//                if ($ch == 'BigCommerce'):
//                    $object = array(
//                        "first_name" => $_POST['cu-first-name'],
//                        "last_name" => $_POST['cu-last-name'],
//                        "email" => $_POST['cu-email']
//                    );
//                    $customer_add_object = array(
//                        "first_name" => $_POST['cu-first-name'],
//                        "last_name" => $_POST['cu-last-name'],
//                        "street_1" => $_POST['cu-Street-1'],
//                        "street_2" => $_POST['cu-Street-2'],
//                        "city" => $_POST['cu-City'],
//                        "state" => $_POST['cu-State'],
//                        "zip" => $_POST['cu-Zip'],
//                        "country" => $_POST['cu-Country'],
//                        "phone" => $_POST['cu-Phone-Number']
//                    );
//
//                    $create_customer_bigCommerce = Stores::bigcommerce_create_customer($object, $customer_add_object);
//                endif;
//                /* Create Customer Shopify */
//                if ($ch == 'Shopify') :
//
//                    $customer_object = array(
//                        "customer" => array(
//                            "first_name" => $_POST['cu-first-name'],
//                            "last_name" => $_POST['cu-last-name'],
//                            "email" => $_POST['cu-email'],
//                            "phone" => $_POST['cu-Phone-Number'],
//                            "verified_email" => true,
//                            "addresses" => [array(
//                            "address1" => $_POST['cu-Street-1'],
//                            "address2" => $_POST['cu-Street-2'],
//                            "city" => $_POST['cu-City'],
//                            "province" => "ON",
//                            "phone" => $_POST['cu-Phone-Number'],
//                            "province" => $_POST['cu-State'],
//                            "zip" => $_POST['cu-Zip'],
//                            "last_name" => $_POST['cu-last-name'],
//                            "first_name" => $_POST['cu-first-name'],
//                            "country" => $_POST['cu-Country']
//                                )],
//                        )
//                    );
//
//                    $create_customer_shopify = Stores::shopify_create_customer($customer_object);
//                endif;
//            endforeach;


            if ($model->save(false)):



                Yii::$app->session->setFlash('success', 'Success! Customer has been created.');
                return $this->redirect(['/people']);

            endif;

        else :
            return $this->render('create', [
                        'model' => $model,
            ]);
        endif;

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing CustomerUser model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            //Update Big Commerce Customer//
            $update_customer_bigcommerce = Stores::bigcommerce_update_customer();

            return $this->redirect(['view', 'id' => $model->customer_ID]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Save Customer Account details through ajax.
     *
     * @return string
     */
    public function actionSavecustomer() {

        $request = Yii::$app->request;
        $userid = Yii::$app->user->identity->id;

        $post_data = Yii::$app->request->post();

        if (Yii::$app->request->post()) {

            $customer_id = Yii::$app->request->post('customer_id');
            $customerdata = CustomerUser::find()->Where(['customer_ID' => $customer_id])->one();
            $channel_abb = $customerdata->channel_abb_id;
            $channel_abb_id = substr($channel_abb, 3);

            $first_name = Yii::$app->request->post('customer_first_name');
            $last_name = Yii::$app->request->post('customer_last_name');
            $email = Yii::$app->request->post('customer_email_add');
            $phone = Yii::$app->request->post('customer_Phone_no');
            $ship1 = Yii::$app->request->post('ship_street1');
            $ship2 = Yii::$app->request->post('ship_street2');
            $ship_city = Yii::$app->request->post('ship_city');
            $ship_state = Yii::$app->request->post('ship_state');
            $ship_zip = Yii::$app->request->post('ship_zip');
            $ship_country = Yii::$app->request->post('ship_country');
            $customerdata->first_name = $first_name;
            $customerdata->last_name = $last_name;
            $customerdata->email = $email;
            $customerdata->DOB = Yii::$app->request->post('customer_dob');
            $customerdata->gender = Yii::$app->request->post('gender');
            $customerdata->phone_number = $phone;

            //billing 
//            $customerdata->street_1 = Yii::$app->request->post('corporate_street1');
//            $customerdata->street_2 = Yii::$app->request->post('corporate_street2');
//            $customerdata->city = Yii::$app->request->post('corporate_city');
//            $customerdata->state = Yii::$app->request->post('corporate_state');
//            $customerdata->zip = Yii::$app->request->post('corporate_zip');
//            $customerdata->country = Yii::$app->request->post('corporate_country');
            //shipping
//            $customerdata->ship_street_1 = $ship1;
//            $customerdata->ship_street_2 = $ship2;
//            $customerdata->ship_city = $ship_city;
//            $customerdata->ship_state = $ship_state;
//            $customerdata->ship_zip = $ship_zip;
//            $customerdata->ship_country = $ship_country;
            // $customerdata->shipping_address = $ship1 . ',' . $ship2 . ',' . $ship_city . ',' . $ship_state . ',' . $ship_zip . ',' . $ship_country;
            //Update Big Commerce Customer//
//            $object = array("first_name" => $first_name, "last_name" => $last_name, "email" => $email, "phone" => $phone);
//            $update_customer_bigcommerce = Stores::bigcommerce_update_customer($channel_abb_id, $object);

            if ($customerdata->save(false)) {
                /* Function call For save Billing And Shipping Save */
                $saveCustomerAbb = $this->saveCustomerAbb($customer_id, $post_data);

                $response = ['status' => 'success', 'data' => 'customer info saved'];
                Yii::$app->session->setFlash('success', 'Success! customer info has been updated.');
            } else {

                $response = ['status' => 'error', 'data' => 'customer info not saved'];
                Yii::$app->session->setFlash('danger', 'Error! customer info has not been updated.');
            }
        }
        echo json_encode($response);
        exit;
    }

    public function saveCustomerAbb($customer_id, $post_data) {


        $customerabb = CustomerAbbrivation::find()->Where(['customer_id' => $customer_id])->all();
        /* For billing */
        $connected_bill_street = isset($post_data['bill_street1']) ? $post_data['bill_street1'] : '';
        $connected_bill_street2 = isset($post_data['bill_street2']) ? $post_data['bill_street2'] : '';
        $connected_bill_city = isset($post_data['bill_city']) ? $post_data['bill_city'] : '';
        $connected_bill_state = isset($post_data['bill_state']) ? $post_data['bill_state'] : '';
        $connected_bill_zip = isset($post_data['bill_zip']) ? $post_data['bill_zip'] : '';
        $connected_bill_country = isset($post_data['bill_country']) ? $post_data['bill_country'] : '';

        $connected_ship_street = isset($post_data['ship_street1']) ? $post_data['ship_street1'] : '';
        $connected_ship_street2 = isset($post_data['ship_street2']) ? $post_data['ship_street2'] : '';
        $connected_ship_city = isset($post_data['ship_city']) ? $post_data['ship_city'] : '';
        $connected_ship_state = isset($post_data['ship_state']) ? $post_data['ship_state'] : '';
        $connected_ship_zip = isset($post_data['ship_zip']) ? $post_data['ship_zip'] : '';
        $connected_ship_country = isset($post_data['ship_country']) ? $post_data['ship_country'] : '';

        foreach ($customerabb as $customer_abb_data) {
            $channel_name = $customer_abb_data->channel_accquired;
            $channel_abb_id = $customer_abb_data->channel_abb_id;
            $mul_store_id = $customer_abb_data->mul_store_id;
            $function = $this->finalSaveCustomerAbb($channel_name, $customer_id, $channel_abb_id, $mul_store_id, $connected_bill_street, $connected_bill_street2, $connected_bill_city, $connected_bill_state, $connected_bill_zip, $connected_bill_country, $connected_ship_street, $connected_ship_street2, $connected_ship_city, $connected_ship_state, $connected_ship_zip, $connected_ship_country);

//            if ($customer_abb_data->channel_accquired == 'BigCommerce') {
//                $channel_name = 'BigCommerce';
//                $channel_abb_id = $customer_abb_data->channel_abb_id;
//                /* Function call For save Billing And Shipping Save According To Channel */
//                $function = $this->finalSaveCustomerAbb($connected_bill_street, $connected_bill_street2, $connected_bill_city, $connected_bill_state, $connected_bill_zip, $connected_bill_country, $connected_ship_street, $connected_ship_street2, $connected_ship_city, $connected_ship_state, $connected_ship_zip, $connected_ship_country, $channel_name, $customer_id, $channel_abb_id);
//            }
//            if ($customer_abb_data->channel_accquired == 'WeChat') {
//                $channel_name = 'WeChat';
//                $channel_abb_id = $customer_abb_data->channel_abb_id;
//                $function = $this->finalSaveCustomerAbb($connected_bill_street, $connected_bill_street2, $connected_bill_city, $connected_bill_state, $connected_bill_zip, $connected_bill_country, $connected_ship_street, $connected_ship_street2, $connected_ship_city, $connected_ship_state, $connected_ship_zip, $connected_ship_country, $channel_name, $customer_id, $channel_abb_id);
//            }
        }
        /* End foreach */
    }

    public function finalSaveCustomerAbb($channel_name, $customer_id, $channel_abb_id, $mul_store_id, $connected_bill_street, $connected_bill_street2, $connected_bill_city, $connected_bill_state, $connected_bill_zip, $connected_bill_country, $connected_ship_street, $connected_ship_street2, $connected_ship_city, $connected_ship_state, $connected_ship_zip, $connected_ship_country) {

        $final_ship_street = $final_ship_street2 = $final_ship_city = $final_ship_state = $final_ship_zip = $final_ship_country = '';
        $final_bill_street = $final_bill_street2 = $final_bill_city = $final_bill_state = $final_bill_zip = $final_bill_country = '';
        if (!empty($connected_bill_street)) {
            foreach ($connected_bill_street as $bill_street_key => $bill_street_val) {
                if ($bill_street_key == $channel_name) {
                    $final_bill_street = $bill_street_val;
                }
            }
        }

        if (!empty($connected_bill_street2)) {
            foreach ($connected_bill_street2 as $bill_street2_key => $bill_street2_val) {
                if ($bill_street2_key == $channel_name) {
                    $final_bill_street2 = $bill_street2_val;
                }
            }
        }

        if (!empty($connected_bill_city)) {
            foreach ($connected_bill_city as $bill_city_key => $bill_city_val) {
                if ($bill_city_key == $channel_name) {
                    $final_bill_city = $bill_city_val;
                }
            }
        }

        if (!empty($connected_bill_state)) {
            foreach ($connected_bill_state as $bill_state_key => $bill_state_val) {
                if ($bill_state_key == $channel_name) {
                    $final_bill_state = $bill_state_val;
                }
            }
        }

        if (!empty($connected_bill_zip)) {
            foreach ($connected_bill_zip as $bill_zip_key => $bill_zip_val) {
                if ($bill_zip_key == $channel_name) {
                    $final_bill_zip = $bill_zip_val;
                }
            }
        }

        if (!empty($connected_bill_country)) {
            foreach ($connected_bill_country as $bill_country_key => $bill_country_val) {
                if ($bill_country_key == $channel_name) {
                    $final_bill_country = $bill_country_val;
                }
            }
        }

        /* For shipping */

        if (!empty($connected_ship_street)) {
            foreach ($connected_ship_street as $ship_street_key => $ship_street_val) {
                if ($ship_street_key == $channel_name) {
                    $final_ship_street = $ship_street_val;
                }
            }
        }


        if (!empty($connected_ship_street2)) {
            foreach ($connected_ship_street2 as $ship_street2_key => $ship_street2_val) {
                if ($ship_street2_key == $channel_name) {
                    $final_ship_street2 = $ship_street2_val;
                }
            }
        }

        if (!empty($connected_ship_city)) {
            foreach ($connected_ship_city as $ship_city_key => $ship_city_val) {
                if ($ship_city_key == $channel_name) {
                    $final_ship_city = $ship_city_val;
                }
            }
        }

        if (!empty($connected_ship_state)) {
            foreach ($connected_ship_state as $ship_state_key => $ship_state_val) {
                if ($ship_state_key == $channel_name) {
                    $final_ship_state = $ship_state_val;
                }
            }
        }

        if (!empty($connected_ship_zip)) {
            foreach ($connected_ship_zip as $ship_zip_key => $ship_zip_val) {
                if ($ship_zip_key == $channel_name) {
                    $final_ship_zip = $ship_zip_val;
                }
            }
        }

        if (!empty($connected_ship_country)) {
            foreach ($connected_ship_country as $ship_country_key => $ship_country_val) {
                if ($ship_country_key == $channel_name) {
                    $final_ship_country = $ship_country_val;
                }
            }
        }

        if (!empty($mul_store_id)) {
            $save_customer_abb_data = CustomerAbbrivation::find()->Where(['channel_abb_id' => $channel_abb_id, 'channel_accquired' => $channel_name, 'mul_store_id' => $mul_store_id])->one();
        } else {
            $save_customer_abb_data = CustomerAbbrivation::find()->Where(['channel_abb_id' => $channel_abb_id, 'channel_accquired' => $channel_name])->one();
        }

        if (!empty($save_customer_abb_data)) {
            $save_customer_abb_data->bill_street_1 = trim($final_bill_street);
            $save_customer_abb_data->bill_street_2 = trim($final_bill_street2);
            $save_customer_abb_data->bill_city = trim($final_bill_city);
            $save_customer_abb_data->bill_state = trim($final_bill_state);
            $save_customer_abb_data->bill_zip = trim($final_bill_zip);
            $save_customer_abb_data->bill_country = trim($final_bill_country);
            $save_customer_abb_data->ship_street_1 = trim($final_ship_street);
            $save_customer_abb_data->ship_street_2 = trim($final_ship_street2);
            $save_customer_abb_data->ship_city = trim($final_ship_city);
            $save_customer_abb_data->ship_state = trim($final_ship_state);
            $save_customer_abb_data->ship_zip = trim($final_ship_zip);
            $save_customer_abb_data->ship_country = trim($final_ship_country);

            $save_customer_abb_data->save(false);
        }
    }

    public function actionGetCountry() {
        $query = isset($_REQUEST['query']) ? $_REQUEST['query'] : $_REQUEST['term'];
        $sql = "SELECT name FROM countries WHERE name LIKE '{$query}%'";
        $countries = Countries::findBySql($sql)->all();
        $cat_array = array();
        foreach ($countries as $cat) {
            $cat_array[] = $cat->name;
        }
        //RETURN JSON ARRAY
        return json_encode($cat_array);
    }

    public function actionGetCities() {
        $query = isset($_REQUEST['query']) ? $_REQUEST['query'] : $_REQUEST['term'];
        $sql = "SELECT name FROM cities WHERE name LIKE '{$query}%'";
        $cities = Cities::findBySql($sql)->all();
        $cat_array = array();
        foreach ($cities as $cat) {
            $cat_array[] = $cat->name;
        }
        //RETURN JSON ARRAY
        return json_encode($cat_array);
    }

    public function actionGetStates() {
        $query = isset($_REQUEST['query']) ? $_REQUEST['query'] : $_REQUEST['term'];
        $sql = "SELECT name FROM states WHERE name LIKE '{$query}%'";
        $states = States::findBySql($sql)->all();
        $cat_array = array();
        foreach ($states as $cat) {
            $cat_array[] = $cat->name;
        }
        //RETURN JSON ARRAY
        return json_encode($cat_array);
    }

    /**
     * Deletes an existing CustomerUser model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CustomerUser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustomerUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = CustomerUser::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

//Create Customer Hook

    public function actionCreateCustomerHook($id) {
        $get_big_id_users = User::find()->where(['id' => $id])->one();
        $config = yii\helpers\ArrayHelper::merge(
                        require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_big_id_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);

        $webhookContent = "";
        $webhook = fopen('php://input', 'rb');
        while (!feof($webhook)) {
            $webhookContent .= fread($webhook, 4096);
        }
//        $fwrite = fopen($_SERVER['DOCUMENT_ROOT']."/bigcommerce_callback_cat.php", "w");
//        fwrite($fwrite, $webhookContent);
//        fclose($fwrite);
//        die;

        $data = json_decode($webhookContent);
        $data_id = $data->data->id;
        //$data_id=4;
        // Get api details from db
        $Stores_data = Stores::find()->Where(['store_name' => 'BigCommerce'])->with('storeconnection')->one();
        $big_client_id = $Stores_data->storeconnection->big_client_id;
        $auth_token = $Stores_data->storeconnection->big_access_token;
        $big_store_hash = $Stores_data->storeconnection->big_store_hash;

        Bigcommerce::configure(array(
            'client_id' => $big_client_id,
            'auth_token' => $auth_token,
            'store_hash' => $big_store_hash
        ));

        $ping = Bigcommerce::getTime();
        echo $ping->format('H:i:s');

        if ($data->data->type == "customer" && $data->scope == "store/customer/created"):

            $customer_detail = Bigcommerce::getResource('/customers/' . $data_id);
            $email = $customer_detail->email;
            // Customer Duplicacy check
            $customer_email_check = CustomerUser::find()->Where(['email' => $email])->one();
            if (empty($customer_email_check)) :
                if (!empty($customer_detail)):
                    $Customers_model = new CustomerUser();
                    $Customers_model->channel_abb_id = 'BGC' . $data_id;
                    $Customers_model->first_name = isset($customer_detail->first_name) ? $customer_detail->first_name : "";
                    $Customers_model->last_name = isset($customer_detail->last_name) ? $customer_detail->last_name : "";
                    $Customers_model->email = isset($customer_detail->email) ? $customer_detail->email : "";
                    $Customers_model->channel_acquired = 'BigCommerce';
                    $Customers_model->date_acquired = date('Y-m-d h:i:s', strtotime($customer_detail->date_created));
                    $Customers_model->phone_number = isset($customer_detail->phone) ? $customer_detail->phone : "";
                    $Customers_model->created_at = date('Y-m-d h:i:s', time());
                    //Save Elliot User id
                    $Customers_model->elliot_user_id = $id;
                    if ($Customers_model->save(false)) :

                        $fwrite = fopen($_SERVER['DOCUMENT_ROOT'] . '/bigcommerce_callback_cat.php', "w") or die("Unable to open file!");
                        fwrite($fwrite, "save");
                        fclose($fwrite);
                    else :
                        $fwrite = fopen($_SERVER['DOCUMENT_ROOT'] . '/bigcommerce_callback_cat.php', "w") or die("Unable to open file!");
                        fwrite($fwrite, "not save");
                        fclose($fwrite);
                    endif;
                endif;
            endif;

            // Save Customer Address
            //Get Customer Add in BigCommerce
            $customers_add = Bigcommerce::getCollection('/customers/' . $data_id . '/addresses', 'Address');
            if (!empty($customers_add)) :
                $customerdata = CustomerUser::find()->Where(['email' => $email])->one();
                // Save customer Address
                $customerdata->street_1 = isset($customers_add[0]->street_1) ? $customers_add[0]->street_1 : "";
                $customerdata->street_2 = isset($customers_add[0]->street_2) ? $customers_add[0]->street_2 : "";
                $customerdata->city = isset($customers_add[0]->city) ? $customers_add[0]->city : "";
                $customerdata->country = isset($customers_add[0]->country) ? $customers_add[0]->country : "";
                $customerdata->state = isset($customers_add[0]->state) ? $customers_add[0]->state : "";
                $customerdata->zip = isset($customers_add[0]->zip) ? $customers_add[0]->zip : "";
                $customerdata->address_type = isset($customers_add[0]->address_type) ? $customers_add[0]->address_type : "";
                if ($customerdata->save(false)):
                endif;

            endif;
        endif;

        // Customer Update BiGComemrce to Elliot
        if ($data->data->type == "customer" && $data->scope == "store/customer/updated"):

//            $fwrite = fopen($_SERVER['DOCUMENT_ROOT']."/bigcommerce_callback_cat.php", "w");
//            fwrite($fwrite, $webhookContent);
//            fclose($fwrite);
            $customer_detail = Bigcommerce::getResource('/customers/' . $data_id);
            $customers_add = Bigcommerce::getCollection('/customers/' . $data_id . '/addresses', 'Address');
            $ab_id = "BGC" . $data_id;
            $customer_update = CustomerUser::find()->Where(['channel_abb_id' => $ab_id])->one();
            $customer_update->first_name = isset($customer_detail->first_name) ? $customer_detail->first_name : "";
            $customer_update->last_name = isset($customer_detail->last_name) ? $customer_detail->last_name : "";
            $customer_update->email = isset($customer_detail->email) ? $customer_detail->email : "";
            $customer_update->phone_number = isset($customer_detail->phone) ? $customer_detail->phone : "";

            //update add
            $customer_update->street_1 = isset($customers_add[0]->street_1) ? $customers_add[0]->street_1 : "";
            $customer_update->street_2 = isset($customers_add[0]->street_2) ? $customers_add[0]->street_2 : "";
            $customer_update->city = isset($customers_add[0]->city) ? $customers_add[0]->city : "";
            $customer_update->country = isset($customers_add[0]->country) ? $customers_add[0]->country : "";
            $customer_update->state = isset($customers_add[0]->state) ? $customers_add[0]->state : "";
            $customer_update->zip = isset($customers_add[0]->zip) ? $customers_add[0]->zip : "";
            $customer_update->address_type = isset($customers_add[0]->address_type) ? $customers_add[0]->address_type : "";

            $customer_update->save(false);

        endif;
        // Customer Deleted BiGComemrce to Elliot
        if ($data->data->type == "customer" && $data->scope == "store/customer/deleted"):

            $ab_id = "BGC" . $data_id;
            $delete = CustomerUser::find()->Where(['channel_abb_id' => $ab_id])->one();
            $delete->delete();

        endif;
    }

    public function actionOrderhookcreate($id) {


        $get_big_id_users = User::find()->where(['id' => $id])->one();

        $config = yii\helpers\ArrayHelper::merge(
                        require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_big_id_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);

        $Stores_data = Stores::find()->Where(['store_name' => 'BigCommerce'])->with('storeconnection')->one();
        $big_client_id = $Stores_data->storeconnection->big_client_id;
        $auth_token = $Stores_data->storeconnection->big_access_token;
        $big_store_hash = $Stores_data->storeconnection->big_store_hash;

        Bigcommerce::configure(array(
            'client_id' => $big_client_id,
            'auth_token' => $auth_token,
            'store_hash' => $big_store_hash
        ));


        $ping = Bigcommerce::getTime();
//        $hookrs = Bigcommerce::listWebhooks();
        foreach ($hookrs as $hk) {
            Bigcommerce::deleteWebhook($hk->id);
        }
        $hookrs = Bigcommerce::listWebhooks();
        echo'<pre>';
        print_r($hookrs);
        die;
    }

    public function actionOrderbigcommercecreate($id, $connection_id) {

        $store_connection_id = $connection_id;
        $get_big_id_users = User::find()->where(['id' => $id])->one();

        $config = yii\helpers\ArrayHelper::merge(
                        require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_big_id_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);

        $webhookContent = "";
        $webhook = fopen('php://input', 'rb');
        while (!feof($webhook)) {
            $webhookContent .= fread($webhook, 4096);
        }

        $myfile = fopen($_SERVER['DOCUMENT_ROOT'] . "/bigcommerce_callback.php", "w") or die("Unable to open file!");
        fwrite($myfile, $webhookContent);
        fclose($myfile);

//   $webhookContent='{
//                    "scope": "store/order/statusUpdated",
//                    "store_id": "1023506",
//                    "data": {
//                      "type": "order",
//                      "id": 102,
//                      "status": {
//                        "previous_status_id": 1,
//                        "new_status_id": 10
//                      }
//                    },
//                    "hash": "914ae3ccbc4f70d1308a1ba5bdef4ddd0d4f1a32",
//                    "created_at": 1504775122,
//                    "producer": "stores/1iz6ni"
//                  }';

        $data = json_decode($webhookContent);
        $order_id = $data->data->id;
        // Get api details from db
        $Stores_data = StoresConnection::find()->where(['stores_connection_id' => $store_connection_id, 'user_id' => $id])->one();
        $bgc_store_id = $Stores_data->store_id;
        $big_client_id = $Stores_data->big_client_id;
        $auth_token = $Stores_data->big_access_token;
        $big_store_hash = $Stores_data->big_store_hash;

        Bigcommerce::configure(array(
            'client_id' => $big_client_id,
            'auth_token' => $auth_token,
            'store_hash' => $big_store_hash
        ));

        $ping = Bigcommerce::getTime();

        $Bgc_store_details = Bigcommerce::getStore();
        $country_code = $Bgc_store_details->country_code;
        $currency_code = $Bgc_store_details->currency;

        echo $ping->format('H:i:s');


        if (!empty($order_id)) {

            $orders_data = Bigcommerce::getResource('/orders/' . $order_id, 'Order');
            $ship_details = Bigcommerce::getCollection('/orders/' . $orders_data->id . '/shipping_addresses', 'Order');
            $bigcm_customer_id = $orders_data->customer_id;

            $customer_data = Bigcommerce::getResource('/customers/' . $bigcm_customer_id, 'Customer');
            $bigcm_customer_email = '';
            if (!empty($customer_data)) {
                $bigcm_customer_email = $customer_data->email;
            } else {
                $bigcm_customer_email = isset($orders_data->billing_address->email) ? $orders_data->billing_address->email : "";
            }
            $order_status = $orders_data->status;
            $product_qauntity = $orders_data->items_total;
            $total_amount = $orders_data->total_inc_tax;
            $base_shipping_cost = $orders_data->base_shipping_cost;
            $shipping_cost_tax = $orders_data->shipping_cost_tax;
            $base_handling_cost = $orders_data->base_handling_cost;
            $handling_cost_tax = $orders_data->handling_cost_tax;
            $base_wrapping_cost = $orders_data->base_wrapping_cost;
            $wrapping_cost_tax = $orders_data->wrapping_cost_tax;
            $payment_method = $orders_data->payment_method;
            $payment_provider_id = $orders_data->payment_provider_id;
            $payment_status = $orders_data->payment_status;
            $refunded_amount = $orders_data->refunded_amount;
            $discount_amount = $orders_data->discount_amount;
            $coupon_discount = $orders_data->coupon_discount;
            $order_date = date('Y-m-d H:i:s', strtotime($orders_data->date_created));
            $order_last_modified_date = date('Y-m-d H:i:s', strtotime($orders_data->date_modified));
            //billing Address
            $billing_add1 = isset($orders_data->billing_address->street_1) ? $orders_data->billing_address->street_1 : "" . ',' . isset($orders_data->billing_address->street_2) ? $orders_data->billing_address->street_2 : "";
            $billing_add2 = isset($orders_data->billing_address->city) ? $orders_data->billing_address->city : "" . ',' . isset($orders_data->billing_address->state) ? $orders_data->billing_address->state : "" .
                    ',' . isset($orders_data->billing_address->zip) ? $orders_data->billing_address->zip : "" . ',' . isset($orders_data->billing_address->country) ? $orders_data->billing_address->country : "";
            $billing_address = $billing_add1 . ',' . $billing_add2;

            //billing Address
            $bill_street_1 = isset($orders_data->billing_address->street_1) ? $orders_data->billing_address->street_1 : '';
            $bill_street_2 = isset($orders_data->billing_address->street_2) ? $orders_data->billing_address->street_2 : '';
            $bill_city = isset($orders_data->billing_address->city) ? $orders_data->billing_address->city : '';
            $bill_state = isset($orders_data->billing_address->state) ? $orders_data->billing_address->state : '';
            $bill_zip = isset($orders_data->billing_address->zip) ? $orders_data->billing_address->zip : '';
            $bill_country = isset($orders_data->billing_address->country) ? $orders_data->billing_address->country : '';
            $bill_country_iso = isset($orders_data->billing_address->country_iso2) ? $orders_data->billing_address->country_iso2 : '';
            $get_email = isset($orders_data->billing_address->email) ? $orders_data->billing_address->email : "";
            $get_phone = isset($orders_data->billing_address->phone) ? $orders_data->billing_address->phone : "";
            $bill_first_name = isset($orders_data->billing_address->first_name) ? $orders_data->billing_address->first_name : "";
            $bill_last_name = isset($orders_data->billing_address->last_name) ? $orders_data->billing_address->last_name : "";

            //Shipping Address
            $ship_street_1 = isset($ship_details[0]->street_1) ? $ship_details[0]->street_1 : $orders_data->billing_address->street_1;
            $ship_street_2 = isset($ship_details[0]->street_2) ? $ship_details[0]->street_2 : $orders_data->billing_address->street_2;
            $ship_city = isset($ship_details[0]->city) ? $ship_details[0]->city : $orders_data->billing_address->city;
            $ship_state = isset($ship_details[0]->state) ? $ship_details[0]->state : $orders_data->billing_address->state;
            $ship_zip = isset($ship_details[0]->zip) ? $ship_details[0]->zip : $orders_data->billing_address->zip;
            $ship_country = isset($ship_details[0]->country) ? $ship_details[0]->country : $orders_data->billing_address->country;
            $ship_country_iso = isset($ship_details[0]->country_iso2) ? $ship_details[0]->country_iso2 : $orders_data->billing_address->country_iso2;


            $order_product_data = array();
            $product_dtails = Bigcommerce::getCollection('/orders/' . $orders_data->id . '/products', 'Order');
            foreach ($product_dtails as $product_dtails_data) {
                $product_id = $product_dtails_data->product_id;
                $product_name = $product_dtails_data->name;
                $product_qty = $product_dtails_data->quantity;
                $order_product_sku = $product_dtails_data->sku;
                $price = $product_dtails_data->total_inc_tax;
                $product_weight = $product_dtails_data->weight;
                $order_product_data[] = array(
                    'product_id' => $product_id,
                    'name' => $product_name,
                    'sku' => $order_product_sku,
                    'price' => $price,
                    'qty_ordered' => $product_qty,
                    'weight' => $product_weight,
                );
            }
        }

        // Order Created bigcomemrce to elliot
        if ($data->data->type == "order" && $data->scope == "store/order/created" || $data->data->type == "order" && $data->scope == "store/order/statusUpdated") {
            $order_data = array(
                'order_id' => $order_id,
                'mul_store_id' => $store_connection_id, //Give multiple store id
                'mul_channel_id' => '', // Give multiple channel id
                'status' => $order_status,
                'magento_store_id' => '',
                'order_grand_total' => $total_amount,
                'customer_id' => $bigcm_customer_id,
                'customer_email' => $bigcm_customer_email,
                'order_shipping_amount' => $shipping_cost_tax,
                'order_tax_amount' => '',
                'total_qty_ordered' => $product_qauntity,
                'created_at' => $order_date,
                'updated_at' => $order_last_modified_date,
                'payment_method' => $payment_method,
                'refund_amount' => $refunded_amount,
                'discount_amount' => $discount_amount,
                'channel_store_name' => 'BigCommerce',
                'channel_store_prefix' => 'BGC',
                'elliot_user_id' => $id,
                'currency_code' => $currency_code,
                'store_id' => $bgc_store_id,
                'channel_id' => '',
                'shipping_address' => array(
                    'street_1' => $ship_street_1,
                    'street_2' => $ship_street_2,
                    'state' => $ship_state,
                    'city' => $ship_city,
                    'country' => $ship_country,
                    'country_id' => $ship_country_iso,
                    'postcode' => $ship_zip,
                    'email' => $get_email,
                    'telephone' => $get_phone,
                    'firstname' => '',
                    'lastname' => '',
                ),
                'billing_address' => array(
                    'street_1' => $bill_street_1,
                    'street_2' => $bill_street_2,
                    'state' => $bill_state,
                    'city' => $bill_city,
                    'country' => $bill_country,
                    'country_id' => $bill_country_iso,
                    'postcode' => $bill_zip,
                    'email' => $get_email,
                    'telephone' => $get_phone,
                    'firstname' => $bill_first_name,
                    'lastname' => $bill_last_name,
                ),
                'items' => $order_product_data,
            );
            Stores::orderImportingCommon($order_data);
        }
    }

    public function actionOrderbigcommerceupdate($id, $connection_id) {


        $store_connection_id = $connection_id;
        $get_big_id_users = User::find()->where(['id' => $id])->one();

        $config = yii\helpers\ArrayHelper::merge(
                        require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_big_id_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);

        $webhookContent = "";
        $webhook = fopen('php://input', 'rb');
        while (!feof($webhook)) {
            $webhookContent .= fread($webhook, 4096);
        }

        $myfile = fopen($_SERVER['DOCUMENT_ROOT'] . "/bigcommerce_callback.php", "w") or die("Unable to open file!");
        fwrite($myfile, $webhookContent);
        fclose($myfile);

//  $webhookContent='{
//                    "scope": "store/order/statusUpdated",
//                    "store_id": "1023506",
//                    "data": {
//                      "type": "order",
//                      "id": 102,
//                      "status": {
//                        "previous_status_id": 1,
//                        "new_status_id": 10
//                      }
//                    },
//                    "hash": "914ae3ccbc4f70d1308a1ba5bdef4ddd0d4f1a32",
//                    "created_at": 1504775122,
//                    "producer": "stores/1iz6ni"
//                  }';

        $data = json_decode($webhookContent);
        $order_id = $data->data->id;
        // Get api details from db
        $Stores_data = StoresConnection::find()->where(['stores_connection_id' => $store_connection_id, 'user_id' => $id])->one();
        $bgc_store_id = $Stores_data->store_id;
        $big_client_id = $Stores_data->big_client_id;
        $auth_token = $Stores_data->big_access_token;
        $big_store_hash = $Stores_data->big_store_hash;

        Bigcommerce::configure(array(
            'client_id' => $big_client_id,
            'auth_token' => $auth_token,
            'store_hash' => $big_store_hash
        ));

        $ping = Bigcommerce::getTime();

        $Bgc_store_details = Bigcommerce::getStore();
        $country_code = $Bgc_store_details->country_code;
        $currency_code = $Bgc_store_details->currency;

        echo $ping->format('H:i:s');


        if (!empty($order_id)) {

            $orders_data = Bigcommerce::getResource('/orders/' . $order_id, 'Order');
            $ship_details = Bigcommerce::getCollection('/orders/' . $orders_data->id . '/shipping_addresses', 'Order');
            $bigcm_customer_id = $orders_data->customer_id;

            $customer_data = Bigcommerce::getResource('/customers/' . $bigcm_customer_id, 'Customer');
            $bigcm_customer_email = '';
            if (!empty($customer_data)) {
                $bigcm_customer_email = $customer_data->email;
            } else {
                $bigcm_customer_email = isset($orders_data->billing_address->email) ? $orders_data->billing_address->email : "";
            }
            $order_status = $orders_data->status;
            $product_qauntity = $orders_data->items_total;
            $total_amount = $orders_data->total_inc_tax;
            $base_shipping_cost = $orders_data->base_shipping_cost;
            $shipping_cost_tax = $orders_data->shipping_cost_tax;
            $base_handling_cost = $orders_data->base_handling_cost;
            $handling_cost_tax = $orders_data->handling_cost_tax;
            $base_wrapping_cost = $orders_data->base_wrapping_cost;
            $wrapping_cost_tax = $orders_data->wrapping_cost_tax;
            $payment_method = $orders_data->payment_method;
            $payment_provider_id = $orders_data->payment_provider_id;
            $payment_status = $orders_data->payment_status;
            $refunded_amount = $orders_data->refunded_amount;
            $discount_amount = $orders_data->discount_amount;
            $coupon_discount = $orders_data->coupon_discount;
            $order_date = date('Y-m-d H:i:s', strtotime($orders_data->date_created));
            $order_last_modified_date = date('Y-m-d H:i:s', strtotime($orders_data->date_modified));
            //billing Address
            $billing_add1 = isset($orders_data->billing_address->street_1) ? $orders_data->billing_address->street_1 : "" . ',' . isset($orders_data->billing_address->street_2) ? $orders_data->billing_address->street_2 : "";
            $billing_add2 = isset($orders_data->billing_address->city) ? $orders_data->billing_address->city : "" . ',' . isset($orders_data->billing_address->state) ? $orders_data->billing_address->state : "" .
                    ',' . isset($orders_data->billing_address->zip) ? $orders_data->billing_address->zip : "" . ',' . isset($orders_data->billing_address->country) ? $orders_data->billing_address->country : "";
            $billing_address = $billing_add1 . ',' . $billing_add2;

            //billing Address
            $bill_street_1 = isset($orders_data->billing_address->street_1) ? $orders_data->billing_address->street_1 : '';
            $bill_street_2 = isset($orders_data->billing_address->street_2) ? $orders_data->billing_address->street_2 : '';
            $bill_city = isset($orders_data->billing_address->city) ? $orders_data->billing_address->city : '';
            $bill_state = isset($orders_data->billing_address->state) ? $orders_data->billing_address->state : '';
            $bill_zip = isset($orders_data->billing_address->zip) ? $orders_data->billing_address->zip : '';
            $bill_country = isset($orders_data->billing_address->country) ? $orders_data->billing_address->country : '';
            $bill_country_iso = isset($orders_data->billing_address->country_iso2) ? $orders_data->billing_address->country_iso2 : '';
            $get_email = isset($orders_data->billing_address->email) ? $orders_data->billing_address->email : "";
            $get_phone = isset($orders_data->billing_address->phone) ? $orders_data->billing_address->phone : "";
            $bill_first_name = isset($orders_data->billing_address->first_name) ? $orders_data->billing_address->first_name : "";
            $bill_last_name = isset($orders_data->billing_address->last_name) ? $orders_data->billing_address->last_name : "";

            //Shipping Address
            $ship_street_1 = isset($ship_details[0]->street_1) ? $ship_details[0]->street_1 : $orders_data->billing_address->street_1;
            $ship_street_2 = isset($ship_details[0]->street_2) ? $ship_details[0]->street_2 : $orders_data->billing_address->street_2;
            $ship_city = isset($ship_details[0]->city) ? $ship_details[0]->city : $orders_data->billing_address->city;
            $ship_state = isset($ship_details[0]->state) ? $ship_details[0]->state : $orders_data->billing_address->state;
            $ship_zip = isset($ship_details[0]->zip) ? $ship_details[0]->zip : $orders_data->billing_address->zip;
            $ship_country = isset($ship_details[0]->country) ? $ship_details[0]->country : $orders_data->billing_address->country;
            $ship_country_iso = isset($ship_details[0]->country_iso2) ? $ship_details[0]->country_iso2 : $orders_data->billing_address->country_iso2;


            $order_product_data = array();
            $product_dtails = Bigcommerce::getCollection('/orders/' . $orders_data->id . '/products', 'Order');
            foreach ($product_dtails as $product_dtails_data) {
                $product_id = $product_dtails_data->product_id;
                $product_name = $product_dtails_data->name;
                $product_qty = $product_dtails_data->quantity;
                $order_product_sku = $product_dtails_data->sku;
                $price = $product_dtails_data->total_inc_tax;
                $product_weight = $product_dtails_data->weight;
                $order_product_data[] = array(
                    'product_id' => $product_id,
                    'name' => $product_name,
                    'sku' => $order_product_sku,
                    'price' => $price,
                    'qty_ordered' => $product_qty,
                    'weight' => $product_weight,
                );
            }


            if ($data->data->type == "order" && $data->scope == "store/order/updated" || $data->data->type == "order" && $data->scope == "store/order/statusUpdated") {

                $order_data = array(
                    'order_id' => $order_id,
                    'mul_store_id' => $store_connection_id, //Give multiple store id
                    'mul_channel_id' => '', // Give multiple channel id
                    'status' => $order_status,
                    'magento_store_id' => '',
                    'order_grand_total' => $total_amount,
                    'customer_id' => $bigcm_customer_id,
                    'customer_email' => $bigcm_customer_email,
                    'order_shipping_amount' => $shipping_cost_tax,
                    'order_tax_amount' => '',
                    'total_qty_ordered' => $product_qauntity,
                    'created_at' => $order_date,
                    'updated_at' => $order_last_modified_date,
                    'payment_method' => $payment_method,
                    'refund_amount' => $refunded_amount,
                    'discount_amount' => $discount_amount,
                    'channel_store_name' => 'BigCommerce',
                    'channel_store_prefix' => 'BGC',
                    'elliot_user_id' => $id,
                    'currency_code' => $currency_code,
                    'store_id' => $bgc_store_id,
                    'channel_id' => '',
                    'shipping_address' => array(
                        'street_1' => $ship_street_1,
                        'street_2' => $ship_street_2,
                        'state' => $ship_state,
                        'city' => $ship_city,
                        'country' => $ship_country,
                        'country_id' => $ship_country_iso,
                        'postcode' => $ship_zip,
                        'email' => $get_email,
                        'telephone' => $get_phone,
                        'firstname' => '',
                        'lastname' => '',
                    ),
                    'billing_address' => array(
                        'street_1' => $bill_street_1,
                        'street_2' => $bill_street_2,
                        'state' => $bill_state,
                        'city' => $bill_city,
                        'country' => $bill_country,
                        'country_id' => $bill_country_iso,
                        'postcode' => $bill_zip,
                        'email' => $get_email,
                        'telephone' => $get_phone,
                        'firstname' => $bill_first_name,
                        'lastname' => $bill_last_name,
                    ),
                    'items' => $order_product_data,
                );
                Stores::orderUpdateCommon($order_data);
            }
        }
    }

    public function actionCategoryhooksbg($id) {

        $get_big_id_users = User::find()->where(['id' => $id])->one();
        $user_company = $get_big_id_users->company_name;
        $config = yii\helpers\ArrayHelper::merge(
                        require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_big_id_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);
        $webhookContent = "";

        $webhook = fopen('php://input', 'rb');
        while (!feof($webhook)) {
            $webhookContent .= fread($webhook, 4096);
        }

        $data = json_decode($webhookContent);
        $hook_action_id = $data->data->id;
        //        $hook_action_id=128;

        $Stores_data = Stores::find()->Where(['store_name' => 'BigCommerce'])->with('storeconnection')->one();
        $bigcommerce_store_id = $Stores_data->store_id;

        $Stores_connection = Stores::find()->Where(['store_name' => 'BigCommerce'])->with('storeconnection')->one();
        $big_client_id = $Stores_connection->storeconnection->big_client_id;
        $auth_token = $Stores_connection->storeconnection->big_access_token;
        $big_store_hash = $Stores_connection->storeconnection->big_store_hash;

        Bigcommerce::configure(array(
            'client_id' => $big_client_id,
            'auth_token' => $auth_token,
            'store_hash' => $big_store_hash
        ));


        if ($data->data->type == "category" && ($data->scope == "store/category/updated" || $data->scope == "store/category/created")) {

            $cat = Bigcommerce::getCategory($data->data->id);
            $ab_id = "BGC" . $data->data->id;

            $model = Categories::find()->Where(['channel_abb_id' => $ab_id])->one();

            if (!empty($model)) {
                $model->category_name = $cat->name;

                $model->save(false);
                if ($cat->parent_id) {
                    $parent_id = "BGC" . $cat->parent_id;
                    $parent = Categories::find()->Where(['channel_abb_id' => $parent_id])->one();
                    if (empty($parent)) {
                        $parent->category_name = $cat->name;
                        $parent->channel_abb_id = $parent_id;
                        $parent->elliot_user_id = $id;
                        $created_date = date('Y-m-d h:i:s', time());
                        $parent->created_at = $created_date;
                        $parent->save(false);
                        $parent_id_e = $parent->category_ID;

                        $model = Categories::find()->Where(['channel_abb_id' => $ab_id])->one();
                        $model->parent_category_ID = $parent_id_e;
                        $model->save(false);
                    } else {
                        $model->parent_category_ID = $parent->category_ID;
                        $model->save(false);
                    }
                } else {
                    $model->parent_category_ID = 0;
                    $model->save(false);
                }
            } else {
                $categoryModel = new Categories();
                $parent_id = "BGC" . $cat->parent_id;
                $parent = Categories::find()->Where(['channel_abb_id' => $parent_id])->one();
                $p_id = empty($parent) ? 0 : $parent->category_ID;
                $categoryModel->category_name = $cat->name;
                $categoryModel->channel_abb_id = $ab_id;
                $categoryModel->parent_category_ID = $p_id;
                $categoryModel->elliot_user_id = $id;
                $created_date = date('Y-m-d h:i:s', time());
                $categoryModel->created_at = $created_date;
                $categoryModel->save(false);
            }
        } elseif ($data->data->type == "category" && $data->scope == "store/category/deleted") {
            $ab_id = "BGC" . $data->data->id;
            $delete = Categories::find()->Where(['channel_abb_id' => $ab_id])->one();
            $delete->delete();
        }
    }

    public function actionProductskuhooksbg($id) {
        $users_Id = $id;
        $get_big_id_users = User::find()->where(['id' => $id])->one();
        $user_company = $get_big_id_users->company_name;
        $config = yii\helpers\ArrayHelper::merge(
                        require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_big_id_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);
        $webhookContent = "";
        $webhook = fopen('php://input', 'rb');
        while (!feof($webhook)) {
            $webhookContent .= fread($webhook, 4096);
        }
//        $webhookContent = '{
//  "scope": "store/product/updated",
//  "store_id": "1021879",
//  "data": {
//    "type": "product",
//    "id": 93
//  },
//  "hash": "6183c2a992f9a90887f1c5cee548f89160e3d66e",
//  "created_at": 1497525097,
//  "producer": "stores/9c4sjk3b"
//}';

        $myfile = fopen($_SERVER['DOCUMENT_ROOT'] . "/bigcommerce_callback.php", "w") or die("Unable to open file!");
        fwrite($myfile, $webhookContent);
        fclose($myfile);

        $data = json_decode($webhookContent);
        $hook_action_id = $data->data->id;
//        $hook_action_id=128;
        $Stores_data = Stores::find()->Where(['store_name' => 'BigCommerce'])->with('storeconnection')->one();
        $bigcommerce_store_id = $Stores_data->store_id;
        $Stores_connection = Stores::find()->Where(['store_name' => 'BigCommerce'])->with('storeconnection')->one();
        $big_client_id = $Stores_connection->storeconnection->big_client_id;
        $auth_token = $Stores_connection->storeconnection->big_access_token;
        $big_store_hash = $Stores_connection->storeconnection->big_store_hash;

        Bigcommerce::configure(array(
            'client_id' => $big_client_id,
            'auth_token' => $auth_token,
            'store_hash' => $big_store_hash
        ));

        $hooks = Bigcommerce::listWebhooks();
        echo "<pre>";
        print_r($hooks);
    }

    //update skus from Bigcommerce to Elliot
    public function actionSkuupdategb($id) {

        $users_Id = $id;
        $get_big_id_users = User::find()->where(['id' => $id])->one();
        $user_company = $get_big_id_users->company_name;
        $config = yii\helpers\ArrayHelper::merge(
                        require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_big_id_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);
        $webhookContent = '';
        $webhook = fopen('php://input', 'rb');
        while (!feof($webhook)) {
            $webhookContent .= fread($webhook, 4096);
        }

        $myfile = fopen($_SERVER['DOCUMENT_ROOT'] . "/bigcommerce_callback.php", "w") or die("Unable to open file!");
        fwrite($myfile, $webhookContent);
        fclose($myfile);

        $response = json_decode($webhookContent);

        $Stores_data = Stores::find()->Where(['store_name' => 'BigCommerce'])->with('storeconnection')->one();
        $bigcommerce_store_id = $Stores_data->store_id;
        $Stores_connection = Stores::find()->Where(['store_name' => 'BigCommerce'])->with('storeconnection')->one();
        $big_client_id = $Stores_connection->storeconnection->big_client_id;
        $auth_token = $Stores_connection->storeconnection->big_access_token;
        $big_store_hash = $Stores_connection->storeconnection->big_store_hash;

        Bigcommerce::configure(array(
            'client_id' => $big_client_id,
            'auth_token' => $auth_token,
            'store_hash' => $big_store_hash
        ));
        if ($response->data->type == "sku"):
            $p_id = $response->data->sku->product_id;
            $sku_id = $response->data->id;
            $product_sku = Bigcommerce::getResource("/products/" . $p_id . "/skus/" . $sku_id);
            $saved_product_id = '';
            $checkVarItemModel = ProductVariation::find()->where(['store_variation_id' => 'BGC' . $product_sku->id])->all();
            if (!empty($checkVarItemModel)):
                foreach ($checkVarItemModel as $chk):
                    $chk->delete();
                    $saved_product_id = $chk->product_id;
                endforeach;
            endif;
            $users_Id = $id;

            if (isset($product_sku->sku)):
                $get_variation_sku_al = ProductVariation::find()->where(['store_variation_id' => 'BGC' . $product_sku->id, 'item_name' => 'SKU'])->one();
//            $get_variation_sku_al = ProductVariation::find()->where(['store_variation_id' => 'BGC72', 'item_name' => 'SKU'])->one();
                if (empty($get_variation_sku_al)):
                    $newP_Var_Model1 = new ProductVariation;
                    $newP_Var_Model1->product_id = $saved_product_id;
                    $newP_Var_Model1->elliot_user_id = $users_Id;
                    $newP_Var_Model1->item_name = 'SKU';
                    $newP_Var_Model1->item_value = $product_sku->sku;
                    $newP_Var_Model1->store_variation_id = 'BGC' . $product_sku->id;
                    $created_date = date('Y-m-d H:i:s', time());
                    $newP_Var_Model1->created_at = $created_date;
                    $newP_Var_Model1->save(false);
                endif;

            endif;

            //UPC
            if (isset($product_sku->upc)):
                $newP_Var_Model2 = new ProductVariation;
                $newP_Var_Model2->product_id = $saved_product_id;
                $newP_Var_Model2->elliot_user_id = $users_Id;
                $newP_Var_Model2->item_name = 'UPC';
                $newP_Var_Model2->item_value = $product_sku->upc;
                $newP_Var_Model2->store_variation_id = 'BGC' . $product_sku->id;
                $created_date = date('Y-m-d H:i:s', time());
                $newP_Var_Model2->created_at = $created_date;
                $newP_Var_Model2->save(false);
            endif;
            //Price
            if (isset($product_sku->adjusted_price)):
                $newP_Var_Model3 = new ProductVariation;
                $newP_Var_Model3->product_id = $saved_product_id;
                $newP_Var_Model3->elliot_user_id = $users_Id;
                $newP_Var_Model3->item_name = 'Price';
                $newP_Var_Model3->item_value = $product_sku->adjusted_price;
                $newP_Var_Model3->store_variation_id = 'BGC' . $product_sku->id;
                $created_date = date('Y-m-d H:i:s', time());
                $newP_Var_Model3->created_at = $created_date;
                $newP_Var_Model3->save(false);
            endif;
            //Inventory
            if (isset($product_sku->inventory_level)):
                $newP_Var_Model4 = new ProductVariation;
                $newP_Var_Model4->product_id = $saved_product_id;
                $newP_Var_Model4->elliot_user_id = $users_Id;
                $newP_Var_Model4->item_name = 'Inventory';
                $newP_Var_Model4->item_value = $product_sku->inventory_level;
                $newP_Var_Model4->store_variation_id = 'BGC' . $product_sku->id;
                $created_date = date('Y-m-d H:i:s', time());
                $newP_Var_Model4->created_at = $created_date;
                $newP_Var_Model4->save(false);
            endif;
            //Weight
            if (isset($product_sku->adjusted_weight)):
                $newP_Var_Model5 = new ProductVariation;
                $newP_Var_Model5->product_id = $saved_product_id;
                $newP_Var_Model5->elliot_user_id = $users_Id;
                $newP_Var_Model5->item_name = 'Weight';
                $newP_Var_Model5->item_value = $product_sku->adjusted_weight;
                $newP_Var_Model5->store_variation_id = 'BGC' . $product_sku->id;
                $created_date = date('Y-m-d H:i:s', time());
                $newP_Var_Model5->created_at = $created_date;
                $newP_Var_Model5->save(false);
            endif;
            //is_purchasing_disabled
            if (isset($product_sku->is_purchasing_disabled)):
                $newP_Var_Model6 = new ProductVariation;
                $newP_Var_Model6->product_id = $saved_product_id;
                $newP_Var_Model6->elliot_user_id = $users_Id;
                $newP_Var_Model6->item_name = 'Purchase_disable';
                $newP_Var_Model6->item_value = $product_sku->is_purchasing_disabled;
                $newP_Var_Model6->store_variation_id = 'BGC' . $product_sku->id;
                $created_date = date('Y-m-d H:i:s', time());
                $newP_Var_Model6->created_at = $created_date;
                $newP_Var_Model6->save(false);
            endif;
            //is_purchasing_disabled_msg
            if (isset($product_sku->purchasing_disabled_message)):
                $newP_Var_Model7 = new ProductVariation;
                $newP_Var_Model7->product_id = $saved_product_id;
                $newP_Var_Model7->elliot_user_id = $users_Id;
                $newP_Var_Model7->item_name = 'Purchase_disable_message';
                $newP_Var_Model7->item_value = $product_sku->purchasing_disabled_message;
                $newP_Var_Model7->store_variation_id = 'BGC' . $product_sku->id;
                $created_date = date('Y-m-d H:i:s', time());
                $newP_Var_Model7->created_at = $created_date;
                $newP_Var_Model7->save(false);
            endif;
            //image_file
            if (isset($product_sku->image_file)):
                $newP_Var_Model8 = new ProductVariation;
                $newP_Var_Model8->product_id = $saved_product_id;
                $newP_Var_Model8->elliot_user_id = $users_Id;
                $newP_Var_Model8->item_name = 'Image';
                $newP_Var_Model8->item_value = $product_sku->image_file;
                $newP_Var_Model8->store_variation_id = 'BGC' . $product_sku->id;
                $created_date = date('Y-m-d H:i:s', time());
                $newP_Var_Model8->created_at = $created_date;
                $newP_Var_Model8->save(false);
            endif;
            //Variants 
            if (isset($product_sku->options)):
                foreach ($product_sku->options as $option):
                    $var_op_id = $option->product_option_id;
                    $op_object = Bigcommerce::getProductOption($p_id, $var_op_id);
                    if (!empty($op_object)):
                        $var_op_val_id = $option->option_value_id;
                        $op_val_object = Bigcommerce::getOptionValue($op_object->option_id, $var_op_val_id);
                        if (!empty($op_val_object)):
                            $Pop_object = Bigcommerce::getOption($op_object->option_id);
                            if (!empty($Pop_object)):
                                $get_variation = Variations::find()->select('variations_ID')->where(['variation_name' => $Pop_object->name, 'elliot_user_id' => $users_Id])->one();
                                if (!empty($get_variation)):
                                    $get_variation_already = ProductVariation::find()->where(['store_variation_id' => 'BGC' . $product_sku->id, 'item_name' => $Pop_object->name])->one();
                                    if (empty($get_variation_already)):
//                                        echo "<pre>";
//                                        print_r($Pop_object);

                                        $newP_Var_Model9 = new ProductVariation;
                                        $newP_Var_Model9->product_id = $saved_product_id;
                                        $newP_Var_Model9->elliot_user_id = $users_Id;
                                        $newP_Var_Model9->variation_id = $get_variation->variations_ID;
                                        $newP_Var_Model9->item_name = $Pop_object->name;
                                        $newP_Var_Model9->item_value = $op_val_object->label;
                                        $newP_Var_Model9->store_variation_id = 'BGC' . $product_sku->id;
                                        $created_date = date('Y-m-d H:i:s', time());
                                        $newP_Var_Model9->created_at = $created_date;
                                        $newP_Var_Model9->save(false);
                                    endif;

                                endif;
                            endif;
                        endif;
                    endif;
                endforeach;
            endif;
        endif;
    }

    //update products from Bigcommerce to Elliot
    public function actionProducthooksbg($id, $connection_id) {

        $users_Id = $id;
        $store_connection_id = $connection_id;

        $get_big_id_users = User::find()->where(['id' => $id])->one();
        $user_company = $get_big_id_users->company_name;
        $config = yii\helpers\ArrayHelper::merge(
                        require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_big_id_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);

        /* For Web Hook Content */
        $webhookContent = "";
        $webhook = fopen('php://input', 'rb');
        while (!feof($webhook)) {
            $webhookContent .= fread($webhook, 4096);
        }

        $myfile = fopen($_SERVER['DOCUMENT_ROOT'] . "/bigcommerce_callback.php", "w") or die("Unable to open file!");
        fwrite($myfile, $webhookContent);
        fclose($myfile);

        $data = json_decode($webhookContent);
        $hook_action_id = $data->data->id;
        //$hook_action_id = 122;

        $Stores_connection = StoresConnection::find()->where(['stores_connection_id' => $store_connection_id, 'user_id' => $users_Id])->one();
        $bigcommerce_store_id = $Stores_connection->store_id;
        $big_client_id = $Stores_connection->big_client_id;
        $auth_token = $Stores_connection->big_access_token;
        $big_store_hash = $Stores_connection->big_store_hash;

        Bigcommerce::configure(array(
            'client_id' => $big_client_id,
            'auth_token' => $auth_token,
            'store_hash' => $big_store_hash
        ));

        $Bgc_store_details = Bigcommerce::getStore();
        $country_code = $Bgc_store_details->country_code;
        $currency_code = $Bgc_store_details->currency;
        $bgcdoamin = $Bgc_store_details->domain;


        if (!empty($hook_action_id)) {

            $product = Bigcommerce::getProduct($hook_action_id);
            /* Get Bigcoomerce product according to hook id */

            if (!empty($product)) {
                $p_id = $product->id;
                $p_name = $product->name;
                //Give prefix big Commerce Product id
                $prefix_product_id = 'BGC' . $p_id;
                $p_sku = $product->sku;
                $p_upc = $product->upc;
                $p_des = $product->description;
                $p_avail = $product->availability;
                if ($p_avail == 'available' || $p_avail == 'preorder') {
                    $product_status = 1;
                } else {
                    $product_status = 0;
                }

                $p_brand = ucfirst($user_company);
                $product_url = $bgcdoamin . $product->custom_url;
                //$product->brand_id;
                //$product->brand; //object
                $p_weight = $product->weight;
                $p_price = $product->price;
                $p_saleprice = $product->sale_price;
                /* For if sale price empty null or 0 then price value is  sale price value */
                if ($p_saleprice == '' || $p_saleprice == Null || $p_saleprice == 0) {
                    $p_saleprice = $p_price;
                }
                $p_visibility = $product->is_visible;
                $p_stk_lvl = $product->inventory_level;
                $p_stk_warning_lvl = $product->inventory_warning_level;
                if ($p_stk_warning_lvl == '' || $p_stk_warning_lvl == 0 || $p_stk_warning_lvl == Null) {
                    $p_stk_warning_lvl = 5;
                }
                $p_stk_track = $product->inventory_tracking;
                $p_sale = $product->total_sold;
                $p_condition = $product->condition;
                //Fields which are required but not avaialable @Bigcommerce
                $p_ean = '';
                $p_jan = '';
                $p_isbn = '';
                $p_mpn = '';
                $p_created_date = date('Y-m-d H:i:s', strtotime($product->date_created));
                $p_updated_date = date('Y-m-d H:i:s', strtotime($product->date_modified));

                $product_categories_ids = $product->categories;

                $product_image_data = array();
                if (!empty($product->images)) {
                    foreach ($product->images as $_image) {
                        $p_image_link = $_image->standard_url;
                        $p_image_label = $_image->description;
                        $p_image_priority = $_image->sort_order;
                        $p_image_created_date = date('Y-m-d H:i:s', strtotime($_image->date_created));
                        $p_image_updated_date = date('Y-m-d H:i:s', strtotime($_image->date_created));

                        $product_image_data[] = array(
                            'image_url' => $p_image_link,
                            'label' => $p_image_label,
                            'position' => $p_image_priority,
                            'base_img' => $p_image_link,
                        );
                    }
                }
            }
            /* End Get Bigcoomerce product according to hook id */
        }

        if ($data->data->type == "product" && $data->scope == "store/product/created") {
            /* For Product Create */
            /* Object for Bigcommerce create product in elliot */
            $product_data = array(
                'product_id' => $p_id, // Stores/Channel product ID
                'name' => $p_name, // Product name
                'sku' => $p_sku, // Product SKU
                'description' => $p_des, // Product Description
                'product_url_path' => $product_url, // Product url if null give blank value
                'weight' => $p_weight, // Product weight if null give blank value
                'status' => $product_status, // Product status (visible or hidden). Give in 0 or 1 form (0 = hidden, 1 = visible).
                'price' => $p_price, // Porduct price
                'sale_price' => $p_saleprice, // Product sale price if null give Product price value
                'qty' => $p_stk_lvl, //Product quantity 
                'stock_status' => $p_visibility, // Product stock status ("in stock" or "out of stock"). 
                // Give in 0 or 1 form (0 = out of stock, 1 = in stock)
                'websites' => array(), //This is for only magento give only and blank array
                'brand' => $p_brand, // Product brand if any
                'low_stock_notification' => $p_stk_warning_lvl, // Porduct low stock notification if any otherwise give default 5 value
                'created_at' => $p_created_date, // Product created at date format date('Y-m-d H:i:s')
                'updated_at' => $p_updated_date, // Product updated at date format date('Y-m-d H:i:s')
                'mul_store_id' => $store_connection_id, //Give multiple store id
                'mul_channel_id' => '', // Give multiple channel id
                'channel_store_name' => 'BigCommerce', // Channel or Store name
                'channel_store_prefix' => 'BGC', // Channel or store prefix id
                'elliot_user_id' => $users_Id, // Elliot user id
                'store_id' => $bigcommerce_store_id, // if your are importing store give store id
                'channel_id' => '', // if your are importing channel give channel id
                'country_code' => $country_code, // store or channel country code
                'currency_code' => $currency_code,
                'upc' => $p_upc, // Product barcode if any
                'ean' => '', // Product ean if any
                'jan' => '', // Product jan if any
                'isban' => '', // Product isban if any
                'mpn' => '', // Product mpn if any
                'categories' => $product_categories_ids, // Product categroy array. If null give a blank array 
                'images' => $product_image_data, // Product images data
            );
            Stores::productImportingCommon($product_data);
        }

        if ($data->data->type == "product" && $data->scope == "store/product/updated") {
            /* Object for Bigcommerce create product in elliot */
            $product_data = array(
                'product_id' => $p_id, // Stores/Channel product ID
                'name' => $p_name, // Product name
                'sku' => $p_sku, // Product SKU
                'description' => $p_des, // Product Description
                'product_url_path' => $product_url, // Product url if null give blank value
                'weight' => $p_weight, // Product weight if null give blank value
                'status' => $product_status, // Product status (visible or hidden). Give in 0 or 1 form (0 = hidden, 1 = visible).
                'price' => $p_price, // Porduct price
                'sale_price' => $p_saleprice, // Product sale price if null give Product price value
                'qty' => $p_stk_lvl, //Product quantity 
                'stock_status' => $p_visibility, // Product stock status ("in stock" or "out of stock"). 
                // Give in 0 or 1 form (0 = out of stock, 1 = in stock)
                'websites' => array(), //This is for only magento give only and blank array
                'brand' => $p_brand, // Product brand if any
                'low_stock_notification' => $p_stk_warning_lvl, // Porduct low stock notification if any otherwise give default 5 value
                'created_at' => $p_created_date, // Product created at date format date('Y-m-d H:i:s')
                'updated_at' => $p_updated_date, // Product updated at date format date('Y-m-d H:i:s')
                'mul_store_id' => $store_connection_id, //Give multiple store id
                'mul_channel_id' => '', // Give multiple channel id
                'channel_store_name' => 'BigCommerce', // Channel or Store name
                'channel_store_prefix' => 'BGC', // Channel or store prefix id
                'elliot_user_id' => $users_Id, // Elliot user id
                'store_id' => $bigcommerce_store_id, // if your are importing store give store id
                'channel_id' => '', // if your are importing channel give channel id
                'country_code' => $country_code, // store or channel country code
                'currency_code' => $currency_code,
                'upc' => $p_upc, // Product barcode if any
                'ean' => '', // Product ean if any
                'jan' => '', // Product jan if any
                'isban' => '', // Product isban if any
                'mpn' => '', // Product mpn if any
                'categories' => $product_categories_ids, // Product categroy array. If null give a blank array 
                'images' => $product_image_data, // Product images data
            );
            Stores::producUpdateCommon($product_data);
        }

        if ($data->data->type == "product" && $data->scope == "store/product/deleted") {
            /* For delete a product */
            $ab_id = "BGC" . $hook_action_id;
            $productModel = ProductAbbrivation::find()->Where(['channel_abb_id' => $ab_id, 'channel_accquired' => 'BigCommerce', 'mul_store_id' => $store_connection_id])->one();
            $product_id = $productModel->product_id;
            $product_channel = ProductChannel::find()->Where(['product_id' => $product_id])->count();
            if ($product_channel == 1) {
                $product_data = Products::find()->Where(['id' => $product_id])->one();
                $product_data->product_status = 'in_active';
                $product_data->save(false);
                $product_channel = ProductChannel::find()->Where(['product_id' => $product_id, 'store_id' => $store_connection_id])->one();
                $product_channel->status = 'no';
                $product_channel->save(false);
            } else {
                $product_channel = ProductChannel::find()->Where(['product_id' => $product_id, 'store_id' => $store_connection_id])->one();
                $product_channel->status = 'no';
                $product_channel->save(false);
            }
        }


//        if ($data->data->type == "product" && ($data->scope == "store/product/updated" || $data->scope == "store/product/created")) {
//            $product = Bigcommerce::getProduct($hook_action_id);
//            $ab_id = "BGC" . $product->id;
//            //$productModel = Products::find()->Where(['product_name' => $product->name, 'SKU' => $product->sku])->one();
//            $productModel = ProductAbbrivation::find()->Where(['channel_abb_id' => $ab_id, 'channel_accquired' => 'BigCommerce'])->one();
//
//
//            if (!empty($productModel)) {
//                $p_sku = $product->sku;
//                $p_upc = $product->upc;
//                $p_des = $product->description;
//                $p_avail = $product->availability;
//                // $p_brand = ucfirst($user_company);
//                $product->brand_id;
//                $product->brand; //object
//                $p_weight = $product->weight;
//                $p_price = $product->price;
//                $p_saleprice = $product->sale_price;
//                $p_visibility = $product->is_visible;
//                $p_stk_lvl = $product->inventory_level;
//                $p_stk_warning_lvl = $product->inventory_warning_level;
//                $p_stk_track = $product->inventory_tracking;
//                $p_sale = $product->total_sold;
//                $p_condition = $product->condition;
//                $p_created_date = date('Y-m-d h:i:s', strtotime($product->date_created));
//                $p_updated_date = date('Y-m-d h:i:s', strtotime($product->date_modified));
//
//                //$productModel->product_name = $product->name;
//                //$productModel->SKU = $p_sku;
//                //$productModel->UPC = $p_upc;
//                //$productModel->description = $p_des;
//                //End
//                /* if ($p_avail == 'available' || $p_avail == 'preorder'):
//                  $productModel->availability = 'In Stock';
//                  else:
//                  $productModel->availability = 'Out of Stock';
//                  endif; */
////                $productModel->brand = $p_brand;
////                if ($p_condition == ''):
////                    $productModel->condition = 'New';
////                else:
////                    $productModel->condition = $p_condition;
////                endif;
////                $productModel->weight = $p_weight;
//                //Option set(Variation Set id)
////                if (!empty($product->option_set)):
////                    $op_set_id = $product->option_set_id;
////                    $op_set_object = Bigcommerce::getOptionSet($op_set_id);
////                    $get_var_set_id = VariationsSet::find()->where(['variations_set_name' => $op_set_object->name, 'elliot_user_id' => $users_Id])->one();
////                    $option_set_id = $get_var_set_id->variations_set_id;
////                    $productModel->variations_set_id = $option_set_id;
////                endif;
//                //Inventory Fields Mapping
//                $productModel->stock_quantity = $p_stk_lvl;
//                if ($p_stk_lvl == 0):
//                    $productModel->stock_level = 'Out of Stock';
//                else:
//                    $productModel->stock_level = 'In Stock';
//                endif;
//                if ($p_stk_track == 'none'):
//                    $productModel->stock_status = 'Hidden';
//                else:
//                    $productModel->stock_status = 'Visible';
//                endif;
//                //                if ($p_stk_lvl < $p_stk_warning_lvl):
//                //                    $productModel->low_stock_notification = 2;
//                //                else:
//                //                    $productModel->low_stock_notification = $p_stk_warning_lvl;
//                //                endif;
//                $productModel->price = $p_price;
//                $productModel->salePrice = $p_saleprice;
//                $productModel->created_at = $p_created_date;
//                $productModel->updated_at = $p_updated_date;
//                //Save Elliot User id
//                //$productModel->elliot_user_id = $id;
//                $productModel->save(false);
//
//                $product_id = $productModel->id;
//
//                /* if (!empty($product->categories)) {
//                  $get_category = ProductCategories::deleteAll(['product_ID' => $product_id]);
//
//                  foreach ($product->categories as $key => $value) {
//                  $cat_ab_id = "BGC" . $value;
//
//                  //$get_category = Categories::find()->select('category_ID')->where(['channel_abb_id' => $cat_ab_id])->one();
//                  $get_category = Categories::find()->where(['channel_abb_id' => $cat_ab_id])->one();
//                  if (!empty($get_category)):
//                  $cat_id = $get_category->category_ID;
//                  //Create Model for Each new Product Category
//                  $productCategoryModel = new ProductCategories;
//                  $productCategoryModel->category_ID = $cat_id;
//                  $productCategoryModel->product_ID = $product_id;
//                  $productCategoryModel->created_at = $p_created_date;
//                  $productCategoryModel->updated_at = $p_updated_date;
//                  //Save Elliot User id
//                  $productCategoryModel->elliot_user_id = $id;
//                  $productCategoryModel->save(false);
//
//
//                  endif;
//                  }
//                  } */
//
//                /* if (!empty($product->images)) {
//
//                  $get_category = ProductImages::find()->where(['product_ID' => $product_id])->one();
//                  if (!empty($get_category)) {
//                  $get_category->delete();
//                  }
//
//                  foreach ($product->images as $product_image) {
//                  // echo '<Pre>';print_r*$product_image;
//                  $p_image_link = $product_image->standard_url;
//                  $p_image_label = $product_image->description;
//                  $p_image_priority = $product_image->sort_order;
//                  $p_image_created_date = date('Y-m-d h:i:s', strtotime($product_image->date_created));
//                  $p_image_updated_date = date('Y-m-d h:i:s', strtotime($product_image->date_created));
//                  //Create Model for Each new Product Image
//                  $productImageModel = new ProductImages;
//                  $productImageModel->product_ID = $product_id;
//                  $productImageModel->label = $p_image_label;
//                  $productImageModel->link = $p_image_link;
//                  $prior = $p_image_priority + 1;
//                  $productImageModel->priority = $prior;
//                  if ($prior == 1):
//                  $productImageModel->default_image = 'Yes';
//                  endif;
//                  $productImageModel->image_status = 1;
//                  $productImageModel->created_at = $p_image_created_date;
//                  $productImageModel->updated_at = $p_image_updated_date;
//                  $productImageModel->save(false);
//                  }
//                  } */
//            } else {
//                /* For categoreis */
//                if (!empty($product->categories)) :
//                    foreach ($product->categories as $key => $category):
//                        $bgc_cat_data = Bigcommerce::getResource('/categories/' . $category, 'Category');
//                        $cat_channel_abb_id = 'BGC' . $category;
//                        $cat_name = $bgc_cat_data->name;
//                        $cat_parent_id = $bgc_cat_data->parent_id;
//                        //Check whelther category with the same name available
//                        $checkModel = Categories::find()->where(['category_name' => $cat_name])->one();
//                        if (empty($checkModel)):
//                            //Create Model for each new category fetched
//                            $categoryModel = new Categories();
//                            $categoryModel->category_name = $cat_name;
//                            if ($cat_parent_id > 0):
//                                $get_parent = Bigcommerce::getCategory($cat_parent_id);
//                                $find_category = Categories::find()->select('category_ID')->where(['category_name' => $get_parent->name])->one();
//                                if (!empty($find_category)):
//                                    $cat_parent_id = $find_category->category_ID;
//                                endif;
//                                $categoryModel->channel_abb_id = '';
//                                $categoryModel->parent_category_ID = $cat_parent_id;
//                                $created_date = date('Y-m-d H:i:s', time());
//                                $categoryModel->created_at = $created_date;
//                                //Save Elliot User id 
//                                $categoryModel->elliot_user_id = $users_Id;
//                                if ($categoryModel->save()):
//                                    /*                                     * Save Category Id in Category Abbrivation table */
//                                    $CategoryAbbrivation = new CategoryAbbrivation();
//                                    $CategoryAbbrivation->channel_abb_id = $cat_channel_abb_id;
//                                    $CategoryAbbrivation->category_ID = $categoryModel->category_ID;
//                                    $CategoryAbbrivation->channel_accquired = 'BigCommerce';
//                                    $CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
//                                    $CategoryAbbrivation->save(false);
//
//                                endif;
//                            endif;
//                        else:
//                            /** If category is already exist in elliot then Save Category Id in Category Abbrivation table */
//                            $categorytAbbrivation_check = CategoryAbbrivation::find()->Where(['channel_abb_id' => $cat_channel_abb_id])->one();
//                            if (empty($categorytAbbrivation_check)):
//                                $CategoryAbbrivation = new CategoryAbbrivation();
//                                $CategoryAbbrivation->channel_abb_id = $cat_channel_abb_id;
//                                $CategoryAbbrivation->category_ID = $checkModel->category_ID;
//                                $CategoryAbbrivation->channel_accquired = 'BigCommerce';
//                                $CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
//                                $CategoryAbbrivation->save(false);
//                            endif;
//                        endif;
//                    endforeach;
//                endif;
//                /* End For categoreis */
//
//                /* Starts Product Save */
//                $p_sku = $product->sku;
//                $p_upc = $product->upc;
//                $p_des = $product->description;
//                $p_avail = $product->availability;
//                $p_brand = ucfirst($user_company);
//                //$product->brand_id;
//                //$product->brand; //object
//                $p_weight = $product->weight;
//                $p_price = $product->price;
//                $p_saleprice = $product->sale_price;
//                $p_visibility = $product->is_visible;
//                $p_stk_lvl = $product->inventory_level;
//                $p_stk_warning_lvl = $product->inventory_warning_level;
//                $p_stk_track = $product->inventory_tracking;
//                $p_sale = $product->total_sold;
//                $p_condition = $product->condition;
//                //Fields which are required but not avaialable @Bigcommerce
//                $p_ean = '';
//                $p_jan = '';
//                $p_isbn = '';
//                $p_mpn = '';
//                $p_created_date = date('Y-m-d h:i:s', strtotime($product->date_created));
//                $p_updated_date = date('Y-m-d h:i:s', strtotime($product->date_modified));
//
//                $new_productModel = new Products ();
//                //print_r($new_productModel); die;
//                $new_productModel->channel_abb_id = 'BGC' . $product->id;
//                $new_productModel->product_name = $product->name;
//                // echo "uuu"; die;
//                $new_productModel->SKU = $p_sku;
//                $new_productModel->UPC = $p_upc;
//                $new_productModel->EAN = $p_ean;
//                $new_productModel->Jan = $p_jan;
//                $new_productModel->ISBN = $p_isbn;
//                $new_productModel->MPN = $p_mpn;
//                $new_productModel->description = $p_des;
//                //End
//                if ($p_avail == 'available' || $p_avail == 'preorder'):
//                    $new_productModel->availability = 'In Stock';
//                else:
//                    $new_productModel->availability = 'Out of Stock';
//                endif;
//                $new_productModel->brand = $p_brand;
//                if ($p_condition == ''):
//                    $new_productModel->condition = 'New';
//                else:
//                    $new_productModel->condition = $p_condition;
//                endif;
//                $new_productModel->weight = $p_weight;
//                //Option set(Variation Set id)
//                if (!empty($product->option_set)):
//                    $op_set_id = $product->option_set_id;
//                    $op_set_object = Bigcommerce::getOptionSet($op_set_id);
//                    $get_var_set_id = VariationsSet::find()->where(['variations_set_name' => $op_set_object->name, 'elliot_user_id' => $users_Id])->one();
//                    $option_set_id = $get_var_set_id->variations_set_id;
//                    $new_productModel->variations_set_id = $option_set_id;
//                endif;
//                //Inventory Fields Mapping
//                $new_productModel->stock_quantity = $p_stk_lvl;
//                if ($p_stk_lvl == 0):
//                    $new_productModel->stock_level = 'Out of Stock';
//                else:
//                    $new_productModel->stock_level = 'In Stock';
//                endif;
//                if ($p_visibility == 0):
//                    $new_productModel->stock_status = 'Hidden';
//                else:
//                    $new_productModel->stock_status = 'Visible';
//                endif;
//                if ($p_stk_warning_lvl == '' || $p_stk_warning_lvl == 0 || $p_stk_warning_lvl == Null) :
//                    $p_stk_warning_lvl = 5;
//                endif;
//                $new_productModel->low_stock_notification = $p_stk_warning_lvl;
//                $new_productModel->price = $p_price;
//                $new_productModel->sales_price = $p_saleprice;
//                $new_productModel->created_at = $p_created_date;
//                $new_productModel->updated_at = $p_updated_date;
//                //Save Elliot User id
//                $new_productModel->elliot_user_id = $id;
//
//                if ($new_productModel->save(false)):
//                    /* Save Product Abbrivation Id */
//                    $product_abberivation = new ProductAbbrivation();
//                    $product_abberivation->channel_abb_id = 'BGC' . $product->id;
//                    $product_abberivation->product_id = $new_productModel->id;
//                    $product_abberivation->channel_accquired = 'BigCommerce';
//                    $product_abberivation->price = $p_price;
//                    $product_abberivation->salePrice = $p_saleprice;
//                    $product_abberivation->schedules_sales_date = date('Y-m-d', time());
//                    $product_abberivation->stock_quantity = $p_stk_lvl;
//                    $product_abberivation->allocate_inventory = 100;
//                    if ($p_stk_lvl == 0):
//                        $product_abberivation->stock_level = 'Out of Stock';
//                    else:
//                        $product_abberivation->stock_level = 'In Stock';
//                    endif;
//                    if ($p_visibility == 0):
//                        $product_abberivation->stock_status = 'Hidden';
//                    else:
//                        $product_abberivation->stock_status = 'Visible';
//                    endif;
//                    $product_abberivation->created_at = date('Y-m-d H:i:s', time());
//                    $product_abberivation->save(false);
//                endif;
//                $product_id = $new_productModel->id;
//
//                /* Starts For Product categories */
//                if (!empty($product->categories)):
//                    foreach ($product->categories as $key => $value):
//                        $category = CategoryAbbrivation::find()->where(['channel_abb_id' => 'BGC' . $value])->with('category')->one();
//                        if (!empty($category)):
//                            $cat_id = $category->category->category_ID;
//                            //Create Model for Each new Product Category
//                            $productCategoryModel = new ProductCategories;
//                            $productCategoryModel->category_ID = $cat_id;
//                            $productCategoryModel->product_ID = $product_id;
//                            $productCategoryModel->created_at = $p_created_date;
//                            $productCategoryModel->updated_at = $p_updated_date;
//                            //Save Elliot User id
//                            $productCategoryModel->elliot_user_id = $users_Id;
//                            $productCategoryModel->save(false);
//                        endif;
//                    endforeach;
//                endif;
//                /* End for product categories */
//
//                if (!empty($product->images)) {
//
//                    $get_category = ProductImages::find()->where(['product_ID' => $product_id])->one();
//                    if (!empty($get_category)) {
//                        $get_category->delete();
//                    }
//                    foreach ($product->images as $product_image) {
//                        // echo '<Pre>';print_r*$product_image;
//                        $p_image_link = $product_image->standard_url;
//                        $p_image_label = $product_image->description;
//                        $p_image_priority = $product_image->sort_order;
//                        $p_image_created_date = date('Y-m-d h:i:s', strtotime($product_image->date_created));
//                        $p_image_updated_date = date('Y-m-d h:i:s', strtotime($product_image->date_created));
//                        //Create Model for Each new Product Image
//                        $productImageModel = new ProductImages;
//                        $productImageModel->product_ID = $product_id;
//                        $productImageModel->label = $p_image_label;
//                        $productImageModel->link = $p_image_link;
//                        $prior = $p_image_priority + 1;
//                        $productImageModel->priority = $prior;
//                        if ($prior == 1):
//                            $productImageModel->default_image = 'Yes';
//                        endif;
//                        $productImageModel->image_status = 1;
//                        $productImageModel->created_at = $p_image_created_date;
//                        $productImageModel->updated_at = $p_image_updated_date;
//                        $productImageModel->save(false);
//                    }
//                }
//
//                //Product Channel/Store Table Entry
//                $productChannelModel = new ProductChannel;
//                $productChannelModel->store_id = $bigcommerce_store_id;
//                $productChannelModel->product_id = $product_id;
//                $productChannelModel->status = 'yes';
//                $productChannelModel->created_at = $p_created_date;
//                $productChannelModel->updated_at = $p_updated_date;
//                $productChannelModel->save(false);
//            }
//        }
    }

    /*
     * Shopify Porduct Create Hooks
     */

    public function actionShopifyProductCreate() {

        $webhookContent = "";
        $webhook = fopen('php://input', 'rb');
        while (!feof($webhook)) {
            $webhookContent .= fread($webhook, 4096);
        }
        $jqry = json_decode($webhookContent, true);
//        $content = json_encode($webhookContent);
//        $file = $_SERVER['DOCUMENT_ROOT'] . '/shopify.txt';
//        $fwrite = fopen($file, "w");
//        fwrite($fwrite, $content);
//        fclose($fwrite);

        $user_id = '';
        if (isset($_GET['id'])) {
            $user_id = $_GET['id'];
        }
        $store_connection_id = '';
        if (isset($_GET['connection_id'])) {
            $store_connection_id = $_GET['connection_id'];
        }
        $store_name = '';
        if (isset($_GET['store_name'])) {
            $store_name = $_GET['store_name'];
        } else {
            $store_name = 'Shopify';
        }

        $get_users = User::find()->select('domain_name,company_name,email')->where(['id' => $user_id])->one();
        if (!empty($get_users)) {
            $config = yii\helpers\ArrayHelper::merge(
                            require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
            );
            $application = new yii\web\Application($config);
            $get_shopify_id = Stores::find()->select('store_id')->where(['store_name' => $store_name])->one();
            $shopify_store_id = $get_shopify_id->store_id;
            $store_shopify = StoresConnection::find()->where(['stores_connection_id' => $store_connection_id, 'user_id' => $user_id])->one();
            if (!empty($store_shopify)) {
                $shopify_shop = $store_shopify->url;
                $shopify_api_key = $store_shopify->api_key;
                $shopify_api_password = $store_shopify->key_password;
                $shopify_shared_secret = $store_shopify->shared_secret;

                $id = $jqry['id'];
                $sc = new Shopify($shopify_shop, $shopify_api_password, $shopify_api_key, $shopify_shared_secret);
                $_product = $sc->call('GET', '/admin/products/' . $id . '.json');
                $product_id = $_product['id'];

                $title = $_product['title'];
                $description = $_product['body_html'];
                $product_handle = $_product['handle'];
                $product_url = 'https://' . $shopify_shop . '/products/' . $product_handle;
                $created_date = date('Y-m-d h:i:s', strtotime($_product['created_at']));
                $updated_date = date('Y-m-d h:i:s', strtotime($_product['updated_at']));
                /* Fields which are required but not avaialable @Shopify */
                $p_ean = '';
                $p_jan = '';
                $p_isbn = '';
                $p_mpn = '';

                $product_variants = $_product['variants'];
                $count = 1;
                foreach ($product_variants as $_variants) {
                    $variants_id = $_variants['id'];
                    $variants_price = $_variants['price'];
                    $variant_sku = ($_variants['sku'] == '') ? $variant_sku = '' : $_variants['sku'];
                    $variants_barcode = $_variants['barcode'];
                    if ($variants_barcode == '') {
                        $variants_barcode = '';
                    }
                    $variants_qty = $_variants['inventory_quantity'];
                    if ($variants_qty > 0) {
                        $stock_status = 1;
                    } else {
                        $stock_status = 0;
                    }
                    $variants_weight = $_variants['weight'];
                    $variants_created_at = $_variants['created_at'];
                    $variants_updated_at = $_variants['updated_at'];
                    $variants_title_value = $_variants['title'];

                    $product_images = $_product['images'];
                    $product_image_data = array();
                    if (count($product_images) > 0) {
                        foreach ($product_images as $_image) {
                            $image_id = $_image['id'];
                            $image_src = $_image['src'];
                            $image_position = $_image['position'];
                            $image_variant_id_array = $_image['variant_ids'];
                            $default_img_id = $_product['image']['id'];
                            if ($default_img_id == $image_id) {
                                $base_image = $_product['image']['src'];
                            }
                            $product_image_data[] = array(
                                'image_url' => $image_src,
                                'label' => '',
                                'position' => $image_position,
                                'base_img' => $base_image,
                            );
                        }
                    }

                    if ($count == 1) {
                        $store_id = Stores::find()->select('store_id')->where(['store_name' => $store_name])->one();
                        $store_details = $sc->call('GET', '/admin/shop.json');
                        $store_country_code = $store_details['country_code'];
                        $store_currency_code = $store_details['currency'];
                        $product_data = array(
                            'product_id' => $product_id, // Stores/Channel product ID
                            'name' => $title, // Product name
                            'sku' => $variant_sku, // Product SKU
                            'description' => $description, // Product Description
                            'product_url_path' => $product_url, // Product url if null give blank value
                            'weight' => $variants_weight, // Product weight if null give blank value
                            'status' => 1, // Product status (visible or hidden). Give in 0 or 1 form (0 = hidden, 1 = visible).
                            'price' => $variants_price, // Porduct price
                            'sale_price' => $variants_price, // Product sale price if null give Product price value
                            'qty' => $variants_qty, //Product quantity 
                            'stock_status' => $stock_status, // Product stock status ("in stock" or "out of stock"). 
                            // Give in 0 or 1 form (0 = out of stock, 1 = in stock)
                            'websites' => array(), //This is for only magento give only and blank array
                            'brand' => '', // Product brand if any
                            'low_stock_notification' => 5, // Porduct low stock notification if any otherwise give default 5 value
                            'created_at' => $created_date, // Product created at date format date('Y-m-d H:i:s')
                            'updated_at' => $updated_date, // Product updated at date format date('Y-m-d H:i:s')
                            'channel_store_name' => $store_name, // Channel or Store name
                            'channel_store_prefix' => 'SP', // Channel or store prefix id
                            'mul_store_id' => $store_connection_id, //Give multiple store id
                            'mul_channel_id' => '', // Give multiple channel id
                            'elliot_user_id' => $user_id, // Elliot user id
                            'store_id' => $store_id->store_id, // if your are importing store give store id
                            'channel_id' => '', // if your are importing channel give channel id
                            'country_code' => $store_country_code, // if your are importing channel give channel id
                            'currency_code' => $store_currency_code,
                            'upc' => $variants_barcode, // Product upc if any
                            'jan' => '', // Product jan if any
                            'isban' => '', // Product isban if any
                            'mpn' => '', // Product mpn if any
                            'categories' => array(), // Product categroy array. If null give a blank array 
                            'images' => $product_image_data, // Product images data
                        );
                        Stores::productImportingCommon($product_data);
                    }
                    $count++;
                }
            }
        }
    }

    public function actionShopifyProductUpdate() {
        $webhookContent = "";
        $webhook = fopen('php://input', 'rb');
        while (!feof($webhook)) {
            $webhookContent .= fread($webhook, 4096);
        }
        $content = json_encode($webhookContent);
        $file = $_SERVER['DOCUMENT_ROOT'] . '/bigcommerce_callback.php';
        $fwrite = fopen($file, "w");
        fwrite($fwrite, $content);
        fclose($fwrite);



        $user_id = '';
        if (isset($_GET['id'])) {
            $user_id = $_GET['id'];
        }

        $store_connection_id = '';
        if (isset($_GET['connection_id'])) {
            $store_connection_id = $_GET['connection_id'];
        }
        $store_name = '';
        if (isset($_GET['store_name'])) {
            $store_name = $_GET['store_name'];
        } else {
            $store_name = 'Shopify';
        }

        $get_users = User::find()->select('domain_name,company_name,email')->where(['id' => $user_id])->one();
        if (!empty($get_users)) {
            $config = yii\helpers\ArrayHelper::merge(
                            require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
            );
            $application = new yii\web\Application($config);

            $store_shopify = StoresConnection::find()->where(['stores_connection_id' => $store_connection_id, 'user_id' => $user_id])->one();
            if (!empty($store_shopify)) {
                $shopify_shop = $store_shopify->url;
                $shopify_api_key = $store_shopify->api_key;
                $shopify_api_password = $store_shopify->key_password;
                $shopify_shared_secret = $store_shopify->shared_secret;

                $sc = new Shopify($shopify_shop, $shopify_api_password, $shopify_api_key, $shopify_shared_secret);
                $store_details = $sc->call('GET', '/admin/shop.json');
                $store_country_code = $store_details['country_code'];
                $store_currency_code = $store_details['currency'];

                $_product = json_decode($webhookContent, true);
                $product_id = 'SP' . $_product['id'];
                $title = $_product['title'];
                $description = $_product['body_html'];
                $created_date = date('Y-m-d h:i:s', strtotime($_product['created_at']));
                $updated_date = date('Y-m-d h:i:s', strtotime($_product['updated_at']));
                $product_variants = $_product['variants'];
                $count = 1;
                foreach ($product_variants as $_variants) {
                    $variants_id = $_variants['id'];
                    $variants_price = $_variants['price'];
                    $variant_sku = ($_variants['sku'] == '') ? $variant_sku = '' : $_variants['sku'];
                    $variants_barcode = $_variants['barcode'];
                    $variants_qty = $_variants['inventory_quantity'];
                    $variants_weight = $_variants['weight'];
                    $variants_created_at = $_variants['created_at'];
                    $variants_updated_at = $_variants['updated_at'];
                    $variants_title_value = $_variants['title'];
                    if ($count == 1) {
                        $productAbbrivation = ProductAbbrivation::find()->Where(['channel_abb_id' => $product_id, 'channel_accquired' => $store_name, 'mul_store_id' => $store_connection_id])->one();
                        if (!empty($productAbbrivation)) {
                            $conversion_rate = Stores::getCurrencyConversionRate($store_currency_code, 'USD');
                            $product_price = number_format((float) $conversion_rate * $variants_price, 2, '.', '');
                            $productAbbrivation->price = $product_price;
                            $productAbbrivation->salePrice = $product_price;
                            $productAbbrivation->stock_quantity = $variants_qty;
                            if ($variants_qty <= 0) {
                                $productAbbrivation->stock_level = 'Out of Stock';
                                $productAbbrivation->stock_status = 'Hidden';
                            }
                            $productAbbrivation->save(false);
                        }
                    }
                    $count++;
                }
            }
        }
    }

    public function actionShopifyProductDelete() {
        $webhookContent = "";
        $webhook = fopen('php://input', 'rb');
        while (!feof($webhook)) {
            $webhookContent .= fread($webhook, 4096);
        }
        $product = json_decode($webhookContent, true);
        //$content = json_encode($webhookContent);
        //$file = $_SERVER['DOCUMENT_ROOT'] . '/shopify.txt';
        //$fwrite = fopen($file, "w");
        //fwrite($fwrite, $content);
        //fclose($fwrite);

        $user_id = '';
        if (isset($_GET['id'])) {
            $user_id = $_GET['id'];
        }

        $store_connection_id = '';
        if (isset($_GET['connection_id'])) {
            $store_connection_id = $_GET['connection_id'];
        }
        $store_name = '';
        if (isset($_GET['store_name'])) {
            $store_name = $_GET['store_name'];
        } else {
            $store_name = 'Shopify';
        }

        $get_users = User::find()->select('domain_name,company_name,email')->where(['id' => $user_id])->one();
        if (!empty($get_users)) {
            $config = yii\helpers\ArrayHelper::merge(
                            require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
            );
            $application = new yii\web\Application($config);
            $shopify_product_id = 'SP' . $product['id'];

            $productModel = ProductAbbrivation::find()->Where(['channel_abb_id' => $shopify_product_id, 'channel_accquired' => $store_name, 'mul_store_id' => $store_connection_id])->one();
            if (!empty($productModel)) {
                $product_id = $productModel->product_id;
                //$product_channel = ProductChannel::find()->Where(['product_id' => $product_id, 'store_id' => $store_connection_id])->one();
                $product_model_check = Products::find()->Where(['id' => $product_id])->count();
                if ($product_model_check == 1) {
                    $product_model = Products::find()->Where(['id' => $product_id])->one();
                    $product_model->product_status = 'in_active';
                    $product_model->permanent_hidden = 'in_active';
                    $product_model->save(false);

                    $product_channel_model = ProductChannel::find()->Where(['product_id' => $product_id, 'store_id' => $store_connection_id])->one();
                    $product_channel_model->status = 'no';
                    $product_channel_model->save(false);
                } else {
                    $product_channel = ProductChannel::find()->Where(['product_id' => $product_id, 'store_id' => $store_connection_id])->one();
                    $product_channel->status = 'no';
                    $product_channel->save(false);
                }
            }
        }
    }

    public function actionShopifyOrderCreate() {
        $webhookContent = "";
        $webhook = fopen('php://input', 'rb');
        while (!feof($webhook)) {
            $webhookContent .= fread($webhook, 4096);
        }
        $_order = json_decode($webhookContent, true);
//        $content = json_encode($webhookContent);
//        $file = $_SERVER['DOCUMENT_ROOT'] . '/shopify.txt';
//        $fwrite = fopen($file, "w");
//        fwrite($fwrite, $content);
//        fclose($fwrite);

        $user_id = '';
        if (isset($_GET['id'])) {
            $user_id = $_GET['id'];
        }

        $store_connection_id = '';
        if (isset($_GET['connection_id'])) {
            $store_connection_id = $_GET['connection_id'];
        }

        $store_name = '';
        if (isset($_GET['store_name'])) {
            $store_name = $_GET['store_name'];
        } else {
            $store_name = 'Shopify';
        }

        $get_users = User::find()->select('domain_name,company_name,email')->where(['id' => $user_id])->one();
        if (!empty($get_users)) {
            $config = yii\helpers\ArrayHelper::merge(
                            require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
            );
            $application = new yii\web\Application($config);

            $get_shopify_id = Stores::find()->select('store_id')->where(['store_name' => $store_name])->one();
            $shopify_store_id = $get_shopify_id->store_id;
            $store_shopify = StoresConnection::find()->where(['stores_connection_id' => $store_connection_id, 'user_id' => $user_id])->one();
            if (!empty($store_shopify)) {
                $shopify_shop = $store_shopify->url;
                $shopify_api_key = $store_shopify->api_key;
                $shopify_api_password = $store_shopify->key_password;
                $shopify_shared_secret = $store_shopify->shared_secret;
                $sc = new Shopify($shopify_shop, $shopify_api_password, $shopify_api_key, $shopify_shared_secret);

                $store_details = $sc->call('GET', '/admin/shop.json');
                $store_country_code = $store_details['country_code'];
                $store_currency_code = $store_details['currency'];

                /** Order data */
                $order_id = $_order['id'];
                $customer_email = $_order['email'];
                $customer_id = $_order['customer']['id'];
                $order_created_at = $_order['created_at'];
                $order_updated_at = $_order['updated_at'];
                $payment_gateway = $_order['gateway'];
                $order_total = $_order['total_price'];
                $subtotal = $_order['subtotal_price'];
                $total_tax = $_order['total_tax'];
                $order_status = $_order['financial_status'];

                $total_discount = $_order['total_discounts'];
                $total_product_count = count($_order['line_items']);
                $base_shipping_cost = $_order['shipping_lines'][0]['price'];
                $refund = $_order['refunds'];
                $refund_amount = '';
                /* Refund total amount */
                foreach ($refund as $_refund) {
                    $refund_line_items = $_refund['refund_line_items'];
                    foreach ($refund_line_items as $_refund_items) {
                        $refund_amount += $_refund_items['subtotal'];
                    }
                }
                /* Order status change according to status */
                switch ($order_status) {
                    case "pending":
                        $order_status_2 = 'Pending';
                        break;
                    case "authorized":
                        $order_status_2 = "In Transit";
                        break;
                    case "partially_paid":
                        $order_status_2 = "Completed";
                        break;
                    case "paid":
                        $order_status_2 = "Completed";
                        break;
                    case "partially_refunded":
                        $order_status_2 = "Refunded";
                        break;
                    case "refunded":
                        $order_status_2 = "Refunded";
                        break;
                    case "voided":
                        $order_status_2 = "Cancel";
                        break;
                    default:
                        $order_status_2 = "Completed";
                }
                /** Order Billing Address */
                $order_billing_add = $_order['billing_address'];
                $billing_f_name = $order_billing_add['first_name'];
                $billing_l_name = $order_billing_add['last_name'];
                $billing_street_1 = $order_billing_add['address1'];
                $billing_street_2 = $order_billing_add['address2'];
                $billing_phone = $order_billing_add['phone'];
                $billing_city = $order_billing_add['city'];
                $billing_zip = $order_billing_add['zip'];
                $billing_state = $order_billing_add['province'];
                $billing_country = $order_billing_add['country'];
                $billing_country_code = $order_billing_add['country_code'];

                /** Order Shipping Address */
                $order_shipping_add = $_order['shipping_address'];
                $shipping_f_name = $order_shipping_add['first_name'];
                $shipping_l_name = $order_shipping_add['last_name'];
                $shipping_street_1 = $order_shipping_add['address1'];
                $shipping_street_2 = $order_shipping_add['address2'];
                $shipping_phone = $order_shipping_add['phone'];
                $shipping_city = $order_shipping_add['city'];
                $shipping_zip = $order_shipping_add['zip'];
                $shipping_state = $order_shipping_add['province'];
                $shipping_country = $order_shipping_add['country'];
                $shipping_country_code = $order_shipping_add['country_code'];
                $shipping_address = $shipping_street_1 . "," . $shipping_street_2 . "," . $shipping_city . "," . $shipping_state . "," . $shipping_zip . "," . $shipping_country . ",";
                $billing_address = $billing_street_1 . "," . $billing_street_2 . "," . $billing_city . "," . $billing_state . "," . $billing_zip . "," . $billing_country . ",";

                /* Order items */
                $order_items = $_order['line_items'];
                $order_product_data = array();
                foreach ($order_items as $_items) {
                    $title = $_items['title'];
                    $quantity = $_items['quantity'];
                    $price = $_items['price'];
                    $sku = '';
                    if ($_items['sku'] != "") {
                        $sku = $_items['sku'];
                    }
                    $product_id = ($_items['product_id'] == '') ? 0 : $_items['product_id'];
                    $product_weight = $_items['grams'];
                    $order_product_data[] = array(
                        'product_id' => $product_id,
                        'name' => $title,
                        'sku' => $sku,
                        'price' => $price,
                        'qty_ordered' => $quantity,
                        'weight' => $product_weight,
                    );
                }

                /* Check Customer exists or not */
                $check_customer = CustomerAbbrivation::find()->Where(['channel_abb_id' => 'SP' . $customer_id,])->one();
                if (empty($check_customer)) {
                    $_customer = $sc->call('GET', '/admin/customers/' . $customer_id . '.json');
                    $channel_name = $store_name;
                    $customer_id = $_customer['id'];
                    $Prefix_customer_abb_id = 'SP' . $customer_id;
                    $customer_email = isset($_customer['email']) ? $_customer['email'] : '';
                    $customer_f_name = isset($_customer['first_name']) ? $_customer['first_name'] : '';
                    $customer_l_name = isset($_customer['last_name']) ? $_customer['last_name'] : '';
                    $cus_phone = isset($_customer['phone']) ? $_customer['phone'] : '';
                    $customer_create_date = isset($_customer['created_at']) ? $_customer['created_at'] : time();
                    $customer_update_date = isset($_customer['updated_at']) ? $_customer['updated_at'] : time();
                    $add_1 = $add_2 = $city = $state = $country = $zip = $add_phone = '';
                    if (array_key_exists('default_address', $_customer)):
                        $add_1 = isset($_customer['default_address']['address1']) ? $_customer['default_address']['address1'] : '';
                        $add_2 = isset($_customer['default_address']['address2']) ? $_customer['default_address']['address2'] : '';
                        $city = isset($_customer['default_address']['city']) ? $_customer['default_address']['city'] : '';
                        $state = isset($_customer['default_address']['province']) ? $_customer['default_address']['province'] : '';
                        $country = isset($_customer['default_address']['country']) ? $_customer['default_address']['country'] : '';
                        $country_code = isset($_customer['default_address']['country_code']) ? $_customer['default_address']['country_code'] : '';
                        $zip = isset($_customer['default_address']['zip']) ? $_customer['default_address']['zip'] : '';
                        $add_phone = isset($_customer['default_address']['phone']) ? $_customer['default_address']['phone'] : '';
                    endif;
                    $phone = ($cus_phone == '') ? $add_phone : $cus_phone;
                    $customer_data = array(
                        'customer_id' => $customer_id,
                        'elliot_user_id' => $user_id,
                        'first_name' => $customer_f_name,
                        'last_name' => $customer_l_name,
                        'email' => $customer_email,
                        'channel_store_name' => $store_name,
                        'channel_store_prefix' => 'SP',
                        'mul_store_id' => $store_connection_id,
                        'mul_channel_id' => '',
                        'customer_created_at' => $customer_create_date,
                        'customer_updated_at' => $customer_update_date,
                        'billing_address' => array(
                            'street_1' => $add_1,
                            'street_2' => $add_2,
                            'city' => $city,
                            'state' => $state,
                            'country' => $country,
                            'country_iso' => $country_code,
                            'zip' => $zip,
                            'phone_number' => $phone,
                            'address_type' => 'Default',
                        ),
                        'shipping_address' => array(
                            'street_1' => '',
                            'street_2' => '',
                            'city' => '',
                            'state' => '',
                            'country' => '',
                            'zip' => '',
                        ),
                    );
                    Stores::customerImportingCommon($customer_data);
                }
                $check_order = Orders::find()->Where(['channel_abb_id' => 'SP' . $order_id])->one();
                if (empty($check_order)) {
                    $customer_id = ($customer_id == '') ? 0 : $customer_id;
                    $order_data = array(
                        'order_id' => $order_id,
                        'status' => $order_status_2,
                        'magento_store_id' => '',
                        'order_grand_total' => $order_total,
                        'customer_id' => $customer_id,
                        'customer_email' => $customer_email,
                        'order_shipping_amount' => $base_shipping_cost,
                        'order_tax_amount' => $total_tax,
                        'total_qty_ordered' => $total_product_count,
                        'created_at' => $order_created_at,
                        'updated_at' => $order_updated_at,
                        'payment_method' => $payment_gateway,
                        'refund_amount' => $refund_amount,
                        'discount_amount' => $total_discount,
                        'channel_store_name' => $store_name,
                        'channel_store_prefix' => 'SP',
                        'mul_store_id' => $store_connection_id,
                        'mul_channel_id' => '',
                        'elliot_user_id' => $user_id,
                        'store_id' => $get_shopify_id->store_id,
                        'currency_code' => $store_currency_code,
                        'import_or_create' => 'create',
                        'channel_id' => '',
                        'shipping_address' => array(
                            'street_1' => $shipping_street_1,
                            'street_2' => $shipping_street_2,
                            'state' => $shipping_state,
                            'city' => $shipping_city,
                            'country' => $shipping_country,
                            'country_id' => $shipping_country_code,
                            'postcode' => $shipping_zip,
                            'email' => $customer_email,
                            'telephone' => $shipping_phone,
                            'firstname' => $shipping_f_name,
                            'lastname' => $shipping_l_name,
                        ),
                        'billing_address' => array(
                            'street_1' => $billing_street_1,
                            'street_2' => $billing_street_2,
                            'state' => $billing_state,
                            'city' => $billing_city,
                            'country' => $billing_country,
                            'country_id' => $billing_country_code,
                            'postcode' => $billing_zip,
                            'email' => $customer_email,
                            'telephone' => $billing_phone,
                            'firstname' => $billing_f_name,
                            'lastname' => $billing_l_name,
                        ),
                        'items' => $order_product_data,
                    );
                    Stores::orderImportingCommon($order_data);
                }
            }
        }
    }

    public function actionShopifyOrderPaid() {
//        $webhookContent = "";
//        $webhook = fopen('php://input', 'rb');
//        while (!feof($webhook)) {
//            $webhookContent .= fread($webhook, 4096);
//        }
//        $_order = json_decode($webhookContent, true);
//        $content = json_encode($webhookContent);
//        $file = $_SERVER['DOCUMENT_ROOT'] . '/shopify_order_paid.txt';
//        $fwrite = fopen($file, "w");
//        fwrite($fwrite, $content);
//        fclose($fwrite);
    }

    public function actionShopifyOrderUpdate() {
        $webhookContent = "";
        $webhook = fopen('php://input', 'rb');
        while (!feof($webhook)) {
            $webhookContent .= fread($webhook, 4096);
        }
        $_order = json_decode($webhookContent, true);
//        $content = json_encode($webhookContent);
//        $file = $_SERVER['DOCUMENT_ROOT'] . '/shopify.txt';
//        $fwrite = fopen($file, "w");
//        fwrite($fwrite, $content);
//        fclose($fwrite);

        $user_id = '';
        if (isset($_GET['id'])) {
            $user_id = $_GET['id'];
        }

        $store_connection_id = '';
        if (isset($_GET['connection_id'])) {
            $store_connection_id = $_GET['connection_id'];
        }

        $store_name = '';
        if (isset($_GET['store_name'])) {
            $store_name = $_GET['store_name'];
        } else {
            $store_name = 'Shopify';
        }

        $get_users = User::find()->select('domain_name,company_name,email')->where(['id' => $user_id])->one();
        if (!empty($get_users)) {
            $config = yii\helpers\ArrayHelper::merge(
                            require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
            );
            $application = new yii\web\Application($config);

            $get_shopify_id = Stores::find()->select('store_id')->where(['store_name' => $store_name])->one();
            $shopify_store_id = $get_shopify_id->store_id;
            $store_shopify = StoresConnection::find()->where(['stores_connection_id' => $store_connection_id, 'user_id' => $user_id])->one();
            if (!empty($store_shopify)) {
                $shopify_shop = $store_shopify->url;
                $shopify_api_key = $store_shopify->api_key;
                $shopify_api_password = $store_shopify->key_password;
                $shopify_shared_secret = $store_shopify->shared_secret;
                $sc = new Shopify($shopify_shop, $shopify_api_password, $shopify_api_key, $shopify_shared_secret);

                $store_details = $sc->call('GET', '/admin/shop.json');
                $store_country_code = $store_details['country_code'];
                $store_currency_code = $store_details['currency'];

                /** Order data */
                $order_id = $_order['id'];
                $customer_email = $_order['email'];
                $customer_id = $_order['customer']['id'];
                $order_created_at = $_order['created_at'];
                $order_updated_at = $_order['updated_at'];
                $payment_gateway = $_order['gateway'];
                $order_total = $_order['total_price'];
                $subtotal = $_order['subtotal_price'];
                $total_tax = $_order['total_tax'];
                $order_status = $_order['financial_status'];
                $cancelled_at = $_order['cancelled_at'];
                if ($cancelled_at != '') {
                    $order_status = 'voided';
                }

                $total_discount = $_order['total_discounts'];
                $total_product_count = count($_order['line_items']);
                $base_shipping_cost = $_order['shipping_lines'][0]['price'];
                $refund = $_order['refunds'];
                $refund_amount = '';
                /** Refund total amount */
                foreach ($refund as $_refund) {
                    $refund_line_items = $_refund['refund_line_items'];
                    foreach ($refund_line_items as $_refund_items) {
                        $refund_amount += $_refund_items['subtotal'];
                    }
                }
                /** Order status change according to status */
                switch ($order_status) {
                    case "pending":
                        $order_status_2 = 'Pending';
                        break;
                    case "authorized":
                        $order_status_2 = "In Transit";
                        break;
                    case "partially_paid":
                        $order_status_2 = "Completed";
                        break;
                    case "paid":
                        $order_status_2 = "Completed";
                        break;
                    case "partially_refunded":
                        $order_status_2 = "Refunded";
                        break;
                    case "refunded":
                        $order_status_2 = "Refunded";
                        break;
                    case "voided":
                        $order_status_2 = "Cancel";
                        break;
                    default:
                        $order_status_2 = "Completed";
                }
                /** Order Billing Address */
                $order_billing_add = $_order['billing_address'];
                $billing_f_name = $order_billing_add['first_name'];
                $billing_l_name = $order_billing_add['last_name'];
                $billing_street_1 = $order_billing_add['address1'];
                $billing_street_2 = $order_billing_add['address2'];
                $billing_phone = $order_billing_add['phone'];
                $billing_city = $order_billing_add['city'];
                $billing_zip = $order_billing_add['zip'];
                $billing_state = $order_billing_add['province'];
                $billing_country = $order_billing_add['country'];
                $billing_country_code = $order_billing_add['country_code'];

                /** Order Shipping Address */
                $order_shipping_add = $_order['shipping_address'];
                $shipping_f_name = $order_shipping_add['first_name'];
                $shipping_l_name = $order_shipping_add['last_name'];
                $shipping_street_1 = $order_shipping_add['address1'];
                $shipping_street_2 = $order_shipping_add['address2'];
                $shipping_phone = $order_shipping_add['phone'];
                $shipping_city = $order_shipping_add['city'];
                $shipping_zip = $order_shipping_add['zip'];
                $shipping_state = $order_shipping_add['province'];
                $shipping_country = $order_shipping_add['country'];
                $shipping_country_code = $order_shipping_add['country_code'];
                $shipping_address = $shipping_street_1 . "," . $shipping_street_2 . "," . $shipping_city . "," . $shipping_state . "," . $shipping_zip . "," . $shipping_country . ",";
                $billing_address = $billing_street_1 . "," . $billing_street_2 . "," . $billing_city . "," . $billing_state . "," . $billing_zip . "," . $billing_country . ",";

                /* Order items */
                $order_items = $_order['line_items'];
                $order_product_data = array();
                foreach ($order_items as $_items) {
                    $title = $_items['title'];
                    $quantity = $_items['quantity'];
                    $price = $_items['price'];
                    $sku = '';
                    if ($_items['sku'] != "") {
                        $sku = $_items['sku'];
                    }
                    $product_id = ($_items['product_id'] == '') ? 0 : $_items['product_id'];
                    $product_weight = $_items['grams'];
                    $order_product_data[] = array(
                        'product_id' => $product_id,
                        'name' => $title,
                        'sku' => $sku,
                        'price' => $price,
                        'qty_ordered' => $quantity,
                        'weight' => $product_weight,
                    );
                }
                $order_model = Orders::find()->Where(['channel_abb_id' => "SP" . $order_id])->one();
                if (!empty($order_model)) {
                    $customer_id = ($customer_id == '') ? 0 : $customer_id;
                    $order_data = array(
                        'order_id' => $order_id,
                        'status' => $order_status_2,
                        'magento_store_id' => '',
                        'order_grand_total' => $order_total,
                        'customer_id' => $customer_id,
                        'customer_email' => $customer_email,
                        'order_shipping_amount' => $base_shipping_cost,
                        'order_tax_amount' => $total_tax,
                        'total_qty_ordered' => $total_product_count,
                        'created_at' => $order_created_at,
                        'updated_at' => $order_updated_at,
                        'payment_method' => $payment_gateway,
                        'refund_amount' => $refund_amount,
                        'discount_amount' => $total_discount,
                        'channel_store_name' => $store_name,
                        'channel_store_prefix' => 'SP',
                        'mul_store_id' => $store_connection_id,
                        'mul_channel_id' => '',
                        'elliot_user_id' => $user_id,
                        'store_id' => $get_shopify_id->store_id,
                        'currency_code' => $store_currency_code,
                        'import_or_create' => 'create',
                        'channel_id' => '',
                        'shipping_address' => array(
                            'street_1' => $shipping_street_1,
                            'street_2' => $shipping_street_2,
                            'state' => $shipping_state,
                            'city' => $shipping_city,
                            'country' => $shipping_country,
                            'country_id' => $shipping_country_code,
                            'postcode' => $shipping_zip,
                            'email' => $customer_email,
                            'telephone' => $shipping_phone,
                            'firstname' => $shipping_f_name,
                            'lastname' => $shipping_l_name,
                        ),
                        'billing_address' => array(
                            'street_1' => $billing_street_1,
                            'street_2' => $billing_street_2,
                            'state' => $billing_state,
                            'city' => $billing_city,
                            'country' => $billing_country,
                            'country_id' => $billing_country_code,
                            'postcode' => $billing_zip,
                            'email' => $customer_email,
                            'telephone' => $billing_phone,
                            'firstname' => $billing_f_name,
                            'lastname' => $billing_l_name,
                        ),
                        'items' => $order_product_data,
                    );
                    Stores::orderUpdateCommon($order_data);
                }
            }
        }
    }

    public function actionShopifyCustomerCreate() {
//        $webhookContent = "";
//        $webhook = fopen('php://input', 'rb');
//        while (!feof($webhook)) {
//            $webhookContent .= fread($webhook, 4096);
//        }
//        $_customer = json_decode($webhookContent, true);
//        $content = json_encode($webhookContent);
//        $file = $_SERVER['DOCUMENT_ROOT'] . '/shopify_customer_create.txt';
//        $fwrite = fopen($file, "w");
//        fwrite($fwrite, $content);
//        fclose($fwrite);
//
//        $user_id = $_GET['id'];
//        $get_users = User::find()->select('domain_name,company_name,email')->where(['id' => $user_id])->one();
//        $config = yii\helpers\ArrayHelper::merge(
//                        require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
//        );
//        $application = new yii\web\Application($config);
//
//        $channel_name = 'Shopify';
//        $customer_id = $_customer['id'];
//        $Prefix_customer_abb_id = 'sp-' . $customer_id;
//        $customer_email = $_customer['email'];
//        $customer_f_name = $_customer['first_name'];
//        $customer_l_name = $_customer['last_name'];
//        $customer_create_date = $_customer['created_at'];
//        $add_1 = $add_2 = $city = $state = $country = $zip = $phone = '';
//        if (array_key_exists('default_address', $_customer)) {
//            $add_1 = $_customer['default_address']['address1'];
//            $add_2 = $_customer['default_address']['address2'];
//            $city = $_customer['default_address']['city'];
//            $state = $_customer['default_address']['province'];
//            $country = $_customer['default_address']['country'];
//            $zip = $_customer['default_address']['zip'];
//            $phone = $_customer['default_address']['phone'];
//        }
//        $checkCustomerModel = CustomerUser::find()->where(['channel_abb_id' => $Prefix_customer_abb_id])->one();
//        if (empty($checkCustomerModel)) {
//            $Customers_model = new CustomerUser();
//            $Customers_model->channel_abb_id = $Prefix_customer_abb_id;
//            $Customers_model->first_name = $customer_f_name;
//            $Customers_model->last_name = $customer_l_name;
//            $Customers_model->email = $customer_email;
//            $Customers_model->channel_acquired = $channel_name;
//            $Customers_model->date_acquired = date('Y-m-d h:i:s', strtotime($customer_create_date));
//            $Customers_model->street_1 = $add_1;
//            $Customers_model->street_2 = $add_2;
//            $Customers_model->city = $city;
//            $Customers_model->country = $country;
//            $Customers_model->state = $state;
//            $Customers_model->zip = $zip;
//            $Customers_model->phone_number = $phone;
//            $Customers_model->created_at = date('Y-m-d h:i:s', time());
//            //Save Elliot User id
//            $Customers_model->elliot_user_id = $user_id;
//            $Customers_model->save(false);
//        }
    }

    public function actionShopifyCustomerUpdate() {
//        $webhookContent = "";
//        $webhook = fopen('php://input', 'rb');
//        while (!feof($webhook)) {
//            $webhookContent .= fread($webhook, 4096);
//        }
//        $_customer = json_decode($webhookContent, true);
//        $content = json_encode($webhookContent);
//        $file = $_SERVER['DOCUMENT_ROOT'] . '/shopify_customer_update.txt';
//        $fwrite = fopen($file, "w");
//        fwrite($fwrite, $content);
//        fclose($fwrite);
//
//        $user_id = $_GET['id'];
//        $get_users = User::find()->select('domain_name,company_name,email')->where(['id' => $user_id])->one();
//        $config = yii\helpers\ArrayHelper::merge(
//                        require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
//        );
//        $application = new yii\web\Application($config);
//
//        $channel_name = 'Shopify';
//        $customer_id = $_customer['id'];
//        $Prefix_customer_abb_id = 'sp-' . $customer_id;
//        $customer_email = $_customer['email'];
//        $customer_f_name = $_customer['first_name'];
//        $customer_l_name = $_customer['last_name'];
//        $customer_create_date = $_customer['created_at'];
//        $add_1 = $add_2 = $city = $state = $country = $zip = $phone = '';
//        if (array_key_exists('default_address', $_customer)) {
//            $add_1 = $_customer['default_address']['address1'];
//            $add_2 = $_customer['default_address']['address2'];
//            $city = $_customer['default_address']['city'];
//            $state = $_customer['default_address']['province'];
//            $country = $_customer['default_address']['country'];
//            $zip = $_customer['default_address']['zip'];
//            $phone = $_customer['default_address']['phone'];
//        }
//        $Customers_model = CustomerUser::find()->where(['channel_abb_id' => $Prefix_customer_abb_id])->one();
//        if (!empty($Customers_model)) {
//            $Customers_model->channel_abb_id = $Prefix_customer_abb_id;
//            $Customers_model->first_name = $customer_f_name;
//            $Customers_model->last_name = $customer_l_name;
//            $Customers_model->email = $customer_email;
//            $Customers_model->channel_acquired = $channel_name;
//            $Customers_model->date_acquired = date('Y-m-d h:i:s', strtotime($customer_create_date));
//            $Customers_model->street_1 = $add_1;
//            $Customers_model->street_2 = $add_2;
//            $Customers_model->city = $city;
//            $Customers_model->country = $country;
//            $Customers_model->state = $state;
//            $Customers_model->zip = $zip;
//            $Customers_model->phone_number = $phone;
//            //Save Elliot User id
//            $Customers_model->elliot_user_id = $user_id;
//            $Customers_model->save(false);
//        } else {
//            $Customers_model = new CustomerUser();
//            $Customers_model->channel_abb_id = $Prefix_customer_abb_id;
//            $Customers_model->first_name = $customer_f_name;
//            $Customers_model->last_name = $customer_l_name;
//            $Customers_model->email = $customer_email;
//            $Customers_model->channel_acquired = $channel_name;
//            $Customers_model->date_acquired = date('Y-m-d h:i:s', strtotime($customer_create_date));
//            $Customers_model->street_1 = $add_1;
//            $Customers_model->street_2 = $add_2;
//            $Customers_model->city = $city;
//            $Customers_model->country = $country;
//            $Customers_model->state = $state;
//            $Customers_model->zip = $zip;
//            $Customers_model->phone_number = $phone;
//            $Customers_model->created_at = date('Y-m-d h:i:s', time());
//            //Save Elliot User id
//            $Customers_model->elliot_user_id = $user_id;
//            $Customers_model->save(false);
//        }
    }

    public function actionSku() {


//          Bigcommerce::configure(array(
//            'client_id' => 'hg1xnpfdl4c04l0775xsrbkm30dbp0j',
//            'auth_token' => 'qmbir07sas0vhqboioa9hy1fnmrnz97',
//            'store_hash' => 'amnictxy47     '
//        ));

        Bigcommerce::configure(array(
            'client_id' => 'sr3a415jy5xdfmoom7bg5doovite511',
            'auth_token' => '4bak4uoxvyyu3yxfzgm48bvzl163wqp',
            'store_hash' => 'g8aum86c5z'
        ));

//        $products = Bigcommerce::getProducts();
//        $orders = Bigcommerce::getOrders();
//        foreach($orders as $o){
//            echo'<pre>';
//            print_r(Bigcommerce::getCollection('/orders/' . $o->id . '/products', 'Order'));
//        }
        $hooks = Bigcommerce::getCollection('/hooks');


        foreach ($hooks as $value) {
            $a = Bigcommerce::deleteResource('/hooks/' . $value->id);
        }
        echo'<pre>';
        print_r($hooks);
        die('herere');


        $woo_store_url = 'https://carrollandco.com/';
        $woo_consumer_key = 'ck_354b863ca9fa58d29bcdafeaf96c7f91429ebe9a';
        $woo_secret_key = 'cs_43a449e163ae8885dee311f7ba0ac592ef2262a5';

        $woocommerce = new Woocommerce(
                'https://carrollandco.com/', 'ck_354b863ca9fa58d29bcdafeaf96c7f91429ebe9a', 'cs_43a449e163ae8885dee311f7ba0ac592ef2262a5', [
            'wp_api' => true,
            'version' => 'wc/v1',
            "query_string_auth" => true,
                ]
        );


        $data = [
            'name' => 'Make2',
            'sku' => 'make2sku',
            'type' => 'simple',
            'regular_price' => '15.48',
            'sale_price' => '15.48',
            'description' => 'Type a description for this product here...',
            'categories' => [
                    [
                    'id' => 67
                ]
            ],
            'images' => [
            ]
        ];
        echo'<pre>';
        print_r($woocommerce->post('products', $data));
        die('end here');



//         $woocommerce = new Woocommerce(
//                'http://beta.brstdev.com/adrianna/', 'ck_d7dd231e1bd2232d1ba7692bfd68a0923ada3ddc', 'cs_03091b1664975069c150f91ac144e1af00b9a6ad', [
//            'wp_api' => true,
//            'version' => 'wc/v1',
//            "query_string_auth" => true,
//                ]
//        );

        $woocommerce_product_id = 10313;
        $stock_qty = 0;
        $woo_store_url = 'https://carrollandco.com/';
        $woo_consumer_key = 'ck_354b863ca9fa58d29bcdafeaf96c7f91429ebe9a';
        $woo_secret_key = 'cs_43a449e163ae8885dee311f7ba0ac592ef2262a5';

        $woocommerce = new Woocommerce(
                '' . $woo_store_url . '/wp-json/wc/v1/products/' . $woocommerce_product_id . '?stock_quantity=' . $stock_qty . '&consumer_key=' . $woo_consumer_key . '&consumer_secret=' . $woo_secret_key . '', $woo_consumer_key, $woo_secret_key, [
            'wp_api' => true,
            'version' => 'wc/v1',
            "query_string_auth" => true,
                ]
        );
        //       $woocommerce = new Woocommerce(
//                        '' . $woo_store_url . '/wp-json/wc/v1/products/' . $woocommerce_product_id . '?in_stock' . $stock . '&=regular_price=' . $price . '&stock_quantity=' . $stock_qty . '&sale_price=' . $salePrice . '&consumer_key=' . $woo_consumer_key . '&consumer_secret=' . $woo_secret_key . '', $woo_consumer_key, $woo_secret_key, [
//                    'wp_api' => true,
//                    'version' => 'wc/v1',
//                    "query_string_auth" => true,
//                        ]
//                );
//

        $data = [
            'stock_quantity' => 0
        ];

        $a = $woocommerce->put('products/10313', $data);


        echo'<pre>';
        print_r($a);
        die('hereee');


        $price = 396;
        $woocommerce_product_id = 1013;
        $woocommerce_store_url = 'https://fauxfreckles.com/';
        $woocommerce_consumer = 'ck_ffd450a6af544d3617de5a8c7a971fe8d4f7ac95';
        $woocommerce_secret = 'cs_85b8c62e9e6d5783675e33b584cbe9a1f3bab5aa';
        $woocommerce = new Woocommerce(
                '' . $woocommerce_store_url . '/wp-json/wc/v1/customers/?consumer_key=' . $woocommerce_consumer . '&consumer_secret=' . $woocommerce_secret . '', $woocommerce_consumer, $woocommerce_secret, [
            'wp_api' => true,
            'version' => 'wc/v1',
            "query_string_auth" => true,
                ]
        );

        $data = ['regular_price' => $price];

        $a = $woocommerce->put('products/' . $woocommerce_product_id . '', $data);
        echo'<pre>';
        print_r($a);
        die('end here');



//        $id=111;


        $woocommerce = new Woocommerce(
                'https://carrollandco.com/', 'ck_354b863ca9fa58d29bcdafeaf96c7f91429ebe9a', 'cs_43a449e163ae8885dee311f7ba0ac592ef2262a5', [
            'wp_api' => true,
            'version' => 'wc/v1',
                ]
        );


        $cust = $woocommerce->get('');
        //$cust =  $woocommerce->get('');
        echo'<pre>';
        print_r($cust);
        die('endd here');





//        $woocommerce = new Woocommerce(
//                'http://beta.brstdev.com/elliot',
//                'ck_4a1e35ec2816306b5d593ebfc0226cb4e0bb902a',
//                'cs_5b560aae249cebb6739f1a48e4e85fc4a679611c', [
//            'wp_api' => true,
//            'version' => 'wc/v1',
//                ]
//        );
//        $woocommerce = new Woocommerce(
//                'https://carrollandco.com/wp-json/wc/v1/products?consumer_key=ck_354b863ca9fa58d29bcdafeaf96c7f91429ebe9&consumer_secret=cs_43a449e163ae8885dee311f7ba0ac592ef2262a5', 'ck_354b863ca9fa58d29bcdafeaf96c7f91429ebe9', 'cs_43a449e163ae8885dee311f7ba0ac592ef2262a5', [
//            'wp_api' => true,
//            'version' => 'wc/v1',
//            "query_string_auth" => true,
//                ]
//        );
//        $woocommerce = new Woocommerce(
//                'https://carrollandco.com/wp-json/wc/v1/customers?consumer_key=ck_7c5e5068077ca218be2aea6211edc46fc5577902&consumer_secret=cs_21d3b7e515a213aff5a701563443f5a0981f9ee7',
//                'ck_7c5e5068077ca218be2aea6211edc46fc5577902',
//                'cs_21d3b7e515a213aff5a701563443f5a0981f9ee7', [
//            'wp_api' => true,
//            'version' => 'wc/v1',
//            "query_string_auth" => true,
//                ]
//        );
//        
//        
//        $woocommerce = new Woocommerce(
//                    
//                    'https://carrollandco.com/wp-json/wc/v1/products/categories/86?consumer_key=ck_354b863ca9fa58d29bcdafeaf96c7f91429ebe9a&consumer_secret=cs_43a449e163ae8885dee311f7ba0ac592ef2262a5', 
//                'ck_354b863ca9fa58d29bcdafeaf96c7f91429ebe9a', 
//                'cs_43a449e163ae8885dee311f7ba0ac592ef2262a5', [
//                'wp_api' => true,
//                'version' => 'wc/v1',
//                "query_string_auth" => true,
//                    ]
//            );
//        
//        //$data = ['per_page' => 100];
//        $data = ['page' => 1,'per_page'=>100];
//            $ab = $woocommerce->get('products/categories/86');
//
//            echo'<pre>';
//                print_r($ab);
//                die;


        $woocommerce = new Woocommerce(
                'http://beta.brstdev.com/adrianna/', 'ck_d7dd231e1bd2232d1ba7692bfd68a0923ada3ddc', 'cs_03091b1664975069c150f91ac144e1af00b9a6ad', [
            'wp_api' => true,
            'version' => 'wc/v1',
            "query_string_auth" => true,
                ]
        );


        $data = ['page' => 1, 'per_page' => 10];
        $orders_woocommerce = $woocommerce->get('orders', $data);
        echo'<pre>';
        print_r($orders_woocommerce);
        die('hereee');

//        $order_deleted = [
//                'name' => 'order created',
//                'topic' => 'product.created',
//                'secret' => 'cs_43a449e163ae8885dee311f7ba0ac592ef2262a5',
//                'delivery_url' => 'https://s86.co/customer-user/wc-order-delete-hook'
//          ];
//           
//           $a= $woocommerce->post('webhooks', $order_deleted);
        $hooks = $woocommerce->get('webhooks');
        echo'<pre>';
//           print_r($a);
        print_r($hooks);

        die;
    }

    /*     * ****************** WOO COMMERCE WEBHOOKS ************************* */

    /*
      WooCommerce Customer Create WebHook
     */

    public function actionWcCreateCustomerHook($id) {

//        echo $id;
//        die;
        // $id=649;
        $get_big_id_users = User::find()->where(['id' => $id])->one();


        $config = yii\helpers\ArrayHelper::merge(
                        require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_big_id_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);
        // Get api details from db
        $Stores_data = Stores::find()->Where(['store_name' => 'WooCommerce'])->with('storeconnection')->one();
        $woo_store_url = $Stores_data->storeconnection->woo_store_url;
        $woo_consumer_key = $Stores_data->storeconnection->woo_consumer_key;
        $woo_secret_key = $Stores_data->storeconnection->woo_secret_key;

        /* Read Webhook */
        $webhookContent = "";
        $webhook = fopen('php://input', 'rb');
        while (!feof($webhook)) {
            $webhookContent .= fread($webhook, 4096);
        }


        $data = json_decode($webhookContent);


        $file = $_SERVER['DOCUMENT_ROOT'] . '/bigcommerce_callback.php';
        $fwrite = fopen($file, "w");
        fwrite($fwrite, $webhookContent);
        fclose($fwrite);

        /* For Https server */
        $check_url = parse_url($woo_store_url);
        $url_protocol = $check_url['scheme'];
        if ($url_protocol == 'http'):

            $customer_id = $data->customer->id;
            /* For Http Url */
            $woocommerce = new Woocommerce(
                    $woo_store_url, $woo_consumer_key, $woo_secret_key, [
                'wp_api' => true,
                'version' => 'wc/v1',
                    ]
            );
        else:
            /* For Https Url */
            $customer_id = $data->id;
            $woocommerce = new Woocommerce(
                    '' . $woo_store_url . '/wp-json/wc/v1/customers/' . $customer_id . '/?consumer_key=' . $woo_consumer_key . '&consumer_secret=' . $woo_secret_key . '', $woo_consumer_key, $woo_secret_key, [
                'wp_api' => true,
                'version' => 'wc/v1',
                "query_string_auth" => true,
                    ]
            );

        endif;


        /* starts woocommerce webhook create customer */
        $_customer = $woocommerce->get('customers/' . $customer_id . '');
        $cus_id = $_customer['id'];
        $cus_created_at = $_customer['date_created'];
        $cus_updated_at = $_customer['date_modified'];
        $cus_email = $_customer['email'];
        $cus_f_name = $_customer['first_name'];
        $cus_l_name = $_customer['last_name'];
        $bill_street1 = $bill_street2 = $bill_city = $bill_state = $bill_country = $bill_phone = $bill_zip = '';
        if (isset($_customer['billing'])):

            $bill_street1 = $_customer['billing']['address_1'];
            $bill_street2 = $_customer['billing']['address_2'];
            $bill_city = $_customer['billing']['city'];
            $bill_state = $_customer['billing']['state'];
            $bill_country = $_customer['billing']['country'];
            $bill_phone = $_customer['billing']['phone'];
            $bill_zip = $_customer['billing']['postcode'];

        endif;
        $ship_street1 = $ship_street2 = $ship_city = $ship_state = $ship_country = $ship_zip = '';
        if (isset($_customer['shipping'])):

            $ship_street1 = $_customer['shipping']['address_1'];
            $ship_street2 = $_customer['shipping']['address_2'];
            $ship_city = $_customer['shipping']['city'];
            $ship_state = $_customer['shipping']['state'];
            $ship_country = $_customer['shipping']['country'];
            $ship_zip = $_customer['shipping']['postcode'];
        endif;

        $checkCustomerModel = CustomerUser::find()->where(['channel_abb_id' => 'WC_' . $cus_id])->one();
        if (empty($checkCustomerModel)):

            $Customers_model = new CustomerUser ();
            $Customers_model->channel_abb_id = 'WC_' . $cus_id;
            $Customers_model->first_name = $cus_f_name;
            $Customers_model->last_name = $cus_l_name;
            $Customers_model->email = $cus_email;
            $Customers_model->channel_acquired = 'WooCommerce';
            $Customers_model->date_acquired = date('Y-m-d h:i:s', strtotime($cus_created_at));
            $Customers_model->street_1 = $bill_street1;
            $Customers_model->street_2 = $bill_street2;
            $Customers_model->city = $bill_city;
            $Customers_model->country = $bill_country;
            $Customers_model->state = $bill_state;
            $Customers_model->zip = $bill_zip;
            $Customers_model->phone_number = $bill_phone;
            $Customers_model->ship_street_1 = $ship_street1;
            $Customers_model->ship_street_2 = $ship_street2;
            $Customers_model->ship_city = $ship_city;
            $Customers_model->ship_state = $ship_state;
            $Customers_model->ship_zip = $ship_zip;
            $Customers_model->ship_country = $ship_country;
            $Customers_model->created_at = date('Y-m-d h:i:s', strtotime($cus_created_at));
            $Customers_model->updated_at = date('Y-m-d h:i:s', strtotime($cus_updated_at));
            //Save Elliot User id
            $Customers_model->elliot_user_id = $id;
            //$Customers_model->save(false);
            $Customers_model->save(false);
        endif;
    }

    public function actionWcCustomerUpdateHook($id) {


        $get_big_id_users = User::find()->where(['id' => $id])->one();
        $config = yii\helpers\ArrayHelper::merge(
                        require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_big_id_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);
        // Get api details from db
        $Stores_data = Stores::find()->Where(['store_name' => 'WooCommerce'])->with('storeconnection')->one();
        $woo_store_url = $Stores_data->storeconnection->woo_store_url;
        $woo_consumer_key = $Stores_data->storeconnection->woo_consumer_key;
        $woo_secret_key = $Stores_data->storeconnection->woo_secret_key;

        /* Read Webhook */
        $webhookContent = "";
        $webhook = fopen('php://input', 'rb');
        while (!feof($webhook)) {
            $webhookContent .= fread($webhook, 4096);
        }
        $data = json_decode($webhookContent);

//        $file = $_SERVER['DOCUMENT_ROOT'].'/bigcommerce_callback.php';
//        $fwrite = fopen($file, "w");
//        fwrite($fwrite, $customer_id);
//        fclose($fwrite);

        /* For Https server */
        $check_url = parse_url($woo_store_url);
        $url_protocol = $check_url['scheme'];
        if ($url_protocol == 'http'):
            /* For Http Url */
            $customer_id = $data->customer->id;
            $woocommerce = new Woocommerce(
                    $woo_store_url, $woo_consumer_key, $woo_secret_key, [
                'wp_api' => true,
                'version' => 'wc/v1',
                    ]
            );
        else:
            /* For Https Url */
            $customer_id = $data->id;
            $woocommerce = new Woocommerce(
                    '' . $woo_store_url . '/wp-json/wc/v1/customers/' . $customer_id . '/?consumer_key=' . $woo_consumer_key . '&consumer_secret=' . $woo_secret_key . '', $woo_consumer_key, $woo_secret_key, [
                'wp_api' => true,
                'version' => 'wc/v1',
                "query_string_auth" => true,
                    ]
            );

        endif;


        /* starts woocommerce webhook Update customer */
        $_customer = $woocommerce->get('customers/' . $customer_id . '');
        $cus_id = $_customer['id'];
        $cus_created_at = $_customer['date_created'];
        $cus_updated_at = $_customer['date_modified'];
        $cus_email = $_customer['email'];
        $cus_f_name = $_customer['first_name'];
        $cus_l_name = $_customer['last_name'];
        $bill_street1 = $bill_street2 = $bill_city = $bill_state = $bill_country = $bill_phone = $bill_zip = '';
        if (isset($_customer['billing'])):

            $bill_street1 = $_customer['billing']['address_1'];
            $bill_street2 = $_customer['billing']['address_2'];
            $bill_city = $_customer['billing']['city'];
            $bill_state = $_customer['billing']['state'];
            $bill_country = $_customer['billing']['country'];
            $bill_phone = $_customer['billing']['phone'];
            $bill_zip = $_customer['billing']['postcode'];
        endif;
        $ship_street1 = $ship_street2 = $ship_city = $ship_state = $ship_country = $ship_zip = '';
        if (isset($_customer['shipping'])):

            $ship_street1 = $_customer['shipping']['address_1'];
            $ship_street2 = $_customer['shipping']['address_2'];
            $ship_city = $_customer['shipping']['city'];
            $ship_state = $_customer['shipping']['state'];
            $ship_country = $_customer['shipping']['country'];
            $ship_zip = $_customer['shipping']['postcode'];

        endif;

        $checkCustomerModel = CustomerUser::find()->where(['channel_abb_id' => 'WC_' . $cus_id])->one();
        if (!empty($checkCustomerModel)):

            $checkCustomerModel->channel_abb_id = 'WC_' . $cus_id;
            $checkCustomerModel->first_name = $cus_f_name;
            $checkCustomerModel->last_name = $cus_l_name;
            $checkCustomerModel->email = $cus_email;
            $checkCustomerModel->channel_acquired = 'WooCommerce';
            $checkCustomerModel->date_acquired = date('Y-m-d h:i:s', strtotime($cus_created_at));
            $checkCustomerModel->street_1 = $bill_street1;
            $checkCustomerModel->street_2 = $bill_street2;
            $checkCustomerModel->city = $bill_city;
            $checkCustomerModel->country = $bill_country;
            $checkCustomerModel->state = $bill_state;
            $checkCustomerModel->zip = $bill_zip;
            $checkCustomerModel->phone_number = $bill_phone;
            $checkCustomerModel->ship_street_1 = $ship_street1;
            $checkCustomerModel->ship_street_2 = $ship_street2;
            $checkCustomerModel->ship_city = $ship_city;
            $checkCustomerModel->ship_state = $ship_state;
            $checkCustomerModel->ship_zip = $ship_zip;
            $checkCustomerModel->ship_country = $ship_country;
            $checkCustomerModel->created_at = date('Y-m-d h:i:s', strtotime($cus_created_at));
            $checkCustomerModel->updated_at = date('Y-m-d h:i:s', strtotime($cus_updated_at));
            //Save Elliot User id
            $checkCustomerModel->elliot_user_id = $id;
            //$Customers_model->save(false);
            $checkCustomerModel->save(false);
        endif;
    }

    /* WooCommerce Webhook Customer Deleted  */

    public function actionWcCustomerDeleteHook($id) {


        $get_big_id_users = User::find()->where(['id' => $id])->one();
        $config = yii\helpers\ArrayHelper::merge(
                        require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_big_id_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);
        // Get api details from db
        $Stores_data = Stores::find()->Where(['store_name' => 'WooCommerce'])->with('storeconnection')->one();
        $woo_store_url = $Stores_data->storeconnection->woo_store_url;
        $woo_consumer_key = $Stores_data->storeconnection->woo_consumer_key;
        $woo_secret_key = $Stores_data->storeconnection->woo_secret_key;

        /* Read Webhook */
        $webhookContent = "";
        $webhook = fopen('php://input', 'rb');
        while (!feof($webhook)) {
            $webhookContent .= fread($webhook, 4096);
        }
        $data = json_decode($webhookContent);
        $file = $_SERVER['DOCUMENT_ROOT'] . '/bigcommerce_callback.php';
        $cus_id = $data->id;
//        $fwrite = fopen($file, "w");
//        fwrite($fwrite, $cus_id);
//        fclose($fwrite);

        $DeleteCustomer = CustomerUser::find()->where(['channel_abb_id' => 'WC_' . $cus_id])->one();
        $DeleteCustomer->delete();
    }

    /* WooCommerce Webhook Product created */

    public function actionWcProductCreateHook($id, $connection_id) {

        $store_connection_id = $connection_id;

        $get_big_id_users = User::find()->where(['id' => $id])->one();
        $config = yii\helpers\ArrayHelper::merge(
                        require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_big_id_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
        );

        $application = new yii\web\Application($config);

        // Get api details from db
        $store_woocommerce = StoresConnection::find()->where(['stores_connection_id' => $store_connection_id])->with('storesDetails')->one();

        $woo_store_url = $store_woocommerce->woo_store_url;
        $woo_consumer_key = $store_woocommerce->woo_consumer_key;
        $woo_secret_key = $store_woocommerce->woo_secret_key;
        $woocommerce_store_id = $store_woocommerce->store_id;
        $store_country_code = $store_woocommerce->storesDetails->country_code;
        $store_currency_code = $store_woocommerce->storesDetails->currency;

        /* Read Webhook */
        $webhookContent = "";
        $webhook = fopen('php://input', 'rb');
        while (!feof($webhook)) {
            $webhookContent .= fread($webhook, 4096);
        }
        $data = json_decode($webhookContent);

        $myfile = fopen($_SERVER['DOCUMENT_ROOT'] . "/bigcommerce_callback.php", "w") or die("Unable to open file!");
        fwrite($myfile, $webhookContent);
        fclose($myfile);

        /* For Https server */
        $check_url = parse_url($woo_store_url);
        $url_protocol = $check_url['scheme'];
        if ($url_protocol == 'http'):
            /* For Http Url */
            $product_id = $data->product->id;
            $woocommerce = new Woocommerce(
                    $woo_store_url, $woo_consumer_key, $woo_secret_key, [
                'wp_api' => true,
                'version' => 'wc/v1',
                    ]
            );
        else:
            /* For Https Url */
            $product_id = $data->id;
            $woocommerce = new Woocommerce(
                    '' . $woo_store_url . '/wp-json/wc/v1/products/' . $product_id . '/?consumer_key=' . $woo_consumer_key . '&consumer_secret=' . $woo_secret_key . '', $woo_consumer_key, $woo_secret_key, [
                'wp_api' => true,
                'version' => 'wc/v1',
                "query_string_auth" => true,
                    ]
            );

        endif;

        /* Woocommerce product  created */
        $product = $woocommerce->get('products/' . $product_id . '');
        $p_id = $product['id'];
        $p_name = $product['name'];
        //check for the Duplicate entry in Product Table
        $p_abb_id = $product['id'];
        //Give prefix big Commerce Product id
        $prefix_product_id = 'WC' . $p_abb_id;
        $p_sku = $product['sku'];
        $p_des = $product['description'];
        $product_status = $product['status'];
        if ($product_status == 'publish') {
            $p_status = 1;
        } else {
            $p_status = 0;
        }

        $product_url = $product['permalink'];
        $p_avail = $product['in_stock'];
        //$product->brand_id;
        //$product->brand; //object
        $p_weight = $product['weight'];
        $p_price = $product ['regular_price'];
        $p_saleprice = $product['sale_price'];
        $p_visibility = $product['catalog_visibility'];
        $p_stk_lvl = $product['stock_quantity'];
        $p_sale = $product['total_sales'];
        //Fields which are required but not avaialable @WooCommerce
        $p_ean = '';
        $p_jan = '';
        $p_isbn = '';
        $p_mpn = '';
        $p_upc = '';
        $p_brand = '';
        $p_created_date = date('Y-m-d H:i:s', strtotime($product['date_created']));
        $p_updated_date = date('Y-m-d H:i:s', strtotime($product['date_modified']));

        $product_categories_ids = array();
        if (!empty($product['categories'])) {
            foreach ($product['categories'] as $key => $value) {
                $cat_id = $value['id'];
                $product_categories_ids[] = $cat_id;
            }
        }

        /* For images */
        $product_image_data = array();
        if (!empty($product['images'])) {
            $i = 1;
            foreach ($product['images'] as $product_image) {
                $image_link = $product_image['src'];
                $position = $product_image['position'];
                $label = $product_image['name'];
                $product_image_data[] = array(
                    'image_url' => $image_link,
                    'label' => $label,
                    'position' => $i,
                    'base_img' => $image_link,
                );
                $i++;
            }
        }
        $product_data = array(
            'product_id' => $p_abb_id, // Stores/Channel product ID
            'name' => $p_name, // Product name
            'sku' => $p_sku, // Product SKU
            'description' => $p_des, // Product Description
            'product_url_path' => $product_url, // Product url if null give blank value
            'weight' => $p_weight, // Product weight if null give blank value
            'status' => $p_status, // Product status (visible or hidden). Give in 0 or 1 form (0 = hidden, 1 = visible).
            'price' => $p_price, // Porduct price
            'sale_price' => $p_saleprice, // Product sale price if null give Product price value
            'qty' => $p_stk_lvl, //Product quantity 
            'stock_status' => $p_avail, // Product stock status ("in stock" or "out of stock"). 
            // Give in 0 or 1 form (0 = out of stock, 1 = in stock)
            'websites' => array(), //This is for only magento give only and blank array
            'brand' => '', // Product brand if any
            'low_stock_notification' => 5, // Porduct low stock notification if any otherwise give default 5 value
            'created_at' => $p_created_date, // Product created at date format date('Y-m-d H:i:s')
            'updated_at' => $p_updated_date, // Product updated at date format date('Y-m-d H:i:s')
            'channel_store_name' => 'WooCommerce', // Channel or Store name
            'channel_store_prefix' => 'WOO', // Channel or store prefix id
            'mul_store_id' => $store_connection_id, //Give multiple store id
            'mul_channel_id' => '', // Give multiple channel id
            'country_code' => $store_country_code, // if your are importing channel give channel id
            'currency_code' => $store_currency_code,
            'elliot_user_id' => $id, // Elliot user id
            'store_id' => $woocommerce_store_id, // if your are importing store give store id
            'channel_id' => '', // if your are importing channel give channel id
            'barcode' => '', // Product barcode if any
            'ean' => '', // Product ean if any
            'jan' => '', // Product jan if any
            'isban' => '', // Product isban if any
            'mpn' => '', // Product mpn if any
            'categories' => array(), // Product categroy array. If null give a blank array 
            'images' => $product_image_data, // Product images data
        );

        $elliot_product_id = Stores::productImportingCommon($product_data);
        if ($elliot_product_id != '') {
            $productCategoryDelete = ProductCategories::deleteAll(['product_ID' => $elliot_product_id]);
            foreach ($product_categories_ids as $_cat) {
                $category_id = CategoryAbbrivation::find()->where(['channel_abb_id' => 'WOO' . $_cat])->one();
                if (!empty($category_id)) {
                    $category_model = new ProductCategories();
                    $category_model->elliot_user_id = $id;
                    $category_model->category_ID = $category_id->category_ID;
                    $category_model->product_ID = $elliot_product_id;
                    $category_model->created_at = date('Y-m-d h:i:s', strtotime($p_created_date));
                    $category_model->save();
                } else {
                    try {
                        StoresController::getWoocommerceAllCategory($id, $woocommerce, $woo_store_url, $woo_consumer_key, $woo_secret_key);
                        $category_id_2 = CategoryAbbrivation::find()->where(['channel_abb_id' => 'WOO' . $_cat])->one();
                        if (!empty($category_id_2)):
                            $category_model_2 = new ProductCategories();
                            $category_model_2->elliot_user_id = $user_id;
                            $category_model_2->category_ID = $category_id_2->category_ID;
                            $category_model_2->product_ID = $elliot_product_id;
                            $category_model_2->created_at = date('Y-m-d h:i:s', strtotime($product_created_at));
                            $category_model_2->save();
                        endif;
                    } catch (Exception $e) {
                        $e->getMessage();
                    }
                }
            }
        }
    }

    /* WooCommerce Product Update WebHook */

    public function actionWcUpdateProduct($id, $connection_id) {

        $store_connection_id = $connection_id;

        $get_big_id_users = User::find()->where(['id' => $id])->one();
        $config = yii\helpers\ArrayHelper::merge(
                        require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_big_id_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);
        // Get api details from db
        $store_woocommerce = StoresConnection::find()->where(['stores_connection_id' => $store_connection_id])->with('storesDetails')->one();

        $woo_store_url = $store_woocommerce->woo_store_url;
        $woo_consumer_key = $store_woocommerce->woo_consumer_key;
        $woo_secret_key = $store_woocommerce->woo_secret_key;
        $woocommerce_store_id = $store_woocommerce->store_id;
        $store_country_code = $store_woocommerce->storesDetails->country_code;
        $store_currency_code = $store_woocommerce->storesDetails->currency;

        $webhookContent = "";
        $webhook = fopen('php://input', 'rb');
        while (!feof($webhook)) {
            $webhookContent .= fread($webhook, 4096);
        }
        $data = json_decode($webhookContent);
        $file = $_SERVER['DOCUMENT_ROOT'] . '/bigcommerce_callback.php';
        $fwrite = fopen($file, "w");
        fwrite($fwrite, $webhookContent);
        fclose($fwrite);


        /* For Https server */
        $check_url = parse_url($woo_store_url);
        $url_protocol = $check_url['scheme'];
        if ($url_protocol == 'http'):
            /* For Http Url */
            $product_id = $data->product->id;
            //$product_id = 1047;
            $woocommerce = new Woocommerce(
                    $woo_store_url, $woo_consumer_key, $woo_secret_key, [
                'wp_api' => true,
                'version' => 'wc/v1',
                    ]
            );
        else:
            /* For Https Url */
            $product_id = $data->id;
            $woocommerce = new Woocommerce(
                    '' . $woo_store_url . '/wp-json/wc/v1/products/' . $product_id . '/?consumer_key=' . $woo_consumer_key . '&consumer_secret=' . $woo_secret_key . '', $woo_consumer_key, $woo_secret_key, [
                'wp_api' => true,
                'version' => 'wc/v1',
                "query_string_auth" => true,
                    ]
            );

        endif;


        /* Woocommerce product  created */
        $product = $woocommerce->get('products/' . $product_id . '');
        $p_id = $product['id'];

        $p_name = $product['name'];
        //check for the Duplicate entry in Product Table
        $p_abb_id = $product['id'];
        //Give prefix big Commerce Product id
        $prefix_product_id = 'WC' . $p_abb_id;
        $p_sku = $product['sku'];
        $p_des = $product['description'];
        $product_status = $product['status'];
        if ($product_status == 'publish') {
            $p_status = 1;
        } else {
            $p_status = 0;
        }

        $product_url = $product['permalink'];
        $p_avail = $product['in_stock'];
        //$product->brand_id;
        //$product->brand; //object
        $p_weight = $product['weight'];
        $p_price = $product ['regular_price'];
        $p_saleprice = $product['sale_price'];
        $p_visibility = $product['catalog_visibility'];
        $p_stk_lvl = $product['stock_quantity'];
        $p_sale = $product['total_sales'];
        //Fields which are required but not avaialable @WooCommerce
        $p_ean = '';
        $p_jan = '';
        $p_isbn = '';
        $p_mpn = '';
        $p_upc = '';
        $p_brand = '';
        $p_created_date = date('Y-m-d H:i:s', strtotime($product['date_created']));
        $p_updated_date = date('Y-m-d H:i:s', strtotime($product['date_modified']));

        $product_data = array(
            'product_id' => $p_abb_id, // Stores/Channel product ID
            'name' => $p_name, // Product name
            'sku' => $p_sku, // Product SKU
            'description' => $p_des, // Product Description
            'product_url_path' => $product_url, // Product url if null give blank value
            'weight' => $p_weight, // Product weight if null give blank value
            'status' => $p_status, // Product status (visible or hidden). Give in 0 or 1 form (0 = hidden, 1 = visible).
            'price' => $p_price, // Porduct price
            'sale_price' => $p_saleprice, // Product sale price if null give Product price value
            'qty' => $p_stk_lvl, //Product quantity 
            'stock_status' => $p_avail, // Product stock status ("in stock" or "out of stock"). 
            // Give in 0 or 1 form (0 = out of stock, 1 = in stock)
            'websites' => array(), //This is for only magento give only and blank array
            'brand' => '', // Product brand if any
            'low_stock_notification' => 5, // Porduct low stock notification if any otherwise give default 5 value
            'created_at' => $p_created_date, // Product created at date format date('Y-m-d H:i:s')
            'updated_at' => $p_updated_date, // Product updated at date format date('Y-m-d H:i:s')
            'channel_store_name' => 'WooCommerce', // Channel or Store name
            'channel_store_prefix' => 'WOO', // Channel or store prefix id
            'mul_store_id' => $store_connection_id, //Give multiple store id
            'mul_channel_id' => '', // Give multiple channel id
            'country_code' => $store_country_code, // if your are importing channel give channel id
            'currency_code' => $store_currency_code,
            'elliot_user_id' => $id, // Elliot user id
            'store_id' => $woocommerce_store_id, // if your are importing store give store id
            'channel_id' => '', // if your are importing channel give channel id
            'barcode' => '', // Product barcode if any
            'ean' => '', // Product ean if any
            'jan' => '', // Product jan if any
            'isban' => '', // Product isban if any
            'mpn' => '', // Product mpn if any
            'categories' => array(), // Product categroy array. If null give a blank array 
            'images' => array(), // Product images data
        );

        Stores::producUpdateCommon($product_data);
    }

    /* WooCommerce Delete Product Webhook */

    public function actionWcDeleteProductHook($id, $connection_id) {

        $store_connection_id = $connection_id;

        $get_big_id_users = User::find()->where(['id' => $id])->one();
        $config = yii\helpers\ArrayHelper::merge(
                        require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_big_id_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);

        $webhookContent = "";
        $webhook = fopen('php://input', 'rb');
        while (!feof($webhook)) {
            $webhookContent .= fread($webhook, 4096);
        }
        $data = json_decode($webhookContent);
        $file = $_SERVER['DOCUMENT_ROOT'] . '/bigcommerce_callback.php';
        $fwrite = fopen($file, "w");
        fwrite($fwrite, $webhookContent);
        fclose($fwrite);

        $product_id = $data->id;
        //$product_id =1064;

        /* For delete a product */
        $ab_id = "WOO" . $product_id;
        $productModel = ProductAbbrivation::find()->Where(['channel_abb_id' => $ab_id, 'channel_accquired' => 'WooCommerce', 'mul_store_id' => $store_connection_id])->one();
        $product_id = $productModel->product_id;
        $product_channel = ProductChannel::find()->Where(['product_id' => $product_id])->count();
        if ($product_channel == 1) {
            $product_data = Products::find()->Where(['id' => $product_id])->one();
            $product_data->product_status = 'in_active';
            $product_data->permanent_hidden = 'in_active';
            $product_data->save(false);
            $product_channel = ProductChannel::find()->Where(['product_id' => $product_id, 'store_id' => $store_connection_id])->one();
            $product_channel->status = 'no';
            $product_channel->save(false);
        } else {
            $product_channel = ProductChannel::find()->Where(['product_id' => $product_id, 'store_id' => $store_connection_id])->one();
            $product_channel->status = 'no';
            $product_channel->save(false);
        }
    }

    /* WooCommerce Create Order WebHook */

    public function actionWcCreateOrderHook($id, $connection_id) {

        $store_connection_id = $connection_id;

        $get_big_id_users = User::find()->where(['id' => $id])->one();
        $config = yii\helpers\ArrayHelper::merge(
                        require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_big_id_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);

        // Get api details from db
        $store_woocommerce = StoresConnection::find()->where(['stores_connection_id' => $store_connection_id])->with('storesDetails')->one();

        $woo_store_url = $store_woocommerce->woo_store_url;
        $woo_consumer_key = $store_woocommerce->woo_consumer_key;
        $woo_secret_key = $store_woocommerce->woo_secret_key;
        $woocommerce_store_id = $store_woocommerce->store_id;
        $store_country_code = $store_woocommerce->storesDetails->country_code;
        $store_currency_code = $store_woocommerce->storesDetails->currency;

        $webhookContent = "";
        $webhook = fopen('php://input', 'rb');
        while (!feof($webhook)) {
            $webhookContent .= fread($webhook, 4096);
        }
        $data = json_decode($webhookContent);
        $file = $_SERVER['DOCUMENT_ROOT'] . '/bigcommerce_callback.php';
        $fwrite = fopen($file, "w");
        fwrite($fwrite, $webhookContent);
        fclose($fwrite);

        /* For Https server */
        $check_url = parse_url($woo_store_url);
        $url_protocol = $check_url['scheme'];
        if ($url_protocol == 'http'):
            /* For Http Url */
            $order_id = $data->order->id;
            $woocommerce = new Woocommerce(
                    $woo_store_url, $woo_consumer_key, $woo_secret_key, [
                'wp_api' => true,
                'version' => 'wc/v1',
                    ]
            );
        else:
            /* For Https Url */
            $order_id = $data->id;
            $woocommerce = new Woocommerce(
                    '' . $woo_store_url . '/wp-json/wc/v1/orders/' . $order_id . '/?consumer_key=' . $woo_consumer_key . '&consumer_secret=' . $woo_secret_key . '', $woo_consumer_key, $woo_secret_key, [
                'wp_api' => true,
                'version' => 'wc/v1',
                "query_string_auth" => true,
                    ]
            );
        endif;

        $orders_data = $woocommerce->get('orders/' . $order_id . '');
        //get WooCommerce Order id
        $woocommerce_order_id = $orders_data['id'];
        $customer_id = $orders_data['customer_id'];
        //Fetch Ship details
        $order_status = $orders_data['status'];
        $items_total = count($orders_data['line_items']);
        $product_qauntity = $items_total;
        //billing Address
        $billing_add1 = isset($orders_data['billing']['address_1']) ? $orders_data['billing']['address_1'] : "" . ',' . isset($orders_data['billing']['address_1']) ? $orders_data['billing']['address_1'] : "";
        $billing_add2 = isset($orders_data['billing']['city']) ? $orders_data['billing']['city'] : "" . ',' . isset($orders_data['billing']['state']) ? $orders_data['billing']['state'] : "" .
                ',' . isset($orders_data['billing']['postcode']) ? $orders_data['billing']['postcode'] : "" . ',' . isset($orders_data['billing']['country']) ? $orders_data['billing']['country'] : "";
        $billing_address = $billing_add1 . ',' . $billing_add2;

        //billing Address
        $bill_street_1 = isset($orders_data['billing']['address_1']) ? $orders_data['billing']['address_1'] : '';
        $bill_street_2 = isset($orders_data['billing']['address_2']) ? $orders_data['billing']['address_2'] : '';
        $bill_city = isset($orders_data['billing']['city']) ? $orders_data['billing']['city'] : '';
        $bill_state = isset($orders_data['billing']['state']) ? $orders_data['billing']['state'] : '';
        $bill_zip = isset($orders_data['billing']['postcode']) ? $orders_data['billing']['postcode'] : '';
        $bill_country = isset($orders_data['billing']['country']) ? $orders_data['billing']['country'] : '';
        $bill_phone = isset($orders_data['billing']['phone']) ? $orders_data['billing']['phone'] : '';
        $bill_fname = isset($orders_data['billing']['first_name']) ? $orders_data['billing']['first_name'] : '';
        $bill_lname = isset($orders_data['billing']['last_name']) ? $orders_data['billing']['last_name'] : '';


        //Shipping Address
        $ship_street_1 = isset($ship_details['shipping']['address_1']) ? $ship_details['shipping']['address_1'] : $orders_data['billing']['address_1'];
        $ship_street_2 = isset($ship_details['shipping']['address_2']) ? $ship_details['shipping']['address_2'] : $orders_data['billing']['address_2'];
        $ship_city = isset($ship_details['shipping']['city']) ? $ship_details['shipping']['city'] : $orders_data['billing']['city'];
        $ship_state = isset($ship_details['shipping']['state']) ? $ship_details['shipping']['state'] : $orders_data['billing']['state'];
        $ship_zip = isset($ship_details['shipping']['postcode']) ? $ship_details['shipping']['postcode'] : $orders_data['billing']['postcode'];
        $ship_country = isset($ship_details['shipping']['country']) ? $ship_details['shipping']['country'] : $orders_data['billing']['country'];

        $shipping_address = $ship_street_1 . ',' . $ship_street_2 . ',' . $ship_city . ',' . $ship_state . ',' . $ship_zip . ',' . $ship_country;

        $total_amount = $orders_data['total'];
        $base_shipping_cost = $orders_data['shipping_total'];
        $shipping_cost_tax = $orders_data['shipping_tax'];
        $discount_amount = $orders_data['discount_total'];
        $payment_method = $orders_data['payment_method'];
        $payment_provider_id = $orders_data['transaction_id'];
        /* For refunded Amount */
        $refunded_amount = '';
        if (!empty($orders_data['refunds'])) {
            foreach ($orders_data['refunds'] as $refund) {
                $refunded_amount = +$refund['total'];
            }
        } else {
            $refunded_amount = '';
        }
        /* End refunded Amount */
        /*
         * Order status change according to status
         */
        switch ($order_status) {
            case "pending":
                $order_status = 'Pending';
                break;
            case "processing":
                $order_status = "In Transit";
                break;
            case "on-hold":
                $order_status = "On Hold";
                break;
            case "completed":
                $order_status = "Completed";
                break;
            case "cancelled":
                $order_status = "Cancel";
                break;
            case "refunded":
                $order_status = "Refunded";
                break;
            default:
                $order_status = "Pending";
        }

        $refunded_amount = $refunded_amount;
        $order_date = date('Y-m-d H:i:s', strtotime($orders_data['date_created']));
        $order_last_modified_date = date('Y-m-d H:i:s', strtotime($orders_data['date_modified']));
        $customer_email = isset($orders_data['billing']['email']) ? $orders_data['billing']['email'] : "";

        /* For order Items */
        $order_items = $orders_data['line_items'];
        $order_product_data = array();
        foreach ($order_items as $product_dtails_data) {
            $product_id = $product_dtails_data['product_id'];
            $title = $product_dtails_data['name'];
            $sku = $product_dtails_data['sku'];
            $price = $product_dtails_data['total'];
            $quantity = $product_dtails_data['quantity'];
            $product_weight = isset($product_dtails_data['weight']) ? $product_dtails_data['weight'] : 0;

            $order_product_data[] = array(
                'product_id' => $product_id,
                'name' => $title,
                'sku' => $sku,
                'price' => $price,
                'qty_ordered' => $quantity,
                'weight' => $product_weight,
            );
        }

        $order_data = array(
            'order_id' => $woocommerce_order_id,
            'mul_store_id' => $store_connection_id, //Give multiple store id
            'mul_channel_id' => '', // Give multiple channel id
            'status' => $order_status,
            'magento_store_id' => '',
            'order_grand_total' => $total_amount,
            'customer_id' => $customer_id,
            'customer_email' => $customer_email,
            'order_shipping_amount' => $shipping_cost_tax,
            'order_tax_amount' => $shipping_cost_tax,
            'total_qty_ordered' => $product_qauntity,
            'created_at' => $order_date,
            'updated_at' => $order_last_modified_date,
            'payment_method' => $payment_method,
            'refund_amount' => $refunded_amount,
            'discount_amount' => $discount_amount,
            'channel_store_name' => 'WooCommerce',
            'channel_store_prefix' => 'WOO',
            'currency_code' => $store_currency_code,
            'elliot_user_id' => $id,
            'store_id' => $woocommerce_store_id,
            'channel_id' => '',
            'shipping_address' => array(
                'street_1' => $ship_street_1,
                'street_2' => $ship_street_2,
                'state' => $ship_state,
                'city' => $ship_city,
                'country' => $ship_country,
                'country_id' => '',
                'postcode' => $ship_zip,
                'email' => $customer_email,
                'telephone' => '',
                'firstname' => '',
                'lastname' => '',
            ),
            'billing_address' => array(
                'street_1' => $bill_street_1,
                'street_2' => $bill_street_2,
                'state' => $bill_state,
                'city' => $bill_city,
                'country' => $bill_country,
                'country_id' => '',
                'postcode' => $bill_zip,
                'email' => $customer_email,
                'telephone' => $bill_phone,
                'firstname' => $bill_fname,
                'lastname' => $bill_lname,
            ),
            'items' => $order_product_data,
        );
        Stores::orderImportingCommon($order_data);
    }

    /* WooCommerce Update Order Webhook */

    public function actionWcUpdateOrderHook($id, $connection_id) {

        $store_connection_id = $connection_id;
        $get_big_id_users = User::find()->where(['id' => $id])->one();
        $config = yii\helpers\ArrayHelper::merge(
                        require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_big_id_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);

        // Get api details from db
        $store_woocommerce = StoresConnection::find()->where(['stores_connection_id' => $store_connection_id])->with('storesDetails')->one();

        $woo_store_url = $store_woocommerce->woo_store_url;
        $woo_consumer_key = $store_woocommerce->woo_consumer_key;
        $woo_secret_key = $store_woocommerce->woo_secret_key;
        $woocommerce_store_id = $store_woocommerce->store_id;
        $store_country_code = $store_woocommerce->storesDetails->country_code;
        $store_currency_code = $store_woocommerce->storesDetails->currency;

        $webhookContent = "";
        $webhook = fopen('php://input', 'rb');
        while (!feof($webhook)) {
            $webhookContent .= fread($webhook, 4096);
        }
        $data = json_decode($webhookContent);
        $file = $_SERVER['DOCUMENT_ROOT'] . '/bigcommerce_callback.php';
        $fwrite = fopen($file, "w");
        fwrite($fwrite, $webhookContent);
        fclose($fwrite);
        /* For Https server */
        $check_url = parse_url($woo_store_url);
        $url_protocol = $check_url['scheme'];
        if ($url_protocol == 'http'):
            /* For Http Url */
            $order_id = $data->order->id;
            $woocommerce = new Woocommerce(
                    $woo_store_url, $woo_consumer_key, $woo_secret_key, [
                'wp_api' => true,
                'version' => 'wc/v1',
                    ]
            );
        else:
            /* For Https Url */
            $order_id = $data->id;
            $woocommerce = new Woocommerce(
                    '' . $woo_store_url . '/wp-json/wc/v1/orders/' . $order_id . '/?consumer_key=' . $woo_consumer_key . '&consumer_secret=' . $woo_secret_key . '', $woo_consumer_key, $woo_secret_key, [
                'wp_api' => true,
                'version' => 'wc/v1',
                "query_string_auth" => true,
                    ]
            );
        endif;

        //$order_id = 788;
        $orders_data = $woocommerce->get('orders/' . $order_id . '');
        //get WooCommerce Order id
        $woocommerce_order_id = $orders_data['id'];
        $customer_id = $orders_data['customer_id'];
        //Fetch Ship details
        $order_status = $orders_data['status'];
        $items_total = count($orders_data['line_items']);
        $product_qauntity = $items_total;
        //billing Address
        $billing_add1 = isset($orders_data['billing']['address_1']) ? $orders_data['billing']['address_1'] : "" . ',' . isset($orders_data['billing']['address_1']) ? $orders_data['billing']['address_1'] : "";
        $billing_add2 = isset($orders_data['billing']['city']) ? $orders_data['billing']['city'] : "" . ',' . isset($orders_data['billing']['state']) ? $orders_data['billing']['state'] : "" .
                ',' . isset($orders_data['billing']['postcode']) ? $orders_data['billing']['postcode'] : "" . ',' . isset($orders_data['billing']['country']) ? $orders_data['billing']['country'] : "";
        $billing_address = $billing_add1 . ',' . $billing_add2;

        //billing Address
        $bill_street_1 = isset($orders_data['billing']['address_1']) ? $orders_data['billing']['address_1'] : '';
        $bill_street_2 = isset($orders_data['billing']['address_2']) ? $orders_data['billing']['address_2'] : '';
        $bill_city = isset($orders_data['billing']['city']) ? $orders_data['billing']['city'] : '';
        $bill_state = isset($orders_data['billing']['state']) ? $orders_data['billing']['state'] : '';
        $bill_zip = isset($orders_data['billing']['postcode']) ? $orders_data['billing']['postcode'] : '';
        $bill_country = isset($orders_data['billing']['country']) ? $orders_data['billing']['country'] : '';
        $bill_phone = isset($orders_data['billing']['phone']) ? $orders_data['billing']['phone'] : '';
        $bill_fname = isset($orders_data['billing']['first_name']) ? $orders_data['billing']['first_name'] : '';
        $bill_lname = isset($orders_data['billing']['last_name']) ? $orders_data['billing']['last_name'] : '';


        //Shipping Address
        $ship_street_1 = isset($ship_details['shipping']['address_1']) ? $ship_details['shipping']['address_1'] : $orders_data['billing']['address_1'];
        $ship_street_2 = isset($ship_details['shipping']['address_2']) ? $ship_details['shipping']['address_2'] : $orders_data['billing']['address_2'];
        $ship_city = isset($ship_details['shipping']['city']) ? $ship_details['shipping']['city'] : $orders_data['billing']['city'];
        $ship_state = isset($ship_details['shipping']['state']) ? $ship_details['shipping']['state'] : $orders_data['billing']['state'];
        $ship_zip = isset($ship_details['shipping']['postcode']) ? $ship_details['shipping']['postcode'] : $orders_data['billing']['postcode'];
        $ship_country = isset($ship_details['shipping']['country']) ? $ship_details['shipping']['country'] : $orders_data['billing']['country'];

        $shipping_address = $ship_street_1 . ',' . $ship_street_2 . ',' . $ship_city . ',' . $ship_state . ',' . $ship_zip . ',' . $ship_country;

        $total_amount = $orders_data['total'];
        $base_shipping_cost = $orders_data['shipping_total'];
        $shipping_cost_tax = $orders_data['shipping_tax'];
        $discount_amount = $orders_data['discount_total'];
        $payment_method = $orders_data['payment_method'];
        $payment_provider_id = $orders_data['transaction_id'];
        /* For refunded Amount */
        $refunded_amount = '';
        if (!empty($orders_data['refunds'])) {
            foreach ($orders_data['refunds'] as $refund) {
                $refunded_amount = +$refund['total'];
            }
        } else {
            $refunded_amount = '';
        }
        /* End refunded Amount */
        /*
         * Order status change according to status
         */
        switch ($order_status) {
            case "pending":
                $order_status = 'Pending';
                break;
            case "processing":
                $order_status = "In Transit";
                break;
            case "on-hold":
                $order_status = "On Hold";
                break;
            case "completed":
                $order_status = "Completed";
                break;
            case "cancelled":
                $order_status = "Cancel";
                break;
            case "refunded":
                $order_status = "Refunded";
                break;
            default:
                $order_status = "Pending";
        }

        $refunded_amount = $refunded_amount;
        $order_date = date('Y-m-d H:i:s', strtotime($orders_data['date_created']));
        $order_last_modified_date = date('Y-m-d H:i:s', strtotime($orders_data['date_modified']));
        $customer_email = isset($orders_data['billing']['email']) ? $orders_data['billing']['email'] : "";

        /* For order Items */
        $order_items = $orders_data['line_items'];
        $order_product_data = array();
        foreach ($order_items as $product_dtails_data) {
            $product_id = $product_dtails_data['product_id'];
            $title = $product_dtails_data['name'];
            $sku = $product_dtails_data['sku'];
            $price = $product_dtails_data['total'];
            $quantity = $product_dtails_data['quantity'];
            $product_weight = isset($product_dtails_data['weight']) ? $product_dtails_data['weight'] : 0;

            $order_product_data[] = array(
                'product_id' => $product_id,
                'name' => $title,
                'sku' => $sku,
                'price' => $price,
                'qty_ordered' => $quantity,
                'weight' => $product_weight,
            );
        }

        $order_data = array(
            'order_id' => $woocommerce_order_id,
            'mul_store_id' => $store_connection_id, //Give multiple store id
            'mul_channel_id' => '', // Give multiple channel id
            'status' => $order_status,
            'magento_store_id' => '',
            'order_grand_total' => $total_amount,
            'customer_id' => $customer_id,
            'customer_email' => $customer_email,
            'order_shipping_amount' => $shipping_cost_tax,
            'order_tax_amount' => $shipping_cost_tax,
            'total_qty_ordered' => $product_qauntity,
            'created_at' => $order_date,
            'updated_at' => $order_last_modified_date,
            'payment_method' => $payment_method,
            'refund_amount' => $refunded_amount,
            'discount_amount' => $discount_amount,
            'channel_store_name' => 'WooCommerce',
            'channel_store_prefix' => 'WOO',
            'currency_code' => $store_currency_code,
            'elliot_user_id' => $id,
            'store_id' => $woocommerce_store_id,
            'channel_id' => '',
            'shipping_address' => array(
                'street_1' => $ship_street_1,
                'street_2' => $ship_street_2,
                'state' => $ship_state,
                'city' => $ship_city,
                'country' => $ship_country,
                'country_id' => '',
                'postcode' => $ship_zip,
                'email' => $customer_email,
                'telephone' => '',
                'firstname' => '',
                'lastname' => '',
            ),
            'billing_address' => array(
                'street_1' => $bill_street_1,
                'street_2' => $bill_street_2,
                'state' => $bill_state,
                'city' => $bill_city,
                'country' => $bill_country,
                'country_id' => '',
                'postcode' => $bill_zip,
                'email' => $customer_email,
                'telephone' => $bill_phone,
                'firstname' => $bill_fname,
                'lastname' => $bill_lname,
            ),
            'items' => $order_product_data,
        );
        Stores::orderUpdateCommon($order_data);
    }

    /* WooCommerce Order Delete Webhook */

    public function actionWcOrderDeleteHook($id) {

        $get_big_id_users = User::find()->where(['id' => $id])->one();
        $config = yii\helpers\ArrayHelper::merge(
                        require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_big_id_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);

        // Get api details from db
        $Stores_data = Stores::find()->Where(['store_name' => 'WooCommerce'])->with('storeconnection')->one();
        $woo_store_url = $Stores_data->storeconnection->woo_store_url;
        $woo_consumer_key = $Stores_data->storeconnection->woo_consumer_key;
        $woo_secret_key = $Stores_data->storeconnection->woo_secret_key;
        $woocommerce_store_id = $Stores_data->store_id;

        $webhookContent = "";
        $webhook = fopen('php://input', 'rb');
        while (!feof($webhook)) {
            $webhookContent .= fread($webhook, 4096);
        }
        $data = json_decode($webhookContent);
        $file = $_SERVER['DOCUMENT_ROOT'] . '/bigcommerce_callback.php';
        $fwrite = fopen($file, "w");
        fwrite($fwrite, $webhookContent);
        fclose($fwrite);

        $order_id = $data->id;
        $DeleteOrdersModel = Orders::find()->where(['channel_abb_id' => 'WC' . $order_id])->one();
        $DeleteOrdersModel->delete();
    }

    public function actionMagentoProductCreate() {
        $url_data = $_POST['data'];
        $url_data = urldecode($url_data);
        $product_info = json_decode($url_data, true);
        $shop_url = $product_info['magento_url'];
        $url_parse = parse_url($shop_url);
        $shop_url_1 = '';
        if ($url_parse['scheme'] == 'https') {
            $shop_url_1 = str_replace("https", 'http', $shop_url);
        } else {
            $shop_url_1 = str_replace("http", 'https', $shop_url);
        }

        $get_users = User::find()->where("magento_shop_url IN('" . $shop_url . "','" . $shop_url_1 . "')")->all();
        foreach ($get_users as $_user) {
            $config = yii\helpers\ArrayHelper::merge(
                require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $_user->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
            );
            $application = new yii\web\Application($config);
            $user_id = $_user->id;
            $get_magento_id = Stores::find()->select('store_id')->where(['store_name' => 'Magento'])->one();
            $magento_store_id = $get_magento_id->store_id;
            $store_magento = StoresConnection::find()->where(['store_id' => $magento_store_id, 'user_id' => $user_id])->andWhere("mag_shop IN('" . $shop_url . "','" . $shop_url_1 . "')")->one();
            if (!empty($store_magento)) {
                $magento_shop = $store_magento->mag_shop;
                $magento_soap_user = $store_magento->mag_soap_user;
                $magento_soap_api_key = $store_magento->mag_soap_api;
                $store_connection_id = $store_magento->stores_connection_id;
                
                $country_detail = StoreDetails::find()->where(['store_connection_id' => $store_connection_id])->one();
                $country_code = '';
                $currency_code = '';
                if(!empty($country_detail)){
                    $country_code = $country_detail->country_code;
                    $currency_code = $country_detail->currency;
                }
                
                $mage_product_id = $product_info['product_id'];
                $product_name = $product_info['name'];
                $product_sku = $product_info['sku'];
                $product_url = '';
                if(isset($product_info['url_path'])){
                    $product_url_key = $product_info['url_path'];
                    $product_url = $magento_shop . '/' . $product_url_key;
                }
                $product_type = $product_info['product_type'];
                $product_categories_ids = $product_info['categories'];
                $product_description = $product_info['description'];
                $product_weight = $product_info['weight'];
                $product_status = $product_info['product_status'];
                $product_created_at = $product_info['product_created_at'];
                $product_updated_at = $product_info['product_updated_at'];
                $product_price = $product_info['price'];
                $qty = $product_info['qty'];
                $stock_status = $product_info['in_stock_status'];
                $websites = $product_info['websites'];
                $barcode = '';
                $p_ean = '';
                $p_jan = '';
                $p_isbn = '';
                $p_mpn = '';
                
                $product_image = $product_info['images'];
                $product_image_pos = $product_info['position'];
                $base_img = $product_info['base_img'];
                $i = 0;
                $product_image_data = array();
                foreach ($product_image as $_image) {
                    if ($base_img == $_image) {
                        $base_image = $_image;
                    }
                    $product_image_data[] = array(
                        'image_url' => $_image,
                        'label' => '',
                        'position' => $product_image_pos[$i],
                        'base_img' => $base_image,
                    );
                    $i++;
                }
                
                $product_data = array(
                    'product_id' => $mage_product_id, // Stores/Channel product ID
                    'name' => $product_name, // Product name
                    'sku' => $product_sku, // Product SKU
                    'description' => $product_description, // Product Description
                    'product_url_path' => $product_url, // Product url if null give blank value
                    'weight' => $product_weight, // Product weight if null give blank value
                    'status' => $product_status, // Product status (visible or hidden). Give in 0 or 1 form (0 = hidden, 1 = visible).
                    'price' => $product_price, // Porduct price
                    'sale_price' => $product_price, // Product sale price if null give Product price value
                    'qty' => $qty, //Product quantity 
                    'stock_status' => $stock_status, // Product stock status ("in stock" or "out of stock"). 
                    // Give in 0 or 1 form (0 = out of stock, 1 = in stock)
                    'websites' => $websites, //This is for only magento give only and blank array
                    'brand' => '', // Product brand if any
                    'low_stock_notification' => 5, // Porduct low stock notification if any otherwise give default 5 value
                    'created_at' => $product_created_at, // Product created at date format date('Y-m-d H:i:s')
                    'updated_at' => $product_updated_at, // Product updated at date format date('Y-m-d H:i:s')
                    'channel_store_name' => 'Magento', // Channel or Store name
                    'channel_store_prefix' => 'MG', // Channel or store prefix id.
                    'mul_store_id' => $store_connection_id, //Give multiple store id
                    'mul_channel_id' => '', // Give multiple channel id
                    'elliot_user_id' => $user_id, // Elliot user id
                    'store_id' => $magento_store_id, // if your are importing store give store id
                    'channel_id' => '', // if your are importing channel give channel id
                    'country_code' => $country_code,
                    'currency_code' => $currency_code,
                    'upc' => '', // Product barcode if any
                    'ean' => '', // Product ean if any
                    'jan' => '', // Product jan if any
                    'isban' => '', // Product isban if any
                    'mpn' => '', // Product mpn if any
                    'categories' => $product_categories_ids, // Product categroy array. If null give a blank array 
                    'images' => $product_image_data, // Product images data
                );
                $check_abbrivation = ProductAbbrivation::find()->Where(['channel_abb_id' => 'MG'.$mage_product_id, 'mul_store_id' => $store_connection_id])->one();
                if(empty($check_abbrivation)){
                    Stores::productImportingCommon($product_data);
                }
                else{
                    Stores::producUpdateCommon($product_data);
                }
            }
        }
    }

    public function actionMagentoOrderCreate() {
        $url_data = $_POST['data'];
        $url_data = urldecode($url_data);
        $order_data = json_decode($url_data, true);
        $shop_url = $order_data['magento_url'];
        $url_parse = parse_url($shop_url);
        $shop_url_1 = '';
        if ($url_parse['scheme'] == 'https') {
            $shop_url_1 = str_replace("https", 'http', $shop_url);
        } else {
            $shop_url_1 = str_replace("http", 'https', $shop_url);
        }

        $get_users = User::find()->where("magento_shop_url IN('" . $shop_url . "','" . $shop_url_1 . "')")->all();
        foreach ($get_users as $_user) {
            $config = yii\helpers\ArrayHelper::merge(
                            require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $_user->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
            );
            $application = new yii\web\Application($config);
            $user_id = $_user->id;
            $get_magento_id = Stores::find()->select('store_id')->where(['store_name' => 'Magento'])->one();
            $magento_store_id = $get_magento_id->store_id;
            $store_magento = StoresConnection::find()->where(['store_id' => $magento_store_id, 'user_id' => $user_id])->andWhere("mag_shop IN('" . $shop_url . "','" . $shop_url_1 . "')")->one();
            if (!empty($store_magento)) {
                $magento_shop = $store_magento->mag_shop;
                $magento_soap_user = $store_magento->mag_soap_user;
                $magento_soap_api_key = $store_magento->mag_soap_api;
                $store_connection_id = $store_magento->stores_connection_id;
                
                $country_detail = StoreDetails::find()->where(['store_connection_id' => $store_connection_id])->one();
                $country_code = '';
                $currency_code = '';
                if(!empty($country_detail)){
                    $country_code = $country_detail->country_code;
                    $currency_code = $country_detail->currency;
                }

                /** Order data */
                $order_id = $order_data['order_id'];
                $order_state = $order_data['order_state'];
                $order_status = $order_data['order_status'];
                $order_store_id = $order_data['order_store_id'];
                $order_total = $order_data['order_total'];
                $customer_email = $order_data['customer_email'];
                $order_shipping_amount = $order_data['order_shipping_amount'];
                $order_tax_amount = $order_data['base_tax_amount'];
                $order_product_qty = $order_data['order_product_qty'];
                $customer_id = $order_data['customer_id'];
                $order_created_at = $order_data['order_created_at'];
                $order_updated_at = $order_data['order_updated_at'];
                $refund_amount = $order_data['refund_amount'];
                $payment_method = $order_data['payment_method'];
                $discount_amount = $order_data['discount_amount'];

                switch (strtolower($order_status)) {
                    case "pending":
                        $order_status_2 = 'Pending';
                        break;
                    case "complete":
                        $order_status_2 = "Completed";
                        break;
                    case "canceled":
                        $order_status_2 = "Cancel";
                        break;
                    case "closed":
                        $order_status_2 = "Closed";
                        break;
                    case "fraud":
                        $order_status_2 = "Cancel";
                        break;
                    case "holded":
                        $order_status_2 = "On Hold";
                        break;
                    case "payment_review":
                        $order_status_2 = "Pending";
                        break;
                    case "paypal_canceled_reversal":
                        $order_status_2 = "Cancel";
                        break;
                    case "paypal_canceled_reversal":
                        $order_status_2 = "Cancel";
                        break;
                    case "paypal_reversed":
                        $order_status_2 = "Cancel";
                        break;
                    case "pending_payment":
                        $order_status_2 = "Pending";
                        break;
                    case "pending_paypal":
                        $order_status_2 = "Pending";
                        break;
                    case "processing":
                        $order_status_2 = "In Transit";
                        break;
                    default:
                        $order_status_2 = "Pending";
                }

                /**
                 * Order Shipping address
                 */
                $ship_state = $ship_zip = $ship_address = $ship_address_1 = $ship_address_2 = $ship_city = $ship_customer_email = '';
                $ship_phone = $ship_country_id = $ship_firstname = $ship_lastname = $ship_middlename = '';
                if (isset($order_data['shipping_detail'])) {
                    $order_shipping_add = $order_data['shipping_detail'];
                    $ship_state = $order_shipping_add['region'];
                    $ship_zip = $order_shipping_add['postcode'];
                    $ship_address = $order_shipping_add['street'];
                    $ship_address_1 = $ship_address[0];
                    $ship_address_2 = '';
                    if (array_key_exists('1', $ship_address)) {
                        $ship_address_2 = $ship_address[1];
                    }
                    $ship_city = $order_shipping_add['city'];
                    $ship_customer_email = $order_shipping_add['email'];
                    $ship_phone = $order_shipping_add['telephone'];
                    $ship_country_id = $order_shipping_add['country_id'];
                    $ship_firstname = $order_shipping_add['firstname'];
                    $ship_lastname = $order_shipping_add['lastname'];
                    $ship_middlename = $order_shipping_add['middlename'];
                    $shipping_address = implode(',', $ship_address) . "," . $ship_city . "," . $ship_state . "," . $ship_zip . "," . $ship_country_id;
                }
                /**
                 * Order Billing address
                 */
                $bill_state = $bill_zip = $bill_address = $bill_address_1 = $bill_address_2 = $bill_city = $bill_customer_email = '';
                $bill_phone = $bill_country_id = $bill_firstname = $bill_lastname = $bill_middlename = $billing_address = '';
                if (isset($order_data['billing_detail'])) {
                    $order_billing_add = $order_data['billing_detail'];
                    $bill_state = $order_billing_add['region'];
                    $bill_zip = $order_billing_add['postcode'];
                    $bill_address = $order_billing_add['street'];
                    $bill_address_1 = $bill_address[0];
                    $bill_address_2 = '';
                    if (array_key_exists('1', $bill_address)) {
                        $ship_address_2 = $ship_address[1];
                    }
                    $bill_city = $order_billing_add['city'];
                    $bill_customer_email = $order_billing_add['email'];
                    $bill_phone = $order_billing_add['telephone'];
                    $bill_country_id = $order_billing_add['country_id'];
                    $bill_firstname = $order_billing_add['firstname'];
                    $bill_lastname = $order_billing_add['lastname'];
                    $bill_middlename = $order_billing_add['middlename'];
                    $billing_address = implode(',', $bill_address) . "," . $bill_city . "," . $bill_state . "," . $bill_zip . "," . $bill_country_id;
                }
                
                /* Check Customer exists or not */
                $check_customer = CustomerAbbrivation::find()->Where(['channel_abb_id' => 'MG' . $customer_id,])->one();
                if (empty($check_customer)) {
                    if($customer_id=='' || $customer_id==0){
                        $customer_id = '0';
                    }
                    $api_url = $magento_shop . '/api/soap/?wsdl';
                    $cli = new SoapClient($api_url);
                    $session_id = $cli->login($magento_soap_user, $magento_soap_api_key);
                    
                    $_customer = $cli->call($session_id, 'customer.info', $customer_id);
                    $cus_id = $_customer['customer_id'];
                    $cus_created_at = $_customer['created_at'];
                    $cus_updated_at = $_customer['updated_at'];
                    $cus_email = $_customer['email'];
                    $cus_f_name = $_customer['firstname'];
                    $cus_l_name = $_customer['lastname'];
                    $website_id = $_customer['website_id'];
                    $bill_city = $bill_company = $bill_country_id = $bill_zip = $bill_region = $bill_add = $bill_telephone = '';
                    if (isset($_customer['default_billing'])):
                        $customer_default_bill_id = $_customer['default_billing'];
                        $customer_bill_address = $cli->call($session_id, 'customer_address.info', $customer_default_bill_id);
                        $bill_city = $customer_bill_address['city'];
                        $bill_company = $customer_bill_address['company'];
                        $bill_country_id = $customer_bill_address['country_id'];
                        $bill_zip = $customer_bill_address['postcode'];
                        $bill_region = $customer_bill_address['region'];
                        $bill_add = $customer_bill_address['street'];
                        $bill_telephone = $customer_bill_address['telephone'];
                    endif;
                    $ship_city = $ship_company = $ship_country_id = $ship_zip = $ship_region = $ship_street = $ship_telephone = '';
                    if (isset($_customer['default_shipping'])):
                        $customer_default_ship_id = $_customer['default_billing'];
                        $customer_ship_address = $cli->call($session_id, 'customer_address.info', $customer_default_ship_id);
                        $ship_city = $customer_ship_address['city'];
                        $ship_company = $customer_ship_address['company'];
                        $ship_country_id = $customer_ship_address['country_id'];
                        $ship_zip = $customer_ship_address['postcode'];
                        $ship_region = $customer_ship_address['region'];
                        $ship_street = $customer_ship_address['street'];
                        $ship_telephone = $customer_ship_address['telephone'];
                    endif;
                    $customer_data = array(
                        'customer_id' => $customer_id,
                        'elliot_user_id' => $user_id,
                        'first_name' => $cus_f_name,
                        'last_name' => $cus_l_name,
                        'email' => $cus_email,
                        'channel_store_name' => 'Magento',
                        'channel_store_prefix' => 'MG',
                        'mul_store_id' => $store_connection_id,
                        'mul_channel_id' => '',
                        'customer_created_at' => $cus_created_at,
                        'customer_updated_at' => $cus_updated_at,
                        'billing_address' => array(
                            'street_1' => $bill_add,
                            'street_2' => '',
                            'city' => $bill_city,
                            'state' => $bill_region,
                            'country' => $bill_country_id,
                            'country_iso' => $bill_country_id,
                            'zip' => $bill_zip,
                            'phone_number' => $bill_telephone,
                            'address_type' => 'Default',
                        ),
                        'shipping_address' => array(
                            'street_1' => $ship_street,
                            'street_2' => '',
                            'city' => $ship_city,
                            'state' => $ship_region,
                            'country' => $ship_country_id,
                            'zip' => $ship_zip,
                        ),
                    );
                    Stores::customerImportingCommon($customer_data);
                }
                
                
                $customer_id = ($customer_id == '') ? 0 : $customer_id;
                $order_data = array(
                    'order_id' => $order_id,
                    'status' => $order_status_2,
                    'magento_store_id' => '',
                    'order_grand_total' => $order_total,
                    'customer_id' => $customer_id,
                    'customer_email' => $customer_email,
                    'order_shipping_amount' => $order_shipping_amount,
                    'order_tax_amount' => $order_tax_amount,
                    'total_qty_ordered' => $order_product_qty,
                    'created_at' => $order_created_at,
                    'updated_at' => $order_updated_at,
                    'payment_method' => $payment_method,
                    'refund_amount' => $refund_amount,
                    'discount_amount' => $discount_amount,
                    'channel_store_name' => 'Magento',
                    'channel_store_prefix' => 'MG',
                    'mul_store_id' => $store_connection_id,
                    'mul_channel_id' => '',
                    'elliot_user_id' => $user_id,
                    'store_id' => $magento_store_id,
                    'currency_code' => $currency_code,
                    'import_or_create' => 'create',
                    'channel_id' => '',
                    'shipping_address' => array(
                        'street_1' => $ship_address_1,
                        'street_2' => $ship_address_2,
                        'state' => $ship_state,
                        'city' => $ship_city,
                        'country' => $ship_country_id,
                        'country_id' => $ship_country_id,
                        'postcode' => $shipping_zip,
                        'email' => $customer_email,
                        'telephone' => $shipping_phone,
                        'firstname' => $shipping_f_name,
                        'lastname' => $shipping_l_name,
                    ),
                    'billing_address' => array(
                        'street_1' => $billing_street_1,
                        'street_2' => $billing_street_2,
                        'state' => $billing_state,
                        'city' => $billing_city,
                        'country' => $billing_country,
                        'country_id' => $billing_country_code,
                        'postcode' => $billing_zip,
                        'email' => $customer_email,
                        'telephone' => $billing_phone,
                        'firstname' => $billing_f_name,
                        'lastname' => $billing_l_name,
                    ),
                    'items' => $order_product_data,
                );
                Stores::orderImportingCommon($order_data);
                
                $check_order = Orders::find()->Where(['channel_abb_id' => 'SP' . $order_id])->one();
                if (empty($check_order)) {
                    
                }
                

//                if ($customer_id == '') {
//                    $check_customer = CustomerUser::find()->Where(['first_name' => $bill_firstname, 'last_name' => $bill_lastname, 'email' => $customer_email, 'phone_number' => $bill_phone])->one();
//                    if (empty($check_customer)) {
//                        $customer_id_email_check = CustomerUser::find()->Where(['email' => $customer_email])
//                                        ->with(['customerabbrivation' => function($query) {
//                                                $query->andWhere(['channel_abb_id' => 'MG0']);
//                                            }])->asArray()->all();
//                        if (empty($customer_id_email_check)) {
//                            $Customers_model = new CustomerUser();
//                            $Customers_model->channel_abb_id = "";
//                            $Customers_model->first_name = $bill_firstname;
//                            $Customers_model->last_name = $bill_lastname;
//                            $Customers_model->email = $customer_email;
//                            $Customers_model->channel_acquired = 'Magento';
//                            $Customers_model->date_acquired = date('Y-m-d H:i:s', strtotime($order_created_at));
//                            $Customers_model->street_1 = $bill_address_1;
//                            $Customers_model->street_2 = $bill_address_2;
//                            $Customers_model->city = $bill_city;
//                            $Customers_model->country = $bill_country_id;
//                            $Customers_model->country_iso = $bill_country_id;
//                            $Customers_model->state = $bill_state;
//                            $Customers_model->zip = $bill_zip;
//                            $Customers_model->phone_number = $bill_phone;
//                            $Customers_model->address_type = 'Default';
//                            $Customers_model->elliot_user_id = $user_id;
//                            $Customers_model->created_at = date('Y-m-d H:i:s', strtotime($order_created_at));
//                            $Customers_model->updated_at = date('Y-m-d H:i:s', strtotime($order_updated_at));
//                            if ($Customers_model->save(false)) {
//                                $Customers_abbrivation = new CustomerAbbrivation();
//                                $Customers_abbrivation->channel_abb_id = 'MG0';
//                                $Customers_abbrivation->customer_id = $Customers_model->customer_ID;
//                                $Customers_abbrivation->channel_accquired = 'Magento';
//                                $Customers_abbrivation->bill_street_1 = $bill_address_1;
//                                $Customers_abbrivation->bill_street_2 = $bill_address_2;
//                                $Customers_abbrivation->bill_city = $bill_city;
//                                $Customers_abbrivation->bill_state = $bill_state;
//                                $Customers_abbrivation->bill_zip = $bill_zip;
//                                $Customers_abbrivation->bill_country = $bill_country_id;
//                                $Customers_abbrivation->bill_country_iso = $bill_country_id;
//                                $Customers_abbrivation->created_at = date('Y-m-d H:i:s', strtotime($order_created_at));
//                                $Customers_abbrivation->updated_at = date('Y-m-d H:i:s', strtotime($order_updated_at));
//                                $Customers_abbrivation->save(false);
//                            }
//                            $elliot_customer_id = $Customers_model->customer_ID;
//                        } else {
//                            $elliot_customer_id = $customer_id_email_check[0]['customer_ID'];
//                        }
//                    } else {
//                        $Customers_abbrivation = new CustomerAbbrivation();
//                        $Customers_abbrivation->channel_abb_id = 'MG' . $customer_id;
//                        $Customers_abbrivation->customer_id = $check_customer->customer_ID;
//                        $Customers_abbrivation->channel_accquired = 'Magento';
//                        $Customers_abbrivation->bill_street_1 = $bill_address;
//                        $Customers_abbrivation->bill_street_2 = '';
//                        $Customers_abbrivation->bill_city = $bill_city;
//                        $Customers_abbrivation->bill_state = $bill_state;
//                        $Customers_abbrivation->bill_zip = $bill_zip;
//                        $Customers_abbrivation->bill_country = $bill_country_id;
//                        $Customers_abbrivation->bill_country_iso = $bill_country_id;
//                        $Customers_abbrivation->created_at = date('Y-m-d H:i:s', strtotime($order_created_at));
//                        $Customers_abbrivation->updated_at = date('Y-m-d H:i:s', strtotime($order_updated_at));
//                        $Customers_abbrivation->save(false);
//                        $elliot_customer_id = $check_customer->customer_ID;
//                    }
//                } else {
//                    $customer_db1 = CustomerAbbrivation::find()->Where(['channel_abb_id' => 'MG' . $customer_id])->one();
//                    if (!empty($customer_db1)) {
//                        $elliot_customer_id = $customer_db1->customer_id;
//                    }
//                }
//                $check_order = Orders::find()->Where(['channel_abb_id' => 'MG' . $order_id])->one();
//                if (!empty($check_order)) {
//                    $check_order->elliot_user_id = $user_id;
//                    $check_order->channel_abb_id = 'MG' . $order_id;
//                    $check_order->customer_id = $elliot_customer_id;
//                    $check_order->order_status = $order_status_2;
//                    $check_order->product_qauntity = $order_product_qty;
//                    $check_order->shipping_address = $shipping_address;
//                    $check_order->ship_street_1 = $ship_address_1;
//                    $check_order->ship_street_2 = $ship_address_2;
//                    $check_order->ship_city = $ship_city;
//                    $check_order->ship_state = $ship_state;
//                    $check_order->ship_zip = $ship_zip;
//                    $check_order->ship_country = $ship_country_id;
//                    $check_order->billing_address = $billing_address;
//                    $check_order->bill_street_1 = $bill_address_1;
//                    $check_order->bill_street_2 = $ship_address_2;
//                    $check_order->bill_city = $bill_city;
//                    $check_order->bill_state = $bill_state;
//                    $check_order->bill_zip = $bill_zip;
//                    $check_order->bill_country = $bill_country_id;
//                    $check_order->base_shipping_cost = $order_shipping_amount;
//                    $check_order->shipping_cost_tax = $order_tax_amount;
//                    $check_order->payment_method = $payment_method;
//                    $check_order->payment_status = $order_status_2;
//                    $check_order->refunded_amount = $refund_amount;
//                    $check_order->discount_amount = $discount_amount;
//                    $check_order->total_amount = $order_total;
//                    $check_order->magento_store_id = $order_store_id;
//                    $check_order->order_date = $order_created_at;
//                    $check_order->updated_at = date('Y-m-d h:i:s', strtotime($order_updated_at));
//                    $check_order->created_at = date('Y-m-d h:i:s', strtotime($order_created_at));
//                    $check_order->save(false);
//                    $last_order_id = $check_order->order_ID;
//
//                    $OrdersProduct = OrdersProducts::deleteAll(['order_Id' => $last_order_id]);
//                    $order_items = $order_data['order_items'];
//                    foreach ($order_items as $_items):
//                        $title = $_items['name'];
//                        $quantity = $_items['qty_ordered'];
//                        $price = $_items['price'];
//                        $sku = $_items['sku'];
//                        $product_id = $_items['product_id'];
//                        $product_weight = $_items['weight'];
//                        $checkProductId = ProductAbbrivation::find()->where(['channel_abb_id' => 'MG' . $product_id])->one();
//                        if (empty($checkProductId)) {
//                            $checkProductIdAnother = Products::find()->where(['SKU' => $sku])->one();
//                            if (empty($checkProductIdAnother)) {
//                                $productModel = new Products ();
//                                $productModel->channel_abb_id = 'MG0000';
//                                $productModel->product_name = $title;
//                                $productModel->SKU = $sku;
//                                $productModel->UPC = '';
//                                $productModel->EAN = '';
//                                $productModel->Jan = '';
//                                $productModel->ISBN = '';
//                                $productModel->MPN = '';
//                                $productModel->description = '';
//                                $productModel->adult = 'no';
//                                $productModel->age_group = NULL;
//                                $productModel->gender = 'Unisex';
//                                $productModel->brand = '';
//                                $productModel->stock_quantity = 1;
//                                $productModel->availability = 'Out of Stock';
//                                $productModel->stock_level = 'Out of Stock';
//                                $productModel->stock_status = 'Visible';
//                                $productModel->product_status = 'in_active';
//                                $productModel->price = $price;
//                                $productModel->sales_price = $price;
//                                $productModel->elliot_user_id = $user_id;
//                                $productModel->magento_store_id = $order_store_id;
//                                $productModel->save(false);
//                                $last_pro_id = $productModel->id;
//
//                                $custom_productChannelModel = new ProductChannel;
//                                $store_id = Stores::find()->select('store_id')->where(['store_name' => 'Magento'])->one();
//                                $custom_productChannelModel->store_id = $store_id->store_id;
//                                $custom_productChannelModel->product_id = $last_pro_id;
//                                $custom_productChannelModel->elliot_user_id = $user_id;
//                                $custom_productChannelModel->created_at = date('Y-m-d h:i:s', time());
//                                $custom_productChannelModel->save(false);
//
//                                $product_abberivation = new ProductAbbrivation();
//                                $product_abberivation->channel_abb_id = 'MG0000';
//                                $product_abberivation->product_id = $last_pro_id;
//                                $product_abberivation->price = $price;
//                                $product_abberivation->salePrice = $price;
//                                $product_abberivation->schedules_sales_date = date('Y-m-d', time());
//                                $product_abberivation->stock_quantity = 0;
//                                $product_abberivation->stock_level = 'Out of Stock';
//                                $product_abberivation->channel_accquired = 'Magento';
//                                $product_abberivation->created_at = date('Y-m-d H:i:s', time());
//                                $product_abberivation->updated_at = date('Y-m-d H:i:s', time());
//                                $product_abberivation->save(false);
//                            } else {
//                                $last_pro_id = $checkProductIdAnother->id;
//                                $product_abberivation = new ProductAbbrivation();
//                                $product_abberivation->channel_abb_id = 'MG0000';
//                                $product_abberivation->product_id = $last_pro_id;
//                                $product_abberivation->price = $price;
//                                $product_abberivation->salePrice = $price;
//                                $product_abberivation->schedules_sales_date = date('Y-m-d', time());
//                                $product_abberivation->stock_quantity = 0;
//                                $product_abberivation->stock_level = 'Out of Stock';
//                                $product_abberivation->channel_accquired = 'Magento';
//                                $product_abberivation->created_at = date('Y-m-d H:i:s', time());
//                                $product_abberivation->updated_at = date('Y-m-d H:i:s', time());
//                                $product_abberivation->save(false);
//
//                                $custom_productChannelModel = new ProductChannel;
//                                $store_id = Stores::find()->select('store_id')->where(['store_name' => 'Magento'])->one();
//                                $custom_productChannelModel->store_id = $store_id->store_id;
//                                $custom_productChannelModel->product_id = $last_pro_id;
//                                $custom_productChannelModel->elliot_user_id = $user_id;
//                                $custom_productChannelModel->status = 'no';
//                                $custom_productChannelModel->created_at = date('Y-m-d h:i:s', time());
//                                $custom_productChannelModel->save(false);
//                            }
//                        } else {
//                            $last_pro_id = $checkProductId->product_id;
//                            $prev_qty = $checkProductId->stock_quantity;
//                            $checkProductId->stock_quantity = $prev_qty - $quantity;
//                            $checkProductId->save(false);
//                        }
//                        $order_products_model = new OrdersProducts;
//                        $order_products_model->order_Id = $last_order_id;
//                        $order_products_model->product_Id = $last_pro_id;
//                        $order_products_model->qty = $quantity;
//                        $order_products_model->order_product_sku = $sku;
//                        $order_products_model->created_at = date('Y-m-d h:i:s', time());
//                        $order_products_model->elliot_user_id = $user_id;
//                        $order_products_model->save(false);
//                    endforeach;
//                } else {
//                    $order_model = new Orders();
//                    $order_model->elliot_user_id = $user_id;
//                    $order_model->channel_abb_id = 'MG' . $order_id;
//                    $order_model->customer_id = $elliot_customer_id;
//                    $order_model->order_status = $order_status_2;
//                    $order_model->product_qauntity = $order_product_qty;
//                    $order_model->shipping_address = $shipping_address;
//                    $order_model->ship_street_1 = $ship_address_1;
//                    $order_model->ship_street_2 = $ship_address_2;
//                    $order_model->ship_city = $ship_city;
//                    $order_model->ship_state = $ship_state;
//                    $order_model->ship_zip = $ship_zip;
//                    $order_model->ship_country = $ship_country_id;
//                    $order_model->billing_address = $billing_address;
//                    $order_model->bill_street_1 = $bill_address_1;
//                    $order_model->bill_street_2 = $ship_address_2;
//                    $order_model->bill_city = $bill_city;
//                    $order_model->bill_state = $bill_state;
//                    $order_model->bill_zip = $bill_zip;
//                    $order_model->bill_country = $bill_country_id;
//                    $order_model->base_shipping_cost = $order_shipping_amount;
//                    $order_model->shipping_cost_tax = $order_tax_amount;
//                    $order_model->payment_method = $payment_method;
//                    $order_model->payment_status = $order_status_2;
//                    $order_model->refunded_amount = $refund_amount;
//                    $order_model->discount_amount = $discount_amount;
//                    $order_model->total_amount = $order_total;
//                    $order_model->magento_store_id = $order_store_id;
//                    $order_model->order_date = $order_created_at;
//                    $order_model->updated_at = date('Y-m-d h:i:s', strtotime($order_updated_at));
//                    $order_model->created_at = date('Y-m-d h:i:s', strtotime($order_created_at));
//                    $order_model->save(false);
//                    $last_order_id = $order_model->order_ID;
//
//                    $store = Stores::find()->where(['store_name' => 'Magento'])->one();
//                    $store_id = $store->store_id;
//                    $order_channels_model = new OrderChannel();
//                    $order_channels_model->elliot_user_id = $user_id;
//                    $order_channels_model->order_id = $last_order_id;
//                    $order_channels_model->store_id = $store_id;
//                    $order_channels_model->created_at = date('Y-m-d h:i:s', strtotime($order_created_at));
//                    $order_channels_model->save(false);
//
//                    /**
//                     * Order items
//                     */
//                    $order_items = $order_data['order_items'];
//                    foreach ($order_items as $_items):
//                        $title = $_items['name'];
//                        $quantity = $_items['qty_ordered'];
//                        $price = $_items['price'];
//                        $sku = $_items['sku'];
//                        $product_id = $_items['product_id'];
//                        $product_weight = $_items['weight'];
//                        $checkProductId = ProductAbbrivation::find()->where(['channel_abb_id' => 'MG' . $product_id])->one();
//                        if (empty($checkProductId)) {
//                            $checkProductIdAnother = Products::find()->where(['SKU' => $sku])->one();
//                            if (empty($checkProductIdAnother)) {
//                                $productModel = new Products ();
//                                $productModel->channel_abb_id = 'MG0000';
//                                $productModel->product_name = $title;
//                                $productModel->SKU = $sku;
//                                $productModel->UPC = '';
//                                $productModel->EAN = '';
//                                $productModel->Jan = '';
//                                $productModel->ISBN = '';
//                                $productModel->MPN = '';
//                                $productModel->description = '';
//                                $productModel->adult = 'no';
//                                $productModel->age_group = NULL;
//                                $productModel->gender = 'Unisex';
//                                $productModel->brand = '';
//                                $productModel->stock_quantity = 1;
//                                $productModel->availability = 'Out of Stock';
//                                $productModel->stock_level = 'Out of Stock';
//                                $productModel->stock_status = 'Hidden';
//                                $productModel->product_status = 'in_active';
//                                $productModel->price = $price;
//                                $productModel->sales_price = $price;
//                                $productModel->elliot_user_id = $user_id;
//                                $productModel->magento_store_id = $order_store_id;
//                                $productModel->save(false);
//                                $last_pro_id = $productModel->id;
//
//                                $custom_productChannelModel = new ProductChannel;
//                                $store_id = Stores::find()->select('store_id')->where(['store_name' => 'Magento'])->one();
//                                $custom_productChannelModel->store_id = $store_id->store_id;
//                                $custom_productChannelModel->product_id = $last_pro_id;
//                                $custom_productChannelModel->elliot_user_id = $user_id;
//                                $custom_productChannelModel->created_at = date('Y-m-d h:i:s', time());
//                                $custom_productChannelModel->save(false);
//
//                                $product_abberivation = new ProductAbbrivation();
//                                $product_abberivation->channel_abb_id = 'MG0000';
//                                $product_abberivation->product_id = $last_pro_id;
//                                $product_abberivation->price = $price;
//                                $product_abberivation->salePrice = $price;
//                                $product_abberivation->schedules_sales_date = date('Y-m-d', time());
//                                $product_abberivation->stock_quantity = 0;
//                                $product_abberivation->stock_level = 'Out of Stock';
//                                $product_abberivation->channel_accquired = 'Magento';
//                                $product_abberivation->created_at = date('Y-m-d H:i:s', time());
//                                $product_abberivation->updated_at = date('Y-m-d H:i:s', time());
//                                $product_abberivation->save(false);
//                            } else {
//                                $last_pro_id = $checkProductIdAnother->id;
//                                $product_abberivation = new ProductAbbrivation();
//                                $product_abberivation->channel_abb_id = 'MG0000';
//                                $product_abberivation->product_id = $last_pro_id;
//                                $product_abberivation->price = $price;
//                                $product_abberivation->salePrice = $price;
//                                $product_abberivation->schedules_sales_date = date('Y-m-d', time());
//                                $product_abberivation->stock_quantity = 0;
//                                $product_abberivation->stock_level = 'Out of Stock';
//                                $product_abberivation->channel_accquired = 'Magento';
//                                $product_abberivation->created_at = date('Y-m-d H:i:s', time());
//                                $product_abberivation->updated_at = date('Y-m-d H:i:s', time());
//                                $product_abberivation->save(false);
//
//                                $custom_productChannelModel = new ProductChannel;
//                                $store_id = Stores::find()->select('store_id')->where(['store_name' => 'Magento'])->one();
//                                $custom_productChannelModel->store_id = $store_id->store_id;
//                                $custom_productChannelModel->product_id = $last_pro_id;
//                                $custom_productChannelModel->elliot_user_id = $user_id;
//                                $custom_productChannelModel->status = 'no';
//                                $custom_productChannelModel->created_at = date('Y-m-d h:i:s', time());
//                                $custom_productChannelModel->save(false);
//                            }
//                        } else {
//                            $last_pro_id = $checkProductId->product_id;
//                            $prev_qty = $checkProductId->stock_quantity;
//                            $checkProductId->stock_quantity = $prev_qty - $quantity;
//                            $checkProductId->save(false);
//                        }
//                        $order_products_model = new OrdersProducts;
//                        $order_products_model->order_Id = $last_order_id;
//                        $order_products_model->product_Id = $last_pro_id;
//                        $order_products_model->qty = $quantity;
//                        $order_products_model->order_product_sku = $sku;
//                        $order_products_model->created_at = date('Y-m-d h:i:s', time());
//                        $order_products_model->elliot_user_id = $user_id;
//                        $order_products_model->save(false);
//                    endforeach;
//                }
            }
        }
    }

    public function actionMagentoCustomerCreate() {
        $url_data = $_POST['data'];
        $url_data = urldecode($url_data);
        $customer_data = json_decode($url_data, true);
        $shop_url = $customer_data['magento_url'];
        $url_parse = parse_url($shop_url);
        $shop_url_1 = '';
        if ($url_parse['scheme'] == 'https') {
            $shop_url_1 = str_replace("https", 'http', $shop_url);
        } else {
            $shop_url_1 = str_replace("http", 'https', $shop_url);
        }

        $get_users = User::find()->where("magento_shop_url IN('" . $shop_url . "','" . $shop_url_1 . "')")->all();
        foreach ($get_users as $_user) {
            $config = yii\helpers\ArrayHelper::merge(
                            require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $_user->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
            );
            $application = new yii\web\Application($config);

            $user_id = $_user->id;
            $get_magento_id = Stores::find()->select('store_id')->where(['store_name' => 'Magento'])->one();
            $magento_store_id = $get_magento_id->store_id;
            $store_magento = StoresConnection::find()->where(['store_id' => $magento_store_id, 'user_id' => $user_id])->one();
            if (!empty($store_magento)) {
                //$magento_shop = $store_magento->mag_shop;
                //$magento_soap_user = $store_magento->mag_soap_user;
                //$magento_soap_api_key = $store_magento->mag_soap_api;

                $checkCustomerModel = CustomerUser::find()->where(['channel_abb_id' => 'MG' . $customer_data['customer_id']])->one();
                if (empty($checkCustomerModel)):
                    $Customers_model = new CustomerUser();
                    $Customers_model->channel_abb_id = 'MG' . $customer_data['customer_id'];
                    $Customers_model->first_name = $customer_data['firstname'];
                    $Customers_model->last_name = $customer_data['lastname'];
                    $Customers_model->email = $customer_data['email'];
                    $Customers_model->channel_acquired = 'Magento';
                    $Customers_model->date_acquired = date('Y-m-d h:i:s', strtotime($customer_data['created_at']));
                    $Customers_model->street_1 = '';
                    $Customers_model->street_2 = '';
                    $Customers_model->city = '';
                    $Customers_model->country = '';
                    $Customers_model->state = '';
                    $Customers_model->zip = '';
                    $Customers_model->phone_number = '';
                    $Customers_model->ship_street_1 = '';
                    $Customers_model->ship_city = '';
                    $Customers_model->ship_state = '';
                    $Customers_model->ship_zip = '';
                    $Customers_model->ship_country = '';
                    $Customers_model->magento_store_id = $customer_data['website_id'];
                    $Customers_model->created_at = date('Y-m-d h:i:s', strtotime($customer_data['created_at']));
                    $Customers_model->updated_at = date('Y-m-d h:i:s', strtotime($customer_data['updated_at']));
                    //Save Elliot User id
                    $Customers_model->elliot_user_id = $user_id;
                    $Customers_model->save(false);
                endif;
            }
        }
    }

    public function actionMagentoCustomerAddressUpdate() {
        $url_data = $_POST['data'];
        $url_data = urldecode($url_data);
        $customer_data = json_decode($url_data, true);
        $shop_url = $customer_data['magento_url'];
        $url_parse = parse_url($shop_url);
        $shop_url_1 = '';
        if ($url_parse['scheme'] == 'https') {
            $shop_url_1 = str_replace("https", 'http', $shop_url);
        } else {
            $shop_url_1 = str_replace("http", 'https', $shop_url);
        }

        $street_add_1 = $customer_data['street'][0];
        $street_add_2 = '';
        if (array_key_exists(1, $customer_data['street'])) {
            $street_add_2 = $customer_data['street'][1];
        }
        $get_users = User::find()->where("magento_shop_url IN('" . $shop_url . "','" . $shop_url_1 . "')")->all();
        foreach ($get_users as $_user) {
            $config = yii\helpers\ArrayHelper::merge(
                            require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $_user->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
            );
            $application = new yii\web\Application($config);
            $user_id = $_user->id;
            $get_magento_id = Stores::find()->select('store_id')->where(['store_name' => 'Magento'])->one();
            $magento_store_id = $get_magento_id->store_id;
            $store_magento = StoresConnection::find()->where(['store_id' => $magento_store_id, 'user_id' => $user_id])->one();
            if (!empty($store_magento)) {
                $Customers_model = CustomerUser::find()->where(['channel_abb_id' => 'MG' . $customer_data['customer_id']])->one();
                if (!empty($Customers_model)):
                    $Customers_model->street_1 = $street_add_1;
                    $Customers_model->street_2 = $street_add_2;
                    $Customers_model->city = $customer_data['city'];
                    $Customers_model->country = $customer_data['country_id'];
                    $Customers_model->state = $customer_data['region'];
                    $Customers_model->zip = $customer_data['postcode'];
                    $Customers_model->phone_number = $customer_data['telephone'];
                    $Customers_model->ship_street_1 = $street_add_1;
                    $Customers_model->ship_street_2 = $street_add_2;
                    $Customers_model->ship_city = $customer_data['city'];
                    $Customers_model->ship_state = $customer_data['region'];
                    $Customers_model->ship_zip = $customer_data['postcode'];
                    $Customers_model->ship_country = $customer_data['country_id'];
                    $Customers_model->save(false);
                endif;
            }
        }
    }

    /*
     * ajax hit for make area chart dynamic
     * area chart use on the peopal section on view all
     */

    public function actionAreachartonpeople() {
        if (!empty($_POST['data'])) {
            $post = $_POST['data'];
        }
        $currentmonth = date('m');
        $currentyear = date('Y');
        $current_date = date('Y-m-d', time());
        $connectedchannel = ChannelConnection::find()->where(['user_id' => Yii::$app->user->identity->id])->all();
        $connectedstore = StoresConnection::find()->where(['user_id' => Yii::$app->user->identity->id])->all();
        $connecteduser = array_merge($connectedchannel, $connectedstore);
        if (!empty($connecteduser)) {
            $arr = array();
            $country = '';
            foreach ($connecteduser as $connectedstore) {
                if (!empty($connectedstore->channel_id)) {
                    $channelname = Channels::find()->where(['channel_ID' => $connectedstore->channel_id])->one();
                    if (($channelname->channel_name) == 'Facebook' || ($channelname->channel_name) == 'Google shopping') {
                        continue;
                    } elseif (($channelname->channel_name) == 'Service Account' || ($channelname->channel_name) == 'Subscription Account') {
                        $name = 'WeChat';
                    } else {
                        $storename = Channels::find()->where(['channel_ID' => $connectedstore->channel_id])->one();
                        $name = $storename->channel_name;
                    }
                } else if (!empty($connectedstore->store_id)) {
                    //                    $storename = Stores::find()->where(['store_id' => $connectedstore->store_id])->one();
                    //                    $name = $storename->store_name;

                    $storename = StoreDetails::find()->where(['store_connection_id' => $connectedstore->stores_connection_id])->one();
                    $name = $storename->channel_accquired;
                    $country = $storename->country;
                }
                $fin_name = trim($name . ' ' . $country);

                /*                 * *********************** FOR TODAY************************ */
                if ($post == 'areacharttoday' || $post == 'areacharttodaymob') {

                    $current_date = date('Y-m-d', time());
                    $j = 0;
                    for ($i = 1; $i <= 24; $i++):
                        if ($i == 1) {
                            $current_date_hour = date('Y-m-d h:i:s', time());
                        } else {
                            $current_date_hour = date('Y-m-d h:i:s', strtotime('-' . $j . ' hour'));
                        }
                        $previous_hour = date('Y-m-d h:i:s', strtotime('-' . $i . ' hour'));
                        $previous_hour1 = date('h:i:s', strtotime('-' . $i . ' hour'));
                        $date_check = date('Y-m-d', strtotime($previous_hour));
                        if ($date_check == $current_date):
                            $current_time = date('Y-m-d');
                            $connection = \Yii::$app->db;
                            //$orders_data = $connection->createCommand('SELECT * from order_channel Where ' . $fieldname . '="' . $store_channel_id . '" AND date(created_at)="' . $current_date . '"  ');
                            $orders_data = $connection->createCommand('select * from Customer_User WHERE channel_acquired="' . $name . '"  AND created_at BETWEEN ("' . $previous_hour . '") AND ("' . $current_date_hour . '") AND date(created_at)="' . $current_date . '" ');
                            $orders_count = count($orders_data->queryAll());
                            $today[] = array('name' => $fin_name, 'user' => $orders_count, 'date' => $previous_hour1);
                        // $arr=array_reverse($arr);
                        endif;
                        $j++;
                    endfor;
                }
                /*                 * *********************** FOR WEEKLY************************ */
                if ($post == 'areachartweek' || $post == 'areachartweekmob') {
                    for ($j = 7; $j >= 1; $j--):
                        $Week_previous_date = date('Y-m-d', strtotime('-' . $j . ' days'));
                        $connection = \Yii::$app->db;
                        $orders_data = $connection->createCommand('SELECT * from Customer_User Where channel_acquired="' . $name . '" AND date(created_at)="' . $Week_previous_date . '"  ');
                        $orders_count = count($orders_data->queryAll());
                        $arr[] = array('name' => $fin_name, 'user' => $orders_count, 'date' => $Week_previous_date);
                    endfor;
                }
                /*                 * *********************** FOR MONTHLY************************ */
                if ($post == 'areachartmonth' || $post == 'areachartmonthmob') {
                    for ($j = ($currentmonth - 1); $j >= 0; $j--):
                        $Week_previous_date = date('m', strtotime('-' . $j . ' month'));
                        $Week_pre_date = date('M', strtotime('-' . $j . ' month'));
                        $connection = \Yii::$app->db;
                        $orders_data = $connection->createCommand('SELECT * from Customer_User Where channel_acquired="' . $name . '" AND month(created_at)="' . $Week_previous_date . '" AND year(created_at)="' . $currentyear . '"');
                        $orders_count = count($orders_data->queryAll());
                        $arr[] = array('name' => $fin_name, 'user' => $orders_count, 'date' => $Week_pre_date . ' ' . $currentyear);
                    endfor;
                }
                /*                 * *********************** FOR QUARTERLY************************ */
                if ($post == 'areachartQuarter' || $post == 'areachartQuartermob') {
                    $data1_m = floor($currentmonth / 4);
                    $data2_m = $currentmonth % 4;
                    if ($data2_m >= 1) {
                        $data3_m = $data1_m + 1;
                    } else {
                        $data3_m = $data1_m;
                    }
                    $i = $currentmonth - 1;
                    for ($j = $data3_m; $j >= 1; $j--):
                        $Week_previous_date = date('m', strtotime('-' . $i . ' month'));
                        $Week_pre_date = $Week_previous_date + 3;
                        //$monthNum = 4;
//echo $monthName = date("M", mktime(0, 0, 0, $monthNum, 10));
                        $i = $i - 4;
                        $connection = \Yii::$app->db;
                        $orders_data = $connection->createCommand('SELECT * from Customer_User Where channel_acquired="' . $name . '" AND month(created_at) BETWEEN"' . $Week_previous_date . '" AND"' . $Week_pre_date . '" AND year(created_at)="' . $currentyear . '"');
                        $orders_count = count($orders_data->queryAll());
                        $arr[] = array('name' => $fin_name, 'user' => $orders_count, 'date' => date("M", mktime(0, 0, 0, $Week_previous_date, 10)) . '-' . date("M", mktime(0, 0, 0, $Week_pre_date, 10)));
                    endfor;
                }
                /*                 * *********************** FOR YEARLY************************ */
                if ($post == 'areachartyear' || $post == 'areachartyearmob') {
                    for ($j = 9; $j >= 0; $j--):
                        $Week_previous_date = date('Y', strtotime('-' . $j . ' years'));
                        $connection = \Yii::$app->db;
                        $orders_data = $connection->createCommand('SELECT * from Customer_User Where channel_acquired="' . $name . '" AND year(created_at)="' . $Week_previous_date . '"  ');
                        $orders_count = count($orders_data->queryAll());
                        $arr[] = array('name' => $fin_name, 'user' => $orders_count, 'date' => $Week_previous_date);
                    endfor;
                }
            }
        }

        if (!empty($today)) {
            $arr = array_reverse($today);
        }
        $array1 = array();
        foreach ($arr as $a):
            /* Check if key exists or not */
            $dates = $a['date'];
            if (array_key_exists($dates, $array1)) {
                $array1[$dates][] = array(
                    'name' => $a['name'],
                    'user' => $a['user'],
                );
            } else {
                $array1[$dates][] = array(
                    'name' => $a['name'],
                    'user' => $a['user'],
                );
            }
        endforeach;

        $data_val_2 = array();
        foreach ($array1 as $_data => $value) {
            $data_val = array();
            foreach ($value as $_value) {
                $data_val['period'] = $_data;
                //$data_val[$_value['name']]=$_value['name'];
                $data_val[$_value['name'] . " Customers"] = $_value['user'];
            }
            $data_val_2[] = $data_val;
        }

        $keyname = array();
        $keyname_2 = array();
        foreach ($data_val as $key => $val) {
            if ($key == 'period') {
                
            } else {
                $keyname[] = $key;
            }
        }
        $keyname_2 = $keyname;

        /*
         * array to store hex color 
         */
        $colorarr = array(
            0 => '#0091ea',
            1 => '#00b0ff',
            2 => '#40c4ff',
            3 => '#80d8ff',
            4 => '#01579b',
            5 => '#0277bd',
            6 => '#039be5',
            7 => '#03a9f4',
            8 => '#b3e5fc',
            9 => '#81d4fa',
            10 => '#29b6f6',
            11 => '#e1f5fe',
            12 => '#b5af79',
            13 => '#914f84',
            14 => '#ce6d87',
            15 => '#e04a72',
        );
        $randcolor = array();
        $randcolor_2 = array();
        $showcolorhtml = '';
        $c = 0;
        $cc = 0;


        foreach ($keyname as $color) {
            // $randcolor[] = '#' . random_color();
            $randcolor[] = $colorarr[$c++];
            //$strname = explode(' ', $color);
            $strname = trim(str_replace("Customers", "", $color));
            $showcolorhtml .= '<li><span data-color="main-chart-color" class="BigCommerce connected_store_name" style="background-color: ' . $colorarr[$cc++] . ';"></span>' . $strname . '</li>';
        }

        $randcolor_2 = $randcolor;
        $data = array('data' => json_encode($data_val_2), 'ykeyslabels' => json_encode($keyname_2), 'linecolors' => json_encode($randcolor_2), 'showcolorhtml' => $showcolorhtml);
        return \yii\helpers\Json::encode($data);
    }

    public function actionCreatecountryajax() {

        if (!empty($_POST['country_name'])) {

            $country = $_POST['country_name'];
            $country_query = Countries::find()->where(['name' => $country])->one();
            $countryID = $country_query->id;

            $states_query = States::find()->where(['country_id' => $countryID])->all();
            foreach ($states_query as $states) {
                echo '<option value="' . $states->id . '">' . $states->name . '</option>';
            }
        }

        if (!empty($_POST['state_ID'])) {
            $state_id = $_POST['state_ID'];

            $city_query = Cities::find()->where(['state_id' => $state_id])->all();
            foreach ($city_query as $cities) {
                echo '<option value="' . $cities->name . '">' . $cities->name . '</option>';
            }
        }


        if (!empty($_POST['ship_countryName'])) {
            $ship_country = $_POST['ship_countryName'];
            $shipcountry_query = Countries::find()->where(['name' => $ship_country])->one();
            $shipCountryID = $shipcountry_query->id;

            $states_query = States::find()->where(['country_id' => $shipCountryID])->all();
            foreach ($states_query as $ship_states) {
                echo '<option value="' . $ship_states->id . '">' . $ship_states->name . '</option>';
            }
        }

        if (!empty($_POST['ship_StateID'])) {

            $ship_stateID = $_POST['ship_StateID'];

            $shipcity_query = Cities::find()->where(['state_id' => $ship_stateID])->all();
            foreach ($shipcity_query as $shipcity) {
                echo '<option value="' . $shipcity->name . '">' . $shipcity->name . '</option>';
            }
        }
    }

    public function actionTestingCalls() {
       
    $connection     = \Yii::$app->db;
    $model          = $connection->createCommand('SELECT `orderSum`.*,`Customer_User`.* FROM `Customer_User` Left JOIN 
			    (SELECT `customer_id`,count(customer_id) as count, SUM(total_amount) as total FROM `orders` GROUP BY `customer_id`)
			    `orderSum` ON orderSum.customer_id = Customer_User.customer_ID ORDER BY total DESC limit 25');
    $clv_data       = $model->queryAll();
    $json = json_encode($clv_data);
    $object = json_decode($json);
    echo'<pre>';
    print_r($object);
    die('end here');
        
        $query = CustomerUser::find();
        $subQuery = Orders::find()
                ->select('customer_id, SUM(total_amount) as total')
                ->groupBy('customer_id');
        $query->leftJoin(['orderSum' => $subQuery], 'orderSum.customer_id = Customer_User.customer_ID');
        
        $models = $query->all();
 
      //  $query->all();
        echo'<pre>';
        print_r($models);
        die('end here');




        //$data = CustomerUser::find()->with('orders')->limit(10)->all();
        echo"aas";
        $data = CustomerUser::find()->with([
                    'orders' => function ($query) {
                        $query->select('customer_id,sum(total_amount) as total')->groupBy('customer_id');
                        // SELECT SUM(total_amount), customer_id FROM orders WHERE customer_id=' . $customer_id . ' GROUP BY customer_id
                    }
                ])->asArray()->limit(25)->all();

        echo'<pre>';
        print_r($data);
        die('end here');

        $orders = Orders::find()->joinWith([
                    'ordersProducts' => function($query) {
                        $query->alias('YO');
                    }])->all();


        echo'ithe';


        //$orders=Orders::find()->all();
        echo'<pre>';
        print_r($orders);
//        $data= json_encode($orders);
//        echo'<pre>';
//        print_r($data);
        die('sdfsdfsfd');

        Bigcommerce::configure(array(
            'client_id' => 'bmvizwfqt2dz6pezddtvscr7p4mlbbe',
            'auth_token' => '9hvduwpe13idvx30x2e8mzlvo8dvrhm',
            'store_hash' => 'lhg3t5e'
        ));

        $customers = Bigcommerce::getOrders();
        echo '<pre>';
        print_r($customers);
    }

    public function actionInactiveCustomers() {

        return $this->render('inactivecustomer');
    }

}
