<?php

namespace backend\controllers;

use Yii;
use backend\models\OrdersSerach;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\CustomerUser;
use yii\filters\AccessControl;
use backend\models\User;
use backend\models\Stores;
use backend\models\Orders;
use Bigcommerce\Api\Client as Bigcommerce;
use backend\models\ChannelConnection;
use backend\models\StoresConnection;
use backend\models\StoreDetails;
use backend\models\OrderChannel;
use backend\models\Channels;
use backend\models\MagentoStores;
use backend\models\OrdersProducts;
use backend\controllers\ChannelsController;
use backend\models\OrderFullfillment;
use kartik\mpdf\Pdf;

/**
 * OrdersController implements the CRUD actions for Orders model.
 */
class OrdersController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'invoice', 'createorderhook', 'bigorder', 'connected', 'inactive-orders'],
                'rules' => [
                        [
                        'actions' => ['signup', 'createorderhook', 'bigorder'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                        [
                        'actions' => ['logout', 'index', 'view', 'create', 'update', 'invoice', 'createorderhook', 'bigorder', 'connected', 'inactive-orders'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new OrdersSerach();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Orders Invoice.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionInvoice() {
        return $this->render('invoice');
    }

    /**
     * Connected Orders.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionConnected() {
        return $this->render('connected');
    }

    /**
     * Displays a single Orders model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {

//        $customer_data = CustomerUser::find()->Where(['customer_ID' => $id])->one();
        return $this->render('view', [
                    'model' => $this->findModel($id),
                  //  'Customer' => $customer_data,
        ]);
    }

    /**
     * Creates a new Orders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Orders();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->order_ID]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Orders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->order_ID]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Orders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //Create Order hook 

    public function actionOrderhookcreate() {
        Bigcommerce::configure(array(
            'client_id' => 'aycuauwggn61cp9mu7phkqv0y9fm4ka',
            'auth_token' => '6xz40wg5pif7bujbl9et1p3e23uebet',
            'store_hash' => 'u79a2svh'
        ));


        $ping = Bigcommerce::getTime();
        $hookrs = Bigcommerce::deleteWebhook(12372060);
        $hookrs = Bigcommerce::createWebhook(array(
                    'scope' => 'store/order/*',
                    'destination' => 'https://aarush2.s86.co/orders/bigorder?id=433',
                    'is_active' => true
        ));

        //  $hookrs= Bigcommerce::deleteWebhook(12372023);
        $hookrs = Bigcommerce::listWebhooks();
        echo'<pre>';
        print_r($hookrs);
        die;
    }

    public function actionBigorder($id) {

//        $get_big_id_users = User::find()->where(['id' => $id])->one();
//       
//        
//        $config = yii\helpers\ArrayHelper::merge(
//                require($_SERVER['DOCUMENT_ROOT'].'/common/config/main.php'),
//                require($_SERVER['DOCUMENT_ROOT'].'/common/users_config/' . $get_big_id_users->domain_name . '/main-local.php'),
//                require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main.php'),
//                require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main-local.php')
//        );
//        $application = new yii\web\Application($config);

        $webhookContent = "test";
        $webhook = fopen('php://input', 'rb');
        while (!feof($webhook)) {
            $webhookContent .= fread($webhook, 4096);
        }

        $myfile = fopen($_SERVER['DOCUMENT_ROOT'] . "/bigcommerce_callback_cat.php", "w") or die("Unable to open file!");
        fwrite($myfile, $webhookContent);
        fclose($myfile);
        echo "write done";
        die;
    }

    public function actionCreateorderhook($id) {

        $get_big_id_users = User::find()->where(['id' => $id])->one();

        $config = yii\helpers\ArrayHelper::merge(
                        require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_big_id_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);

        $webhookContent = "";
        $webhook = fopen('php://input', 'rb');
        while (!feof($webhook)) {
            $webhookContent .= fread($webhook, 4096);
        }

        $data = json_decode($webhookContent);
        $data_id = $data->data->id;
        //$data_id = 100;
        // Get api details from db
//        $Stores_data = Stores::find()->Where(['store_name' => 'BigCommerce'])->with('storeconnection')->one();
//        $big_client_id = $Stores_data->storeconnection->big_client_id;
//        $auth_token = $Stores_data->storeconnection->big_access_token;
//        $big_store_hash = $Stores_data->storeconnection->big_store_hash;

        Bigcommerce::configure(array(
            'client_id' => 'aycuauwggn61cp9mu7phkqv0y9fm4ka',
            'auth_token' => '6xz40wg5pif7bujbl9et1p3e23uebet',
            'store_hash' => 'u79a2svh'
        ));


        $ping = Bigcommerce::getTime();

        echo $ping->format('H:i:s');

        $myfile = fopen($_SERVER['DOCUMENT_ROOT'] . "/bigcommerce_callback_cat.php", "w") or die("Unable to open file!");
        fwrite($myfile, $webhookContent);
        fclose($myfile);
        die;
        if ($data->data->type == "order" && $data->scope == "store/order/updated"):

            $myfile = fopen($_SERVER['DOCUMENT_ROOT'] . "/bigcommerce_callback_cat.php", "w") or die("Unable to open file!");
            fwrite($myfile, "test");
            fclose($myfile);
            $orders_data = Bigcommerce::getResource('/orders/' . $data_id, 'Order');
            //for shipping address
            $ship_details = Bigcommerce::getCollection('/orders/' . $data_id . '/shipping_addresses', 'Order');
            $order_status = $orders_data->status;
            $product_qauntity = $orders_data->items_total;

            //ship address
            $ship_street_1 = isset($ship_details[0]->street_1) ? $ship_details[0]->street_1 : $orders_data->billing_address->street_1;
            $ship_street_2 = isset($ship_details[0]->street_2) ? $ship_details[0]->street_1 : $orders_data->billing_address->street_2;
            $ship_city = isset($ship_details[0]->city) ? $ship_details[0]->city : $orders_data->billing_address->city;
            $ship_state = isset($ship_details[0]->state) ? $ship_details[0]->city : $orders_data->billing_address->state;
            $ship_zip = isset($ship_details[0]->zip) ? $ship_details[0]->zip : $orders_data->billing_address->zip;
            $ship_country = isset($ship_details[0]->country) ? $ship_details[0]->country : $orders_data->billing_address->country;

            $shipping_address = $ship_street_1 . ',' . $ship_street_2 . ',' . $ship_city . ',' . $ship_state . ',' . $ship_zip . ',' . $ship_country;


            //Bill Addresss
            $billing_add1 = isset($orders_data->billing_address->street_1) ? $orders_data->billing_address->street_1 : "" . ',' . isset($orders_data->billing_address->street_2) ? $orders_data->billing_address->street_2 : "";
            $billing_add2 = isset($orders_data->billing_address->city) ? $orders_data->billing_address->city : "" . ',' . isset($orders_data->billing_address->state) ? $orders_data->billing_address->state : "" .
                    ',' . isset($orders_data->billing_address->zip) ? $orders_data->billing_address->zip : "" . ',' . isset($orders_data->billing_address->country) ? $orders_data->billing_address->country : "";
            $billing_address = $billing_add1 . ',' . $billing_add2;

            $total_amount = $orders_data->total_inc_tax;
            $base_shipping_cost = $orders_data->base_shipping_cost;
            $shipping_cost_tax = $orders_data->shipping_cost_tax;
            $base_handling_cost = $orders_data->base_handling_cost;
            $handling_cost_tax = $orders_data->handling_cost_tax;
            $base_wrapping_cost = $orders_data->base_wrapping_cost;
            $wrapping_cost_tax = $orders_data->wrapping_cost_tax;
            $payment_method = $orders_data->payment_method;
            $payment_provider_id = $orders_data->payment_provider_id;
            $payment_status = $orders_data->payment_status;
            $refunded_amount = $orders_data->refunded_amount;
            $discount_amount = $orders_data->discount_amount;
            $coupon_discount = $orders_data->coupon_discount;
            $order_last_modified_date = $orders_data->date_modified;

            $ab_id = "BGC" . $data_id;
            $orders_data = Orders::find()->Where(['channel_abb_id' => $ab_id])->one();

            $orders_data->order_status = $order_status;
            $orders_data->product_qauntity = $product_qauntity;
            $orders_data->ship_street_1 = $ship_street_1;
            $orders_data->ship_street_2 = $ship_street_2;
            $orders_data->ship_city = $ship_city;
            $orders_data->ship_state = $ship_state;
            $orders_data->ship_zip = $ship_zip;
            $orders_data->ship_country = $ship_country;
            $orders_data->shipping_address = $shipping_address;
            $orders_data->billing_address = $billing_address;
            $orders_data->base_shipping_cost = $base_shipping_cost;
            $orders_data->shipping_cost_tax = $shipping_cost_tax;
            $orders_data->base_handling_cost = $base_handling_cost;
            $orders_data->handling_cost_tax = $handling_cost_tax;
            $orders_data->base_wrapping_cost = $base_wrapping_cost;
            $orders_data->wrapping_cost_tax = $wrapping_cost_tax;
            $orders_data->payment_method = $payment_method;
            $orders_data->payment_provider_id = $payment_provider_id;
            $orders_data->payment_status = $payment_status;
            $orders_data->refunded_amount = $refunded_amount;
            $orders_data->discount_amount = $discount_amount;
            $orders_data->coupon_discount = $coupon_discount;
            $orders_data->total_amount = $total_amount;
            $orders_data->updated_at = date('Y-m-d h:i:s', strtotime($order_last_modified_date));

            if ($orders_data->save(false)):

                $myfile = fopen($_SERVER['DOCUMENT_ROOT'] . "/bigcommerce_callback_cat.php", "w") or die("Unable to open file!");
                fwrite($myfile, "update");
                fclose($myfile);
            endif;
        endif;
    }

    /*
     * order chart ajax action
     */

    public function actionAreachartonorders() {
        if (!empty($_POST['data'])) {
            $post = $_POST['data'];
        }
        $currentmonth = date('m');
        $currentyear = date('Y');
        $current_date = date('Y-m-d', time());
        $connectedchannel = ChannelConnection::find()->where(['user_id' => Yii::$app->user->identity->id])->all();
        $connectedstore = StoresConnection::find()->where(['user_id' => Yii::$app->user->identity->id])->all();
        $connecteduser = array_merge($connectedchannel, $connectedstore);

        if (!empty($connecteduser)) {
            $arr = array();
            $country = '';
            foreach ($connecteduser as $connectedstore) {
                if (!empty($connectedstore->channel_id)) {
                    $channelname = Channels::find()->where(['channel_ID' => $connectedstore->channel_id])->one();
                    $fieldname = 'channel_id';
                    if (($channelname->channel_name) == 'Facebook' || ($channelname->channel_name) == 'Google shopping') {
                        continue;
                    } elseif (($channelname->channel_name) == 'Service Account' || ($channelname->channel_name) == 'Subscription Account') {
                        $name = 'WeChat';
                        $store_channel_id = $connectedstore->channel_id;
                    } else {
                        $store_channel_id = $connectedstore->channel_id;
                        $name = $channelname->channel_name;
                    }
                } elseif (!empty($connectedstore->store_id)) {
                    $fieldname = 'store_id';
                    $store_channel_id = $connectedstore->stores_connection_id;
                    $storename = StoreDetails::find()->where(['store_connection_id' => $store_channel_id])->one();

                    $name = $storename->channel_accquired;
                    $country = $storename->country;
                }
                $fin_name = trim($name . ' ' . $country);
                /*                 * *********************** FOR TODAY************************ */
                if ($post == 'ordercharttoday' || $post == 'ordercharttodaymob') {

                    $current_date = date('Y-m-d', time());
                    $j = 0;
                    for ($i = 1; $i <= 24; $i++):
                        if ($i == 1) {
                            $current_date_hour = date('Y-m-d h:i:s', time());
                        } else {
                            $current_date_hour = date('Y-m-d h:i:s', strtotime('-' . $j . ' hour'));
                        }
                        $previous_hour = date('Y-m-d h:i:s', strtotime('-' . $i . ' hour'));
                        $previous_hour1 = date('h:i:s', strtotime('-' . $i . ' hour'));
                        $date_check = date('Y-m-d', strtotime($previous_hour));
                        if ($date_check == $current_date):
                            $current_time = date('Y-m-d');
                            $connection = \Yii::$app->db;
                            //$orders_data = $connection->createCommand('SELECT * from order_channel Where ' . $fieldname . '="' . $store_channel_id . '" AND date(created_at)="' . $current_date . '"  ');
                            $orders_data = $connection->createCommand('select * from order_channel WHERE ' . $fieldname . '="' . $store_channel_id . '"  AND created_at BETWEEN ("' . $previous_hour . '") AND ("' . $current_date_hour . '") AND date(created_at)="' . $current_date . '" ');
                            $orders_count = count($orders_data->queryAll());
                            $today[] = array('name' => $fin_name, 'user' => $orders_count, 'date' => $previous_hour1);
                        // $arr=array_reverse($arr);
                        endif;
                        $j++;
                    endfor;
                }
                /*                 * *********************** FOR WEEKLY************************ */
                if ($post == 'orderchartweek' || $post == 'orderchartweekmob') {
                    for ($j = 7; $j >= 1; $j--):
                        $Week_previous_date = date('Y-m-d', strtotime('-' . $j . ' days'));
                        $connection = \Yii::$app->db;
                        $orders_data = $connection->createCommand('SELECT * from order_channel Where ' . $fieldname . '="' . $store_channel_id . '" AND date(created_at)="' . $Week_previous_date . '"  ');
                        $orders_count = count($orders_data->queryAll());
                        $arr[] = array('name' => $fin_name, 'user' => $orders_count, 'date' => $Week_previous_date);
                    endfor;
                }

                /*                 * *********************** FOR MONTHLY************************ */
                if ($post == 'orderchartmonth' || $post == 'orderchartmonthmob') {
                    for ($j = ($currentmonth - 1); $j >= 0; $j--):
                        $Week_previous_date = date('m', strtotime('-' . $j . ' month'));
                        $Week_pre_date = date('M', strtotime('-' . $j . ' month'));
                        $connection = \Yii::$app->db;
                        $orders_data = $connection->createCommand('SELECT * from order_channel Where ' . $fieldname . '="' . $store_channel_id . '" AND month(created_at)="' . $Week_previous_date . '" AND year(created_at)="' . $currentyear . '"');
                        $orders_count = count($orders_data->queryAll());
                        $arr[] = array('name' => $fin_name, 'user' => $orders_count, 'date' => $Week_pre_date . ' ' . $currentyear);
                    endfor;
                    //  echo $fieldname;echo '<br>';
                    // echo $store_channel_id;echo '<br>';
                }
                /*                 * *********************** FOR Quarterly************************ */
                if ($post == 'orderchartQuater' || $post == 'orderchartQuatermob') {
                    $data1_m = floor($currentmonth / 4);
                    $data2_m = $currentmonth % 4;
                    if ($data2_m >= 1) {
                        $data3_m = $data1_m + 1;
                    } else {
                        $data3_m = $data1_m;
                    }
                    $i = $currentmonth - 1;
                    for ($j = $data3_m; $j >= 1; $j--):
                        $Week_previous_date = date('m', strtotime('-' . $i . ' month'));
                        $Week_pre_date = $Week_previous_date + 3;
                        // echo 'previous'; echo $Week_previous_date;echo 'pre';echo $Week_pre_date;echo '<br>';
                        // $dateObj   = DateTime::createFromFormat('!m', $Week_pre_date);
                        $i = $i - 4;
                        // $k=$k-3;
                        $connection = \Yii::$app->db;
                        $orders_data = $connection->createCommand('SELECT * from order_channel Where ' . $fieldname . '="' . $store_channel_id . '" AND month(created_at) BETWEEN"' . $Week_previous_date . '" AND"' . $Week_pre_date . '" AND year(created_at)="' . $currentyear . '"');
                        $orders_count = count($orders_data->queryAll());
                        // $arr[] = array('name' => $name, 'user' => $orders_count, 'date' => $Week_previous_date . '-' . $Week_pre_date);  
                        $arr[] = array('name' => $fin_name, 'user' => $orders_count, 'date' => date("M", mktime(0, 0, 0, $Week_previous_date, 10)) . ' - ' . date("M", mktime(0, 0, 0, $Week_pre_date, 10)));
                    endfor;
                }

                /*                 * *********************** FOR YEARLY************************ */
                if ($post == 'orderchartyear' || $post == 'orderchartyearmob') {
                    for ($j = 9; $j >= 0; $j--):
                        $Week_previous_date = date('Y', strtotime('-' . $j . ' years'));
                        $connection = \Yii::$app->db;
                        $orders_data = $connection->createCommand('SELECT * from order_channel Where ' . $fieldname . '="' . $store_channel_id . '" AND year(created_at)="' . $Week_previous_date . '"  ');
                        $orders_count = count($orders_data->queryAll());
                        $arr[] = array('name' => $fin_name, 'user' => $orders_count, 'date' => $Week_previous_date);
                    endfor;
                }
            }
        }

        if (!empty($today)) {
            $arr = array_reverse($today);
        }

        $array1 = array();
        foreach ($arr as $a):
            /* Check if key exists or not */
            $dates = $a['date'];
            if (array_key_exists($dates, $array1)) {
                $array1[$dates][] = array(
                    'name' => $a['name'],
                    'user' => $a['user'],
                );
            } else {
                $array1[$dates][] = array(
                    'name' => $a['name'],
                    'user' => $a['user'],
                );
            }
        endforeach;

        $data_val_2 = array();
        foreach ($array1 as $_data => $value) {
            $data_val = array();
            foreach ($value as $_value) {
                $data_val['period'] = $_data;
                //$data_val[$_value['name']]=$_value['name'];
                $data_val[$_value['name']] = $_value['user'];
            }
            $data_val_2[] = $data_val;
        }

        $keyname = array();
        $keyname_2 = array();
        if (!empty($data_val)) {
            foreach ($data_val as $key => $val) {
                if ($key == 'period') {
                    
                } else {
                    $keyname[] = $key;
                }
            }
        }
        $keyname_2 = $keyname;

        /*
         * array to store hex color 
         */
        $colorarr = array(
            0 => '#0091ea', #e04a72
            1 => '#00b0ff', #83ed1a
            2 => '#40c4ff',
            3 => '#80d8ff',
            4 => '#01579b',
            5 => '#0277bd',
            6 => '#039be5',
            7 => '#03a9f4',
            8 => '#b3e5fc',
            9 => '#81d4fa',
            10 => '#29b6f6',
            11 => '#e1f5fe',
            12 => '#b5af79',
            13 => '#914f84',
            14 => '#ce6d87',
            15 => '#e04a72',
        );
        $randcolor = array();
        $randcolor_2 = array();
        $showcolorhtml = '';
        $c = 0;
        $cc = 0;
        foreach ($keyname as $color) {
            // $randcolor[] = '#' . random_color();
            $randcolor[] = $colorarr[$c++];
            //$strname = explode(' ', $color);
            $showcolorhtml .= '<li><span data-color="main-chart-color" class="BigCommerce connected_store_name" style="background-color: ' . $colorarr[$cc++] . ';"></span>' . $color . '</li>';
        }

        $randcolor_2 = $randcolor;
        $data = array('data' => json_encode($data_val_2), 'ykeyslabels' => json_encode($keyname_2), 'linecolors' => json_encode($randcolor_2), 'showcolorhtml' => $showcolorhtml);
        return \yii\helpers\Json::encode($data);
    }

    /*
     * listing of orders through ajax
     */

    public function actionOrdersajax() {
        $post = Yii::$app->request->get();
        $start = $post['start'];
        $length = $post['length'];
        $draw = $post['draw'];
        $search = $post['search']['value'] ? $post['search']['value'] : '';
        $orderby = $post['order']['0']['column'] ? $post['order']['0']['column'] : '';
        $sortbyvalue = true;
        if ($orderby == 1) {
            $orderby_str = 'orders.channel_abb_id';
            $sortbyvalue = true;
        } elseif ($orderby == 2) {
            $orderby_str = 'Customer_User.first_name';
        } elseif ($orderby == 4) {
            $orderby_str = 'orders.total_amount';
            $sortbyvalue = true;
        } elseif ($orderby == 5) {
            $orderby_str = 'orders.order_date';
            $sortbyvalue = true;
        } elseif ($orderby == 6) {
            $orderby_str = 'orders.total_amount';
            $sortbyvalue = true;
        } else {
            $orderby_str = 'orders.channel_abb_id';
            $sortbyvalue = true;
        }
        $asc = $post['order']['0']['dir'] ? $post['order']['0']['dir'] : '';

        $count = Orders::find()->Where(['elliot_user_id' => Yii::$app->user->identity->id])->with('customer')->count();

        $response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count);
        $orders_data = Orders::find()
                ->joinWith('customer')
                ->where(['and', ['orders.elliot_user_id' => Yii::$app->user->identity->id], ['orders.order_visible_status' => 'active']])
                ->limit($length)->offset($start)
                ->andFilterWhere([
                    'or',
                        ['like', 'orders.channel_abb_id', $search],
                        ['like', 'orders.total_amount', $search],
                        ['like', 'concat(Customer_User.first_name," ",Customer_User.last_name)', $search],
                        ['like', 'orders.order_date', $search],
                        ['like', 'concat(orders.ship_city,", ",orders.ship_country)', $search],
                ])
                ->orderBy($orderby_str . " " . $asc)
                ->all();
        
        if($search!=''){
            $count = Orders::find()
                ->joinWith('customer')
                ->where(['and', ['orders.elliot_user_id' => Yii::$app->user->identity->id], ['orders.order_visible_status' => 'active']])
                ->andFilterWhere([
                    'or',
                        ['like', 'orders.channel_abb_id', $search],
                        ['like', 'orders.total_amount', $search],
                        ['like', 'concat(Customer_User.first_name," ",Customer_User.last_name)', $search],
                        ['like', 'orders.order_date', $search],
                        ['like', 'concat(orders.ship_city,", ",orders.ship_country)', $search],
                ])
                ->count();
        }

        $user = Yii::$app->user->identity;
        $selected_currency = \backend\models\CurrencySymbols::find()->where(['name' => strtolower($user->currency)])->select(['id', 'symbol'])->asArray()->one();
        if (isset($selected_currency) and ! empty($selected_currency)) {
            $currency_symbol = $selected_currency['symbol'];
        }
        if (!empty($orders_data)) :
            $i = 1;
            foreach ($orders_data as $orders_data_value) :
                $channel_abb_id = isset($orders_data_value->channel_abb_id) ? $orders_data_value->channel_abb_id : "";
                $channel_explode = explode('_', $channel_abb_id);
                $channel_name = $channel_explode[0];

                //magento store details
                $magento_store_id = isset($orders_data_value->magento_store_id) ? $orders_data_value->magento_store_id : "";
                $magento_store_name = '';
                if ($magento_store_id != "") {
                    $magento_store_detail = MagentoStores::find()->where(['magento_store_id' => $magento_store_id])->one();
                    $magento_store_name = $magento_store_detail->magento_store_name;
                }

                //get Store image
                $accquired_name = $orders_data_value->channel_accquired;
                $channel_img = ChannelsController::getStoreChannelImageByName($accquired_name);
                /* $stores = Stores::find()->Where(['store_name' => 'BigCommerce'])->one();
                  $stores_rel = OrderChannel::find()->Where(['order_id' => $orders_data_value->order_ID])->one();
                  if ($stores_rel->store_id) {
                  $stores_rel = Stores::find()->Where(['store_ID' => $stores_rel->store_id])->one();
                  $channel_img = $stores_rel->store_image;
                  } else {
                  $stores_rel = Channels::find()->Where(['channel_ID' => $stores_rel->channel_id])->one();
                  if($stores_rel->parent_name == "channel_WeChat"){
                  $channel_img = 'img/marketplace_logos/elliot-wechat-connection-icon.png';
                  }
                  elseif($stores_rel->parent_name == "channel_Square"){
                  $channel_img = 'img/square_logo_grid.png';
                  }
                  elseif($stores_rel->parent_name=="channel_Lazada"){
                  $channel_img =  'img/marketplace_logos/lazada-listing-logo.png';
                  }
                  elseif($stores_rel->parent_name=="channel_TMall"){
                  $channel_img =  'img/marketplace_logos/tmall-icon-logo.png';
                  }
                  else{
                  $channel_img  = $stores_rel->channel_image;
                  }
                  } */
                $order_amount = isset($orders_data_value->total_amount) ? $orders_data_value->total_amount : 0;
                $order_value = number_format((float) $order_amount, 2, '.', ',');
                $firstname = $orders_data_value->customer->first_name;
                $lname = $orders_data_value->customer->last_name;
                $date_order = date('m/d/y h:i A', strtotime($orders_data_value->order_date));
                $city = $orders_data_value->ship_city ? $orders_data_value->ship_city : "";
                $country = $orders_data_value->ship_country ? $orders_data_value->ship_country : "";
                if ($country == '') {
                    $ship = $city;
                } else {
                    $ship = $city . ', ' . $country;
                }

                $ship_to = preg_replace('/^([^a-zA-Z0-9])*/', '', $ship);

                $conversion_rate = 1;
                if (isset($user->currency)) {
		    $conversion_rate = Stores::getDbConversionRate($user->currency);
		    $order_value = $order_value * $conversion_rate;
		    $order_value = number_format((float) $order_value, 2, '.', ',');
		    
//                    $username = Yii::$app->params['xe_account_id'];
//                    $password = Yii::$app->params['xe_account_api_key'];
//                    $URL = Yii::$app->params['xe_base_url'] . 'convert_from.json/?from=USD&to=' . $user->currency . '&amount=1';
//
//                    $ch = curl_init();
//                    curl_setopt($ch, CURLOPT_URL, $URL);
//                    curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
//                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//                    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
//                    curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
//                    $result = curl_exec($ch);
//                    $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
//                    curl_close($ch);
//
//                    $result = json_decode($result, true);
//                    if (isset($result) and ! empty($result) and isset($result['to']) and isset($result['to'][0]) and isset($result['to'][0]['quotecurrency'])) {
//                        $conversion_rate = $result['to'][0]['mid'];
//                        $conversion_rate = number_format((float) $conversion_rate, 2, '.', '');
//                        $order_value = $order_value * $conversion_rate;
//                        $order_value = number_format((float) $order_value, 2, '.', ',');
//                    }
                }



                $order_status = $orders_data_value->order_status;
                $label = '';
                if ($order_status == 'Completed') :
                    $label = 'label-success';
                endif;

                if ($order_status == 'Returned' || $order_status == 'Refunded' || $order_status == 'Cancel') :
                    $label = 'label-danger';
                endif;

                if ($order_status == 'In Transit'):
                    $label = 'label-primary';
                endif;

                if ($order_status == 'Awaiting Fulfillment' || $order_status == 'Incomplete' || $order_status == 'waiting-for-shipment' || $order_status == 'Pending' || $order_status == 'Awaiting Payment' || $order_status == 'On Hold'):
                    $label = 'label-warning';
                endif;
                if ($order_status == 'Shipped'):
                    $label = 'label-primary';
                endif;

                //For tracking
//                $order_id = $orders_data_value->order_ID;
//                $orders_data = OrdersProducts::find()->Where(['order_Id' => $order_id])->with('product')->all();
//                $OrderFullfillmentData = OrderFullfillment::find()->Where(['order_Id' => $order_id, 'key_data'=>'1'])->one();
//                 if(!empty($OrderFullfillmentData)){ 
//			$value_data = json_decode($OrderFullfillmentData->value_data);
//			$data =  $value_data->Data;
//			$hawbs = $data->Hawbs;
//			
//                $tracking=' <p style="margin-top: 7px;"><a target="_blank" href="http://www.sf-express.com/hk/en/dynamic_functions/waybill/" class="label  label-warning">'.$hawbs.'</a></p>';
//                 }else{
//                     $tracking='';
//                 } 


                $arr[0] = '<div class="be-checkbox"><input name="order_check" id="ck' . $i . '" value="' . $orders_data_value->order_ID . '" name="ck1" type="checkbox" data-parsley-multiple="groups" value="bar" data-parsley-mincheck="2" data-parsley-errors-container="#error-container1" class="order_row_check"><label class="getId" for="ck' . $i . '"></label></div>';
                $arr[1] = '<a href="/orders/view/' . $orders_data_value->order_ID . '">' . $channel_abb_id . '</a>';
                $arr[2] = '<a href="/people/view?id=' . $orders_data_value->customer->customer_ID . '">' . $firstname . ' ' . $lname . '</a>';
                $arr[3] = $channel_img . '<lable class="magento-store-name">' . $magento_store_name . '</lable>';
                $arr[4] = $currency_symbol . $order_value;
                $arr[5] = $date_order;
                $arr[6] = $ship_to;
                $arr[7] = '<span class="label  ' . $label . '">' . $order_status . '</span>';
                $data_arr[] = $arr;
                $i++;
            endforeach;




            if ($sortbyvalue) {

                if ($orderby == 5) {

                    if ($asc == 'asc') {
                        usort($data_arr, function($a, $b) {
                            $a[5] = strtotime($a[5]);
                            $b[5] = strtotime($b[5]);
                            return $a[5] - $b[5];
                        });
                    } else {
                        usort($data_arr, function($a, $b) {
                            $a[5] = strtotime($a[5]);
                            $b[5] = strtotime($b[5]);
                            return $b[5] - $a[5];
                        });
                    }
                }

                $data_arr1 = array_values($data_arr);
            } else {
                $data_arr1 = array_values($data_arr);
            }

            $response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count, "data" => $data_arr1);
            echo json_encode($response_arr);
        else:
            $response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count, "data" => array());
            echo json_encode($response_arr);
        endif;
    }

    //function for inactive orders listing starts here

    public function actionInactiveordersajax() {

        $post = Yii::$app->request->get();
        $orders_from = $post['orders'];
        $store_id = '';
        $channel_name_explode = explode(" ", $orders_from);
        $channel_name_check = $channel_name_explode[0];
        $check_valid_store = Stores::find()->where(['store_name' => $channel_name_check])->one();
        if (!empty($check_valid_store)) {
            $fin_channel = trim($channel_name_check);
        } else {
            $fin_channel = $orders_from;
        }


        $order_type = array("BigCommerce", "Magento", "Shopify", "WooCommerce", "VTEX", "Magento2", "ShopifyPlus");
        if (in_array($fin_channel, $order_type)) {

            $channel_space_pos = strpos($orders_from, ' ');
            $country_sub_str = substr($orders_from, $channel_space_pos);
            $store_country = trim($country_sub_str);

            $store_details_data = StoreDetails::find()->Where(['channel_accquired' => $fin_channel, 'country' => $store_country])->one();
            $store_connection_id = $store_details_data->store_connection_id;
            $store_id = $store_connection_id;
            $store_id_channel_id = $store_connection_id;
            $store_table_column = 'order_channel.store_id';
        } else {

            if ($fin_channel == "WeChat") {
                $channels_data = Channels::find()->Where(['parent_name' => 'channel_' . $fin_channel])->one();
            } else {
                $channels_data = Channels::find()->Where(['channel_name' => $fin_channel])->one();
            }

            $channel_id = $channels_data->channel_ID;
            $store_id_channel_id = $channels_data->channel_ID;
            $store_table_column = 'order_channel.channel_id';
        }

        $start = $post['start'];
        $length = $post['length'];
        $draw = $post['draw'];
        $search = $post['search']['value'] ? $post['search']['value'] : '';
        $orderby = $post['order']['0']['column'] ? $post['order']['0']['column'] : '';
        $sortbyvalue = true;
        if ($orderby == 1) {
            $orderby_str = 'orders.channel_abb_id';
            $sortbyvalue = true;
        } elseif ($orderby == 2) {
            $orderby_str = 'Customer_User.first_name';
        } elseif ($orderby == 4) {
            $orderby_str = 'orders.total_amount';
            $sortbyvalue = true;
        } elseif ($orderby == 5) {
            $orderby_str = 'orders.order_date';
            $sortbyvalue = true;
        } elseif ($orderby == 6) {
            $orderby_str = 'orders.total_amount';
            $sortbyvalue = true;
        } else {
            $orderby_str = 'orders.channel_abb_id';
            $sortbyvalue = true;
        }
        $asc = $post['order']['0']['dir'] ? $post['order']['0']['dir'] : '';
        $count = Orders::find()
                ->joinWith('customer')
                ->joinWith('orderChannels')
                ->where(['and', ['orders.elliot_user_id' => Yii::$app->user->identity->id], ['orders.order_visible_status' => 'in_active']])
                ->andWhere([$store_table_column => $store_id_channel_id])
                ->count();
        $response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count);
        if (!empty($store_id)) {
            $orders_data = Orders::find()
                    ->joinWith('customer')
                    ->joinWith('orderChannels')
                    ->where(['and', ['orders.elliot_user_id' => Yii::$app->user->identity->id], ['orders.order_visible_status' => 'in_active']])
                    ->limit($length)->offset($start)
                    ->andFilterWhere([
                        'or',
                            ['like', 'orders.channel_abb_id', $search],
                            ['like', 'orders.total_amount', $search],
                            ['like', 'concat(Customer_User.first_name," ",Customer_User.last_name)', $search],
                            ['like', 'orders.order_date', $search],
                            ['like', 'concat(orders.ship_city,", ",orders.ship_country)', $search],
                    ])
                    ->andWhere(['order_channel.store_id' => $store_id])
                    ->orderBy($orderby_str . " " . $asc)
                    ->all();
        } else {

            $orders_data = Orders::find()
                    ->joinWith('customer')
                    ->joinWith('orderChannels')
                    ->where(['and', ['orders.elliot_user_id' => Yii::$app->user->identity->id], ['orders.order_visible_status' => 'in_active']])
                    ->limit($length)->offset($start)
                    ->andFilterWhere([
                        'or',
                            ['like', 'orders.channel_abb_id', $search],
                            ['like', 'orders.total_amount', $search],
                            ['like', 'concat(Customer_User.first_name," ",Customer_User.last_name)', $search],
                            ['like', 'orders.order_date', $search],
                            ['like', 'concat(orders.ship_city,", ",orders.ship_country)', $search],
                    ])
                    ->andWhere(['order_channel.channel_id' => $channel_id])
                    ->orderBy($orderby_str . " " . $asc)
                    ->all();
        }

        if (!empty($orders_data)) {
            $i = 1;
            foreach ($orders_data as $orders_data_value) {

                $channel_abb_id = isset($orders_data_value->channel_abb_id) ? $orders_data_value->channel_abb_id : "";
                $channel_explode = explode('_', $channel_abb_id);
                $channel_name = $channel_explode[0];

                //magento store details
                $magento_store_id = isset($orders_data_value->magento_store_id) ? $orders_data_value->magento_store_id : "";
                $magento_store_name = '';
                if ($magento_store_id != "") {
                    $magento_store_detail = MagentoStores::find()->where(['magento_store_id' => $magento_store_id])->one();
                    $magento_store_name = $magento_store_detail->magento_store_name;
                }

                //get Store image
                $stores = Stores::find()->Where(['store_name' => 'BigCommerce'])->one();
                $stores_rel = OrderChannel::find()->Where(['order_id' => $orders_data_value->order_ID])->one();
                if ($stores_rel->store_id) {
                    $stores_rel = Stores::find()->Where(['store_ID' => $stores_rel->store_id])->one();
                    $channel_img = $stores_rel->store_image;
                } else {
                    $stores_rel = Channels::find()->Where(['channel_ID' => $stores_rel->channel_id])->one();
                    if ($stores_rel->parent_name == "channel_WeChat") {
                        $channel_img = 'img/marketplace_logos/elliot-wechat-connection-icon.png';
                    } elseif ($stores_rel->parent_name == "channel_Square") {
                        $channel_img = 'img/square_logo_grid.png';
                    } elseif ($stores_rel->parent_name == "channel_Lazada") {
                        $channel_img = 'img/marketplace_logos/lazada-listing-logo.png';
                    } else {
                        $channel_img = $stores_rel->channel_image;
                    }
                }
                $order_amount = isset($orders_data_value->total_amount) ? $orders_data_value->total_amount : 0;
                $order_value = number_format((float) $order_amount, 2, '.', ',');
                $firstname = $orders_data_value->customer->first_name;
                $lname = $orders_data_value->customer->last_name;
                $date_order = date('m/d/y h:i A', strtotime($orders_data_value->order_date));
                $city = $orders_data_value->ship_city ? $orders_data_value->ship_city : "";
                $country = $orders_data_value->ship_country ? $orders_data_value->ship_country : "";
                $ship = $city . ', ' . $country;
                $ship_to = preg_replace('/^([^a-zA-Z0-9])*/', '', $ship);
                $user = Yii::$app->user->identity;
                $conversion_rate = 1;
                if (isset($user->currency)) {
		    
		    $conversion_rate = Stores::getDbConversionRate($user->currency);
		    $order_value = $order_value * $conversion_rate;
		    $order_value = number_format((float) $order_value, 2, '.', ',');
		    
//                    $username = Yii::$app->params['xe_account_id'];
//                    $password = Yii::$app->params['xe_account_api_key'];
//                    $URL = Yii::$app->params['xe_base_url'] . 'convert_from.json/?from=USD&to=' . $user->currency . '&amount=1';
//
//                    $ch = curl_init();
//                    curl_setopt($ch, CURLOPT_URL, $URL);
//                    curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
//                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//                    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
//                    curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
//                    $result = curl_exec($ch);
//                    $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
//                    curl_close($ch);
//
//                    $result = json_decode($result, true);
//                    if (isset($result) and ! empty($result) and isset($result['to']) and isset($result['to'][0]) and isset($result['to'][0]['quotecurrency'])) {
//                        $conversion_rate = $result['to'][0]['mid'];
//                        $conversion_rate = number_format((float) $conversion_rate, 2, '.', '');
//                        $order_value = $order_value * $conversion_rate;
//                        $order_value = number_format((float) $order_value, 2, '.', ',');
//                    }
                }



                $order_status = $orders_data_value->order_status;
                $label = '';
                if ($order_status == 'Completed') :
                    $label = 'label-success';
                endif;

                if ($order_status == 'Returned' || $order_status == 'Refunded' || $order_status == 'Cancel') :
                    $label = 'label-danger';
                endif;

                if ($order_status == 'In Transit'):
                    $label = 'label-primary';
                endif;

                if ($order_status == 'Awaiting Fulfillment' || $order_status == 'Incomplete' || $order_status == 'waiting-for-shipment' || $order_status == 'Pending' || $order_status == 'Awaiting Payment' || $order_status == 'On Hold'):
                    $label = 'label-warning';
                endif;
                if ($order_status == 'Shipped'):
                    $label = 'label-primary';
                endif;

                $selected_currency = \backend\models\CurrencySymbols::find()->where(['name' => strtolower($user->currency)])->select(['id', 'symbol'])->asArray()->one();
                if (isset($selected_currency) and ! empty($selected_currency)) {
                    $currency_symbol = $selected_currency['symbol'];
                }

                $arr[0] = '<div class="be-checkbox"><input name="inactiveorder_check" id="ck' . $i . '" value="' . $orders_data_value->order_ID . '" name="ck1" type="checkbox" data-parsley-multiple="groups" value="bar" data-parsley-mincheck="2" data-parsley-errors-container="#error-container1" class="order_row_check"><label class="getId" for="ck' . $i . '"></label></div>';
                $arr[1] = '<a href="javascript:">' . $channel_abb_id . '</a>';
                $arr[2] = $firstname . ' ' . $lname;
                $arr[3] = '<img class="ch_img" src=' . $channel_img . ' width="50" height="50" alt="Channel Image"><lable class="magento-store-name">' . $magento_store_name . '</lable>';
                $arr[4] = $currency_symbol . $order_value;
                $arr[5] = $date_order;
                $arr[6] = $ship_to;
                $arr[7] = '<span class="label  ' . $label . '">' . $order_status . '</span>';
                $data_arr[] = $arr;
                $i++;
            }

            if ($sortbyvalue) {

                if ($orderby == 5) {

                    if ($asc == 'asc') {
                        usort($data_arr, function($a, $b) {
                            $a[5] = strtotime($a[5]);
                            $b[5] = strtotime($b[5]);
                            return $a[5] - $b[5];
                        });
                    } else {
                        usort($data_arr, function($a, $b) {
                            $a[5] = strtotime($a[5]);
                            $b[5] = strtotime($b[5]);
                            return $b[5] - $a[5];
                        });
                    }
                }

                $data_arr1 = array_values($data_arr);
            } else {
                $data_arr1 = array_values($data_arr);
            }
            $response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count, "data" => $data_arr1);
            echo json_encode($response_arr);
        } else {
            $response_arr = array("draw" => $draw, "recordsTotal" => $count, "recordsFiltered" => $count, "data" => array());
            echo json_encode($response_arr);
        }
    }

    //function for inactive order listing ends here





    /*
     * ajax hit from new_custom_elliot.js to delete multiple select data from orders
     */
    public function actionAjaxOrdersDelete() {
        //echo '<pre>';print_r($_POST);die;
        if (!isset($_POST['orderIds']) || count($_POST['orderIds']) == 0) {
            return FALSE;
        } else {
            $product_ids = $_POST['orderIds'];
            foreach ($product_ids as $_product_id) {

                $delete_product = Orders::updateAll(array('order_visible_status' => 'in_active'), 'order_ID = ' . $_product_id);
            }
            return TRUE;
        }
    }

    public function actionAjaxInactiveOrdersDeletion() {
        // echo '<pre>';
        // print_r($_POST);
        // die('ayaa');
        if (!isset($_POST['order_id']) || count($_POST['order_id']) == 0) {
            return false;
        } else {
            $ordersID = $_POST['order_id'];
            foreach ($ordersID as $orders) {
                $undo_delete = Orders::updateAll(array('order_visible_status' => 'active'), 'order_ID = ' . $orders);
            }
            return true;
        }
    }

    public function actionInactiveOrders() {

        return $this->render('inactiveorders');
    }

    public function actionSavepdf($id) {
       
      //  $customer_data = CustomerUser::find()->Where(['customer_ID' => $id])->one();
        $content = $this->renderPartial('view', [
            'model' => $this->findModel($id),
          //  'Customer' => $customer_data,
        ]);

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_CORE,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting 
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}',
            'mode' => Pdf::MODE_UTF8,
            // set mPDF properties on the fly
            'options' => ['title' => 'Elliot Invoice'],
            // call mPDF methods on the fly
            'methods' => [
                'SetHeader' => ['Elliot'],
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

}
    