<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use backend\models\User;
use backend\models\StoresConnection;
use backend\models\Stores;
use backend\models\Categories;
use backend\models\Variations;
use backend\models\VariationsItemList;
use backend\models\Products;
use backend\models\OrdersProducts;
use backend\models\OrderChannel;
use backend\models\CustomerUser;
use backend\models\Notification;
use backend\models\ProductCategories;
use backend\models\ProductVariation;
use backend\models\ProductImages;
use backend\models\ProductChannel;
use backend\models\Orders;
use backend\models\Channels;
use common\models\CustomFunction;
use backend\models\ChannelConnection;
use backend\models\Integrations;
use backend\models\ProductAbbrivation;
use backend\models\CategoryAbbrivation;
use backend\models\CronTasks;
use Bigcommerce\Api\ShopifyClient as Shopify;

class ShopifyController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    /**
     * Lists all CustomerUser models.
     * @return mixed
     */
    public function beforeAction($action) {

        $this->enableCsrfValidation = false;


        return parent::beforeAction($action);
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'test', 'auth-shopify', 'shopify-importing'],
                'rules' => [
                    [
                        /* action hit without log-in */
                        'actions' => ['login', 'test', 'auth-shopify', 'shopify-importing'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        /* action hit only with log-in */
                        'actions' => ['index', 'test', 'auth-shopify', 'shopify-importing'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    /**
     * Authorize the shopify API Credentials
     */
    public function actionAuthShopify() {
        //echo "<pre>"; print_r($_POST); echo "</pre>"; die('I am herer');
        $array_msg = array();
        $shopify_shop = $_POST['shopify_shop'];
        $shopify_api = $_POST['shopify_api'];
        $shopify_pass = $_POST['shopify_pass'];
        $shopify_access_token = $_POST['shopify_shared_secret'];
        $user_id = $_POST['user_id'];
        $url = 'https://' . $shopify_api . ':' . $shopify_pass . '@' . $shopify_shop . '/admin/customers.json';
        //$jsondata = file_get_contents($url);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => FALSE,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "postman-token: 8d6a1ad5-395f-cb02-e58d-223387324331"
            ),
        ));

        $response = curl_exec($curl);
        $response_data = json_decode($response);
        if ($response_data != '') {
            if (array_key_exists('errors', $response_data)) {
                $msg = $response_data->errors;
                $array_msg['api_error'] = 'Your API credentials are not working. Because of following reason ' . $msg . '. Please check and try again.';
                return json_encode($array_msg);
                exit();
            }
        }

        $validate_url = array();
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            //echo "cURL Error #:" . $err;
            $array_msg['api_error'] = 'Your API credentials are not working. Please check and try again,';
            return json_encode($array_msg);
            exit();
        } else {
            $user = User::find()->where(['id' => $user_id])->one();
            $user_domain = $user->domain_name;
            $config = yii\helpers\ArrayHelper::merge(
                            require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $user_domain . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php'));
            $application = new yii\web\Application($config);
            $store = Stores::find()->select('store_id')->where(['store_name' => 'Shopify'])->one();
            $store_id = $store->store_id;
            $checkConnection = StoresConnection::find()->where(['store_id' => $store_id, 'user_id' => $user_id])->one();
            if (empty($checkConnection)):
                $connectionModel = new StoresConnection();
                $connectionModel->store_id = $store_id;
                $connectionModel->user_id = $user_id;
                $connectionModel->connected = 'Yes';
                $connectionModel->url = $shopify_shop;
                $connectionModel->api_key = $shopify_api;
                $connectionModel->key_password = $shopify_pass;
                $connectionModel->shared_secret = $shopify_access_token;
                $created_date = date('Y-m-d h:i:s', time());
                $connectionModel->created = $created_date;
                if ($connectionModel->save()):
                    //Yii::$app->session->setFlash('success', 'Success! Your shop has been connected successfully. Syncing will start soon.');
                    $array_msg['success'] = "Success! Your shop has been connected successfully. Syncing will start soon.";
                else:
                    $array_msg['error'] = "Error Something went wrong. Please try again";
                endif;
            endif;
        }
        return json_encode($array_msg);
        exit();
    }
    
    /**
     * Shopify Importing start
     */
    public function actionShopifyImporting() {
        $user_id = $_GET['user_id'];
        $get_users = User::find()->select('domain_name,company_name,email')->where(['id' => $user_id])->one();
        $email = $get_users->email;
        $company_name = $get_users->company_name;
        $config = yii\helpers\ArrayHelper::merge(
            require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), 
            require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_users->domain_name . '/main-local.php'), 
            require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), 
            require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);
        $get_shopify_id = Stores::find()->select('store_id')->where(['store_name' => 'Shopify'])->one();
        $shopify_store_id = $get_shopify_id->store_id;
        $store_shopify = StoresConnection::find()->where(['store_id' => $shopify_store_id, 'user_id' => $user_id])->one();
        if(!empty($store_shopify)){
            $shopify_shop = $store_shopify->url;
            $shopify_api_key = $store_shopify->api_key;
            $shopify_api_password = $store_shopify->key_password;
            $shopify_shared_secret = $store_shopify->shared_secret;
            $sc = new Shopify($shopify_shop, $shopify_api_password, $shopify_api_key, $shopify_shared_secret);
            $this->shopifyCustomerImporting($sc, $user_id);
        }
    }
    
    /**
     * Shopify Customer importing
     */
    public function shopifyCustomerImporting($sc, $user_id) {
        $total_customer_count = $sc->call('GET', '/admin/customers/count.json');
        $count = ceil($total_customer_count / 250);
        for ($i = 1; $i <= $count; $i++){
            $customer_result = $sc->call('GET', '/admin/customers.json?page=' . $i . '&limit=250');
            foreach ($customer_result as $_customer){
                $channel_name = 'Shopify';
                $customer_id = $_customer['id'];
                $Prefix_customer_abb_id = 'SP' . $customer_id;
                $customer_email = $_customer['email'];
                $customer_f_name = $_customer['first_name'];
                $customer_l_name = $_customer['last_name'];
                $customer_create_date = $_customer['created_at'];
                $customer_update_date = $_customer['updated_at'];
                $add_1 = $add_2 = $city = $state = $country = $zip = $phone = '';
                if (array_key_exists('default_address', $_customer)){
                    $add_1 = $_customer['default_address']['address1'];
                    $add_2 = $_customer['default_address']['address2'];
                    $city = $_customer['default_address']['city'];
                    $state = $_customer['default_address']['province'];
                    $country = $_customer['default_address']['country'];
                    $zip = $_customer['default_address']['zip'];
                    $phone = $_customer['default_address']['phone'];
                }
                $checkCustomerModel = CustomerUser::find()->where(['channel_abb_id' => $Prefix_customer_abb_id])->one();
                if (empty($checkCustomerModel)){
                    $Customers_model = new CustomerUser();
                    $Customers_model->channel_abb_id = $Prefix_customer_abb_id;
                    $Customers_model->first_name = $customer_f_name;
                    $Customers_model->last_name = $customer_l_name;
                    $Customers_model->email = $customer_email;
                    $Customers_model->channel_acquired = $channel_name;
                    $Customers_model->date_acquired = date('Y-m-d h:i:s', strtotime($customer_create_date));
                    $Customers_model->street_1 = $add_1;
                    $Customers_model->street_2 = $add_2;
                    $Customers_model->city = $city;
                    $Customers_model->country = $country;
                    $Customers_model->state = $state;
                    $Customers_model->zip = $zip;
                    $Customers_model->phone_number = $phone;
                    $Customers_model->ship_street_1 = $add_1;
                    $Customers_model->ship_street_2 = $add_2;
                    $Customers_model->ship_city = $city;
                    $Customers_model->ship_state = $state;
                    $Customers_model->ship_zip = $zip;
                    $Customers_model->ship_country = $country;
                    $Customers_model->created_at = date('Y-m-d h:i:s', strtotime($customer_create_date));
                    $Customers_model->updated_at = date('Y-m-d h:i:s', strtotime($customer_update_date));
                    //Save Elliot User id
                    $Customers_model->elliot_user_id = $user_id;
                    $Customers_model->save(false);
                }
            }
        }
    }
    
    /**
     * Shopify Product Importing
     */
    public function shopifyProductImporting($sc, $user_id, $shopify_shop) {
        $total_product_count = $sc->call('GET', '/admin/products/count.json');
        $count = ceil($total_product_count / 250);
        for ($i = 1; $i <= $count; $i++){
            $product_result = $sc->call('GET', '/admin/products.json?page=' . $i . '&limit=250');
            foreach ($product_result as $_product) {
                $product_id = 'SP' . $_product['id'];
                $title = $_product['title'];
                $description = $_product['body_html'];
                $product_handle = $_product['handle']; 
                $created_date = date('Y-m-d h:i:s', strtotime($_product['created_at']));
                $updated_date = date('Y-m-d h:i:s', strtotime($_product['updated_at']));
                /* Fields which are required but not avaialable @Shopify */
                $p_ean = '';
                $p_jan = '';
                $p_isbn = '';
                $p_mpn = '';
                $product_variants = $_product['variants'];
                $count = 1;
                foreach ($product_variants as $_variants) {
                    $variants_id = $_variants['id'];
                    $variants_price = $_variants['price'];
                    $variant_sku = ($_variants['sku']=='')?$variant_sku = '':$_variants['sku'];
                    $variants_barcode = $_variants['barcode'];
                    $variants_qty = $_variants['inventory_quantity'];
                    $variants_weight = $_variants['weight'];
                    $variants_created_at = $_variants['created_at'];
                    $variants_updated_at = $_variants['updated_at'];
                    $variants_title_value = $_variants['title'];
                    if ($count==1) {
                        $checkProductModel = Products::find()->where(['sku' => $variant_sku])->one();
                        if (empty($checkProductModel)){
                            $productModel = new Products ();
                            $productModel->channel_abb_id = '';
                            $productModel->product_name = $title;
                            $productModel->SKU = $variant_sku;
                            $productModel->product_url = 'https://' . $shopify_shop . '/products/' . $product_handle;
                            $productModel->UPC = $variants_barcode;
                            $productModel->EAN = $p_ean;
                            $productModel->Jan = $p_jan;
                            $productModel->ISBN = $p_isbn;
                            $productModel->MPN = $p_mpn;
                            $productModel->description = $description;
                            $productModel->adult = 'no';
                            $productModel->age_group = NULL;
                            $productModel->gender = 'Unisex';
                            $productModel->brand = '';
                            $productModel->weight = $variants_weight;
                            $productModel->stock_quantity = $variants_qty;
                            if ($variants_qty == 0):
                                $productModel->availability = 'Out of Stock';
                                $productModel->stock_level = 'Out of Stock';
                            else:
                                $productModel->availability = 'In Stock';
                                $productModel->stock_level = 'In Stock';
                            endif;
                            $productModel->stock_status = 'Visible';
                            $productModel->low_stock_notification = 5;
                            $productModel->price = $variants_price;
                            $productModel->sales_price = $variants_price;
                            $productModel->created_at = date('Y-m-d h:i:s', strtotime($created_date));
                            $productModel->updated_at = date('Y-m-d h:i:s', strtotime($updated_date));
                            $productModel->elliot_user_id = $user_id;
                            if($productModel->save(false)){
                                $last_pro_id = $productModel->id;
                                $product_abberivation = new ProductAbbrivation();
                                $product_abberivation->channel_abb_id = $product_id;
                                $product_abberivation->product_id = $last_pro_id;
                                $product_abberivation->price = $variants_price;
                                $product_abberivation->salePrice = $variants_price;
                                $product_abberivation->schedules_sales_date = date('Y-m-d', time());
                                $product_abberivation->stock_quantity = $variants_qty;
                                if($product_qty>0){
                                    $product_abberivation->stock_level = 'In Stock';
                                }
                                else{
                                    $product_abberivation->stock_level = 'Out of Stock';
                                }
                                $product_abberivation->channel_accquired = 'Shopify';
                                $product_abberivation->created_at = date('Y-m-d H:i:s', time());
                                $product_abberivation->updated_at = date('Y-m-d H:i:s', time());
                                $product_abberivation->save(false);
                                
                                $store_data = Stores::find()->where(['store_name' => 'Shopify'])->one();
                                $shopify_store_id = $store_data->store_id;
                                $productChannelModel = new ProductChannel;
                                $productChannelModel->store_id = $shopify_store_id;
                                $productChannelModel->product_id = $last_pro_id;
                                $productChannelModel->status = 'yes';
                                $productChannelModel->created_at = date('Y-m-d h:i:s', time());
                                $productChannelModel->save(false);
                            }
                            $product_images = $_product['images'];
                            foreach ($product_images as $_image) {
                                $image_id = $_image['id'];
                                $image_src = $_image['src'];
                                $image_position = $_image['position'];
                                $image_created_at = $_image['created_at'];
                                $image_updated_at = $_image['updated_at'];
                                $image_variant_id_array = $_image['variant_ids'];
                                $default_img_id = $_product['image']['id'];

                                $productImageModel = new ProductImages;
                                $productImageModel->elliot_user_id = $user_id;
                                $productImageModel->product_ID = $last_pro_id;
                                $productImageModel->link = $image_src;
                                $productImageModel->priority = $image_position;
                                $productImageModel->image_status = 1;
                                $productImageModel->created_at = date('Y-m-d h:i:s', strtotime($image_created_at));
                                $productImageModel->updated_at = date('Y-m-d h:i:s', strtotime($image_updated_at));
                                if ($default_img_id == $image_id) {
                                    $productImageModel->default_image = 'Yes';
                                }
                                $productImageModel->save(false);

                                if (count($image_variant_id_array) > 0) {
                                    foreach ($image_variant_id_array as $_image_variant) {
                                        $productVariationModel = new ProductVariation;
                                        $productVariationModel->elliot_user_id = $user_id;
                                        $productVariationModel->product_id = $last_pro_id;
                                        $productVariationModel->item_name = 'variants image';
                                        $productVariationModel->item_value = $image_src;
                                        $productVariationModel->store_variation_id = 'SP' . $_image_variant;
                                        $productVariationModel->created_at = date('Y-m-d h:i:s', strtotime($image_created_at));
                                        $productVariationModel->updated_at = date('Y-m-d h:i:s', strtotime($image_updated_at));
                                        $productVariationModel->save(false);
                                    }
                                }
                            }
                            /**
                             * If product have multiple variants
                             */
                            $variants_options = $_product['options'];
                            $variants_options_text_array = array();
                            foreach ($variants_options as $_options) {
                                $variants_options_text_array[] = $_options['name'];
                            }
                            
                            $productVariationModel = new ProductVariation;
                            $productVariationModel->elliot_user_id = $user_id;
                            $productVariationModel->product_id = $last_pro_id;
                            $productVariationModel->item_name = 'price';
                            $productVariationModel->item_value = $variants_price;
                            $productVariationModel->store_variation_id = 'SP' . $variants_id;
                            $productVariationModel->created_at = date('Y-m-d h:i:s', strtotime($variants_created_at));
                            $productVariationModel->updated_at = date('Y-m-d h:i:s', strtotime($variants_updated_at));
                            $productVariationModel->save(false);

                            $productVariationModel = new ProductVariation;
                            $productVariationModel->elliot_user_id = $user_id;
                            $productVariationModel->product_id = $last_pro_id;
                            $productVariationModel->item_name = 'sku';
                            $productVariationModel->item_value = $variants_sku;
                            $productVariationModel->store_variation_id = 'SP' . $variants_id;
                            $productVariationModel->created_at = date('Y-m-d h:i:s', strtotime($variants_created_at));
                            $productVariationModel->updated_at = date('Y-m-d h:i:s', strtotime($variants_updated_at));
                            $productVariationModel->save(false);

                            if ($variants_barcode != '') {
                                $productVariationModel = new ProductVariation;
                                $productVariationModel->elliot_user_id = $user_id;
                                $productVariationModel->product_id = $last_pro_id;
                                $productVariationModel->item_name = 'barcode';
                                $productVariationModel->item_value = $variants_barcode;
                                $productVariationModel->store_variation_id = 'SP' . $variants_id;
                                $productVariationModel->created_at = date('Y-m-d h:i:s', strtotime($variants_created_at));
                                $productVariationModel->updated_at = date('Y-m-d h:i:s', strtotime($variants_updated_at));
                                $productVariationModel->save(false);
                            }
                            if ($variants_weight != '') {
                                $productVariationModel = new ProductVariation;
                                $productVariationModel->elliot_user_id = $user_id;
                                $productVariationModel->product_id = $last_pro_id;
                                $productVariationModel->item_name = 'weight';
                                $productVariationModel->item_value = $variants_weight;
                                $productVariationModel->store_variation_id = 'SP' . $variants_id;
                                $productVariationModel->created_at = date('Y-m-d h:i:s', strtotime($variants_created_at));
                                $productVariationModel->updated_at = date('Y-m-d h:i:s', strtotime($variants_updated_at));
                                $productVariationModel->save(false);
                            }

                            if ($variants_qty != '') {
                                $productVariationModel = new ProductVariation;
                                $productVariationModel->elliot_user_id = $user_id;
                                $productVariationModel->product_id = $last_pro_id;
                                $productVariationModel->item_name = 'qty';
                                $productVariationModel->item_value = $variants_qty;
                                $productVariationModel->store_variation_id = 'SP' . $variants_id;
                                $productVariationModel->created_at = date('Y-m-d h:i:s', strtotime($variants_created_at));
                                $productVariationModel->updated_at = date('Y-m-d h:i:s', strtotime($variants_updated_at));
                                $productVariationModel->save(false);
                            }
                            $i = 1;
                            foreach ($variants_options_text_array as $options_text) {
                                $value = explode('/', $variants_title_value);
                                if ($i == 1) {
                                    $productVariationModel = new ProductVariation;
                                    $productVariationModel->elliot_user_id = $user_id;
                                    $productVariationModel->product_id = $last_pro_id;
                                    $productVariationModel->item_name = $options_text;
                                    $productVariationModel->item_value = $value[0];
                                    $productVariationModel->store_variation_id = 'SP' . $variants_id;
                                    $productVariationModel->created_at = date('Y-m-d h:i:s', strtotime($variants_created_at));
                                    $productVariationModel->updated_at = date('Y-m-d h:i:s', strtotime($variants_updated_at));
                                    $productVariationModel->save(false);
                                } elseif ($i == 2) {
                                    $productVariationModel = new ProductVariation;
                                    $productVariationModel->elliot_user_id = $user_id;
                                    $productVariationModel->product_id = $last_pro_id;
                                    $productVariationModel->item_name = $options_text;
                                    $productVariationModel->item_value = $value[1];
                                    $productVariationModel->store_variation_id = 'SP' . $variants_id;
                                    $productVariationModel->created_at = date('Y-m-d h:i:s', strtotime($variants_created_at));
                                    $productVariationModel->updated_at = date('Y-m-d h:i:s', strtotime($variants_updated_at));
                                    $productVariationModel->save(false);
                                } elseif ($i == 3) {
                                    $productVariationModel = new ProductVariation;
                                    $productVariationModel->elliot_user_id = $user_id;
                                    $productVariationModel->product_id = $last_pro_id;
                                    $productVariationModel->item_name = $options_text;
                                    $productVariationModel->item_value = $value[2];
                                    $productVariationModel->store_variation_id = 'SP' . $variants_id;
                                    $productVariationModel->created_at = date('Y-m-d h:i:s', strtotime($variants_created_at));
                                    $productVariationModel->updated_at = date('Y-m-d h:i:s', strtotime($variants_updated_at));
                                    $productVariationModel->save(false);
                                }
                                $i++;
                            }
                        }
                        else{
                            $check_abbrivation =  ProductAbbrivation::find()->where(['channel_abb_id'=>$product_id])->one();
                            if(empty($check_abbrivation)){
                                $product_abberivation = new ProductAbbrivation();
                                $product_abberivation->channel_abb_id = $product_id;
                                $product_abberivation->product_id = $checkProductModel->id;;
                                $product_abberivation->price = $variants_price;
                                $product_abberivation->salePrice = $variants_price;
                                $product_abberivation->schedules_sales_date = date('Y-m-d', time());
                                $product_abberivation->stock_quantity = $variants_qty;
                                if($variants_qty>0){
                                    $product_abberivation->stock_level = 'In Stock';
                                }
                                else{
                                    $product_abberivation->stock_level = 'Out of Stock';
                                }
                                $product_abberivation->channel_accquired = 'Shopify';
                                $product_abberivation->created_at = date('Y-m-d H:i:s', time());
                                $product_abberivation->updated_at = date('Y-m-d H:i:s', time());
                                $product_abberivation->save(false);
                                
                                $store_data = Stores::find()->where(['store_name' => 'Shopify'])->one();
                                $shopify_store_id = $store_data->store_id;
                                $productChannelModel = new ProductChannel;
                                $productChannelModel->store_id = $shopify_store_id;
                                $productChannelModel->product_id = $checkProductModel->id;
                                $productChannelModel->status = 'yes';
                                $productChannelModel->created_at = date('Y-m-d H:i:s', time());
                                $productChannelModel->save(false);
                                
                                $productVariationModel = new ProductVariation;
                                $productVariationModel->elliot_user_id = $user_id;
                                $productVariationModel->product_id = $last_pro_id;
                                $productVariationModel->item_name = 'price';
                                $productVariationModel->item_value = $variants_price;
                                $productVariationModel->store_variation_id = 'SP' . $variants_id;
                                $productVariationModel->created_at = date('Y-m-d h:i:s', strtotime($variants_created_at));
                                $productVariationModel->updated_at = date('Y-m-d h:i:s', strtotime($variants_updated_at));
                                $productVariationModel->save(false);

                                $productVariationModel = new ProductVariation;
                                $productVariationModel->elliot_user_id = $user_id;
                                $productVariationModel->product_id = $last_pro_id;
                                $productVariationModel->item_name = 'sku';
                                $productVariationModel->item_value = $variants_sku;
                                $productVariationModel->store_variation_id = 'SP' . $variants_id;
                                $productVariationModel->created_at = date('Y-m-d h:i:s', strtotime($variants_created_at));
                                $productVariationModel->updated_at = date('Y-m-d h:i:s', strtotime($variants_updated_at));
                                $productVariationModel->save(false);

                                if ($variants_barcode != '') {
                                    $productVariationModel = new ProductVariation;
                                    $productVariationModel->elliot_user_id = $user_id;
                                    $productVariationModel->product_id = $last_pro_id;
                                    $productVariationModel->item_name = 'barcode';
                                    $productVariationModel->item_value = $variants_barcode;
                                    $productVariationModel->store_variation_id = 'SP' . $variants_id;
                                    $productVariationModel->created_at = date('Y-m-d h:i:s', strtotime($variants_created_at));
                                    $productVariationModel->updated_at = date('Y-m-d h:i:s', strtotime($variants_updated_at));
                                    $productVariationModel->save(false);
                                }
                                if ($variants_weight != '') {
                                    $productVariationModel = new ProductVariation;
                                    $productVariationModel->elliot_user_id = $user_id;
                                    $productVariationModel->product_id = $last_pro_id;
                                    $productVariationModel->item_name = 'weight';
                                    $productVariationModel->item_value = $variants_weight;
                                    $productVariationModel->store_variation_id = 'SP' . $variants_id;
                                    $productVariationModel->created_at = date('Y-m-d h:i:s', strtotime($variants_created_at));
                                    $productVariationModel->updated_at = date('Y-m-d h:i:s', strtotime($variants_updated_at));
                                    $productVariationModel->save(false);
                                }

                                if ($variants_qty != '') {
                                    $productVariationModel = new ProductVariation;
                                    $productVariationModel->elliot_user_id = $user_id;
                                    $productVariationModel->product_id = $last_pro_id;
                                    $productVariationModel->item_name = 'qty';
                                    $productVariationModel->item_value = $variants_qty;
                                    $productVariationModel->store_variation_id = 'SP' . $variants_id;
                                    $productVariationModel->created_at = date('Y-m-d h:i:s', strtotime($variants_created_at));
                                    $productVariationModel->updated_at = date('Y-m-d h:i:s', strtotime($variants_updated_at));
                                    $productVariationModel->save(false);
                                }
                                
                                $i = 1;
                                foreach ($variants_options_text_array as $options_text) {
                                    $value = explode('/', $variants_title_value);
                                    if ($i == 1) {
                                        $productVariationModel = new ProductVariation;
                                        $productVariationModel->elliot_user_id = $user_id;
                                        $productVariationModel->product_id = $last_pro_id;
                                        $productVariationModel->item_name = $options_text;
                                        $productVariationModel->item_value = $value[0];
                                        $productVariationModel->store_variation_id = 'SP' . $variants_id;
                                        $productVariationModel->created_at = date('Y-m-d h:i:s', strtotime($variants_created_at));
                                        $productVariationModel->updated_at = date('Y-m-d h:i:s', strtotime($variants_updated_at));
                                        $productVariationModel->save(false);
                                    } elseif ($i == 2) {
                                        $productVariationModel = new ProductVariation;
                                        $productVariationModel->elliot_user_id = $user_id;
                                        $productVariationModel->product_id = $last_pro_id;
                                        $productVariationModel->item_name = $options_text;
                                        $productVariationModel->item_value = $value[1];
                                        $productVariationModel->store_variation_id = 'SP' . $variants_id;
                                        $productVariationModel->created_at = date('Y-m-d h:i:s', strtotime($variants_created_at));
                                        $productVariationModel->updated_at = date('Y-m-d h:i:s', strtotime($variants_updated_at));
                                        $productVariationModel->save(false);
                                    } elseif ($i == 3) {
                                        $productVariationModel = new ProductVariation;
                                        $productVariationModel->elliot_user_id = $user_id;
                                        $productVariationModel->product_id = $last_pro_id;
                                        $productVariationModel->item_name = $options_text;
                                        $productVariationModel->item_value = $value[2];
                                        $productVariationModel->store_variation_id = 'SP' . $variants_id;
                                        $productVariationModel->created_at = date('Y-m-d h:i:s', strtotime($variants_created_at));
                                        $productVariationModel->updated_at = date('Y-m-d h:i:s', strtotime($variants_updated_at));
                                        $productVariationModel->save(false);
                                    }
                                    $i++;
                                }
                            }
                        }
                    }
                    else{
                        $checkAnotherProductModel = Products::find()->where(['sku' => $variant_sku])->one();
                        if(!empty($checkAnotherProductModel)){
                            $productVariationModel = new ProductVariation;
                            $productVariationModel->elliot_user_id = $user_id;
                            $productVariationModel->product_id = $last_pro_id;
                            $productVariationModel->item_name = 'price';
                            $productVariationModel->item_value = $variants_price;
                            $productVariationModel->store_variation_id = 'SP' . $variants_id;
                            $productVariationModel->created_at = date('Y-m-d h:i:s', strtotime($variants_created_at));
                            $productVariationModel->updated_at = date('Y-m-d h:i:s', strtotime($variants_updated_at));
                            $productVariationModel->save(false);

                            $productVariationModel = new ProductVariation;
                            $productVariationModel->elliot_user_id = $user_id;
                            $productVariationModel->product_id = $last_pro_id;
                            $productVariationModel->item_name = 'sku';
                            $productVariationModel->item_value = $variants_sku;
                            $productVariationModel->store_variation_id = 'SP' . $variants_id;
                            $productVariationModel->created_at = date('Y-m-d h:i:s', strtotime($variants_created_at));
                            $productVariationModel->updated_at = date('Y-m-d h:i:s', strtotime($variants_updated_at));
                            $productVariationModel->save(false);

                            if ($variants_barcode != '') {
                                $productVariationModel = new ProductVariation;
                                $productVariationModel->elliot_user_id = $user_id;
                                $productVariationModel->product_id = $last_pro_id;
                                $productVariationModel->item_name = 'barcode';
                                $productVariationModel->item_value = $variants_barcode;
                                $productVariationModel->store_variation_id = 'SP' . $variants_id;
                                $productVariationModel->created_at = date('Y-m-d h:i:s', strtotime($variants_created_at));
                                $productVariationModel->updated_at = date('Y-m-d h:i:s', strtotime($variants_updated_at));
                                $productVariationModel->save(false);
                            }
                            if ($variants_weight != '') {
                                $productVariationModel = new ProductVariation;
                                $productVariationModel->elliot_user_id = $user_id;
                                $productVariationModel->product_id = $last_pro_id;
                                $productVariationModel->item_name = 'weight';
                                $productVariationModel->item_value = $variants_weight;
                                $productVariationModel->store_variation_id = 'SP' . $variants_id;
                                $productVariationModel->created_at = date('Y-m-d h:i:s', strtotime($variants_created_at));
                                $productVariationModel->updated_at = date('Y-m-d h:i:s', strtotime($variants_updated_at));
                                $productVariationModel->save(false);
                            }

                            if ($variants_qty != '') {
                                $productVariationModel = new ProductVariation;
                                $productVariationModel->elliot_user_id = $user_id;
                                $productVariationModel->product_id = $last_pro_id;
                                $productVariationModel->item_name = 'qty';
                                $productVariationModel->item_value = $variants_qty;
                                $productVariationModel->store_variation_id = 'SP' . $variants_id;
                                $productVariationModel->created_at = date('Y-m-d h:i:s', strtotime($variants_created_at));
                                $productVariationModel->updated_at = date('Y-m-d h:i:s', strtotime($variants_updated_at));
                                $productVariationModel->save(false);
                            }
                        }
                    }
                    $count++;
                }
            }
        }
    }
    /**
     * Shopify add hooks
     */
    public function addShopifyHooks($user_id, $sc) {
        $product_create_hook = array
            (
            "webhook" => array
                (
                "topic" => "products/create",
                "address" => Yii::$app->params['BASE_URL'] . "people/shopify-product-create?id=" . $user_id,
                "format" => "json"
            )
        );

        $product_delete_hook = array
            (
            "webhook" => array
                (
                "topic" => "products/delete",
                "address" => Yii::$app->params['BASE_URL'] . "people/shopify-product-delete?id=" . $user_id,
                "format" => "json"
            )
        );

        $product_update_hook = array
            (
            "webhook" => array
                (
                "topic" => "products/update",
                "address" => Yii::$app->params['BASE_URL'] . "people/shopify-product-update?id=" . $user_id,
                "format" => "json"
            )
        );

        $order_create_hook = array
            (
            "webhook" => array
                (
                "topic" => "orders/create",
                "address" => Yii::$app->params['BASE_URL'] . "people/shopify-order-create?id=" . $user_id,
                "format" => "json"
            )
        );

        $order_paid_hook = array
            (
            "webhook" => array
                (
                "topic" => "orders/paid",
                "address" => Yii::$app->params['BASE_URL'] . "people/shopify-order-paid?id=" . $user_id,
                "format" => "json"
            )
        );

        $order_update_hook = array
            (
            "webhook" => array
                (
                "topic" => "orders/updated",
                "address" => Yii::$app->params['BASE_URL'] . "people/shopify-order-update?id=" . $user_id,
                "format" => "json"
            )
        );

        $customer_create_hook = array
            (
            "webhook" => array
                (
                "topic" => "customers/create",
                "address" => Yii::$app->params['BASE_URL'] . "people/shopify-customer-create?id=" . $user_id,
                "format" => "json"
            )
        );

        $customer_update_hook = array
            (
            "webhook" => array
                (
                "topic" => "customers/update",
                "address" => Yii::$app->params['BASE_URL'] . "people/shopify-customer-update?id=" . $user_id,
                "format" => "json"
            )
        );

        $hooks_tag = $sc->call('GET', '/admin/webhooks.json');
        $myhooks = array();
        foreach ($hooks_tag as $exist_hooks) {
            $myhooks[] = $exist_hooks['address'];
        }
        if (!in_array(Yii::$app->params['BASE_URL'] . "people/shopify-product-create?id=" . $user_id, $myhooks)) {
            $hooks_add = $sc->call('POST', '/admin/webhooks.json', $product_create_hook);
        }
        if (!in_array(Yii::$app->params['BASE_URL'] . "people/shopify-product-delete?id=" . $user_id, $myhooks)) {
            $hooks_add = $sc->call('POST', '/admin/webhooks.json', $product_delete_hook);
        }
        if (!in_array(Yii::$app->params['BASE_URL'] . "people/shopify-product-update?id=" . $user_id, $myhooks)) {
            $hooks_add = $sc->call('POST', '/admin/webhooks.json', $product_update_hook);
        }
        if (!in_array(Yii::$app->params['BASE_URL'] . "people/shopify-order-create?id=" . $user_id, $myhooks)) {
            $hooks_add = $sc->call('POST', '/admin/webhooks.json', $order_create_hook);
        }
        if (!in_array(Yii::$app->params['BASE_URL'] . "people/shopify-order-paid?id=" . $user_id, $myhooks)) {
            $hooks_add = $sc->call('POST', '/admin/webhooks.json', $order_paid_hook);
        }
        if (!in_array(Yii::$app->params['BASE_URL'] . "people/shopify-order-update?id=" . $user_id, $myhooks)) {
            $hooks_add = $sc->call('POST', '/admin/webhooks.json', $order_update_hook);
        }
        if (!in_array(Yii::$app->params['BASE_URL'] . "people/shopify-customer-create?id=" . $user_id, $myhooks)) {
            $hooks_add = $sc->call('POST', '/admin/webhooks.json', $customer_create_hook);
        }
        if (!in_array(Yii::$app->params['BASE_URL'] . "people/shopify-customer-update?id=" . $user_id, $myhooks)) {
            $hooks_add = $sc->call('POST', '/admin/webhooks.json', $customer_update_hook);
        }
    }
    
    public function actionTest(){
        $shopify_shop = 'green-money-shop.myshopify.com';
        $shopify_api_password = '3b18e13aa394c4129c9324046fc8c4ee';
        $shopify_api_key = '0364cf7fb91fdc6a5e5154222f2d7b85';
        $shopify_shared_secret = 'ad176773009c429b08ff9c3ec4a0d405';
        $sc = new Shopify($shopify_shop, $shopify_api_password, $shopify_api_key, $shopify_shared_secret);
        //$result = $sc->call('GET', '/admin/customers.json?page=1&limit=50');
        $result = $sc->call('GET', '/admin/products.json?page=1&limit=25');
        echo "<pre>"; print_r($result); echo "</pre>"; die;
    }
    

}
