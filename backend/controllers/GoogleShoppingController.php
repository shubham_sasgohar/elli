<?php

namespace backend\controllers;

ob_start();

use backend\models\ChannelConnection;
use backend\models\Channels;
use backend\models\StoresConnection;
use Bigcommerce\Api\Connection;
use backend\models\ProductImages;
use backend\controllers\ChannelsController;
use backend\models\Categories;
use backend\models\Products;
use backend\models\Variations;
use backend\models\VariationsItemList;
use backend\models\OrdersProducts;
use backend\models\OrderChannel;
use backend\models\CustomerUser;
use backend\models\Notification;
use backend\models\ProductCategories;
use backend\models\ProductVariation;
use backend\models\ProductChannel;
use backend\models\Orders;
use backend\models\MerchantProducts;
use backend\models\User;
use backend\models\GoogleProductCategories;
use backend\models\Stores;
use backend\models\StoreDetails;
use yii\filters\AccessControl;
use Yii;

require($_SERVER['DOCUMENT_ROOT'] . '/googlefeeds/vendor/autoload.php');
require($_SERVER['DOCUMENT_ROOT'] . '/googlefeeds/NinthYard/GoogleShoppingFeed/Feed.php');
require($_SERVER['DOCUMENT_ROOT'] . '/googlefeeds/NinthYard/GoogleShoppingFeed/Item.php');
require($_SERVER['DOCUMENT_ROOT'] . '/googlefeeds/NinthYard/GoogleShoppingFeed/Node.php');
require($_SERVER['DOCUMENT_ROOT'] . '/googlefeeds/NinthYard/GoogleShoppingFeed/Containers/GoogleShopping.php');

use NinthYard\GoogleShoppingFeed\Containers\GoogleShopping;

class GoogleShoppingController extends \yii\web\Controller {

    public function actionFeed() {
        $feed = $_POST['feed'];
        //$feed='yes';
        $channel_big = Channels::find()->where(['channel_name' => 'Google shopping'])->one();
        $user_id = Yii::$app->user->identity->id;
        if (!empty($channel_big)) {
            $channel_id = $channel_big->channel_ID;
        }
        if ($feed == 'no') {
            $channel_connection_delete = ChannelConnection::find()->Where(['user_id' => $user_id, 'channel_id' => $channel_id])->one();
            if (!empty($channel_connection_delete)) {
                $channel_connection_id = $channel_connection_delete->channel_connection_id;
                $channel_details_check = StoreDetails::find()->where(['channel_connection_id' => $channel_connection_id])->one();
                if(!empty($channel_details_check)){
                    $channel_details_check->delete();
                }
                $channel_connection_delete->delete();
            }
        } else {
            $Google_connection = new ChannelConnection();
            $Google_connection->channel_id = $channel_big->channel_ID;
            $Google_connection->user_id = $user_id;
            $Google_connection->connected = 'yes';
            $Google_connection->graph_status = 'yes';
            $Google_connection->import_status  = 'Completed';
            $Google_connection->created = date('Y-m-d H:i:s');
            $Google_connection->save(false);
            
            $channel_connection_id = $Google_connection->channel_connection_id;
            $channel_details_check = StoreDetails::find()->where(['channel_connection_id' => $channel_connection_id])->one();
            if(empty($channel_details_check)){
                $save_store_details = new StoreDetails();
                $save_store_details->channel_connection_id = $channel_connection_id;
                $save_store_details->store_name = 'Google Shopping';
                $save_store_details->store_url = '';
                $save_store_details->country = 'United States';
                $save_store_details->country_code = 'US';
                $save_store_details->currency = 'USD';
                $save_store_details->currency_symbol = '$';
                $save_store_details->channel_accquired = 'Google Shopping';
                $save_store_details->created_at = date('Y-m-d H:i:s', time());
                $save_store_details->save(false);
            }
        }

        $user_id = Yii::$app->user->identity->id;
        $user_data = User::find()->Where(['id' => $user_id])->one();
        $user_data->google_feed = $feed;
        if ($user_data->save(false)):
            $config = yii\helpers\ArrayHelper::merge(
                            require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
            );
            $application = new yii\web\Application($config);

            $user_data1 = User::find()->Where(['id' => $user_id])->one();
            $user_data1->google_feed = $feed;
            $user_data1->save(false);
        endif;
        return true;
    }

    public function actionGooglefeed() {
//        Yii::$app->params['BASE_URL']

        $userdata = User::find()->where(['google_feed' => 'yes'])->all();

//      echo '<pre>'; print_r($userdata); echo '</pre>'; die('sdfa');
        foreach ($userdata as $user) {
            $userdomain = $user->domain_name;
            $userid = $user->id;


            // create a new cURL resource
            $ch = curl_init();

            $url = Yii::$app->params['BASE_URL'] . 'google-shopping/feedxml?domain=' . $userdomain;
//            $url = 'https://s86.co/google-shopping/feedxml?domain=' . $userdomain;
            // set URL and other appropriate options
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);

            // grab URL and pass it to the browser
            curl_exec($ch);

            // close cURL resource, and free up system resources
            curl_close($ch);

            /* $application = new yii\web\Application($config);
              $user_prducts_data = '';
              $user_prducts_data = Products::find()->with('productImages')->all(); */

            /*  GoogleShopping::title('Test Feed');
              GoogleShopping::link('http://www.example.com/');
              GoogleShopping::description('Test Google Shopping Feed');
              //$products = Products::find()->with('productImages')->all();

              foreach ($user_prducts_data as $product) {

              $item = GoogleShopping::createItem();

              if (!empty($product->productImages)) {
              $image = $product->productImages['0']['link'];
              } else {
              $image = '';
              }
              $stock = strtolower($product->stock_level);
              $item->id($product->channel_abb_id); //A SKU code for example, or any unique identifies (eg. could be the id from a database table)
              $item->title($product->product_name);
              $item->price($product->price); //Price one wishes to sell a product for (unless sale_price option is added, then it's the original price)
              $item->mpn($product->MPN);
              $item->availability($stock);
              $item->brand($product->brand);
              $item->sale_price($product->sales_price); //The actual price one wishes to sell a product for (optional)
              $item->link('http://www.example.com/example-product.html');
              $item->image_link($image);

              $data = GoogleShopping::asRss(false);
              GoogleShopping::removeLastItem(false);
              $item->delete();
              }

              $user_prducts_data = '';

              //echo '<pre>'; print_r($data); echo '</pre>'; die('dsfa');
              //$data = GoogleShopping::removeLastItem(false);
              $filename = $_SERVER['DOCUMENT_ROOT'].'/googlefeeds/' . $userid . '.xml';
              file_put_contents($filename, $data);
              (object)$data = ""; */
        }
    }

    public function actionFeedxml() {

        $userdomain = $_GET['domain'];


        $config = yii\helpers\ArrayHelper::merge(
                        require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $userdomain . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);
        GoogleShopping::title('Test Feed');
        GoogleShopping::link('http://www.example.com/');
        GoogleShopping::description('Test Google Shopping Feed');
        $user_prducts_data = Products::find()->with('productImages')->all();

        foreach ($user_prducts_data as $product) {

            $item = GoogleShopping::createItem();

            if (!empty($product->productImages)) {
                $image = $product->productImages['0']['link'];
            } else {
                $image = '';
            }
            $stock = strtolower($product->stock_level);
            $item->id($product->channel_abb_id); //A SKU code for example, or any unique identifies (eg. could be the id from a database table)
            $item->title($product->product_name);
            $item->google_product_category($product->Google_cat_id);
            $item->price($product->price); //Price one wishes to sell a product for (unless sale_price option is added, then it's the original price)
            $item->mpn($product->MPN);
            $item->availability($stock);
            $item->brand($product->brand);
            $item->sale_price($product->sales_price); //The actual price one wishes to sell a product for (optional)
            $item->link('http://www.example.com/example-product.html');
            $item->image_link($image);

            $data = GoogleShopping::asRss(false);



            GoogleShopping::removeLastItem(false);
            $item->delete();
        }

        $user_prducts_data = '';

        //echo '<pre>'; print_r($data); echo '</pre>'; die('dsfa');
        //$data = GoogleShopping::removeLastItem(false);
        $filename = $_SERVER['DOCUMENT_ROOT'] . '/img/uploaded_files/googlefeeds/' . $userdomain . '.xml';
        file_put_contents($filename, GoogleShopping::asRss(false));
        echo 'done';
    }

    public function actionIndex() {
        //$id = Yii::$app->user->identity->id;
        //$channel = Channels::find()->Where(['channel_name' => 'Google shopping'])->one();
        //$channel_id = $channel['channel_ID'];
        //$channel_connection = ChannelConnection::find()->Where(['user_id' => $id, 'channel_id' => $channel_id])->one();
        return $this->render('index');

        //$channel_connection->delete();
        //if (!empty($channel_connection)) {
            //$google_merchant_id = $channel_connection['google_merchant_id'];
            /* 	//echo '<pre>'; print_r($channel_connection); echo '</pre>';
              $google_client_id = $channel['google_client_id'];
              $google_client_secret = $channel['google_client_secret'];
              $google_merchant_id = $channel['google_merchant_id'];
              $this->actionGoogle($google_client_id,$google_client_secret,$google_merchant_id); */
            // require_once $_SERVER['DOCUMENT_ROOT'].'/backend/web/googleApi/php/vendor/autoload.php';
            //$client = new \Google_Client();
            //echo '<pre>'; print_r($_SESSION['access_token']); echo '</pre>';
//            if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
//                $client->setAccessToken($_SESSION['access_token']);
//                $service = new \Google_Service_ShoppingContent($client);
//                $parameters = array('maxResults' => 250);
//                $products = $service->products->listProducts($google_merchant_id, $parameters);
//                $count = 0;
//                //echo '<pre>'; print_r($products->getResources()); die('dsaf');
//                $GoogleApiArray = $products->getResources();
//                $this->datasave($GoogleApiArray);
//            } 
//            else {
//                $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
//                $url = explode('google-shopping', $actual_link);
//                $actual_link = $url[0] . 'google-shopping';
//                $redirect_uri = $actual_link . '/oauth2callback/';
//                header('location: ' . $redirect_uri);
//            }
//            //$array_msg = array();
//            if (!empty($GoogleApiArray)) {
//                $_SESSION["googlecheck"] = "Okay";
//                return $this->render('index');
//            } 
//            else {
//                if (empty($GoogleApiArray)) {
//                    $channel_connection_delete = ChannelConnection::find()->Where(['user_id' => $id, 'channel_id' => $channel_id])->one();
//                    if (!empty($channel_connection_delete)) {
//                        $channel_connection_delete->delete();
//                        $_SESSION['access_token'] = '';
//                    }
//                }
//                $_SESSION["googlecheck"] = "No";
//
//                return $this->render('index');
//            }
//        } else {
//            $_SESSION["googlecheck"] = "enter";
//            return $this->render('index');
//        }
    }

    public function actionSave() {
        //echo '<pre>'; print_r($_POST); echo '</pre>'; 
        $user_id = Yii::$app->user->identity->id;
        $channel_big = Channels::find()->where(['channel_name' => 'Google shopping'])->one();

        if (!empty($channel_big)) {
            $channel_id = $channel_big->channel_ID;
        }
        $clientID = $_POST['clientID'];
        $secretKey = $_POST['secretKey'];
        $merchantID = $_POST['merchantID'];
        //$url = $this->actionOauth2callback($clientID,$secretKey,$merchantID); 
        $Google_connection = new ChannelConnection();
        $Google_connection->google_client_id = $_POST['clientID'];
        $Google_connection->google_client_secret = $_POST['secretKey'];
        $Google_connection->google_merchant_id = $_POST['merchantID'];
        $Google_connection->channel_id = $channel_big->channel_ID;
        $Google_connection->user_id = $user_id;
        $Google_connection->created = date('Y-m-d H:i:s');
        if ($Google_connection->save(false)) {
            echo 'success';
        } else {
            echo 'error';
        }
    }

    public function actionGoogle() {

        require_once $_SERVER['DOCUMENT_ROOT'] . '/backend/web/googleApi/php/vendor/autoload.php';
        $client = new \Google_Client();

        $client->setApplicationName('Sample Content API application');
        $client->setClientId('110121437169995257253');
        $client->setClientSecret('fpUEuTN1VJmzNkkJLSBWGFy2');
        $client->addScope('https://www.googleapis.com/auth/content');
        $client->setScopes('https://www.googleapis.com/auth/content');
        $client->addScope('https://www.googleapis.com/auth/drive.metadata.readonly');
        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {

            $client->setAccessToken($_SESSION['access_token']);


            $service = new \Google_Service_ShoppingContent($client);
            $parameters = array('maxResults' => 250);
            $products = $service->products->listProducts('117189394', $parameters);
            $count = 0;

            foreach ($products->getResources() as $product) {
                echo '<pre>';
                printf("%s %s\n", $product->getId(), $product->getTitle());
                echo '</pre>';
            }
        } else {

            $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            $url = explode('google-shopping', $actual_link);
            $actual_link = $url[0] . 'google-shopping';
            $redirect_uri = $actual_link . '/oauth2callback/';
            header('location: ' . $redirect_uri);
            die;
        }
    }

    public function actionOauth2callback() {
        //session_start();
        require_once $_SERVER['DOCUMENT_ROOT'] . '/backend/web/googleApi/php/vendor/autoload.php';
        $client = new \Google_Client();
        $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $redirect_uri = $actual_link . '/oauth2callback/';

        $id = Yii::$app->user->identity->id;
        $channel = Channels::find()->Where(['channel_name' => 'Google shopping'])->one();
        $channel_id = $channel['channel_ID'];
        $channel_connection = ChannelConnection::find()->Where(['user_id' => $id, 'channel_id' => $channel_id])->one();
        //echo '<pre>'; print_r($channel_connection); echo '</pre>'; die('dsfa');
        if (!empty($channel_connection)) {

            $google_client_id = $channel_connection['google_client_id'];
            $google_client_secret = $channel_connection['google_client_secret'];
            $google_merchant_id = $channel_connection['google_merchant_id'];
            //$this->actionGoogle($google_client_id,$google_client_secret,$google_merchant_id);
//$client->setAuthConfigFile('client_secrets.json');
            $client->setClientId($google_client_id);

            $client->setClientSecret($google_client_secret);
            $url = explode('oauth2callback', $actual_link);
            $actual_link1 = $url[0] . 'oauth2callback/';
            //$redirect_uri = $actual_link . '/oauth2callback/';

            $client->setRedirectUri($actual_link1);

            $client->setApplicationName('Sample Content API application');
            $client->addScope('https://www.googleapis.com/auth/content');
            $client->setScopes('https://www.googleapis.com/auth/content');
            $client->addScope('https://www.googleapis.com/auth/drive.metadata.readonly');

            if (!isset($_GET['code'])) {

                $auth_url = $client->createAuthUrl();
                header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
                exit;
                //echo filter_var($auth_url, FILTER_SANITIZE_URL);
            } else {
                $client->authenticate($_GET['code']);
                $_SESSION['access_token'] = $client->getAccessToken(); //echo $url[0]; die('dsf');

                header('Location: ' . filter_var($url[0], FILTER_SANITIZE_URL));
                exit;
            }
        }
    }

    public function actionInventoryupdate() {

        $id = Yii::$app->user->identity->id;
        $channel = Channels::find()->Where(['channel_name' => 'Google shopping'])->one();
        $channel_id = $channel['channel_ID'];
        $channel_connection = ChannelConnection::find()->Where(['user_id' => $id, 'channel_id' => $channel_id])->one();

        //$channel_connection->delete();
        if (!empty($channel_connection)) {
            $google_merchant_id = $channel_connection['google_merchant_id'];
            /* 	//echo '<pre>'; print_r($channel_connection); echo '</pre>';
              $google_client_id = $channel['google_client_id'];
              $google_client_secret = $channel['google_client_secret'];
              $google_merchant_id = $channel['google_merchant_id'];
              $this->actionGoogle($google_client_id,$google_client_secret,$google_merchant_id); */
            require_once $_SERVER['DOCUMENT_ROOT'] . '/backend/web/googleApi/php/vendor/autoload.php';
            $client = new \Google_Client();

            //echo '<pre>'; print_r($_SESSION['access_token']); echo '</pre>';
            if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
                $stock = strtolower($_SESSION['stockGS']);
                $price123 = $_SESSION['priceGS'];
                $currency = $_SESSION['currencyGS'];
                $merchantID = $_SESSION['merchantGS'];
                $skuGS = $_SESSION['skuGS'];
                //echo $stock.'-'.$price.'-'.$currency.'-'.$merchantID.'-'.$skuGS; die('sdaf');
                $client->setAccessToken($_SESSION['access_token']);
                $service = new \Google_Service_ShoppingContent($client);
                //echo '<pre>'; print_r($service); echo '</pre>'; die('dsf');
                /* $parameters = array('maxResults' => 250);
                  $products = $service->products->listProducts($google_merchant_id, $parameters); */

                $inventory = new \Google_Service_ShoppingContent_InventorySetRequest();
                $inventory->setAvailability($stock);

                $price = new \Google_Service_ShoppingContent_Price();
                $price->setValue($price123);
                $price->setCurrency($currency);

                $inventory->setPrice($price);

                // Make the request

                $response = $service->inventory->set($merchantID, 'online', $skuGS, $inventory);
                //echo '<pre>'; print_r($_SESSION); echo '</pre>'; die('dsf');
                $_SESSION['stockGS'] = '';
                $_SESSION['priceGS'] = '';
                $_SESSION['currencyGS'] = '';
                $_SESSION['merchantGS'] = '';
                $_SESSION['skuGS'] = '';
                $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                $link = explode('google-shopping', $actual_link);
                $url = $link[0] . 'products/';
                header('Location: ' . $url);
                exit;
            } else {
                $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                $link = explode('google-shopping', $actual_link);
                $url = $link[0] . 'products';
                header('Location: ' . $url);
                exit;
            }
        }
    }

    public function datasave($data) {
        //echo '<pre>'; print_r($data); echo '</pre>'; die();
        $user_id = Yii::$app->user->identity->id;
        foreach ($data as $product) {

            $pro_id = 'GS' . $product->getId();
            $title = $product->getTitle();
            $variants_sku = $product->getId();
            $variants_barcode = 'null';
            $p_mpn = $product->getMpn();
            $description = $product->getDescription();
            $adult = $product->getAdult();
            $agegroup = $product->getAgeGroup();
            $gender = $product->getGender();
            $brand = $product->getBrand();
            $unit_pricing_measure = $product->getUnitPricingMeasure();
            $availability = $product->getAvailability();
            $price = $product['price']['value'];
            $currency = $product['price']['currency'];
            $price = explode(' ', $price);
            if (!empty($price)) {
                $price = $price[0];
                // $currency = $price[1]; 
            } else {
                $price = $product['price']['value'];
            }
            $sale_price = $product['customAttributes']['4']['value'];
            $sale_price = explode(' ', $sale_price);
            if (!empty($sale_price)) {
                $sale_price = $sale_price[0];
            } else {
                $sale_price = $product['customAttributes']['4']['value'];
            }
            if (is_numeric($sale_price)) {
                $sale_price = $sale_price;
            } else {
                $sale_price = '';
            }
            // $sale_​​price = '';
            $variants_created_at = $product->getAvailabilityDate();
            $variants_updated_at = $product->getExpirationDate();
            $google_product_category = $product['customAttributes']['11']['value'];
            //$google_​​product_​​category = '';
            $imagelink = $product->getImageLink();
            $mobile_link = $product->getMobileLink();

            //die('dsf');
            $checkVarModel = Products::find()->where(['channel_abb_id' => $pro_id])->one();
            // echo '<pre>'; print_r($checkVarModel); echo '</pre>';
            if (empty($checkVarModel)) {
                // echo  $pro_id =  'GG_'.$product->getId(); echo '</br>';
                //echo  $adult ; echo '</br>';
                // die('dsf');
                $productModel = new Products();
                $productModel->channel_abb_id = $pro_id;
                $productModel->product_name = $title;
                $productModel->SKU = $variants_sku;
                $productModel->UPC = $variants_barcode;
                if (!empty($currency)) {
                    $productModel->currency = $currency;
                }

                $productModel->EAN = 'null';
                $productModel->Jan = 'null';
                $productModel->ISBN = 'null';


                if ($p_mpn) {
                    $productModel->MPN = $p_mpn;
                } else {
                    $productModel->MPN = 'null';
                }
                $productModel->description = $description;
                if ($adult) {
                    $productModel->adult = $adult;
                } else {
                    $productModel->adult = 'null';
                }
                $productModel->age_group = $agegroup;
                $productModel->gender = $gender;
                $productModel->brand = $brand;
                $productModel->weight = $unit_pricing_measure;
                $productModel->stock_quantity = '';
                $productModel->availability = $availability;
                $productModel->stock_status = 'Visible';

                $productModel->price = $price;
                $productModel->sales_price = $sale_price;
                $productModel->created_at = date('Y-m-d h:i:s', strtotime($variants_created_at));
                $productModel->updated_at = date('Y-m-d h:i:s', strtotime($variants_updated_at));
                $productModel->elliot_user_id = $user_id;
                $productModel->save(false);
                $last_pro_id = $productModel->id;
                $custom_productChannelModel = new ProductChannel;
                //$store_id = Stores::find()->select('store_id')->where(['store_name' => 'Shopify'])->one();
                $channel = Channels::find()->Where(['channel_name' => 'Google shopping'])->one();
                $channel_id = $channel['channel_ID'];
                $custom_productChannelModel->store_id = '';
                $custom_productChannelModel->channel_id = $channel_id;
                $custom_productChannelModel->product_id = $last_pro_id;
                $custom_productChannelModel->elliot_user_id = $user_id;
                $custom_productChannelModel->created_at = date('Y-m-d h:i:s', time());
                $custom_productChannelModel->save(false);


                //Create Model for Each new Product Category
                /*  $Categories = new Categories;
                  $Categories->channel_abb_id = $pro_id;
                  if (is_numeric($google_​​product_​​category)) {
                  $excel = new PhpExcelReader;
                  $excel->read('taxonomy-with-ids.en-US.xls');
                  }
                  else{

                  } */

                if (is_numeric($google_product_category)) {
                    //echo $google_​​product_​​category;
                    require_once $_SERVER['DOCUMENT_ROOT'] . '/backend/controllers/PhpExcelReaderController.php';
                    $excel = new \PhpExcelReader();
                    $excel->read($_SERVER['DOCUMENT_ROOT'] . '/backend/controllers/taxonomy-with-ids.en-US.xls');
                    $nr_sheets = count($excel->sheets);       // gets the number of sheets
                    // traverses the number of sheets and sets html table with each sheet data in $excel_data
                    for ($i = 0; $i < $nr_sheets; $i++) {

                        $sheet = $excel->sheets[$i];
                        $re = array();     // starts html table

                        $x = 1;
                        while ($x <= $sheet['numRows']) {

                            $y = 1;
                            while ($y <= $sheet['numCols']) {
                                $cell = isset($sheet['cells'][$x][$y]) ? $sheet['cells'][$x][$y] : '';
                                if ($google_product_category == $cell) {
                                    $catx = $x;
                                }
                                $re[$x][$y] = $cell;
                                $y++;
                            }
                            $x++;
                        }
                        $cateName = $re[$catx][2];
                        //echo '<pre>'; print_r($re[$catx]) ; echo '</pre>'; die();
                    }
                    $catcheck = 'GS' . $google_product_category;

                    $Categories = Categories::find()->where(['channel_abb_id' => $catcheck])->one();
                    if (empty($Categories)) {
                        $Categories = new Categories;
                    }
                    $Categories->channel_abb_id = 'GS' . $google_product_category;
                    $Categories->category_name = $cateName;
                    $Categories->parent_category_ID = '0';
                    $Categories->store_category_groupid = $google_product_category;
                    $Categories->created_at = date('Y-m-d h:i:s', time());
                    $Categories->elliot_user_id = $user_id;
                    $Categories->save(false);



                    $last_categories_id = $Categories->category_ID;
                    $productCategoryModel = ProductCategories::find()->where(['product_ID' => $last_pro_id])->one();
                    if (empty($productCategoryModel)) {
                        $productCategoryModel = new ProductCategories;
                    }

                    $productCategoryModel->elliot_user_id = $user_id;
                    $productCategoryModel->product_ID = $last_pro_id;
                    $productCategoryModel->category_ID = $last_categories_id;
                    $productCategoryModel->created_at = date('Y-m-d h:i:s', time());
                    $productCategoryModel->save(false);
                } else {
                    $catName = explode('>', $google_product_category);
                    if (!empty($catName)) {
                        $catName = end($catName);
                    } else {
                        $catName = $google_product_category;
                    }
                    $catName;
                    //die('dfasd');
                    //echo $google_​​product_​​category;
                    require_once $_SERVER['DOCUMENT_ROOT'] . '/backend/controllers/PhpExcelReaderController.php';
                    $excel = new \PhpExcelReader();
                    $excel->read($_SERVER['DOCUMENT_ROOT'] . '/backend/controllers/taxonomy-with-ids.en-US.xls');
                    $nr_sheets = count($excel->sheets);       // gets the number of sheets
                    // traverses the number of sheets and sets html table with each sheet data in $excel_data
                    for ($i = 0; $i < $nr_sheets; $i++) {

                        $sheet = $excel->sheets[$i];
                        $re = array();     // starts html table

                        $x = 1;
                        while ($x <= $sheet['numRows']) {

                            $y = 1;
                            while ($y <= $sheet['numCols']) {
                                $cell = isset($sheet['cells'][$x][$y]) ? $sheet['cells'][$x][$y] : '';
                                if (trim($catName) == trim($cell)) {
                                    $catx = $x;
                                }
                                $re[$x][$y] = $cell;
                                $y++;
                            }
                            $x++;
                        }
                        $cateid = $re[$catx][1];
                        //echo '<pre>'; print_r($re[$catx]) ; echo '</pre>'; die();
                        $catcheck = 'GS' . $cateid;
                    }

                    $Categories = Categories::find()->where(['channel_abb_id' => $catcheck])->one();
                    if (empty($Categories)) {
                        $Categories = new Categories;
                    }
                    //$Categories = new Categories;
                    $Categories->category_name = $catName;
                    $Categories->channel_abb_id = 'GS' . $cateid;
                    $Categories->parent_category_ID = '0';
                    $Categories->store_category_groupid = $cateid;
                    $Categories->created_at = date('Y-m-d h:i:s', time());
                    $Categories->elliot_user_id = $user_id;
                    $Categories->save(false);



                    $last_categories_id = $Categories->category_ID;

                    $productCategoryModel = ProductCategories::find()->where(['product_ID' => $last_pro_id])->one();
                    if (empty($productCategoryModel)) {
                        $productCategoryModel = new ProductCategories;
                    }
                    $productCategoryModel = new ProductCategories;
                    $productCategoryModel->elliot_user_id = $user_id;
                    $productCategoryModel->product_ID = $last_pro_id;
                    $productCategoryModel->category_ID = $last_categories_id;
                    $productCategoryModel->created_at = date('Y-m-d h:i:s', time());
                    $productCategoryModel->save(false);
                }





                $ProductImages = new ProductImages;
                $ProductImages->elliot_user_id = $user_id;
                $ProductImages->product_ID = $last_pro_id;
                $ProductImages->default_image = $imagelink;
                $ProductImages->alternative_image = $mobile_link;
                $ProductImages->priority = '1';
                $ProductImages->created_at = date('Y-m-d h:i:s', time());
                $ProductImages->save(false);
            } else {
                //echo  $pro_id =  'GG_'.$product->getId(); echo '</br>';
                $productModel = Products::find()->where(['channel_abb_id' => $pro_id])->one();
                // $productModel = new Products();
                $productModel->channel_abb_id = $pro_id;
                $productModel->product_name = $title;
                $productModel->SKU = $variants_sku;
                $productModel->UPC = $variants_barcode;


                $productModel->EAN = 'null';
                $productModel->Jan = 'null';
                $productModel->ISBN = 'null';
                if (!empty($currency)) {
                    $productModel->currency = $currency;
                }

                if ($p_mpn) {
                    $productModel->MPN = $p_mpn;
                } else {
                    $productModel->MPN = 'null';
                }
                $productModel->description = $description;
                if ($adult) {
                    $productModel->adult = $adult;
                } else {
                    $productModel->adult = 'null';
                }
                $productModel->age_group = $agegroup;
                $productModel->gender = $gender;
                $productModel->brand = $brand;
                $productModel->weight = $unit_pricing_measure;
                $productModel->stock_quantity = '';
                $productModel->availability = $availability;
                $productModel->stock_status = 'Visible';

                $productModel->price = $price;
                $productModel->sales_price = $sale_price;
                $productModel->created_at = date('Y-m-d h:i:s', strtotime($variants_created_at));
                $productModel->updated_at = date('Y-m-d h:i:s', strtotime($variants_updated_at));
                $productModel->elliot_user_id = $user_id;
                $productModel->save(false);
                $last_pro_id = $productModel->id;
                // $custom_productChannelModel = new ProductChannel;
                //$store_id = Stores::find()->select('store_id')->where(['store_name' => 'Shopify'])->one();
                $custom_productChannelModel = ProductChannel::find()->where(['product_channel_id' => $last_pro_id])->one();
                $channel = Channels::find()->Where(['channel_name' => 'Google shopping'])->one();
                $channel_id = $channel['channel_ID'];
                $custom_productChannelModel->store_id = '';
                $custom_productChannelModel->channel_id = $channel_id;
                $custom_productChannelModel->product_id = $last_pro_id;
                $custom_productChannelModel->elliot_user_id = $user_id;
                $custom_productChannelModel->created_at = date('Y-m-d h:i:s', time());
                $custom_productChannelModel->save(false);




                if (is_numeric($google_product_category)) {
                    //echo $google_​​product_​​category;
                    require_once $_SERVER['DOCUMENT_ROOT'] . '/backend/controllers/PhpExcelReaderController.php';
                    $excel = new \PhpExcelReader();
                    $excel->read($_SERVER['DOCUMENT_ROOT'] . '/backend/controllers/taxonomy-with-ids.en-US.xls');
                    $nr_sheets = count($excel->sheets);       // gets the number of sheets
                    // traverses the number of sheets and sets html table with each sheet data in $excel_data
                    for ($i = 0; $i < $nr_sheets; $i++) {

                        $sheet = $excel->sheets[$i];
                        $re = array();     // starts html table

                        $x = 1;
                        while ($x <= $sheet['numRows']) {

                            $y = 1;
                            while ($y <= $sheet['numCols']) {
                                $cell = isset($sheet['cells'][$x][$y]) ? $sheet['cells'][$x][$y] : '';
                                if ($google_product_category == $cell) {
                                    $catx = $x;
                                }
                                $re[$x][$y] = $cell;
                                $y++;
                            }
                            $x++;
                        }
                        $cateName = $re[$catx][2];
                        //echo '<pre>'; print_r($re[$catx]) ; echo '</pre>'; die();
                    }
                    $checkcat = 'GS' . $google_product_category;
                    $Categories = Categories::find()->where(['channel_abb_id' => $checkcat])->one();
                    // echo '<pre>'; print_r($checkVarModel); echo '</pre>';
                    if (empty($Categories)) {
                        $Categories = new Categories;
                        $Categories->category_name = $cateName;
                        $Categories->channel_abb_id = 'GS' . $google_product_category;
                        $Categories->parent_category_ID = '0';
                        $Categories->store_category_groupid = $google_product_category;
                        $Categories->created_at = date('Y-m-d h:i:s', time());
                        $Categories->elliot_user_id = $user_id;
                        $Categories->save(false);
                    }
                    $productCategoryModel = ProductCategories::find()->where(['product_ID' => $last_pro_id])->one();
                    //echo $user_id;
                    if (empty($productCategoryModel)) {
                        $productCategoryModel = new ProductCategories;
                    }
                    $last_categories_id = $Categories->category_ID;
                    //$productCategoryModel = new ProductCategories;
                    $productCategoryModel->elliot_user_id = $user_id;
                    $productCategoryModel->product_ID = $last_pro_id;
                    $productCategoryModel->category_ID = $last_categories_id;
                    $productCategoryModel->created_at = date('Y-m-d h:i:s', time());
                    $productCategoryModel->save(false);
                    /* else{
                      // $Categories = new Categories;
                      $Categories = Categories::find()->where(['channel_abb_id' => $checkcat])->one();
                      $Categories->channel_abb_id = 'GS'.$google_product_category;
                      $Categories->category_name = $cateName;
                      $Categories->parent_category_ID = '0';
                      $Categories->store_category_groupid = $google_product_category;
                      $Categories->created_at = date('Y-m-d h:i:s', time());
                      $Categories->elliot_user_id = $user_id;
                      $Categories->save(false);



                      $last_categories_id = $Categories->category_ID;
                      $productCategoryModel = ProductCategories::find()->where(['category_ID' => $last_categories_id])->one();
                      //echo $user_id;
                      if(empty($productCategoryModel)) {
                      $productCategoryModel = new ProductCategories;
                      }
                      //	$productCategoryModel = new ProductCategories; sdsdfsdf
                      $productCategoryModel->elliot_user_id = $user_id;
                      $productCategoryModel->product_ID = $last_pro_id;
                      $productCategoryModel->category_ID = $last_categories_id;
                      $productCategoryModel->created_at = date('Y-m-d h:i:s', time());
                      $productCategoryModel->save(false);
                      //echo '<pre>'; print_r($productCategoryModel); echo '</pre>'; die('df');


                      } */
                } else {
                    $catName = explode('>', $google_product_category);
                    if (!empty($catName)) {
                        $catName = end($catName);
                    } else {
                        $catName = $google_product_category;
                    }
                    $catName;
                    //die('dfasd');
                    //echo $google_​​product_​​category;
                    require_once $_SERVER['DOCUMENT_ROOT'] . '/backend/controllers/PhpExcelReaderController.php';
                    $excel = new \PhpExcelReader();
                    $excel->read($_SERVER['DOCUMENT_ROOT'] . '/backend/controllers/taxonomy-with-ids.en-US.xls');
                    $nr_sheets = count($excel->sheets);       // gets the number of sheets
                    // traverses the number of sheets and sets html table with each sheet data in $excel_data
                    for ($i = 0; $i < $nr_sheets; $i++) {

                        $sheet = $excel->sheets[$i];
                        $re = array();     // starts html table

                        $x = 1;
                        while ($x <= $sheet['numRows']) {

                            $y = 1;
                            while ($y <= $sheet['numCols']) {
                                $cell = isset($sheet['cells'][$x][$y]) ? $sheet['cells'][$x][$y] : '';
                                if (trim($catName) == trim($cell)) {
                                    $catx = $x;
                                }
                                $re[$x][$y] = $cell;
                                $y++;
                            }
                            $x++;
                        }
                        $cateid = $re[$catx][1];
                        //echo '<pre>'; print_r($re[$catx]) ; echo '</pre>'; die();
                    }
                    $checkcat = 'GS' . $cateid;

                    $Categories = Categories::find()->where(['channel_abb_id' => $checkcat])->one();
                    // echo '<pre>'; print_r($checkVarModel); echo '</pre>';
                    if (empty($Categories)) {
                        $Categories = new Categories;
                        $Categories->category_name = $catName;
                        $Categories->channel_abb_id = 'GS' . $cateid;
                        $Categories->parent_category_ID = '0';
                        $Categories->store_category_groupid = $cateid;
                        $Categories->created_at = date('Y-m-d h:i:s', time());
                        $Categories->elliot_user_id = $user_id;
                        $Categories->save(false);



                        /* $last_categories_id = $Categories->category_ID;
                          $productCategoryModel = new ProductCategories;
                          $productCategoryModel->elliot_user_id = $user_id;
                          $productCategoryModel->product_ID = $last_pro_id;
                          $productCategoryModel->category_ID = $last_categories_id;
                          $productCategoryModel->created_at = date('Y-m-d h:i:s', time());
                          $productCategoryModel->save(false); */
                    }
                    $productCategoryModel = ProductCategories::find()->where(['product_ID' => $last_pro_id])->one();
                    if (empty($productCategoryModel)) {
                        $productCategoryModel = new ProductCategories;
                    }
                    $last_categories_id = $Categories->category_ID;
                    //$productCategoryModel = new ProductCategories;
                    $productCategoryModel->elliot_user_id = $user_id;
                    $productCategoryModel->product_ID = $last_pro_id;
                    $productCategoryModel->category_ID = $last_categories_id;
                    $productCategoryModel->created_at = date('Y-m-d h:i:s', time());
                    $productCategoryModel->save(false);
                    /* else
                      {
                      // $Categories = new Categories;
                      $Categories = Categories::find()->where(['channel_abb_id' => $checkcat])->one();
                      $Categories->channel_abb_id = 'GS'.$cateid;
                      $Categories->category_name = $catName;
                      $Categories->parent_category_ID = '0';
                      $Categories->store_category_groupid = $cateid;
                      $Categories->created_at = date('Y-m-d h:i:s', time());
                      $Categories->elliot_user_id = $user_id;
                      $Categories->save(false);



                      $last_categories_id = $Categories->category_ID;
                      $productCategoryModel = ProductCategories::find()->where(['category_ID' => $last_categories_id])->one();
                      if(empty($productCategoryModel)) {
                      $productCategoryModel = new ProductCategories;
                      }
                      $productCategoryModel->elliot_user_id = $user_id;
                      $productCategoryModel->product_ID = $last_pro_id;
                      $productCategoryModel->category_ID = $last_categories_id;
                      $productCategoryModel->created_at = date('Y-m-d h:i:s', time());
                      $productCategoryModel->save(false);


                      } */
                }




                $ProductImages = ProductImages::find()->where(['product_ID' => $last_pro_id])->one();

                //$ProductImages = new ProductImages;
                $ProductImages->elliot_user_id = $user_id;
                $ProductImages->product_ID = $last_pro_id;
                $ProductImages->default_image = $imagelink;
                $ProductImages->alternative_image = $mobile_link;
                $ProductImages->priority = '1';
                $ProductImages->created_at = date('Y-m-d h:i:s', time());
                $ProductImages->save(false);
            }

            // echo '<pre>'; printf("%s %s\n", $product->getId(), $product->getTitle()); echo '</pre>';
        }
    }

    /* Get List of Product with user id */

    public function actionProducts() {
        $id = $_GET['id'];
        $id = base64_decode($id);
        $id1 = explode('*', $id);
        // echo '<pre>'; print_r($id1); die('sdf');
        $id = $id1[1];
        $user_data = User::find()->Where(['id' => $id])->one();
        if (!empty($user_data)) {
            $userdomain = $user_data->domain_name;


            $config = yii\helpers\ArrayHelper::merge(
                            require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $userdomain . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
            );
            $application = new yii\web\Application($config);
            $ProductData = Products::find()->with('productImages')->with('productCategories')->with('productVariations')->where(['elliot_user_id' => $id])->all();
            //echo '<pre>'; print_r($ProductData); echo '</pre>';die('dfas');
            $json_array = array();

            foreach ($ProductData as $product) {
                $id = $product->id;
                $json_array[$id]['id'] = $product->id;
                $json_array[$id]['elliot_user_id'] = $product->elliot_user_id;
                $json_array[$id]['channel_abb_id'] = $product->channel_abb_id;
                $json_array[$id]['product_name'] = $product->product_name;
                $json_array[$id]['SKU'] = $product->SKU;
                $json_array[$id]['product_url'] = $product->product_url;
                $json_array[$id]['UPC'] = $product->UPC;
                $json_array[$id]['EAN'] = $product->EAN;
                $json_array[$id]['Jan'] = $product->Jan;
                $json_array[$id]['ISBN'] = $product->ISBN;
                $json_array[$id]['MPN'] = $product->MPN;
                $json_array[$id]['MPN'] = $product->MPN;
                $json_array[$id]['description'] = utf8_encode(htmlentities(str_replace('"', '', $product->description)));
                $json_array[$id]['adult'] = $product->adult;
                $json_array[$id]['age_group'] = $product->age_group;
                $json_array[$id]['availability'] = $product->availability;
                $json_array[$id]['brand'] = $product->brand;
                $json_array[$id]['condition'] = $product->condition;
                $json_array[$id]['gender'] = $product->gender;
                $json_array[$id]['weight'] = $product->weight;
                $json_array[$id]['variations_set_id'] = $product->variations_set_id;
                $json_array[$id]['stock_quantity'] = $product->stock_quantity;
                $json_array[$id]['currency'] = $product->currency;
                $json_array[$id]['stock_level'] = $product->stock_level;
                $json_array[$id]['stock_status'] = $product->stock_status;
                $json_array[$id]['low_stock_notification'] = $product->low_stock_notification;
                $json_array[$id]['price'] = $product->price;
                $json_array[$id]['sales_price'] = $product->sales_price;
                $json_array[$id]['schedules_sales_date'] = $product->schedules_sales_date;
                $json_array[$id]['created_at'] = $product->created_at;
                $json_array[$id]['updated_at'] = $product->updated_at;
                $json_array[$id]['occasion'] = $product->occasion;
                $json_array[$id]['weather'] = $product->weather;
                $json_array[$id]['magento_store_id'] = $product->magento_store_id;
                $Images = $product->productImages;
                $Categories = $product->productCategories;
                $Variations = $product->productVariations;
                $im = 0;
                foreach ($Images as $image) {
                    $json_array[$id]['productImages'][$im]['image_ID'] = $image->image_ID;
                    $json_array[$id]['productImages'][$im]['elliot_user_id'] = $image->elliot_user_id;
                    $json_array[$id]['productImages'][$im]['product_ID'] = $image->product_ID;
                    $json_array[$id]['productImages'][$im]['label'] = $image->label;
                    $json_array[$id]['productImages'][$im]['link'] = $image->link;
                    $json_array[$id]['productImages'][$im]['tag_status'] = $image->tag_status;
                    $json_array[$id]['productImages'][$im]['tag'] = $image->tag;
                    $json_array[$id]['productImages'][$im]['priority'] = $image->priority;
                    $json_array[$id]['productImages'][$im]['default_image'] = $image->default_image;
                    $json_array[$id]['productImages'][$im]['alternative_image'] = $image->alternative_image;
                    $json_array[$id]['productImages'][$im]['html_video_link'] = $image->html_video_link;
                    $json_array[$id]['productImages'][$im]['_360_degree_video_link'] = $image->_360_degree_video_link;
                    $json_array[$id]['productImages'][$im]['image_status'] = $image->image_status;
                    $json_array[$id]['productImages'][$im]['created_at'] = $image->created_at;
                    $json_array[$id]['productImages'][$im]['updated_at'] = $image->updated_at;
                    $im++;
                }
                $c = 0;
                foreach ($Categories as $category) {
                    $json_array[$id]['productCategories'][$c]['product_categories_id'] = $category->product_categories_id;
                    $json_array[$id]['productCategories'][$c]['elliot_user_id'] = $category->elliot_user_id;
                    $json_array[$id]['productCategories'][$c]['category_ID'] = $category->category_ID;
                    $json_array[$id]['productCategories'][$c]['product_ID'] = $category->product_ID;
                    $json_array[$id]['productCategories'][$c]['created_at'] = $category->created_at;
                    $json_array[$id]['productCategories'][$c]['updated_at'] = $category->updated_at;
                    $c++;
                }
                $v = 0;
                foreach ($Variations as $variation) {
                    $json_array[$id]['productVariations'][$variation->store_variation_id][$v]['product_variation_id'] = $variation->product_variation_id;
                    $json_array[$id]['productVariations'][$variation->store_variation_id][$v]['elliot_user_id'] = $variation->elliot_user_id;
                    $json_array[$id]['productVariations'][$variation->store_variation_id][$v]['variation_id'] = $variation->variation_id;
                    $json_array[$id]['productVariations'][$variation->store_variation_id][$v]['product_id'] = $variation->product_id;
                    $json_array[$id]['productVariations'][$variation->store_variation_id][$v]['item_name'] = $variation->item_name;
                    $json_array[$id]['productVariations'][$variation->store_variation_id][$v]['item_value'] = $variation->item_value;
                    $json_array[$id]['productVariations'][$variation->store_variation_id][$v]['store_variation_id'] = $variation->store_variation_id;
                    $json_array[$id]['productVariations'][$variation->store_variation_id][$v]['created_at'] = $variation->created_at;
                    $json_array[$id]['productVariations'][$variation->store_variation_id][$v]['updated_at'] = $variation->updated_at;
                    $v++;
                }
            }

            //echo '<pre>'; print_r($json_array); echo '</pre>'; die('sdf');
            echo json_encode($json_array);
        } else {
            $add = 'denied';
            return $add;
        }
    }

    /* Get List of Product with user id */

    public function actionGoogleProductCategory() {

        if (!empty($_GET['querymain'])) {

            require_once $_SERVER['DOCUMENT_ROOT'] . '/backend/controllers/PhpExcelReaderController.php';
            $excel = new \PhpExcelReader();
            $excel->read($_SERVER['DOCUMENT_ROOT'] . '/backend/controllers/taxonomy-with-ids.en-US.xls');
            $nr_sheets = count($excel->sheets);       // gets the number of sheets
            // traverses the number of sheets and sets html table with each sheet data in $excel_data
            for ($i = 0; $i < $nr_sheets; $i++) {

                $sheet = $excel->sheets[$i];
                $re = array();     // starts html table

                $x = 1;
                while ($x <= $sheet['numRows']) {

                    $y = 1;
                    while ($y <= $sheet['numCols']) {
                        $cell = isset($sheet['cells'][$x][$y]) ? $sheet['cells'][$x][$y] : '';
                        if ($cell == trim($_GET['querymain'])) {
                            //print_r($sheet['cells'][$x]); die('dfdsf');
                            if (!empty($sheet['cells'][$x][3])) {
                                //$re[] = $sheet['cells'][$x][3].','.$sheet['cells'][$x][1];
                                $re[$sheet['cells'][$x][1]] = $sheet['cells'][$x][3];
                            }
                        }
                        // $re[$x][$y] = $cell;
                        $y++;
                    }
                    $x++;
                }
            }
            $data = array_filter(array_unique($re));
            //	echo json_encode($data); die('dsf');
            return json_encode($data);
            //echo '<pre>'; print_r(array_filter(array_unique($re))) ; echo '</pre>'; die('dfasd');
        } else {

            $query = isset($_REQUEST['query']) ? $_REQUEST['query'] : $_REQUEST['term'];
            $sql = "SELECT google_category_name FROM google_product_categories WHERE google_category_name LIKE '{$query}%'";
            $google_categories = GoogleProductCategories::findBySql($sql)->all();
            $cat_array = array();
            foreach ($google_categories as $cat) {
                $cat_array[] = $cat->google_category_name;
            }
            //RETURN JSON ARRAY
            return json_encode($cat_array);
        }
    }

}
