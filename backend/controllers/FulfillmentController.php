<?php

//

namespace backend\controllers;

use Yii;
use backend\models\Fulfillment;
use backend\models\FulfillmentList;
use backend\models\FulfillmentSearch;
use backend\models\Stores;
use Bigcommerce\Api\Connection;
use Bigcommerce\Api\Client as Bigcommerce;
use backend\models\Orders;
use backend\models\Products;
use backend\models\ProductImages;
use backend\models\Countries;
use backend\models\StoresConnection;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\ProductVariation;
use yii\filters\AccessControl;
use yii\db\Query;

/**
 * FulfillmentController implements the CRUD actions for Fulfillment model.
 */
class FulfillmentController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'shipstation', 'carriers', 'software'],
                'rules' => [
                        [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                        [
                        'actions' => ['logout', 'index', 'view', 'create', 'update', 'shipstation', 'carriers', 'software'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Fulfillment models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new FulfillmentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Fulfillment models.
     * @return mixed
     */
    public function actionShipstation() {


        return $this->render('shipstation');
    }

    /**
     * Displays a single Fulfillment model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Fulfillment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Fulfillment();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Fulfillment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    //ShipStation Instruction Page
    public function actionInstruction() {

        return $this->render('instruction');
    }

    //export orders elliot to shipstation

    public function actionGenratexml() {

        //header('Content-type: text/xml');
        //Get current login User id
        $user_id = Yii::$app->request->get('shipid');

        //$user_id = Yii::$app->user->identity->id;
        //Get All orders 
        $curr_url = explode(".", "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
        $uname = $curr_url[0];
        $new_db_name = $uname . '_elliot';
        $config = [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=' . $new_db_name . '',
            'username' => Yii::$app->params['DB_USERNAME'],
            'password' => Yii::$app->params['DB_PASSWORD'],
            'charset' => 'utf8',
        ];
        $db_merchant = Yii::createObject($config);
        $orders_data = $db_merchant->createCommand("SELECT * from orders WHERE elliot_user_id='" . $user_id . "'")->queryAll();

        $data = '<?xml version="1.0" encoding="utf-8"?>';
        $data .= '<Orders>';
        foreach ($orders_data as $asorders) :
            //Get customers
            $customer_id = $asorders['customer_id'];
            $customer_data = $db_merchant->createCommand("SELECT * from Customer_User WHERE customer_ID='" . $customer_id . "'")->queryAll();

            //For Countries Abbrivation
            $countries_data = $db_merchant->createCommand("SELECT * from countries WHERE name ='" . $asorders['ship_country'] . "'")->queryAll();
            if (!empty($countries_data)) :
                $country_abb = $countries_data[0]['sortname'];
            else:
                $country_abb = 'US';
            endif;

            //$countries_data = Countries::find()->Where(['name' => $asorders->ship_country])->one();
            $data .= '<Order>';
            $data .= '<OrderID><![CDATA[' .$asorders['order_ID'] . ']]></OrderID>';
            $data .= '<OrderNumber><![CDATA[' .$asorders['order_ID'] . ']]></OrderNumber>';
            $data .= '<OrderDate>12/8/2011 21:56 PM</OrderDate>';
            $data .= '<OrderStatus><![CDATA[' .$asorders['order_status'] . ']]></OrderStatus>';
            $data .= '<LastModified>12/8/2011 21:56 PM</LastModified>';
            $data .= '<PaymentMethod><![CDATA[' .$asorders['payment_method'] . ']]></PaymentMethod>';
            $data .= '<OrderTotal><![CDATA[' .$asorders['total_amount'] . ']]></OrderTotal>';
            $data .= '<ShippingAmount>' . (int) $asorders['shipping_cost_tax'] . '</ShippingAmount>';
            $data .= '<Customer>';
            $data .= '<CustomerCode>' . $customer_data[0]['email'] . '</CustomerCode>';
            $data .= '<BillTo>';
//            $data .= '<Name>' . $customer_data[0]['first_name'] . '</Name>';
            $data .= '<Name><![CDATA[' . $customer_data[0]['first_name'] . ']]></Name>';
            $data .= '<Phone>' . $customer_data[0]['phone_number'] . '</Phone>';
            $data .= '<Email>' . $customer_data[0]['email'] . '</Email>';
            $data .= '</BillTo>';
            $data .= '<ShipTo>';
            $data .= '<Name><![CDATA[' . $customer_data[0]['first_name'] . ']]></Name>';
            $data .= '<Address1><![CDATA[' . $asorders['ship_street_1'] . ']]></Address1>';
            $data .= '<Address2><![CDATA[' . $asorders['ship_street_2'] . ']]></Address2>';
            $data .= '<City><![CDATA[' . $asorders['ship_city'] . ']]></City>';
            $data .= '<State><![CDATA[' . $asorders['ship_state'] . ']]></State>';
            $data .= '<PostalCode><![CDATA[' . $asorders['ship_zip'] . ']]></PostalCode>';
            $data .= '<Country><![CDATA[' . $country_abb . ']]></Country>';
            $data .= '<Phone>' . $customer_data[0]['phone_number'] . '</Phone>';
            $data .= '</ShipTo>';
            $data .= '</Customer>';
            $data .= '<Items>';
            //For product items
            $order_product = $db_merchant->createCommand("SELECT * from orders_products WHERE elliot_user_id='" . $user_id . "' AND order_Id= '" . $asorders['order_ID'] . "'")->queryAll();
            foreach ($order_product as $order_products) :
                $product_id = $order_products['product_Id'];
                $qty = $order_products['qty'];
                $elliot_user_id = $order_products['elliot_user_id'];
                $order_product_sku = $order_products['order_product_sku'];
                //Product Image
                $product_img_data = $db_merchant->createCommand("SELECT * from Product_images WHERE product_ID='" . $product_id . "'")->queryAll();
                // Products details
                $product_details = $db_merchant->createCommand("SELECT * from products WHERE id='" . $product_id . "'")->queryAll();

                $data .= '<Item>';
                $data .= '<SKU><![CDATA[' . $product_details[0]['SKU'] . ']]></SKU>';
                // $data .= '<Name>anc</Name>';
                $data .= '<Name><![CDATA[' . $product_details[0]['product_name'] . ']]></Name>';
                if (!empty($product_img_data)) :
                    $data .= '<ImageUrl><![CDATA[' . $product_img_data[0]['link'] . ']]></ImageUrl>';
                endif;
                $data .= '<Weight><![CDATA[' . $product_details[0]['weight'] . ']]></Weight>';
                $data .= '<Quantity><![CDATA[' . $qty . ']]></Quantity>';
                $data .= '<UnitPrice><![CDATA[' . $product_details[0]['price'] . ']]></UnitPrice>';
                //Product variations for Options
                $product_variations_data = $db_merchant->createCommand("SELECT * from product_variation WHERE elliot_user_id='" . $elliot_user_id . "' AND product_id='" . $product_id . "' AND item_value='" . $order_product_sku . "' ")->queryAll();
                if (!empty($product_variations_data)) :
                    $store_variotion_id = $product_variations_data[0]['store_variation_id'];
                    $variation = $db_merchant->createCommand("SELECT * from product_variation WHERE store_variation_id='" . $store_variotion_id . "' AND variation_id !='NULL' ")->queryAll();
                    $data .= '<Options>';
                    foreach ($variation as $variations):
                        // For options   
                        $data .= '<Option>';
                        $data .= '<Name>' . $variations['item_name'] . '</Name>';
                        $data .= '<Value>' . $variations['item_value'] . '</Value>';
                        $data .= '</Option>';
                    endforeach;

                    $data .= '</Options>';
                else :
                    $data .= '<Options>';
                    $data .= '<Option>';
                    $data .= '<Name>custom</Name>';
                    $data .= '<Value>custom</Value>';
                    $data .= '</Option>';
                    $data .= '</Options>';
                endif;

                $data .= '</Item>';
            endforeach;
            $data .= '</Items>';
            $data .= '</Order>';
        endforeach;
        $data .= '</Orders>';

        echo $data;


        if ($_REQUEST) :

            $config = yii\helpers\ArrayHelper::merge(
                            require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $uname . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
            );

            $application = new yii\web\Application($config);

            // $shipstation = $db_merchant->createCommand("SELECT * from warehouse WHERE elliot_user_id='" . $user_id . "'")->queryAll();
            $fullfillment_list = FulfillmentList::find()->Where(['fulfillment_name' => 'ShipStation'])->one();
            $fullfillment_list_id = $fullfillment_list->id;

            $shipstation = Fulfillment::find()->Where(['elliot_user_id' => $user_id])->one();
            if (empty($shipstation)) {
                $date = date('Y-m-d h:i:s', time());
                $model = new Fulfillment();
                $model->elliot_user_id = $user_id;
                $model->name = 'Shipstation';
                $model->fulfillment_id = $fullfillment_list_id;
                $model->connected = 'Yes';
                $model->created_at = $date;
                if ($model->save(false)):
                endif;
                //  $insert_ship_data = $db_merchant->createCommand("INSERT INTO warehouse( elliot_user_id, connected ,created_at ) VALUES (" . $user_id . " , Yes ," . $date . ")")->queryAll();
            }else {
                $data = Fulfillment::find()->Where(['elliot_user_id' => $user_id])->one();
                $data->connected = 'Yes';
                $data->fulfillment_id = $fullfillment_list_id;
                $data->save(false);
                //  $update_ship_data = $db_merchant->createCommand("UPDATE warehouse SET connected = 'Yes' WHERE elliot_user_id = " . $user_id . " ")->queryAll(); 
            }
        endif;


        die;
    }

    /**
     * Deletes an existing Fulfillment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Fulfillment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Fulfillment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Fulfillment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionTablerate() {


        return $this->render('tablerate');
    }

    public function actionCarriers() {

        return $this->render('carriers');
    }

    public function actionSoftware() {

        $this->checkAndMigration();
        return $this->render('software');
    }

    private function checkAndMigration() {
        // Insert 3PL Central to fulfillment_list table
        //$_3pl = FulfillmentList::findOne(['fulfillment_name' => '3PL Central']);

        //if($_3pl === NULL) {
            $_3pl = new FulfillmentList();
            $_3pl->fulfillment_name = '3PL Central';
            $_3pl->image_link = '3PL Central';
            //$_3pl->type = 'Software';
            $_3pl->save();
        //}
    }

}
