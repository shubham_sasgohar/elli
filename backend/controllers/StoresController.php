<?php

namespace backend\controllers;

use Yii;
use backend\models\Stores;
use backend\models\StoresSearch;
use backend\models\StoresConnection;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Bigcommerce\Api\Connection;
use Bigcommerce\Api\Client as Bigcommerce;
use backend\models\Categories;
use backend\models\Variations;
use backend\models\VariationsItemList;
use backend\models\Products;
use backend\models\OrdersProducts;
use backend\models\OrderChannel;
use backend\models\CustomerUser;
use backend\models\CustomerAbbrivation;
use backend\models\Notification;
use backend\models\ProductCategories;
use backend\models\ProductVariation;
use backend\models\ProductImages;
use backend\models\ProductChannel;
use backend\models\ProductAbbrivation;
use backend\models\Orders;
use backend\models\CurrencySymbols;
use backend\models\MerchantProducts;
use yii\filters\AccessControl;
use common\models\CustomFunction;
use backend\models\ChannelConnection;
use backend\models\Channels;
use backend\models\CronTasks;
use backend\models\User;
use Bigcommerce\Api\ShopifyClient as Shopify;
use SoapClient;
use SoapFault;
use Automattic\WooCommerce\Client as Woocommerce;
use Automattic\WooCommerce\HttpClient\HttpClientException;
use backend\models\CategoryAbbrivation;
use backend\models\StoreDetails;
use backend\models\Countries;
use backend\models\Channelsetting;
use backend\models\CurrencyConversion;

/**
 * StoresController implements the CRUD actions for Stores model.
 */
class StoresController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
	return [
	    'access' => [
		'class' => AccessControl::className(),
		'only' => ['index', 'view', 'create', 'update', 'connect-bigcommerce', 'auth-bigcommerce', 'bigcommerce', 'shopify', 'auth-shopify', 'shopify-import', 'magento', 'auth-magento', 'magento-importing', 'woocommerce', 'auth-woocommerce', 'woocommerce-importing', 'magento-soap-configuration', 'test'],
		'rules' => [
			[
			'actions' => ['signup', 'connect-bigcommerce', 'magento-importing', 'auth-woocommerce', 'woocommerce-importing', 'shopify-import', 'test'],
			'allow' => true,
			'roles' => ['?'],
		    ],
			[
			'actions' => ['index', 'view', 'create', 'update', 'connect-bigcommerce', 'auth-bigcommerce', 'bigcommerce', 'shopify', 'auth-shopify', 'magento', 'auth-magento', 'magento-importing', 'woocommerce', 'auth-woocommerce', 'woocommerce-importing', 'magento-soap-configuration', 'shopify-import', 'test'],
			'allow' => true,
			'roles' => ['@'],
		    ],
		],
	    ],
	    'verbs' => [
		'class' => VerbFilter::className(),
		'actions' => [
		    'delete' => ['POST'],
		],
	    ],
	];
    }

    /**
     * Lists all Stores models.
     * @return mixed
     */
    public function actionIndex() {
	$eStores = Stores::find()->all();
	$user_id = Yii::$app->user->identity->id;
	$storeNchannels['store'] = StoresConnection::find()->where(['user_id' => $user_id])->all();
	$storeNchannels['channel'] = ChannelConnection::find()->where(['user_id' => $user_id])->all();


	$searchModel = new StoresSearch();
	$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
	if (isset($_REQUEST['notify'])):
	    $notify = $_REQUEST['notify'];
	else:
	    $notify = 'false';
	endif;
	return $this->render('index', [
		    'searchModel' => $searchModel,
		    'dataProvider' => $dataProvider,
		    'eStores' => $eStores,
		    'store_connection' => $storeNchannels,
		    'notify' => $notify,
	]);
    }

    /**
     * Displays a single Stores model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
	return $this->render('view', [
		    'model' => $this->findModel($id),
	]);
    }

    /**
     * Creates a new Stores model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
	$model = new Stores();

	if ($model->load(Yii::$app->request->post()) && $model->save()) {
	    return $this->redirect(['view', 'id' => $model->store_id]);
	} else {
	    return $this->render('create', [
			'model' => $model,
	    ]);
	}
    }

    /**
     * Updates an existing Stores model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
	$model = $this->findModel($id);

	if ($model->load(Yii::$app->request->post()) && $model->save()) {
	    return $this->redirect(['view', 'id' => $model->store_id]);
	} else {
	    return $this->render('update', [
			'model' => $model,
	    ]);
	}
    }

    /**
     * Deletes an existing Stores model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
	$this->findModel($id)->delete();

	return $this->redirect(['index']);
    }

    /**
     * Finds the Stores model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Stores the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
	if (($model = Stores::findOne($id)) !== null) {
	    return $model;
	} else {
	    throw new NotFoundHttpException('The requested page does not exist.');
	}
    }

    /**
     * Bigcommerce Connection Wizard.
     * @return mixed
     */
    public function actionBigcommerce() {
	$eStores = Stores::find()->all();
	$searchModel = new StoresSearch();
	$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

	return $this->render('bigcommerce', [
		    'searchModel' => $searchModel,
		    'dataProvider' => $dataProvider,
		    'eStores' => $eStores,
	]);
    }

    /**
     * For WOO-COMMERCE Store
     * @return store_hash
     */
    public function actionWoocommerce() {
	return $this->render('woocommerce');
    }

    /**
     * For Shopify Store
     * @return store_hash
     */
    public function actionShopify() {
	return $this->render('shopify');
    }

    /**
     * For Magento Soap Configuration link
     * @return store_hash
     */
    public function actionMagentoSoapConfiguration() {
	return $this->render('magentosoapconfiguration');
    }

    /**
     * Authorize the Bigcommerce API Credentials
     * @return store_hash
     */
    public function actionAuthBigcommerce() {
	$array_msg = array();
	$big_api_path = $_POST['api_path'];
	$big_access_token = $_POST['access_token'];
	$big_client_id = $_POST['client_id'];
	$big_client_secret = $_POST['client_secret'];
	$parsed_api_path = parse_url($big_api_path);
	if (array_key_exists('path', $parsed_api_path)) {
	    $store_path = $parsed_api_path['path'];
	    $store_path_array = explode('/', $store_path);
	    if (!array_key_exists(2, $store_path_array)) {
		$array_msg['api_error'] = 'Your BigCommerce API Path is not valid. Please check and try again.';
		return json_encode($array_msg);
	    } else {
		$merchant_store_hash = $store_path_array[2];
	    }
	} else {
	    $array_msg['api_error'] = 'Your BigCommerce API Path is not valid. Please check and try again.';
	    return json_encode($array_msg);
	}


	Bigcommerce::configure(array(
	    'client_id' => $big_client_id,
	    'auth_token' => $big_access_token,
	    'store_hash' => $merchant_store_hash
	));
	$ping = Bigcommerce::getTime();
	if ($ping == '') {
	    $array_msg['api_error'] = 'Your API credentials are not working. Please check and try again.';
	    return json_encode($array_msg);
	}
	$check_api_key = StoresConnection::find()->where(['big_api_path' => $big_api_path, 'big_access_token' => $big_access_token, 'big_client_id' => $big_client_id, 'big_client_secret' => $big_client_secret, 'big_store_hash' => $merchant_store_hash])->one();
	if (!empty($check_api_key)) {
	    $array_msg['api_error'] = 'This BigCommerce store is already integrated with Elliot. Please use another API details to integrate with Elliot.';
	    return json_encode($array_msg);
	}
	if ($ping) {
	    $Bgc_store_details = Bigcommerce::getStore();
	    $country_name = $Bgc_store_details->country;
	    $check_country = StoreDetails::find()->where(['country' => $country_name, 'channel_accquired' => 'BigCommerce'])->one();
	    if (!empty($check_country)) {
		$array_msg['api_error'] = 'This BigCommerce store with same country <b>"' . $country_name . '"</b> is already integrated with Eliiot.';
		return json_encode($array_msg);
	    } else {
		$array_msg['store_hash'] = $merchant_store_hash;
		return json_encode($array_msg);
	    }
	}
    }

    /**
     * Authorize the shopify API Credentials
     */
    public function actionAuthShopify() {
	//echo "<pre>"; print_r($_POST); die('I am herer!');
	$array_msg = array();
	$shopify_shop = $_POST['shopify_shop'];
	$shopify_api = $_POST['shopify_api'];
	$shopify_pass = $_POST['shopify_pass'];
	$shopify_access_token = $_POST['shopify_shared_secret'];
	$shopify_plus = $_POST['shopify_plus'];
	$user_id = $_POST['user_id'];
	$store_id = '';
	$store_name = '';
	if ($shopify_plus == "true") {
	    $store = Stores::find()->select('store_id')->where(['store_name' => 'ShopifyPlus'])->one();
	    $store_id = $store->store_id;
	    $store_name = 'ShopifyPlus';
	} else {
	    $store = Stores::find()->select('store_id')->where(['store_name' => 'Shopify'])->one();
	    $store_id = $store->store_id;
	    $store_name = 'Shopify';
	}

	$check_api_key = StoresConnection::find()->where(['url' => $shopify_shop, 'api_key' => $shopify_api, 'key_password' => $shopify_pass, 'shared_secret' => $shopify_access_token])->one();
	if (!empty($check_api_key)) {
	    $array_msg['api_error'] = 'This Shopify shop is already integrated with Elliot. Please use another API details to integrate with Elliot.';
	    return json_encode($array_msg);
	}

	$url = 'https://' . $shopify_api . ':' . $shopify_pass . '@' . $shopify_shop . '/admin/customers.json';
	//$jsondata = file_get_contents($url);
	$curl = curl_init();
	curl_setopt_array($curl, array(
	    CURLOPT_URL => $url,
	    CURLOPT_RETURNTRANSFER => true,
	    CURLOPT_ENCODING => "",
	    CURLOPT_MAXREDIRS => 10,
	    CURLOPT_TIMEOUT => FALSE,
	    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	    CURLOPT_CUSTOMREQUEST => "GET",
	    CURLOPT_SSL_VERIFYPEER => false,
	    CURLOPT_HTTPHEADER => array(
		"cache-control: no-cache",
		"postman-token: 8d6a1ad5-395f-cb02-e58d-223387324331"
	    ),
	));

	$response = curl_exec($curl);
	$response_data = json_decode($response);
	if ($response_data != '') {
	    if (array_key_exists('errors', $response_data)) {
		$msg = $response_data->errors;
		$array_msg['api_error'] = 'Your API credentials are not working. Because of following reason ' . $msg . '. Please check and try again.';
		return json_encode($array_msg);
	    } else {
		$sc = new Shopify($shopify_shop, $shopify_pass, $shopify_api, $shopify_access_token);
		$store_details = $sc->call('GET', '/admin/shop.json');
		$call_left = $sc->callsLeft();
		if ($call_left <= 8) {
		    sleep(20);
		}
		$store_country = $store_details['country_name'];
		$check_country = StoreDetails::find()->where(['country' => $store_country, 'channel_accquired' => $store_name])->one();
		if (!empty($check_country)) {
		    $array_msg['api_error'] = 'This Shopify shop with same country <b>' . $store_country . '</b> is already integrated with Eliiot.';
		    return json_encode($array_msg);
		}
	    }
	}

	$validate_url = array();
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	    //echo "cURL Error #:" . $err;
	    $array_msg['api_error'] = 'Your API credentials are not working. Please check and try again.';
	    return json_encode($array_msg);
	} else {
	    $user = User::find()->where(['id' => $user_id])->one();
	    $user_domain = $user->domain_name;
	    $config = yii\helpers\ArrayHelper::merge(
			    require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $user_domain . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
	    );
	    $application = new yii\web\Application($config);

	    //$checkConnection = StoresConnection::find()->where(['store_id' => $store_id, 'user_id' => $user_id])->one();
	    //if (empty($checkConnection)){
	    $connectionModel = new StoresConnection();
	    $connectionModel->store_id = $store_id;
	    $connectionModel->user_id = $user_id;
	    $connectionModel->connected = 'Yes';
	    $connectionModel->url = $shopify_shop;
	    $connectionModel->api_key = $shopify_api;
	    $connectionModel->key_password = $shopify_pass;
	    $connectionModel->shared_secret = $shopify_access_token;
	    $created_date = date('Y-m-d h:i:s', time());
	    $connectionModel->created = $created_date;
	    if ($connectionModel->save(false)) {
		//Yii::$app->session->setFlash('success', 'Success! Your shop has been connected successfully. Syncing will start soon.');
		$array_msg['success'] = "Your Shopify store has been connected successfully. Importing has started and you will be notified once it is completed.";
		$array_msg['store_connection_id'] = $connectionModel->stores_connection_id;
		$array_msg['store_name'] = $store_name;
		$array_msg['domain_name'] = $user_domain;
	    } else {
		$array_msg['error'] = "Error Something went wrong. Please try again";
	    }
	    //}
	}
	return json_encode($array_msg);
    }

    /**
     * Connect the Bigcommerce Store
     * @return type
     */
    public function actionConnectBigcommerce() {
	$array_msg = array();
	header('content-type: application/json; charset=utf-8');
	header("Access-Control-Allow-Origin: *");
	$uid = $_GET['uid'];
	$big_api_path = $_GET['api_path'];
	$big_access_token = $_GET['access_token'];
	$big_client_id = $_GET['client_id'];
	$big_client_secret = $_GET['client_secret'];
	$merchant_store_hash = $_GET['store_hash'];

////         
//        $uid = Yii::$app->user->identity->id;
//        $big_api_path = 'https://api.bigcommerce.com/stores/1iz6ni/v3/';
//        $big_access_token = 'plff9lyma3id54vhbcnmc5y8ecwb9v9123dffgder';
//        $big_client_id = 'ry1ws90px3dr2xkb4pyet74zs3phe';
//        $big_client_secret = '852lbjjkjrlfjbpzlw5l9b38bfkwwzq';
//        $merchant_store_hash = '1iz6ni';
	//Configure Bigcommerce object 
	Bigcommerce::configure(array(
	    'client_id' => $big_client_id,
	    'auth_token' => $big_access_token,
	    'store_hash' => $merchant_store_hash
	));



	//Test Connection
	$ping = Bigcommerce::getTime();
	if ($ping == '') {
	    $array_msg['api_error'] = 'Your API credentials are not working. Please check and try again.';
	    return json_encode($array_msg);
	}

	if ($ping) {

	    $user = User::find()->where(['id' => $uid])->one();
	    $user_domain = $user->domain_name;
	    $config = yii\helpers\ArrayHelper::merge(
			    require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $user_domain . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
	    );
	    $application = new yii\web\Application($config);

	    $status = 'true';
	    $store = Stores::find()->select('store_id')->where(['store_name' => 'BigCommerce'])->one();
	    $store_id = $store->store_id;
	    //$uid = Yii::$app->user->identity->id;
	    //$checkConnection = StoresConnection::find()->where(['store_id' => $store_id, 'user_id' => $uid])->one();
	    //if (empty($checkConnection)) {
	    $connectionModel = new StoresConnection();
	    $connectionModel->store_id = $store_id;
	    $connectionModel->user_id = $uid;
	    $connectionModel->connected = 'Yes';
	    $connectionModel->big_api_path = $big_api_path;
	    $connectionModel->big_access_token = $big_access_token;
	    $connectionModel->big_client_id = $big_client_id;
	    $connectionModel->big_client_secret = $big_client_secret;
	    $connectionModel->big_store_hash = $merchant_store_hash;
	    $created_date = date('Y-m-d h:i:s', time());
	    $connectionModel->created = $created_date;
	    if ($connectionModel->save()) {
		$store_connection_id = $connectionModel->stores_connection_id;
		//Import the BigCommerce Store Data          
		$get_notify = Stores::bigcommerce_initial_import($user, $store_connection_id);
		//If Import done successfully
		if ($get_notify == 'true') {
		    //Send Notification Email 
		    $email = $user->email;
		    $company_name = $user->company_name;
		    $email_message = 'Success, Your BigCommerce Store is Now Connected';
		    $send_email_notif = CustomFunction::ConnectBigCommerceEmail($email, $company_name, $email_message);
		    //Drop-Down Notification for User
		    $notif_type = 'BigCommerce';
		    $notif_db = Notification::find()->Where(['notif_type' => $notif_type])->one();
		    if (empty($notif_db)) {
			$notification_model = new Notification();
			$notification_model->user_id = $user->id;
			$notification_model->notif_type = $notif_type;
			$notification_model->notif_description = 'Your BigCommerce store data has been successfully imported.';
			$notification_model->created_at = date('Y-m-d h:i:s', time());
			$notification_model->save(false);
		    }
		    //Sticky Notification :StoresConnection Model Entry for Import Status
		    $get_rec = StoresConnection::find()->Where(['user_id' => $user->id, 'stores_connection_id' => $store_connection_id])->one();
		    $get_rec->import_status = 'Completed';
		    $get_rec->save(false);
		    Yii::$app->session->setFlash('success', 'Success! Your BigCommerce store data has been successfully imported.');
		    echo $user_domain;
		    die;
		}
	    } else {
		Yii::$app->session->setFlash('danger', 'Error! Please check your API Credentials.');
		return $this->redirect(['bigcommerce']);
	    }
	    //}
	} else {
	    $status = "false";
	    Yii::$app->session->setFlash('danger', 'Error! Please check your API Credentials.');
	    return $this->redirect(['bigcommerce']);
	}
    }

    /**
     * Import from the Bigcommerce Store
     * @return type
     */
    public function actionBigcommerceImport() {
	echo 'test';
    }

    //Disable channels 
    public function actionChanneldisable() {
	$dt = Yii::$app->request->post();
	$_SESSION['access_token'] = '';
	if (isset($dt['ch_id'])) {
	    if ($dt['ch_type'] == "store") {
		$store_big = StoresConnection::find()->where(['stores_connection_id' => $dt['ch_id']])->one();
		$user_id = $store_big->user_id;
		$store_id = $store_big->store_id;
		$get_shopify_id = Stores::find()->select('store_id')->where(['store_name' => 'Shopify'])->one();
		$shopify_store_id = $get_shopify_id->store_id;
		if ($store_id == $shopify_store_id) {
		    $shopify_shop = $store_big->url;
		    $shopify_api_key = $store_big->api_key;
		    $shopify_api_password = $store_big->key_password;
		    $shopify_shared_secret = $store_big->shared_secret;
		    $user_id = $store_big->user_id;
		    $sc = new Shopify($shopify_shop, $shopify_api_password, $shopify_api_key, $shopify_shared_secret);
		    $hooks = $sc->call('GET', '/admin/webhooks.json');
		    foreach ($hooks as $_hook) {
			if (in_array(Yii::$app->params['BASE_URL'] . "people/shopify-product-create?id=" . $user_id, $_hook)) {
			    $sc->call('DELETE', '/admin/webhooks/' . $_hook['id'] . '.json');
			}
			if (in_array(Yii::$app->params['BASE_URL'] . "people/shopify-product-delete?id=" . $user_id, $_hook)) {
			    $sc->call('DELETE', '/admin/webhooks/' . $_hook['id'] . '.json');
			}
			if (in_array(Yii::$app->params['BASE_URL'] . "people/shopify-product-update?id=" . $user_id, $_hook)) {
			    $sc->call('DELETE', '/admin/webhooks/' . $_hook['id'] . '.json');
			}
			if (in_array(Yii::$app->params['BASE_URL'] . "people/shopify-order-create?id=" . $user_id, $_hook)) {
			    $sc->call('DELETE', '/admin/webhooks/' . $_hook['id'] . '.json');
			}
			if (in_array(Yii::$app->params['BASE_URL'] . "people/shopify-order-paid?id=" . $user_id, $_hook)) {
			    $sc->call('DELETE', '/admin/webhooks/' . $_hook['id'] . '.json');
			}
			if (in_array(Yii::$app->params['BASE_URL'] . "people/shopify-order-update?id=" . $user_id, $_hook)) {
			    $sc->call('DELETE', '/admin/webhooks/' . $_hook['id'] . '.json');
			}
			if (in_array(Yii::$app->params['BASE_URL'] . "people/shopify-customer-create?id=" . $user_id, $_hook)) {
			    $sc->call('DELETE', '/admin/webhooks/' . $_hook['id'] . '.json');
			}
			if (in_array(Yii::$app->params['BASE_URL'] . "people/shopify-customer-update?id=" . $user_id, $_hook)) {
			    $sc->call('DELETE', '/admin/webhooks/' . $_hook['id'] . '.json');
			}
		    }
		}
		$store_big->delete();
		//Update magnto shop url when store is disable by user
		$get_magento_id = Stores::find()->select('store_id')->where(['store_name' => 'Magento'])->one();
		$magento_store_id = $get_magento_id->store_id;
		if ($store_id == $magento_store_id) {
		    $config = yii\helpers\ArrayHelper::merge(
				    require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php'));
		    $application = new yii\web\Application($config);
		    $main_data = User::find()->where(['id' => $user_id])->one();
		    $main_data->magento_shop_url = '';
		    $main_data->save(false);
		}
		return;
	    } elseif ($dt['ch_type'] == "channel") {
		$store_big = ChannelConnection::find()->where(['channel_connection_id' => $dt['ch_id']])->one();
		$store_big->delete();
		return;
	    }
	}
	$all_conntection = StoresConnection::find()->all();
	$all_channels = ChannelConnection::find()->all();

	$cnt = 0;
	$arr1 = array();
	foreach ($all_conntection as $conn) {
	    $arr1[$cnt]['connection_id'] = $conn->stores_connection_id;
	    $store = Stores::find()->select(['store_name', 'store_image'])->where(['store_id' => $conn->store_id])->one();

	    $arr1[$cnt]['store_name'] = $store->store_name;
	    $arr1[$cnt]['store_image'] = $store->store_image;
	}

	$cnt1 = 0;
	$arr2 = array();
	foreach ($all_channels as $channel) {
	    $arr2[$cnt]['connection_id'] = $channel->channel_connection_id;
	    $chnl = Channels::find()->select(['channel_name', 'channel_image', 'parent_name'])->where(['channel_id' => $channel->channel_id])->one();

	    $arr2[$cnt]['channel_name'] = $chnl->channel_name;
	    $arr2[$cnt]['channel_image'] = $chnl->channel_image;
	    $arr2[$cnt]['parent_name'] = $chnl->parent_name;
	}

	$arr = array("store" => $arr1, "channel" => $arr2);
	return $this->render('channeldisable', [
		    'connections' => $arr,
	]);
    }

    /*
     * Shopify add hooks
     */

    public function addShopifyHooks($user_id, $store_connection_id, $sc, $store_name) {
	$product_create_hook = array
	    (
	    "webhook" => array
		(
		"topic" => "products/create",
		"address" => Yii::$app->params['BASE_URL'] . "people/shopify-product-create?id=" . $user_id . "&connection_id=" . $store_connection_id . "&store_name=" . $store_name,
		"format" => "json"
	    )
	);

	$product_delete_hook = array
	    (
	    "webhook" => array
		(
		"topic" => "products/delete",
		"address" => Yii::$app->params['BASE_URL'] . "people/shopify-product-delete?id=" . $user_id . "&connection_id=" . $store_connection_id . "&store_name=" . $store_name,
		"format" => "json"
	    )
	);

	$product_update_hook = array
	    (
	    "webhook" => array
		(
		"topic" => "products/update",
		"address" => Yii::$app->params['BASE_URL'] . "people/shopify-product-update?id=" . $user_id . "&connection_id=" . $store_connection_id . "&store_name=" . $store_name,
		"format" => "json"
	    )
	);

	$order_create_hook = array
	    (
	    "webhook" => array
		(
		"topic" => "orders/create",
		"address" => Yii::$app->params['BASE_URL'] . "people/shopify-order-create?id=" . $user_id . "&connection_id=" . $store_connection_id . "&store_name=" . $store_name,
		"format" => "json"
	    )
	);

	$order_update_hook = array
	    (
	    "webhook" => array
		(
		"topic" => "orders/updated",
		"address" => Yii::$app->params['BASE_URL'] . "people/shopify-order-update?id=" . $user_id . "&connection_id=" . $store_connection_id . "&store_name=" . $store_name,
		"format" => "json"
	    )
	);

	$hooks_tag = $sc->call('GET', '/admin/webhooks.json');
	$call_left = $sc->callsLeft();
	if ($call_left <= 8) {
	    sleep(20);
	}
	$myhooks = array();
	foreach ($hooks_tag as $exist_hooks) {
	    $myhooks[] = $exist_hooks['address'];
	}
	if (!in_array(Yii::$app->params['BASE_URL'] . "people/shopify-product-create?id=" . $user_id . "&connection_id=" . $store_connection_id . "&store_name=" . $store_name, $myhooks)) {
	    $hooks_add = $sc->call('POST', '/admin/webhooks.json', $product_create_hook);
	}
	if (!in_array(Yii::$app->params['BASE_URL'] . "people/shopify-product-delete?id=" . $user_id . "&connection_id=" . $store_connection_id . "&store_name=" . $store_name, $myhooks)) {
	    $hooks_add = $sc->call('POST', '/admin/webhooks.json', $product_delete_hook);
	}
	if (!in_array(Yii::$app->params['BASE_URL'] . "people/shopify-product-update?id=" . $user_id . "&connection_id=" . $store_connection_id . "&store_name=" . $store_name, $myhooks)) {
	    $hooks_add = $sc->call('POST', '/admin/webhooks.json', $product_update_hook);
	}
	if (!in_array(Yii::$app->params['BASE_URL'] . "people/shopify-order-create?id=" . $user_id . "&connection_id=" . $store_connection_id . "&store_name=" . $store_name, $myhooks)) {
	    $hooks_add = $sc->call('POST', '/admin/webhooks.json', $order_create_hook);
	}
	if (!in_array(Yii::$app->params['BASE_URL'] . "people/shopify-order-update?id=" . $user_id . "&connection_id=" . $store_connection_id . "&store_name=" . $store_name, $myhooks)) {
	    $hooks_add = $sc->call('POST', '/admin/webhooks.json', $order_update_hook);
	}
    }

    public function actionShopifyImport() {
	$user_id = $_GET['user_id'];
	$store_connection_id = $_GET['store_connection_id'];
	$store_name = $_GET['store_name'];
	$domain_name = $_GET['domain_name'];
	//echo "<pre>"; print_r($_GET); die('End here');
	ini_set("memory_limit", "-1");
	set_time_limit(0);
	//$get_users = User::find()->select('domain_name,company_name,email')->where(['id' => $user_id])->one();

	$config = yii\helpers\ArrayHelper::merge(
			require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
	);
	$application = new yii\web\Application($config);
	$get_users = User::find()->select('domain_name,company_name,email')->where(['id' => $user_id])->one();
	$email = $get_users->email;
	$company_name = $get_users->company_name;

	$get_shopify_id = Stores::find()->where(['store_name' => $store_name])->one();
	$shopify_store_id = $get_shopify_id->store_id;
	$store_shopify = StoresConnection::find()->where(['store_id' => $shopify_store_id, 'user_id' => $user_id, 'stores_connection_id' => $store_connection_id])->one();
	if (!empty($store_shopify)) {
	    $shopify_shop = $store_shopify->url;
	    $shopify_api_key = $store_shopify->api_key;
	    $shopify_api_password = $store_shopify->key_password;
	    $shopify_shared_secret = $store_shopify->shared_secret;

	    $sc = new Shopify($shopify_shop, $shopify_api_password, $shopify_api_key, $shopify_shared_secret);
	    $store_details = $sc->call('GET', '/admin/shop.json');
	    $call_left = $sc->callsLeft();
	    if ($call_left <= 8) {
		sleep(20);
	    }
	    $store_country_code = $store_details['country_code'];
	    $store_currency_code = $store_details['currency'];

	    if ($store_country_code != '') {

		$conversion_rate = Stores::getCurrencyConversionRate($store_currency_code, 'USD');
		$currency_check = CurrencyConversion::find()->Where(['to_currency' => $store_currency_code])->one();
		if (empty($currency_check)) {
		    $store_currency_conversion = new CurrencyConversion();
		    $store_currency_conversion->from_currency = 'USD';
		    $store_currency_conversion->from_value = 1;
		    $store_currency_conversion->to_currency = $store_currency_code;
		    $store_currency_conversion->to_value = $conversion_rate;
		    $store_currency_conversion->created_at = date('Y-m-d H:i:s', time());
		    $store_currency_conversion->save(false);
		} else {
		    $currency_check->from_currency = 'USD';
		    $currency_check->from_value = 1;
		    $currency_check->to_currency = $store_currency_code;
		    $currency_check->to_value = $conversion_rate;
		    $currency_check->save(false);
		}
	    }

	    $this->shopifyStoreImport($user_id, $sc, $store_connection_id, $store_name);
	    $this->addShopifyHooks($user_id, $store_connection_id, $sc, $store_name);
	    $this->shopifyProductImport($user_id, $sc, $shopify_shop, $store_connection_id, $store_country_code, $store_currency_code, $store_name, $conversion_rate);
	    $this->shopifyCategoryImport($user_id, $sc, $store_connection_id, $store_name);
	    $this->shopifyAssignProductToCollection($user_id, $sc);
	    $this->shopifyCustomerImport($user_id, $sc, $store_connection_id, $store_name);
	    $this->shopifyOrderImport($user_id, $sc, $store_connection_id, $store_currency_code, $store_name, $conversion_rate);
	    $this->shopifyCurrencydetails($user_id, $store_connection_id, $store_currency_code, $store_name, $shopify_store_id);



	    /* Send Email Magento */
	    $email_message = 'Success, Your Shopify Store is Now Connected';
	    $send_email_notif = CustomFunction::ConnectBigCommerceEmail($email, $company_name, $email_message);

	    //Drop-Down Notification for User
	    $notif_type = $store_name;
	    $notif_db = Notification::find()->Where(['notif_type' => $notif_type])->one();
	    if (empty($notif_db)):
		$notification_model = new Notification();
		$notification_model->user_id = $user_id;
		$notification_model->notif_type = $notif_type;
		$notification_model->notif_description = 'Your ' . $store_name . ' store data has been successfully imported.';
		$notification_model->created_at = date('Y-m-d h:i:s', time());
		$notification_model->save(false);
	    endif;
	    //Sticky Notification :StoresConnection Model Entry for Import Status
	    $get_rec = StoresConnection::find()->Where(['user_id' => $user_id, 'store_id' => $shopify_store_id, 'stores_connection_id' => $store_connection_id])->one();
	    if (!empty($get_rec)) {
		$get_rec->import_status = 'Completed';
		$get_rec->save(false);
	    }
	    return;
	}
    }

    public function shopifyCurrencydetails($user_id, $store_connection_id, $store_currency_code, $store_name, $shopify_store_id) {
	$currency_channel_exists = Channelsetting::find()->Where(['mul_store_id' => $store_connection_id, 'setting_key' => 'currency', 'user_id' => $user_id])->one();
	if (empty($currency_channel_exists)) {
	    $channel_setting = new Channelsetting();
	    $channel_setting->store_id = $shopify_store_id;
	    $channel_setting->user_id = $user_id;
	    $channel_setting->channel_name = $store_name;
	    $channel_setting->setting_key = 'currency';
	    $channel_setting->setting_value = $store_currency_code;
	    $channel_setting->mul_store_id = $store_connection_id;
	    $channel_setting->created_at = date('Y-m-d H:i:s');
	    $channel_setting->save(false);
	}
    }

    /**
     * Shopify Customer Import
     */
    public function shopifyCustomerImport($user_id, $sc, $store_connection_id, $store_name) {
	try {
	    $total_customer_count = $sc->call('GET', '/admin/customers/count.json');
	    $call_left = $sc->callsLeft();
	    if ($call_left <= 8) {
		sleep(20);
	    }
	    $count = ceil($total_customer_count / 250);
	    for ($i = 1; $i <= $count; $i++) {
		$customer_result = $sc->call('GET', '/admin/customers.json?page=' . $i . '&limit=250');
		$call_left = $sc->callsLeft();
		if ($call_left <= 8) {
		    sleep(20);
		}

		$curr_time = date('Y-m-d h:i:s');
		$myfile = fopen("bigcommerce_callback.php", "a") or die("Unable to open file!");
		$txt = "Customer importing call left => " . $call_left . " Time is " . $curr_time;
		fwrite($myfile, "\r\n" . $txt);
		fclose($myfile);

		foreach ($customer_result as $_customer) {
		    $customer_id = $_customer['id'];
		    $Prefix_customer_abb_id = 'SP' . $customer_id;
		    $customer_email = isset($_customer['email']) ? $_customer['email'] : '';
		    $customer_f_name = isset($_customer['first_name']) ? $_customer['first_name'] : '';
		    $customer_l_name = isset($_customer['last_name']) ? $_customer['last_name'] : '';
		    $customer_create_date = isset($_customer['created_at']) ? $_customer['created_at'] : time();
		    $customer_update_date = isset($_customer['updated_at']) ? $_customer['updated_at'] : time();
		    $cus_phone = isset($_customer['phone']) ? $_customer['phone'] : '';

		    $add_1 = $add_2 = $city = $state = $country = $zip = $add_phone = $country_code = '';
		    if (array_key_exists('default_address', $_customer)):
			$add_1 = isset($_customer['default_address']['address1']) ? $_customer['default_address']['address1'] : '';
			$add_2 = isset($_customer['default_address']['address2']) ? $_customer['default_address']['address2'] : '';
			$city = isset($_customer['default_address']['city']) ? $_customer['default_address']['city'] : '';
			$state = isset($_customer['default_address']['province']) ? $_customer['default_address']['province'] : '';
			$country = isset($_customer['default_address']['country']) ? $_customer['default_address']['country'] : '';
			$country_code = isset($_customer['default_address']['country_code']) ? $_customer['default_address']['country_code'] : '';
			$zip = isset($_customer['default_address']['zip']) ? $_customer['default_address']['zip'] : '';
			$add_phone = isset($_customer['default_address']['phone']) ? $_customer['default_address']['phone'] : '';
		    endif;

		    $phone = ($cus_phone == '') ? $add_phone : $cus_phone;

		    $customer_data = array(
			'customer_id' => $customer_id,
			'elliot_user_id' => $user_id,
			'first_name' => $customer_f_name,
			'last_name' => $customer_l_name,
			'email' => $customer_email,
			'channel_store_name' => $store_name,
			'channel_store_prefix' => 'SP',
			'mul_store_id' => $store_connection_id,
			'mul_channel_id' => '',
			'customer_created_at' => $customer_create_date,
			'customer_updated_at' => $customer_update_date,
			'billing_address' => array(
			    'street_1' => $add_1,
			    'street_2' => $add_2,
			    'city' => $city,
			    'state' => $state,
			    'country' => $country,
			    'country_iso' => $country_code,
			    'zip' => $zip,
			    'phone_number' => $phone,
			    'address_type' => 'Default',
			),
			'shipping_address' => array(
			    'street_1' => '',
			    'street_2' => '',
			    'city' => '',
			    'state' => '',
			    'country' => '',
			    'zip' => '',
			),
		    );
		    Stores::customerImportingCommon($customer_data);
		}
	    }
	} catch (Exception $e) {
	    $msg = $e->getMessage();
	    $curr_time = date('Y-m-d h:i:s');
	    $myfile = fopen("bigcommerce_callback.php", "a") or die("Unable to open file!");
	    $txt = "Customer importing log here => " . $msg . " Time is " . $curr_time;
	    fwrite($myfile, "\r\n" . $txt);
	    fclose($myfile);
	    sleep(30);
	}
    }

    public function shopifyCategoryImport($user_id, $sc, $store_connection_id, $store_name) {
	$get_users = User::find()->select('domain_name,company_name,email')->where(['id' => $user_id])->one();
	$config = yii\helpers\ArrayHelper::merge(
			require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
	);
	$application = new yii\web\Application($config);
	$msg = true;
	$total_collection_count = $sc->call('GET', '/admin/custom_collections/count.json');
	$call_left = $sc->callsLeft();
	if ($call_left <= 8) {
	    sleep(20);
	}
	$count = ceil($total_collection_count / 250);
	for ($i = 1; $i <= $count; $i++):
	    $collection_result = $sc->call('GET', '/admin/custom_collections.json?page=' . $i . '&limit=250');
	    $call_left = $sc->callsLeft();
	    if ($call_left <= 8) {
		sleep(20);
	    }
	    foreach ($collection_result as $_collection) {
		$collection_id = $_collection['id'];
		$collection_title = $_collection['title'];
		$collection_created_at = $_collection['published_at'];
		$collection_updated_at = $_collection['updated_at'];

		$category_data = array(
		    'category_id' => $collection_id, // Give category id of Store/channels
		    'parent_id' => 0, // Give Category parent id of Elliot if null then give 0
		    'name' => $collection_title, // Give category name
		    'channel_store_name' => $store_name, // Give Channel/Store name
		    'channel_store_prefix' => 'SP', // Give Channel/Store prefix id
		    'mul_store_id' => $store_connection_id, // Give Channel/Store prefix id
		    'mul_channel_id' => '', // Give Channel/Store prefix id
		    'elliot_user_id' => $user_id, // Give Elliot user id
		    'created_at' => $collection_created_at, // Give Created at date if null then give current date format date('Y-m-d H:i:s')
		    'updated_at' => $collection_updated_at, // Give Updated at date if null then give current date format date('Y-m-d H:i:s')
		);
		Stores::categoryImportingCommon($category_data);    // Then call store modal function and give $category data

		/* $checkModel = Categories::find()->where(['category_name' => $collection_title])->one();
		  if (empty($checkModel)) {
		  $categoryModel = new Categories();
		  $categoryModel->category_name = $collection_title;
		  $categoryModel->channel_abb_id = '';
		  $categoryModel->parent_category_ID = 0;
		  $categoryModel->elliot_user_id = $user_id;
		  $categoryModel->created_at = date('Y-m-d h:i:s', strtotime($collection_created_at));
		  $categoryModel->updated_at = date('Y-m-d h:i:s', strtotime($collection_updated_at));
		  if ($categoryModel->save()) {
		  $CategoryAbbrivation = new CategoryAbbrivation();
		  $CategoryAbbrivation->channel_abb_id = $collection_id;
		  $CategoryAbbrivation->category_ID = $categoryModel->category_ID;
		  $CategoryAbbrivation->channel_accquired = 'Shopify';
		  $CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
		  $CategoryAbbrivation->save(false);
		  }
		  } else {
		  $CategoryAbbrivation = new CategoryAbbrivation();
		  $CategoryAbbrivation->channel_abb_id = $collection_id;
		  $CategoryAbbrivation->category_ID = $checkModel->category_ID;
		  $CategoryAbbrivation->channel_accquired = 'Shopify';
		  $CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
		  $CategoryAbbrivation->save(false);
		  } */
	    }
	endfor;
    }

    public function shopifyAssignProductToCollection($user_id, $sc) {
	$collection_list = $sc->call('GET', '/admin/custom_collections.json?limit=250');
	$call_left = $sc->callsLeft();
	if ($call_left <= 8) {
	    sleep(20);
	}
	foreach ($collection_list as $_collection) {
	    $collection_id = $_collection['id'];
	    $checkCatModel = CategoryAbbrivation::find()->where(['channel_abb_id' => 'SP' . $collection_id])->one();
	    if (!empty($checkCatModel)) {
		$cat_id = $checkCatModel->category_ID;
		$collection_product_list = $sc->call('GET', '/admin/products.json?collection_id=' . $collection_id);
		$call_left = $sc->callsLeft();
		if ($call_left <= 8) {
		    sleep(20);
		}
		foreach ($collection_product_list as $product) {
		    $product_id = $product['id'];
		    $checkProduct = ProductAbbrivation::find()->where(['channel_abb_id' => 'SP' . $product_id])->one();
		    if (!empty($checkProduct)) {
			$pro_id = $checkProduct->product_id;
			$category_model = new ProductCategories();
			$category_model->elliot_user_id = $user_id;
			$category_model->category_ID = $cat_id;
			$category_model->product_ID = $pro_id;
			$category_model->created_at = date('Y-m-d h:i:s', strtotime(time()));
			$category_model->save();
		    }
		}
	    }
	}
    }

    /** Shopify store import * */
    public function shopifyStoreImport($user_id, $sc, $store_connection_id, $store_name) {
	$store_details = $sc->call('GET', '/admin/shop.json');
	$call_left = $sc->callsLeft();
	if ($call_left <= 8) {
	    sleep(20);
	}
	$store_url = $store_details['myshopify_domain'];
	$store_country = $store_details['country_name'];
	$store_country_code = $store_details['country_code'];
	$store_currency = $store_details['currency'];
	$store_owner = $store_details['shop_owner'];
	$store_add = $store_details['address1'];
	$store_city = $store_details['city'];
	$store_province = $store_details['province'];
	$store_zip = $store_details['zip'];
	$store_phone = $store_details['phone'];

	$others_details = array(
	    "contact_name" => $store_owner,
	    "company" => "",
	    "address" => $store_add,
	    "city" => $store_city,
	    "province" => $store_province,
	    "country" => $store_country,
	    "post_code" => $store_zip,
	    "phone" => $store_phone,
	    "currency" => $store_currency
	);

	$others_details_serialized = serialize($others_details);
	$store_details_check = StoreDetails::find()->where(['store_connection_id' => $store_connection_id])->one();
	if (empty($store_details_check)) {
	    $save_store_details = new StoreDetails();
	    $save_store_details->store_connection_id = $store_connection_id;
	    $save_store_details->store_name = $store_owner;
	    $save_store_details->store_url = $store_url;
	    $save_store_details->country = $store_country;
	    $save_store_details->country_code = $store_country_code;
	    $save_store_details->currency = $store_currency;
	    $save_store_details->currency_symbol = $store_currency;
	    $save_store_details->others = $others_details_serialized;
	    $save_store_details->channel_accquired = $store_name;
	    $save_store_details->created_at = date('Y-m-d H:i:s', time());
	    $save_store_details->save(false);
	}
    }

    public function shopifyProductImport($user_id, $sc, $shopify_shop, $store_connection_id, $store_country_code, $store_currency_code, $store_name, $conversion_rate) {
	    $total_product_count = $sc->call('GET', '/admin/products/count.json');
	    $call_left = $sc->callsLeft();
        if ($call_left <= 8) {
            sleep(20);
        }
        $product_count = ceil($total_product_count / 250);
        for ($i = 1; $i <= $product_count; $i++) {
            $product_result = $sc->call('GET', '/admin/products.json?page=' . $i . '&limit=250');
            $call_left = $sc->callsLeft();
            if ($call_left <= 8) {
            sleep(20);
            }
            foreach ($product_result as $_product) {
            $product_id = $_product['id'];
            $title = $_product['title'];
            $description = $_product['body_html'];
            $product_handle = $_product['handle'];
            $product_url = 'https://' . $shopify_shop . '/products/' . $product_handle;
            $created_date = date('Y-m-d h:i:s', strtotime($_product['created_at']));
            $updated_date = date('Y-m-d h:i:s', strtotime($_product['updated_at']));
            /* Fields which are required but not avaialable @Shopify */
            $p_ean = '';
            $p_jan = '';
            $p_isbn = '';
            $p_mpn = '';
            $variants_barcode = '';
            $product_variants = $_product['variants'];
            $count = 1;
            foreach ($product_variants as $_variants) {
                $variants_id = $_variants['id'];
                $variants_price = $_variants['price'];

                $variant_sku = ($_variants['sku'] == '') ? $variant_sku = '' : $_variants['sku'];
                $variants_barcode = $_variants['barcode'];
                if ($variants_barcode == '') {
                    $variants_barcode = '';
                }
                $variants_qty = $_variants['inventory_quantity'];
                if ($variants_qty > 0) {
                    $stock_status = 1;
                } else {
                    $stock_status = 0;
                }
                $variants_weight = $_variants['weight'];
                $variants_created_at = $_variants['created_at'];
                $variants_updated_at = $_variants['updated_at'];
                $variants_title_value = $_variants['title'];

                $product_images = $_product['images'];

                $product_image_data = array();
                foreach ($product_images as $_image) {
                    $image_id = $_image['id'];
                    $image_src = $_image['src'];
                    $image_position = $_image['position'];
                    $image_variant_id_array = $_image['variant_ids'];
                    $default_img_id = $_product['image']['id'];
                    if ($default_img_id == $image_id) {
                        $base_image = $_product['image']['src'];
                    }
                    $product_image_data[] = array(
                        'image_url' => $image_src,
                        'label' => '',
                        'position' => $image_position,
                        'base_img' => $base_image,
                    );
                }
                if ($count == 1) {
                    $store_id = Stores::find()->select('store_id')->where(['store_name' => $store_name])->one();
                    $product_data = array(
                        'product_id' => $product_id, // Stores/Channel product ID
                        'name' => $title, // Product name
                        'sku' => $variant_sku, // Product SKU
                        'description' => $description, // Product Description
                        'product_url_path' => $product_url, // Product url if null give blank value
                        'weight' => $variants_weight, // Product weight if null give blank value
                        'status' => 1, // Product status (visible or hidden). Give in 0 or 1 form (0 = hidden, 1 = visible).
                        'price' => $variants_price, // Porduct price
                        'sale_price' => $variants_price, // Product sale price if null give Product price value
                        'qty' => $variants_qty, //Product quantity
                        'stock_status' => $stock_status, // Product stock status ("in stock" or "out of stock").
                        // Give in 0 or 1 form (0 = out of stock, 1 = in stock)
                        'websites' => array(), //This is for only magento give only and blank array
                        'brand' => '', // Product brand if any
                        'low_stock_notification' => 5, // Porduct low stock notification if any otherwise give default 5 value
                        'created_at' => $created_date, // Product created at date format date('Y-m-d H:i:s')
                        'updated_at' => $updated_date, // Product updated at date format date('Y-m-d H:i:s')
                        'channel_store_name' => $store_name, // Channel or Store name
                        'channel_store_prefix' => 'SP', // Channel or store prefix id
                        'mul_store_id' => $store_connection_id, //Give multiple store id
                        'mul_channel_id' => '', // Give multiple channel id
                        'elliot_user_id' => $user_id, // Elliot user id
                        'store_id' => $store_id->store_id, // if your are importing store give store id
                        'channel_id' => '', // if your are importing channel give channel id
                        'country_code' => $store_country_code,
                        'currency_code' => $store_currency_code,
                        'conversion_rate' => $conversion_rate,
                        'upc' => $variants_barcode, // Product upc if any
                        'jan' => '', // Product jan if any
                        'isban' => '', // Product isban if any
                        'mpn' => '', // Product mpn if any
                        'categories' => array(), // Product categroy array. If null give a blank array
                        'images' => $product_image_data, // Product images data
                    );
                    Stores::productImportingCommon($product_data);
                }
                $count++;
            }
            }
        }
    }

    public function shopifyOrderImport($user_id, $sc, $store_connection_id, $store_currency_code, $store_name, $conversion_rate) {
	try {
	    $total_order_count = $sc->call('GET', '/admin/orders/count.json?status=any');
	    $call_left = $sc->callsLeft();
	    if ($call_left <= 8) {
		sleep(20);
	    }
	    $count = ceil($total_order_count / 250);
	    for ($i = 1; $i <= $count; $i++):
		$order_result = $sc->call('GET', '/admin/orders.json?page=' . $i . '&limit=250&status=any');
		$call_left = $sc->callsLeft();
		if ($call_left <= 8) {
		    sleep(20);
		}

		$curr_time = date('Y-m-d h:i:s');
		$myfile = fopen("bigcommerce_callback.php", "a") or die("Unable to open file!");
		$txt = "Order importing call left => " . $call_left . " Time is " . $curr_time;
		fwrite($myfile, "\r\n" . $txt);
		fclose($myfile);

		foreach ($order_result as $_order):
		    $order_id = $_order['id'];
		    $customer_email = $_order['email'];
		    $customer_id = '';
		    if (isset($_order['customer']['id'])) {
			$customer_id = $_order['customer']['id'];
		    }
		    if ($customer_email != '' && $customer_id != '') {
			$order_created_at = $_order['created_at'];
			$order_updated_at = $_order['updated_at'];
			$payment_gateway = $_order['gateway'];
			$order_total = $_order['total_price'];
			$subtotal = $_order['subtotal_price'];
			$total_tax = $_order['total_tax'];
			$order_status = $_order['financial_status'];
			$cancelled_at = $_order['cancelled_at'];
			if ($cancelled_at != '') {
			    $order_status = 'voided';
			}

			$total_discount = $_order['total_discounts'];
			$total_product_count = count($_order['line_items']);
			$base_shipping_cost = isset($_order['shipping_lines'][0]['price']) ? $_order['shipping_lines'][0]['price'] : 0;
			$refund = $_order['refunds'];
			/*
			 * Refund total amount
			 */
			$refund_amount = '';
			foreach ($refund as $_refund) {
			    $refund_line_items = $_refund['refund_line_items'];
			    foreach ($refund_line_items as $_refund_items) {
				$refund_amount += $_refund_items['subtotal'];
			    }
			}

			/*
			 * Order status change according to status
			 */
			switch ($order_status) {
			    case "pending":
				$order_status_2 = 'Pending';
				break;
			    case "authorized":
				$order_status_2 = "In Transit";
				break;
			    case "partially_paid":
				$order_status_2 = "Completed";
				break;
			    case "paid":
				$order_status_2 = "Completed";
				break;
			    case "partially_refunded":
				$order_status_2 = "Refunded";
				break;
			    case "refunded":
				$order_status_2 = "Refunded";
				break;
			    case "voided":
				$order_status_2 = "Cancel";
				break;
			    default:
				$order_status_2 = "Pending";
			}

			/** Order Billing Address */
			$billing_f_name = $billing_l_name = $billing_street_1 = $billing_street_2 = $billing_phone = $billing_city = $billing_zip = $billing_state = $billing_country = $billing_country_code = '';
			if (isset($_order['billing_address'])) {
			    $order_billing_add = $_order['billing_address'];
			    $billing_f_name = $order_billing_add['first_name'];
			    $billing_l_name = $order_billing_add['last_name'];
			    $billing_street_1 = $order_billing_add['address1'];
			    $billing_street_2 = $order_billing_add['address2'];
			    $billing_phone = $order_billing_add['phone'];
			    $billing_city = $order_billing_add['city'];
			    $billing_zip = $order_billing_add['zip'];
			    $billing_state = $order_billing_add['province'];
			    $billing_country = $order_billing_add['country'];
			    $billing_country_code = $order_billing_add['country_code'];
			}

			/** Order Shipping Address */
			$shipping_f_name = $shipping_l_name = $shipping_street_1 = $shipping_street_2 = $shipping_phone = $shipping_city = $shipping_zip = $shipping_state = $shipping_country = '';
			if (isset($_order['shipping_address'])) {
			    $order_shipping_add = $_order['shipping_address'];
			    $shipping_f_name = $order_shipping_add['first_name'];
			    $shipping_l_name = $order_shipping_add['last_name'];
			    $shipping_street_1 = $order_shipping_add['address1'];
			    $shipping_street_2 = $order_shipping_add['address2'];
			    $shipping_phone = $order_shipping_add['phone'];
			    $shipping_city = $order_shipping_add['city'];
			    $shipping_zip = $order_shipping_add['zip'];
			    $shipping_state = $order_shipping_add['province'];
			    $shipping_country = $order_shipping_add['country'];
			    $shipping_country_code = $order_shipping_add['country_code'];
			}
			$shipping_address = $shipping_street_1 . "," . $shipping_street_2 . "," . $shipping_city . "," . $shipping_state . "," . $shipping_zip . "," . $shipping_country;
			$billing_address = $billing_street_1 . "," . $billing_street_2 . "," . $billing_city . "," . $billing_state . "," . $billing_zip . "," . $billing_country;

			$order_items = $_order['line_items'];
			$order_product_data = array();
			foreach ($order_items as $_items) {
			    $title = $_items['title'];
			    $quantity = $_items['quantity'];
			    $price = $_items['price'];
			    $sku = '';
			    if ($_items['sku'] != "") {
				$sku = $_items['sku'];
			    }
			    $product_id = ($_items['product_id'] == '') ? 0 : $_items['product_id'];
			    $product_weight = $_items['grams'];
			    $order_product_data[] = array(
				'product_id' => $product_id,
				'name' => $title,
				'sku' => $sku,
				'price' => $price,
				'qty_ordered' => $quantity,
				'weight' => $product_weight,
			    );
			}

			$get_shopify_id = Stores::find()->select('store_id')->where(['store_name' => $store_name])->one();
			$customer_id = ($customer_id == '') ? 0 : $customer_id;
			$order_data = array(
			    'order_id' => $order_id,
			    'status' => $order_status_2,
			    'magento_store_id' => '',
			    'order_grand_total' => $order_total,
			    'customer_id' => $customer_id,
			    'customer_email' => $customer_email,
			    'order_shipping_amount' => $base_shipping_cost,
			    'order_tax_amount' => $total_tax,
			    'total_qty_ordered' => $total_product_count,
			    'created_at' => $order_created_at,
			    'updated_at' => $order_updated_at,
			    'payment_method' => $payment_gateway,
			    'refund_amount' => $refund_amount,
			    'discount_amount' => $total_discount,
			    'channel_store_name' => $store_name,
			    'channel_store_prefix' => 'SP',
			    'mul_store_id' => $store_connection_id,
			    'mul_channel_id' => '',
			    'elliot_user_id' => $user_id,
			    'store_id' => $get_shopify_id->store_id,
			    'currency_code' => $store_currency_code,
			    'conversion_rate' => $conversion_rate,
			    'channel_id' => '',
			    'shipping_address' => array(
				'street_1' => $shipping_street_1,
				'street_2' => $shipping_street_2,
				'state' => $shipping_state,
				'city' => $shipping_city,
				'country' => $shipping_country,
				'country_id' => $shipping_country_code,
				'postcode' => $shipping_zip,
				'email' => $customer_email,
				'telephone' => $shipping_phone,
				'firstname' => $shipping_f_name,
				'lastname' => $shipping_l_name,
			    ),
			    'billing_address' => array(
				'street_1' => $billing_street_1,
				'street_2' => $billing_street_2,
				'state' => $billing_state,
				'city' => $billing_city,
				'country' => $billing_country,
				'country_id' => $billing_country_code,
				'postcode' => $billing_zip,
				'email' => $customer_email,
				'telephone' => $billing_phone,
				'firstname' => $billing_f_name,
				'lastname' => $billing_l_name,
			    ),
			    'items' => $order_product_data,
			);
			Stores::orderImportingCommon($order_data);
		    }
		    //                else {
		    //error_log('Followig product are not imported from shopify herer the id ' . $_order['id'] . "+++++", 3, 'order_import_error.log');
		    //                }
		endforeach;
	    endfor;
	} catch (Exception $e) {
	    $msg = $e->getMessage();
	    $curr_time = date('Y-m-d h:i:s');
	    $myfile = fopen("bigcommerce_callback.php", "a") or die("Unable to open file!");
	    $txt = "Customer importing log here => " . $msg . " Time is " . $curr_time;
	    fwrite($myfile, "\r\n" . $txt);
	    fclose($myfile);
	    sleep(30);
	}
    }

    /*
     * Magento sign up frontend
     */

    public function actionMagento() {
	return $this->render('magento');
    }

    /**
     * Authorize the shopify API Credentials
     */
    public function actionAuthMagento() {
	$array_msg = array();
	$magento_shop = $_POST['magento_shop'];
	$magento_soap_user = $_POST['magento_soap_user'];
	$magento_soap_api = $_POST['magento_soap_api'];
	$user_id = $_POST['user_id'];
	$magento_country = $_POST['magento_country'];

	$api_url = $magento_shop . "/api/soap/?wsdl";
	try {
	    $cli = new SoapClient($api_url, array('trace' => true, 'exceptions' => true));
	    $session_id = $cli->login($magento_soap_user, $magento_soap_api);
	} catch (SoapFault $e) {
	    $msg = $e->faultstring;
	    $array_msg['api_error'] = 'Your API credentials are not working.' . $msg;
	    return json_encode($array_msg);
	}
	$config = yii\helpers\ArrayHelper::merge(
			require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php'));
	$application = new yii\web\Application($config);

	$orders_data = User::find()->where(['id' => $user_id])->one();
	$orders_data->magento_shop_url = $magento_shop;
	$orders_data->save(false);

	$user = User::find()->where(['id' => $user_id])->one();
	$user->magento_shop_url = $magento_shop;
	$user->save(false);
	$user_domain = $user->domain_name;
	$config = yii\helpers\ArrayHelper::merge(
			require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $user_domain . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
	);
	$application = new yii\web\Application($config);

	$mage_country = Countries::countryDetails($magento_country);
	$check_api_key = StoresConnection::find()->where(['mag_shop' => $magento_shop, 'mag_soap_user' => $magento_soap_user, 'mag_soap_api' => $magento_soap_api])->one();
	if (!empty($check_api_key)) {
	    $array_msg['api_error'] = 'This Magento shop is already integrated with Elliot. Please use another API details to integrate with Elliot.';
	    return json_encode($array_msg);
	}
	$check_country = StoreDetails::find()->where(['country' => $mage_country->name, 'channel_accquired' => 'Magento'])->one();
	if (!empty($check_country)) {
	    $array_msg['api_error'] = 'This Magento shop with same country <b>' . $mage_country->name . '</b> is already integrated with Eliiot.';
	    return json_encode($array_msg);
	}

	$store = Stores::find()->select('store_id')->where(['store_name' => 'Magento'])->one();
	$magento_store_id = $store->store_id;
	//$checkConnection = StoresConnection::find()->where(['store_id' => $magento_store_id, 'user_id' => $user_id])->one();
	//if (empty($checkConnection)){
	$connectionModel = new StoresConnection();
	$connectionModel->store_id = $magento_store_id;
	$connectionModel->user_id = $user_id;
	$connectionModel->connected = 'Yes';
	$connectionModel->mag_shop = $magento_shop;
	$connectionModel->mag_soap_user = $magento_soap_user;
	$connectionModel->mag_soap_api = $magento_soap_api;
	$created_date = date('Y-m-d h:i:s', time());
	$connectionModel->created = $created_date;
	if ($connectionModel->save(FALSE)) {
	    $array_msg['success'] = "Your Magento shop has been connected successfully. Importing is started. Once importing is done you will get a notify message.";
	    $array_msg['store_connection_id'] = $connectionModel->stores_connection_id;
	} else {
	    $array_msg['error'] = "Error Something went wrong. Please try again";
	}
	//}
	return json_encode($array_msg);
    }

    /*
     * Importing from Magento store to Elliot
     */

    public function actionMagentoImporting() {
	$user_id = $_GET['user_id'];
	$conversion_rate='';
	$store_connection_id = $_GET['store_connection_id'];
	$magento_country_code = $_GET['magento_country_code'];
	$get_users = User::find()->select('domain_name,company_name,email')->where(['id' => $user_id])->one();
	$email = $get_users->email;
	$company_name = $get_users->company_name;
	$config = yii\helpers\ArrayHelper::merge(
			require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
	);
	$application = new yii\web\Application($config);

	$get_magento_id = Stores::find()->select('store_id')->where(['store_name' => 'Magento'])->one();
	$magento_store_id = $get_magento_id->store_id;
	$store_magento = StoresConnection::find()->where(['store_id' => $magento_store_id, 'user_id' => $user_id, 'stores_connection_id' => $store_connection_id])->one();
	if (!empty($store_magento)) {
	    $magento_shop = $store_magento->mag_shop;
	    $magento_soap_user = $store_magento->mag_soap_user;
	    $magento_soap_api_key = $store_magento->mag_soap_api;
	    try {
		$api_url = $magento_shop . '/api/soap/?wsdl';
		$cli = new SoapClient($api_url);
		$session_id = $cli->login($magento_soap_user, $magento_soap_api_key);

		//All Stores of Magento
		$stores_list = $cli->call($session_id, 'store.list');
		// All Category of magento store
		$category_result = $cli->call($session_id, 'catalog_category.tree');
		//All Product list of magento store
		$product_list = $cli->call($session_id, 'catalog_product.list');
		//All Customer list of magento store
		$customer_list = $cli->call($session_id, 'customer.list');
		//All Order list of magento store
		$orders_list = $cli->call($session_id, 'sales_order.list');

		$country_detail = Countries::countryDetails($magento_country_code);
		if (!empty($country_detail)) {
		    $currency_code=$country_detail->currency_code;
		    /*Save Currency conversion details*/
		    $conversion_rate = Stores::getCurrencyConversionRate($currency_code, 'USD');
		    $currency_check = CurrencyConversion::find()->Where(['to_currency' => $currency_code])->one();
		    if (empty($currency_check)) {
			$store_currency_conversion = new CurrencyConversion();
			$store_currency_conversion->from_currency = 'USD';
			$store_currency_conversion->from_value = 1;
			$store_currency_conversion->to_currency = $currency_code;
			$store_currency_conversion->to_value = $conversion_rate;
			$store_currency_conversion->created_at = date('Y-m-d H:i:s', time());
			$store_currency_conversion->save(false);
		    } else {
			$currency_check->from_currency = 'USD';
			$currency_check->from_value = 1;
			$currency_check->to_currency = $currency_code;
			$currency_check->to_value = $conversion_rate;
			$currency_check->save(false);
		    }
		    
		}


		Stores::getMagentoStoresDetails($magento_shop, $store_connection_id, $country_detail);
		Stores::getMagentoStores($stores_list, $user_id, $store_connection_id);
		Stores::getMagentoAllCategory($category_result, $user_id, $store_connection_id);
		Stores::magentoParentCategoryAssign($category_result, $store_connection_id);
		Stores::getMagentoAllProducts($product_list, $user_id, $session_id, $cli, $magento_shop, $store_connection_id, $country_detail,$conversion_rate);
		Stores::getMagentoAllCustomers($customer_list, $user_id, $session_id, $cli, $store_connection_id);
		Stores::getMagentoAllOrders($orders_list, $user_id, $session_id, $cli, $store_connection_id, $country_detail,$conversion_rate);


		/* End Email Magento */
		$email_message = 'Success, Your Magento Store is Now Connected';
		$send_email_notif = CustomFunction::ConnectBigCommerceEmail($email, $company_name, $email_message);

		//Drop-Down Notification for User
		$notif_type = 'Magento';
		$notif_db = Notification::find()->Where(['notif_type' => $notif_type])->one();
		if (empty($notif_db)):
		    $notification_model = new Notification();
		    $notification_model->user_id = $user_id;
		    $notification_model->notif_type = $notif_type;
		    $notification_model->notif_description = 'Your Magento store data has been successfully imported.';
		    $notification_model->created_at = date('Y-m-d h:i:s', time());
		    $notification_model->save(false);
		endif;
		//Sticky Notification :StoresConnection Model Entry for Import Status
		$get_rec = StoresConnection::find()->Where(['user_id' => $user_id, 'store_id' => $magento_store_id, 'stores_connection_id' => $store_connection_id])->one();
		$get_rec->import_status = 'Completed';
		$get_rec->save(false);
	    } catch (SoapFault $e) {
		$e->faultstring;
	    }
	}
    }

    /* For  WOOCommerce */

    public function actionAuthWoocommerce() {
	$array_msg = array();
	$woocommerce_store_url = trim($_POST['woocommerce_store_url']);
	$woocommerce_consumer = $_POST['woocommerce_consumer'];
	$woocommerce_secret = $_POST['woocommerce_secret'];
	$woocommerce_country_code = $_POST['woocommerce_country'];
	$user_id = $_POST['user_id'];

	$user = User::find()->where(['id' => $user_id])->one();
	$user_domain = $user->domain_name;

	/* For Country */
	$currency_Symbol = '';
	$countryObject = Countries::countryDetails($woocommerce_country_code);
	$country_name = $countryObject->name;
	$currency_code = $countryObject->currency_code;
	$currency_symbol = $countryObject->currency_symbol;
	$currency_name = strtolower($currency_code);
	$currency_Symbol = CurrencySymbols::find()->select(['symbol'])->where(['name' => $currency_name])->one();
	if (!empty($currency_Symbol)) {
	    $currency_Symbol = $currency_Symbol->symbol;
	}





	/* For Https server */
	$check_url = parse_url($woocommerce_store_url);
	$url_protocol = $check_url['scheme'];
	if ($url_protocol == 'http'):
	    /* For Http Url */
	    $woocommerce = new Woocommerce(
		    $woocommerce_store_url, $woocommerce_consumer, $woocommerce_secret, [
		'wp_api' => true,
		'version' => 'wc/v1',
		    ]
	    );
	else:
	    $woocommerce = new Woocommerce(
		    '' . $woocommerce_store_url . '/wp-json/wc/v1/customers/?consumer_key=' . $woocommerce_consumer . '&consumer_secret=' . $woocommerce_secret . '', $woocommerce_consumer, $woocommerce_secret, [
		'wp_api' => true,
		'version' => 'wc/v1',
		"query_string_auth" => true,
		    ]
	    );
	endif;

	try {
	    $results = $woocommerce->get('customers');
	    // Example: ['customers' => [[ 'id' => 8, 'created_at' => '2015-05-06T17:43:51Z', 'email' => ...
	    // Last request data.
	    $lastRequest = $woocommerce->http->getRequest();
	    $lastRequest->getUrl(); // Requested URL (string).
	    $lastRequest->getMethod(); // Request method (string).
	    $lastRequest->getParameters(); // Request parameters (array).
	    $lastRequest->getHeaders(); // Request headers (array).
	    $lastRequest->getBody(); // Request body (JSON).
	    // Last response data.
	    $lastResponse = $woocommerce->http->getResponse();
	    $lastResponse->getCode(); // Response code (int).
	    $lastResponse->getHeaders(); // Response headers (array).
	    $lastResponse->getBody(); // Response body (JSON).  
	} catch (HttpClientException $e) {
	    $error = $e->getMessage();
	    $array_msg['api_error'] = 'Your API credentials are not working. Please check and try again.';
	    return json_encode($array_msg);
	}



	$check_api_key = StoresConnection::find()->where(['woo_store_url' => $woocommerce_store_url, 'woo_consumer_key' => $woocommerce_consumer, 'woo_secret_key' => $woocommerce_secret])->one();
	if (!empty($check_api_key)) {
	    $array_msg['api_error'] = 'This WooCommerce store is all ready integrated with Elliot. Please use another API details to integrate with Elliot.';
	    return json_encode($array_msg);
	}

	$check_country = StoreDetails::find()->where(['country' => $country_name, 'channel_accquired' => 'WooCommerce'])->one();
	if (!empty($check_country)) {
	    $array_msg['api_error'] = 'This WooCommerce store with same country <b>"' . $country_name . '"</b> is already integrated with Eliiot.';
	    return json_encode($array_msg);
	}



	$store = Stores::find()->select('store_id')->where(['store_name' => 'WooCommerce'])->one();
	$woocommerce_store_id = $store->store_id;
//        $checkConnection = StoresConnection::find()->where(['store_id' => $woocommerce_store_id, 'user_id' => $user_id])->one();
//        if (empty($checkConnection)):
	$connectionModel = new StoresConnection();
	$connectionModel->store_id = $woocommerce_store_id;
	$connectionModel->user_id = $user_id;
	$connectionModel->connected = 'Yes';
	$connectionModel->woo_store_url = $woocommerce_store_url;
	$connectionModel->woo_consumer_key = $woocommerce_consumer;
	$connectionModel->woo_secret_key = $woocommerce_secret;
	$created_date = date('Y-m-d h:i:s', time());
	$connectionModel->created = $created_date;
	if ($connectionModel->save()):
	    $store_connection_id = $connectionModel->stores_connection_id;
	    $save_store_details = new StoreDetails();
	    $save_store_details->store_connection_id = $store_connection_id;
	    $save_store_details->store_name = $woocommerce_store_url;
	    $save_store_details->store_url = $woocommerce_store_url;
	    $save_store_details->country = $country_name;
	    $save_store_details->country_code = $woocommerce_country_code;
	    $save_store_details->currency = $currency_code;
	    $save_store_details->currency_symbol = $currency_Symbol;
	    $save_store_details->channel_accquired = 'WooCommerce';
	    $save_store_details->created_at = date('Y-m-d H:i:s', time());
	    $save_store_details->save(false);

	    //Yii::$app->session->setFlash('success', 'Success! Your shop has been connected successfully. Syncing will start soon.');
	    $array_msg['success'] = "Your WooCommerce store has been connected successfully. Syncing will start soon.";
	    $array_msg['store_connection_id'] = $store_connection_id;
	else:
	    $array_msg['error'] = "Error Something went wrong. Please try again";
	endif;
	//     endif;
	// Yii::$app->session->setFlash('success', 'Success! Your WooCommerce store data has been successfully imported.');
	return json_encode($array_msg);
    }

    public function actionWoocommerceImporting() {

	$user_id = $_GET['user_id'];
	$conversion_rate = '';
	$store_connection_id = $_GET['store_connection_id'];
	$get_users = User::find()->select('domain_name,company_name,email')->where(['id' => $user_id])->one();
	$user_domain = $get_users->domain_name;
	$config = yii\helpers\ArrayHelper::merge(
			require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
	);
	$application = new yii\web\Application($config);

	$store_woocommerce = StoresConnection::find()->where(['stores_connection_id' => $store_connection_id])->with('storesDetails')->one();
	$woo_store_url = $store_woocommerce->woo_store_url;
	$woo_consumer_key = $store_woocommerce->woo_consumer_key;
	$woo_secret_key = $store_woocommerce->woo_secret_key;
	$woocommerce_store_id = $store_woocommerce->store_id;
	$store_country_code = $store_woocommerce->storesDetails->country_code;
	$store_currency_code = $store_woocommerce->storesDetails->currency;

	if ($store_currency_code != '') {

	    $conversion_rate = Stores::getCurrencyConversionRate($store_currency_code, 'USD');
	    $currency_check = CurrencyConversion::find()->Where(['to_currency' => $store_currency_code])->one();
	    if (empty($currency_check)) {
		$store_currency_conversion = new CurrencyConversion();
		$store_currency_conversion->from_currency = 'USD';
		$store_currency_conversion->from_value = 1;
		$store_currency_conversion->to_currency = $store_currency_code;
		$store_currency_conversion->to_value = $conversion_rate;
		$store_currency_conversion->created_at = date('Y-m-d H:i:s', time());
		$store_currency_conversion->save(false);
	    } else {
		$currency_check->from_currency = 'USD';
		$currency_check->from_value = 1;
		$currency_check->to_currency = $store_currency_code;
		$currency_check->to_value = $conversion_rate;
		$currency_check->save(false);
	    }
	}

	/* For Https server */
	$check_url = parse_url($woo_store_url);
	$url_protocol = $check_url['scheme'];
	if ($url_protocol == 'http'):
	    /* For Http Url */
	    $woocommerce = new Woocommerce(
		    $woo_store_url, $woo_consumer_key, $woo_secret_key, [
		'wp_api' => true,
		'version' => 'wc/v1',
		    ]
	    );
	else:
	    // $woocommerce = '';
	    $woocommerce = new Woocommerce(
		    '' . $woo_store_url . '/wp-json/wc/v1/webhooks/?consumer_key=' . $woo_consumer_key . '&consumer_secret=' . $woo_secret_key . '', $woo_consumer_key, $woo_secret_key, [
		'wp_api' => true,
		'version' => 'wc/v1',
		"query_string_auth" => true,
		    ]
	    );
	endif;
	/* Get WebHooks For WOO Commerce */
	$hooks = $woocommerce->get('webhooks');

	/* Iport WebHooks */
	/*      $customer_create = [
	  'name' => 'customer created',
	  'topic' => 'customer.created',
	  'secret' => $woo_secret_key,
	  'delivery_url' => Yii::$app->params['BASE_URL'] . 'people/wc-create-customer-hook?id=' . $user_id."&connection_id=". $store_connection_id
	  ];
	  $customer_update = [
	  'name' => 'customer updated',
	  'topic' => 'customer.updated',
	  'secret' => $woo_secret_key,
	  'delivery_url' => Yii::$app->params['BASE_URL'] . 'people/wc-customer-update-hook?id=' . $user_id."&connection_id=". $store_connection_id
	  ];
	  $customer_delete = [
	  'name' => 'customer deleted',
	  'topic' => 'customer.deleted',
	  'secret' => $woo_secret_key,
	  'delivery_url' => Yii::$app->params['BASE_URL'] . 'people/wc-customer-delete-hook?id=' . $user_id."&connection_id=". $store_connection_id
	  ]; */

	$product_created = [
	    'name' => 'product created',
	    'topic' => 'product.created',
	    'secret' => $woo_secret_key,
	    'delivery_url' => Yii::$app->params['BASE_URL'] . 'people/wc-product-create-hook?id=' . $user_id . "&connection_id=" . $store_connection_id
	];
	$product_updated = [
	    'name' => 'product updated',
	    'topic' => 'product.updated',
	    'secret' => $woo_secret_key,
	    'delivery_url' => Yii::$app->params['BASE_URL'] . 'people/wc-update-product?id=' . $user_id . "&connection_id=" . $store_connection_id
	];
	$product_deleted = [
	    'name' => 'product deleted',
	    'topic' => 'product.deleted',
	    'secret' => $woo_secret_key,
	    'delivery_url' => Yii::$app->params['BASE_URL'] . 'people/wc-delete-product-hook?id=' . $user_id . "&connection_id=" . $store_connection_id
	];
	$order_created = [
	    'name' => 'order created',
	    'topic' => 'order.created',
	    'secret' => $woo_secret_key,
	    'delivery_url' => Yii::$app->params['BASE_URL'] . 'people/wc-create-order-hook?id=' . $user_id . "&connection_id=" . $store_connection_id
	];
	$order_updated = [
	    'name' => 'order updated',
	    'topic' => 'order.updated',
	    'secret' => $woo_secret_key,
	    'delivery_url' => Yii::$app->params['BASE_URL'] . 'people/wc-update-order-hook?id=' . $user_id . "&connection_id=" . $store_connection_id
	];
	/*
	  $order_deleted = [
	  'name' => 'order deleted',
	  'topic' => 'order.deleted',
	  'secret' => $woo_secret_key,
	  'delivery_url' => Yii::$app->params['BASE_URL'] . 'people/wc-order-delete-hook?id=' . $user_id."&connection_id=". $store_connection_id
	  ]; */

	$myhooks = array();
	foreach ($hooks as $_hook):
	    $myhooks[] = $_hook['delivery_url'];
	endforeach;
	/* Check hooks in Woocommerce exists or not */
	/*  if (!in_array(Yii::$app->params['BASE_URL'] . 'people/wc-create-customer-hook?id=' . $user_id, $myhooks)) {
	  $woocommerce->post('webhooks', $customer_create);
	  }
	  if (!in_array(Yii::$app->params['BASE_URL'] . 'people/wc-customer-update-hook?id=' . $user_id, $myhooks)) {
	  $woocommerce->post('webhooks', $customer_update);
	  }
	  if (!in_array(Yii::$app->params['BASE_URL'] . 'people/wc-customer-delete-hook?id=' . $user_id, $myhooks)) {
	  $woocommerce->post('webhooks', $customer_delete);
	  } */
	if (!in_array(Yii::$app->params['BASE_URL'] . 'people/wc-product-create-hook?id=' . $user_id, $myhooks)) {
	    $woocommerce->post('webhooks', $product_created);
	}
	if (!in_array(Yii::$app->params['BASE_URL'] . 'people/wc-update-product?id=' . $user_id, $myhooks)) {
	    $woocommerce->post('webhooks', $product_updated);
	}
	if (!in_array(Yii::$app->params['BASE_URL'] . 'people/wc-delete-product-hook?id=' . $user_id, $myhooks)) {
	    $woocommerce->post('webhooks', $product_deleted);
	}
	if (!in_array(Yii::$app->params['BASE_URL'] . 'people/wc-create-order-hook?id=' . $user_id, $myhooks)) {
	    $woocommerce->post('webhooks', $order_created);
	}
	if (!in_array(Yii::$app->params['BASE_URL'] . 'people/wc-update-order-hook?id=' . $user_id, $myhooks)) {
	    $woocommerce->post('webhooks', $order_updated);
	}
	/*
	  if (!in_array(Yii::$app->params['BASE_URL'] . 'people/wc-order-delete-hook?id=' . $user_id, $myhooks)) {
	  $woocommerce->post('webhooks', $order_deleted);
	  } */


	$this->getWoocommerceAllCategory($user_id, $woocommerce, $woo_store_url, $woo_consumer_key, $woo_secret_key, $store_connection_id);
	$this->getWoocommerceAllCustomers($user_id, $woocommerce, $woo_store_url, $woo_consumer_key, $woo_secret_key, $store_connection_id);
	$this->getWoocommerceAllProducts($user_id, $woocommerce_store_id, $woocommerce, $woo_store_url, $woo_consumer_key, $woo_secret_key, $store_connection_id, $store_country_code, $store_currency_code, $conversion_rate);
	$this->getWoocommerceAllOrders($user_id, $woocommerce_store_id, $woocommerce, $woo_store_url, $woo_consumer_key, $woo_secret_key, $store_connection_id, $store_currency_code, $conversion_rate);
	$this->woocommerceCurrencyDetails($user_id, $store_connection_id, $store_currency_code, 'WooCommerce', $woocommerce_store_id);

	//Send Notification Email
	$email = $get_users->email;
	$company_name = $get_users->company_name;
	//$send_email_notif = CustomFunction::ConnectBigCommerceEmail($email, $company_name);
	//Drop-Down Notification for User
	$notif_type = 'WooCommerce';
	$notif_db = Notification::find()->Where(['notif_type' => $notif_type])->one();
	if (empty($notif_db)):
	    $notification_model = new Notification();
	    $notification_model->user_id = $user_id;
	    $notification_model->notif_type = $notif_type;
	    $notification_model->notif_description = 'Your WooCommerce store data has been successfully imported.';
	    $notification_model->created_at = date('Y-m-d h:i:s', time());
	    $notification_model->save(false);
	endif;
	//Sticky Notification :StoresConnection Model Entry for Import Status
	$get_rec = StoresConnection::find()->Where(['user_id' => $user_id, 'store_id' => $woocommerce_store_id, 'stores_connection_id' => $store_connection_id])->one();
	$get_rec->import_status = 'Completed';
	$get_rec->save(false);
	/* Send Email To user When Woocommere Is import */
	$email_message = 'Success, Your WooCommerce Store is Now Connected';
	$send_email_notif = CustomFunction::ConnectBigCommerceEmail($email, $company_name, $email_message);
//        Yii::$app->session->setFlash('success', 'Success! Your WooCommerce store data has been successfully imported.');
	echo $user_domain;
	die;
    }

    public function woocommerceCurrencyDetails($user_id, $store_connection_id, $store_currency_code, $store_name, $woocommerce_store_id) {
	$currency_channel_exists = Channelsetting::find()->Where(['mul_store_id' => $store_connection_id, 'setting_key' => 'currency', 'user_id' => $user_id])->one();
	if (empty($currency_channel_exists)) {
	    $channel_setting = new Channelsetting();
	    $channel_setting->store_id = $woocommerce_store_id;
	    $channel_setting->user_id = $user_id;
	    $channel_setting->channel_name = $store_name;
	    $channel_setting->setting_key = 'currency';
	    $channel_setting->setting_value = $store_currency_code;
	    $channel_setting->mul_store_id = $store_connection_id;
	    $channel_setting->created_at = date('Y-m-d H:i:s');
	    $channel_setting->save(false);
	}
    }

    /* FOR WOOCOMMERCE CUSTOMERS */

    public function getWoocommerceAllCustomers($user_id, $woocommerce, $woo_store_url, $woo_consumer_key, $woo_secret_key, $store_connection_id) {



	/* For Https server */
	$check_url = parse_url($woo_store_url);
	$url_protocol = $check_url['scheme'];
	if ($url_protocol == 'http'):
	    /* For Http Url */
	    $data = array();
	    for ($i = 1; $i <= 1000; $i++) {

		$data = ['page' => $i, 'per_page' => 100];
		$customer_list = $woocommerce->get('customers', $data);
		//if data is empty then loop break else data save
		if (empty($customer_list)):
		    break;
		else:
		    foreach ($customer_list as $_customer):
			$customer_abb_id = $_customer['id'];
			$create_at = date('Y-m-d h:i:s', strtotime($_customer['date_created']));
			$update_at = date('Y-m-d h:i:s', strtotime($_customer['date_modified']));
			$customer_email = $_customer['email'];
			$first_name = $_customer['first_name'];
			$last_name = $_customer['last_name'];

			$bill_street1 = $bill_street2 = $bill_city = $bill_state = $bill_country = $bill_phone = $bill_zip = '';
			if (isset($_customer['billing'])):

			    $street1 = $_customer['billing']['address_1'];
			    $street2 = $_customer['billing']['address_2'];
			    $city = $_customer['billing']['city'];
			    $state = $_customer['billing']['state'];
			    $country = $_customer['billing']['country'];
			    $phone_number = $_customer['billing']['phone'];
			    $zip = $_customer['billing']['postcode'];

			endif;
			$ship_street1 = $ship_street2 = $ship_city = $ship_state = $ship_country = $ship_zip = '';
			if (isset($_customer['shipping'])):

			    $ship_street1 = $_customer['shipping']['address_1'];
			    $ship_street2 = $_customer['shipping']['address_2'];
			    $ship_city = $_customer['shipping']['city'];
			    $ship_state = $_customer['shipping']['state'];
			    $ship_country = $_customer['shipping']['country'];
			    $ship_zip = $_customer['shipping']['postcode'];
			endif;

			$customer_data = array(
			    'customer_id' => $customer_abb_id,
			    'elliot_user_id' => $user_id,
			    'first_name' => $first_name,
			    'last_name' => $last_name,
			    'email' => $customer_email,
			    'channel_store_name' => 'WooCommerce',
			    'channel_store_prefix' => 'WOO',
			    'mul_store_id' => $store_connection_id,
			    'mul_channel_id' => '', // Give multiple channel id
			    'customer_created_at' => $create_at,
			    'customer_updated_at' => $update_at,
			    'billing_address' => array(
				'street_1' => $street1,
				'street_2' => $street2,
				'city' => $city,
				'state' => $state,
				'country' => $country,
				'country_iso' => '',
				'zip' => $zip,
				'phone_number' => $phone_number,
				'address_type' => '',
			    ),
			    'shipping_address' => array(
				'street_1' => $ship_street1,
				'street_2' => $ship_street2,
				'city' => $ship_city,
				'state' => $ship_state,
				'country' => $ship_country,
				'zip' => $ship_zip,
			    ),
			);
			Stores::customerImportingCommon($customer_data);
		    endforeach;
		endif;
	    }
	/* For HTTPS URL */
	else:

	    $data = array();
	    for ($i = 1; $i <= 1000; $i++) {

		$woocommerce_https = new Woocommerce(
			'' . $woo_store_url . '/wp-json/wc/v1/customers/?page=' . $i . '&per_page=100&consumer_key=' . $woo_consumer_key . '&consumer_secret=' . $woo_secret_key . '', $woo_consumer_key, $woo_secret_key, [
		    'wp_api' => true,
		    'version' => 'wc/v1',
		    "query_string_auth" => true,
			]
		);
		$data = ['page' => $i, 'per_page' => 100];
		$customer_list = $woocommerce_https->get('customers', $data);
		//if data is empty then loop break else data save
		if (empty($customer_list)):
		    break;
		else:
		    foreach ($customer_list as $_customer):


			$customer_abb_id = $_customer['id'];
			$create_at = date('Y-m-d h:i:s', strtotime($_customer['date_created']));
			$update_at = date('Y-m-d h:i:s', strtotime($_customer['date_modified']));
			$customer_email = $_customer['email'];
			$first_name = $_customer['first_name'];
			$last_name = $_customer['last_name'];

			$bill_street1 = $bill_street2 = $bill_city = $bill_state = $bill_country = $bill_phone = $bill_zip = '';
			if (isset($_customer['billing'])):

			    $street1 = $_customer['billing']['address_1'];
			    $street2 = $_customer['billing']['address_2'];
			    $city = $_customer['billing']['city'];
			    $state = $_customer['billing']['state'];
			    $country = $_customer['billing']['country'];
			    $phone_number = $_customer['billing']['phone'];
			    $zip = $_customer['billing']['postcode'];



			endif;
			$ship_street1 = $ship_street2 = $ship_city = $ship_state = $ship_country = $ship_zip = '';
			if (isset($_customer['shipping'])):

			    $ship_street1 = $_customer['shipping']['address_1'];
			    $ship_street2 = $_customer['shipping']['address_2'];
			    $ship_city = $_customer['shipping']['city'];
			    $ship_state = $_customer['shipping']['state'];
			    $ship_country = $_customer['shipping']['country'];
			    $ship_zip = $_customer['shipping']['postcode'];
			endif;

			$customer_data = array(
			    'customer_id' => $customer_abb_id,
			    'elliot_user_id' => $user_id,
			    'first_name' => $first_name,
			    'last_name' => $last_name,
			    'email' => $customer_email,
			    'channel_store_name' => 'WooCommerce',
			    'channel_store_prefix' => 'WOO',
			    'mul_store_id' => $store_connection_id,
			    'mul_channel_id' => '', // Give multiple channel id
			    'customer_created_at' => $create_at,
			    'customer_updated_at' => $update_at,
			    'billing_address' => array(
				'street_1' => $street1,
				'street_2' => $street2,
				'city' => $city,
				'state' => $state,
				'country' => $country,
				'country_iso' => '',
				'zip' => $zip,
				'phone_number' => $phone_number,
				'address_type' => '',
			    ),
			    'shipping_address' => array(
				'street_1' => $ship_street1,
				'street_2' => $ship_street2,
				'city' => $ship_city,
				'state' => $ship_state,
				'country' => $ship_country,
				'zip' => $ship_zip,
			    ),
			);
			Stores::customerImportingCommon($customer_data);
		    endforeach;
		endif;
	    }
	endif;
    }

    public function getWoocommerceAllCategory($user_id, $woocommerce, $woo_store_url, $woo_consumer_key, $woo_secret_key, $store_connection_id) {
	// For Woo Commerce categories 

	/* For Https server */
	$check_url = parse_url($woo_store_url);
	$url_protocol = $check_url['scheme'];
	if ($url_protocol == 'http') {
	    /* For Http Url */
	    $data = array();
	    for ($i = 1; $i <= 1000; $i++) {
		$data = ['page' => $i, 'per_page' => 100];
		$categories_wc = $woocommerce->get('products/categories', $data);
		//if data is empty then loop break else data save
		if (empty($categories_wc)) {
		    break;
		} else {
		    foreach ($categories_wc as $category) {
			$cat_channel_abb_id = $category['id'];
			$cat_name = $category['name'];
			$cat_parent_id = $category['parent'];
			$category_parent_id = 0;

			$category_abbrivation_data = CategoryAbbrivation::find()->Where(['channel_abb_id' => 'WOO' . $cat_parent_id])->one();
			if (!empty($category_abbrivation_data)) {
			    $category_parent_id = $category_abbrivation_data->id;
			} else {
			    $category_parent_id = 0;
			}

			$category_data = array(
			    'category_id' => $cat_channel_abb_id, // Give category id of Store/channels
			    'parent_id' => $category_parent_id, // Give Category parent id of Elliot if null then give 0
			    'name' => $cat_name, // Give category name
			    'channel_store_name' => 'WooCommerce', // Give Channel/Store name
			    'channel_store_prefix' => 'WOO', // Give Channel/Store prefix id
			    'mul_store_id' => $store_connection_id, // Give Channel/Store prefix id
			    'mul_channel_id' => '', // Give Channel/Store prefix id
			    'elliot_user_id' => $user_id, // Give Elliot user id
			    'created_at' => date('Y-m-d H:i:s'), // Give Created at date if null then give current date format date('Y-m-d H:i:s')
			    'updated_at' => date('Y-m-d H:i:s'), // Give Updated at date if null then give current date format date('Y-m-d H:i:s')
			);
			Stores::categoryImportingCommon($category_data);
		    }
		}
	    }
	    /* For Https Url */
	} else {

	    /* For Https Url */
	    $data = array();
	    for ($i = 1; $i <= 1000; $i++) {

		$woocommerce_https = new Woocommerce(
			'' . $woo_store_url . '/wp-json/wc/v1/products/categories/?page=' . $i . '&per_page=100&consumer_key=' . $woo_consumer_key . '&consumer_secret=' . $woo_secret_key . '', $woo_consumer_key, $woo_secret_key, [
		    'wp_api' => true,
		    'version' => 'wc/v1',
		    "query_string_auth" => true,
			]
		);

		$data = ['page' => $i, 'per_page' => 100];
		$categories_wc = $woocommerce_https->get('products/categories', $data);
		//if data is empty then loop break else data save
		if (empty($categories_wc)) {
		    break;
		} else {
		    foreach ($categories_wc as $category) {
			$cat_channel_abb_id = $category['id'];
			$cat_name = $category['name'];
			$cat_parent_id = $category['parent'];
			$category_parent_id = 0;

			$category_abbrivation_data = CategoryAbbrivation::find()->Where(['channel_abb_id' => 'WOO' . $cat_parent_id])->one();
			if (!empty($category_abbrivation_data)) {
			    $category_parent_id = $category_abbrivation_data->id;
			} else {
			    $category_parent_id = 0;
			}

			$category_data = array(
			    'category_id' => $cat_channel_abb_id, // Give category id of Store/channels
			    'parent_id' => $category_parent_id, // Give Category parent id of Elliot if null then give 0
			    'name' => $cat_name, // Give category name
			    'channel_store_name' => 'WooCommerce', // Give Channel/Store name
			    'channel_store_prefix' => 'WOO', // Give Channel/Store prefix id
			    'mul_store_id' => $store_connection_id, // Give Channel/Store prefix id
			    'mul_channel_id' => '', // Give Channel/Store prefix id
			    'elliot_user_id' => $user_id, // Give Elliot user id
			    'created_at' => date('Y-m-d H:i:s'), // Give Created at date if null then give current date format date('Y-m-d H:i:s')
			    'updated_at' => date('Y-m-d H:i:s'), // Give Updated at date if null then give current date format date('Y-m-d H:i:s')
			);
			Stores::categoryImportingCommon($category_data);
		    }
		}
	    }
	}
    }

    /* Import all Woocommerce Products */

    public function getWoocommerceAllProducts($user_id, $woocommerce_store_id, $woocommerce, $woo_store_url, $woo_consumer_key, $woo_secret_key, $store_connection_id, $store_country_code, $store_currency_code, $conversion_rate) {


	/* For Https server */
	$check_url = parse_url($woo_store_url);
	$url_protocol = $check_url['scheme'];
	if ($url_protocol == 'http') {
	    /* For Http Url */

	    $data = array();
	    for ($i = 1; $i <= 1000; $i++) {
		//if data is empty then loop break else data save
		$data = ['page' => $i, 'per_page' => 100];
		$Prdoucts_woocommerce = $woocommerce->get('products', $data);
		if (empty($Prdoucts_woocommerce)) {
		    break;
		} else {
		    foreach ($Prdoucts_woocommerce as $product) {
			$p_id = $product['id'];
			$p_name = $product['name'];
			$p_sku = $product['sku'];
			$p_des = $product['description'];
			$product_url = $product['permalink'];
			$p_weight = $product['weight'];
			$product_status = $product['status'];
			if ($product_status == 'publish') {
			    $p_status = 1;
			} else {
			    $p_status = 0;
			}
			$p_price = $product ['regular_price'];

			$p_saleprice = $product['sale_price'];
			/* For if sale price empty null or 0 then price value is  sale price value */
			if ($p_saleprice == '' || $p_saleprice == Null || $p_saleprice == 0) {
			    $p_saleprice = $p_price;
			}
			$p_stk_lvl = $product['stock_quantity'];

			$p_avail = $product['in_stock'];

			$create_date = ($product['date_created'] != "") ? $product['date_created'] : date('Y-m-d H:i:s');
			$update_date = ($product['date_modified'] != "") ? $product['date_modified'] : date('Y-m-d H:i:s');
			$p_created_date = date('Y-m-d H:i:s', strtotime($create_date));
			$p_updated_date = date('Y-m-d H:i:s', strtotime($update_date));
			/* For categories */
			$product_categories_ids = array();
			if (!empty($product['categories'])) {
			    foreach ($product['categories'] as $key => $value) {
				$cat_id = $value['id'];
				$product_categories_ids[] = $cat_id;
			    }
			}
			/* For images */
			$product_image_data = array();
			if (!empty($product['images'])) {
			    $i = 1;
			    foreach ($product['images'] as $product_image) {
				$image_link = $product_image['src'];
				$position = $product_image['position'];
				$label = $product_image['name'];
				$product_image_data[] = array(
				    'image_url' => $image_link,
				    'label' => $label,
				    'position' => $i,
				    'base_img' => $image_link,
				);
				$i++;
			    }
			}
			$product_data = array(
			    'product_id' => $p_id, // Stores/Channel product ID
			    'name' => $p_name, // Product name
			    'sku' => $p_sku, // Product SKU
			    'description' => $p_des, // Product Description
			    'product_url_path' => $product_url, // Product url if null give blank value
			    'weight' => $p_weight, // Product weight if null give blank value
			    'status' => $p_status, // Product status (visible or hidden). Give in 0 or 1 form (0 = hidden, 1 = visible).
			    'price' => $p_price, // Porduct price
			    'sale_price' => $p_saleprice, // Product sale price if null give Product price value
			    'qty' => $p_stk_lvl, //Product quantity 
			    'stock_status' => $p_avail, // Product stock status ("in stock" or "out of stock"). 
			    // Give in 0 or 1 form (0 = out of stock, 1 = in stock)
			    'websites' => array(), //This is for only magento give only and blank array
			    'brand' => '', // Product brand if any
			    'low_stock_notification' => 5, // Porduct low stock notification if any otherwise give default 5 value
			    'created_at' => $p_created_date, // Product created at date format date('Y-m-d H:i:s')
			    'updated_at' => $p_updated_date, // Product updated at date format date('Y-m-d H:i:s')
			    'mul_store_id' => $store_connection_id, //Give multiple store id
			    'mul_channel_id' => '', // Give multiple channel id
			    'channel_store_name' => 'WooCommerce', // Channel or Store name
			    'channel_store_prefix' => 'WOO', // Channel or store prefix id
			    'elliot_user_id' => $user_id, // Elliot user id
			    'store_id' => $woocommerce_store_id, // if your are importing store give store id
			    'channel_id' => '', // if your are importing channel give channel id
			    'country_code' => $store_country_code, // if your are importing channel give channel id
			    'currency_code' => $store_currency_code,
			    'conversion_rate' => $conversion_rate,
			    'barcode' => '', // Product barcode if any
			    'ean' => '', // Product ean if any
			    'jan' => '', // Product jan if any
			    'isban' => '', // Product isban if any
			    'mpn' => '', // Product mpn if any
			    'categories' => $product_categories_ids, // Product categroy array. If null give a blank array 
			    'images' => $product_image_data, // Product images data
			);

			Stores::productImportingCommon($product_data);
			//variations
			/* if (!empty($product['attributes'])) {
			  foreach ($product['attributes'] as $variation){
			  $var_name = $variation['name'];
			  //Check whelther Variation with the same name available
			  $checkVarModel = Variations::find()->where(['variation_name' => $var_name])->one();

			  if (empty($checkVarModel)){
			  //Create Model for each new Variation(Product Option) fetched
			  $variationModel = new Variations();
			  $variationModel->variation_name = $var_name;
			  $created_date = date('Y-m-d H:i:s', time());
			  $variationModel->created_at = $created_date;
			  //Save Elliot User id
			  $variationModel->elliot_user_id = $user_id;
			  if ($variationModel->save()){
			  $variation_id = $variationModel->variations_ID;
			  //Create Model for each new Variation Item(Product Option Value) fetched
			  if (!empty($variation['options'])){
			  foreach ($variation['options'] as $wc_att){

			  $variationItemModel = new VariationsItemList();
			  $variationItemModel->variation_id = $variation_id;
			  $variationItemModel->item_name = $var_name;
			  $variationItemModel->item_value = $wc_att;
			  $created_date = date('Y-m-d H:i:s', time());
			  $variationItemModel->created_at = $created_date;
			  //Save Elliot User id
			  $variationItemModel->elliot_user_id = $user_id;
			  $variationItemModel->save();
			  }
			  }
			  }
			  }
			  }
			  } */
			/* if (!empty($product['variations'])){
			  foreach ($product['variations'] as $product_sku){
			  //Woocommerce variation
			  //SKU
			  if (isset($product_sku['sku'])){
			  $newP_Var_Model = new ProductVariation;
			  $newP_Var_Model->product_id = $saved_product_id;
			  $newP_Var_Model->elliot_user_id = $user_id;
			  $newP_Var_Model->item_name = 'SKU';
			  $newP_Var_Model->item_value = $product_sku['sku'];
			  $newP_Var_Model->store_variation_id = 'WOO' . $product_sku['id'];
			  $created_date = date('Y-m-d H:i:s', time());
			  $newP_Var_Model->created_at = $created_date;
			  $newP_Var_Model->save(false);
			  }

			  //Price
			  if (isset($product_sku['price'])){
			  $newP_Var_Model = new ProductVariation;
			  $newP_Var_Model->product_id = $saved_product_id;
			  $newP_Var_Model->elliot_user_id = $user_id;
			  $newP_Var_Model->item_name = 'Price';
			  $newP_Var_Model->item_value = $product_sku['price'];
			  $newP_Var_Model->store_variation_id = 'WOO' . $product_sku['id'];
			  $created_date = date('Y-m-d H:i:s', time());
			  $newP_Var_Model->created_at = $created_date;
			  $newP_Var_Model->save(false);
			  }
			  //sale_price
			  if (isset($product_sku['sale_price'])){
			  $newP_Var_Model = new ProductVariation;
			  $newP_Var_Model->product_id = $saved_product_id;
			  $newP_Var_Model->elliot_user_id = $user_id;
			  $newP_Var_Model->item_name = 'Sales Price';
			  $newP_Var_Model->item_value = $product_sku['sale_price'];
			  $newP_Var_Model->store_variation_id = 'WOO' . $product_sku['id'];
			  $created_date = date('Y-m-d H:i:s', time());
			  $newP_Var_Model->created_at = $created_date;
			  $newP_Var_Model->save(false);
			  }
			  //Inventory
			  if (isset($product_sku['stock_quantity'])){
			  $newP_Var_Model = new ProductVariation;
			  $newP_Var_Model->product_id = $saved_product_id;
			  $newP_Var_Model->elliot_user_id = $user_id;
			  $newP_Var_Model->item_name = 'Stock Quantity';
			  $newP_Var_Model->item_value = $product_sku['stock_quantity'];
			  $newP_Var_Model->store_variation_id = 'WOO' . $product_sku['id'];
			  $created_date = date('Y-m-d H:i:s', time());
			  $newP_Var_Model->created_at = $created_date;
			  $newP_Var_Model->save(false);
			  }
			  //Weight
			  if (isset($product_sku['weight'])){
			  $newP_Var_Model = new ProductVariation;
			  $newP_Var_Model->product_id = $saved_product_id;
			  $newP_Var_Model->elliot_user_id = $user_id;
			  $newP_Var_Model->item_name = 'Weight';
			  $newP_Var_Model->item_value = $product_sku['weight'];
			  $newP_Var_Model->store_variation_id = 'BGC' . $product_sku['id'];
			  $created_date = date('Y-m-d H:i:s', time());
			  $newP_Var_Model->created_at = $created_date;
			  $newP_Var_Model->save(false);
			  }
			  //in_stock
			  if (isset($product_sku['in_stock'])){
			  if ($product_sku['in_stock'] == 1) {
			  $stock = 'In Stock';
			  } else {
			  $stock = 'Out of Stock';
			  }
			  $newP_Var_Model = new ProductVariation;
			  $newP_Var_Model->product_id = $saved_product_id;
			  $newP_Var_Model->elliot_user_id = $user_id;
			  $newP_Var_Model->item_name = 'Stock Status';
			  $newP_Var_Model->item_value = $stock;
			  $newP_Var_Model->store_variation_id = 'WOO' . $product_sku['id'];
			  $created_date = date('Y-m-d H:i:s', time());
			  $newP_Var_Model->created_at = $created_date;
			  $newP_Var_Model->save(false);
			  }
			  }
			  } */
		    }
		}
	    }
	    /* For Https Url */
	} else {
	    /* For Https Url */
	    $data = array();
	    for ($i = 1; $i <= 1000; $i++) {
		//if data is empty then loop break else data save

		$woocommerce_https = new Woocommerce(
			'' . $woo_store_url . '/wp-json/wc/v1/products/?page=' . $i . '&per_page=100&consumer_key=' . $woo_consumer_key . '&consumer_secret=' . $woo_secret_key . '', $woo_consumer_key, $woo_secret_key, [
		    'wp_api' => true,
		    'version' => 'wc/v1',
		    "query_string_auth" => true,
			]
		);


		$data = ['page' => $i, 'per_page' => 100];
		$Prdoucts_woocommerce = $woocommerce_https->get('products', $data);
		if (empty($Prdoucts_woocommerce)) {
		    break;
		} else {
		    foreach ($Prdoucts_woocommerce as $product) {
			$p_id = $product['id'];
			$p_name = $product['name'];
			$p_sku = $product['sku'];
			$p_des = $product['description'];
			$product_url = $product['permalink'];
			$p_weight = $product['weight'];
			$product_status = $product['status'];
			if ($product_status == 'publish') {
			    $p_status = 1;
			} else {
			    $p_status = 0;
			}
			$p_price = $product ['regular_price'];

			$p_saleprice = $product['sale_price'];
			/* For if sale price empty null or 0 then price value is  sale price value */
			if ($p_saleprice == '' || $p_saleprice == Null || $p_saleprice == 0) {
			    $p_saleprice = $p_price;
			}
			$p_stk_lvl = $product['stock_quantity'];

			$p_avail = $product['in_stock'];

			$create_date = ($product['date_created'] != "") ? $product['date_created'] : date('Y-m-d H:i:s');
			$update_date = ($product['date_modified'] != "") ? $product['date_modified'] : date('Y-m-d H:i:s');
			$p_created_date = date('Y-m-d H:i:s', strtotime($create_date));
			$p_updated_date = date('Y-m-d H:i:s', strtotime($update_date));
			/* For categories */
			$product_categories_ids = array();
			if (!empty($product['categories'])) {
			    foreach ($product['categories'] as $key => $value) {
				$cat_id = $value['id'];
				$product_categories_ids[] = $cat_id;
			    }
			}
			/* For images */
			$product_image_data = array();
			if (!empty($product['images'])) {
			    $i = 1;
			    foreach ($product['images'] as $product_image) {
				$image_link = $product_image['src'];
				$position = $product_image['position'];
				$label = $product_image['name'];
				$product_image_data[] = array(
				    'image_url' => $image_link,
				    'label' => $label,
				    'position' => $i,
				    'base_img' => $image_link,
				);
				$i++;
			    }
			}

			$product_data = array(
			    'product_id' => $p_id, // Stores/Channel product ID
			    'name' => $p_name, // Product name
			    'sku' => $p_sku, // Product SKU
			    'description' => $p_des, // Product Description
			    'product_url_path' => $product_url, // Product url if null give blank value
			    'weight' => $p_weight, // Product weight if null give blank value
			    'status' => $p_status, // Product status (visible or hidden). Give in 0 or 1 form (0 = hidden, 1 = visible).
			    'price' => $p_price, // Porduct price
			    'sale_price' => $p_saleprice, // Product sale price if null give Product price value
			    'qty' => $p_stk_lvl, //Product quantity 
			    'stock_status' => $p_avail, // Product stock status ("in stock" or "out of stock"). 
			    // Give in 0 or 1 form (0 = out of stock, 1 = in stock)
			    'websites' => array(), //This is for only magento give only and blank array
			    'brand' => '', // Product brand if any
			    'low_stock_notification' => 5, // Porduct low stock notification if any otherwise give default 5 value
			    'created_at' => $p_created_date, // Product created at date format date('Y-m-d H:i:s')
			    'updated_at' => $p_updated_date, // Product updated at date format date('Y-m-d H:i:s')
			    'mul_store_id' => $store_connection_id, //Give multiple store id
			    'mul_channel_id' => '', // Give multiple channel id
			    'channel_store_name' => 'WooCommerce', // Channel or Store name
			    'channel_store_prefix' => 'WOO', // Channel or store prefix id
			    'elliot_user_id' => $user_id, // Elliot user id
			    'store_id' => $woocommerce_store_id, // if your are importing store give store id
			    'channel_id' => '', // if your are importing channel give channel id
			    'country_code' => $store_country_code, // if your are importing channel give channel id
			    'currency_code' => $store_currency_code,
			    'conversion_rate' => $conversion_rate,
			    'barcode' => '', // Product barcode if any
			    'ean' => '', // Product ean if any
			    'jan' => '', // Product jan if any
			    'isban' => '', // Product isban if any
			    'mpn' => '', // Product mpn if any
			    'categories' => $product_categories_ids, // Product categroy array. If null give a blank array 
			    'images' => $product_image_data, // Product images data
			);

			Stores::productImportingCommon($product_data);
			//variations
			/* if (!empty($product['attributes'])) {
			  foreach ($product['attributes'] as $variation){
			  $var_name = $variation['name'];
			  //Check whelther Variation with the same name available
			  $checkVarModel = Variations::find()->where(['variation_name' => $var_name])->one();

			  if (empty($checkVarModel)){
			  //Create Model for each new Variation(Product Option) fetched
			  $variationModel = new Variations();
			  $variationModel->variation_name = $var_name;
			  $created_date = date('Y-m-d H:i:s', time());
			  $variationModel->created_at = $created_date;
			  //Save Elliot User id
			  $variationModel->elliot_user_id = $user_id;
			  if ($variationModel->save()){
			  $variation_id = $variationModel->variations_ID;
			  //Create Model for each new Variation Item(Product Option Value) fetched
			  if (!empty($variation['options'])){
			  foreach ($variation['options'] as $wc_att){

			  $variationItemModel = new VariationsItemList();
			  $variationItemModel->variation_id = $variation_id;
			  $variationItemModel->item_name = $var_name;
			  $variationItemModel->item_value = $wc_att;
			  $created_date = date('Y-m-d H:i:s', time());
			  $variationItemModel->created_at = $created_date;
			  //Save Elliot User id
			  $variationItemModel->elliot_user_id = $user_id;
			  $variationItemModel->save();
			  }
			  }
			  }
			  }
			  }
			  } */
			/* if (!empty($product['variations'])){
			  foreach ($product['variations'] as $product_sku){
			  //Woocommerce variation
			  //SKU
			  if (isset($product_sku['sku'])){
			  $newP_Var_Model = new ProductVariation;
			  $newP_Var_Model->product_id = $saved_product_id;
			  $newP_Var_Model->elliot_user_id = $user_id;
			  $newP_Var_Model->item_name = 'SKU';
			  $newP_Var_Model->item_value = $product_sku['sku'];
			  $newP_Var_Model->store_variation_id = 'WOO' . $product_sku['id'];
			  $created_date = date('Y-m-d H:i:s', time());
			  $newP_Var_Model->created_at = $created_date;
			  $newP_Var_Model->save(false);
			  }

			  //Price
			  if (isset($product_sku['price'])){
			  $newP_Var_Model = new ProductVariation;
			  $newP_Var_Model->product_id = $saved_product_id;
			  $newP_Var_Model->elliot_user_id = $user_id;
			  $newP_Var_Model->item_name = 'Price';
			  $newP_Var_Model->item_value = $product_sku['price'];
			  $newP_Var_Model->store_variation_id = 'WOO' . $product_sku['id'];
			  $created_date = date('Y-m-d H:i:s', time());
			  $newP_Var_Model->created_at = $created_date;
			  $newP_Var_Model->save(false);
			  }
			  //sale_price
			  if (isset($product_sku['sale_price'])){
			  $newP_Var_Model = new ProductVariation;
			  $newP_Var_Model->product_id = $saved_product_id;
			  $newP_Var_Model->elliot_user_id = $user_id;
			  $newP_Var_Model->item_name = 'Sales Price';
			  $newP_Var_Model->item_value = $product_sku['sale_price'];
			  $newP_Var_Model->store_variation_id = 'WOO' . $product_sku['id'];
			  $created_date = date('Y-m-d H:i:s', time());
			  $newP_Var_Model->created_at = $created_date;
			  $newP_Var_Model->save(false);
			  }
			  //Inventory
			  if (isset($product_sku['stock_quantity'])){
			  $newP_Var_Model = new ProductVariation;
			  $newP_Var_Model->product_id = $saved_product_id;
			  $newP_Var_Model->elliot_user_id = $user_id;
			  $newP_Var_Model->item_name = 'Stock Quantity';
			  $newP_Var_Model->item_value = $product_sku['stock_quantity'];
			  $newP_Var_Model->store_variation_id = 'WOO' . $product_sku['id'];
			  $created_date = date('Y-m-d H:i:s', time());
			  $newP_Var_Model->created_at = $created_date;
			  $newP_Var_Model->save(false);
			  }
			  //Weight
			  if (isset($product_sku['weight'])){
			  $newP_Var_Model = new ProductVariation;
			  $newP_Var_Model->product_id = $saved_product_id;
			  $newP_Var_Model->elliot_user_id = $user_id;
			  $newP_Var_Model->item_name = 'Weight';
			  $newP_Var_Model->item_value = $product_sku['weight'];
			  $newP_Var_Model->store_variation_id = 'BGC' . $product_sku['id'];
			  $created_date = date('Y-m-d H:i:s', time());
			  $newP_Var_Model->created_at = $created_date;
			  $newP_Var_Model->save(false);
			  }
			  //in_stock
			  if (isset($product_sku['in_stock'])){
			  if ($product_sku['in_stock'] == 1) {
			  $stock = 'In Stock';
			  } else {
			  $stock = 'Out of Stock';
			  }
			  $newP_Var_Model = new ProductVariation;
			  $newP_Var_Model->product_id = $saved_product_id;
			  $newP_Var_Model->elliot_user_id = $user_id;
			  $newP_Var_Model->item_name = 'Stock Status';
			  $newP_Var_Model->item_value = $stock;
			  $newP_Var_Model->store_variation_id = 'WOO' . $product_sku['id'];
			  $created_date = date('Y-m-d H:i:s', time());
			  $newP_Var_Model->created_at = $created_date;
			  $newP_Var_Model->save(false);
			  }
			  }
			  } */
		    }
		}
	    }
	}
    }

    public function getWoocommerceAllOrders($user_id, $woocommerce_store_id, $woocommerce, $woo_store_url, $woo_consumer_key, $woo_secret_key, $store_connection_id, $store_currency_code, $conversion_rate) {


	/* For Https server */
	$check_url = parse_url($woo_store_url);
	$url_protocol = $check_url['scheme'];
	if ($url_protocol == 'http') {
	    /* For Http Url */

	    $data = array();
	    for ($i = 1; $i <= 1000; $i++) {
		//if data is empty then loop break else data save
		$data = ['page' => $i, 'per_page' => 100];
		$orders_woocommerce = $woocommerce->get('orders', $data);

		if (empty($orders_woocommerce)) {
		    break;
		} else {

		    foreach ($orders_woocommerce as $orders_data) {
			//get WooCommerce Order id
			$woocommerce_order_id = $orders_data['id'];
			$customer_id = $orders_data['customer_id'];
			//Fetch Ship details
			$order_status = $orders_data['status'];
			$items_total = count($orders_data['line_items']);
			$product_qauntity = $items_total;
			//billing Address
			$billing_add1 = isset($orders_data['billing']['address_1']) ? $orders_data['billing']['address_1'] : "" . ',' . isset($orders_data['billing']['address_1']) ? $orders_data['billing']['address_1'] : "";
			$billing_add2 = isset($orders_data['billing']['city']) ? $orders_data['billing']['city'] : "" . ',' . isset($orders_data['billing']['state']) ? $orders_data['billing']['state'] : "" .
				',' . isset($orders_data['billing']['postcode']) ? $orders_data['billing']['postcode'] : "" . ',' . isset($orders_data['billing']['country']) ? $orders_data['billing']['country'] : "";
			$billing_address = $billing_add1 . ',' . $billing_add2;

			//billing Address
			$bill_street_1 = isset($orders_data['billing']['address_1']) ? $orders_data['billing']['address_1'] : '';
			$bill_street_2 = isset($orders_data['billing']['address_2']) ? $orders_data['billing']['address_2'] : '';
			$bill_city = isset($orders_data['billing']['city']) ? $orders_data['billing']['city'] : '';
			$bill_state = isset($orders_data['billing']['state']) ? $orders_data['billing']['state'] : '';
			$bill_zip = isset($orders_data['billing']['postcode']) ? $orders_data['billing']['postcode'] : '';
			$bill_country = isset($orders_data['billing']['country']) ? $orders_data['billing']['country'] : '';
			$bill_phone = isset($orders_data['billing']['phone']) ? $orders_data['billing']['phone'] : '';
			$bill_fname = isset($orders_data['billing']['first_name']) ? $orders_data['billing']['first_name'] : '';
			$bill_lname = isset($orders_data['billing']['last_name']) ? $orders_data['billing']['last_name'] : '';


			//Shipping Address
			$ship_street_1 = isset($ship_details['shipping']['address_1']) ? $ship_details['shipping']['address_1'] : $orders_data['billing']['address_1'];
			$ship_street_2 = isset($ship_details['shipping']['address_2']) ? $ship_details['shipping']['address_2'] : $orders_data['billing']['address_2'];
			$ship_city = isset($ship_details['shipping']['city']) ? $ship_details['shipping']['city'] : $orders_data['billing']['city'];
			$ship_state = isset($ship_details['shipping']['state']) ? $ship_details['shipping']['state'] : $orders_data['billing']['state'];
			$ship_zip = isset($ship_details['shipping']['postcode']) ? $ship_details['shipping']['postcode'] : $orders_data['billing']['postcode'];
			$ship_country = isset($ship_details['shipping']['country']) ? $ship_details['shipping']['country'] : $orders_data['billing']['country'];

			$shipping_address = $ship_street_1 . ',' . $ship_street_2 . ',' . $ship_city . ',' . $ship_state . ',' . $ship_zip . ',' . $ship_country;

			$total_amount = $orders_data['total'];
			$base_shipping_cost = $orders_data['shipping_total'];
			$shipping_cost_tax = $orders_data['shipping_tax'];
			$discount_amount = $orders_data['discount_total'];
			$payment_method = $orders_data['payment_method'];
			$payment_provider_id = $orders_data['transaction_id'];
			/* For refunded Amount */
			$refunded_amount = '';
			if (!empty($orders_data['refunds'])) {
			    foreach ($orders_data['refunds'] as $refund) {
				$refunded_amount = +$refund['total'];
			    }
			} else {
			    $refunded_amount = '';
			}
			/* End refunded Amount */
			/*
			 * Order status change according to status
			 */
			switch ($order_status) {
			    case "pending":
				$order_status = 'Pending';
				break;
			    case "processing":
				$order_status = "In Transit";
				break;
			    case "on-hold":
				$order_status = "On Hold";
				break;
			    case "completed":
				$order_status = "Completed";
				break;
			    case "cancelled":
				$order_status = "Cancel";
				break;
			    case "refunded":
				$order_status = "Refunded";
				break;
			    default:
				$order_status = "Pending";
			}

			$refunded_amount = $refunded_amount;
			$order_date = date('Y-m-d H:i:s', strtotime($orders_data['date_created']));
			$order_last_modified_date = date('Y-m-d H:i:s', strtotime($orders_data['date_modified']));
			$customer_email = isset($orders_data['billing']['email']) ? $orders_data['billing']['email'] : "";

			/* For order Items */
			$order_items = $orders_data['line_items'];
			$order_product_data = array();
			foreach ($order_items as $product_dtails_data) {
			    $product_id = $product_dtails_data['product_id'];
			    $title = $product_dtails_data['name'];
			    $sku = $product_dtails_data['sku'];
			    $price = $product_dtails_data['total'];
			    $quantity = $product_dtails_data['quantity'];
			    $product_weight = isset($product_dtails_data['weight']) ? $product_dtails_data['weight'] : 0;

			    $order_product_data[] = array(
				'product_id' => $product_id,
				'name' => $title,
				'sku' => $sku,
				'price' => $price,
				'qty_ordered' => $quantity,
				'weight' => $product_weight,
			    );
			}

			$order_data = array(
			    'order_id' => $woocommerce_order_id,
			    'mul_store_id' => $store_connection_id, //Give multiple store id
			    'mul_channel_id' => '', // Give multiple channel id
			    'status' => $order_status,
			    'magento_store_id' => '',
			    'order_grand_total' => $total_amount,
			    'customer_id' => $customer_id,
			    'customer_email' => $customer_email,
			    'order_shipping_amount' => $shipping_cost_tax,
			    'order_tax_amount' => $shipping_cost_tax,
			    'total_qty_ordered' => $product_qauntity,
			    'created_at' => $order_date,
			    'updated_at' => $order_last_modified_date,
			    'payment_method' => $payment_method,
			    'refund_amount' => $refunded_amount,
			    'discount_amount' => $discount_amount,
			    'channel_store_name' => 'WooCommerce',
			    'channel_store_prefix' => 'WOO',
			    'currency_code' => $store_currency_code,
			    'conversion_rate' => $conversion_rate,
			    'elliot_user_id' => $user_id,
			    'store_id' => $woocommerce_store_id,
			    'channel_id' => '',
			    'shipping_address' => array(
				'street_1' => $ship_street_1,
				'street_2' => $ship_street_2,
				'state' => $ship_state,
				'city' => $ship_city,
				'country' => $ship_country,
				'country_id' => '',
				'postcode' => $ship_zip,
				'email' => $customer_email,
				'telephone' => '',
				'firstname' => '',
				'lastname' => '',
			    ),
			    'billing_address' => array(
				'street_1' => $bill_street_1,
				'street_2' => $bill_street_2,
				'state' => $bill_state,
				'city' => $bill_city,
				'country' => $bill_country,
				'country_id' => '',
				'postcode' => $bill_zip,
				'email' => $customer_email,
				'telephone' => $bill_phone,
				'firstname' => $bill_fname,
				'lastname' => $bill_lname,
			    ),
			    'items' => $order_product_data,
			);
			Stores::orderImportingCommon($order_data);
		    }
		}
	    }

	    /* For Https url */
	} else {

	    $data = array();
	    for ($i = 1; $i <= 1000; $i++) {
		//if data is empty then loop break else data save

		$woocommerce_https = new Woocommerce(
			'' . $woo_store_url . '/wp-json/wc/v1/orders/?page=' . $i . '&per_page=100&consumer_key=' . $woo_consumer_key . '&consumer_secret=' . $woo_secret_key . '', $woo_consumer_key, $woo_secret_key, [
		    'wp_api' => true,
		    'version' => 'wc/v1',
		    "query_string_auth" => true,
			]
		);

		$data = ['page' => $i, 'per_page' => 100];
		$orders_woocommerce = $woocommerce_https->get('orders', $data);

		if (empty($orders_woocommerce)) {
		    break;
		} else {

		    foreach ($orders_woocommerce as $orders_data) {
			//get WooCommerce Order id
			$woocommerce_order_id = $orders_data['id'];
			$customer_id = $orders_data['customer_id'];
			//Fetch Ship details
			$order_status = $orders_data['status'];
			$items_total = count($orders_data['line_items']);
			$product_qauntity = $items_total;
			//billing Address
			$billing_add1 = isset($orders_data['billing']['address_1']) ? $orders_data['billing']['address_1'] : "" . ',' . isset($orders_data['billing']['address_1']) ? $orders_data['billing']['address_1'] : "";
			$billing_add2 = isset($orders_data['billing']['city']) ? $orders_data['billing']['city'] : "" . ',' . isset($orders_data['billing']['state']) ? $orders_data['billing']['state'] : "" .
				',' . isset($orders_data['billing']['postcode']) ? $orders_data['billing']['postcode'] : "" . ',' . isset($orders_data['billing']['country']) ? $orders_data['billing']['country'] : "";
			$billing_address = $billing_add1 . ',' . $billing_add2;

			//billing Address
			$bill_street_1 = isset($orders_data['billing']['address_1']) ? $orders_data['billing']['address_1'] : '';
			$bill_street_2 = isset($orders_data['billing']['address_2']) ? $orders_data['billing']['address_2'] : '';
			$bill_city = isset($orders_data['billing']['city']) ? $orders_data['billing']['city'] : '';
			$bill_state = isset($orders_data['billing']['state']) ? $orders_data['billing']['state'] : '';
			$bill_zip = isset($orders_data['billing']['postcode']) ? $orders_data['billing']['postcode'] : '';
			$bill_country = isset($orders_data['billing']['country']) ? $orders_data['billing']['country'] : '';
			$bill_phone = isset($orders_data['billing']['phone']) ? $orders_data['billing']['phone'] : '';
			$bill_fname = isset($orders_data['billing']['first_name']) ? $orders_data['billing']['first_name'] : '';
			$bill_lname = isset($orders_data['billing']['last_name']) ? $orders_data['billing']['last_name'] : '';


			//Shipping Address
			$ship_street_1 = isset($ship_details['shipping']['address_1']) ? $ship_details['shipping']['address_1'] : $orders_data['billing']['address_1'];
			$ship_street_2 = isset($ship_details['shipping']['address_2']) ? $ship_details['shipping']['address_2'] : $orders_data['billing']['address_2'];
			$ship_city = isset($ship_details['shipping']['city']) ? $ship_details['shipping']['city'] : $orders_data['billing']['city'];
			$ship_state = isset($ship_details['shipping']['state']) ? $ship_details['shipping']['state'] : $orders_data['billing']['state'];
			$ship_zip = isset($ship_details['shipping']['postcode']) ? $ship_details['shipping']['postcode'] : $orders_data['billing']['postcode'];
			$ship_country = isset($ship_details['shipping']['country']) ? $ship_details['shipping']['country'] : $orders_data['billing']['country'];

			$shipping_address = $ship_street_1 . ',' . $ship_street_2 . ',' . $ship_city . ',' . $ship_state . ',' . $ship_zip . ',' . $ship_country;

			$total_amount = $orders_data['total'];
			$base_shipping_cost = $orders_data['shipping_total'];
			$shipping_cost_tax = $orders_data['shipping_tax'];
			$discount_amount = $orders_data['discount_total'];
			$payment_method = $orders_data['payment_method'];
			$payment_provider_id = $orders_data['transaction_id'];
			/* For refunded Amount */
			$refunded_amount = '';
			if (!empty($orders_data['refunds'])) {
			    foreach ($orders_data['refunds'] as $refund) {
				$refunded_amount = +$refund['total'];
			    }
			} else {
			    $refunded_amount = '';
			}
			/* End refunded Amount */
			/*
			 * Order status change according to status
			 */
			switch ($order_status) {
			    case "pending":
				$order_status = 'Pending';
				break;
			    case "processing":
				$order_status = "In Transit";
				break;
			    case "on-hold":
				$order_status = "On Hold";
				break;
			    case "completed":
				$order_status = "Completed";
				break;
			    case "cancelled":
				$order_status = "Cancel";
				break;
			    case "refunded":
				$order_status = "Refunded";
				break;
			    default:
				$order_status = "Pending";
			}

			$refunded_amount = $refunded_amount;
			$order_date = date('Y-m-d H:i:s', strtotime($orders_data['date_created']));
			$order_last_modified_date = date('Y-m-d H:i:s', strtotime($orders_data['date_modified']));
			$customer_email = isset($orders_data['billing']['email']) ? $orders_data['billing']['email'] : "";

			/* For order Items */
			$order_items = $orders_data['line_items'];
			$order_product_data = array();
			foreach ($order_items as $product_dtails_data) {
			    $product_id = $product_dtails_data['product_id'];
			    $title = $product_dtails_data['name'];
			    $sku = $product_dtails_data['sku'];
			    $price = $product_dtails_data['total'];
			    $quantity = $product_dtails_data['quantity'];
			    $product_weight = isset($product_dtails_data['weight']) ? $product_dtails_data['weight'] : 0;

			    $order_product_data[] = array(
				'product_id' => $product_id,
				'name' => $title,
				'sku' => $sku,
				'price' => $price,
				'qty_ordered' => $quantity,
				'weight' => $product_weight,
			    );
			}

			$order_data = array(
			    'order_id' => $woocommerce_order_id,
			    'mul_store_id' => $store_connection_id, //Give multiple store id
			    'mul_channel_id' => '', // Give multiple channel id
			    'status' => $order_status,
			    'magento_store_id' => '',
			    'order_grand_total' => $total_amount,
			    'customer_id' => $customer_id,
			    'customer_email' => $customer_email,
			    'order_shipping_amount' => $shipping_cost_tax,
			    'order_tax_amount' => $shipping_cost_tax,
			    'total_qty_ordered' => $product_qauntity,
			    'created_at' => $order_date,
			    'updated_at' => $order_last_modified_date,
			    'payment_method' => $payment_method,
			    'refund_amount' => $refunded_amount,
			    'discount_amount' => $discount_amount,
			    'channel_store_name' => 'WooCommerce',
			    'channel_store_prefix' => 'WOO',
			    'currency_code' => $store_currency_code,
			    'conversion_rate' => $conversion_rate,
			    'elliot_user_id' => $user_id,
			    'store_id' => $woocommerce_store_id,
			    'channel_id' => '',
			    'shipping_address' => array(
				'street_1' => $ship_street_1,
				'street_2' => $ship_street_2,
				'state' => $ship_state,
				'city' => $ship_city,
				'country' => $ship_country,
				'country_id' => '',
				'postcode' => $ship_zip,
				'email' => $customer_email,
				'telephone' => '',
				'firstname' => '',
				'lastname' => '',
			    ),
			    'billing_address' => array(
				'street_1' => $bill_street_1,
				'street_2' => $bill_street_2,
				'state' => $bill_state,
				'city' => $bill_city,
				'country' => $bill_country,
				'country_id' => '',
				'postcode' => $bill_zip,
				'email' => $customer_email,
				'telephone' => $bill_phone,
				'firstname' => $bill_fname,
				'lastname' => $bill_lname,
			    ),
			    'items' => $order_product_data,
			);
			Stores::orderImportingCommon($order_data);
		    }
		}
	    }
	}
    }

    public function actionProductxml() {

	\Yii::$app->response->format = \yii\web\Response::FORMAT_XML;
	$users = Products::find('product_name', 'SKU', 'description')->one();
	return $users;


//         \Yii::$app->response->format = \yii\web\Response::FORMAT_XML;
//        $product_data=Products::find()->all();
//        echo'<pre>';
//        print_r($product_data);
//        die;
    }

    public function actionTest() {

	$currency = 'INR';
	echo Stores::getCurrencyConversionRate($currency, 'USD');
    }

    function actionShopifycron() {


	//$file = $_SERVER['DOCUMENT_ROOT'] . '/shopify.txt';
	//$fwrite = fopen($file, "w");
	//fwrite($fwrite, "from cron");
	//fclose($fwrite);
    }

}
