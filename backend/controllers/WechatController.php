<?php

namespace backend\controllers;

use Yii;
use backend\models\User;
use backend\models\Channels;
use backend\models\ChannelSettings;
use backend\models\ChannelsSearch;
use backend\models\ChannelConnection;
use backend\models\CustomerUser;
use backend\models\CustomerAbbrivation;
use yii\web\Controller;
use backend\controllers\IntegrationsController as Integrations;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\StoresConnection;
use backend\models\Stores;
use backend\models\Categories;
use backend\models\CategoryAbbrivation;
use backend\models\Products;
use backend\models\ProductImages;
use backend\models\ProductChannel;
use backend\models\MerchantProducts;
use backend\models\ProductAbbrivation;
use backend\models\Variations;
use backend\models\ProductVariation;
use backend\models\VariationsItemList;
use backend\models\ProductCategories;
use backend\models\Orders;
use backend\models\OrdersProducts;
use backend\models\OrderChannel;
use backend\models\Notification;
use backend\models\CronTasks;
use Bigcommerce\Api\Client as Bigcommerce;
use yii\helpers\Html;
use backend\models\MagentoStores;
use backend\models\Countries;
use backend\models\StoreDetails;

/**
 * ChannelsController implements the CRUD actions for Channels model.
 */
class WechatController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'wechat', 'flipkartconnect', 'wechatconnect', 'wechatimport', 'test'],
                'rules' => [
                    [
                        'actions' => ['signup', 'wechatimport', 'test'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'index', 'view', 'create', 'update', 'wechat', 'flipkartconnect', 'wechatconnect', 'wechatimport', 'test'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionWechatquickconnect() {
        $returnResult = array();

        $appID = $_POST['user'];
        $appSecret = $_POST['password'];

        $ch = curl_init('https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=' . $appID . '&secret=' . $appSecret);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

        // execute!
        $response = curl_exec($ch);
        $response = json_decode($response, true);

        curl_close($ch);

        if (isset($response['errmsg'])) {
            $returnResult['status'] = 0;
            $returnResult['msg'] = $response['errmsg'];
            echo json_encode($returnResult);
            return;
        }

        $access_token = $response['access_token'];
        $user_id = Yii::$app->user->identity->id;

        $user = User::find()->where(['id' => $user_id])->one();

        $type = $_POST['type'];
        $channel = Channels::find()->select( 'channel_ID')->where(['channel_name' => $type, 'parent_name' => 'channel_WeChat'])->one();
        $wechat_channel_id = $channel->channel_ID;
        $checkConnection = ChannelConnection::find()->where(['channel_id' => $wechat_channel_id, 'user_id' => $user_id])->one();
        if (empty($checkConnection))
        {
            $connectionModel = new ChannelConnection();
            $connectionModel->channel_id = $wechat_channel_id;
            $connectionModel->user_id = $user_id;
            $connectionModel->connected = 'Yes';
            $connectionModel->tmall_account = '';
            $connectionModel->tmall_app_key = $appID;
            $connectionModel->tmall_app_secret = $appSecret;
            $created_date = date('Y-m-d h:i:s', time());
            $connectionModel->created = $created_date;
            $connectionModel->save();
        }

        $categoryResult = $this->getWeChatCategories($access_token);
        $category_response = json_decode(json_encode($categoryResult), true);
        $this->wechatCategoryImporting($category_response, $user_id);
        $productResult = $this->getWeChatProducts($access_token);
        $this->wechatProductImporting($productResult, $access_token, $appID, $appSecret, $user_id, $type);
        $orderResult = $this->getWeChatOrders($access_token);
        $this->wechatOrderImporting($orderResult, $access_token, $appID, $appSecret, $user_id, $type);

        // close the connection, release resources used
        $this->render('wechatquickconnect');
    }

    public function wechatProductImporting($productResult, $token, $appID, $appSecret, $user_id, $type) {
        $category_list = CategoryAbbrivation::find()->select('channel_abb_id')->where(['channel_accquired'=>'WeChat'])->all();
        $count = 1;
        $product_ids = array();

        foreach($category_list as $cat)
        {
            $cat_id = substr($cat->channel_abb_id, 2);
            $_from = 1;
            $_to = 50;

        }

        $product_datas = $productResult['products_info'];
        $total_product_count = sizeof($product_datas);
        //$total_loop = ceil($total_product_count / 50);
        for($pro = 0; $pro < $total_product_count; $pro++) {
            $product = $product_datas[$pro];
            $product_ids[] = $product["product_id"];
        }

        $brand_list = array();

        sort($product_ids);
        $cc = 1;
        foreach($product_datas as $product_data)
        {
            $cc++;

            $product_id = $product_data['product_id'];
            $prefix_product_id = 'WC'.$product_id;
            $name = $product_data['product_base']['name'];
            $category_id = $product_data['product_base']['category_id'][0];
            //$description = $product_data['item']['desc'];
            $created_at = date("Y-m-d H:i:s");
            $product_visible_status = 1;
            //$product_active_status = $product_data['IsActive'];
            $brand_name = '';

            //$product_sku_data = $product_data['item']['skus'];
            //$product_outer_id = $product_data['item']['outer_id'];
            //print_r($product_data); return;

            $count = 1;
            $product_sku_data = $product_data['sku_list'];

            foreach($product_sku_data as $_product_sku)
            {
                $sku_id = $_product_sku['sku_id'];
                $sku_name = $name;
                $product_price = $_product_sku['price'] / 100;
                //$product_image_url = $_product_sku['image'];
                $product_image_url = $product_data['product_base']['main_img'];
                $product_stock = 0;
                $product_qty = $_product_sku['quantity'];
                //$product_weight = $_product_sku['measures']['cubicweight'];
                $product_weight = $product_data['delivery_info']['weight'];
                if($count == 1)
                {
                    $checkProductModel = Products::find()->where(['product_name' => $sku_name, 'sku' => $sku_id])->one();
                    if (empty($checkProductModel))
                    {
                        //Create Model for Each new Product
                        $productModel = new Products ();
                        $productModel->channel_abb_id = "";
                        $productModel->product_name = $sku_name;
                        $productModel->SKU = $sku_id;
                        $productModel->UPC = '';
                        $productModel->EAN = '';
                        $productModel->Jan = '';
                        $productModel->ISBN = '';
                        $productModel->MPN = '';
                        //$productModel->description = $description;
                        $productModel->adult = 'no';
                        $productModel->age_group = NULL;
                        $productModel->gender = 'Unisex';
                        if ($product_stock > 0):
                            $productModel->availability = 'In Stock';
                        else:
                            $productModel->availability = 'Out of Stock';
                        endif;
                        $productModel->brand = $brand_name;
                        $productModel->condition = 'New';
                        $productModel->weight = $product_weight;
                        $productModel->stock_quantity = $product_qty;
                        if ($product_stock > 0):
                            $productModel->stock_level = 'In Stock';
                        else:
                            $productModel->stock_level = 'Out of Stock';
                        endif;
                        if ($product_visible_status == 1):
                            $productModel->stock_status = 'Visible';
                        else:
                            $productModel->stock_status = 'Hidden';
                        endif;
                        $productModel->price = $product_price;
                        $productModel->sales_price = $product_price;
                        $productModel->created_at = $created_at;
                        $productModel->updated_at = date('Y-m-d h:i:s', time());
                        //Save Elliot User id
                        $productModel->elliot_user_id = $user_id;
                        if ($productModel->save(false)):
                            /* Save Product Abbrivation Id */
                            $last_pro_id = $productModel->id;
                            $product_abberivation = new ProductAbbrivation();
                            $product_abberivation->channel_abb_id = $prefix_product_id;
                            $product_abberivation->product_id = $last_pro_id;
                            $product_abberivation->channel_accquired = 'WeChat';
                            $product_abberivation->created_at = date('Y-m-d H:i:s', time());
                            $product_abberivation->save(false);
                        endif;

                        if (!empty($category_id)):
                            $category = CategoryAbbrivation::find()->where(['channel_abb_id' => 'WC'.$category_id])->one();
                            if (!empty($category)):
                                $cat_id = $category->category_ID;
                                $productCategoryModel = new ProductCategories;
                                $productCategoryModel->category_ID = $cat_id;
                                $productCategoryModel->product_ID = $last_pro_id;
                                $productCategoryModel->created_at = $created_at;
                                $productCategoryModel->updated_at = date('Y-m-d h:i:s', time());
                                //Save Elliot User id
                                $productCategoryModel->elliot_user_id = $user_id;
                                $productCategoryModel->save(false);
                            endif;
                        endif;
                        //Product Images Table Entry
                        if ($product_image_url!=''):
                            $productImageModel = new ProductImages;
                            $productImageModel->product_ID = $last_pro_id;
                            $productImageModel->label = '';
                            $productImageModel->link = $product_image_url;
                            $productImageModel->default_image = 'Yes';
                            $productImageModel->image_status = 1;
                            $productImageModel->created_at = $created_at;
                            $productImageModel->updated_at = date('Y-m-d h:i:s', time());
                            $productImageModel->save(false);
                        endif;

                        $get_wechat_id = Channels::find()->select('channel_ID')->where(['channel_name' => $type, 'parent_name' => 'channel_WeChat'])->one();
                        $wechat_channel_id = $get_wechat_id->channel_ID;

                        $productChannelModel = new ProductChannel;
                        $productChannelModel->channel_id = $wechat_channel_id;
                        $productChannelModel->product_id = $last_pro_id;
                        $productChannelModel->created_at = $created_at;
                        $productChannelModel->status = 'yes';
                        $productChannelModel->updated_at = date('Y-m-d h:i:s', time());
                        $productChannelModel->save(false);
                    }
                    else
                    {
                        $check_abbrivation =  ProductAbbrivation::find()->where(['channel_abb_id' => $prefix_product_id])->one();
                        if(empty($check_abbrivation))
                        {
                            /* Save Product Abbrivation Id */
                            $product_abberivation = new ProductAbbrivation();
                            $product_abberivation->channel_abb_id = $prefix_product_id;
                            $product_abberivation->product_id = $checkProductModel->id;
                            $product_abberivation->channel_accquired = 'WeChat';
                            $product_abberivation->created_at = date('Y-m-d H:i:s', time());
                            $product_abberivation->save(false);


                            $get_wechat_id = Channels::find()->select('channel_ID')->where(['channel_name' => $type, 'parent_name' => 'channel_WeChat'])->one();
                            $wechat_channel_id = $get_wechat_id->channel_ID;

                            //Product Channel/Store Table Entry
                            $productChannelModel = new ProductChannel;
                            $productChannelModel->channel_id = $wechat_channel_id;
                            $productChannelModel->product_id = $checkProductModel->id;
                            $productChannelModel->status = 'yes';
                            $productChannelModel->created_at = date('Y-m-d H:i:s', time());
                            $productChannelModel->save(false);
                        }
                    }
                }
                $count++;
            }

        }
    }

    public function wechatCategoryImporting($category_list, $user_id)
    {
        $category_list = $category_list["groups_detail"];
        foreach($category_list as $cat)
        {
            $cat_id = $cat['group_id'];
            $cat_name = $cat['group_name'];
            $checkCategoryName = Categories::find()->where(['category_name' => $cat_name])->one();
            if(empty($checkCategoryName))
            {
                $categoryModel = new Categories();
                $categoryModel->channel_abb_id = '';
                $categoryModel->category_name = $cat_name;
                $categoryModel->parent_category_ID = 0;
                $categoryModel->elliot_user_id = $user_id;
                $created_date = date('Y-m-d h:i:s', time());
                $categoryModel->created_at = $created_date;
                if($categoryModel->save())
                {
                    $CategoryAbbrivation = new CategoryAbbrivation();
                    $CategoryAbbrivation->channel_abb_id = 'WC'.$cat_id;
                    $CategoryAbbrivation->category_ID = $categoryModel->category_ID;
                    $CategoryAbbrivation->channel_accquired = 'WeChat';
                    $CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
                    $CategoryAbbrivation->save(false);
                }
            }
            else
            {
                $CategoryAbbrivation = new CategoryAbbrivation();
                $CategoryAbbrivation->channel_abb_id = 'WC'.$cat_id;
                $CategoryAbbrivation->category_ID = $checkCategoryName->category_ID;
                $CategoryAbbrivation->channel_accquired = 'WeChat';
                $CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
                $CategoryAbbrivation->save(false);
            }

        }
    }

    function getWeChatCategories($token) {
        $url = 'https://api.weixin.qq.com/merchant/group/getall?access_token=' . $token;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => FALSE,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array('Content-Type:application/x-www-form-urlencoded')
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #: " . $err;
        } else {
            return json_decode($response, true);
        }
        return $response;

    }

    function getWeChatOrders($token) {
        $url = 'https://api.weixin.qq.com/merchant/order/getbyfilter?access_token=' . $token;

        $data = "status = 2&begintime=".strtotime('2000-01-01')."&endtime=".strtotime('2017-09-20');

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => FALSE,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array('Content-Type:application/x-www-form-urlencoded')
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #: " . $err;
        } else {
            return json_decode($response, true);
        }
        return $response;
    }

    function getWeChatProducts($token) {
        $url = 'https://api.weixin.qq.com/merchant/getbystatus?access_token=' . $token;

        $data = "status = 0";

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => FALSE,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array('Content-Type:application/x-www-form-urlencoded')
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #: " . $err;
        } else {
            return json_decode($response, true);
        }
        return $response;
    }
    /**
     * Intigrate wechat store with the system
     */

    public function wechatOrderImporting($orderResult, $access_token, $appID, $appSecret, $user_id, $type) {
        $order_trades = $orderResult['order_list'];

        foreach($order_trades as $order_collection)
        {
            $order_id = $order_collection['order_id'];
//            $origin = $order_collection['origin'];
//            $salesChannel = $order_collection['salesChannel'];
            $order_status = $order_collection['order_status'];
            $order_total = $order_collection['order_total_price'];
            $order_created_at = date("Y-m-d h:i:s", strtotime($order_collection['order_create_time']));
            $order_updated_at = date("Y-m-d h:i:s", strtotime($order_collection['order_create_time']));
            $group_price = $order_collection['order_express_price'];

            switch ($order_status) {
                case 6:
                    $order_status_2 = "Pending";
                    break;
                case 8:
                    $order_status_2 = 'Pending';
                    break;
                default:
                    $order_status_2 = "Pending";
            }

            $discount_price = $group_price;
            $shipping_price = $group_price;
            $tax_price = $group_price;

            //$payment_method = $order_collection['paymentData']['transactions'][0]['payments'][0]['paymentSystemName'];
            $payment_method = '';


            $firstName = $order_collection['receiver_name'];
            $email = '';
            $lastName = '';
            $phone = $order_collection['receiver_mobile'];
            $wechat_customer_profile_id = '';

            $receiver_name = $order_collection['receiver_name'];
            $city = $order_collection['receiver_city'];

            $shipping_address = $order_collection['receiver_address'];
            $billing_address = $order_collection['receiver_address'];

            $total_quantity_ordered = $order_collection['product_count'];

            $customerCheckModel = CustomerUser::find()->Where(['channel_abb_id' => 'WC'.$wechat_customer_profile_id])->one();
            if(empty($customerCheckModel))
            {
                if($wechat_customer_profile_id == '')
                {
                    $wechat_customer_profile_id = '0000';
                }
                $customerCheckAnotherModel = CustomerUser::find()->Where(['channel_abb_id' => 'WC'.$wechat_customer_profile_id, 'email'=>$email])->one();
                if(empty($customerCheckAnotherModel))
                {
                    $Customers_create_model = new CustomerUser();
                    $Customers_create_model->channel_abb_id = 'WC'.$wechat_customer_profile_id;
                    $Customers_create_model->first_name = $firstName;
                    $Customers_create_model->last_name = $lastName;
                    $Customers_create_model->email = $email;
                    $Customers_create_model->channel_acquired = 'WeChat';
                    $Customers_create_model->date_acquired = '';
                    $Customers_create_model->created_at = date('Y-m-d h:i:s', time());
                    $Customers_create_model->elliot_user_id = $user_id;
                    $Customers_create_model->save(false);
                    $elliot_customer_id = $Customers_create_model->customer_ID;
                }
                else
                {
                    $elliot_customer_id = $customerCheckAnotherModel->customer_ID;
                }
            }
            else
            {
                $customerCheckModel->street_1 = '';
                $customerCheckModel->street_2 = '';
                $customerCheckModel->city = $city;
                $customerCheckModel->country = '';
                $customerCheckModel->state = '';
                $customerCheckModel->zip = '';
                $customerCheckModel->phone_number = $phone;
                $customerCheckModel->ship_street_1 = '';
                $customerCheckModel->ship_street_2 = '';
                $customerCheckModel->ship_city = $city;
                $customerCheckModel->ship_state = '';
                $customerCheckModel->ship_zip = '';
                $customerCheckModel->ship_country = '';
                $customerCheckModel->save(false);
                $elliot_customer_id = $customerCheckModel->customer_ID;
            }

            $check_order = Orders::find()->Where(['channel_abb_id' => 'WC'.$order_id])->one();
            if(empty($check_order))
            {
                $order_model = new Orders();
                $order_model->elliot_user_id = $user_id;
                $order_model->channel_abb_id = 'WC' . $order_id;
                $order_model->customer_id = $elliot_customer_id;
                $order_model->order_status = $order_status_2;
                $order_model->product_qauntity = $total_quantity_ordered;
                $order_model->shipping_address = $shipping_address;
                $order_model->ship_street_1 = '';
                $order_model->ship_street_2 = '';
                $order_model->ship_city = $city;
                $order_model->ship_state = '';
                $order_model->ship_zip = '';
                $order_model->ship_country = '';
                $order_model->billing_address = $billing_address;
                $order_model->bill_street_1 = '';
                $order_model->bill_street_2 = '';
                $order_model->bill_city = $city;
                $order_model->bill_state = '';
                $order_model->bill_zip = '';
                $order_model->bill_country = '';
                $order_model->base_shipping_cost = $shipping_price;
                $order_model->shipping_cost_tax = $tax_price;
                $order_model->payment_method = $payment_method;
                $order_model->payment_status = $order_status_2;
                $order_model->refunded_amount = '';
                $order_model->discount_amount = $discount_price;
                $order_model->total_amount = $order_total;
                $order_model->order_date = $order_created_at;
                $order_model->updated_at = $order_updated_at;
                //$order_model->created_at = date('Y-m-d h:i:s', time());
                $order_model->created_at = $order_created_at;
                $order_model->save(false);
                $last_order_id = $order_model->order_ID;

                $wechat_channel = Channels::find()->where(['channel_name' => $type, 'parent_name' => 'channel_WeChat'])->one();
                $wechat_channel_id = $wechat_channel->channel_ID;
                $order_channels_model = new OrderChannel();
                $order_channels_model->elliot_user_id = $user_id;
                $order_channels_model->order_id = $last_order_id;
                $order_channels_model->channel_id = $wechat_channel;
                $order_channels_model->created_at = date('Y-m-d h:i:s', time());
                $order_channels_model->save(false);

                    $product_id = $order_collection['product_id'];
                    $quantity_ordered = $order_collection['product_count'];
                    $product_name = $order_collection['product_name'];
                    $product_price = $order_collection['product_price'];
                    $product_sku = $order_collection['product_sku'];

                    $checkProductId = Products::find()->where(['product_name' => $product_name, 'sku' => $product_sku])->one();
                    if(empty($checkProductId))
                    {
                        if($product_id=='')
                        {
                            $product_id = '0000';
                        }
                        $productModel = new Products ();
                        $productModel->channel_abb_id = '';
                        $productModel->product_name = $product_name;
                        $productModel->SKU = $product_sku;
                        $productModel->UPC = '';
                        $productModel->EAN = '';
                        $productModel->Jan = '';
                        $productModel->ISBN = '';
                        $productModel->MPN = '';
                        $productModel->description = '';
                        $productModel->adult = 'no';
                        $productModel->age_group = NULL;
                        $productModel->gender = 'Unisex';
                        $productModel->brand = '';
                        $productModel->stock_quantity = 1;
                        $productModel->availability = 'Out of Stock';
                        $productModel->stock_level = 'Out of Stock';
                        $productModel->stock_status = 'Visible';
                        $productModel->price = $product_price;
                        $productModel->sales_price = $product_price;
                        $productModel->elliot_user_id = $user_id;
                        $productModel->save(false);
                        $last_pro_id = $productModel->id;

                        $custom_productChannelModel = new ProductChannel;
                        $channel_id = Channels::find()->select('channel_ID')->where(['channel_name' => $type, 'parent_name' => 'channel_WeChat'])->one();
                        $custom_productChannelModel->channel_id = $channel_id->channel_ID;
                        $custom_productChannelModel->product_id = $last_pro_id;
                        $custom_productChannelModel->elliot_user_id = $user_id;
                        $custom_productChannelModel->status = 'no';
                        $custom_productChannelModel->created_at = date('Y-m-d h:i:s', time());
                        $custom_productChannelModel->save(false);

                        /* Save Product Abbrivation Id */
                        $product_abberivation = new ProductAbbrivation();
                        $product_abberivation->channel_abb_id = 'WC'.$product_id;
                        $product_abberivation->product_id = $last_pro_id;
                        $product_abberivation->channel_accquired = 'WeChat';
                        $product_abberivation->created_at = date('Y-m-d H:i:s', time());
                        $product_abberivation->save(false);

                    }
                    else
                    {
                        $last_pro_id = $checkProductId->id;
                    }

                    $order_products_model = new OrdersProducts;
                    $order_products_model->order_Id = $last_order_id;
                    $order_products_model->product_Id = $last_pro_id;
                    $order_products_model->qty = $quantity_ordered;
                    $order_products_model->order_product_sku = $product_sku;
                    $order_products_model->created_at = date('Y-m-d h:i:s', time());
                    $order_products_model->elliot_user_id = $user_id;
                    $order_products_model->save(false);
                }
        }

    }
}
