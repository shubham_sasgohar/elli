<?php

namespace backend\controllers;

use Yii;
use backend\models\User;
use backend\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create','update','savesubprofile'],
                'rules' => [
                        [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                        [
                        'actions' => ['logout', 'index', 'view', 'create','update','savesubprofile'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new User();
        $users_Id = Yii::$app->user->identity->id;
        $merchant_data = User::find()->Where(['parent_id' => $users_Id])->all();

        if ($model->load(Yii::$app->request->post())) {

            $email = $model->email;
            $first_name = $model->first_name;
            $user_name = $model->first_name;
            $passHash = $model->setPassword('123456');
            $model->auth_key = $model->generateAuthKey();
            $model->password_hash = $passHash;
            $model->parent_id = $users_Id;
            $model->company_name=Yii::$app->user->identity->company_name;
            
             if ($model->save(false)) {
                Yii::$app->mailer->compose()
                        ->setFrom('mail@elliot.com')
                        ->setTo($email)
                        ->setSubject('Your temporary password')
//                    ->setTextBody('123456')
                        ->setHtmlBody('<h2>Hi ' . $user_name . '<h2>'
                                . '<h2>Your New Password Is :<b>123456</b> :</h2>'
                                . '<h3>Please make sure you go to our site to login with your username and temporary password.</h3>'
                                . '')
                        ->send();
                Yii::$app->session->setFlash('success', 'Success! User Created Successfully');
                return $this->render('create', [
                            'model' => $model,
                ]);
            } else {
                Yii::$app->session->setFlash('danger', 'Error! User are not Created Successfully');
                return $this->render('create', [
                            'model' => $model,
                ]);
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    public function actionSavesubprofile() {


        $request = Yii::$app->request;
        $userid = Yii::$app->request->post('sub_user_id');

        if(Yii::$app->request->post()) {

        $userdata = User::find()->Where(['id' => $userid])->one();
        $userdata->first_name = Yii::$app->request->post('sub_firstname'); 
        $userdata->last_name = Yii::$app->request->post('sub_lastname');
        $userdata->email = Yii::$app->request->post('sub_email');
        $userdata->role = Yii::$app->request->post('sub_role');

        }

        if ($userdata->save(false)) {
                $response = ['status' => 'success', 'data' => 'profile info saved'];
                Yii::$app->session->setFlash('success', 'Success! Profile info has been updated.');
                return $this->redirect(['view', 'id' => $userid]);
            } else {

                $response = ['status' => 'error', 'data' => 'profile info not saved'];
                Yii::$app->session->setFlash('danger', 'Error! Profile info has not been updated.');

            }
        echo json_encode($response);
        exit;


    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
