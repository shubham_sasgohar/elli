<?php

namespace backend\controllers;

ob_start();

//session_start();

use Yii;
use backend\models\CustomerUser;
use backend\models\User;
use backend\models\CustomerUserSearch;
use backend\models\Stores;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\Countries;
use backend\models\States;
use backend\models\Cities;
use backend\models\StoresConnection;
use Bigcommerce\Api\Client as Bigcommerce;
use backend\models\Orders;
use backend\models\OrdersProducts;
use backend\models\OrderChannel;
use backend\models\Products;
use backend\models\Categories;
use backend\models\ProductCategories;
use backend\models\ProductChannel;
use backend\models\ProductImages;
use Bigcommerce\Api\ShopifyClient as Shopify;
use backend\models\ProductVariation;
use backend\models\VariationsSet;
use backend\models\Variations;
use backend\models\VariationsItemList;
use backend\models\MerchantProducts;
use Automattic\WooCommerce\Client as Woocommerce;
use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use Facebook\FacebookRequestException;
use Facebook\FacebookRedirectLoginHelper;
use backend\models\ProductAbbrivation;
use yii\helpers\ArrayHelper;
use backend\models\CurrencyConversion;

class LazadaController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
	return [
	    'access' => [
		'class' => AccessControl::className(),
		'only' => ['view', 'create', 'update', 'savecustomer', 'create-customer-hook', 'orderbigcommerce', 'orderhookcreate', 'producthooksbg', 'skuupdategb', 'productskuhooksbg', 'categoryhooksbg', 'shopify-product-create', 'shopify-product-update', 'shopify-product-delete', 'shopify-order-create', 'shopify-order-paid', 'shopify-order-update', 'shopify-customer-create', 'shopify-customer-update', 'sku', 'facebook', 'lazada', 'flipkart', 'malaysia-importing', 'categorydelete'],
		'rules' => [
			[
			'actions' => ['signup', 'create-customer-hook', 'orderbigcommerce', 'orderhookcreate', 'producthooksbg', 'skuupdategb', 'productskuhooksbg', 'categoryhooksbg', 'shopify-product-create', 'shopify-product-update', 'shopify-product-delete', 'shopify-order-create', 'shopify-order-paid', 'shopify-order-update', 'shopify-customer-create', 'shopify-customer-update', 'sku', 'index', 'malaysia-importing'],
			'allow' => true,
			'roles' => ['?'],
		    ],
			[
			'actions' => ['logout', 'index', 'view', 'create', 'update', 'savecustomer', 'create-customer-hook', 'orderbigcommerce', 'orderhookcreate', 'producthooksbg', 'skuupdategb', 'productskuhooksbg', 'categoryhooksbg', 'shopify-product-create', 'shopify-product-update', 'shopify-product-delete', 'shopify-order-create', 'shopify-order-paid', 'shopify-order-update', 'shopify-customer-create', 'shopify-customer-update', 'sku', 'facebook', 'lazada', 'flipkart', 'malaysia-importing', 'categorydelete'],
			'allow' => true,
			'roles' => ['@'],
		    ],
		],
	    ],
	    'verbs' => [
		'class' => VerbFilter::className(),
		'actions' => [
		    'delete' => ['POST'],
		],
	    ],
	];
    }

    /**
     * Lists all CustomerUser models.
     * @return mixed
     */
    public function beforeAction($action) {

	$this->enableCsrfValidation = false;


	return parent::beforeAction($action);
    }

    function flatten($element) {
	$flatArray = array();
	foreach ($element as $key => $node) {
	    if (array_key_exists('children', $node)) {
		$flatArray = array_merge($flatArray, $this->flatten($node['children']));
		unset($node['children']);
		$flatArray[] = $node;
	    } else {
		$flatArray[] = $node;
	    }
	}


	return $flatArray;
    }

    public function actionAuthLazadaMalaysia($type = null) {


	if (empty($_GET['type']))
	    die('Not Allowed');

	$channel_type = '';
	switch ($type) {
	    case 'Malaysia':
		$channel_type = 'Lazada Malaysia';
		break;
	    case 'Singapore':
		$channel_type = 'Lazada Singapore';
		break;
	    case 'Indonesia':
		$channel_type = 'Lazada Indonesia';
		break;
	    case 'Thailand':
		$channel_type = 'Lazada Thailand';
		break;
	    case 'Vietnam':
		$channel_type = 'Lazada Vietnam';
		break;

	    default:
		break;
	}

	$lazada = \backend\models\Channels::find()->where(['channel_name' => $channel_type])->one();
//echo'<pre>';
//print_r($lazada);die;
	$channel_id = $lazada->channel_ID;

	if (isset($_POST) and ! empty($_POST)) {
	    $request_data = Yii::$app->request->post();
	    date_default_timezone_set("UTC");
	    $user_id = Yii::$app->user->identity->id;

	    $channel_connected = \backend\models\ChannelConnection::find()->where(['channel_id' => $request_data['channel_id'], 'user_id' => $user_id])->one();
	    if (empty($channel_connected)) {
		$user_email = $request_data['lazada_user_email'];
		$url = $request_data['lazada_api_url'];
		$api_key = $request_data['lazada_api_key'];
	    }
	    $parameters = array(
		'UserID' => $user_email,
		'Version' => '1.0',
		'Action' => 'GetProducts',
		'Format' => 'JSON',
		'Timestamp' => date('c')
	    );

	    ksort($parameters);

	    $encoded = array();
	    foreach ($parameters as $name => $value) {
		$encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
	    }
	    $concatenated = implode('&', $encoded);
	    $parameters['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, $api_key, false));
	    $queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);

	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url . "?" . $queryString);
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	    $data = curl_exec($ch);
	    curl_close($ch);
	    $response = json_decode($data, true);
	    $array_msg = [];
//            print_R($response);die;
	    if (empty($response) or ( isset($response) and ! empty($response) and isset($response['ErrorResponse']))) {
		$array_msg['api_error'] = 'Your API credentials are not working. Please check and try again.';
		echo json_encode($array_msg);
		die;
	    } else {
		$lazada = \backend\models\Channels::find()->where(['channel_name' => $channel_type])->one();

		$channel_id = $lazada->channel_ID;

		if (empty($channel_connected)) {
		    $channel_connection = new \backend\models\ChannelConnection;

		    $channel_connection->elliot_user_id = Yii::$app->user->identity->id;
		    $channel_connection->user_id = Yii::$app->user->identity->id;
		    $channel_connection->channel_id = $channel_id;
		    $channel_connection->lazada_user_email = $request_data['lazada_user_email'];
		    $channel_connection->lazada_api_key = $request_data['lazada_api_key'];
		    $channel_connection->lazada_api_url = $request_data['lazada_api_url'];
		    $channel_connection->connected = 'yes';
		    $channel_connection->created = date('Y-m-d h:i:s', time());
		    if ($channel_connection->save()) {
			$array_msg['success'] = "Your " . $channel_type . " store has been connected successfully. Importing is started. Once importing is done you will get a notify message.";
		    } else {
			$array_msg['error'] = "Error Something went wrong. Please try again";
		    }
		}
	    }
	}

	echo json_encode($array_msg);
	die;
    }

    function actionLazada($type = null) {
//        echo'd';die;

	return $this->render('lazada', [
		    'type' => $type,
	]);
    }

    function actionMalaysiaImporting($type = null) {

	if (empty($_GET['type']))
	    die('Not Allowed');

	$channel_type = '';
	switch ($type) {
	    case 'Malaysia':
		$channel_type = 'Lazada Malaysia';
		break;
	    case 'Singapore':
		$channel_type = 'Lazada Singapore';
		break;
	    case 'Indonesia':
		$channel_type = 'Lazada Indonesia';
		break;
	    case 'Thailand':
		$channel_type = 'Lazada Thailand';
		break;
	    case 'Vietnam':
		$channel_type = 'Lazada Vietnam';
		break;

	    default:
		break;
	}


	$user_id = $_GET['user_id'];
	$user = User::find()->where(['id' => $user_id])->one();

	$config = yii\helpers\ArrayHelper::merge(
			require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $user->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
	);
	$application = new yii\web\Application($config);

	$lazada = \backend\models\Channels::find()->where(['channel_name' => $channel_type])->one();

	$channel_id = $lazada->channel_ID;
	$channel_connected = \backend\models\ChannelConnection::find()->where(['channel_id' => $channel_id, 'user_id' => $user_id])->one();
	if (isset($channel_connected) and ! empty($channel_connected)) {
	    if ($this->importing($channel_id, $channel_connected, $channel_type, $user)) {
		$get_rec = \backend\models\ChannelConnection::find()->Where(['user_id' => $user->id, 'channel_id' => $channel_id])->one();
		$get_rec->import_status = 'Completed';
		//$get_rec->save(false);
		if ($get_rec->save(false)) {
		    $channel_setting = new \backend\models\Channelsetting();
		    $currency_channel_exists = \backend\models\Channelsetting::find()->Where(['user_id' => $user->id, 'channel_id' => $channel_id, 'setting_key' => 'currency'])->one();
		    if (empty($currency_channel_exists)) {
			$channel_setting->channel_id = $channel_id;
			$channel_setting->user_id = $user_id;
			$channel_setting->channel_name = $channel_type;
			$channel_setting->setting_key = 'currency';
			$channel_setting->setting_value = 'MYR';
			$channel_setting->created_at = date('Y-m-d H:i:s');
			$channel_setting->save(false);
		    }

//                    Yii::$app->session->setFlash('success', 'Success! your ' . $channel_type . ' Store data is imported successfully');
//                    $user_domain = Yii::$app->user->identity->domain_name;
//                    $url = Yii::$app->params['PROTOCOL'] . $user_domain . '.' . Yii::$app->params['DOMAIN_NAME'];
		    // $url = 'user-subscription';
		} else {
//                    Yii::$app->session->setFlash('danger', 'Error! Your ' . $channel_type . ' Store importing is not done.');
		}


		$notif_type = $channel_type;
		$notif_db = \backend\models\Notification::find()->Where(['notif_type' => $notif_type])->one();
		if (empty($notif_db)) {
		    $notification_model = new \backend\models\Notification();
		    $notification_model->user_id = $user->id;
		    $notification_model->notif_type = $notif_type;
		    $notification_model->notif_description = 'Your ' . $notif_type . ' data has been successfully imported.';
		    $notification_model->created_at = date('Y-m-d h:i:s', time());
		    $notification_model->save(false);
		}
		$cron_task_lazada_malaysia = \backend\models\CronTasks::find()->where(['elliot_user_id' => $user->id, 'task_source' => $notif_type])->one();
		if (empty($cron_task_lazada_malaysia)) {
		    $CronTasks = new \backend\models\CronTasks();
		    $CronTasks->elliot_user_id = $user->id;
		    $CronTasks->task_name = 'import';
		    $CronTasks->task_source = $notif_type;
		    $CronTasks->status = 'Completed';
		    $CronTasks->created_at = date('Y-m-d h:i:s', time());
		    $CronTasks->updated_at = date('Y-m-d h:i:s', time());
		    $CronTasks->save();
		}
		$msg = 'Success, Your ' . $notif_type . ' Store is Now Connected';
		$company_name = ucfirst($user->company_name);
		$send_email_notif = \common\models\CustomFunction::ConnectBigCommerceEmail($user->email, $company_name, $msg);
	    }
	}



//            return Yii::$app->getResponse()->redirect($url);
    }

    function actionLazadaupdate($type = null) {
	if (empty($_GET['type']))
	    die('Not Allowed');

//this is file for put lazada api xml format
//                require_once $_SERVER['DOCUMENT_ROOT'].'/backend/web/';

	$user = Yii::$app->user->identity;

	$parameters = array(
	    // The user ID for which we are making the call.
	    'UserID' => $user_email,
	    // The API version. Currently must be 1.0
	    'Version' => '1.0',
	    // The API method to call.
	    'Action' => 'CreateProduct',
	    // The format of the result.
	    'Format' => 'JSON',
	    // The current time formatted as ISO8601
	    'Timestamp' => date('c')
	);
// Sort parameters by name.
	ksort($parameters);
//        echo'<pre>';
//        print_r($parameters);
// URL encode the parameters.
	$encoded = array();
	foreach ($parameters as $name => $value) {
	    $encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
	}
	echo"<pre>";
	print_r($encoded);
	$request = ['Request' => ['PrimaryCategory' => 3,
		'Attributes' => [
		    'name' => 'api create product test sample',
		    'model' => '2017',
		    'short_description' => 'updated description',
		],
		'Skus' => [
		    'Available' => '20',
		    'SellerSku' => 'test01'
		]]
	];



//                ksort($request);
//
//        $fields_string = http_build_query($request);
//        print_r($fields_string);
//        die;
// Concatenate the sorted and URL encoded parameters into a string.
	$concatenated = implode('&', $encoded);
//        $new=$concatenated.'&'.$fields_string;
//        print_r($concatenated);die;
// Compute signature and add it to the parameters.
	$parameters['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, $api_key, false));

// Build Query String
//        echo'<br>';
	$queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);






	$input_request = "<?xml version='1.0' encoding='UTF-8'?>
            <Request>
            <Product>
                <PrimaryCategory>10000671</PrimaryCategory>
            <SPUId></SPUId>
        <Attributes>
            <name>api create product test sample</name>
  <name_ms>api create product test sample</name_ms>
  <warranty_type>Local Manufacturer Warranty</warranty_type>
            <short_description>This is a nice product</short_description>
            <brand>Remark</brand>
            <model>asdf</model>
            <kid_years>Kids (6-10yrs)</kid_years>
            <warranty>11 Months</warranty>
        </Attributes>
        <Skus>
            <Sku>
                <SellerSku>api-create-test-4</SellerSku>
  <tax_class>default</tax_class>
                <color_family>Green</color_family>
                <size>40</size>
                <quantity>1</quantity>
                <price>388.50</price>
                <package_length>11</package_length>
                <package_height>22</package_height>
                <package_weight>33</package_weight>
                <package_width>44</package_width>
                <package_content>this is what's in the box</package_content>
                <Images>
                    <Image>http://sg.s.alibaba.lzd.co/original/59046bec4d53e74f8ad38d19399205e6.jpg</Image>
                    <Image>http://sg.s.alibaba.lzd.co/original/179715d3de39a1918b19eec3279dd482.jpg</Image>
                </Images>
            </Sku>
          
        </Skus>
    </Product>
</Request>";

	$target = $url . '/?' . $queryString;

	file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/backend/web/test.xml', $input_request);

//        echo $target;die;
	$tmpFile = $_SERVER['DOCUMENT_ROOT'] . '/backend/web/test.xml';
///SENDING DATA TO LAZADA API
	$curl = curl_init();
//TRUE to HTTP PUT a file. The file to PUT must be set with CURLOPT_INFILE and CURLOPT_INFILESIZE.
	curl_setopt($curl, CURLOPT_PUT, 1);
//display headers
	curl_setopt($curl, CURLOPT_HEADER, true);
//The name of a file holding one or more certificates to verify the peer with. This only makes sense when used in combination with CURLOPT_SSL_VERIFYPEER.
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
//  A directory that holds multiple CA certificates. Use this option alongside CURLOPT_SSL_VERIFYPEER.
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
//  TRUE to HTTP PUT a file. The file to PUT must be set with CURLOPT_INFILE and CURLOPT_INFILESIZE.
	curl_setopt($curl, CURLOPT_INFILESIZE, filesize($tmpFile));
//The expected size, in bytes, of the file when uploading a file to a remote site.
	curl_setopt($curl, CURLOPT_INFILE, ($in = fopen($tmpFile, 'r')));
//A custom request method to use instead of "GET" or "HEAD" when doing a HTTP request. 
	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
//An array of HTTP header fields to set,
//        curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/xml']);
	curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/x-www-form-urlencoded']);

//The URL to fetch. 
	curl_setopt($curl, CURLOPT_URL, $target);
//TRUE to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

//        curl_setopt($curl, CURLOPT_POSTFIELDS, "xmlRequest=" . $input_request);
//        $str = http_build_query($request);
////        echo $str;die;
//        curl_setopt($curl, CURLOPT_POSTFIELDS, $str);
//executing code for connection
	$result = curl_exec($curl);
//closing connection
	curl_close($curl);
//closing the open file CURLOPT_INFILE
	fclose($in);
//printing data for user
	echo 'here';
	print_r($result);
	DIE;













//        print_r($queryString);
	$url = $user->lazada_api_url;

// Open cURL connection
	$ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, $url . "?" . $queryString);
	curl_setopt($ch, CURLOPT_URL, $url . "/?" . $queryString);

// Save response to the variable $data
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	$str = http_build_query($request);
//        echo $str;die;
	curl_setopt($ch, CURLOPT_POSTFIELDS, $str);
	$data = curl_exec($ch);

// Close Curl connection
	curl_close($ch);
	$response = json_decode($data, true);
	echo'<pre>';
	print_r($response);
	die;

	if (isset($_POST) and ! empty($_POST)) {
	    $request_data = Yii::$app->request->post();
//            echo'<pre>';
//            print_r($request_data);die;
	    date_default_timezone_set("UTC");
	    $channel_connected = \backend\models\ChannelConnection::find()->where(['channel_id' => $request_data['channel_id']])->one();
//                        print_r($channel_connected);
//die;
	    if (empty($channel_connected)) {
		$user_email = $request_data['lazada_user_email'];
		$url = $request_data['lazada_api_url'];
		$api_key = $request_data['lazada_api_key'];
	    } else {
		$user_email = $channel_connected->lazada_user_email;
		$url = $channel_connected->lazada_api_url;
		$api_key = $channel_connected->lazada_api_key;
	    }
	    $parameters = array(
		// The user ID for which we are making the call.
		'UserID' => $user_email,
		// The API version. Currently must be 1.0
		'Version' => '1.0',
		// The API method to call.
		'Action' => 'GetProducts',
		// The format of the result.
		'Format' => 'JSON',
		// The current time formatted as ISO8601
		'Timestamp' => date('c')
//    'Timestamp' => $utc 
	    );

// Sort parameters by name.
	    ksort($parameters);

// URL encode the parameters.
	    $encoded = array();
	    foreach ($parameters as $name => $value) {
		$encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
	    }

// Concatenate the sorted and URL encoded parameters into a string.
	    $concatenated = implode('&', $encoded);


// Compute signature and add it to the parameters.
	    $parameters['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, $api_key, false));

// Build Query String
	    $queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);

// Open cURL connection
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url . "?" . $queryString);

// Save response to the variable $data
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	    $data = curl_exec($ch);

// Close Curl connection
	    curl_close($ch);
	    $response = json_decode($data, true);
	    print_r($response);
	    die;
	    if (empty($response) or ( isset($response) and ! empty($response) and isset($response['ErrorResponse']))) {
		echo'error';
		die;
	    } else {
		$channel_connection = new \backend\models\ChannelConnection;
//                echo'<pre>';
//                print_r($channel_connection);
//                die;
		if (empty($channel_connected)) {
		    $channel_connection->elliot_user_id = Yii::$app->user->identity->id;
		    $channel_connection->user_id = Yii::$app->user->identity->id;
		    $channel_connection->channel_id = $request_data['channel_id'];
		    $channel_connection->lazada_user_email = $request_data['lazada_user_email'];
		    $channel_connection->lazada_api_key = $request_data['lazada_api_key'];
		    $channel_connection->lazada_api_url = $request_data['lazada_api_url'];
		    $channel_connection->connected = 'yes';
		    $channel_connection->save();
		}
		if (isset($response['SuccessResponse']) and ! empty($response['SuccessResponse']) and isset($response['SuccessResponse']['Body']) and ! empty($response['SuccessResponse']['Body']['Products'])) {
		    $products = $response['SuccessResponse']['Body']['Products'];
		    print_r($products);
		    die;
		}
	    }
	}
	return $this->render('lazada', [
		    'type' => $type,
	]);
    }

    function actionLazadaimage() {


	$user = Yii::$app->user->identity;

	$parameters = array(
	    // The user ID for which we are making the call.
	    'UserID' => 'info@fauxfreckles.com',
	    // The API version. Currently must be 1.0
	    'Version' => '1.0',
	    // The API method to call.
	    'Action' => 'MigrateImage',
	    // The format of the result.
	    'Format' => 'JSON',
	    // The current time formatted as ISO8601
	    'Timestamp' => date('c')
	);
// Sort parameters by name.
	ksort($parameters);
//        echo'<pre>';
//        print_r($parameters);
// URL encode the parameters.
	$encoded = array();
	foreach ($parameters as $name => $value) {
	    $encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
	}

//                ksort($request);
//
//        $fields_string = http_build_query($request);
//        print_r($fields_string);
//        die;
// Concatenate the sorted and URL encoded parameters into a string.
	$concatenated = implode('&', $encoded);
//        $new=$concatenated.'&'.$fields_string;
//        print_r($concatenated);die;
// Compute signature and add it to the parameters.
	$parameters['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, 'JeVfOHaBm0RdndrqR1HsZGyL20xHwGSxi3gngcsvOaCnPE_1nsf2Yb7f', false));

// Build Query String
//        echo'<br>';
	$queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);



//        $images = ['https://cdn.shopify.com/s/files/1/1922/6875/products/bottle_nps16mc_lilatastic_3_09042129-a53e-4db6-9aae-8126e23fbdeb.jpg?v=1497014227', 'https://cdn.shopify.com/s/files/1/1922/6875/products/jeans.jpg?v=1492508205'];
	$images = ['https://cdn.shopify.com/s/files/1/1922/6875/products/jeans.jpg?v=1492508205'];
	if (isset($images) and ! empty($images)) {
	    if (isset($_GET['a']) and ! empty($_GET['a'])) {
		$images = array_reverse($images);
	    }
	    foreach ($images as $single) {
		$input_request = "<?xml version='1.0' encoding='UTF-8'?>
          <Request>
             <Image>
                <Url>$single
                </Url>
            </Image>
           </Request>";
		$url = "https://api.sellercenter.lazada.com.my/";

		$target = $url . '/?' . $queryString;
		echo'<pre>';
		print_R($input_request);
		file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/backend/web/test.xml', $input_request);

//        echo $target;die;
		$tmpFile = $_SERVER['DOCUMENT_ROOT'] . '/backend/web/test.xml';
///SENDING DATA TO LAZADA API
		$curl = curl_init();
//TRUE to HTTP PUT a file. The file to PUT must be set with CURLOPT_INFILE and CURLOPT_INFILESIZE.
		curl_setopt($curl, CURLOPT_PUT, 1);
//display headers
		curl_setopt($curl, CURLOPT_HEADER, false);
//The name of a file holding one or more certificates to verify the peer with. This only makes sense when used in combination with CURLOPT_SSL_VERIFYPEER.
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
//  A directory that holds multiple CA certificates. Use this option alongside CURLOPT_SSL_VERIFYPEER.
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
//  TRUE to HTTP PUT a file. The file to PUT must be set with CURLOPT_INFILE and CURLOPT_INFILESIZE.
		curl_setopt($curl, CURLOPT_INFILESIZE, filesize($tmpFile));
//The expected size, in bytes, of the file when uploading a file to a remote site.
		curl_setopt($curl, CURLOPT_INFILE, ($in = fopen($tmpFile, 'r')));
//A custom request method to use instead of "GET" or "HEAD" when doing a HTTP request. 
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
//An array of HTTP header fields to set,
//        curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/xml']);
		curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/x-www-form-urlencoded']);

//The URL to fetch. 
		curl_setopt($curl, CURLOPT_URL, $target);
//TRUE to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

//        curl_setopt($curl, CURLOPT_POSTFIELDS, "xmlRequest=" . $input_request);
//        $str = http_build_query($request);
////        echo $str;die;
//        curl_setopt($curl, CURLOPT_POSTFIELDS, $str);
//executing code for connection
		$result = curl_exec($curl);
//closing connection
		curl_close($curl);
//closing the open file CURLOPT_INFILE
		fclose($in);
//printing data for user
		echo 'he3re';
		echo '<pre>';
		print_r($result);
		echo'hhh';
		print_r(json_decode($result, true));
	    }
	}
	die('bahar');



//        die;
	$parameters_category = array(
	    // The user ID for which we are making the call.
	    'UserID' => 'info@fauxfreckles.com',
	    // The API version. Currently must be 1.0
	    'Version' => '1.0',
	    // The API method to call.
	    'Action' => 'GetCategoryTree',
	    // The format of the result.
	    'Format' => 'JSON',
	    // The current time formatted as ISO8601
	    'Timestamp' => date('c')
//    'Timestamp' => $utc 
	);

// Sort parameters by name.
	ksort($parameters_category);

// URL encode the parameters.
	$encoded = array();
	foreach ($parameters_category as $name => $value) {
	    $encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
	}

// Concatenate the sorted and URL encoded parameters into a string.
	$concatenated = implode('&', $encoded);


// Compute signature and add it to the parameters.
	$parameters_category['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, 'JeVfOHaBm0RdndrqR1HsZGyL20xHwGSxi3gngcsvOaCnPE_1nsf2Yb7f', false));

// Build Query String
	$queryString = http_build_query($parameters_category, '', '&', PHP_QUERY_RFC3986);

// Open cURL connection
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url . "?" . $queryString);

// Save response to the variable $data
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	$data = curl_exec($ch);
// Close Curl connection
	curl_close($ch);
	$response = json_decode($data, true);
	echo'<pre>';
	print_r($response);
	$flat_array = $this->flatten($response['SuccessResponse']['Body']);
	print_r($flat_array);
	DIE;

	$key_found = array_search($single_product['PrimaryCategory'], array_column($flat_array, 'categoryId'));
	$category_name = $flat_array[$key_found]['name'];













//        print_r($queryString);
	$url = $user->lazada_api_url;

// Open cURL connection
	$ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, $url . "?" . $queryString);
	curl_setopt($ch, CURLOPT_URL, $url . "/?" . $queryString);

// Save response to the variable $data
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	$str = http_build_query($request);
//        echo $str;die;
	curl_setopt($ch, CURLOPT_POSTFIELDS, $str);
	$data = curl_exec($ch);

// Close Curl connection
	curl_close($ch);
	$response = json_decode($data, true);
	echo'<pre>';
	print_r($response);
	die;

	if (isset($_POST) and ! empty($_POST)) {
	    $request_data = Yii::$app->request->post();
//            echo'<pre>';
//            print_r($request_data);die;
	    date_default_timezone_set("UTC");
	    $channel_connected = \backend\models\ChannelConnection::find()->where(['channel_id' => $request_data['channel_id']])->one();
//                        print_r($channel_connected);
//die;
	    if (empty($channel_connected)) {
		$user_email = $request_data['lazada_user_email'];
		$url = $request_data['lazada_api_url'];
		$api_key = $request_data['lazada_api_key'];
	    } else {
		$user_email = $channel_connected->lazada_user_email;
		$url = $channel_connected->lazada_api_url;
		$api_key = $channel_connected->lazada_api_key;
	    }
	    $parameters = array(
		// The user ID for which we are making the call.
		'UserID' => $user_email,
		// The API version. Currently must be 1.0
		'Version' => '1.0',
		// The API method to call.
		'Action' => 'GetProducts',
		// The format of the result.
		'Format' => 'JSON',
		// The current time formatted as ISO8601
		'Timestamp' => date('c')
//    'Timestamp' => $utc 
	    );

// Sort parameters by name.
	    ksort($parameters);

// URL encode the parameters.
	    $encoded = array();
	    foreach ($parameters as $name => $value) {
		$encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
	    }

// Concatenate the sorted and URL encoded parameters into a string.
	    $concatenated = implode('&', $encoded);


// Compute signature and add it to the parameters.
	    $parameters['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, $api_key, false));

// Build Query String
	    $queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);

// Open cURL connection
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url . "?" . $queryString);

// Save response to the variable $data
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	    $data = curl_exec($ch);

// Close Curl connection
	    curl_close($ch);
	    $response = json_decode($data, true);
//              print_r($response);
//            die;  
	    if (empty($response) or ( isset($response) and ! empty($response) and isset($response['ErrorResponse']))) {
		echo'error';
		die;
	    } else {
		$channel_connection = new \backend\models\ChannelConnection;
//                echo'<pre>';
//                print_r($channel_connection);
//                die;
		if (empty($channel_connected)) {
		    $channel_connection->elliot_user_id = Yii::$app->user->identity->id;
		    $channel_connection->user_id = Yii::$app->user->identity->id;
		    $channel_connection->channel_id = $request_data['channel_id'];
		    $channel_connection->lazada_user_email = $request_data['lazada_user_email'];
		    $channel_connection->lazada_api_key = $request_data['lazada_api_key'];
		    $channel_connection->lazada_api_url = $request_data['lazada_api_url'];
		    $channel_connection->connected = 'yes';
		    $channel_connection->save();
		}
		if (isset($response['SuccessResponse']) and ! empty($response['SuccessResponse']) and isset($response['SuccessResponse']['Body']) and ! empty($response['SuccessResponse']['Body']['Products'])) {
		    $products = $response['SuccessResponse']['Body']['Products'];
		    print_r($products);
		    die;
		}
	    }
	}
	return $this->render('lazada', [
		    'type' => $type,
	]);
    }

    function actionLazada1($type = null) {
//        if (empty($_GET['type']))
//            die;
//        if (isset($_POST) and ! empty($_POST)) {
//            $request_data = Yii::$app->request->post();
//            echo'<pre>';
//            print_r($request_data);
//            die;
	date_default_timezone_set("UTC");
	$user = Yii::$app->user->identity;

	$parameters = array(
	    // The user ID for which we are making the call.
	    'UserID' => $user->lazada_user_email,
	    // The API version. Currently must be 1.0
	    'Version' => '1.0',
	    // The API method to call.
	    'Action' => 'GetOrders',
	    // The format of the result.
	    'Format' => 'JSON',
	    // The current time formatted as ISO8601
	    'Timestamp' => date('c')
//    'Timestamp' => $utc 
	);

// Sort parameters by name.
	ksort($parameters);

// URL encode the parameters.
	$encoded = array();
	foreach ($parameters as $name => $value) {
	    $encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
	}

// Concatenate the sorted and URL encoded parameters into a string.
	$concatenated = implode('&', $encoded);

// The API key for the user as generated in the Seller Center GUI.
// Must be an API key associated with the UserID parameter.
	$api_key = Yii::$app->user->identity->lazada_api_key;

// Compute signature and add it to the parameters.
	$parameters['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, $api_key, false));

	echo'<pre>';
	print_R($parameters);


// Build Query String
	$queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);

// Open cURL connection
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url . "/?" . $queryString);

// Save response to the variable $data
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	$data = curl_exec($ch);

// Close Curl connection
	curl_close($ch);
	print_r(json_decode($data));
	die;
//        }
	return $this->render('lazada', [
		    'type' => $type,
	]);
    }

    function actionLazada2($type = null) {
//        if (empty($_GET['type']))
//            die;
//        if (isset($_POST) and ! empty($_POST)) {
//            $request_data = Yii::$app->request->post();
//            echo'<pre>';
//            print_r($request_data);
//            die;
	date_default_timezone_set("UTC");
	$user = Yii::$app->user->identity;

	$parameters = array(
	    // The user ID for which we are making the call.
	    'UserID' => $user->lazada_user_email,
	    // The API version. Currently must be 1.0
	    'Version' => '1.0',
	    // The API method to call.
	    'Action' => 'GetOrderItems',
	    // The format of the result.
	    'Format' => 'JSON',
	    // The current time formatted as ISO8601
	    'Timestamp' => date('c')
	    , 'OrderId' => '109121557'
//    'Timestamp' => $utc 
	);

// Sort parameters by name.
	ksort($parameters);

// URL encode the parameters.
	$encoded = array();
	foreach ($parameters as $name => $value) {
	    $encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
	}

// Concatenate the sorted and URL encoded parameters into a string.
	$concatenated = implode('&', $encoded);

// The API key for the user as generated in the Seller Center GUI.
// Must be an API key associated with the UserID parameter.
	$api_key = Yii::$app->user->identity->lazada_api_key;

// Compute signature and add it to the parameters.
	$parameters['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, $api_key, false));

	echo'<pre>';
	print_R($parameters);



// Build Query String
	$queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);

// Open cURL connection
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url . "/?" . $queryString);

// Save response to the variable $data
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	$data = curl_exec($ch);

// Close Curl connection
	curl_close($ch);
	print_r(json_decode($data));
	die;
//        }
	return $this->render('lazada', [
		    'type' => $type,
	]);
    }

    public function actionMalaysiaCronImporting() {
//        echo'here';die;
	$user_id = $_GET['user_id'];
	$channel = $_GET['channel'];
	$get_users = User::find()->where(['id' => $user_id])->one();
	$config = yii\helpers\ArrayHelper::merge(
			require($_SERVER['DOCUMENT_ROOT'] . '/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/common/users_config/' . $get_users->domain_name . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'] . '/backend/config/main-local.php')
	);
	$application = new yii\web\Application($config);

	$cron_tasks_data = \backend\models\CronTasks::find()->where(['elliot_user_id' => $user_id, 'task_source' => 'Lazada Malaysia', 'status' => 'Completed'])->one();
	if (!empty($cron_tasks_data)) {
	    $cron_tasks_data->status = 'Pending';
	    $cron_tasks_data->updated_at = date('Y-m-d h:i:s', time());
	    $cron_tasks_data->save();

	    $channel_data = \backend\models\Channels::find()->where(['channel_name' => 'Lazada Malaysia'])->one();
	    $channel_id = $channel_data->channel_ID;
//            echo $channel_id;
	    $channel_connected = \backend\models\ChannelConnection::find()->where(['user_id' => $user_id, 'channel_id' => $channel_id])->one();
	    if (!empty($channel_connected)) {
		$this->importing($channel_id, $channel_connected, 'Lazada Malaysia', $get_users);
		echo'1';
		$cron_tasks_data = \backend\models\CronTasks::find()->where(['elliot_user_id' => $user_id, 'task_source' => 'Lazada Malaysia', 'status' => 'Pending'])->one();
		if (!empty($cron_tasks_data)) {
		    $cron_tasks_data->status = 'Completed';
		    $cron_tasks_data->updated_at = date('Y-m-d h:i:s', time());
		    $cron_tasks_data->save();
		}
	    }
	}
    }

    function importing($channel_id, $channel_connected, $channel_type, $user) {
	$currency_code = 'MYR';
	switch ($channel_type) {
	    case 'Lazada Malaysia':
		$currency_code = 'MYR';
		break;
	    case 'Lazada Singapore':
		$currency_code = 'SGD';
		break;
	    case 'Lazada Indonesia':
		$currency_code = 'IDR';
		break;
	    case 'Lazada Thailand':
		$currency_code = 'THB';
		break;
	    case 'Lazada Vietnam':
		$currency_code = 'VND';
		break;

	    default:
		break;
	}

	if (!empty($channel_connected)) {
	    $user_email = $channel_connected->lazada_user_email;
	    $url = $channel_connected->lazada_api_url;
	    $api_key = $channel_connected->lazada_api_key;
	}
	$parameters = array(
	    'UserID' => $user_email,
	    'Version' => '1.0',
	    'Action' => 'GetProducts',
	    'Format' => 'JSON',
	    'Timestamp' => date('c')
	);

	ksort($parameters);
	$encoded = array();
	foreach ($parameters as $name => $value) {
	    $encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
	}

	$concatenated = implode('&', $encoded);


	$parameters['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, $api_key, false));

	$queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url . "?" . $queryString);

	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	$data = curl_exec($ch);
	curl_close($ch);
	$response = json_decode($data, true);
//            print_r($response);
//die;
	$conversion_rate = 1;
	if (empty($response) or ( isset($response) and ! empty($response) and isset($response['ErrorResponse']))) {
	    echo'error';
	    die;
	} else {
	    if (isset($response['SuccessResponse']) and ! empty($response['SuccessResponse']) and isset($response['SuccessResponse']['Body']) and ! empty($response['SuccessResponse']['Body']['Products'])) {
		$this->LazadaChannelImport($user->id, $channel_connected->channel_connection_id);

		/* Save Currency conversion details */
		$conversion_rate = Stores::getCurrencyConversionRate($currency_code, 'USD');

		$currency_check = CurrencyConversion::find()->Where(['to_currency' => $currency_code])->one();
		if (empty($currency_check)) {
		    $store_currency_conversion = new CurrencyConversion();
		    $store_currency_conversion->from_currency = 'USD';
		    $store_currency_conversion->from_value = 1;
		    $store_currency_conversion->to_currency = $currency_code;
		    $store_currency_conversion->to_value = $conversion_rate;
		    $store_currency_conversion->created_at = date('Y-m-d H:i:s', time());
		    $store_currency_conversion->save(false);
		} else {
		    $currency_check->from_currency = 'USD';
		    $currency_check->from_value = 1;
		    $currency_check->to_currency = $currency_code;
		    $currency_check->to_value = $conversion_rate;
		    $currency_check->save(false);
		}
		$products = $response['SuccessResponse']['Body']['Products'];
		if (isset($products) and ! empty($products)) {
//                            echo"<Pre>";print_R($products);die;

		    $user_company = $user->company_name;
		    foreach ($products as $single_product) {

			$parameters_category = array(
			    'UserID' => $user_email,
			    'Version' => '1.0',
			    'Action' => 'GetCategoryTree',
			    'Format' => 'JSON',
			    'Timestamp' => date('c')
			);

			ksort($parameters_category);
			$encoded = array();
			foreach ($parameters_category as $name => $value) {
			    $encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
			}
			$concatenated = implode('&', $encoded);
			$parameters_category['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, $api_key, false));
			$queryString = http_build_query($parameters_category, '', '&', PHP_QUERY_RFC3986);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url . "?" . $queryString);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			$data = curl_exec($ch);
			curl_close($ch);
			$response = json_decode($data, true);
			//echo"<Pre>";
			//print_R($data);
			//print_r($response);die;
			$category_name = "Home";
			if (isset($response['SuccessResponse'])) {
			    $flat_array = $this->flatten($response['SuccessResponse']['Body']);
			    $key_found = array_search($single_product['PrimaryCategory'], array_column($flat_array, 'categoryId'));
			    $category_name = $flat_array[$key_found]['name'];
			}
			$p_name = $single_product['Attributes']['name'];
			$p_sku = $single_product['Skus'][0]['SellerSku'];
			$p_des = $single_product['Attributes']['short_description'];
			$p_avail = $single_product['Skus'][0]['Available'];
			$p_brand = ucfirst($user_company);
			$p_weight = isset($single_product['Skus'][0]['package_weight']) ? $single_product['Skus'][0]['package_weight'] : '';
			$p_price = $single_product['Skus'][0]['price'];
			$p_saleprice = $single_product['Skus'][0]['special_price'] ? $single_product['Skus'][0]['special_price'] : $single_product['Skus'][0]['price'];
			$p_price = number_format((float) $p_price * $conversion_rate, 2, '.', '');
			$p_saleprice = number_format((float) $p_saleprice * $conversion_rate, 2, '.', '');
			$p_visibility = ($single_product['Skus'][0]['Status'] == 'active') ? true : false;
			$channel_abb_id = 'LZ' . $p_sku;
			$ProductAbbrivation_check = ProductAbbrivation::find()->Where(['channel_abb_id' => $channel_abb_id])->one();
			$category_abb_id = 'LZ' . $single_product['PrimaryCategory'];

			if (empty($ProductAbbrivation_check)) {
			    $checkProductModel = Products::find()->where(['SKU' => $p_sku])->one();
			    if (isset($checkProductModel) and ! empty($checkProductModel)) {

				$last_pro_id = $checkProductModel->id;
				$product_abberivation = new ProductAbbrivation();
				$product_abberivation->channel_abb_id = $channel_abb_id;
				$product_abberivation->product_id = $last_pro_id;
				$product_abberivation->price = $p_price;
				$product_abberivation->salePrice = $p_saleprice;
				$product_abberivation->schedules_sales_date = @$single_product['Skus'][0]['special_from_date'];
				$product_abberivation->stock_quantity = $single_product['Skus'][0]['Available'];
				if ($single_product['Skus'][0]['Available'] > 0) {
				    $product_abberivation->stock_level = 'In Stock';
				} else {
				    $product_abberivation->stock_level = 'Out of Stock';
				}
				$product_abberivation->channel_accquired = $channel_type;
				$product_abberivation->created_at = date('Y-m-d H:i:s', time());
				$product_abberivation->updated_at = date('Y-m-d H:i:s', time());
				$product_abberivation->save(false);

				$productChannelModel = new ProductChannel();
				$productChannelModel->channel_id = $channel_id;
				$productChannelModel->product_id = $checkProductModel->id;
				$productChannelModel->status = 'yes';
				$productChannelModel->created_at = date('Y-m-d h:i:s', time());
				$productChannelModel->save(false);
			    } else {

				$productModel = new Products ();
				$productModel->channel_abb_id = $channel_abb_id;
				$productModel->product_name = $p_name;
				$productModel->SKU = $p_sku;
				$productModel->description = $p_des;
				$productModel->adult = 'no';
				$productModel->age_group = NULL;
				$productModel->gender = 'Unisex';
				$productModel->brand = $p_brand;
				$productModel->condition = 'New';
				$productModel->weight = $p_weight;
				$productModel->UPC = '';
				$productModel->EAN = '';
				$productModel->Jan = '';
				$productModel->ISBN = '';
				$productModel->MPN = '';
				$productModel->country_code = 'MY';
				$productModel->stock_quantity = $single_product['Skus'][0]['Available'];
				if ($single_product['Skus'][0]['Available'] > 0) {
				    $productModel->stock_level = 'In Stock';
				    $productModel->availability = 'In Stock';
				} else {
				    $productModel->stock_level = 'Out of Stock';
				    $productModel->availability = 'Out of Stock';
				}
				$productModel->stock_status = 'Visible';
				$productModel->low_stock_notification = 5;
				$productModel->price = $p_price;
				$productModel->sales_price = $p_saleprice;
				$productModel->schedules_sales_date = @$single_product['Skus'][0]['special_from_date'];
				$productModel->created_at = date('Y-m-d H:i:s', time());
				$productModel->updated_at = date('Y-m-d H:i:s', time());
				$productModel->elliot_user_id = $user->id;
				$productModel->save(false);
				$saved_product_id = $productModel->id;

				$ProductAbbrivation = new ProductAbbrivation();
				$ProductAbbrivation->channel_abb_id = $channel_abb_id;
				$ProductAbbrivation->product_id = $saved_product_id;
				$ProductAbbrivation->price = $p_price;
				$ProductAbbrivation->salePrice = $p_saleprice;
				$ProductAbbrivation->schedules_sales_date = @$single_product['Skus'][0]['special_from_date'];
				if ($single_product['Skus'][0]['Available'] > 0) {
				    $ProductAbbrivation->stock_level = 'In Stock';
				} else {
				    $ProductAbbrivation->stock_level = 'Out of Stock';
				}
				$ProductAbbrivation->stock_quantity = $single_product['Skus'][0]['Available'];
				$ProductAbbrivation->channel_accquired = $channel_type;
				$ProductAbbrivation->created_at = date('Y-m-d H:i:s', time());
				$ProductAbbrivation->save(false);

				$get_category = Categories::find()->select('category_ID')->where(['category_name' => $category_name])->one();
				$category_abb_id = 'LZ' . $single_product['PrimaryCategory'];
//                                print_r($get_category);die;
				if (empty($get_category)) {
				    $category = new Categories();
				    $category->channel_abb_id = $category_abb_id;
				    $category->category_name = $category_name;
				    $category->parent_category_ID = '';
				    $category->elliot_user_id = $user->id;
				    $category->store_category_groupid = '';
				    $category->created_at = date('Y-m-d H:i:s', time());
				    $category->updated_at = date('Y-m-d H:i:s', time());
				    $category->save(false);
				    $category_id = $category->category_ID;
				    $CategoryAbbrivation = new \backend\models\CategoryAbbrivation();
				    $CategoryAbbrivation->channel_abb_id = $category_abb_id;
				    $CategoryAbbrivation->category_ID = $category->category_ID;
				    $CategoryAbbrivation->channel_accquired = $channel_type;
				    $CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
				    $CategoryAbbrivation->save(false);

				    $productCategoryModel = new ProductCategories();
				    $productCategoryModel->category_ID = $category_id;
				    $productCategoryModel->product_ID = $saved_product_id;
				    $productCategoryModel->created_at = date('Y-m-d H:i:s', time());
				    $productCategoryModel->updated_at = date('Y-m-d H:i:s', time());
				    $productCategoryModel->elliot_user_id = $user->id;
				    $productCategoryModel->save(false);
				} else {
				    $categorytAbbrivation_check = \backend\models\CategoryAbbrivation::find()->Where(['channel_abb_id' => $category_abb_id])->one();
				    if (empty($categorytAbbrivation_check)) {
					$CategoryAbbrivation = new \backend\models\CategoryAbbrivation();
					$CategoryAbbrivation->channel_abb_id = $category_abb_id;
					$CategoryAbbrivation->category_ID = $get_category->category_ID;
					$CategoryAbbrivation->channel_accquired = $channel_type;
					$CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
					$CategoryAbbrivation->save(false);
				    }
				}
				if (!empty($single_product['Skus'][0]['Images'])) {
				    foreach ($single_product['Skus'][0]['Images'] as $product_image) {
					if (empty($product_image))
					    continue;
					// echo '<Pre>';print_r*$product_image;
					$p_image_link = $product_image;
					//Create Model for Each new Product Image
					$productImageModel = new ProductImages;
					$productImageModel->link = $p_image_link;
					$productImageModel->product_ID = $saved_product_id;
					$productImageModel->default_image = 'Yes';
					$productImageModel->image_status = 1;
					$productImageModel->save(false);
				    }
				}
				$productChannelModel = new ProductChannel();
				$productChannelModel->channel_id = $channel_id;
				$productChannelModel->product_id = $saved_product_id;
				$productChannelModel->save(false);
				//Product Variations Table Entry
				$product_skus = $single_product['Skus'];
				if (!empty($product_skus)) {
				    foreach ($product_skus as $key => $product_sku) {
					if ($key == 0)
					    continue;
					if (isset($product_sku['SellerSku'])) {
					    $newP_Var_Model = new ProductVariation;
					    $newP_Var_Model->product_id = $saved_product_id;
					    $newP_Var_Model->elliot_user_id = $user->id;
					    $newP_Var_Model->item_name = 'SKU';
					    $newP_Var_Model->item_value = $product_sku['SellerSku'];
					    $created_date = date('Y-m-d H:i:s', time());
					    $newP_Var_Model->created_at = $created_date;
					    $newP_Var_Model->save(false);
					}
				    }
				}

				//Merchant Products Table Entry
				$merchantproductsModel = new MerchantProducts;
				$merchantproductsModel->merchant_ID = $user->id;
				$merchantproductsModel->product_ID = $saved_product_id;
				$merchantproductsModel->elliot_user_id = $user->id;
				$merchantproductsModel->save(false);
			    }
			} else {
			    $checkProductModel = Products::find()->where(['id' => $ProductAbbrivation_check->product_id])->one();

			    $checkProductModel->channel_abb_id = $channel_abb_id;
			    $checkProductModel->product_name = $p_name;
			    $checkProductModel->SKU = $p_sku;
			    $checkProductModel->description = $p_des;
			    $checkProductModel->adult = 'no';
			    $checkProductModel->age_group = NULL;
			    $checkProductModel->gender = 'Unisex';
			    $checkProductModel->availability = 'In Stock';
			    if ($single_product['Skus'][0]['Available'] > 0) {
				$checkProductModel->stock_level = 'In Stock';
				$checkProductModel->availability = 'In Stock';
			    } else {
				$checkProductModel->stock_level = 'Out of Stock';
				$checkProductModel->availability = 'Out of Stock';
			    }
			    $checkProductModel->brand = $p_brand;
			    $checkProductModel->condition = 'New';
			    $checkProductModel->weight = $p_weight;
			    $checkProductModel->UPC = '';
			    $checkProductModel->EAN = '';
			    $checkProductModel->Jan = '';
			    $checkProductModel->ISBN = '';
			    $checkProductModel->MPN = '';
			    //Inventory Fields Mapping
			    $checkProductModel->stock_quantity = $single_product['Skus'][0]['Available'];
			    $checkProductModel->stock_status = 'Visible';
			    $checkProductModel->low_stock_notification = 5;
			    $checkProductModel->price = $p_price;
			    $checkProductModel->sales_price = $p_saleprice;
			    $checkProductModel->schedules_sales_date = @$single_product['Skus'][0]['special_from_date'];

			    $checkProductModel->created_at = date('Y-m-d H:i:s', time());
			    $checkProductModel->updated_at = date('Y-m-d H:i:s', time());
			    ;
			    //Save Elliot User id
			    $checkProductModel->elliot_user_id = $user->id;
//                                    print_r($checkProductModel);
			    $checkProductModel->save(false);
//                                    print_r($checkProductModel);
			    $saved_product_id = $checkProductModel->id;

			    $ProductAbbrivation_check = ProductAbbrivation::find()->Where(['channel_abb_id' => $channel_abb_id, 'product_id' => $saved_product_id])->one();
			    if (empty($ProductAbbrivation_check)) {
				$ProductAbbrivation_check = new ProductAbbrivation();
			    }
			    $ProductAbbrivation_check->channel_abb_id = $channel_abb_id;
			    $ProductAbbrivation_check->product_id = $saved_product_id;
			    $ProductAbbrivation_check->channel_accquired = $channel_type;
			    $ProductAbbrivation_check->stock_quantity = $single_product['Skus'][0]['Available'];
			    $ProductAbbrivation_check->price = $p_price;
			    $ProductAbbrivation_check->salePrice = $p_saleprice;
			    $ProductAbbrivation_check->schedules_sales_date = @$single_product['Skus'][0]['special_from_date'];
			    $ProductAbbrivation_check->created_at = date('Y-m-d H:i:s', time());
			    if ($single_product['Skus'][0]['Available'] > 0) {
				$ProductAbbrivation_check->stock_level = 'In Stock';
			    } else {
				$ProductAbbrivation_check->stock_level = 'Out of Stock';
			    }
			    $ProductAbbrivation_check->save(false);

			    $ProductChannel_check = ProductChannel::find()->Where(['channel_id' => $channel_id, 'product_id' => $saved_product_id])->one();
			    if (empty($ProductChannel_check)) {
				//Product Channel/Store Table Entry
				$productChannelModel = new ProductChannel;
				$productChannelModel->channel_id = $channel_id;
				$productChannelModel->product_id = $saved_product_id;
				$productChannelModel->status = 'yes';
				$productChannelModel->created_at = date('Y-m-d H:i:s', time());
				$productChannelModel->save(false);
			    }

			    $get_category = Categories::find()->select('category_ID')->where(['category_name' => $category_name])->one();
			    if (empty($get_category)) {
				$category = new Categories();
				$category->channel_abb_id = $category_abb_id;
				$category->category_name = $category_name;
				$category->parent_category_ID = '';
				$category->elliot_user_id = $user->id;
				$category->store_category_groupid = '';
				$category->created_at = date('Y-m-d H:i:s', time());
				$category->updated_at = date('Y-m-d H:i:s', time());
				$category->save(false);
				$category_id = $category->category_ID;
				$CategoryAbbrivation = new \backend\models\CategoryAbbrivation();
				$CategoryAbbrivation->channel_abb_id = $category_abb_id;
				$CategoryAbbrivation->category_ID = $category->category_ID;
				$CategoryAbbrivation->channel_accquired = $channel_type;
				$CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
				$CategoryAbbrivation->save(false);

				$productCategoryModel = new ProductCategories();
				$productCategoryModel->category_ID = $category_id;
				$productCategoryModel->product_ID = $saved_product_id;
				$productCategoryModel->created_at = date('Y-m-d H:i:s', time());
				$productCategoryModel->updated_at = date('Y-m-d H:i:s', time());
				$productCategoryModel->elliot_user_id = $user->id;
				$productCategoryModel->save(false);
			    } else {
				$categorytAbbrivation_check = \backend\models\CategoryAbbrivation::find()->Where(['channel_abb_id' => $category_abb_id])->one();
				if (empty($categorytAbbrivation_check)) {
				    $CategoryAbbrivation = new \backend\models\CategoryAbbrivation();
				    $CategoryAbbrivation->channel_abb_id = $category_abb_id;
				    $CategoryAbbrivation->category_ID = $get_category->category_ID;
				    $CategoryAbbrivation->channel_accquired = $channel_type;
				    $CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
				    $CategoryAbbrivation->save(false);
				}
			    }
			    if (!empty($single_product['Skus'][0]['Images'])) {
				foreach ($single_product['Skus'][0]['Images'] as $product_image) {
				    if (empty($product_image))
					continue;

				    $p_image_link = $product_image;
				    $ProductImage_check = ProductImages::find()->Where(['link' => $p_image_link, 'product_id' => $saved_product_id])->one();
				    if (empty($ProductImage_check)) {
					$productImageModel = new ProductImages;
					$productImageModel->link = $p_image_link;
					$productImageModel->product_ID = $saved_product_id;
					$productImageModel->default_image = 'Yes';
					$productImageModel->image_status = 1;
					$productImageModel->save(false);
				    }
				}
			    }
			    $product_skus = $single_product['Skus'];
			    if (!empty($product_skus)) {
				foreach ($product_skus as $key => $product_sku) {
				    if ($key == 0)
					continue;
				    if (isset($product_sku['SellerSku'])) {
					$ProductChannel_check = ProductVariation::find()->Where(['item_name' => 'SKU', 'item_value' => $product_sku['SellerSku'], 'product_id' => $saved_product_id])->one();
					if (empty($ProductChannel_check)) {
					    $newP_Var_Model = new ProductVariation;
					    $newP_Var_Model->product_id = $saved_product_id;
					    $newP_Var_Model->elliot_user_id = $user->id;
					    $newP_Var_Model->item_name = 'SKU';
					    $newP_Var_Model->item_value = $product_sku['SellerSku'];
//                                        $newP_Var_Model->store_variation_id = 'BGC' . $product_sku->id;
					    $created_date = date('Y-m-d H:i:s', time());
					    $newP_Var_Model->created_at = $created_date;
					    $newP_Var_Model->save(false);
					}
				    }
				}
			    }
			}
		    }
		}
	    }
	}


	$parameters = array(
	    'UserID' => $user_email,
	    'Version' => '1.0',
	    'Action' => 'GetOrders',
	    'Format' => 'JSON',
	    'Timestamp' => date('c')
	);

	ksort($parameters);
	$encoded = array();
	foreach ($parameters as $name => $value) {
	    $encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
	}
	$concatenated = implode('&', $encoded);
	$parameters['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, $api_key, false));
	$queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url . "?" . $queryString);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	$data = curl_exec($ch);
	curl_close($ch);
	$order_response_decoded = json_decode($data);
	$orders = @$order_response_decoded->SuccessResponse->Body->Orders;
//                echo"<pre>";
//                echo'ithe';
////die;
//                print_R($orders);
//                die;
	if (isset($orders) and ! empty($orders)) {
	    foreach ($orders as $orders_data) {
		$lazada_id = $orders_data->OrderId;
		$prefix_order_id = 'LZ' . $lazada_id;
		$order_model = Orders::find()->Where(['channel_abb_id' => $prefix_order_id])->one();
		if (empty($order_model)) {
		    if (!empty($prefix_order_id)):
			$customer_data = array(
			    'customer_id' => $lazada_id,
			    'elliot_user_id' => $user->id,
			    'mul_store_id' => '',
			    'mul_channel_id' => $channel_id,
			    'first_name' => isset($orders_data->CustomerFirstName) ? $orders_data->CustomerFirstName : "",
			    'last_name' => isset($orders_data->CustomerLastName) ? $orders_data->CustomerLastName : "",
			    'email' => isset($orders_data->AddressBilling->CustomerEmail) ? $orders_data->AddressBilling->CustomerEmail : "",
			    'channel_store_name' => $channel_type,
			    'channel_store_prefix' => 'LZ',
			    'customer_created_at' => date('Y-m-d H:i:s', strtotime($orders_data->CreatedAt)),
			    'customer_updated_at' => date('Y-m-d H:i:s', strtotime($orders_data->UpdatedAt)),
			    'billing_address' => array(
				'street_1' => isset($orders_data->AddressBilling->Address1) ? $orders_data->AddressBilling->Address1 : "",
				'street_2' => isset($orders_data->AddressBilling->Address2) ? $orders_data->AddressBilling->Address2 : "",
				'city' => isset($orders_data->AddressBilling->City) ? $orders_data->AddressBilling->City : "",
				'state' => '',
				'country' => isset($orders_data->AddressBilling->Country) ? $orders_data->AddressBilling->Country : "",
				'country_iso' => '',
				'zip' => isset($orders_data->AddressBilling->PostCode) ? $orders_data->AddressBilling->PostCode : "",
				'phone_number' => isset($orders_data->AddressBilling->Phone) ? $orders_data->AddressBilling->Phone : "", 'address_type' => 'Default',
			    ),
			    'shipping_address' => array(
				'street_1' => isset($orders_data->AddressShipping->Address1) ? $orders_data->AddressShipping->Address1 : "",
				'street_2' => isset($orders_data->AddressShipping->Address2) ? $orders_data->AddressShipping->Address2 : "",
				'city' => isset($orders_data->AddressShipping->City) ? $orders_data->AddressShipping->City : "",
				'state' => '',
				'country' => isset($orders_data->AddressShipping->Country) ? $orders_data->AddressShipping->Country : "",
				'zip' => isset($orders_data->AddressShipping->PostCode) ? $orders_data->AddressShipping->PostCode : ""),
			);
			Stores::customerImportingCommon($customer_data);
		    endif;
		}

		$customer_db1 = \backend\models\CustomerAbbrivation::find()->Where(['channel_abb_id' => $prefix_order_id])->one();
		$customer_id = $customer_db1->customer_id;
		$order_customer_id = $customer_id;
		$order_status = $orders_data->Statuses[0];
		switch (strtolower($order_status)) {
		    case "pending":
			$order_status = 'Pending';
			break;
		    case "delivered":
			$order_status = "Completed";
			break;
		    case "canceled":
			$order_status = "Cancel";
			break;
		    case "closed":
			$order_status = "Closed";
			break;
		    case "fraud":
			$order_status = "Cancel";
			break;
		    case "holded":
			$order_status = "On Hold";
			break;
		    case "payment_review":
			$order_status = "Pending";
			break;
		    case "return_shipped_by_customer":
			$order_status = "Cancel";
			break;
		    case "returned":
			$order_status = "Cancel";
			break;
		    case "return_waiting_for_approval":
			$order_status = "Cancel";
			break;
		    case "return_rejected":
			$order_status = "Cancel";
			break;
		    case "failed":
			$order_status = "Cancel";
			break;
		    case "pending_payment":
			$order_status = "Pending";
			break;
		    case "pending_paypal":
			$order_status = "Pending";
			break;
		    case "processing":
			$order_status = "In Transit";
			break;
		    case "shipped":
			$order_status = "In Transit";
			break;
		    default:
			$order_status = "Pending";
		}
		$product_qauntity = $orders_data->ItemsCount;
		//billing Address
		$billing_add1 = isset($orders_data->AddressBilling->Address1) ? $orders_data->AddressBilling->Address1 : "" . ',' . isset($orders_data->AddressBilling->Address2) ? $orders_data->AddressBilling->Address2 : "";
		$billing_add2 = isset($orders_data->AddressBilling->City) ? $orders_data->AddressBilling->City : "" . ',' .
			',' . isset($orders_data->AddressBilling->PostCode) ? $orders_data->AddressBilling->PostCode : "" . ',' . isset($orders_data->AddressBilling->Country) ? $orders_data->AddressBilling->Country : "";
		$billing_address = $billing_add1 . ',' . $billing_add2;

		//billing Address
		$bill_street_1 = isset($orders_data->AddressBilling->Address1) ? $orders_data->AddressBilling->Address1 : '';
		$bill_street_2 = isset($orders_data->AddressBilling->Address2) ? $orders_data->AddressBilling->Address2 : '';
		$bill_city = isset($orders_data->AddressBilling->City) ? $orders_data->AddressBilling->City : '';
		$bill_state = '';
		$bill_zip = isset($orders_data->AddressBilling->PostCode) ? $orders_data->AddressBilling->PostCode : '';
		$bill_country = isset($orders_data->AddressBilling->Country) ? $orders_data->AddressBilling->Country : '';
		$bill_country_iso = '';

		//Shipping Address
		$ship_street_1 = isset($orders_data->AddressShipping->Address1) ? $orders_data->AddressShipping->Address1 : $orders_data->AddressBilling->Address1;
		$ship_street_2 = isset($orders_data->AddressShipping->Address2) ? $orders_data->AddressShipping->Address2 : $orders_data->AddressBilling->Address2;
		$ship_city = isset($orders_data->AddressShipping->City) ? $orders_data->AddressShipping->City : $orders_data->AddressBilling->City;
		$ship_state = "";
		$ship_zip = isset($orders_data->AddressShipping->PostCode) ? $orders_data->AddressShipping->PostCode : $orders_data->AddressBilling->PostCode;
		$ship_country = isset($orders_data->AddressShipping->Country) ? $orders_data->AddressShipping->Country : $orders_data->AddressBilling->Country;
		$ship_country_iso = '';

		$shipping_address = $ship_street_1 . ',' . $ship_street_2 . ',' . $ship_city . ',' . $ship_state . ',' . $ship_zip . ',' . $ship_country;

		$total_amount = $orders_data->Price;
		$total_amount = $total_amount * $conversion_rate;
		$base_shipping_cost = '';
		$shipping_cost_tax = '';
		$base_handling_cost = '';
		$handling_cost_tax = '';
		$base_wrapping_cost = '';
		$wrapping_cost_tax = '';
		$payment_method = $orders_data->PaymentMethod;
		$payment_provider_id = '';
		$payment_status = '';
		$refunded_amount = '';
		$discount_amount = '';
		$coupon_discount = '';
		$order_date = $orders_data->CreatedAt;
		$order_last_modified_date = $orders_data->UpdatedAt;
		//Save Order Model details
		if (empty($order_model)) {
		    $order_model = new Orders();
		}
		$order_model->customer_id = $customer_id;
		$order_model->channel_abb_id = $prefix_order_id;
		$order_model->order_status = $order_status;
		$order_model->product_qauntity = $product_qauntity;
		$order_model->bill_street_1 = $bill_street_1;
		$order_model->bill_street_2 = $bill_street_2;
		$order_model->bill_city = $bill_city;
		$order_model->bill_state = $bill_state;
		$order_model->bill_zip = $bill_zip;
		$order_model->bill_country = $bill_country;
		$order_model->bill_country_iso = $bill_country_iso;
		$order_model->ship_street_1 = $ship_street_1;
		$order_model->ship_street_2 = $bill_street_2;
		$order_model->ship_city = $ship_city;
		$order_model->ship_state = $ship_state;
		$order_model->ship_zip = $ship_zip;
		$order_model->ship_country = $ship_country;
		$order_model->ship_country_iso = $ship_country_iso;
		$order_model->shipping_address = $shipping_address;
		$order_model->billing_address = $billing_address;
		$order_model->base_shipping_cost = $base_shipping_cost;
		$order_model->shipping_cost_tax = $shipping_cost_tax;
		$order_model->base_handling_cost = $base_handling_cost;
		$order_model->handling_cost_tax = $handling_cost_tax;
		$order_model->base_wrapping_cost = $base_wrapping_cost;
		$order_model->wrapping_cost_tax = $wrapping_cost_tax;
		$order_model->payment_method = $payment_method;
		$order_model->payment_provider_id = $payment_provider_id;
		$order_model->payment_status = $payment_status;
		$order_model->refunded_amount = $refunded_amount;
		$order_model->discount_amount = $discount_amount;
		$order_model->coupon_discount = $coupon_discount;
		$order_model->total_amount = $total_amount;
		$order_model->order_date = $order_date;
		$order_model->channel_accquired = $channel_type;

		$order_model->updated_at = date('Y-m-d H:i:s', strtotime($order_last_modified_date));
		$order_model->created_at = date('Y-m-d H:i:s', strtotime($order_date));
		//Save Elliot User id
		$order_model->elliot_user_id = $user->id;
		if ($order_model->save(false)) {


		    $parameters = array(
			'UserID' => $user_email,
			'Version' => '1.0',
			'Action' => 'GetOrderItems',
			'Format' => 'JSON',
			'Timestamp' => date('c')
			, 'OrderId' => $lazada_id
		    );

		    ksort($parameters);
		    $encoded = array();
		    foreach ($parameters as $name => $value) {
			$encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
		    }

		    $concatenated = implode('&', $encoded);
		    $parameters['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, $api_key, false));
		    $queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);
		    $ch = curl_init();
		    curl_setopt($ch, CURLOPT_URL, $url . "?" . $queryString);
		    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		    $data = curl_exec($ch);
		    curl_close($ch);
		    $response_decoded = json_decode($data);
		    $db_order_id = $order_model->order_ID;

		    if (isset($response_decoded) and ! empty($response_decoded) and isset($response_decoded->SuccessResponse)) {
			$order_products_lazada = $response_decoded->SuccessResponse->Body->OrderItems;
//echo"<pre>";print_R($order_products_lazada);die;
			if (isset($order_products_lazada) and ! empty($order_products_lazada)) {
			    foreach ($order_products_lazada as $product_dtails_data) {
				$product_details_check = Products::find()->Where(['product_name' => $product_dtails_data->Name])->one();
//                                        print_R($product_details_check);
				if (!empty($product_details_check)) {
				    $product_id = $product_details_check->id;
				} else {
				    //Check for custom product and save a custom product in product table
				    $product_details_custom = Products::find()->where(['channel_abb_id' => 'LZ' . $product_dtails_data->Sku])->one();
				    if (empty($product_details_custom)) {
					$product_custom_Model = new Products();
					$product_custom_Model->channel_abb_id = "LZ" . $product_dtails_data->Sku;
					$custom_product_name = $product_dtails_data->Name;
					$custom_product_sku = isset($product_dtails_data->Sku) ? $product_dtails_data->Sku : "";
					$product_custom_Model->product_name = $custom_product_name;
					$product_custom_Model->SKU = $custom_product_sku;
					$product_custom_Model->UPC = "";
					$product_custom_Model->EAN = "";
					$product_custom_Model->Jan = "";
					$product_custom_Model->ISBN = "";
					$product_custom_Model->MPN = "";
					$product_custom_Model->brand = "";
					$product_custom_Model->product_status = 'in_active';
					$product_custom_Model->elliot_user_id = $user->id;

					if ($product_custom_Model->save(false)) {
					    $product_id = $product_custom_Model->id;

					    $CategoryAbbrivation = ProductAbbrivation::find()->Where(['channel_abb_id' => 'LZ' . $product_dtails_data->Sku, 'product_id' => $product_custom_Model->id])->one();
					    if (empty($CategoryAbbrivation)) {
						$CategoryAbbrivation = new ProductAbbrivation();
					    }
					    $CategoryAbbrivation->channel_abb_id = 'LZ' . $product_dtails_data->Sku;
					    $CategoryAbbrivation->product_id = $product_custom_Model->id;
					    $CategoryAbbrivation->channel_accquired = $channel_type;
					    $CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
					    $CategoryAbbrivation->save(false);
					}

					//Product Channel/Store Table Entry
					$custom_productChannelModel = ProductChannel::find()->Where(['channel_id' => $channel_id, 'product_id' => $product_custom_Model->id])->one();
					if (empty($custom_productChannelModel)) {
					    $custom_productChannelModel = new ProductChannel;
					}
					$custom_productChannelModel->channel_id = $channel_id;
					$custom_productChannelModel->product_id = $product_custom_Model->id;
					$custom_productChannelModel->status = 'no';
					$custom_productChannelModel->created_at = date('Y-m-d H:i:s', time());
					$custom_productChannelModel->save(false);
					/* If order Product Already exist in database */
				    } else {

					$product_id = $product_custom_Model->id;
					/* Save Product Abbrivation Id */
					$ProductChannel_check = ProductChannel::find()->Where(['channel_id' => $channel_id, 'product_id' => $product_custom_Model->id])->one();
					if (empty($ProductChannel_check)) {

					    $CategoryAbbrivation = new ProductAbbrivation();
					    $CategoryAbbrivation->channel_abb_id = 'LZ' . $product_dtails_data->Sku;
					    $CategoryAbbrivation->product_id = $product_custom_Model->id;
					    $CategoryAbbrivation->channel_accquired = $channel_type;
					    $CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
					    $CategoryAbbrivation->save(false);

					    //Product Channel/Store Table Entry
					    $custom_productChannelModel = new ProductChannel;
					    $custom_productChannelModel->channel_id = $channel_id;
					    $custom_productChannelModel->product_id = $product_custom_Model->id;
					    $custom_productChannelModel->status = 'no';
					    $custom_productChannelModel->created_at = date('Y-m-d H:i:s', time());
					    $custom_productChannelModel->save(false);
					}
				    }
				}
				$product_name = $product_dtails_data->Name;
				$product_qty = "";
				$order_product_sku = $product_dtails_data->Sku;
				$db_order_id = $order_model->order_ID;
				$product_order_check = OrdersProducts::find()->Where(['order_Id' => $db_order_id, 'product_Id' => $product_id, 'order_product_sku' => $order_product_sku])->one();
				if (empty($product_order_check)) {
				    $product_order_quantity = 1;
				    $order_products_model = new OrdersProducts;
				    $order_products_model->order_Id = $db_order_id;
				    $order_products_model->product_Id = $product_id;
				    $order_products_model->qty = $product_order_quantity;
				    $order_products_model->price = @$product_dtails_data->ItemPrice * $conversion_rate * $product_order_quantity;
				    $order_products_model->order_product_sku = $order_product_sku;
				    $order_products_model->created_at = $order_model->created_at;
				    //Save Elliot User id
				    $order_products_model->elliot_user_id = $user->id;
				    $order_products_model->save(false);
				} else {
				    $product_order_quantity = $product_order_check->qty + 1;
				    $product_order_check->price = @$product_dtails_data->ItemPrice * $conversion_rate * $product_order_quantity;
				    $product_order_check->qty = $product_order_quantity;
				    $product_order_check->save(false);
				}
			    }
			}
			$order_channels_model = OrderChannel::find()->Where(['channel_id' => $channel_id, 'order_id' => $db_order_id])->one();
			if (empty($order_channels_model)) {
			    $order_channels_model = new OrderChannel();
			}
			$order_channels_model->order_id = $db_order_id;
			$order_channels_model->elliot_user_id = $user->id;
			$order_channels_model->channel_id = $channel_id;
			$order_channels_model->created_at = $order_model->created_at;
			$order_channels_model->save(false);
		    }
		}
	    }
	}


	return true;
    }

    function actionGetproducts() {


	$parameters = array(
	    // The user ID for which we are making the call.
	    'UserID' => 'info@fauxfreckles.com',
	    // The API version. Currently must be 1.0
	    'Version' => '1.0',
	    // The API method to call.
	    'Action' => 'GetProducts',
	    // The format of the result.
	    'Format' => 'JSON',
	    // The current time formatted as ISO8601
	    'Timestamp' => date('c')
//    'Timestamp' => $utc 
	);

// Sort parameters by name.
	ksort($parameters);

// URL encode the parameters.
	$encoded = array();
	foreach ($parameters as $name => $value) {
	    $encoded[] = rawurlencode($name) . '=' . rawurlencode($value);
	}

// Concatenate the sorted and URL encoded parameters into a string.
	$concatenated = implode('&', $encoded);


// Compute signature and add it to the parameters.
	$parameters['Signature'] = rawurlencode(hash_hmac('sha256', $concatenated, 'JeVfOHaBm0RdndrqR1HsZGyL20xHwGSxi3gngcsvOaCnPE_1nsf2Yb7f', false));

// Build Query String
	$queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);
	$url = "https://api.sellercenter.lazada.com.my/";

// Open cURL connection
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url . "?" . $queryString);

// Save response to the variable $data
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	$data = curl_exec($ch);
//            print_r($parameters);
// Close Curl connection
	curl_close($ch);
	$response = json_decode($data, true);
	echo'<pre>';
	print_r($response);
	die;
    }

    function actionDashboard() {
	$user = Yii::$app->user->identity;
	$user_id = $user->id;

	$products_by_volume = OrdersProducts::find()
		->select(['sum(qty) AS product_quantity', 'product_Id'])
		->where(['elliot_user_id' => $user_id])
		->groupBy(['product_id'])
		->orderBy(['product_quantity' => SORT_DESC])
		->with(['product' => function($query) {
			$query->where(['not like', 'product_status', 'in_active']);
			$query->select('id, price, product_name');
		    },
		    'product.productImages' => function($query) {
			$query->select('product_ID,link');
		    }])
		->asArray()
		->all();


	$products_by_revenue = OrdersProducts::find()
		->select(['sum(price) AS product_price', 'product_Id'])
		->where(['elliot_user_id' => $user_id])
		->groupBy(['product_id'])
		->orderBy(['product_price' => SORT_DESC])
		->with(['product' => function($query) {
			$query->where(['not like', 'product_status', 'in_active']);
			$query->select('id, price, product_name,country_code');
		    },
		    'product.productImages' => function($query) {
			$query->select('product_ID,link');
		    }])
		->asArray()
		->all();

	$countries = \backend\models\StoreDetails::find()->select(['id', 'country_code', 'country'])->distinct()->asArray()->all();
	$countries = ArrayHelper::map($countries, 'country_code', 'country');
//        echo"<pre>";            print_R($countries);die;
	if (isset($_GET['$$']) and ! empty($_GET['$$'])) {
	    echo "<pre>";
	    print_r($products_by_volume);
	    print_r($products_by_revenue);
	    print_r($countries);
	    die;
	}

	return $this->render('dashboard', [
		    'products_by_volume' => $products_by_volume,
		    'products_by_revenue' => $products_by_revenue,
		    'countries' => $countries,
		    'user' => $user
			]
	);
    }

    function actionGetFilteredProducts() {
	$user = Yii::$app->user->identity;
	$user_id = $user->id;
	$this->layout = '@app/path/to/ajax';
	$product_ids_by_region = [];
	$products = [];
	$product_region_ids_volume = [];
	$product_region_ids_revenue = [];
	$currency = $user->currency;
	$currency = strtolower($currency);
	$symbol = \backend\models\CurrencySymbols::find()->select(['symbol'])->where(['name' => $currency])->one();
	if (isset($_POST)) {

	    $region_volume = !empty($_POST['country_code_volume']) ? $_POST['country_code_volume'] : '';
	    $product_ids_by_region_volume = Products::find()->where(['country_code' => $region_volume])->select(['id'])->asArray()->all();
	    if (isset($product_ids_by_region_volume) and ! empty($product_ids_by_region_volume)) {
		foreach ($product_ids_by_region_volume as $single) {
		    $product_region_ids_volume[] = $single['id'];
		}
	    }

	    $region_revenue = !empty($_POST['country_code_revenue']) ? $_POST['country_code_revenue'] : '';
	    $product_ids_by_region_revenue = Products::find()->where(['country_code' => $region_revenue])->select(['id'])->asArray()->all();
	    if (isset($product_ids_by_region_revenue) and ! empty($product_ids_by_region_revenue)) {
		foreach ($product_ids_by_region_revenue as $single) {
		    $product_region_ids_revenue[] = $single['id'];
		}
	    }

	    if (isset($region_volume) and $_POST['id'] == 'volumeSelect') {
		$products = OrdersProducts::find()
			->select(['sum(qty) AS product_quantity', 'product_Id'])
			->where(['elliot_user_id' => $user_id])
			->andFilterWhere([
			    'and',
				['in', 'product_id', $product_region_ids_volume],
			])
			->groupBy(['product_id'])
			->orderBy(['product_quantity' => SORT_DESC])
			->with(['product' => function($query) {
				$query->where(['not like', 'product_status', 'in_active']);
				$query->select('id, price, product_name');
			    },
			    'product.productImages' => function($query) {
				$query->select('product_ID,link');
			    }])
//                            ->limit(20)
			->asArray()
			->all();
	    }


	    if (isset($region_revenue) and $_POST['id'] == 'revenueSelect') {
		$products = OrdersProducts::find()
			->select(['sum(price) AS product_price', 'product_Id'])
			->where(['elliot_user_id' => $user_id])
			->andFilterWhere([
			    'and',
				['in', 'product_id', $product_region_ids_revenue],
			])
			->groupBy(['product_id'])
			->orderBy(['product_price' => SORT_DESC])
			->with(['product' => function($query) {
				$query->where(['not like', 'product_status', 'in_active']);
				$query->select('id, price, product_name');
			    },
			    'product.productImages' => function($query) {
				$query->select('product_ID,link');
			    }])
//                            ->limit(20)
			->asArray()
			->all();
	    }
	}

	return $this->renderAjax('getproducts', [
		    'products' => $products,
		    'user' => $user,
		    'symbol' => $symbol->symbol,
			]
	);
    }

    function actionGetFilteredProductsByVolume() {
	$user = Yii::$app->user->identity;
	$user_id = $user->id;

	$filtered_channel_connection_id = [];
	$filtered_store_connection_id = [];
	$connectedstore = [];
	$connectedchannel = [];
	$channel_name = '';
	$currency = $user->currency;
	$currency = strtolower($currency);
	$symbol = \backend\models\CurrencySymbols::find()->select(['symbol'])->where(['name' => $currency])->one();
	if (isset($_POST['connection_id']) and ! empty($_POST['connection_id'])) {

	    if (isset($_POST['type']) and ! empty($_POST['type'])) {
		if ($_POST['type'] == 'store') {
		    $filtered_store_connection_id = $_POST['connection_id'];
		} elseif ($_POST['type'] == 'channel') {
		    $filtered_channel_connection_id = $_POST['connection_id'];
		    $channel_connection_data = \backend\models\ChannelConnection::find()->where(['channel_connection_id' => $filtered_channel_connection_id])->with('storesDetails')->one();
		    $channel_name = $channel_connection_data->storesDetails->channel_accquired;
		}
	    }
	}

	$order_ids = [];
	if (!empty($filtered_channel_connection_id)) {
	    $connectedchannel = Orders::find()->select('order_ID')->andFilterWhere([
			'and',
			    ['=', 'channel_accquired', $channel_name],
		    ])->andWhere(['elliot_user_id' => $user_id])->asArray()->all();
	    if (isset($connectedchannel) and ! empty($connectedchannel)) {
		foreach ($connectedchannel as $single) {
		    $order_ids[] = $single['order_ID'];
		}
	    }
	} elseif (!empty($filtered_store_connection_id)) {
	    $connectedstore = Orders::find()->select('order_ID')->andFilterWhere([
			'and',
			    ['=', 'mul_store_id', $filtered_store_connection_id],
		    ])->andWhere(['elliot_user_id' => $user_id])->asArray()->all();
	    if (isset($connectedstore) and ! empty($connectedstore)) {
		foreach ($connectedstore as $single) {
		    $order_ids[] = $single['order_ID'];
		}
	    }
	}
	$andWhere = 'andFilterWhere';
	if ((!empty($filtered_channel_connection_id) or ! empty($filtered_store_connection_id))) {
	    $andWhere = 'andWhere';
	    if (isset($order_ids) and ! empty($order_ids)) {
		$andWhere = 'andFilterWhere';
	    }
	}
	$filter = [];


	$data_type = Yii::$app->request->Post('data');
	/* Show Data According to Week */
	if ($data_type == 'week') {
	    //current date
	    $current_date = date('Y-m-d');
	    //6 days ago date
	    $previous_date = date('Y-m-d h:i:s', strtotime('-7 days'));
	    $filter = ['between', 'date(created_at)', $previous_date, $current_date];
	}

	if ($data_type == 'month') {
	    //current date
	    $current_date = date('Y-m-d');
	    //6 days ago date
	    $previous_date = date('Y-m-d', strtotime('-30 days'));
	    $filter = ['between', 'date(created_at)', $previous_date, $current_date];
	}

	if ($data_type == 'today') {
	    //current date
	    $previous_date = $current_date = date('Y-m-d');
	    $filter = ['between', 'date(created_at)', $previous_date, $current_date];
	}

	if ($data_type == 'year') {
	    //current date
	    $current_date = date('Y');
	    $filter = ['=', 'year(created_at)', $current_date];
	}
	if ($data_type == 'quarter') {
	    //current date
	    $current_date = date('Y-m-d');
	    $previous_date = date('Y-m-d', strtotime('-3 months'));
	    $filter = ['between', 'date(created_at)', $previous_date, $current_date];
	}


	// for daterange

	if (!empty($_POST['daterange'])) {
	    $daterange = $_POST['daterange'];
	}
	if ($data_type == 'dateRange' and ! empty($daterange)) {
	    $date = explode('-', $daterange);
	    $previous_date = date('Y-m-d', strtotime($date[0]));
	    $current_date = date('Y-m-d', strtotime($date[1]));
	    $filter = ['between', 'date(created_at)', $previous_date, $current_date];
	}

	$products = OrdersProducts::find()
		->select(['sum(qty) AS product_quantity', 'product_Id'])
		->where(['elliot_user_id' => $user_id])
		->$andWhere([
		    'and',
			['in', 'order_ID', $order_ids],
		])
		->andFilterWhere($filter)
		->groupBy(['product_id'])
		->orderBy(['product_quantity' => SORT_DESC])
		->with(['product' => function($query) {
			$query->where(['not like', 'product_status', 'in_active']);
			$query->select('id, price, product_name');
		    },
		    'product.productImages' => function($query) {
			$query->select('product_ID,link');
		    }])
//                            ->limit(20)
		->asArray()
		->all();

	$products_count = 0;
	if (isset($products) and ! empty($products)) {
	    foreach ($products as $single) {
		if (isset($single['product']) and ! empty($single['product'])) {
		    $products_count++;
		}
	    }
	}

	return $this->renderAjax('getproducts', [
		    'products' => $products,
		    'user' => $user,
		    'symbol' => $symbol->symbol,
		    'products_count' => $products_count,
			]
	);
    }

    function actionGetFilteredProductsByRevenue() {
	$user = Yii::$app->user->identity;
	$user_id = $user->id;
	$filtered_channel_connection_id = [];
	$filtered_store_connection_id = [];
	$connectedstore = [];
	$connectedchannel = [];
	$channel_name = '';
	$currency = $user->currency;
	$currency = strtolower($currency);
	$symbol = \backend\models\CurrencySymbols::find()->select(['symbol'])->where(['name' => $currency])->one();
	if (isset($_POST['connection_id']) and ! empty($_POST['connection_id'])) {

	    if (isset($_POST['type']) and ! empty($_POST['type'])) {
		if ($_POST['type'] == 'store') {
		    $filtered_store_connection_id = $_POST['connection_id'];
		} elseif ($_POST['type'] == 'channel') {
		    $filtered_channel_connection_id = $_POST['connection_id'];
		    $channel_connection_data = \backend\models\ChannelConnection::find()->where(['channel_connection_id' => $filtered_channel_connection_id])->with('storesDetails')->one();
		    $channel_name = $channel_connection_data->storesDetails->channel_accquired;
		}
	    }
	}

	$order_ids = [];
	if (!empty($filtered_channel_connection_id)) {
	    $connectedchannel = Orders::find()->select('order_ID')->andFilterWhere([
			'and',
			    ['=', 'channel_accquired', $channel_name],
		    ])->andWhere(['elliot_user_id' => $user_id])->asArray()->all();
	    if (isset($connectedchannel) and ! empty($connectedchannel)) {
		foreach ($connectedchannel as $single) {
		    $order_ids[] = $single['order_ID'];
		}
	    }
	} elseif (!empty($filtered_store_connection_id)) {
	    $connectedstore = Orders::find()->select('order_ID')->andFilterWhere([
			'and',
			    ['=', 'mul_store_id', $filtered_store_connection_id],
		    ])->andWhere(['elliot_user_id' => $user_id])->asArray()->all();
	    if (isset($connectedstore) and ! empty($connectedstore)) {
		foreach ($connectedstore as $single) {
		    $order_ids[] = $single['order_ID'];
		}
	    }
	}
	$andWhere = 'andFilterWhere';
	if ((!empty($filtered_channel_connection_id) or ! empty($filtered_store_connection_id))) {
	    $andWhere = 'andWhere';
	    if (isset($order_ids) and ! empty($order_ids)) {
		$andWhere = 'andFilterWhere';
	    }
	}
	$filter = [];


	$data_type = Yii::$app->request->Post('data');
	/* Show Data According to Week */
	if ($data_type == 'week') {
	    //current date
	    $current_date = date('Y-m-d', time());
	    //6 days ago date
	    $previous_date = date('Y-m-d h:i:s', strtotime('-7 days'));
	    $filter = ['between', 'date(created_at)', $previous_date, $current_date];
	}

	if ($data_type == 'month') {
	    //current date
	    $current_date = date('Y-m-d');
	    //6 days ago date
	    $previous_date = date('Y-m-d', strtotime('-30 days'));
	    $filter = ['between', 'date(created_at)', $previous_date, $current_date];
	}

	if ($data_type == 'today') {
	    //current date
	    $previous_date = $current_date = date('Y-m-d');
	    $filter = ['between', 'date(created_at)', $previous_date, $current_date];
	}

	if ($data_type == 'year') {
	    //current date
	    $current_date = date('Y');
	    $filter = ['=', 'year(created_at)', $current_date];
	}
	if ($data_type == 'quarter') {
	    //current date
	    $current_date = date('Y-m-d');
	    $previous_date = date('Y-m-d', strtotime('-3 months'));
	    $filter = ['between', 'date(created_at)', $previous_date, $current_date];
	}


	// for daterange

	if (!empty($_POST['daterange'])) {
	    $daterange = $_POST['daterange'];
	}
	if ($data_type == 'dateRange' and ! empty($daterange)) {
	    $date = explode('-', $daterange);
	    $previous_date = date('Y-m-d', strtotime($date[0]));
	    $current_date = date('Y-m-d', strtotime($date[1]));
	    $filter = ['between', 'date(created_at)', $previous_date, $current_date];
	}
	$products = OrdersProducts::find()
		->select(['sum(price) AS product_price', 'product_Id'])
		->where(['elliot_user_id' => $user_id])
		->$andWhere([
		    'and',
			['in', 'order_ID', $order_ids],
		])
		->andFilterWhere($filter)
		->groupBy(['product_id'])
		->orderBy(['product_price' => SORT_DESC])
		->with(['product' => function($query) {
			$query->where(['not like', 'product_status', 'in_active']);
			$query->select('id, price, product_name');
		    },
		    'product.productImages' => function($query) {
			$query->select('product_ID,link');
		    }])
//                            ->limit(20)
		->asArray()
		->all();
	$products_count = 0;
	if (isset($products) and ! empty($products)) {
	    foreach ($products as $single) {
		if (isset($single['product']) and ! empty($single['product'])) {
		    $products_count++;
		}
	    }
	}
	return $this->renderAjax('getproducts', [
		    'products' => $products,
		    'user' => $user,
		    'symbol' => $symbol->symbol,
		    'products_count' => $products_count,
			]
	);
    }

    public function LazadaChannelImport($user_id, $channel_connection_id) {

	$others_details = array(
	    "contact_name" => '',
	    "company" => "",
	    "address" => '',
	    "city" => '',
	    "province" => '',
	    "country" => 'Malaysia',
	    "post_code" => '',
	    "phone" => '',
	    "currency" => 'MYR'
	);

	$others_details_serialized = serialize($others_details);

	$save_store_details = \backend\models\StoreDetails::find()->where(['channel_connection_id' => $channel_connection_id])->one();
	if (empty($save_store_details)) {
	    $save_store_details = new \backend\models\StoreDetails();
	    $save_store_details->channel_connection_id = $channel_connection_id;
	    $save_store_details->store_connection_id = null;
	    $save_store_details->store_name = '';
	    $save_store_details->store_url = '';
	    $save_store_details->channel_accquired = 'Lazada Malaysia';
	    $save_store_details->country = 'Malaysia';
	    $save_store_details->country_code = 'MY';
	    $save_store_details->currency = 'MYR';
	    $save_store_details->currency_symbol = 'RM';
	    $save_store_details->others = $others_details_serialized;
	    $save_store_details->created_at = date('Y-m-d H:i:s', time());
	    $save_store_details->save(false);
	}
    }

    function actionGraph1() {
	$final_new_customer_list = [];
	$final_repeat_customer_list = [];
	$final_customers = [];
	$data_up_down = 0;
	$data_current_year = 0;
	$data_previous_year = 0;
	if (isset($_REQUEST) and ! empty($_REQUEST) and isset($_REQUEST['type']) and ! empty($_REQUEST['type'])) {
	    $post = $_REQUEST['type'];


	    if ($post == 'year') {
		$current_date = date('d');
		$prev_year = date('Y');

		$final_new_customer = [];
		$final_repeat_customer = [];
		for ($i = 12; $i >= 1; $i--) {
		    $days_array_current_year = [];
		    $days_array_last_year = [];
		    $date_current = date('m', strtotime('-' . $i . ' months'));
		    $current_month = date('M', strtotime('-' . $i . ' months'));
		    $date_previous = date('m', strtotime('-1 years', strtotime($date_current)));
		    //for 2017
		    $current_month_current_day = \backend\models\CustomerUser::find()->select(['customer_ID'])->with(['customerabbrivation' => function($query) use ($date_current) {
				    $query->where(['=', 'month(elliot_date)', $date_current]);
				    $query->select(['id', 'customer_id', 'elliot_date']);
				}])->asArray()->all();
		    if (isset($current_month_current_day) and ! empty($current_month_current_day)) {
			foreach ($current_month_current_day as $single_current_month_current_day) {
			    if (isset($single_current_month_current_day['customerabbrivation']) and ! empty($single_current_month_current_day['customerabbrivation'])) {
				$days_array_current_year[] = $single_current_month_current_day['customer_ID'];
			    }
			}
		    }
		    $current_month_current_day_last_year = \backend\models\CustomerUser::find()->select(['customer_ID'])->with(['customerabbrivation' => function($query) use ($date_previous) {
				    $query->where(['=', 'month(elliot_date)', $date_previous]);
				    $query->select(['id', 'customer_id', 'elliot_date']);
				}])->asArray()->all();
		    if (isset($current_month_current_day_last_year) and ! empty($current_month_current_day_last_year)) {
			foreach ($current_month_current_day_last_year as $single_current_month_current_day_last_year) {
			    if (isset($single_current_month_current_day_last_year['customerabbrivation']) and ! empty($single_current_month_current_day_last_year['customerabbrivation'])) {
				$days_array_last_year[] = $single_current_month_current_day_last_year['customer_ID'];
			    }
			}
		    }
		    $new_customer_count = 0;
		    $repeat_customer_count = 0;
		    if (isset($days_array_current_year) and ! empty($days_array_current_year)) {
			foreach ($days_array_current_year as $single) {
			    if (isset($days_array_last_year) and ! empty($days_array_last_year) and in_array($single, $days_array_last_year)) {
				$repeat_customer_count++;
			    } else {
				$new_customer_count++;
			    }
			}
		    }

		    $data_previous_year += $repeat_customer_count;
		    $data_current_year += $new_customer_count;
		    $final_customers[] = ['date' => $current_month, 'new_customer_count' => $new_customer_count, 'repeat_customer_count' => $repeat_customer_count];
		}
	    }
	    if ($post == 'month') {
		$current_date = date('d');
		$prev_year = date('Y');
		$final_new_customer = [];
		$final_repeat_customer = [];
		for ($i = $current_date - 1; $i >= 0; $i--) {
		    $days_array_current_year = [];
		    $days_array_last_year = [];
		    $date_current = date('Y-m-d', strtotime('-' . $i . ' days'));
		    $date_current_text = date('M-d', strtotime('-' . $i . ' days'));

		    $date_previous = date('Y-m-d', strtotime('-1 years', strtotime($date_current)));
		    //for 2017
		    $current_month_current_day = \backend\models\CustomerUser::find()->select(['customer_ID'])->with(['customerabbrivation' => function($query) use ($date_current) {
				    $query->where(['=', 'date(elliot_date)', $date_current]);
				    $query->select(['id', 'customer_id', 'elliot_date']);
				}])->asArray()->all();
		    if (isset($current_month_current_day) and ! empty($current_month_current_day)) {
			foreach ($current_month_current_day as $single_current_month_current_day) {
			    if (isset($single_current_month_current_day['customerabbrivation']) and ! empty($single_current_month_current_day['customerabbrivation'])) {
				$days_array_current_year[] = $single_current_month_current_day['customer_ID'];
			    }
			}
		    }
		    $current_month_current_day_last_year = \backend\models\CustomerUser::find()->select(['customer_ID'])->with(['customerabbrivation' => function($query) use ($date_previous) {
				    $query->where(['=', 'date(elliot_date)', $date_previous]);
				    $query->select(['id', 'customer_id', 'elliot_date']);
				}])->asArray()->all();
		    if (isset($current_month_current_day_last_year) and ! empty($current_month_current_day_last_year)) {
			foreach ($current_month_current_day_last_year as $single_current_month_current_day_last_year) {
			    if (isset($single_current_month_current_day_last_year['customerabbrivation']) and ! empty($single_current_month_current_day_last_year['customerabbrivation'])) {
				$days_array_last_year[] = $single_current_month_current_day_last_year['customer_ID'];
			    }
			}
		    }
		    $new_customer_count = 0;
		    $repeat_customer_count = 0;
		    if (isset($days_array_current_year) and ! empty($days_array_current_year)) {
			foreach ($days_array_current_year as $single) {
			    if (isset($days_array_last_year) and ! empty($days_array_last_year) and in_array($single, $days_array_last_year)) {
				$repeat_customer_count++;
			    } else {
				$new_customer_count++;
			    }
			}
		    }
		    $data_previous_year += $repeat_customer_count;
		    $data_current_year += $new_customer_count;

		    $final_customers[] = ['date' => $date_current_text, 'new_customer_count' => $new_customer_count, 'repeat_customer_count' => $repeat_customer_count];
		}
	    }

	    //for week
	    if ($post == 'week') {
		$current_date = date('d');
		$prev_year = date('Y');
		$final_new_customer = [];
		$final_repeat_customer = [];
		for ($i = 6; $i >= 0; $i--) {
		    $days_array_current_year = [];
		    $days_array_last_year = [];
		    $date_current = date('Y-m-d', strtotime('-' . $i . ' days'));
		    $date_current_text = date('M-d', strtotime('-' . $i . ' days'));

		    $date_previous = date('Y-m-d', strtotime('-1 years', strtotime($date_current)));
		    //for 2017
		    $current_month_current_day = \backend\models\CustomerUser::find()->select(['customer_ID'])->with(['customerabbrivation' => function($query) use ($date_current) {
				    $query->where(['=', 'date(elliot_date)', $date_current]);
				    $query->select(['id', 'customer_id', 'elliot_date']);
				}])->asArray()->all();
		    if (isset($current_month_current_day) and ! empty($current_month_current_day)) {
			foreach ($current_month_current_day as $single_current_month_current_day) {
			    if (isset($single_current_month_current_day['customerabbrivation']) and ! empty($single_current_month_current_day['customerabbrivation'])) {
				$days_array_current_year[] = $single_current_month_current_day['customer_ID'];
			    }
			}
		    }
		    $current_month_current_day_last_year = \backend\models\CustomerUser::find()->select(['customer_ID'])->with(['customerabbrivation' => function($query) use ($date_previous) {
				    $query->where(['=', 'date(elliot_date)', $date_previous]);
				    $query->select(['id', 'customer_id', 'elliot_date']);
				}])->asArray()->all();
		    if (isset($current_month_current_day_last_year) and ! empty($current_month_current_day_last_year)) {
			foreach ($current_month_current_day_last_year as $single_current_month_current_day_last_year) {
			    if (isset($single_current_month_current_day_last_year['customerabbrivation']) and ! empty($single_current_month_current_day_last_year['customerabbrivation'])) {
				$days_array_last_year[] = $single_current_month_current_day_last_year['customer_ID'];
			    }
			}
		    }
		    $new_customer_count = 0;
		    $repeat_customer_count = 0;
		    if (isset($days_array_current_year) and ! empty($days_array_current_year)) {
			foreach ($days_array_current_year as $single) {
			    if (isset($days_array_last_year) and ! empty($days_array_last_year) and in_array($single, $days_array_last_year)) {
				$repeat_customer_count++;
			    } else {
				$new_customer_count++;
			    }
			}
		    }
		    $data_previous_year += $repeat_customer_count;
		    $data_current_year += $new_customer_count;
		    $final_customers[] = ['date' => $date_current_text, 'new_customer_count' => $new_customer_count, 'repeat_customer_count' => $repeat_customer_count];
		}
	    }
	    //for quarter
	    if ($post == 'Quarter') {
//                echo'<pre>';
		$enddate = date('Y-m-d');
		$current_date = date('Y-m-d', strtotime('-3 months'));
		$datediff = strtotime($enddate) - strtotime($current_date);
		$numOfdays = floor($datediff / (60 * 60 * 24));
		$loop_val = floor($numOfdays / 7);
		$prev_year = date('Y');
//                echo $loop_val;
		$final_new_customer = [];
		$final_repeat_customer = [];
		for ($i = 1; $i <= $loop_val; $i++) {
		    $days_array_current_year = [];
		    $days_array_last_year = [];
		    if ($i == 1) {
			$date_current = date('W', strtotime($current_date));
		    } else {
			$date_current = date('W', strtotime('+' . $i * 7 . 'days', strtotime($current_date)));
		    }
		    $date_current_text = 'Week ' . $i;
		    $current_month_current_day = \backend\models\CustomerUser::find()->select(['customer_ID'])->with(['customerabbrivation' => function($query) use ($date_current) {
				    $query->andWhere(['=', 'WEEK(elliot_date)', $date_current]);
				    $query->andWhere(['=', 'Year(elliot_date)', date('Y', time())]);
				    $query->select(['id', 'customer_id', 'elliot_date']);
				}])->asArray()->all();
		    if (isset($current_month_current_day) and ! empty($current_month_current_day)) {
//                        print_R($current_month_current_day);
			foreach ($current_month_current_day as $single_current_month_current_day) {
			    if (isset($single_current_month_current_day['customerabbrivation']) and ! empty($single_current_month_current_day['customerabbrivation'])) {
				$days_array_current_year[] = $single_current_month_current_day['customer_ID'];
			    }
			}
		    }
		    $current_month_current_day_last_year = \backend\models\CustomerUser::find()->select(['customer_ID'])->with(['customerabbrivation' => function($query) use ($date_current) {
				    $query->andWhere(['=', 'WEEK(elliot_date)', $date_current]);
				    $query->andWhere(['=', 'Year(elliot_date)', date('Y', strtotime('-1 years'))]);
				    $query->select(['id', 'customer_id', 'elliot_date']);
				}])->asArray()->all();
		    if (isset($current_month_current_day_last_year) and ! empty($current_month_current_day_last_year)) {
//                        print_R($current_month_current_day_last_year);
			foreach ($current_month_current_day_last_year as $single_current_month_current_day_last_year) {
			    if (isset($single_current_month_current_day_last_year['customerabbrivation']) and ! empty($single_current_month_current_day_last_year['customerabbrivation'])) {
				$days_array_last_year[] = $single_current_month_current_day_last_year['customer_ID'];
			    }
			}
		    }

		    $new_customer_count = 0;
		    $repeat_customer_count = 0;
		    if (isset($days_array_current_year) and ! empty($days_array_current_year)) {
			foreach ($days_array_current_year as $single) {
			    if (isset($days_array_last_year) and ! empty($days_array_last_year) and in_array($single, $days_array_last_year)) {
				$repeat_customer_count++;
			    } else {
				$new_customer_count++;
			    }
			}
		    }
//                    echo $new_customer_count . '<br>' . $repeat_customer_count;
//                    echo'<br>';
		    $data_previous_year += $repeat_customer_count;
		    $data_current_year += $new_customer_count;
		    $final_customers[] = ['date' => $date_current_text, 'new_customer_count' => $new_customer_count, 'repeat_customer_count' => $repeat_customer_count];
		}
	    }
	    //for today
	    if ($post == 'day') {
		$current_date = date('Y-m-d', time());

		$final_new_customer = [];
		$final_repeat_customer = [];
		$j = 0;

		for ($i = 24; $i >= 1; $i--) {
		    $days_array_current_year = [];
		    $days_array_last_year = [];
		    if ($i == 1) {
			$date_current = date('H', time());
			$date_current_day = date('j', time());
			$itereated_date = date('Y-m-d', time());
		    } else {
			$date_current = date('H', strtotime('-' . $i . ' hour'));
			$date_current_day = date('j', strtotime('-' . $i . ' hour'));
			$itereated_date = date('Y-m-d', strtotime('-' . $i . ' hour'));
		    }
		    $date_current_text = date('h:i:s', strtotime('-' . $i . ' hour'));
		    if ($itereated_date == $current_date) {
			$current_month_current_day = \backend\models\CustomerUser::find()->select(['customer_ID'])->with(['customerabbrivation' => function($query) use ($date_current, $date_current_day) {
					$query->andWhere(['=', 'HOUR(elliot_date)', $date_current]);
					$query->andWhere(['=', 'day(elliot_date)', $date_current_day]);
					$query->andWhere(['=', 'Year(elliot_date)', date('Y', time())]);
					$query->select(['id', 'customer_id', 'elliot_date']);
				    }])->asArray()->all();
			if (isset($current_month_current_day) and ! empty($current_month_current_day)) {
			    foreach ($current_month_current_day as $single_current_month_current_day) {
				if (isset($single_current_month_current_day['customerabbrivation']) and ! empty($single_current_month_current_day['customerabbrivation'])) {
				    $days_array_current_year[] = $single_current_month_current_day['customer_ID'];
				}
			    }
			}
			$current_month_current_day_last_year = \backend\models\CustomerUser::find()->select(['customer_ID'])->with(['customerabbrivation' => function($query) use ($date_current, $date_current_day) {
					$query->andWhere(['=', 'HOUR(elliot_date)', $date_current]);
					$query->andWhere(['=', 'day(elliot_date)', $date_current_day]);
					$query->andWhere(['=', 'Year(elliot_date)', date('Y', strtotime('-1 years'))]);
					$query->select(['id', 'customer_id', 'elliot_date']);
				    }])->asArray()->all();
			if (isset($current_month_current_day_last_year) and ! empty($current_month_current_day_last_year)) {
			    foreach ($current_month_current_day_last_year as $single_current_month_current_day_last_year) {
				if (isset($single_current_month_current_day_last_year['customerabbrivation']) and ! empty($single_current_month_current_day_last_year['customerabbrivation'])) {
				    $days_array_last_year[] = $single_current_month_current_day_last_year['customer_ID'];
				}
			    }
			}
			$new_customer_count = 0;
			$repeat_customer_count = 0;
			if (isset($days_array_current_year) and ! empty($days_array_current_year)) {

			    foreach ($days_array_current_year as $single) {
				if (isset($days_array_last_year) and ! empty($days_array_last_year) and in_array($single, $days_array_last_year)) {
				    $repeat_customer_count++;
				} else {
				    $new_customer_count++;
				}
			    }
			}

			$data_previous_year += $repeat_customer_count;
			$data_current_year += $new_customer_count;

			$final_customers[] = ['date' => $date_current_text, 'new_customer_count' => $new_customer_count, 'repeat_customer_count' => $repeat_customer_count];
		    }
		}
	    }

	    if (!empty($_POST['daterange'])) {
		$daterange = $_POST['daterange'];
	    }
	    // for daterange
	    if ($post == 'dateRange' && !empty($daterange)) {
		$date = explode('-', $daterange);
		$startdate = date('Y-m-d', strtotime($date[0]));
		$enddate = date('Y-m-d', strtotime($date[1]));
		$datediff = strtotime($enddate) - strtotime($startdate);
		$numOfdays = floor($datediff / (60 * 60 * 24));
		if ($numOfdays < 30) {

		    $current_date = date('d');
		    $prev_year = date('Y');

		    $final_new_customer = [];
		    $final_repeat_customer = [];
		    for ($i = $numOfdays; $i >= 0; $i--) {
			$days_array_current_year = [];
			$days_array_last_year = [];
			$date_current = date('Y-m-d', strtotime('-' . $i . ' days', strtotime($enddate)));
			$date_current_text = date('M-d', strtotime('-' . $i . ' days', strtotime($enddate)));



			$date_previous = date('Y-m-d', strtotime('-1 years', strtotime($date_current)));
			//for 2017
			$current_month_current_day = \backend\models\CustomerUser::find()->select(['customer_ID'])->with(['customerabbrivation' => function($query) use ($date_current) {
					$query->where(['=', 'date(elliot_date)', $date_current]);
					$query->select(['id', 'customer_id', 'elliot_date']);
				    }])->asArray()->all();
			if (isset($current_month_current_day) and ! empty($current_month_current_day)) {
			    foreach ($current_month_current_day as $single_current_month_current_day) {
				if (isset($single_current_month_current_day['customerabbrivation']) and ! empty($single_current_month_current_day['customerabbrivation'])) {
				    $days_array_current_year[] = $single_current_month_current_day['customer_ID'];
				}
			    }
			}
			$current_month_current_day_last_year = \backend\models\CustomerUser::find()->select(['customer_ID'])->with(['customerabbrivation' => function($query) use ($date_previous) {
					$query->where(['=', 'date(elliot_date)', $date_previous]);
					$query->select(['id', 'customer_id', 'elliot_date']);
				    }])->asArray()->all();
			if (isset($current_month_current_day_last_year) and ! empty($current_month_current_day_last_year)) {
			    foreach ($current_month_current_day_last_year as $single_current_month_current_day_last_year) {
				if (isset($single_current_month_current_day_last_year['customerabbrivation']) and ! empty($single_current_month_current_day_last_year['customerabbrivation'])) {
				    $days_array_last_year[] = $single_current_month_current_day['customer_ID'];
				}
			    }
			}
			$new_customer_count = 0;
			$repeat_customer_count = 0;
			if (isset($days_array_current_year) and ! empty($days_array_current_year)) {
			    foreach ($days_array_current_year as $single) {
				if (isset($days_array_last_year) and ! empty($days_array_last_year) and in_array($single, $days_array_last_year)) {
				    $repeat_customer_count++;
				} else {
				    $new_customer_count++;
				}
			    }
			}
			$data_previous_year += $repeat_customer_count;
			$data_current_year += $new_customer_count;
			$final_customers[] = ['date' => $date_current_text, 'new_customer_count' => $new_customer_count, 'repeat_customer_count' => $repeat_customer_count];
		    }
		}
		if ($numOfdays > 30 && $numOfdays < 150) {
		    $loop_val = floor($numOfdays / 7);
		    $current_date = $startdate;


		    $final_new_customer = [];
		    $final_repeat_customer = [];

		    for ($i = 1; $i <= $loop_val; $i++) {
			$days_array_current_year = [];
			$days_array_last_year = [];
			if ($i == 1) {
			    $date_current = date('W', strtotime($current_date));
			} else {
			    $date_current = date('W', strtotime('+' . $i * 7 . 'days', strtotime($current_date)));
			}
			$date_current_text = 'Week ' . $i;

			$current_month_current_day = \backend\models\CustomerUser::find()->select(['customer_ID'])->with(['customerabbrivation' => function($query) use ($date_current) {
					$query->andWhere(['=', 'WEEK(elliot_date)', $date_current]);
					$query->andWhere(['=', 'Year(elliot_date)', date('Y', time())]);
					$query->select(['id', 'customer_id', 'elliot_date']);
				    }])->asArray()->all();
			if (isset($current_month_current_day) and ! empty($current_month_current_day)) {
			    foreach ($current_month_current_day as $single_current_month_current_day) {
				if (isset($single_current_month_current_day['customerabbrivation']) and ! empty($single_current_month_current_day['customerabbrivation'])) {
				    $days_array_current_year[] = $single_current_month_current_day['customer_ID'];
				}
			    }
			}
			$current_month_current_day_last_year = \backend\models\CustomerUser::find()->select(['customer_ID'])->with(['customerabbrivation' => function($query) use ($date_current) {
					$query->andWhere(['=', 'WEEK(elliot_date)', $date_current]);
					$query->andWhere(['=', 'Year(elliot_date)', date('Y', strtotime('-1 years'))]);
					$query->select(['id', 'customer_id', 'elliot_date']);
				    }])->asArray()->all();
			if (isset($current_month_current_day_last_year) and ! empty($current_month_current_day_last_year)) {
			    foreach ($current_month_current_day_last_year as $single_current_month_current_day_last_year) {
				if (isset($single_current_month_current_day_last_year['customerabbrivation']) and ! empty($single_current_month_current_day_last_year['customerabbrivation'])) {
				    $days_array_last_year[] = $single_current_month_current_day['customer_ID'];
				}
			    }
			}
			$new_customer_count = 0;
			$repeat_customer_count = 0;
			if (isset($days_array_current_year) and ! empty($days_array_current_year)) {
			    foreach ($days_array_current_year as $single) {
				if (isset($days_array_last_year) and ! empty($days_array_last_year) and in_array($single, $days_array_last_year)) {
				    $repeat_customer_count++;
				} else {
				    $new_customer_count++;
				}
			    }
			}
			$data_previous_year += $repeat_customer_count;
			$data_current_year += $new_customer_count;
			$final_customers[] = ['date' => $date_current_text, 'new_customer_count' => $new_customer_count, 'repeat_customer_count' => $repeat_customer_count];
		    }
		}
	    }


	    if ($data_current_year > 0) {
		$data_up_down = (($data_current_year - $data_previous_year) / $data_current_year) * 100;
		$data_up_down = round($data_up_down);
	    } else {
		$data_up_down = 0;
	    }
	    if (isset($_GET['sss']) and ! empty($_GET['sss'])) {
		echo'<pre>';
		print_R($final_repeat_customer);
		print_R($final_new_customer);
		print_R($final_new_customer_list);
		print_R($final_repeat_customer_list);
		die;
	    }
	}
	$no_data = false;
	if (empty($data_current_year) and empty($data_previous_year)) {
	    $no_data = true;
	}

	echo json_encode(['customers_data' => $final_customers, 'data_up_down' => $data_up_down, 'no_data' => $no_data]);
	die;
//        return $this->render('graph', [
//                    'customers' => $customers,
//                    'final_new_customer_list' => $final_new_customer_list,
//                    'final_repeat_customer_list' => $final_repeat_customer_list,
//        ]);
    }

    function actionCategorydelete() {
//        echo'ithe';
	$id = $_REQUEST['id'];
	$multi_store_id = $_REQUEST['multi_store_id'];
	if (isset($multi_store_id) and ! empty($multi_store_id) and ! empty($id)) {
	    echo $id;
	    echo $multi_store_id;
	    $category_abbrivation_records = \backend\models\CategoryAbbrivation::find()->where(['category_ID' => $id, 'mul_store_id' => $multi_store_id])->with(['category'])->count();

	    $category_abbrivation_records_all = \backend\models\CategoryAbbrivation::find()->where(['category_ID' => $id])->with(['category'])->count();
//            echo'<pre>';
//            print_R($category_abbrivation_records);
//            print_R($category_abbrivation_records_all);
//            die;
//            die;
	    if (isset($category_abbrivation_records) and ! empty($category_abbrivation_records)) {
		$count = count($category_abbrivation_records);
		if ($category_abbrivation_records == $category_abbrivation_records_all) {
		    Categories::find($id)->one()->delete();
		} else {
		    \backend\models\CategoryAbbrivation::deleteAll(['category_ID' => $id, 'mul_store_id' => $multi_store_id]);
		}
	    }
	}
    }

    public function actionTestCall() {
	$order_amount_total = Orders::find()->Where(['elliot_user_id' => Yii::$app->user->identity->id])->average('total_amount');
	echo $order_amount_total;
    }

}
