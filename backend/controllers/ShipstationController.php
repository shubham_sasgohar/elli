<?php

namespace backend\controllers;

use Yii;
use backend\models\Shipstation;
use backend\models\ShipstationSearch;
use backend\models\Orders;
use backend\models\Products;
use backend\models\ProductImages;
use backend\models\Countries;
use backend\models\StoresConnection;
use backend\models\Fulfillment;
use backend\models\Stores;
use Bigcommerce\Api\Connection;
use Bigcommerce\Api\Client as Bigcommerce;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\ProductVariation;
use yii\filters\AccessControl;
use yii\db\Query;

/**
 * ShipstationController implements the CRUD actions for Shipstation model.
 */
class ShipstationController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'update', 'invoice', 'genratexml'],
                'rules' => [
                        [
                        'actions' => ['signup', 'genratexml'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                        [
                        'actions' => ['logout', 'index', 'view', 'create', 'update', 'invoice', 'genratexml'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Shipstation models.
     * @return mixed
     */
    public function actionIndex() {

        $searchModel = new ShipstationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Shipstation model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Shipstation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Shipstation();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    public function actionGenratexml() {

        header('Content-type: text/xml');
        //Get current login User id
        $user_id = Yii::$app->request->get('shipid');

        // $user_id = 304;
        //$user_id = Yii::$app->user->identity->id;
        //Get All orders 
        $curr_url = explode(".", "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
        $uname = $curr_url[0];
        $new_db_name = $uname . '_elliot';
        $config = [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=' . $new_db_name . '',
            'username' => Yii::$app->params['DB_USERNAME'],
            'password' => Yii::$app->params['DB_PASSWORD'],
            'charset' => 'utf8',
        ];
        $db_merchant = Yii::createObject($config);
        $orders_data = $db_merchant->createCommand("SELECT * from orders WHERE elliot_user_id='" . $user_id . "'")->queryAll();
        // $orders_data = Orders::find()->Where(['elliot_user_id' => $user_id])->with('customer')->with('ordersProducts')->all();

        $data = '<?xml version="1.0" encoding="utf-8"?>';
        $data .= '<Orders>';
        foreach ($orders_data as $asorders) :
     
            
            //Get customers
            $customer_id = $asorders['customer_id'];
            $customer_data = $db_merchant->createCommand("SELECT * from Customer_User WHERE customer_ID='" . $customer_id . "'")->queryAll();
            //For Countries Abbrivation
            $countries_data = $db_merchant->createCommand("SELECT * from countries WHERE name ='" . $asorders['ship_country'] . "' OR sortname ='" . $asorders['ship_country'] . "'")->queryAll();
       
            //$countries_data = Countries::find()->Where(['name' => $asorders->ship_country])->one();
            $country_abb = $countries_data[0]['sortname'];
            $data .= '<Order>';
            $data .= '<OrderID>' . $asorders['order_ID'] . '</OrderID>';
            $data .= '<OrderNumber>' . $asorders['channel_abb_id'] . '</OrderNumber>';
            $data .= '<OrderDate>12/8/2011 21:56 PM</OrderDate>';
            $data .= '<OrderStatus>' . $asorders['order_status'] . '</OrderStatus>';
            $data .= '<LastModified>12/8/2011 21:56 PM</LastModified>';
            $data .= '<PaymentMethod>' . $asorders['payment_method'] . '</PaymentMethod>';
            $data .= '<OrderTotal>' . $asorders['total_amount'] . '</OrderTotal>';
            $data .= '<ShippingAmount>' . $asorders['shipping_cost_tax'] . '</ShippingAmount>';
            $data .= '<Customer>';
            $data .= '<CustomerCode>' . $customer_data[0]['email'] . '</CustomerCode>';
            $data .= '<BillTo>';
            $data .= '<Name>' . $customer_data[0]['first_name'] . '</Name>';
            $data .= '<Phone>' . $customer_data[0]['phoneno'] . '</Phone>';
            $data .= '<Email>' . $customer_data[0]['email'] . '</Email>';
            $data .= '</BillTo>';
            $data .= '<ShipTo>';
            $data .= '<Name>' . $customer_data[0]['first_name'] . '</Name>';
            $data .= '<Address1>' . $asorders['ship_street_1'] . '</Address1>';
            $data .= '<Address2>' . $asorders['ship_street_2'] . '</Address2>';
            $data .= '<City>' . $asorders['ship_city'] . '</City>';
            $data .= '<State>' . $asorders['ship_state'] . '</State>';
            $data .= '<PostalCode>' . $asorders['ship_zip'] . '</PostalCode>';
            $data .= '<Country>' . $country_abb . '</Country>';
            $data .= '<Phone>' . $customer_data[0]['phoneno'] . '</Phone>';
            $data .= '</ShipTo>';
            $data .= '</Customer>';
            $data .= '<Items>';
            //For product items
            $order_product = $db_merchant->createCommand("SELECT * from orders_products WHERE elliot_user_id='" . $user_id . "' AND order_Id= '" . $asorders['order_ID'] . "'")->queryAll();
            foreach ($order_product as $order_products) :
                
                if(!empty($product_details[0]['SKU'])):
                    $sku=$product_details[0]['SKU'];
                    else:
                    $sku='';
                endif;
                
                if(!empty($product_details[0]['product_name'])):
                    $product_name=$product_details[0]['product_name'];
                    else:
                    $product_name='';
                endif;

                
                $product_id = $order_products['product_Id'];
                $qty = $order_products['qty'];
                $elliot_user_id = $order_products['elliot_user_id'];
                $order_product_sku = $order_products['order_product_sku'];
                //Product Image
                $product_img_data = $db_merchant->createCommand("SELECT * from Product_images WHERE product_ID='" . $product_id . "'")->queryAll();
                // Products details
                $product_details = $db_merchant->createCommand("SELECT * from products WHERE id='" . $product_id . "'")->queryAll();
                $data .= '<Item>';
                $data .= '<SKU>' . $sku . '</SKU>';
                $data .= '<Name>' . $product_name . '</Name>';
                if (!empty($product_img_data)) :
                    $data .= '<ImageUrl>' . $product_img_data[0]['link'] . '</ImageUrl>';
                endif;
                $data .= '<Weight>' . $product_details[0]['weight'] . '</Weight>';
                $data .= '<Quantity>' . $qty . '</Quantity>';
                $data .= '<UnitPrice>' . $product_details[0]['price'] . '</UnitPrice>';
                //Product variations for Options
                $product_variations_data = $db_merchant->createCommand("SELECT * from product_variation WHERE elliot_user_id='" . $elliot_user_id . "' AND product_id='" . $product_id . "' AND item_value='" . $order_product_sku . "' ")->queryAll();
                if (!empty($product_variations_data)) :
                    $store_variotion_id = $product_variations_data[0]['store_variation_id'];
                    $variation = $db_merchant->createCommand("SELECT * from product_variation WHERE store_variation_id='" . $store_variotion_id . "' AND variation_id !='NULL' ")->queryAll();
                    $data .= '<Options>';
                    foreach ($variation as $variations):
                        // For options   
                        $data .= '<Option>';
                        $data .= '<Name>' . $variations['item_name'] . '</Name>';
                        $data .= '<Value>' . $variations['item_value'] . '</Value>';
                        $data .= '</Option>';
                    endforeach;

                    $data .= '</Options>';
                else :
                    $data .= '<Options>';
                    $data .= '<Option>';
                    $data .= '<Name>custom</Name>';
                    $data .= '<Value>custom</Value>';
                    $data .= '</Option>';
                    $data .= '</Options>';
                endif;

                $data .= '</Item>';
            endforeach;
            $data .= '</Items>';
            $data .= '</Order>';
        endforeach;
        $data .= '</Orders>';
        
        echo $data;
        if ($_REQUEST) :

            $config = yii\helpers\ArrayHelper::merge(
                            require($_SERVER['DOCUMENT_ROOT'].'/common/config/main.php'), require($_SERVER['DOCUMENT_ROOT'].'/common/users_config/' . $uname . '/main-local.php'), require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main.php'), require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main-local.php')
            );

            $application = new yii\web\Application($config);
            
        
            // $shipstation = $db_merchant->createCommand("SELECT * from warehouse WHERE elliot_user_id='" . $user_id . "'")->queryAll();
            $shipstation = Fulfillment::find()->Where(['elliot_user_id' => $user_id])->one();
     
            if (empty($shipstation)) {
                $date = date('Y-m-d h:i:s', time());
                $model = new Fulfillment();
                $model->elliot_user_id = $user_id;
                $model->name = 'Shipstation';
                $model->connected = 'Yes';
                $model->created_at = $date;
                if ($model->save(false)):
                   

                endif;
                //  $insert_ship_data = $db_merchant->createCommand("INSERT INTO warehouse( elliot_user_id, connected ,created_at ) VALUES (" . $user_id . " , Yes ," . $date . ")")->queryAll();
            }else {
                $data = Fulfillment::find()->Where(['elliot_user_id' => $user_id])->one();
                $data->connected = 'Yes';
                $data->save(false);
               
               
             // return $this->redirect('/warehouse/shipstation'); 
                //  $update_ship_data = $db_merchant->createCommand("UPDATE warehouse SET connected = 'Yes' WHERE elliot_user_id = " . $user_id . " ")->queryAll(); 
            }
        endif;


        die;
    }
    
public function abc() {

        return $this->redirect('/warehouse/shipstation');
    }

    /**
     * Creates a new Shipstation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionInstruction() {

        return $this->render('instruction');
    }

    /**
     * Updates an existing Shipstation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Shipstation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Shipstation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Shipstation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Shipstation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
