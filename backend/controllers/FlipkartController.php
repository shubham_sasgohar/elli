<?php

namespace backend\controllers;

ob_start();

//session_start();

use Yii;
use backend\models\CustomerUser;
use backend\models\User;
use backend\models\CustomerUserSearch;
use backend\models\Stores;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\Countries;
use backend\models\States;
use backend\models\Cities;
use backend\models\StoresConnection;
use Bigcommerce\Api\Client as Bigcommerce;
use backend\models\Orders;
use backend\models\OrdersProducts;
use backend\models\OrderChannel;
use backend\models\Products;
use backend\models\Categories;
use backend\models\ProductCategories;
use backend\models\ProductChannel;
use backend\models\ProductImages;
use Bigcommerce\Api\ShopifyClient as Shopify;
use backend\models\ProductVariation;
use backend\models\VariationsSet;
use backend\models\Variations;
use backend\models\VariationsItemList;
use backend\models\MerchantProducts;
use Automattic\WooCommerce\Client as Woocommerce;
use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use Facebook\FacebookRequestException;
use Facebook\FacebookRedirectLoginHelper;
use backend\models\ProductAbbrivation;

/**
 * CustomerUserController implements the CRUD actions for CustomerUser model.
 */
class FlipkartController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['view', 'create', 'update', 'savecustomer', 'create-customer-hook', 'orderbigcommerce', 'orderhookcreate', 'producthooksbg', 'skuupdategb', 'productskuhooksbg', 'categoryhooksbg', 'shopify-product-create', 'shopify-product-update', 'shopify-product-delete', 'shopify-order-create', 'shopify-order-paid', 'shopify-order-update', 'shopify-customer-create', 'shopify-customer-update', 'sku', 'facebook', 'lazada', 'flipkart', 'index', 'orders'],
                'rules' => [
                    [
                        'actions' => ['signup', 'create-customer-hook', 'orderbigcommerce', 'orderhookcreate', 'producthooksbg', 'skuupdategb', 'productskuhooksbg', 'categoryhooksbg', 'shopify-product-create', 'shopify-product-update', 'shopify-product-delete', 'shopify-order-create', 'shopify-order-paid', 'shopify-order-update', 'shopify-customer-create', 'shopify-customer-update', 'sku'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'index', 'view', 'create', 'update', 'savecustomer', 'create-customer-hook', 'orderbigcommerce', 'orderhookcreate', 'producthooksbg', 'skuupdategb', 'productskuhooksbg', 'categoryhooksbg', 'shopify-product-create', 'shopify-product-update', 'shopify-product-delete', 'shopify-order-create', 'shopify-order-paid', 'shopify-order-update', 'shopify-customer-create', 'shopify-customer-update', 'sku', 'facebook', 'lazada', 'flipkart', 'index', 'orders'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CustomerUser models.
     * @return mixed
     */
    public function beforeAction($action) {

        $this->enableCsrfValidation = false;


        return parent::beforeAction($action);
    }

    public function actionIndex() {

//        die;
        if (isset($_FILES) and ! empty($_FILES) and isset($_FILES["flipkart_csv"]) and ! empty($_FILES["flipkart_csv"]['size'])) {
            $path = $_FILES['flipkart_csv']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);

            if (in_array($ext, ['xls', 'xlsx'])) {

                $flipkart = \backend\models\Channels::find()->where(['channel_name' => 'Flipkart'])->one();
                $channel_id = $flipkart->channel_ID;

                $filename = $_FILES["flipkart_csv"]["tmp_name"];
                $name = time() . '_' . basename($_FILES["flipkart_csv"]["name"]);
                move_uploaded_file($filename, $_SERVER['DOCUMENT_ROOT'] . '/backend/web/uploads/' . $name);
                require_once $_SERVER['DOCUMENT_ROOT'] . '/backend/web/excel/PHPExcel.php';
                $inputfilename = $_SERVER['DOCUMENT_ROOT'] . '/backend/web/uploads/' . $name;
                $exceldata = array();

                try {
                    $inputfiletype = \PHPExcel_IOFactory::identify($inputfilename);
                    $objReader = \PHPExcel_IOFactory::createReader($inputfiletype);
                    $objPHPExcel = $objReader->load($inputfilename);
                } catch (Exception $e) {
                    die('Error loading file "' . pathinfo($inputfilename, PATHINFO_BASENAME) . '": ' . $e->getMessage());
                }
//  Get worksheet dimensions
                $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
//            print_r($allDataInSheet);
//            die;
                $user = Yii::$app->user->identity;
                if (isset($allDataInSheet) and ! empty($allDataInSheet)) {
//echo'<pre>';
//print_r($lazada);die;
                    $channel_id = $flipkart->channel_ID;
//echo $channel_id;die;
                    $checkConnection = \backend\models\ChannelConnection::find()->Where(['user_id' => Yii::$app->user->identity->id, 'channel_id' => $channel_id])->one();
                    if (empty($checkConnection)) {
                        $checkConnection = new \backend\models\ChannelConnection();
                        $checkConnection->channel_id = $channel_id;
                        $checkConnection->user_id = $user->id;
                        $checkConnection->elliot_user_id = $user->id;
                        $checkConnection->connected = 'yes';
                        $checkConnection->save(false);
                    }

                    $conversion_rate = 1;
                    if (isset($user->currency)) {
                        $username = Yii::$app->params['xe_account_id'];
                        $password = Yii::$app->params['xe_account_api_key'];
                        $URL = Yii::$app->params['xe_base_url'] . 'convert_from.json/?from=INR&to=USD&amount=1';

                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $URL);
                        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                        curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
                        $result = curl_exec($ch);
                        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
                        curl_close($ch);
//echo'<pre>';
                        $result = json_decode($result, true);
                        if (isset($result) and ! empty($result) and isset($result['to']) and isset($result['to'][0]) and isset($result['to'][0]['quotecurrency'])) {
                            $conversion_rate = $result['to'][0]['mid'];
//                        $conversion_rate = number_format((float) $conversion_rate, 2, '.', '');
                        }
                    }
//echo $conversion_rate;echo'<br>';
                    foreach ($allDataInSheet as $key => $single) {
                        if (in_array($key, [1, 2]))
                            continue;
                        $checkProductModel = Products::find()->where(['product_name' => $single['D'], 'SKU' => $single['F']])->one();
                        $prefix_product_id = 'FPK' . @$single['B'];
                        if (empty($checkProductModel)):
//                            echo $single['G']*$conversion_rate;die;
                            if ($single['Y'] == 'instock') {
                                $single['Y'] = 'In Stock';
                            }
                            $productModel = new Products ();
                            $productModel->channel_abb_id = 'FPK' . @$single['B'];
                            $productModel->product_name = @$single['D'];
                            $productModel->SKU = @ $single['F'];
                            $productModel->description = @$single['D'];
                            $productModel->adult = 'no';
                            //Refrence to Google Product Search Categories
                            $productModel->age_group = NULL;
                            $productModel->gender = 'Unisex';
                            $productModel->availability = @$single['Y'];

//                            $productModel->product_status = 'active';
                            $productModel->brand = 'Test';
                            $productModel->condition = 'New';
                            $productModel->weight = @ $single['X'];
                            $productModel->UPC = '';
                            $productModel->EAN = '';
                            $productModel->Jan = '';
                            $productModel->ISBN = '';
                            $productModel->MPN = '';
                            //Inventory Fields Mapping
                            $productModel->stock_quantity = @$single['P'];
                            $productModel->stock_level = @$single['Y'];
                            $productModel->stock_status = 'Visible';
                            $productModel->low_stock_notification = NULL;
                            $productModel->price = @$single['G'] * $conversion_rate;
                            $productModel->sales_price = @$single['H'] * $conversion_rate;
                            $productModel->created_at = date('Y-m-d H:i:s', time());
                            $productModel->updated_at = date('Y-m-d H:i:s', time());
                            ;
                            //Save Elliot User id
                            $productModel->elliot_user_id = $user->id;
                            $productModel->save(false);

                            //After Product Save Fetch Product ID
                            $saved_product_id = $productModel->id;
                            //  echo '<Pre>';print_r($productModel);die;


                            $CategoryAbbrivation = new ProductAbbrivation();
                            $CategoryAbbrivation->channel_abb_id = 'FPK' . @$single['B'];
                            $CategoryAbbrivation->product_id = $saved_product_id;
                            $CategoryAbbrivation->price = @$single['G'] * $conversion_rate;
                            $CategoryAbbrivation->salePrice = @$single['H'] * $conversion_rate;
                            $CategoryAbbrivation->channel_accquired = 'Flipkart';
                            $CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
                            $CategoryAbbrivation->save(false);
                            $get_category = Categories::find()->select('category_ID')->where(['channel_abb_id' => $single['C']])->one();
                            if (empty($get_category)) {
                                $category = new Categories();
                                $category->channel_abb_id = $single['C'];
                                $category->category_name = $single['C'];
                                $category->parent_category_ID = '';
                                $category->elliot_user_id = $user->id;
                                $category->store_category_groupid = '';
                                $category->created_at = date('Y-m-d H:i:s', time());
                                $category->updated_at = date('Y-m-d H:i:s', time());
                                $category->save(false);
                                $category_id = $category->category_ID;

                                $productCategoryModel = new ProductCategories();
                                $productCategoryModel->category_ID = $category_id;
                                $productCategoryModel->product_ID = $saved_product_id;
                                $productCategoryModel->created_at = date('Y-m-d H:i:s', time());
                                $productCategoryModel->updated_at = date('Y-m-d H:i:s', time());
                                //Save Elliot User id
                                $productCategoryModel->elliot_user_id = $user->id;
                                $productCategoryModel->save(false);
                            }
                            $productChannelModel = new ProductChannel();
                            $productChannelModel->channel_id = $channel_id;
//                                $productChannelModel->store_id = 0;
                            $productChannelModel->product_id = $saved_product_id;
                            $productChannelModel->save(false);


                            $merchantproductsModel = new MerchantProducts;
                            $merchantproductsModel->merchant_ID = $user->id;
                            $merchantproductsModel->product_ID = $saved_product_id;
                            //Save Elliot User id
                            $merchantproductsModel->elliot_user_id = $user->id;
                            $merchantproductsModel->save(false);

                        else:

                            /* Save Product Abbrivation Id */
                            $saved_product_id = $checkProductModel->id;

                            $ProductAbbrivation_check = ProductAbbrivation::find()->Where(['channel_abb_id' => $prefix_product_id])->one();
                            if (empty($ProductAbbrivation_check)):
                                $CategoryAbbrivation = new ProductAbbrivation();
                                $CategoryAbbrivation->channel_abb_id = $prefix_product_id;
                                $CategoryAbbrivation->product_id = $saved_product_id;
                                $CategoryAbbrivation->channel_accquired = 'Flipkart';
                                $CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
                                $CategoryAbbrivation->save(false);
                            endif;

                            $ProductChannel_check = ProductChannel::find()->Where(['channel_id' => $channel_id, 'product_id' => $saved_product_id])->one();
                            if (empty($ProductAbbrivation_check)):
                                //Product Channel/Store Table Entry
                                $productChannelModel = new ProductChannel;
                                $productChannelModel->channel_id = $channel_id;
                                $productChannelModel->product_id = $saved_product_id;
                                $productChannelModel->status = 'yes';
                                $productChannelModel->created_at = date('Y-m-d H:i:s', time());
                                $productChannelModel->save(false);
                            endif;
                        endif;
                    }
                }
            }
            Yii::$app->session->setFlash('success', 'Success! Products Imported successfully');
        }
        return $this->render('flipkart');
    }

    public function actionOrders() {

//        die;
        if (isset($_FILES) and ! empty($_FILES) and isset($_FILES["flipkart_orders_csv"]) and ! empty($_FILES["flipkart_orders_csv"]['size'])) {
            $path = $_FILES['flipkart_orders_csv']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);

            if (in_array($ext, ['csv'])) {

                $flipkart = \backend\models\Channels::find()->where(['channel_name' => 'Flipkart'])->one();
                $channel_id = $flipkart->channel_ID;

//                echo"<pre>";
//                print_r($_FILES);
//                print_r($flipkart);
//            die;

                $filename = $_FILES["flipkart_orders_csv"]["tmp_name"];
                $name = time() . '_' . basename($_FILES["flipkart_orders_csv"]["name"]);
                move_uploaded_file($filename, $_SERVER['DOCUMENT_ROOT'] . '/backend/web/uploads/' . $name);
                require_once $_SERVER['DOCUMENT_ROOT'] . '/backend/web/excel/PHPExcel.php';
                $inputfilename = $_SERVER['DOCUMENT_ROOT'] . '/backend/web/uploads/' . $name;
                $csvData = file_get_contents($inputfilename);
                $lines = explode(PHP_EOL, $csvData);
                $csv_data = array();
                foreach ($lines as $line) {
                    $csv_data[] = str_getcsv($line);
                }
                $user = Yii::$app->user->identity;

//echo"<pre>";
//print_R($csv_data);die;
                if (!empty($csv_data)) :
//echo $channel_id;die;
                    $checkConnection = \backend\models\ChannelConnection::find()->Where(['user_id' => Yii::$app->user->identity->id, 'channel_id' => $channel_id])->one();
                    if (empty($checkConnection)) {
                        $checkConnection = new \backend\models\ChannelConnection();
                        $checkConnection->channel_id = $channel_id;
                        $checkConnection->user_id = $user->id;
                        $checkConnection->elliot_user_id = $user->id;
                        $checkConnection->connected = 'yes';
                        $checkConnection->save(false);
                    }
                    $first_column = $csv_data[0][0];
                    foreach ($csv_data as $key_csv => $orders_data) :
                        if ($key_csv == 0)
                            continue;
                        if (empty($orders_data) or empty($orders_data[0]))
                            continue;
//print_r($orders_data[2]);die;
                        $order_id = $orders_data[2];
                        $prefix_order_id = 'FPK' . $order_id;
                        //Check If Order duplicacy
                        $db_orders_check = Orders::find()->Where(['channel_abb_id' => $prefix_order_id])->one();
                        if (empty($db_orders_check)) :
                            if (!empty($prefix_order_id)):
                                $customer_db_data = CustomerUser::find()->Where(['channel_abb_id' => $prefix_order_id])->one();
                                $channel_name1 = 'Flipkart';
                                if (empty($customer_db_data)):
                                    //Save New customers 
                                    $Customers_order_model = new CustomerUser();
                                    $Customers_order_model->channel_abb_id = 'FPK' . $order_id;
                                    $Customers_order_model->first_name = isset($orders_data[18]) ? $orders_data[18] : "";
                                    $Customers_order_model->last_name = "";
                                    $Customers_order_model->email = "";
                                    $Customers_order_model->channel_acquired = $channel_name1;
                                    $Customers_order_model->date_acquired = date('Y-m-d H:i:s', strtotime($orders_data[1]));
                                    $Customers_order_model->street_1 = isset($orders_data[19]) ? $orders_data[19] : "";
                                    $Customers_order_model->street_2 = isset($orders_data[20]) ? $orders_data[20] : "";
                                    $Customers_order_model->city = isset($orders_data[21]) ? $orders_data[21] : "";
                                    $Customers_order_model->country = "";
                                    $Customers_order_model->country_iso = "";
                                    $Customers_order_model->state = isset($orders_data[22]) ? $orders_data[22] : "";
                                    $Customers_order_model->zip = isset($orders_data[23]) ? $orders_data[23] : "";
                                    $Customers_order_model->phone_number = "";
                                    $Customers_order_model->created_at = date('Y-m-d H:i:s', strtotime($orders_data[1]));
                                    $Customers_order_model->updated_at = date('Y-m-d H:i:s', strtotime($orders_data[0]));
                                    //Save Elliot User id
                                    $Customers_order_model->elliot_user_id = $user->id;
                                    $Customers_order_model->save(false);
                                endif;
                            endif;

                            //get Customer Id     $ProductAbbrivation_check = ProductAbbrivation::find()->Where(['channel_abb_id' => $prefix_product_id])->one();
                            $customer_db1 = CustomerUser::find()->Where(['channel_abb_id' => $prefix_order_id])->one();
                            $customer_id = $customer_db1->customer_ID;
                            if ($first_column == 'Order Cancellation date' and ! empty($orders_data[0])) {
                                $order_status = "Cancel";
                            }
                            $product_qauntity = $orders_data[13];
                            //billing Address
                            $billing_add1 = isset($orders_data[19]) ? $orders_data[19] : "" . ',' . isset($orders_data[20]) ? $orders_data[20] : "";
                            $billing_add2 = isset($orders_data[21]) ? $orders_data[21] : "" . ',' . isset($orders_data[22]) ? $orders_data[22] : "" .
                                    ',' . isset($orders_data[23]) ? $orders_data[23] : "";
                            $billing_address = $billing_add1 . ',' . $billing_add2;

                            //billing Address
                            $bill_street_1 = isset($orders_data[19]) ? $orders_data[19] : '';
                            $bill_street_2 = isset($orders_data[20]) ? $orders_data[20] : '';
                            $bill_city = isset($orders_data[21]) ? $orders_data[21] : '';
                            $bill_state = isset($orders_data[22]) ? $orders_data[22] : '';
                            $bill_zip = isset($orders_data[23]) ? $orders_data[23] : '';
                            $bill_country = '';
                            $bill_country_iso = '';

                            //Shipping Address
                            $ship_street_1 = isset($orders_data[19]) ? $orders_data[19] : "";
                            $ship_street_2 = isset($orders_data[20]) ? $orders_data[20] : "";
                            $ship_city = isset($orders_data[21]) ? $orders_data[21] : "";
                            $ship_state = isset($orders_data[22]) ? $orders_data[22] : "";
                            $ship_zip = isset($orders_data[23]) ? $orders_data[23] : "";
                            $ship_country = "";
                            $ship_country_iso = "";

                            $shipping_address = $ship_street_1 . ',' . $ship_street_2 . ',' . $ship_city . ',' . $ship_state . ',' . $ship_zip . ',' . $ship_country;

                            $total_amount = $orders_data[16];
                            $conversion_rate = 1;
                            if (isset($user->currency)) {
                                $username = Yii::$app->params['xe_account_id'];
                                $password = Yii::$app->params['xe_account_api_key'];
                                $URL = Yii::$app->params['xe_base_url'] . 'convert_from.json/?from=INR&to=USD&amount=1';

                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL, $URL);
                                curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
                                curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
                                $result = curl_exec($ch);
                                $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
                                curl_close($ch);
//echo'<pre>';
                                $result = json_decode($result, true);
                                if (isset($result) and ! empty($result) and isset($result['to']) and isset($result['to'][0]) and isset($result['to'][0]['quotecurrency'])) {
                                    $conversion_rate = $result['to'][0]['mid'];
//                        $conversion_rate = number_format((float) $conversion_rate, 2, '.', '');
                                }
                            }
                            $total_amount = $total_amount * $conversion_rate;

                            $base_shipping_cost = $orders_data[14];
                            $shipping_cost_tax = '';
                            $base_handling_cost = '';
                            $handling_cost_tax = '';
                            $base_wrapping_cost = '';
                            $wrapping_cost_tax = '';
                            $payment_method = $orders_data[17];
                            $payment_provider_id = "";
                            $payment_status = "";
                            if ($first_column == 'Order Cancellation date' and ! empty($orders_data[0])) {
                                $payment_status = "Cancel";
                            }
                            $refunded_amount = "";
                            $discount_amount = '';
                            $coupon_discount = '';
                            $order_date = $orders_data[1];
                            $order_last_modified_date = $orders_data[0];
                            //Save Order Model details
                            $order_model = new Orders();
                            $order_model->customer_id = $customer_id;
                            $order_model->channel_abb_id = $prefix_order_id;
                            $order_model->order_status = $order_status;
                            $order_model->product_qauntity = $product_qauntity;
                            $order_model->bill_street_1 = $bill_street_1;
                            $order_model->bill_street_2 = $bill_street_2;
                            $order_model->bill_city = $bill_city;
                            $order_model->bill_state = $bill_state;
                            $order_model->bill_zip = $bill_zip;
                            $order_model->bill_country = $bill_country;
                            $order_model->bill_country_iso = $bill_country_iso;
                            $order_model->ship_street_1 = $ship_street_1;
                            $order_model->ship_street_2 = $bill_street_2;
                            $order_model->ship_city = $ship_city;
                            $order_model->ship_state = $ship_state;
                            $order_model->ship_zip = $ship_zip;
                            $order_model->ship_country = $ship_country;
                            $order_model->ship_country_iso = $ship_country_iso;
                            $order_model->shipping_address = $shipping_address;
                            $order_model->billing_address = $billing_address;
                            $order_model->base_shipping_cost = $base_shipping_cost;
                            $order_model->shipping_cost_tax = $shipping_cost_tax;
                            $order_model->base_handling_cost = $base_handling_cost;
                            $order_model->handling_cost_tax = $handling_cost_tax;
                            $order_model->base_wrapping_cost = $base_wrapping_cost;
                            $order_model->wrapping_cost_tax = $wrapping_cost_tax;
                            $order_model->payment_method = $payment_method;
                            $order_model->payment_provider_id = $payment_provider_id;
                            $order_model->payment_status = $payment_status;
                            $order_model->refunded_amount = $refunded_amount;
                            $order_model->discount_amount = $discount_amount;
                            $order_model->coupon_discount = $coupon_discount;
                            $order_model->total_amount = $total_amount;
                            $order_model->order_date = $order_date;
                            $order_model->updated_at = date('Y-m-d H:i:s', strtotime($order_last_modified_date));
                            $order_model->created_at = date('Y-m-d H:i:s', strtotime($order_date));
                            //Save Elliot User id
                            $order_model->elliot_user_id = $user->id;
                            if ($order_model->save(false)):

//echo $order_id;die;
                                $curl = curl_init();
                                curl_setopt_array($curl, array(
                                    CURLOPT_URL => "https://api.flipkart.net/sellers/v2/orders/" . str_replace("'", "", $orders_data[3]),
                                    CURLOPT_RETURNTRANSFER => true,
                                    CURLOPT_ENCODING => "",
                                    CURLOPT_MAXREDIRS => 10,
                                    CURLOPT_TIMEOUT => 30,
                                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                    CURLOPT_CUSTOMREQUEST => "GET",
                                    CURLOPT_HTTPHEADER => array(
                                        "authorization: Bearer 2f2c5d70-ec70-475f-98c7-1d5cdbf2a1bb",
                                        "cache-control: no-cache",
                                        "content-type: application/json",
                                    ),
                                ));

                                $response = curl_exec($curl);
                                $err = curl_error($curl);
                                $response = json_decode($response, true);
//                                print_R($response);die;
                                curl_close($curl);
                                $product_listing_id = $response['listingId'];




                                $product_details_check = Products::find()->Where(['channel_abb_id' => 'FPK' . $product_listing_id])->one();
                                if (!empty($product_details_check)) {
                                    /* if product already exist in db */
                                    $product_id = $product_details_check->id;
                                } else {
                                    //Check for custom product and save a custom product in product table
                                    //save custom product details 
                                    $product_custom_Model = new Products();
                                    $product_custom_Model->channel_abb_id = 'FPK' . $product_listing_id;
                                    $custom_product_name = $response['title'];
                                    $custom_product_sku = isset($response['sku']) ? $response['sku'] : "";
                                    $product_custom_Model->product_name = $custom_product_name;
                                    $product_custom_Model->SKU = $custom_product_sku;
                                    $product_custom_Model->UPC = "";
                                    $product_custom_Model->EAN = "";
                                    $product_custom_Model->Jan = "";
                                    $product_custom_Model->ISBN = "";
                                    $product_custom_Model->MPN = "";
                                    $product_custom_Model->brand = "";
                                    $product_custom_Model->product_status = 'in_active';
                                    //Save Elliot User id 
                                    $product_custom_Model->elliot_user_id = $user->id;

                                    if ($product_custom_Model->save(false)):


                                        $product_id = $product_custom_Model->id;
                                        /* Save Product Abbrivation Id */
                                        $CategoryAbbrivation = new ProductAbbrivation();
                                        $CategoryAbbrivation->channel_abb_id = 'FPK' . $product_listing_id;
                                        $CategoryAbbrivation->product_id = $product_custom_Model->id;
                                        $CategoryAbbrivation->channel_accquired = 'Flipkart';
                                        $CategoryAbbrivation->created_at = date('Y-m-d H:i:s', time());
                                        $CategoryAbbrivation->save(false);
                                    endif;

                                    //Product Channel/Store Table Entry
                                    $custom_productChannelModel = new ProductChannel;
                                    $custom_productChannelModel->channel_id = $channel_id;
                                    $custom_productChannelModel->product_id = $product_custom_Model->id;
                                    $custom_productChannelModel->status = 'no';
                                    $custom_productChannelModel->created_at = date('Y-m-d H:i:s', time());
                                    $custom_productChannelModel->save(false);
                                    /* If order Product Already exist in database */
                                }

                                $product_qty = $response['quantity'];
                                $order_product_sku = $response['sku'];
                                $db_order_id = $order_model->order_ID;
                                //Save Order Products Details
                                $order_products_model = new OrdersProducts;
                                $order_products_model->order_Id = $db_order_id;
                                $order_products_model->product_Id = $product_id;
                                $order_products_model->qty = $product_qty;
                                $order_products_model->order_product_sku = $order_product_sku;
                                $order_products_model->created_at = $order_model->created_at;

                                //Save Elliot User id
                                $order_products_model->elliot_user_id = $user->id;
                                $order_products_model->save(false);



                                //Save Order channels
                                $order_channels_model = new OrderChannel();
                                $order_channels_model->order_id = $db_order_id;
                                $order_channels_model->channel_id = $channel_id;
                                $order_channels_model->created_at = $order_model->created_at;
                                $order_channels_model->save(false);
                            endif;
                        endif;
                    endforeach;
                endif;
                Yii::$app->session->setFlash('success', 'Success! Orders Imported successfully');
            }
        }
        return $this->render('flipkart');
    }

    function actionTest() {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.flipkart.net/sellers/skus/listings/LSTRBKEV487HQBF7DHCCSPSRO",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer 2f2c5d70-ec70-475f-98c7-1d5cdbf2a1bb",
                "cache-control: no-cache",
                "content-type: application/json",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $response = json_decode($response, true);
        curl_close($curl);
        echo"<pre>";
        print_R($response);
        die;
    }

}
