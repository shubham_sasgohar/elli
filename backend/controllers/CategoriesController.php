<?php

namespace backend\controllers;

use Yii;
use backend\models\Categories;
use backend\models\CategoriesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\Stores;
use backend\models\Channels;
use backend\models\ChannelConnection;
use Bigcommerce\Api\Client as Bigcommerce;
/**
 * CategoriesController implements the CRUD actions for Categories model.
 */
class CategoriesController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'create', 'index', 'update'],
                'rules' => [
                        [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                        [
                        'actions' => ['logout', 'index', 'view', 'create', 'index', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Categories models.
     * @return mixed
     */
    public function actionIndex() {

        $categories = Categories::find()->Where(['elliot_user_id' => Yii::$app->user->id, 'parent_category_ID' => 0])->all();
        $searchModel = new CategoriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'categories' => $categories,
        ]);
    }

    /**
     * Displays a single Categories model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionUpdateNestedCat() {

        if (Yii::$app->request->post()) :

            $data = Yii::$app->request->post('data');
            $cat_arr = json_decode($data);
//            echo'<pre>';
//            print_r($cat_arr);
//            echo "bhjfjkdhjgfjsdf";

            $varfun = $this->displayArrayRecursively($cat_arr);

        endif;
    }

    public static function displayArrayRecursively($cat_arr, $indent = '') {
        if ($cat_arr) {
            $array = array();
            $arr[] = '';

            foreach ($cat_arr as $value) {

                if (array_key_exists("children", $value)) {

                    $count_child = count($value->children);

                    for ($i = 0; $i <= $count_child; $i++) {

                        $data = $value->children[$i]->id;
                        $id = $value->id;
                        $array[$value->id] = $data;
                        $arr = array("parent" => $value->id, "child" => $data);

                        $count_child--;

                        self::displayArrayRecursively($value->children, $indent . '--' . $value->id);


                        $db_cat_data = Categories::find()->Where(['category_ID' => $data])->one();
                        $db_cat_data->parent_category_ID = $id;
                        if ($db_cat_data->save(false)) :

                        endif;
                    }
                } else {
                    $id = $value->id;
                    $db_cat_data = Categories::find()->Where(['category_ID' => $id])->one();
                    $db_cat_data->parent_category_ID = 0;
                    if ($db_cat_data->save(false)) :

                    endif;
                }
            }
        }
    }

    /**
     * Creates a new Categories model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        

        //Only For Bigcommerce
        $model = new Categories();
        $request = Yii::$app->request;
        if ($request->post()):
            $cat_name = $request->post('add_cat_name');
            $parent_name = $request->post('add_parent_cat_name');

            if ($parent_name == 'Please Select') :
                $parent_id = 0;
            else :
                $categories_data = Categories::find()->Where(['category_name' => $parent_name])->one();
                $parent_id = !empty($categories_data)?$categories_data->category_ID:0;
            endif;
            
            $model->category_name = $cat_name;
            $model->parent_category_ID = $parent_id;
            $model->elliot_user_id = Yii::$app->user->identity->id;
            
            if ($model->save()) {
                $create_cat_stores_channels = Stores::create_category($cat_name,$parent_name);
                //Response data
                $group_id=$create_cat_stores_channels->group_id;
                $abb_id=$create_cat_stores_channels->abb_id;
                //after create category stores or channel save abb id
                $update_categories_data = Categories::find()->Where(['category_ID' => $model->category_ID])->one();
                $update_categories_data->channel_abb_id=$abb_id;
                $update_categories_data->store_category_groupid=$group_id;
                $update_categories_data->save(false);
                
                Yii::$app->session->setFlash('success', 'Success! Category has been Created Successfully.');
                return $this->redirect('/categories');
            } else {
                Yii::$app->session->setFlash('danger', 'Error! Category has Not been Created Successfully.');
                return $this->render('create', [
                            'model' => $model,
                ]);
            }

        else :
            return $this->render('create', [
                        'model' => $model,
            ]);
        endif;
    }

    /**
     * Creates a new Categories model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionGetParentcat() {

        $parent_cat = Categories::find()->all();
        foreach ($parent_cat as $key => $parent_cat_data) {
            $parent_name = $parent_cat_data->category_name;
            $parent_cat_array[$key][$parent_cat_data->category_ID] = $parent_name;
        }
        //$a="{'M': 'male', 'F': 'female'}";
        echo json_encode($parent_cat_array);
    }

    /**
     * Updates an existing Categories model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->category_ID]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates cat an existing Categories model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
     public function actionUpdatecat() {

        if (Yii::$app->request->post()) {
            $id = Yii::$app->request->post('id');
            $cat_name = Yii::$app->request->post('update_cat_name');
            $parent_name = Yii::$app->request->post('update_parent_cat_name');


            if ($parent_name == 'Please Select') :
                $parent_id = 0;
                $parent_channel_abb_id = 0;
            else :
                $categories_data = Categories::find()->Where(['category_name' => $parent_name])->one();
                $parent_id = isset($categories_data->category_ID) ? $categories_data->category_ID : '0';
//                $parent_id = $categories_data->category_ID;
                $parent_prefix_channel_abb_id = isset($categories_data->channel_abb_id) ? $categories_data->channel_abb_id : '0';
                $parent_channel_abb_id = substr($parent_prefix_channel_abb_id, 3);
            endif;

            $model = Categories::find()->Where(['category_ID' => $id])->one();
            $cat_channel_abb_id = $model->channel_abb_id;
            $channel_abb_id = substr($cat_channel_abb_id, 3);

            $model->category_name = $cat_name;
            $model->parent_category_ID = $parent_id;
            //update Catergories Bigcommerce
            $object = array("name" => $cat_name, "parent_id" => $parent_channel_abb_id);

            $update_cat_bigCommerce = Stores::bigcommerce_update_category($channel_abb_id, $object, $parent_name,$cat_name ,$id);
            
            $model->save(false);
            Yii::$app->session->setFlash('success', 'Success! Categories has been updated.');
            return $this->redirect(['update', 'id' => $model->category_ID]);
        }
    }

    /**
     * Deletes an existing Categories model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Categories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Categories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Categories::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionEdit($id) {
        $categories = Categories::find()->where(['category_ID' => $id])->one();
        $parent_cat = Categories::find()->where(['category_ID' => $categories->parent_category_ID])->one();
        return $this->render('edit', [
                    'categories' => $categories->category_name,
                    'parent_cat' => $parent_cat->category_name,
        ]);
        //return $this->render('wechat');
    }

}
 