<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;    
use backend\models\User;
use backend\models\StoresConnection;
use backend\models\Stores;
use backend\models\Categories;
use backend\models\Variations;
use backend\models\VariationsItemList;
use backend\models\Products;
use backend\models\OrdersProducts;
use backend\models\OrderChannel;
use backend\models\CustomerUser;
use backend\models\Notification;
use backend\models\ProductCategories;
use backend\models\ProductVariation;
use backend\models\ProductImages;
use backend\models\ProductChannel;
use backend\models\Orders;
use backend\models\Channels;
use backend\models\MagentoStores;
use common\models\CustomFunction;
use backend\models\ProductAbbrivation;
use backend\models\CategoryAbbrivation;
use backend\models\Countries;
use backend\models\StoreDetails;
use backend\models\CustomerAbbrivation;

class Magento2Controller extends \yii\web\Controller
{
    /**
    * @inheritdoc
    */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','login', 'auth-magento2', 'magento2-importing', 'magento2-product-create', 'magento2-order-create', 'magento2-customer-create', 'magento2-customer-address-update', 'magento2x-configuration', 'test'],
                'rules' => [
                        [
                        /* action hit without log-in */
                        'actions' => ['login', 'magento2-importing', 'magento2-product-create', 'magento2-order-create', 'magento2-customer-create', 'magento2-customer-address-update', 'test'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                        [
                        /* action hit only with log-in */
                        'actions' => ['index', 'auth-magento2', 'magento2-importing', 'magento2-product-create', 'magento2-order-create', 'magento2-customer-create', 'magento2-customer-address-update', 'magento2x-configuration', 'test'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    /**
     * Magento 2 configuration action
     */
    
    public function actionMagento2xConfiguration() {
        return $this->render('magento2xconfiguration');
    }
    
    /*
     * Magento 2 save creadentials to database.
     */
    public function actionAuthMagento2(){
        $array_msg = array();
        $magento_shop_url = rtrim($_POST['magento_2_shop'],'/');
        $magento_shop = $magento_shop_url.'/';
        $magento_2_access_token = $_POST['magento_2_access_token'];
        $user_id = $_POST['user_id'];
        $magento_country = $_POST['magento_2_country'];
        
        $adminUrl=$magento_shop.'index.php/rest/V1/store/websites?searchCriteria=';
        $headers = array("Authorization: Bearer $magento_2_access_token");
        $ch = curl_init($adminUrl);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $result = curl_exec($ch);
        $err = curl_error($ch);

        curl_close($ch);

        if ($err) {
            $array_msg['api_error'] = 'It\'s look like You have enter wrong Magento url. Please check and try again.<br>Error is '.$err;
            return json_encode($array_msg);
        } 
        else{
            $result =  json_decode($result, TRUE);
            if($result==''){
                $array_msg['api_error'] = 'You Credentials are not working. Please check and try again';
                return json_encode($array_msg);
            }
            else{
                if(array_key_exists('message', $result) && $result['message']=='Consumer is not authorized to access %resources'){
                    $array_msg['api_error'] = 'You Access Token is no correct. Please check and try again.<br>Error is '.$result['message'];
                    return json_encode($array_msg);
                }
            }
        }
        
        $config = yii\helpers\ArrayHelper::merge(
            require($_SERVER['DOCUMENT_ROOT'].'/common/config/main.php'),
            require($_SERVER['DOCUMENT_ROOT'].'/common/config/main-local.php'), 
            require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main.php'), 
            require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);
        
        $user = User::find()->where(['id' => $user_id])->one();
        $user->magento_shop_url = $magento_shop;
        $user->save(false);
        $user_domain = $user->domain_name;
        
        $config = yii\helpers\ArrayHelper::merge(
            require($_SERVER['DOCUMENT_ROOT'].'/common/config/main.php'),
            require($_SERVER['DOCUMENT_ROOT'].'/common/users_config/' . $user_domain . '/main-local.php'),
            require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main.php'),
            require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);
        
        $mage_country = Countries::countryDetails($magento_country);
        $check_api_key = StoresConnection::find()->where(['mag_shop' => $magento_shop, 'mag_soap_api' => $magento_2_access_token])->one();
        if (!empty($check_api_key)) {
            $array_msg['api_error'] = 'This Magento shop is already integrated with Elliot. Please use another API details to integrate with Elliot.';
            return json_encode($array_msg);
        }
        $check_country = StoreDetails::find()->where(['country' => $mage_country->name, 'channel_accquired' => 'Magento'])->one();
        if (!empty($check_country)) {
            $array_msg['api_error'] = 'This Magento shop with same country <b>' . $mage_country->name . '</b> is already integrated with Eliiot.';
            return json_encode($array_msg);
        }
        
        $store = Stores::find()->select('store_id')->where(['store_name' => 'Magento2'])->one();
        $magento_store_id = $store->store_id;
        //$checkConnection = StoresConnection::find()->where(['store_id' => $magento_store_id, 'user_id' => $user_id])->one();
        //if (empty($checkConnection)){
            $connectionModel = new StoresConnection();
            $connectionModel->store_id = $magento_store_id;
            $connectionModel->user_id = $user_id;
            $connectionModel->connected = 'Yes';
            $connectionModel->mag_shop = $magento_shop;
            $connectionModel->mag_soap_api = $magento_2_access_token;
            $created_date = date('Y-m-d h:i:s', time());
            $connectionModel->created = $created_date;
            if ($connectionModel->save()){
                $array_msg['success'] = "Your Magento shop has been connected successfully. Importing is started. Once importing is done you will get a nofiy message.";
                $array_msg['store_connection_id'] = $connectionModel->stores_connection_id;
            }
            else{
                $array_msg['error'] = "Error Something went wrong. Please try again";
            }
        //}
        return json_encode($array_msg);
    }
    /*
     * Magento 2 Importing begin
     */
    public function actionMagento2Importing() {
        $user_id = $_GET['user_id'];
        $store_connection_id = $_GET['store_connection_id'];
        $magento_country_code = $_GET['magento_2_country'];
        $get_users = User::find()->select('domain_name,company_name,email')->where(['id' => $user_id])->one();
        $email = $get_users->email;
        $company_name = $get_users->company_name;
        $config = yii\helpers\ArrayHelper::merge(
            require($_SERVER['DOCUMENT_ROOT'].'/common/config/main.php'),
            require($_SERVER['DOCUMENT_ROOT'].'/common/users_config/' . $get_users->domain_name . '/main-local.php'), 
            require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main.php'), 
            require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main-local.php')
        );
        $application = new yii\web\Application($config);
        $get_magento_id = Stores::find()->select('store_id')->where(['store_name' => 'Magento2'])->one();
        $magento_store_id = $get_magento_id->store_id;
        $store_magento = StoresConnection::find()->where(['store_id' => $magento_store_id, 'user_id' => $user_id, 'stores_connection_id' => $store_connection_id])->one();
        if (!empty($store_magento)){
            $magento_shop = $store_magento->mag_shop;
            $magento_2_access_token = $store_magento->mag_soap_api;
            $access_token = array("Authorization: Bearer $magento_2_access_token"); 

            $requestUrlStores = $magento_shop.'index.php/rest/V1/store/websites?searchCriteria=';
            $requestUrlCategories = $magento_shop.'index.php/rest/V1/categories?searchCriteria=';
            $requestUrlProducts = $magento_shop.'index.php/rest/V1/products?searchCriteria=';
            $requestUrlCustomers = $magento_shop.'index.php/rest/V1/customers/search?searchCriteria=';
            $requestUrlOrders = $magento_shop.'index.php/rest/V1/orders?searchCriteria=';
            
            $country_detail = Countries::countryDetails($magento_country_code);
            
            $this->magentoStoresDetails($magento_shop, $store_connection_id, $country_detail);
            $this->magento2StoreImporting($access_token, $requestUrlStores, $user_id, $store_connection_id);
            $this->magento2CategoriesImporting($access_token, $requestUrlCategories, $user_id, $store_connection_id);
            $this->Magento2ProductsImporting($access_token, $requestUrlProducts, $user_id, $magento_shop, $store_connection_id, $country_detail);
            $this->Magento2CustomersImporting($access_token, $requestUrlCustomers, $user_id, $store_connection_id);
            $this->Magento2OrdersImporting($access_token, $requestUrlOrders, $user_id, $store_connection_id, $country_detail);
            
            /* SEnd Email Magento */
            $email_message = 'Success, Your Magento2 Store is Now Connected';
            //$email = 'noor.mohamad@brihaspatitech.com';
            $send_email_notif = CustomFunction::ConnectBigCommerceEmail($email, $company_name, $email_message);

            //Drop-Down Notification for User
            $notif_type = 'Magento2';
            $notif_db = Notification::find()->Where(['notif_type' => $notif_type])->one();
            if (empty($notif_db)):
                $notification_model = new Notification();
                $notification_model->user_id = $user_id;
                $notification_model->notif_type = $notif_type;
                $notification_model->notif_description = 'Your Magento2 store data has been successfully imported.';
                $notification_model->created_at = date('Y-m-d h:i:s', time());
                $notification_model->save(false);
            endif;
            //Sticky Notification :StoresConnection Model Entry for Import Status
            $get_rec = StoresConnection::find()->Where(['user_id' => $user_id, 'store_id' => $magento_store_id, 'stores_connection_id' => $store_connection_id])->one();
            $get_rec->import_status = 'Completed';
            $get_rec->save(false);
            //Yii::$app->session->setFlash('success', 'Success! Your Magento2 store data has been successfully imported.');
        }
    }
    public function magentoStoresDetails($magento_shop, $store_connection_id, $country_detail){
        $check_store_detail = StoreDetails::find()->where(['store_connection_id' => $store_connection_id])->one();
        if (empty($check_store_detail)) {
            $save_store_details = new StoreDetails();
            $save_store_details->store_connection_id = $store_connection_id;
            $save_store_details->store_name = '';
            $save_store_details->store_url = $magento_shop;
            $save_store_details->country = $country_detail->name;
            $save_store_details->country_code = $country_detail->sortname;
            $save_store_details->currency = $country_detail->currency_code;
            $save_store_details->currency_symbol = $country_detail->currency_code;
            $save_store_details->others = '';
            $save_store_details->channel_accquired = 'Magento2';
            $save_store_details->created_at = date('Y-m-d H:i:s', time());
            $save_store_details->save(false);
        }
    }
    /**
     * Magento 2 store importing
     * @param type $access_token
     * @param type $requestUrlStores
     * @param type $user_id
     */
    public function magento2StoreImporting($access_token, $requestUrlStores, $user_id, $store_connection_id) {
        $ch = curl_init($requestUrlStores);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $access_token); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $stores_list = curl_exec($ch);
        $stores_list=  json_decode($stores_list, TRUE);
        //echo "<pre>"; print_r($stores_list); die('End herer');
        
        foreach($stores_list as $_store){
            $checkMagentoStoreModel = MagentoStores::find()->where(['elliot_user_id' => $user_id, 'magento_store_id' => $_store['id'], 'mul_store_id'=> $store_connection_id])->one();
            if(empty($checkMagentoStoreModel)){
                $magentoStoreModel = new MagentoStores();
                $magentoStoreModel->elliot_user_id = $user_id;
                $magentoStoreModel->magento_store_id = $_store['id'];
                $magentoStoreModel->magento_store_name = $_store['name'];
                $magentoStoreModel->mul_store_id = $store_connection_id;
                $magentoStoreModel->created_at = date('Y-m-d h:i:s', time());
                $magentoStoreModel->save();
            }
        }
    }
    /**
     * Magento 2 Category importing
     * @param type $access_token
     * @param type $requestUrlCategories
     * @param type $user_id
     */
    public function magento2CategoriesImporting($access_token, $requestUrlCategories, $user_id, $store_connection_id) {
        $ch = curl_init($requestUrlCategories);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $access_token); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $category_result = curl_exec($ch);
        $category_result=  json_decode($category_result, TRUE);
        $this->getMagento2AllCategory($category_result, $user_id, $store_connection_id);
        $this->magento2ParentCategoryAssign($category_result, $store_connection_id);
    }
    
    public function getMagento2AllCategory($cat_arr, $user_id, $store_connection_id) {
        $mag_cat_id = $cat_arr['id'];
        $mag_parent_id = $cat_arr['parent_id'];
        $mag_cat_name = $cat_arr['name'];
        $cat_child = $cat_arr['children_data'];
        
        $category_data = array(
            'category_id'=>$mag_cat_id,     // Give category id of Store/channels
            'parent_id'=>0,     // Give Category parent id of Elliot if null then give 0
            'name'=>$mag_cat_name,   // Give category name
            'channel_store_name'=>'Magento2',   // Give Channel/Store name
            'channel_store_prefix'=>'MG2',  // Give Channel/Store prefix id
            'mul_store_id' => $store_connection_id, // store connection id
            'mul_channel_id' => '', // channel connectin id
            'elliot_user_id'=>$user_id,     // Give Elliot user id
            'created_at'=>date('Y-m-d H:i:s'),  // Give Created at date if null then give current date format date('Y-m-d H:i:s')
            'updated_at'=>date('Y-m-d H:i:s'),  // Give Updated at date if null then give current date format date('Y-m-d H:i:s')
        );
        Stores::categoryImportingCommon($category_data);    // Then call store modal function and give $castegory data
        
        if (count($cat_child) > 0):
            foreach ($cat_child as $value):
                $this->getMagento2AllCategory($value, $user_id, $store_connection_id);
            endforeach;
        endif;
    }
    
    /**
     * Magento Parent Category Assign
     * @param type $cat_arr
     */
    public function magento2ParentCategoryAssign($cat_arr, $store_connection_id) {
        $mag_cat_id = $cat_arr['id'];
        $mag_parent_id = $cat_arr['parent_id'];
        $mag_cat_name = $cat_arr['name'];
        $cat_child = $cat_arr['children_data'];
        if ($mag_parent_id != '') {
            $checkCatModel = CategoryAbbrivation::find()->where(['channel_abb_id' => 'MG2' . $mag_cat_id, 'mul_store_id' => $store_connection_id ])->one();
            if (!empty($checkCatModel)) {
                $elliot_cat_id = $checkCatModel->category_ID;
                $checkParentModel = CategoryAbbrivation::find()->where(['channel_abb_id' => 'MG2' . $mag_parent_id, 'mul_store_id' => $store_connection_id ])->one();
                if (!empty($checkParentModel)) {
                    $category_model = Categories::find()->where(['category_ID' => $elliot_cat_id])->one();
                    $elliot_parent_id = $checkParentModel->category_ID;
                    $category_model->parent_category_ID = $elliot_parent_id;
                    $category_model->save();
                }
            }
        }
        if (count($cat_child) > 0) {
            foreach ($cat_child as $value) {
                self::magento2ParentCategoryAssign($value, $store_connection_id);
            }
        }
    }
    
    /**
     * Magento 2 Products Importing
     * @param type $access_token
     * @param type $requestUrlProducts
     * @param type $user_id
     */
    public function Magento2ProductsImporting($access_token, $requestUrlProducts, $user_id, $magento_shop_url, $store_connection_id, $country_detail) {
        $ch = curl_init();
        $ch = curl_init($requestUrlProducts);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $access_token); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $product_list = curl_exec($ch);
        $product_list =  json_decode($product_list, TRUE);
        $product = $product_list['items'];
        foreach($product as $_products)
        {
            $mage_product_id = $_products['id'];
            $product_sku = str_replace(' ', '%20', $_products['sku']);
            if($product_sku!='')
            {
                $product_request = $magento_shop_url.'index.php/rest/V1/products/'.$product_sku;
                $ch = curl_init();
                $ch = curl_init($product_request);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $access_token); 
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
                curl_setopt($ch, CURLOPT_ENCODING, '');
                curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
                curl_setopt($ch, CURLOPT_TIMEOUT, FALSE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                $product_list = curl_exec($ch);
                $_product =  json_decode($product_list, TRUE);
                if(count($_product)>1 && array_key_exists('message', $_product) || $_product=='')
                {
                    error_log ('This product are not imported from Magento 2X to Elliot. Magento 2 product Sku is '.$_products['sku'].' and shop is '.$magento_shop_url.'+++', 3, 'magento_2_order_import_error.log');
                    continue;
                }
                $product_name = isset($_product['name'])?$_product['name']:'';
                $qty = $_product['extension_attributes']['stock_item']['qty'];
                $stock_status = $_product['extension_attributes']['stock_item']['is_in_stock'];
                $product_description = '';
                $product_url = '';
                $product_categories_ids = array();
                if(isset($_product['custom_attributes']))
                {
                    $custom_attributes = $_product['custom_attributes'];
                    foreach($custom_attributes as $attribute)
                    {
                        if($attribute['attribute_code']=='description')
                        {
                            $product_description = $attribute['value'];
                        }

                        if($attribute['attribute_code']=='category_ids')
                        {
                            $product_categories_ids = $attribute['value'];
                        }

                        if($attribute['attribute_code']=='url_key')
                        {
                            $product_url = $magento_shop_url.$attribute['value'].'.html';
                        }
                    }
                }

                $product_weight = isset($_product['weight'])?$_product['weight']:0;

                $product_status = $_product['status'];
                $product_created_at = $_product['created_at'];
                $product_updated_at = $_product['updated_at'];
                $product_price = isset($_product['price'])?$_product['price']:0;

                $product_images = isset($_product['media_gallery_entries'])?$_product['media_gallery_entries']:array();
                $product_image_data = array();
                foreach ($product_images as $_image) {
                    $magento_product_image_url = $magento_shop_url.'/pub/media/catalog/product'.$_image['file'];
                    $image_position = $_image['position'];
                    $image_type = $_image['types'];
                    if (in_array('image', $image_type)) {
                        $base_image = $magento_product_image_url;
                    }
                    $product_image_data[] = array(
                        'image_url'=>$magento_product_image_url,
                        'label'=>'',
                        'position'=>$image_position,
                        'base_img'=>$base_image,
                    );
                }
                
                $store_id = Stores::find()->select('store_id')->where(['store_name' => 'Magento2'])->one();
                $product_data = array(
                    'product_id'=>$mage_product_id, // Stores/Channel product ID
                    'name'=>$product_name,  // Product name
                    'sku'=>$product_sku,    // Product SKU
                    'description'=>$product_description,    // Product Description
                    'product_url_path'=>$product_url,   // Product url if null give blank value
                    'weight'=>$product_weight,  // Product weight if null give blank value
                    'status'=>$product_status,  // Product status (visible or hidden). Give in 0 or 1 form (0 = hidden, 1 = visible).
                    'price'=>$product_price,    // Porduct price
                    'sale_price'=>$product_price,   // Product sale price if null give Product price value
                    'qty'=>$qty,    //Product quantity 
                    'stock_status'=>$stock_status,  // Product stock status ("in stock" or "out of stock"). 
                                                    // Give in 0 or 1 form (0 = out of stock, 1 = in stock)
                    'websites'=>array(),    //This is for only magento give only and blank array
                    'brand'=>'',    // Product brand if any
                    'low_stock_notification'=>5,    // Porduct low stock notification if any otherwise give default 5 value
                    'created_at'=>$product_created_at,  // Product created at date format date('Y-m-d H:i:s')
                    'updated_at'=>$product_updated_at,  // Product updated at date format date('Y-m-d H:i:s')
                    'channel_store_name'=>'Magento2',   // Channel or Store name
                    'channel_store_prefix'=>'MG2',      // Channel or store prefix id
                    'mul_store_id' => $store_connection_id, //Give multiple store id
                    'mul_channel_id' => '', // Give multiple channel id
                    'elliot_user_id'=>$user_id,         // Elliot user id
                    'store_id'=>$store_id->store_id,    // if your are importing store give store id
                    'channel_id'=>'',   // if your are importing channel give channel id
                    'country_code' => $country_detail->sortname,
                    'currency_code' => $country_detail->currency_code,
                    'upc'=>'',  // Product barcode if any
                    'ean'=>'',  // Product ean if any
                    'jan'=>'',  // Product jan if any
                    'isban'=>'',    // Product isban if any
                    'mpn'=>'',  // Product mpn if any
                    'categories'=>$product_categories_ids, // Product categroy array. If null give a blank array 
                    'images'=>$product_image_data,  // Product images data
                );
                Stores::productImportingCommon($product_data);
            }
        }
    }
    
    /**
     * Magento 2 Customers importing
     * @param type $access_token
     * @param type $requestUrlCustomers
     * @param type $user_id
     */
    public function Magento2CustomersImporting($access_token, $requestUrlCustomers, $user_id, $store_connection_id) {
        $ch = curl_init();
        $ch = curl_init($requestUrlCustomers);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $access_token); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);   
        $customer_list = curl_exec($ch);
        $customer_list =  json_decode($customer_list, TRUE);
        //echo "<pre>"; print_r($customer_list); die('End herer');
        foreach($customer_list['items'] as $_customer)
        {
            $cus_id = $_customer['id'];
            $cus_created_at = $_customer['created_at'];
            $cus_updated_at = $_customer['updated_at'];
            $cus_email = $_customer['email'];
            $cus_f_name = $_customer['firstname'];
            $cus_l_name = $_customer['lastname'];
            $website_id = isset($_customer['website_id'])?$_customer['website_id']:'';
            
            $customer_address = $_customer['addresses'];
            $bill_city = $bill_company = $bill_country_id = $bill_zip = $bill_region = $bill_street_1 = $bill_street_2 = $bill_telephone = '';
            $ship_city = $ship_company = $ship_country_id = $ship_zip = $ship_region = $ship_street_1 = $ship_street_2 = $ship_telephone = '';
            if(count($customer_address)>0)
            {
                $default_billing = isset($_customer['default_billing'])?$_customer['default_billing']:'';
                $default_shipping = isset($_customer['default_shipping'])?$_customer['default_shipping']:'';
                foreach($customer_address as $_customer_add)
                {
                    if($_customer_add['id']==$default_billing)
                    {
                        $bill_city = isset($_customer_add['city'])?$_customer_add['city']:'';
                        $bill_country_id = isset($_customer_add['country_id'])?$_customer_add['country_id']:'';
                        $bill_zip = isset($_customer_add['postcode'])?$_customer_add['postcode']:'';
                        $bill_region = isset($_customer_add['region']['region'])?$_customer_add['region']['region']:'';
                        $bill_street_1 = isset($_customer_add['street'][0])?$_customer_add['street'][0]:'';
                        if(array_key_exists(1, $_customer_add['street']))
                        {
                            $bill_street_2 = $_customer_add['street'][1];
                        }
                        $bill_telephone = $_customer_add['telephone'];
                    }
                    
                    if($_customer_add['id']==$default_shipping)
                    {
                        $ship_city = isset($_customer_add['city'])?$_customer_add['city']:'';
                        $ship_country_id = isset($_customer_add['country_id'])?$_customer_add['country_id']:'';
                        $ship_zip = isset($_customer_add['postcode'])?$_customer_add['postcode']:'';
                        $ship_region = isset($_customer_add['region']['region'])?$_customer_add['region']['region']:'';
                        $ship_street_1 = isset($_customer_add['street'][0])?$_customer_add['street'][0]:'';
                        if(array_key_exists(1, $_customer_add['street']))
                        {
                            $ship_street_2 = $_customer_add['street'][1];
                        }
                        $ship_telephone = $_customer_add['telephone'];
                    }
                }
            }
            
            $customer_data = array(
                'customer_id'=>$cus_id,
                'elliot_user_id'=>$user_id,
                'first_name'=>$cus_f_name,
                'last_name'=>$cus_l_name,
                'email'=>$cus_email,
                'channel_store_name'=>'Magento2',
                'channel_store_prefix'=>'MG2',
                'mul_store_id' => $store_connection_id,
                'mul_channel_id' => '',
                'customer_created_at'=>$cus_created_at,
                'customer_updated_at'=>$cus_updated_at,
                'billing_address'=>array(
                    'street_1'=>$bill_street_1,
                    'street_2'=>$bill_street_2,
                    'city'=>$bill_city,
                    'state'=>$bill_region,
                    'country'=>$bill_country_id,
                    'country_iso'=>$bill_country_id,
                    'zip'=>$bill_zip,
                    'phone_number'=>$bill_telephone,
                    'address_type'=>'Default',
                ),
                'shipping_address' => array(
                    'street_1'=>$ship_street_1,
                    'street_2'=>$ship_street_2,
                    'city'=>$ship_city,
                    'state'=>$ship_region,
                    'country'=>$ship_country_id,
                    'zip'=>$ship_zip,
                ),
            );
            Stores::customerImportingCommon($customer_data);
        }
    }
    /**
     * Magento 2 Orders importing
     * @param type $access_token
     * @param type $requestUrlOrders
     * @param type $user_id
     */
    public function Magento2OrdersImporting($access_token, $requestUrlOrders, $user_id, $store_connection_id, $country_detail) {
        $ch = curl_init();
        $ch = curl_init($requestUrlOrders);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $access_token); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $order_list = curl_exec($ch);
        $order_list =  json_decode($order_list, TRUE);
        //echo "<pre>"; print_r($order_list); die('End here!');
        
        foreach($order_list['items'] as $_order)
        {
            /** Order data  */
            $order_id = isset($_order['entity_id'])?$_order['entity_id']:0;
            $order_state = isset($_order['state'])?$_order['state']:'';
            $order_status = isset($_order['status'])?$_order['status']:'';
            $order_store_id = isset($_order['store_id'])?$_order['store_id']:0;
            $order_total = isset($_order['base_grand_total'])?$_order['base_grand_total']:0;
            $customer_email = isset($_order['customer_email'])?$_order['customer_email']:0;
            $order_shipping_amount = isset($_order['base_shipping_amount'])?$_order['base_shipping_amount']:0;
            $order_tax_amount = isset($_order['base_tax_amount'])?$_order['base_tax_amount']:0;
            $total_product_qty = isset($_order['total_qty_ordered'])?$_order['total_qty_ordered']:0;
            $customer_id = isset($_order['customer_id'])?$_order['customer_id']:0;
            $order_created_at = isset($_order['created_at'])?$_order['created_at']:date('Y-m-d h:i:s');
            $order_updated_at = isset($_order['updated_at'])?$_order['updated_at']:date('Y-m-d h:i:s');
            $payment_method = $_order['payment']['additional_information'][0];
            $refund_amount = isset($_order['subtotal_refunded'])?$_order['subtotal_refunded']:'';
            $discount_amount = isset($_order['discount_amount'])?$_order['discount_amount']:'';
            $order_items = $_order['items'];

            switch (strtolower($order_status)) {
                case "pending":
                    $order_status_2 = 'Pending';
                    break;
                case "complete":
                    $order_status_2 = "Completed";
                    break;
                case "canceled":
                    $order_status_2 = "Cancel";
                    break;
                case "closed":
                    $order_status_2 = "Closed";
                    break;
                case "fraud":
                    $order_status_2 = "Cancel";
                    break;
                case "holded":
                    $order_status_2 = "On Hold";
                    break;
                case "payment_review":
                    $order_status_2 = "Pending";
                    break;
                case "paypal_canceled_reversal":
                    $order_status_2 = "Cancel";
                    break;
                case "paypal_canceled_reversal":
                    $order_status_2 = "Cancel";
                    break;
                case "paypal_reversed":
                    $order_status_2 = "Cancel";
                    break;
                case "pending_payment":
                    $order_status_2 = "Pending";
                    break;
                case "pending_paypal":
                    $order_status_2 = "Pending";
                    break;
                case "processing":
                    $order_status_2 = "In Transit";
                    break;
                default:
                    $order_status_2 = "Pending";
            }

            /** Order Shipping address    */
            $ship_state = $ship_zip = $ship_address_1 = $ship_address_2 = $ship_city = $ship_phone = $ship_country_id = $shipping_address = $ship_first_name = $ship_last_name = '';
            if(isset($_order['extension_attributes']['shipping_assignments'][0]['shipping']['address']))
            {
                $order_shipping_add = $_order['extension_attributes']['shipping_assignments'][0]['shipping']['address'];
                $ship_state = isset($order_shipping_add['region'])?$order_shipping_add['region']:'';
                $ship_zip = isset($order_shipping_add['postcode'])?$order_shipping_add['postcode']:'';
                $ship_address_1 = isset($order_shipping_add['street'][0])?$order_shipping_add['street'][0]:'';
                $ship_address_2 = '';
                if(array_key_exists(1, $order_shipping_add['street']))
                {
                    $ship_address_2 = $order_shipping_add['street'][1];
                }
                $ship_city = isset($order_shipping_add['city'])?$order_shipping_add['city']:'';
                $ship_phone = isset($order_shipping_add['telephone'])?$order_shipping_add['telephone']:'';
                $ship_country_id = isset($order_shipping_add['country_id'])?$order_shipping_add['country_id']:'';
                $ship_first_name = isset($order_shipping_add['firstname'])?$order_shipping_add['firstname']:'';
                $ship_last_name = isset($order_shipping_add['lastname'])?$order_shipping_add['lastname']:'';
            }
            
            /** Order Billing address */
            $bill_state = $bill_zip = $bill_address_1 = $bill_address_2 = $bill_city = $bill_customer_email = $bill_phone = $bill_country_id = $bill_firstname = $bill_lastname = $billing_address = '';
            if($_order['billing_address']){
                $order_billing_add = $_order['billing_address'];
                $bill_state = isset($order_billing_add['region'])?$order_billing_add['region']:'';
                $bill_zip = isset($order_billing_add['postcode'])?$order_billing_add['postcode']:'';
                $bill_address_1 = isset($order_billing_add['street'][0])?$order_billing_add['street'][0]:'';
                $bill_address_2 = '';
                if(array_key_exists(1, $order_billing_add['street'])){
                    $bill_address_2 = $order_billing_add['street'][1];
                }
                $bill_city = isset($order_billing_add['city'])?$order_billing_add['city']:'';
                $bill_customer_email = isset($order_billing_add['email'])?$order_billing_add['email']:'';
                $bill_phone = isset($order_billing_add['telephone'])?$order_billing_add['telephone']:'';
                $bill_country_id = isset($order_billing_add['country_id'])?$order_billing_add['country_id']:'';
                $bill_firstname = isset($order_billing_add['firstname'])?$order_billing_add['firstname']:'';
                $bill_lastname = isset($order_billing_add['lastname'])?$order_billing_add['lastname']:'';
            }
            
            $magento_store_id = Stores::find()->select('store_id')->where(['store_name' => 'Magento2'])->one();
            
            $order_product_data = array();
            foreach($order_items as $_items){
                $product_id = $_items['product_id'];
                $title = $_items['name'];
                $sku = $_items['sku'];
                $price = $_items['price'];
                $quantity = $_items['qty_ordered'];
                $product_weight = isset($_items['weight'])?$_items['weight']:0;
                $order_product_data[] = array(
                    'product_id'=> $product_id,
                    'name'=> $title,
                    'sku'=> $sku,
                    'price'=> $price,
                    'qty_ordered'=> $quantity,
                    'weight'=> $product_weight,
                );
            }
            
            $order_data = array(
                'order_id'=> $order_id,
                'status'=> $order_status_2,
                'magento_store_id'=> $order_store_id,
                'order_grand_total'=>$order_total,
                'customer_id'=> $customer_id,
                'customer_email'=> $customer_email,
                'order_shipping_amount'=> $order_shipping_amount,
                'order_tax_amount'=> $order_tax_amount,
                'total_qty_ordered'=> $total_product_qty,
                'created_at'=> $order_created_at,
                'updated_at'=> $order_updated_at,
                'payment_method'=> $payment_method,
                'refund_amount'=> $refund_amount,
                'discount_amount'=> $discount_amount,
                'channel_store_name'=> 'Magento2',
                'channel_store_prefix'=> 'MG2',
                'mul_store_id' => $store_connection_id,
                'mul_channel_id' => '',
                'elliot_user_id'=> $user_id,
                'store_id'=>$magento_store_id->store_id,
                'currency_code' => $country_detail->currency_code,
                'channel_id'=>'',
                'shipping_address'=>array(
                    'street_1'=> $ship_address_1,
                    'street_2'=> $ship_address_2,
                    'state'=> $ship_state,
                    'city'=> $ship_city,
                    'country'=> $ship_country_id,
                    'country_id'=> $ship_country_id,
                    'postcode'=> $ship_zip,
                    'email'=> $customer_email,
                    'telephone'=> $ship_phone,
                    'firstname'=> $ship_first_name,
                    'lastname'=> $ship_last_name,
                ),
                'billing_address'=>array(
                    'street_1'=> $bill_address_1,
                    'street_2'=> $bill_address_2,
                    'state'=> $bill_state,
                    'city'=> $bill_city,
                    'country'=> $bill_country_id,
                    'country_id'=> $bill_country_id,
                    'postcode'=> $bill_zip,
                    'email'=> $customer_email,
                    'telephone'=> $bill_phone,
                    'firstname'=> $bill_firstname,
                    'lastname'=> $bill_lastname,
                ),
                'items'=>$order_product_data,
            );
            Stores::orderImportingCommon($order_data);
        }
    }
    /**
     * Magento Product create and update
     */
    public function actionMagento2ProductCreate()
    {
        $url_data = $_GET['data'];
        $url_data = urldecode($url_data);
        $product_info = json_decode($url_data, true);
        
        $shop_url = $product_info['magento_url'];
        $url_parse = parse_url($shop_url);
        $shop_url_1 = '';
        if($url_parse['scheme']=='https'){
            $shop_url_1 = str_replace("https", 'http', $shop_url);
        }
        else{
            $shop_url_1 = str_replace("http", 'https', $shop_url);
        }

        $get_users = User::find()->where("magento_shop_url IN('".$shop_url."','".$shop_url_1."')")->all();
        foreach ($get_users as $_user) {
            $config = yii\helpers\ArrayHelper::merge(   
                require($_SERVER['DOCUMENT_ROOT'].'/common/config/main.php'),
                require($_SERVER['DOCUMENT_ROOT'].'/common/users_config/' . $_user->domain_name . '/main-local.php'),
                require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main.php'), 
                require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main-local.php')
            );
            $application = new yii\web\Application($config);
            $user_id = $_user->id;
            $get_magento_id = Stores::find()->select('store_id')->where(['store_name' => 'Magento2'])->one();
            $magento_store_id = $get_magento_id->store_id;
            $store_magento = StoresConnection::find()->where(['store_id' => $magento_store_id, 'user_id' => $user_id])->one();
            if(!empty($store_magento))
            {
                $magento_shop = $store_magento->mag_shop;
                $magento_2_access_token = $store_magento->mag_soap_api;
                if (!empty($store_magento)) {
                    $mage_product_id = $product_info['product_id'];
                    $product_name = $product_info['name'];
                    $product_sku = $product_info['sku'];
                    $product_categories_ids = $product_info['categories'];
                    $product_description = $product_info['description'];
                    $product_weight = $product_info['weight'];
                    $product_status = $product_info['status'];
                    $product_created_at = $product_info['product_created_at'];
                    $product_updated_at = $product_info['product_updated_at'];
                    $product_price = $product_info['price'];
                    $qty = $product_info['qty'];
                    $stock_status = $product_info['is_in_stock'];
                    $websites = $product_info['store_id'];
                    $product_url = $product_info['product_url'];
                    
                    $product_image = $product_info['images'];
                    $product_image_pos = $product_info['position'];
                    $base_img = $product_info['base_img'];
                    $i = 0;
                    $product_image_data = array();
                    if(!empty($product_image)){
                        foreach ($product_image as $_image) {
                            $position = $product_image_pos[$i];
                            if ($base_img == $_image) {
                                $default_img = $_image;
                            }
                            $product_image_data[] = array(
                                'image_url'=>$_image,
                                'label'=>'',
                                'position'=>$position,
                                'base_img'=>$default_img,
                            );
                            $i++;
                        }
                    }
                    
                    $product_data = array(
                        'product_id'=>$mage_product_id, // Stores/Channel product ID
                        'name'=>$product_name,  // Product name
                        'sku'=>$product_sku,    // Product SKU
                        'description'=>$product_description,    // Product Description
                        'product_url_path'=>$product_url,   // Product url if null give blank value
                        'weight'=>$product_weight,  // Product weight if null give blank value
                        'status'=>$product_status,  // Product status (visible or hidden). Give in 0 or 1 form (0 = hidden, 1 = visible).
                        'price'=>$product_price,    // Porduct price
                        'sale_price'=>$product_price,   // Product sale price if null give Product price value
                        'qty'=>$qty,    //Product quantity 
                        'stock_status'=>$stock_status,  // Product stock status ("in stock" or "out of stock"). 
                                                        // Give in 0 or 1 form (0 = out of stock, 1 = in stock)
                        'websites'=>array(),    //This is for only magento give only and blank array
                        'brand'=>'',    // Product brand if any
                        'low_stock_notification'=>5,    // Porduct low stock notification if any otherwise give default 5 value
                        'created_at'=>$product_created_at,  // Product created at date format date('Y-m-d H:i:s')
                        'updated_at'=>$product_updated_at,  // Product updated at date format date('Y-m-d H:i:s')
                        'channel_store_name'=>'Magento2',   // Channel or Store name
                        'channel_store_prefix'=>'MG2',      // Channel or store prefix id
                        'elliot_user_id'=>$user_id,         // Elliot user id
                        'store_id'=>$magento_store_id,    // if your are importing store give store id
                        'channel_id'=>'',   // if your are importing channel give channel id
                        'barcode'=>'',  // Product barcode if any
                        'ean'=>'',  // Product ean if any
                        'jan'=>'',  // Product jan if any
                        'isban'=>'',    // Product isban if any
                        'mpn'=>'',  // Product mpn if any
                        'categories'=>array(), // Product categroy array. If null give a blank array 
                        'images'=>$product_image_data,  // Product images data
                    );
                    
                    $product_check = ProductAbbrivation::find()->where(['channel_abb_id' => 'MG2'.$mage_product_id])->one();
                    if(!empty($product_check)){
                        $elliot_product_id =  Stores::productImportingCommon($product_data);
                        if($elliot_product_id!=''){
                            $productCategoryDelete = ProductCategories::deleteAll(['product_ID' => $elliot_product_id]);
                            foreach ($product_categories_ids as $_cat){
                                $category_id = CategoryAbbrivation::find()->where(['channel_abb_id' => 'MG2' . $_cat])->one();
                                if (!empty($category_id)){
                                    $category_model = new ProductCategories();
                                    $category_model->elliot_user_id = $user_id;
                                    $category_model->category_ID = $category_id->category_ID;
                                    $category_model->product_ID = $elliot_product_id;
                                    $category_model->created_at = date('Y-m-d h:i:s', strtotime($product_created_at));
                                    $category_model->save();
                                }
                                else{
                                    try {
                                        $requestUrlCategories = $magento_shop.'index.php/rest/V1/categories?searchCriteria=';
                                        $access_token = array("Authorization: Bearer $magento_2_access_token"); 
                                        $this->magento2CategoriesImporting($access_token, $requestUrlCategories, $user_id);
                                        $category_id_2 = CategoryAbbrivation::find()->where(['channel_abb_id' => 'MG2' . $_cat])->one();
                                        if (!empty($category_id_2)):
                                            $category_model_2 = new ProductCategories();
                                            $category_model_2->elliot_user_id = $user_id;
                                            $category_model_2->category_ID = $category_id_2->category_ID;
                                            $category_model_2->product_ID = $elliot_product_id;
                                            $category_model_2->created_at = date('Y-m-d h:i:s', strtotime($product_created_at));
                                            $category_model_2->save();
                                        endif;
                                    } 
                                    catch (Exception $e) {
                                        $e->getMessage();
                                    }
                                }
                            }
                        }
                    }
                    else{
                        Stores::producUpdateCommon($product_data);
                    }
                }
            }
        }
    }
    
    /**
     * Magento Order create and update
     */
    public function actionMagento2OrderCreate() {
        $url_data = $_GET['data'];
        $url_data = urldecode($url_data);
        $order_data = json_decode($url_data, true);
        $shop_url = $order_data['magento_url'];
        $url_parse = parse_url($shop_url);
        $shop_url_1 = '';
        if($url_parse['scheme']=='https')
        {
          $shop_url_1 = str_replace("https", 'http', $shop_url);
        }
        else
        {
          $shop_url_1 = str_replace("http", 'https', $shop_url);
        }

        $get_users = User::find()->where("magento_shop_url IN('".$shop_url."','".$shop_url_1."')")->all();
        foreach ($get_users as $_user)
        {
            $config = yii\helpers\ArrayHelper::merge(
                require($_SERVER['DOCUMENT_ROOT'].'/common/config/main.php'),
                require($_SERVER['DOCUMENT_ROOT'].'/common/users_config/' . $_user->domain_name . '/main-local.php'),
                require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main.php'),
                require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main-local.php')
            );
            $application = new yii\web\Application($config);
            $user_id = $_user->id;
            $get_magento_id = Stores::find()->select('store_id')->where(['store_name' => 'Magento2'])->one();
            $magento_store_id = $get_magento_id->store_id;
            $store_magento = StoresConnection::find()->where(['store_id' => $magento_store_id, 'user_id' => $user_id])->one();
            if (!empty($store_magento)) {
                /**
                 * Order data
                 */
                $order_id = isset($order_data['order_id'])?$order_data['order_id']:0;
                $order_state = isset($order_data['order_state'])?$order_data['order_state']:'';
                $order_status = isset($order_data['order_status'])?$order_data['order_status']:'';
                $order_store_id = isset($order_data['order_store_id'])?$order_data['order_store_id']:0;
                $order_total = isset($order_data['order_total'])?$order_data['order_total']:0;
                $customer_email = isset($order_data['customer_email'])?$order_data['customer_email']:'';
                $order_shipping_amount = isset($order_data['order_shipping_amount'])?$order_data['order_shipping_amount']:0;
                $order_tax_amount = isset($order_data['base_tax_amount'])?$order_data['base_tax_amount']:0;
                $order_product_qty = isset($order_data['order_product_qty'])?$order_data['order_product_qty']:'';
                $customer_id = isset($order_data['customer_id'])?$order_data['customer_id']:'';
                $order_created_at = $order_data['order_created_at'];
                $order_updated_at = $order_data['order_updated_at'];
                $refund_amount = isset($order_data['refund_amount'])?$order_data['refund_amount']:0;
                $payment_method = isset($order_data['payment_method'])?$order_data['payment_method']:'';
                $discount_amount = isset($order_data['discount_amount'])?$order_data['discount_amount']:0;
                $order_items = $order_data['order_items'];

                switch (strtolower($order_status)) {
                    case "pending":
                        $order_status_2 = 'Pending';
                        break;
                    case "complete":
                        $order_status_2 = "Completed";
                        break;
                    case "canceled":
                        $order_status_2 = "Cancel";
                        break;
                    case "closed":
                        $order_status_2 = "Closed";
                        break;
                    case "fraud":
                        $order_status_2 = "Cancel";
                        break;
                    case "holded":
                        $order_status_2 = "On Hold";
                        break;
                    case "payment_review":
                        $order_status_2 = "Pending";
                        break;
                    case "paypal_canceled_reversal":
                        $order_status_2 = "Cancel";
                        break;
                    case "paypal_canceled_reversal":
                        $order_status_2 = "Cancel";
                        break;
                    case "paypal_reversed":
                        $order_status_2 = "Cancel";
                        break;
                    case "pending_payment":
                        $order_status_2 = "Pending";
                        break;
                    case "pending_paypal":
                        $order_status_2 = "Pending";
                        break;
                    case "processing":
                        $order_status_2 = "In Transit";
                        break;
                    default:
                        $order_status_2 = "Pending";
                }
                
                /**
                 * Order Shipping address
                 */
                $order_shipping_add = $order_data['shipping_detail'];
                $ship_state = isset($order_shipping_add['region'])?$order_shipping_add['region']:'';
                $ship_zip = $order_shipping_add['postcode'];
                $ship_address = $order_shipping_add['street'];
                $ship_address_1 = $ship_address[0];
                $ship_address_2 = '';
                if (array_key_exists('1', $ship_address)) {
                    $ship_address_2 = $ship_address[1];
                }
                $ship_city = $order_shipping_add['city'];
                $ship_customer_email = $order_shipping_add['email'];
                $ship_phone = $order_shipping_add['telephone'];
                $ship_country_id = $order_shipping_add['country_id'];
                $ship_first_name = $order_shipping_add['firstname'];
                $ship_last_name = $order_shipping_add['lastname'];
                $ship_middlename = $order_shipping_add['middlename'];
                $shipping_address = implode(',', $ship_address) . "," . $ship_city . "," . $ship_state . "," . $ship_zip . "," . $ship_country_id;
                
                /**
                 * Order Billing address
                 */
                $order_billing_add = $order_data['billing_detail'];
                $bill_state = isset($order_billing_add['region'])?$order_billing_add['region']:'';
                $bill_zip = $order_billing_add['postcode'];
                $bill_address = $order_billing_add['street'];
                $bill_address_1 = $bill_address[0];
                $bill_address_2 = '';
                if (array_key_exists('1', $bill_address)) {
                    $bill_address_2 = $ship_address[1];
                }
                $bill_city = $order_billing_add['city'];
                $bill_customer_email = $order_billing_add['email'];
                $bill_phone = $order_billing_add['telephone'];
                $bill_country_id = $order_billing_add['country_id'];
                $bill_firstname = $order_billing_add['firstname'];
                $bill_lastname = $order_billing_add['lastname'];
                $bill_middlename = $order_billing_add['middlename'];
                $billing_address = implode(',', $bill_address) . "," . $bill_city . "," . $bill_state . "," . $bill_zip . "," . $bill_country_id;
                
                
                $order_product_data = array();
                foreach($order_items as $_items){
                    $product_id = $_items['product_id'];
                    $title = $_items['name'];
                    $sku = $_items['sku'];
                    $price = $_items['price'];
                    $quantity = $_items['qty_ordered'];
                    $product_weight = isset($_items['weight'])?$_items['weight']:0;
                    $order_product_data[] = array(
                        'product_id'=> $product_id,
                        'name'=> $title,
                        'sku'=> $sku,
                        'price'=> $price,
                        'qty_ordered'=> $quantity,
                        'weight'=> $product_weight,
                    );
                }

                $order_data = array(
                    'order_id'=> $order_id,
                    'status'=> $order_status_2,
                    'magento_store_id'=> $order_store_id,
                    'order_grand_total'=>$order_total,
                    'customer_id'=> $customer_id,
                    'customer_email'=> $customer_email,
                    'order_shipping_amount'=> $order_shipping_amount,
                    'order_tax_amount'=> $order_tax_amount,
                    'total_qty_ordered'=> $order_product_qty,
                    'created_at'=> $order_created_at,
                    'updated_at'=> $order_updated_at,
                    'payment_method'=> $payment_method,
                    'refund_amount'=> $refund_amount,
                    'discount_amount'=> $discount_amount,
                    'import_or_create'=> 'create',
                    'channel_store_name'=> 'Magento2',
                    'channel_store_prefix'=> 'MG2',
                    'elliot_user_id'=> $user_id,
                    'store_id'=>$magento_store_id,
                    'channel_id'=>'',
                    'shipping_address'=>array(
                        'street_1'=> $ship_address_1,
                        'street_2'=> $ship_address_2,
                        'state'=> $ship_state,
                        'city'=> $ship_city,
                        'country'=> $ship_country_id,
                        'country_id'=> $ship_country_id,
                        'postcode'=> $ship_zip,
                        'email'=> $customer_email,
                        'telephone'=> $ship_phone,
                        'firstname'=> $ship_first_name,
                        'lastname'=> $ship_last_name,
                    ),
                    'billing_address'=>array(
                        'street_1'=> $bill_address_1,
                        'street_2'=> $bill_address_2,
                        'state'=> $bill_state,
                        'city'=> $bill_city,
                        'country'=> $bill_country_id,
                        'country_id'=> $bill_country_id,
                        'postcode'=> $bill_zip,
                        'email'=> $customer_email,
                        'telephone'=> $bill_phone,
                        'firstname'=> $bill_firstname,
                        'lastname'=> $bill_lastname,
                    ),
                    'items'=>$order_product_data,
                );
                
                $check_order = Orders::find()->Where(['channel_abb_id' => 'MG2' . $order_id])->one();
                if(empty($check_order)){
                    Stores::orderImportingCommon($order_data);
                }
                else{
                    Stores::orderUpdateCommon($order_data);
                }
            }
        }
    }
    
    /**
     * Magento 2 Customer Create
     */
    public function actionMagento2CustomerCreate() {
        $url_data = $_GET['data'];
        $url_data = urldecode($url_data);
        $customer_data = json_decode($url_data, true);
        $shop_url = $customer_data['magento_url'];
        $url_parse = parse_url($shop_url);
        $shop_url_1 = '';
        if($url_parse['scheme']=='https')
        {
          $shop_url_1 = str_replace("https", 'http', $shop_url);
        }
        else
        {
          $shop_url_1 = str_replace("http", 'https', $shop_url);
        }
        $get_users = User::find()->where("magento_shop_url IN('".$shop_url."','".$shop_url_1."')")->all();
        foreach ($get_users as $_user) {
            $config = yii\helpers\ArrayHelper::merge(
                require($_SERVER['DOCUMENT_ROOT'].'/common/config/main.php'), 
                require($_SERVER['DOCUMENT_ROOT'].'/common/users_config/' . $_user->domain_name . '/main-local.php'),
                require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main.php'), 
                require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main-local.php')
            );
            $application = new yii\web\Application($config);
            
            $user_id = $_user->id;
            $get_magento_id = Stores::find()->select('store_id')->where(['store_name' => 'Magento2'])->one();
            $magento_store_id = $get_magento_id->store_id;
            $store_magento = StoresConnection::find()->where(['store_id' => $magento_store_id, 'user_id' => $user_id])->one();
            if (!empty($store_magento)) {
                $checkCustomerModel = CustomerUser::find()->where(['channel_abb_id' => 'MG2' . $customer_data['customer_id']])->one();
                
                if (empty($checkCustomerModel)):
                    $Customers_model = new CustomerUser();
                    $Customers_model->channel_abb_id = 'MG2' . $customer_data['customer_id'];
                    $Customers_model->first_name = $customer_data['firstname'];
                    $Customers_model->last_name = $customer_data['lastname'];
                    $Customers_model->email = $customer_data['email'];
                    $Customers_model->channel_acquired = 'Magento2';
                    $Customers_model->date_acquired = date('Y-m-d h:i:s', strtotime($customer_data['created_at']));
                    $Customers_model->street_1 = '';
                    $Customers_model->street_2 = '';
                    $Customers_model->city = '';
                    $Customers_model->country = '';
                    $Customers_model->state = '';
                    $Customers_model->zip = '';
                    $Customers_model->phone_number = '';
                    $Customers_model->ship_street_1 = '';
                    $Customers_model->ship_city = '';
                    $Customers_model->ship_state = '';
                    $Customers_model->ship_zip = '';
                    $Customers_model->ship_country = '';
                    $Customers_model->magento_store_id = $customer_data['website_id'];
                    $Customers_model->created_at = date('Y-m-d h:i:s', strtotime($customer_data['created_at']));
                    $Customers_model->updated_at = date('Y-m-d h:i:s', strtotime($customer_data['updated_at']));
                    //Save Elliot User id
                    $Customers_model->elliot_user_id = $user_id;
                    $Customers_model->save(false);
                endif;
            }
        }
    }
    /**
     * Magento 2 Customer address update
     */
    public function actionMagento2CustomerAddressUpdate() {
        $url_data = $_GET['data'];
        $url_data = urldecode($url_data);
        $customer_data = json_decode($url_data, true);
        $shop_url = $customer_data['magento_url'];
        
        $url_parse = parse_url($shop_url);
        $shop_url_1 = '';
        if($url_parse['scheme']=='https')
        {
          $shop_url_1 = str_replace("https", 'http', $shop_url);
        }
        else
        {
          $shop_url_1 = str_replace("http", 'https', $shop_url);
        }
        $get_users = User::find()->where("magento_shop_url IN('".$shop_url."','".$shop_url_1."')")->all();
        
        $street_add_1 = $customer_data['street'][0];
        $street_add_2 = '';
        if (array_key_exists(1, $customer_data['street'])) {
            $street_add_2 = $customer_data['street'][1];
        }
        foreach ($get_users as $_user) {
            $config = yii\helpers\ArrayHelper::merge(
                require($_SERVER['DOCUMENT_ROOT'].'/common/config/main.php'), 
                require($_SERVER['DOCUMENT_ROOT'].'/common/users_config/'.$_user->domain_name.'/main-local.php'),
                require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main.php'),
                require($_SERVER['DOCUMENT_ROOT'].'/backend/config/main-local.php')
            );
            $application = new yii\web\Application($config);
            $user_id = $_user->id;
            $get_magento_id = Stores::find()->select('store_id')->where(['store_name' => 'Magento2'])->one();
            $magento_store_id = $get_magento_id->store_id;
            $store_magento = StoresConnection::find()->where(['store_id' => $magento_store_id, 'user_id' => $user_id])->one();
            if (!empty($store_magento)) {
                $Customers_model = CustomerUser::find()->where(['channel_abb_id' => 'MG2' . $customer_data['customer_id']])->one();
                if (!empty($Customers_model)):
                    $Customers_model->street_1 = $street_add_1;
                    $Customers_model->street_2 = $street_add_2;
                    $Customers_model->city = $customer_data['city'];
                    $Customers_model->country = $customer_data['country_id'];
                    $Customers_model->state = $customer_data['region'];
                    $Customers_model->zip = $customer_data['postcode'];
                    $Customers_model->phone_number = $customer_data['telephone'];
                    $Customers_model->ship_street_1 = $street_add_1;
                    $Customers_model->ship_street_2 = $street_add_2;
                    $Customers_model->ship_city = $customer_data['city'];
                    $Customers_model->ship_state = $customer_data['region'];
                    $Customers_model->ship_zip = $customer_data['postcode'];
                    $Customers_model->ship_country = $customer_data['country_id'];
                    $Customers_model->save(false);
                endif;
            }
        }
    }
    
    public function actionTest()
    {
        //die('haerere!@');
        //$shop_url = 'http://staging.brstdev.com/Elliot_store/';
        //$url_parse = parse_url($shop_url);
        //$shop_url_1 = '';
        //if($url_parse['scheme']=='https')
        //{
        //  $shop_url_1 = str_replace("https", 'http', $shop_url);
        //}
        //else
        //{
        //  $shop_url_1 = str_replace("http", 'https', $shop_url);
        //}
//        $length = 150;
//        $start = 0;
//        $search = '';        
//        $products = Products::find()
//            ->Where(['products.elliot_user_id' => 1635])
//            ->joinWith('productChannels')
//            ->limit($length)->offset($start)
//            ->andFilterWhere([
//                'or',
//                    ['like', 'product_name', $search],
//                    ['like', 'SKU', $search],
//                    ['like', 'price', $search],
//            ])
//            ->andWhere(['product_channel.store_id' => 3])
//            ->andWhere(['products.product_status' => 'active'])
//            ->andWhere(['products.permanent_hidden' => 'active'])
//            ->andWhere(['product_channel.status' => 'yes'])
//            ->all();
//        echo $count = count($products);
//        echo'<pre>';
//        print_r($products);
//        die('f1111111111111');
        
//        $prefix_id = 'MG24';
//        //$sku = '24-MB01-0001';
//        $sku = '24-MB04';
//        $product_sku_check = Products::find()->Where(['SKU' => $sku])
//        ->with(['productAbbrivation' => function($query) use ($prefix_id) {
//                $query->andWhere(['channel_abb_id' => $prefix_id]);
//        }])->asArray()->one();
//        
//        if(!empty($product_sku_check) && !empty($product_sku_check['productAbbrivation'])){
//            echo "<pre>"; print_r($product_sku_check);
//        }
//        
//        echo "kajsdfklj";
        
        //$order = Orders::find()->orderBy(['created_at' => SORT_DESC])->limit(5)->all();
        //echo "<pre>"; print_r($order); die('End herer!');
		
		// $pro = Products::find()->all();
		// $name_array = array();
		// foreach($pro as $_pro){
			// $name_array[] = $_pro->product_name;
		// }
		
		// echo "<pre>"; print_r($name_array); echo "</pre>";
		// echo "<br>";
		// echo "<br>";
		// echo "Encode data herer";
		// echo "<br>";
		// echo "<br>";
		// echo json_encode($name_array);
		
		
		//$order_data = Orders::find()->select(['SUM(total_amount) as my_sum', 'customer_id'])->groupBy(['customer_id'])->orderBy(['my_sum' => SORT_DESC])->limit(25)->all();
		// $connection = \Yii::$app->db;
		// $order_sum_query = $connection->createCommand('SELECT SUM(total_amount) as customer_total_amount, customer_id FROM orders GROUP BY customer_id order by customer_total_amount '.$asc.' limit '.$start.' '.$length);
        // $order_sum = $order_sum_query->queryAll();
		// $order_sum_array = array();
		// $customer_ids = '';
		// foreach($order_sum as $_order_sum){
			// $order_sum_array[$_order_sum['customer_id']] = number_format((float) $_order_sum['total_amount_data'], 2, '.', '');
			// $customer_ids .= "'".$_order_sum['customer_id']."',";
		// }
		// $customer_ids = rtrim($customer_ids, ',');
		// $customers_data = CustomerUser::find()->Where('customer_ID IN('.$customer_ids.')')->andWhere(['and', ['elliot_user_id' => $user_id], ['people_visible_status' => 'active']])
                // ->limit($length)->offset($start)
                // ->andFilterWhere([
                    // 'or',
                        // ['like', 'concat(first_name," ",last_name)', $search],
                        // ['like', 'email', $search],
                        // ['like', 'concat(city,", ",country)', $search],
                // ])
                // ->orderBy($orderby_str . " " . $asc)
                // ->all();
		
		// echo "<pre>"; print_r($order_sum); echo "</pre>";
		
		//$orders_data = Orders::find()->Where(['customer_id' => '19166'])->sum('product_qauntity');
		//echo "<pre>"; print_r($orders_data); die;
		
		$resource_cnt = Yii::$app->db->createCommand()
		->select('sum(product_qauntity) as amt')
		->from('resources')
		->where('customer_id = 19166')
		->queryRow();

		echo "<pre>"; print_r($resource_cnt); echo "</pre>";
		
		echo "Echo test";
}


}

                    