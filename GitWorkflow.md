## Github Workflow

To prevent chaotic git commit and improve git work flow let's follow this guide.

This project uses the git forking workflow, https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow

Write commit messages "in the imperative mood". Read this if you haven't already: http://chris.beams.io/posts/git-commit/

Each commit should do one thing. It's no problem making lots of small commits, it's actualy much better than infrequent large commit because they are easier to review.

We use the Pull Request system of github.

`development` branch is just our `main` branch.


#### Get Started

1. You first need to fork the project repo(https://github.com/S86Agency/elliot_dev) to your own private repo.
2. Clone your own private repo in to your PC workspace(i.e it will be c:/wamp64/www)
    ```
    $ git clone -b development https://github.com/<your_username>/elliot_dev.git
    ```

3. Add remote `upstream`
    ```
    $ git remote add upstream https://github.com/S86Agency/elliot_dev.git
    ```

    `upstream = team project repo`, `origin = your own project repo forked from team project repo`
    
#### Creating a PR

> DON'T USE **`git pull`** command for updating other changes, you can use `git fetch` and `git rebase` to update changes from `upstream`

1. Fetch the upstream 
    ```
    $ git fetch upstream development
    ```
    
2. Rebase to *`upstream/development`* to update changes from others
    ```
    $ git rebase upstream/development
    ```
    
  *If you have conflict , you have to resolve each conflict on each commit , do it manually or add `kdiff3` / `Beyond Compare` or anything that fit you , add these modification and do `$ git rebase --continue` until you cleared the conflicts.*
  
  *If you have unstashed files you can use `git stash` command to pass temporarily*
  ```
  $ git stash
  $ git rebase upstream/development
  $ git stash pop
  ```
  
3. Do your work, *make commits*, you should create directly a PR and *push your work frequently*.

4. Test your code.

5. You can now push your code.
  * If the branch is new you just run ```$ git push origin development``` 
  * If you had rebase you need to force the ```$ git push origin development -f```
  
6. Create the PR from new commits of `origin/development` into `upstream/development`.
  You need to wait for administrator's review after create PR.
  
  Once your PR was approved and rebased to `upstream/development` you have to fetch and rebase `upstream/development` into your local workspace.

**More you rebase your branch often, less you get problems. So don't wait ! You should rebase each time a commit is added to the `upstream/development`.**

**DISCLAIMER** : Use *`push -f`* only with **Fork and PR project** like here, never on a project were everyone is working on the **same remote**. Your rewrite commits history.

> DON'T MERGE yourself PR, it should be accomplished by Project Manager or Git Manager after review.

> **Do *`Squash and merge`* so that `upstream`'s commits history is not messy.** It's for git administrator.
