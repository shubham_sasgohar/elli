<?php

$vendorDir = dirname(__DIR__);

return array(
    'yiisoft/yii2-bootstrap' =>
    array(
        'name' => 'yiisoft/yii2-bootstrap',
        'version' => '2.0.6.0',
        'alias' =>
        array(
            '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
        ),
    ),
    'yiisoft/yii2-gii' =>
    array(
        'name' => 'yiisoft/yii2-gii',
        'version' => '2.0.5.0',
        'alias' =>
        array(
            '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
        ),
    ),
    'yiisoft/yii2-faker' =>
    array(
        'name' => 'yiisoft/yii2-faker',
        'version' => '2.0.3.0',
        'alias' =>
        array(
            '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
        ),
    ),
    'yiisoft/yii2-imagine' =>
    array(
        'name' => 'yiisoft/yii2-imagine',
        'version' => '2.1.0.0',
        'alias' =>
        array(
            '@yii/imagine' => $vendorDir . '/yiisoft/yii2-imagine',
        ),
    ),
    'toriphes/yii2-console-runner' =>
    array(
        'name' => 'toriphes/yii2-console-runner',
        'version' => '1.0.0.0',
        'alias' =>
        array(
            '@toriphes/console' => $vendorDir . '/toriphes/yii2-console-runner',
        ),
    ),
    'trntv/yii2-command-bus' =>
    array(
        'name' => 'trntv/yii2-command-bus',
        'version' => '2.1.3.0',
        'alias' =>
        array(
            '@trntv/bus' => $vendorDir . '/trntv/yii2-command-bus/src',
        ),
    ),
    'tebazil/yii2-console-runner' =>
    array(
        'name' => 'tebazil/yii2-console-runner',
        'version' => '0.0.0.0',
        'alias' =>
        array(
            '@tebazil/runner' => $vendorDir . '/tebazil/yii2-console-runner/src',
        ),
    ),
    'kartik-v/yii2-mpdf' =>
    array(
        'name' => 'kartik-v/yii2-mpdf',
        'version' => '1.0.1.0',
        'alias' =>
        array(
            '@kartik/mpdf' => $vendorDir . '/kartik-v/yii2-mpdf',
        ),
    ),
    'kartik-v/yii2-mpdf' =>
    array(
        'name' => 'kartik-v/yii2-mpdf',
        'version' => '9999999-dev',
        'alias' =>
        array(
            '@kartik/mpdf' => $vendorDir . '/kartik-v/yii2-mpdf',
        ),
    ),
    'yiisoft/yii2-swiftmailer' =>
    array(
        'name' => 'yiisoft/yii2-swiftmailer',
        'version' => '2.0.7.0',
        'alias' =>
        array(
            '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
        ),
    ),
    'yiisoft/yii2-debug' =>
    array(
        'name' => 'yiisoft/yii2-debug',
        'version' => '2.0.9.0',
        'alias' =>
        array(
            '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
        ),
    ),
);
