<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');

class AlibabaAeLogisticsAeopOnlineLogisticsFeeItem extends SDKDomain {

       	
    private $code;
    
        /**
    * @return 费用项code
    */
        public function getCode() {
        return $this->code;
    }
    
    /**
     * 设置费用项code     
     * @param String $code     
     * 参数示例：<pre>other_delivery_fee</pre>     
     * 此参数必填     */
    public function setCode( $code) {
        $this->code = $code;
    }
    
        	
    private $name;
    
        /**
    * @return 费用名称
    */
        public function getName() {
        return $this->name;
    }
    
    /**
     * 设置费用名称     
     * @param String $name     
     * 参数示例：<pre>附加费</pre>     
     * 此参数必填     */
    public function setName( $name) {
        $this->name = $name;
    }
    
        	
    private $currency;
    
        /**
    * @return 币种
    */
        public function getCurrency() {
        return $this->currency;
    }
    
    /**
     * 设置币种     
     * @param String $currency     
     * 参数示例：<pre>USD</pre>     
     * 此参数必填     */
    public function setCurrency( $currency) {
        $this->currency = $currency;
    }
    
        	
    private $amount;
    
        /**
    * @return 金额
    */
        public function getAmount() {
        return $this->amount;
    }
    
    /**
     * 设置金额     
     * @param Double $amount     
     * 参数示例：<pre>12.34</pre>     
     * 此参数必填     */
    public function setAmount( $amount) {
        $this->amount = $amount;
    }
    
    	
	private $stdResult;
	
	public function setStdResult($stdResult) {
		$this->stdResult = $stdResult;
					    			    			if (array_key_exists ( "code", $this->stdResult )) {
    				$this->code = $this->stdResult->{"code"};
    			}
    			    		    				    			    			if (array_key_exists ( "name", $this->stdResult )) {
    				$this->name = $this->stdResult->{"name"};
    			}
    			    		    				    			    			if (array_key_exists ( "currency", $this->stdResult )) {
    				$this->currency = $this->stdResult->{"currency"};
    			}
    			    		    				    			    			if (array_key_exists ( "amount", $this->stdResult )) {
    				$this->amount = $this->stdResult->{"amount"};
    			}
    			    		    		}
	
	private $arrayResult;
	public function setArrayResult($arrayResult) {
		$this->arrayResult = $arrayResult;
				    		    			if (array_key_exists ( "code", $this->arrayResult )) {
    			$this->code = $arrayResult['code'];
    			}
    		    	    			    		    			if (array_key_exists ( "name", $this->arrayResult )) {
    			$this->name = $arrayResult['name'];
    			}
    		    	    			    		    			if (array_key_exists ( "currency", $this->arrayResult )) {
    			$this->currency = $arrayResult['currency'];
    			}
    		    	    			    		    			if (array_key_exists ( "amount", $this->arrayResult )) {
    			$this->amount = $arrayResult['amount'];
    			}
    		    	    		}
 
   
}
?>