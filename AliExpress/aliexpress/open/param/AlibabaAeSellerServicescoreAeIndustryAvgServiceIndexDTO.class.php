<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');

class AlibabaAeSellerServicescoreAeIndustryAvgServiceIndexDTO extends SDKDomain {

       	
    private $pcateId;
    
        /**
    * @return 发布类目id
    */
        public function getPcateId() {
        return $this->pcateId;
    }
    
    /**
     * 设置发布类目id     
     * @param Long $pcateId     
     * 参数示例：<pre>3280111</pre>     
     * 此参数必填     */
    public function setPcateId( $pcateId) {
        $this->pcateId = $pcateId;
    }
    
        	
    private $pcateFlag;
    
        /**
    * @return 发布类目层级
    */
        public function getPcateFlag() {
        return $this->pcateFlag;
    }
    
    /**
     * 设置发布类目层级     
     * @param Long $pcateFlag     
     * 参数示例：<pre>3</pre>     
     * 此参数必填     */
    public function setPcateFlag( $pcateFlag) {
        $this->pcateFlag = $pcateFlag;
    }
    
        	
    private $buyNotSelRate;
    
        /**
    * @return 拍而不卖率
    */
        public function getBuyNotSelRate() {
        return $this->buyNotSelRate;
    }
    
    /**
     * 设置拍而不卖率     
     * @param String $buyNotSelRate     
     * 参数示例：<pre>0.39%</pre>     
     * 此参数必填     */
    public function setBuyNotSelRate( $buyNotSelRate) {
        $this->buyNotSelRate = $buyNotSelRate;
    }
    
        	
    private $nrDisclaimerIssueRate;
    
        /**
    * @return 免责后未收到货纠纷发起率
    */
        public function getNrDisclaimerIssueRate() {
        return $this->nrDisclaimerIssueRate;
    }
    
    /**
     * 设置免责后未收到货纠纷发起率     
     * @param String $nrDisclaimerIssueRate     
     * 参数示例：<pre>4.6%</pre>     
     * 此参数必填     */
    public function setNrDisclaimerIssueRate( $nrDisclaimerIssueRate) {
        $this->nrDisclaimerIssueRate = $nrDisclaimerIssueRate;
    }
    
        	
    private $snadDisclaimerIssueRate;
    
        /**
    * @return 免责后货不对版纠纷发起率
    */
        public function getSnadDisclaimerIssueRate() {
        return $this->snadDisclaimerIssueRate;
    }
    
    /**
     * 设置免责后货不对版纠纷发起率     
     * @param String $snadDisclaimerIssueRate     
     * 参数示例：<pre>0.37%</pre>     
     * 此参数必填     */
    public function setSnadDisclaimerIssueRate( $snadDisclaimerIssueRate) {
        $this->snadDisclaimerIssueRate = $snadDisclaimerIssueRate;
    }
    
        	
    private $dsrProdScore;
    
        /**
    * @return DSR商品评价综合评分
    */
        public function getDsrProdScore() {
        return $this->dsrProdScore;
    }
    
    /**
     * 设置DSR商品评价综合评分     
     * @param String $dsrProdScore     
     * 参数示例：<pre>4.41</pre>     
     * 此参数必填     */
    public function setDsrProdScore( $dsrProdScore) {
        $this->dsrProdScore = $dsrProdScore;
    }
    
        	
    private $dsrCommunicateScore;
    
        /**
    * @return DSR卖家服务评价综合评分
    */
        public function getDsrCommunicateScore() {
        return $this->dsrCommunicateScore;
    }
    
    /**
     * 设置DSR卖家服务评价综合评分     
     * @param String $dsrCommunicateScore     
     * 参数示例：<pre>4.55</pre>     
     * 此参数必填     */
    public function setDsrCommunicateScore( $dsrCommunicateScore) {
        $this->dsrCommunicateScore = $dsrCommunicateScore;
    }
    
        	
    private $dsrLogisScoreAftDisclaim;
    
        /**
    * @return DSR物流服务评价综合评分（免责后）
    */
        public function getDsrLogisScoreAftDisclaim() {
        return $this->dsrLogisScoreAftDisclaim;
    }
    
    /**
     * 设置DSR物流服务评价综合评分（免责后）     
     * @param String $dsrLogisScoreAftDisclaim     
     * 参数示例：<pre>4.7</pre>     
     * 此参数必填     */
    public function setDsrLogisScoreAftDisclaim( $dsrLogisScoreAftDisclaim) {
        $this->dsrLogisScoreAftDisclaim = $dsrLogisScoreAftDisclaim;
    }
    
    	
	private $stdResult;
	
	public function setStdResult($stdResult) {
		$this->stdResult = $stdResult;
					    			    			if (array_key_exists ( "pcateId", $this->stdResult )) {
    				$this->pcateId = $this->stdResult->{"pcateId"};
    			}
    			    		    				    			    			if (array_key_exists ( "pcateFlag", $this->stdResult )) {
    				$this->pcateFlag = $this->stdResult->{"pcateFlag"};
    			}
    			    		    				    			    			if (array_key_exists ( "buyNotSelRate", $this->stdResult )) {
    				$this->buyNotSelRate = $this->stdResult->{"buyNotSelRate"};
    			}
    			    		    				    			    			if (array_key_exists ( "nrDisclaimerIssueRate", $this->stdResult )) {
    				$this->nrDisclaimerIssueRate = $this->stdResult->{"nrDisclaimerIssueRate"};
    			}
    			    		    				    			    			if (array_key_exists ( "snadDisclaimerIssueRate", $this->stdResult )) {
    				$this->snadDisclaimerIssueRate = $this->stdResult->{"snadDisclaimerIssueRate"};
    			}
    			    		    				    			    			if (array_key_exists ( "dsrProdScore", $this->stdResult )) {
    				$this->dsrProdScore = $this->stdResult->{"dsrProdScore"};
    			}
    			    		    				    			    			if (array_key_exists ( "dsrCommunicateScore", $this->stdResult )) {
    				$this->dsrCommunicateScore = $this->stdResult->{"dsrCommunicateScore"};
    			}
    			    		    				    			    			if (array_key_exists ( "dsrLogisScoreAftDisclaim", $this->stdResult )) {
    				$this->dsrLogisScoreAftDisclaim = $this->stdResult->{"dsrLogisScoreAftDisclaim"};
    			}
    			    		    		}
	
	private $arrayResult;
	public function setArrayResult($arrayResult) {
		$this->arrayResult = $arrayResult;
				    		    			if (array_key_exists ( "pcateId", $this->arrayResult )) {
    			$this->pcateId = $arrayResult['pcateId'];
    			}
    		    	    			    		    			if (array_key_exists ( "pcateFlag", $this->arrayResult )) {
    			$this->pcateFlag = $arrayResult['pcateFlag'];
    			}
    		    	    			    		    			if (array_key_exists ( "buyNotSelRate", $this->arrayResult )) {
    			$this->buyNotSelRate = $arrayResult['buyNotSelRate'];
    			}
    		    	    			    		    			if (array_key_exists ( "nrDisclaimerIssueRate", $this->arrayResult )) {
    			$this->nrDisclaimerIssueRate = $arrayResult['nrDisclaimerIssueRate'];
    			}
    		    	    			    		    			if (array_key_exists ( "snadDisclaimerIssueRate", $this->arrayResult )) {
    			$this->snadDisclaimerIssueRate = $arrayResult['snadDisclaimerIssueRate'];
    			}
    		    	    			    		    			if (array_key_exists ( "dsrProdScore", $this->arrayResult )) {
    			$this->dsrProdScore = $arrayResult['dsrProdScore'];
    			}
    		    	    			    		    			if (array_key_exists ( "dsrCommunicateScore", $this->arrayResult )) {
    			$this->dsrCommunicateScore = $arrayResult['dsrCommunicateScore'];
    			}
    		    	    			    		    			if (array_key_exists ( "dsrLogisScoreAftDisclaim", $this->arrayResult )) {
    			$this->dsrLogisScoreAftDisclaim = $arrayResult['dsrLogisScoreAftDisclaim'];
    			}
    		    	    		}
 
   
}
?>