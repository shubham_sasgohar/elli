<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');
include_once ('aliexpress/open/param/AeopAeProductSKUs.class.php');

class AlibabaAeMarketingJoinLimitedDiscountPromotionParam {

        
        /**
    * @return ae promotion id
    */
        public function getPromotionId() {
        $tempResult = $this->sdkStdResult["promotionId"];
        return $tempResult;
    }
    
    /**
     * 设置ae promotion id     
     * @param Long $promotionId     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setPromotionId( $promotionId) {
        $this->sdkStdResult["promotionId"] = $promotionId;
    }
    
        
        /**
    * @return product id
    */
        public function getProductId() {
        $tempResult = $this->sdkStdResult["productId"];
        return $tempResult;
    }
    
    /**
     * 设置product id     
     * @param Long $productId     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setProductId( $productId) {
        $this->sdkStdResult["productId"] = $productId;
    }
    
        
        /**
    * @return purchase limitatin for per buyer
    */
        public function getPurchaseNumber() {
        $tempResult = $this->sdkStdResult["purchaseNumber"];
        return $tempResult;
    }
    
    /**
     * 设置purchase limitatin for per buyer     
     * @param Integer $purchaseNumber     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setPurchaseNumber( $purchaseNumber) {
        $this->sdkStdResult["purchaseNumber"] = $purchaseNumber;
    }
    
        
        /**
    * @return product discount percentage
    */
        public function getDiscount() {
        $tempResult = $this->sdkStdResult["discount"];
        return $tempResult;
    }
    
    /**
     * 设置product discount percentage     
     * @param Integer $discount     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setDiscount( $discount) {
        $this->sdkStdResult["discount"] = $discount;
    }
    
        
        /**
    * @return product discount for mobile only
    */
        public function getMobileDiscount() {
        $tempResult = $this->sdkStdResult["mobileDiscount"];
        return $tempResult;
    }
    
    /**
     * 设置product discount for mobile only     
     * @param Integer $mobileDiscount     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setMobileDiscount( $mobileDiscount) {
        $this->sdkStdResult["mobileDiscount"] = $mobileDiscount;
    }
    
        
        /**
    * @return Type of list, expressed in json format. Refer to aeopAeProductSKUs data structure.Special tip: For new SKU actual merchantable stock attribute ipmSkuStock, its reasonable value range is 0~999999. If the product has SKU, please ensure that at least one SKU is in stock, which means the value of ipmSkuStock is within 1~999999. Value range of the whole product latitude stock is 1~999999.
    */
        public function getAeopAeProductSKUs() {
        $tempResult = $this->sdkStdResult["aeopAeProductSKUs"];
        return $tempResult;
    }
    
    /**
     * 设置Type of list, expressed in json format. Refer to aeopAeProductSKUs data structure.Special tip: For new SKU actual merchantable stock attribute ipmSkuStock, its reasonable value range is 0~999999. If the product has SKU, please ensure that at least one SKU is in stock, which means the value of ipmSkuStock is within 1~999999. Value range of the whole product latitude stock is 1~999999.     
     * @param array include @see AeopAeProductSKUs[] $aeopAeProductSKUs     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setAeopAeProductSKUs(AeopAeProductSKUs $aeopAeProductSKUs) {
        $this->sdkStdResult["aeopAeProductSKUs"] = $aeopAeProductSKUs;
    }
    
        
    private $sdkStdResult=array();
    
    public function getSdkStdResult(){
    	return $this->sdkStdResult;
    }

}
?>