<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');
include_once ('aliexpress/open/param/AlibabaAeSellerServicescoreAeServiceScoreDTO.class.php');
include_once ('aliexpress/open/param/AlibabaAeSellerServicescoreAeServiceIndexDTO.class.php');
include_once ('aliexpress/open/param/AlibabaAeSellerServicescoreAeIndustryAvgServiceScoreDTO.class.php');
include_once ('aliexpress/open/param/AlibabaAeSellerServicescoreAeIndustryAvgServiceIndexDTO.class.php');

class AlibabaAeSellerServicescoreAeProductServiceScoreIndexSumDTO extends SDKDomain {

       	
    private $itemId;
    
        /**
    * @return 商品ID
    */
        public function getItemId() {
        return $this->itemId;
    }
    
    /**
     * 设置商品ID     
     * @param Long $itemId     
     * 参数示例：<pre>32792839700</pre>     
     * 此参数必填     */
    public function setItemId( $itemId) {
        $this->itemId = $itemId;
    }
    
        	
    private $pcateId;
    
        /**
    * @return 商品所在类目ID
    */
        public function getPcateId() {
        return $this->pcateId;
    }
    
    /**
     * 设置商品所在类目ID     
     * @param Long $pcateId     
     * 参数示例：<pre>301</pre>     
     * 此参数必填     */
    public function setPcateId( $pcateId) {
        $this->pcateId = $pcateId;
    }
    
        	
    private $pcateName;
    
        /**
    * @return 商品所在类目名称
    */
        public function getPcateName() {
        return $this->pcateName;
    }
    
    /**
     * 设置商品所在类目名称     
     * @param String $pcateName     
     * 参数示例：<pre>运动服及配件</pre>     
     * 此参数必填     */
    public function setPcateName( $pcateName) {
        $this->pcateName = $pcateName;
    }
    
        	
    private $pcateFlag;
    
        /**
    * @return 商品所在类目层级
    */
        public function getPcateFlag() {
        return $this->pcateFlag;
    }
    
    /**
     * 设置商品所在类目层级     
     * @param Long $pcateFlag     
     * 参数示例：<pre>2,3</pre>     
     * 此参数必填     */
    public function setPcateFlag( $pcateFlag) {
        $this->pcateFlag = $pcateFlag;
    }
    
        	
    private $checkMordCnt;
    
        /**
    * @return 考核父订单数
    */
        public function getCheckMordCnt() {
        return $this->checkMordCnt;
    }
    
    /**
     * 设置考核父订单数     
     * @param Long $checkMordCnt     
     * 参数示例：<pre>902</pre>     
     * 此参数必填     */
    public function setCheckMordCnt( $checkMordCnt) {
        $this->checkMordCnt = $checkMordCnt;
    }
    
        	
    private $scoreDTO;
    
        /**
    * @return 服务得分信息
    */
        public function getScoreDTO() {
        return $this->scoreDTO;
    }
    
    /**
     * 设置服务得分信息     
     * @param AlibabaAeSellerServicescoreAeServiceScoreDTO $scoreDTO     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setScoreDTO(AlibabaAeSellerServicescoreAeServiceScoreDTO $scoreDTO) {
        $this->scoreDTO = $scoreDTO;
    }
    
        	
    private $indexDTO;
    
        /**
    * @return 服务指标信息
    */
        public function getIndexDTO() {
        return $this->indexDTO;
    }
    
    /**
     * 设置服务指标信息     
     * @param AlibabaAeSellerServicescoreAeServiceIndexDTO $indexDTO     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setIndexDTO(AlibabaAeSellerServicescoreAeServiceIndexDTO $indexDTO) {
        $this->indexDTO = $indexDTO;
    }
    
        	
    private $industryAvgScoreDTO;
    
        /**
    * @return 所在行业平均得分信息
    */
        public function getIndustryAvgScoreDTO() {
        return $this->industryAvgScoreDTO;
    }
    
    /**
     * 设置所在行业平均得分信息     
     * @param AlibabaAeSellerServicescoreAeIndustryAvgServiceScoreDTO $industryAvgScoreDTO     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setIndustryAvgScoreDTO(AlibabaAeSellerServicescoreAeIndustryAvgServiceScoreDTO $industryAvgScoreDTO) {
        $this->industryAvgScoreDTO = $industryAvgScoreDTO;
    }
    
        	
    private $industryAvgIndexDTO;
    
        /**
    * @return 所在行业平均指标信息
    */
        public function getIndustryAvgIndexDTO() {
        return $this->industryAvgIndexDTO;
    }
    
    /**
     * 设置所在行业平均指标信息     
     * @param AlibabaAeSellerServicescoreAeIndustryAvgServiceIndexDTO $industryAvgIndexDTO     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setIndustryAvgIndexDTO(AlibabaAeSellerServicescoreAeIndustryAvgServiceIndexDTO $industryAvgIndexDTO) {
        $this->industryAvgIndexDTO = $industryAvgIndexDTO;
    }
    
    	
	private $stdResult;
	
	public function setStdResult($stdResult) {
		$this->stdResult = $stdResult;
					    			    			if (array_key_exists ( "itemId", $this->stdResult )) {
    				$this->itemId = $this->stdResult->{"itemId"};
    			}
    			    		    				    			    			if (array_key_exists ( "pcateId", $this->stdResult )) {
    				$this->pcateId = $this->stdResult->{"pcateId"};
    			}
    			    		    				    			    			if (array_key_exists ( "pcateName", $this->stdResult )) {
    				$this->pcateName = $this->stdResult->{"pcateName"};
    			}
    			    		    				    			    			if (array_key_exists ( "pcateFlag", $this->stdResult )) {
    				$this->pcateFlag = $this->stdResult->{"pcateFlag"};
    			}
    			    		    				    			    			if (array_key_exists ( "checkMordCnt", $this->stdResult )) {
    				$this->checkMordCnt = $this->stdResult->{"checkMordCnt"};
    			}
    			    		    				    			    			if (array_key_exists ( "scoreDTO", $this->stdResult )) {
    				$scoreDTOResult=$this->stdResult->{"scoreDTO"};
    				$this->scoreDTO = new AlibabaAeSellerServicescoreAeServiceScoreDTO();
    				$this->scoreDTO->setStdResult ( $scoreDTOResult);
    			}
    			    		    				    			    			if (array_key_exists ( "indexDTO", $this->stdResult )) {
    				$indexDTOResult=$this->stdResult->{"indexDTO"};
    				$this->indexDTO = new AlibabaAeSellerServicescoreAeServiceIndexDTO();
    				$this->indexDTO->setStdResult ( $indexDTOResult);
    			}
    			    		    				    			    			if (array_key_exists ( "industryAvgScoreDTO", $this->stdResult )) {
    				$industryAvgScoreDTOResult=$this->stdResult->{"industryAvgScoreDTO"};
    				$this->industryAvgScoreDTO = new AlibabaAeSellerServicescoreAeIndustryAvgServiceScoreDTO();
    				$this->industryAvgScoreDTO->setStdResult ( $industryAvgScoreDTOResult);
    			}
    			    		    				    			    			if (array_key_exists ( "industryAvgIndexDTO", $this->stdResult )) {
    				$industryAvgIndexDTOResult=$this->stdResult->{"industryAvgIndexDTO"};
    				$this->industryAvgIndexDTO = new AlibabaAeSellerServicescoreAeIndustryAvgServiceIndexDTO();
    				$this->industryAvgIndexDTO->setStdResult ( $industryAvgIndexDTOResult);
    			}
    			    		    		}
	
	private $arrayResult;
	public function setArrayResult($arrayResult) {
		$this->arrayResult = $arrayResult;
				    		    			if (array_key_exists ( "itemId", $this->arrayResult )) {
    			$this->itemId = $arrayResult['itemId'];
    			}
    		    	    			    		    			if (array_key_exists ( "pcateId", $this->arrayResult )) {
    			$this->pcateId = $arrayResult['pcateId'];
    			}
    		    	    			    		    			if (array_key_exists ( "pcateName", $this->arrayResult )) {
    			$this->pcateName = $arrayResult['pcateName'];
    			}
    		    	    			    		    			if (array_key_exists ( "pcateFlag", $this->arrayResult )) {
    			$this->pcateFlag = $arrayResult['pcateFlag'];
    			}
    		    	    			    		    			if (array_key_exists ( "checkMordCnt", $this->arrayResult )) {
    			$this->checkMordCnt = $arrayResult['checkMordCnt'];
    			}
    		    	    			    		    		if (array_key_exists ( "scoreDTO", $this->arrayResult )) {
    		$scoreDTOResult=$arrayResult['scoreDTO'];
    			    			$this->scoreDTO = new AlibabaAeSellerServicescoreAeServiceScoreDTO();
    			    			$this->scoreDTO->$this->setStdResult ( $scoreDTOResult);
    		}
    		    	    			    		    		if (array_key_exists ( "indexDTO", $this->arrayResult )) {
    		$indexDTOResult=$arrayResult['indexDTO'];
    			    			$this->indexDTO = new AlibabaAeSellerServicescoreAeServiceIndexDTO();
    			    			$this->indexDTO->$this->setStdResult ( $indexDTOResult);
    		}
    		    	    			    		    		if (array_key_exists ( "industryAvgScoreDTO", $this->arrayResult )) {
    		$industryAvgScoreDTOResult=$arrayResult['industryAvgScoreDTO'];
    			    			$this->industryAvgScoreDTO = new AlibabaAeSellerServicescoreAeIndustryAvgServiceScoreDTO();
    			    			$this->industryAvgScoreDTO->$this->setStdResult ( $industryAvgScoreDTOResult);
    		}
    		    	    			    		    		if (array_key_exists ( "industryAvgIndexDTO", $this->arrayResult )) {
    		$industryAvgIndexDTOResult=$arrayResult['industryAvgIndexDTO'];
    			    			$this->industryAvgIndexDTO = new AlibabaAeSellerServicescoreAeIndustryAvgServiceIndexDTO();
    			    			$this->industryAvgIndexDTO->$this->setStdResult ( $industryAvgIndexDTOResult);
    		}
    		    	    		}
 
   
}
?>