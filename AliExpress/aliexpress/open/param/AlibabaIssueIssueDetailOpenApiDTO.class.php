<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');
include_once ('aliexpress/open/param/IssueMoney.class.php');
include_once ('aliexpress/open/param/IssueMoney.class.php');
include_once ('aliexpress/open/param/IssueMoney.class.php');
include_once ('aliexpress/open/param/AlibabaIssueSolutionDTO.class.php');
include_once ('aliexpress/open/param/AlibabaIssueSolutionDTO.class.php');
include_once ('aliexpress/open/param/AlibabaIssueSolutionDTO.class.php');

class AlibabaIssueIssueDetailOpenApiDTO extends SDKDomain {

       	
    private $id;
    
        /**
    * @return 纠纷ID
    */
        public function getId() {
        return $this->id;
    }
    
    /**
     * 设置纠纷ID     
     * @param Long $id     
     * 参数示例：<pre>4201234566655</pre>     
     * 此参数必填     */
    public function setId( $id) {
        $this->id = $id;
    }
    
        	
    private $gmtCreate;
    
        /**
    * @return 纠纷创建时间
    */
        public function getGmtCreate() {
        return $this->gmtCreate;
    }
    
    /**
     * 设置纠纷创建时间     
     * @param Date $gmtCreate     
     * 参数示例：<pre>2016-08-31 23:23:23</pre>     
     * 此参数必填     */
    public function setGmtCreate( $gmtCreate) {
        $this->gmtCreate = $gmtCreate;
    }
    
        	
    private $orderId;
    
        /**
    * @return 子订单号
    */
        public function getOrderId() {
        return $this->orderId;
    }
    
    /**
     * 设置子订单号     
     * @param Long $orderId     
     * 参数示例：<pre>7647012345678</pre>     
     * 此参数必填     */
    public function setOrderId( $orderId) {
        $this->orderId = $orderId;
    }
    
        	
    private $parentOrderId;
    
        /**
    * @return 主订单号
    */
        public function getParentOrderId() {
        return $this->parentOrderId;
    }
    
    /**
     * 设置主订单号     
     * @param Long $parentOrderId     
     * 参数示例：<pre>7647012345678</pre>     
     * 此参数必填     */
    public function setParentOrderId( $parentOrderId) {
        $this->parentOrderId = $parentOrderId;
    }
    
        	
    private $buyerAliid;
    
        /**
    * @return 买家memberID
    */
        public function getBuyerAliid() {
        return $this->buyerAliid;
    }
    
    /**
     * 设置买家memberID     
     * @param Long $buyerAliid     
     * 参数示例：<pre>12345678</pre>     
     * 此参数必填     */
    public function setBuyerAliid( $buyerAliid) {
        $this->buyerAliid = $buyerAliid;
    }
    
        	
    private $issueReasonId;
    
        /**
    * @return 纠纷原因ID
    */
        public function getIssueReasonId() {
        return $this->issueReasonId;
    }
    
    /**
     * 设置纠纷原因ID     
     * @param Long $issueReasonId     
     * 参数示例：<pre>107</pre>     
     * 此参数必填     */
    public function setIssueReasonId( $issueReasonId) {
        $this->issueReasonId = $issueReasonId;
    }
    
        	
    private $issueReason;
    
        /**
    * @return 纠纷原因
    */
        public function getIssueReason() {
        return $this->issueReason;
    }
    
    /**
     * 设置纠纷原因     
     * @param String $issueReason     
     * 参数示例：<pre>尺寸不符</pre>     
     * 此参数必填     */
    public function setIssueReason( $issueReason) {
        $this->issueReason = $issueReason;
    }
    
        	
    private $issueStatus;
    
        /**
    * @return 纠纷状态
    */
        public function getIssueStatus() {
        return $this->issueStatus;
    }
    
    /**
     * 设置纠纷状态     
     * @param String $issueStatus     
     * 参数示例：<pre>buyer_initiate_arbitration</pre>     
     * 此参数必填     */
    public function setIssueStatus( $issueStatus) {
        $this->issueStatus = $issueStatus;
    }
    
        	
    private $refundMoneyMax;
    
        /**
    * @return 退款上限
    */
        public function getRefundMoneyMax() {
        return $this->refundMoneyMax;
    }
    
    /**
     * 设置退款上限     
     * @param IssueMoney $refundMoneyMax     
     * 参数示例：<pre>33.33RUB</pre>     
     * 此参数必填     */
    public function setRefundMoneyMax(IssueMoney $refundMoneyMax) {
        $this->refundMoneyMax = $refundMoneyMax;
    }
    
        	
    private $refundMoneyMaxLocal;
    
        /**
    * @return 退款上限本币
    */
        public function getRefundMoneyMaxLocal() {
        return $this->refundMoneyMaxLocal;
    }
    
    /**
     * 设置退款上限本币     
     * @param IssueMoney $refundMoneyMaxLocal     
     * 参数示例：<pre>10.22USD</pre>     
     * 此参数必填     */
    public function setRefundMoneyMaxLocal(IssueMoney $refundMoneyMaxLocal) {
        $this->refundMoneyMaxLocal = $refundMoneyMaxLocal;
    }
    
        	
    private $productName;
    
        /**
    * @return 产品名称
    */
        public function getProductName() {
        return $this->productName;
    }
    
    /**
     * 设置产品名称     
     * @param String $productName     
     * 参数示例：<pre>MP3</pre>     
     * 此参数必填     */
    public function setProductName( $productName) {
        $this->productName = $productName;
    }
    
        	
    private $productPrice;
    
        /**
    * @return 产品价格
    */
        public function getProductPrice() {
        return $this->productPrice;
    }
    
    /**
     * 设置产品价格     
     * @param IssueMoney $productPrice     
     * 参数示例：<pre>10USD</pre>     
     * 此参数必填     */
    public function setProductPrice(IssueMoney $productPrice) {
        $this->productPrice = $productPrice;
    }
    
        	
    private $buyerReturnLogisticsCompany;
    
        /**
    * @return 买家退货物流公司
    */
        public function getBuyerReturnLogisticsCompany() {
        return $this->buyerReturnLogisticsCompany;
    }
    
    /**
     * 设置买家退货物流公司     
     * @param String $buyerReturnLogisticsCompany     
     * 参数示例：<pre>EMS</pre>     
     * 此参数必填     */
    public function setBuyerReturnLogisticsCompany( $buyerReturnLogisticsCompany) {
        $this->buyerReturnLogisticsCompany = $buyerReturnLogisticsCompany;
    }
    
        	
    private $buyerReturnNo;
    
        /**
    * @return 买家退货单号
    */
        public function getBuyerReturnNo() {
        return $this->buyerReturnNo;
    }
    
    /**
     * 设置买家退货单号     
     * @param String $buyerReturnNo     
     * 参数示例：<pre>12345678</pre>     
     * 此参数必填     */
    public function setBuyerReturnNo( $buyerReturnNo) {
        $this->buyerReturnNo = $buyerReturnNo;
    }
    
        	
    private $buyerReturnLogisticsLpNo;
    
        /**
    * @return 退货物流订单LP单号
    */
        public function getBuyerReturnLogisticsLpNo() {
        return $this->buyerReturnLogisticsLpNo;
    }
    
    /**
     * 设置退货物流订单LP单号     
     * @param String $buyerReturnLogisticsLpNo     
     * 参数示例：<pre>LP12345678</pre>     
     * 此参数必填     */
    public function setBuyerReturnLogisticsLpNo( $buyerReturnLogisticsLpNo) {
        $this->buyerReturnLogisticsLpNo = $buyerReturnLogisticsLpNo;
    }
    
        	
    private $buyerSolutionList;
    
        /**
    * @return 买家协商方案
    */
        public function getBuyerSolutionList() {
        return $this->buyerSolutionList;
    }
    
    /**
     * 设置买家协商方案     
     * @param array include @see AlibabaIssueSolutionDTO[] $buyerSolutionList     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setBuyerSolutionList(AlibabaIssueSolutionDTO $buyerSolutionList) {
        $this->buyerSolutionList = $buyerSolutionList;
    }
    
        	
    private $sellerSolutionList;
    
        /**
    * @return 卖家协商方案
    */
        public function getSellerSolutionList() {
        return $this->sellerSolutionList;
    }
    
    /**
     * 设置卖家协商方案     
     * @param array include @see AlibabaIssueSolutionDTO[] $sellerSolutionList     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setSellerSolutionList(AlibabaIssueSolutionDTO $sellerSolutionList) {
        $this->sellerSolutionList = $sellerSolutionList;
    }
    
        	
    private $platformSolutionList;
    
        /**
    * @return 平台协商方案
    */
        public function getPlatformSolutionList() {
        return $this->platformSolutionList;
    }
    
    /**
     * 设置平台协商方案     
     * @param array include @see AlibabaIssueSolutionDTO[] $platformSolutionList     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setPlatformSolutionList(AlibabaIssueSolutionDTO $platformSolutionList) {
        $this->platformSolutionList = $platformSolutionList;
    }
    
    	
	private $stdResult;
	
	public function setStdResult($stdResult) {
		$this->stdResult = $stdResult;
					    			    			if (array_key_exists ( "id", $this->stdResult )) {
    				$this->id = $this->stdResult->{"id"};
    			}
    			    		    				    			    			if (array_key_exists ( "gmtCreate", $this->stdResult )) {
    				$this->gmtCreate = $this->stdResult->{"gmtCreate"};
    			}
    			    		    				    			    			if (array_key_exists ( "orderId", $this->stdResult )) {
    				$this->orderId = $this->stdResult->{"orderId"};
    			}
    			    		    				    			    			if (array_key_exists ( "parentOrderId", $this->stdResult )) {
    				$this->parentOrderId = $this->stdResult->{"parentOrderId"};
    			}
    			    		    				    			    			if (array_key_exists ( "buyerAliid", $this->stdResult )) {
    				$this->buyerAliid = $this->stdResult->{"buyerAliid"};
    			}
    			    		    				    			    			if (array_key_exists ( "issueReasonId", $this->stdResult )) {
    				$this->issueReasonId = $this->stdResult->{"issueReasonId"};
    			}
    			    		    				    			    			if (array_key_exists ( "issueReason", $this->stdResult )) {
    				$this->issueReason = $this->stdResult->{"issueReason"};
    			}
    			    		    				    			    			if (array_key_exists ( "issueStatus", $this->stdResult )) {
    				$this->issueStatus = $this->stdResult->{"issueStatus"};
    			}
    			    		    				    			    			if (array_key_exists ( "refundMoneyMax", $this->stdResult )) {
    				$refundMoneyMaxResult=$this->stdResult->{"refundMoneyMax"};
    				$this->refundMoneyMax = new IssueMoney();
    				$this->refundMoneyMax->setStdResult ( $refundMoneyMaxResult);
    			}
    			    		    				    			    			if (array_key_exists ( "refundMoneyMaxLocal", $this->stdResult )) {
    				$refundMoneyMaxLocalResult=$this->stdResult->{"refundMoneyMaxLocal"};
    				$this->refundMoneyMaxLocal = new IssueMoney();
    				$this->refundMoneyMaxLocal->setStdResult ( $refundMoneyMaxLocalResult);
    			}
    			    		    				    			    			if (array_key_exists ( "productName", $this->stdResult )) {
    				$this->productName = $this->stdResult->{"productName"};
    			}
    			    		    				    			    			if (array_key_exists ( "productPrice", $this->stdResult )) {
    				$productPriceResult=$this->stdResult->{"productPrice"};
    				$this->productPrice = new IssueMoney();
    				$this->productPrice->setStdResult ( $productPriceResult);
    			}
    			    		    				    			    			if (array_key_exists ( "buyerReturnLogisticsCompany", $this->stdResult )) {
    				$this->buyerReturnLogisticsCompany = $this->stdResult->{"buyerReturnLogisticsCompany"};
    			}
    			    		    				    			    			if (array_key_exists ( "buyerReturnNo", $this->stdResult )) {
    				$this->buyerReturnNo = $this->stdResult->{"buyerReturnNo"};
    			}
    			    		    				    			    			if (array_key_exists ( "buyerReturnLogisticsLpNo", $this->stdResult )) {
    				$this->buyerReturnLogisticsLpNo = $this->stdResult->{"buyerReturnLogisticsLpNo"};
    			}
    			    		    				    			    			if (array_key_exists ( "buyerSolutionList", $this->stdResult )) {
    			$buyerSolutionListResult=$this->stdResult->{"buyerSolutionList"};
    				$object = json_decode ( json_encode ( $buyerSolutionListResult ), true );
					$this->buyerSolutionList = array ();
					for($i = 0; $i < count ( $object ); $i ++) {
						$arrayobject = new ArrayObject ( $object [$i] );
						$AlibabaIssueSolutionDTOResult=new AlibabaIssueSolutionDTO();
						$AlibabaIssueSolutionDTOResult->setArrayResult($arrayobject );
						$this->buyerSolutionList [$i] = $AlibabaIssueSolutionDTOResult;
					}
    			}
    			    		    				    			    			if (array_key_exists ( "sellerSolutionList", $this->stdResult )) {
    			$sellerSolutionListResult=$this->stdResult->{"sellerSolutionList"};
    				$object = json_decode ( json_encode ( $sellerSolutionListResult ), true );
					$this->sellerSolutionList = array ();
					for($i = 0; $i < count ( $object ); $i ++) {
						$arrayobject = new ArrayObject ( $object [$i] );
						$AlibabaIssueSolutionDTOResult=new AlibabaIssueSolutionDTO();
						$AlibabaIssueSolutionDTOResult->setArrayResult($arrayobject );
						$this->sellerSolutionList [$i] = $AlibabaIssueSolutionDTOResult;
					}
    			}
    			    		    				    			    			if (array_key_exists ( "platformSolutionList", $this->stdResult )) {
    			$platformSolutionListResult=$this->stdResult->{"platformSolutionList"};
    				$object = json_decode ( json_encode ( $platformSolutionListResult ), true );
					$this->platformSolutionList = array ();
					for($i = 0; $i < count ( $object ); $i ++) {
						$arrayobject = new ArrayObject ( $object [$i] );
						$AlibabaIssueSolutionDTOResult=new AlibabaIssueSolutionDTO();
						$AlibabaIssueSolutionDTOResult->setArrayResult($arrayobject );
						$this->platformSolutionList [$i] = $AlibabaIssueSolutionDTOResult;
					}
    			}
    			    		    		}
	
	private $arrayResult;
	public function setArrayResult($arrayResult) {
		$this->arrayResult = $arrayResult;
				    		    			if (array_key_exists ( "id", $this->arrayResult )) {
    			$this->id = $arrayResult['id'];
    			}
    		    	    			    		    			if (array_key_exists ( "gmtCreate", $this->arrayResult )) {
    			$this->gmtCreate = $arrayResult['gmtCreate'];
    			}
    		    	    			    		    			if (array_key_exists ( "orderId", $this->arrayResult )) {
    			$this->orderId = $arrayResult['orderId'];
    			}
    		    	    			    		    			if (array_key_exists ( "parentOrderId", $this->arrayResult )) {
    			$this->parentOrderId = $arrayResult['parentOrderId'];
    			}
    		    	    			    		    			if (array_key_exists ( "buyerAliid", $this->arrayResult )) {
    			$this->buyerAliid = $arrayResult['buyerAliid'];
    			}
    		    	    			    		    			if (array_key_exists ( "issueReasonId", $this->arrayResult )) {
    			$this->issueReasonId = $arrayResult['issueReasonId'];
    			}
    		    	    			    		    			if (array_key_exists ( "issueReason", $this->arrayResult )) {
    			$this->issueReason = $arrayResult['issueReason'];
    			}
    		    	    			    		    			if (array_key_exists ( "issueStatus", $this->arrayResult )) {
    			$this->issueStatus = $arrayResult['issueStatus'];
    			}
    		    	    			    		    		if (array_key_exists ( "refundMoneyMax", $this->arrayResult )) {
    		$refundMoneyMaxResult=$arrayResult['refundMoneyMax'];
    			    			$this->refundMoneyMax = new IssueMoney();
    			    			$this->refundMoneyMax->$this->setStdResult ( $refundMoneyMaxResult);
    		}
    		    	    			    		    		if (array_key_exists ( "refundMoneyMaxLocal", $this->arrayResult )) {
    		$refundMoneyMaxLocalResult=$arrayResult['refundMoneyMaxLocal'];
    			    			$this->refundMoneyMaxLocal = new IssueMoney();
    			    			$this->refundMoneyMaxLocal->$this->setStdResult ( $refundMoneyMaxLocalResult);
    		}
    		    	    			    		    			if (array_key_exists ( "productName", $this->arrayResult )) {
    			$this->productName = $arrayResult['productName'];
    			}
    		    	    			    		    		if (array_key_exists ( "productPrice", $this->arrayResult )) {
    		$productPriceResult=$arrayResult['productPrice'];
    			    			$this->productPrice = new IssueMoney();
    			    			$this->productPrice->$this->setStdResult ( $productPriceResult);
    		}
    		    	    			    		    			if (array_key_exists ( "buyerReturnLogisticsCompany", $this->arrayResult )) {
    			$this->buyerReturnLogisticsCompany = $arrayResult['buyerReturnLogisticsCompany'];
    			}
    		    	    			    		    			if (array_key_exists ( "buyerReturnNo", $this->arrayResult )) {
    			$this->buyerReturnNo = $arrayResult['buyerReturnNo'];
    			}
    		    	    			    		    			if (array_key_exists ( "buyerReturnLogisticsLpNo", $this->arrayResult )) {
    			$this->buyerReturnLogisticsLpNo = $arrayResult['buyerReturnLogisticsLpNo'];
    			}
    		    	    			    		    		if (array_key_exists ( "buyerSolutionList", $this->arrayResult )) {
    		$buyerSolutionListResult=$arrayResult['buyerSolutionList'];
    			$this->buyerSolutionList = AlibabaIssueSolutionDTO();
    			$this->buyerSolutionList->$this->setStdResult ( $buyerSolutionListResult);
    		}
    		    	    			    		    		if (array_key_exists ( "sellerSolutionList", $this->arrayResult )) {
    		$sellerSolutionListResult=$arrayResult['sellerSolutionList'];
    			$this->sellerSolutionList = AlibabaIssueSolutionDTO();
    			$this->sellerSolutionList->$this->setStdResult ( $sellerSolutionListResult);
    		}
    		    	    			    		    		if (array_key_exists ( "platformSolutionList", $this->arrayResult )) {
    		$platformSolutionListResult=$arrayResult['platformSolutionList'];
    			$this->platformSolutionList = AlibabaIssueSolutionDTO();
    			$this->platformSolutionList->$this->setStdResult ( $platformSolutionListResult);
    		}
    		    	    		}
 
   
}
?>