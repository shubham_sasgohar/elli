<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');
include_once ('aliexpress/open/param/AlibabaAeLogisticsAeopShipmentDTO.class.php');

class AlibabaAeLogisticsAeopSellerShipmentSubTradeOrderResponse extends SDKDomain {

       	
    private $subTradeOrderIndex;
    
        /**
    * @return 子订单序号，从1开始
    */
        public function getSubTradeOrderIndex() {
        return $this->subTradeOrderIndex;
    }
    
    /**
     * 设置子订单序号，从1开始     
     * @param Integer $subTradeOrderIndex     
     * 参数示例：<pre>1</pre>     
     * 此参数必填     */
    public function setSubTradeOrderIndex( $subTradeOrderIndex) {
        $this->subTradeOrderIndex = $subTradeOrderIndex;
    }
    
        	
    private $sendType;
    
        /**
    * @return 声明发货类型：part(部分发货)，all(全部发货)
    */
        public function getSendType() {
        return $this->sendType;
    }
    
    /**
     * 设置声明发货类型：part(部分发货)，all(全部发货)     
     * @param String $sendType     
     * 参数示例：<pre>part/all</pre>     
     * 此参数必填     */
    public function setSendType( $sendType) {
        $this->sendType = $sendType;
    }
    
        	
    private $errorCode;
    
        /**
    * @return 错误码
    */
        public function getErrorCode() {
        return $this->errorCode;
    }
    
    /**
     * 设置错误码     
     * @param String $errorCode     
     * 参数示例：<pre>-100</pre>     
     * 此参数必填     */
    public function setErrorCode( $errorCode) {
        $this->errorCode = $errorCode;
    }
    
        	
    private $errorMsg;
    
        /**
    * @return 错误提示
    */
        public function getErrorMsg() {
        return $this->errorMsg;
    }
    
    /**
     * 设置错误提示     
     * @param String $errorMsg     
     * 参数示例：<pre>system error</pre>     
     * 此参数必填     */
    public function setErrorMsg( $errorMsg) {
        $this->errorMsg = $errorMsg;
    }
    
        	
    private $shipmentList;
    
        /**
    * @return 包裹声明发货结果
    */
        public function getShipmentList() {
        return $this->shipmentList;
    }
    
    /**
     * 设置包裹声明发货结果     
     * @param array include @see AlibabaAeLogisticsAeopShipmentDTO[] $shipmentList     
     * 参数示例：<pre>[
    {
        "success": true, 
        "errorMsg": "***", 
        "errorCode": -1, 
        "logisticsNo": "123456", 
        "serviceName": "CAINIAO_STANDARD", 
        "trackingWebSite": "www.baidu.com"
    }
]</pre>     
     * 此参数必填     */
    public function setShipmentList(AlibabaAeLogisticsAeopShipmentDTO $shipmentList) {
        $this->shipmentList = $shipmentList;
    }
    
    	
	private $stdResult;
	
	public function setStdResult($stdResult) {
		$this->stdResult = $stdResult;
					    			    			if (array_key_exists ( "subTradeOrderIndex", $this->stdResult )) {
    				$this->subTradeOrderIndex = $this->stdResult->{"subTradeOrderIndex"};
    			}
    			    		    				    			    			if (array_key_exists ( "sendType", $this->stdResult )) {
    				$this->sendType = $this->stdResult->{"sendType"};
    			}
    			    		    				    			    			if (array_key_exists ( "errorCode", $this->stdResult )) {
    				$this->errorCode = $this->stdResult->{"errorCode"};
    			}
    			    		    				    			    			if (array_key_exists ( "errorMsg", $this->stdResult )) {
    				$this->errorMsg = $this->stdResult->{"errorMsg"};
    			}
    			    		    				    			    			if (array_key_exists ( "shipmentList", $this->stdResult )) {
    			$shipmentListResult=$this->stdResult->{"shipmentList"};
    				$object = json_decode ( json_encode ( $shipmentListResult ), true );
					$this->shipmentList = array ();
					for($i = 0; $i < count ( $object ); $i ++) {
						$arrayobject = new ArrayObject ( $object [$i] );
						$AlibabaAeLogisticsAeopShipmentDTOResult=new AlibabaAeLogisticsAeopShipmentDTO();
						$AlibabaAeLogisticsAeopShipmentDTOResult->setArrayResult($arrayobject );
						$this->shipmentList [$i] = $AlibabaAeLogisticsAeopShipmentDTOResult;
					}
    			}
    			    		    		}
	
	private $arrayResult;
	public function setArrayResult($arrayResult) {
		$this->arrayResult = $arrayResult;
				    		    			if (array_key_exists ( "subTradeOrderIndex", $this->arrayResult )) {
    			$this->subTradeOrderIndex = $arrayResult['subTradeOrderIndex'];
    			}
    		    	    			    		    			if (array_key_exists ( "sendType", $this->arrayResult )) {
    			$this->sendType = $arrayResult['sendType'];
    			}
    		    	    			    		    			if (array_key_exists ( "errorCode", $this->arrayResult )) {
    			$this->errorCode = $arrayResult['errorCode'];
    			}
    		    	    			    		    			if (array_key_exists ( "errorMsg", $this->arrayResult )) {
    			$this->errorMsg = $arrayResult['errorMsg'];
    			}
    		    	    			    		    		if (array_key_exists ( "shipmentList", $this->arrayResult )) {
    		$shipmentListResult=$arrayResult['shipmentList'];
    			$this->shipmentList = AlibabaAeLogisticsAeopShipmentDTO();
    			$this->shipmentList->$this->setStdResult ( $shipmentListResult);
    		}
    		    	    		}
 
   
}
?>