<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');
include_once ('aliexpress/open/param/AlibabaAeLogisticsAeopSellerShipmentSubTradeOrderRequest.class.php');

class AlibabaAeLogisticsSellerShipmentSupportSubTradeOrderParam {

        
        /**
    * @return 交易订单号
    */
        public function getTradeOrderId() {
        $tempResult = $this->sdkStdResult["tradeOrderId"];
        return $tempResult;
    }
    
    /**
     * 设置交易订单号     
     * @param Long $tradeOrderId     
     * 参数示例：<pre>778824234234</pre>     
     * 此参数必填     */
    public function setTradeOrderId( $tradeOrderId) {
        $this->sdkStdResult["tradeOrderId"] = $tradeOrderId;
    }
    
        
        /**
    * @return 声明发货信息
    */
        public function getSubTradeOrderList() {
        $tempResult = $this->sdkStdResult["subTradeOrderList"];
        return $tempResult;
    }
    
    /**
     * 设置声明发货信息     
     * @param array include @see AlibabaAeLogisticsAeopSellerShipmentSubTradeOrderRequest[] $subTradeOrderList     
     * 参数示例：<pre>[
			{
				"subTradeOrderIndex":"1",
				"shipmentList":[
					{
						"newLogisticsNo":"123456",
						"newServiceName":"CAINIAO_STANDARD",
						"trackingWebSite":"www.baidu.com"
					}
				]
			}
		]</pre>     
     * 此参数必填     */
    public function setSubTradeOrderList(AlibabaAeLogisticsAeopSellerShipmentSubTradeOrderRequest $subTradeOrderList) {
        $this->sdkStdResult["subTradeOrderList"] = $subTradeOrderList;
    }
    
        
    private $sdkStdResult=array();
    
    public function getSdkStdResult(){
    	return $this->sdkStdResult;
    }

}
?>