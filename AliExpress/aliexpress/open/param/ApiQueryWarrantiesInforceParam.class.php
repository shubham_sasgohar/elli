<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');

class ApiQueryWarrantiesInforceParam {

        
        /**
    * @return 供应商id
    */
        public function getSupplierId() {
        $tempResult = $this->sdkStdResult["supplierId"];
        return $tempResult;
    }
    
    /**
     * 设置供应商id     
     * @param String $supplierId     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setSupplierId( $supplierId) {
        $this->sdkStdResult["supplierId"] = $supplierId;
    }
    
        
        /**
    * @return 开始时间
    */
        public function getStartTime() {
        $tempResult = $this->sdkStdResult["startTime"];
        return $tempResult;
    }
    
    /**
     * 设置开始时间     
     * @param String $startTime     
     * 参数示例：<pre>2016-01-06 00:00:00</pre>     
     * 此参数必填     */
    public function setStartTime( $startTime) {
        $this->sdkStdResult["startTime"] = $startTime;
    }
    
        
        /**
    * @return 结束时间
    */
        public function getEndTime() {
        $tempResult = $this->sdkStdResult["endTime"];
        return $tempResult;
    }
    
    /**
     * 设置结束时间     
     * @param String $endTime     
     * 参数示例：<pre>2016-01-06 00:00:00</pre>     
     * 此参数必填     */
    public function setEndTime( $endTime) {
        $this->sdkStdResult["endTime"] = $endTime;
    }
    
        
        /**
    * @return 页面大小(不得超过200)
    */
        public function getPageSize() {
        $tempResult = $this->sdkStdResult["pageSize"];
        return $tempResult;
    }
    
    /**
     * 设置页面大小(不得超过200)     
     * @param Integer $pageSize     
     * 参数示例：<pre>50</pre>     
     * 此参数必填     */
    public function setPageSize( $pageSize) {
        $this->sdkStdResult["pageSize"] = $pageSize;
    }
    
        
        /**
    * @return 显示的页码
    */
        public function getPageNo() {
        $tempResult = $this->sdkStdResult["pageNo"];
        return $tempResult;
    }
    
    /**
     * 设置显示的页码     
     * @param Integer $pageNo     
     * 参数示例：<pre>1</pre>     
     * 此参数必填     */
    public function setPageNo( $pageNo) {
        $this->sdkStdResult["pageNo"] = $pageNo;
    }
    
        
        /**
    * @return 查询买家最后一次申请保修开始时间
    */
        public function getLastBuyerApplyServiceQuyerStartTime() {
        $tempResult = $this->sdkStdResult["lastBuyerApplyServiceQuyerStartTime"];
        return $tempResult;
    }
    
    /**
     * 设置查询买家最后一次申请保修开始时间     
     * @param String $lastBuyerApplyServiceQuyerStartTime     
     * 参数示例：<pre>2016-01-06 00:00:00</pre>     
     * 此参数必填     */
    public function setLastBuyerApplyServiceQuyerStartTime( $lastBuyerApplyServiceQuyerStartTime) {
        $this->sdkStdResult["lastBuyerApplyServiceQuyerStartTime"] = $lastBuyerApplyServiceQuyerStartTime;
    }
    
        
        /**
    * @return 查询买家最后一次申请保修结束时间
    */
        public function getLastBuyerApplyServiceQuyerEndTime() {
        $tempResult = $this->sdkStdResult["lastBuyerApplyServiceQuyerEndTime"];
        return $tempResult;
    }
    
    /**
     * 设置查询买家最后一次申请保修结束时间     
     * @param String $lastBuyerApplyServiceQuyerEndTime     
     * 参数示例：<pre>2016-01-06 00:00:00</pre>     
     * 此参数必填     */
    public function setLastBuyerApplyServiceQuyerEndTime( $lastBuyerApplyServiceQuyerEndTime) {
        $this->sdkStdResult["lastBuyerApplyServiceQuyerEndTime"] = $lastBuyerApplyServiceQuyerEndTime;
    }
    
        
    private $sdkStdResult=array();
    
    public function getSdkStdResult(){
    	return $this->sdkStdResult;
    }

}
?>