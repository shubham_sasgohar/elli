<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');

class AlibabaAeSellerGetServiceScoreInfoParam {

        
        /**
    * @return 卖家loginId，需要与授权用户相同
    */
        public function getLoginId() {
        $tempResult = $this->sdkStdResult["loginId"];
        return $tempResult;
    }
    
    /**
     * 设置卖家loginId，需要与授权用户相同     
     * @param String $loginId     
     * 参数示例：<pre>cn11311231</pre>     
     * 此参数必填     */
    public function setLoginId( $loginId) {
        $this->sdkStdResult["loginId"] = $loginId;
    }
    
        
        /**
    * @return 语言环境
    */
        public function getLocaleStr() {
        $tempResult = $this->sdkStdResult["localeStr"];
        return $tempResult;
    }
    
    /**
     * 设置语言环境     
     * @param String $localeStr     
     * 参数示例：<pre>zh_CN，zh_TW，en_US</pre>     
     * 此参数必填     */
    public function setLocaleStr( $localeStr) {
        $this->sdkStdResult["localeStr"] = $localeStr;
    }
    
        
    private $sdkStdResult=array();
    
    public function getSdkStdResult(){
    	return $this->sdkStdResult;
    }

}
?>