<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');
include_once ('aliexpress/open/param/AlibabaAeSellerServicescoreAeCurrentLevelInfoDTO.class.php');

class AlibabaAeSellerServicescoreAeCurrentLevelInfoResponse extends SDKDomain {

       	
    private $isSuccess;
    
        /**
    * @return 请求是否成功
    */
        public function getIsSuccess() {
        return $this->isSuccess;
    }
    
    /**
     * 设置请求是否成功     
     * @param Boolean $isSuccess     
     * 参数示例：<pre>true,false</pre>     
     * 此参数必填     */
    public function setIsSuccess( $isSuccess) {
        $this->isSuccess = $isSuccess;
    }
    
        	
    private $errorMsg;
    
        /**
    * @return 请求失败的原因
    */
        public function getErrorMsg() {
        return $this->errorMsg;
    }
    
    /**
     * 设置请求失败的原因     
     * @param String $errorMsg     
     * 参数示例：<pre>参数错误</pre>     
     * 此参数必填     */
    public function setErrorMsg( $errorMsg) {
        $this->errorMsg = $errorMsg;
    }
    
        	
    private $errorCode;
    
        /**
    * @return 请求失败的原因的代码
    */
        public function getErrorCode() {
        return $this->errorCode;
    }
    
    /**
     * 设置请求失败的原因的代码     
     * @param String $errorCode     
     * 参数示例：<pre>40001</pre>     
     * 此参数必填     */
    public function setErrorCode( $errorCode) {
        $this->errorCode = $errorCode;
    }
    
        	
    private $result;
    
        /**
    * @return 当月服务等级的信息
    */
        public function getResult() {
        return $this->result;
    }
    
    /**
     * 设置当月服务等级的信息     
     * @param AlibabaAeSellerServicescoreAeCurrentLevelInfoDTO $result     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setResult(AlibabaAeSellerServicescoreAeCurrentLevelInfoDTO $result) {
        $this->result = $result;
    }
    
    	
	private $stdResult;
	
	public function setStdResult($stdResult) {
		$this->stdResult = $stdResult;
					    			    			if (array_key_exists ( "isSuccess", $this->stdResult )) {
    				$this->isSuccess = $this->stdResult->{"isSuccess"};
    			}
    			    		    				    			    			if (array_key_exists ( "errorMsg", $this->stdResult )) {
    				$this->errorMsg = $this->stdResult->{"errorMsg"};
    			}
    			    		    				    			    			if (array_key_exists ( "errorCode", $this->stdResult )) {
    				$this->errorCode = $this->stdResult->{"errorCode"};
    			}
    			    		    				    			    			if (array_key_exists ( "result", $this->stdResult )) {
    				$resultResult=$this->stdResult->{"result"};
    				$this->result = new AlibabaAeSellerServicescoreAeCurrentLevelInfoDTO();
    				$this->result->setStdResult ( $resultResult);
    			}
    			    		    		}
	
	private $arrayResult;
	public function setArrayResult($arrayResult) {
		$this->arrayResult = $arrayResult;
				    		    			if (array_key_exists ( "isSuccess", $this->arrayResult )) {
    			$this->isSuccess = $arrayResult['isSuccess'];
    			}
    		    	    			    		    			if (array_key_exists ( "errorMsg", $this->arrayResult )) {
    			$this->errorMsg = $arrayResult['errorMsg'];
    			}
    		    	    			    		    			if (array_key_exists ( "errorCode", $this->arrayResult )) {
    			$this->errorCode = $arrayResult['errorCode'];
    			}
    		    	    			    		    		if (array_key_exists ( "result", $this->arrayResult )) {
    		$resultResult=$arrayResult['result'];
    			    			$this->result = new AlibabaAeSellerServicescoreAeCurrentLevelInfoDTO();
    			    			$this->result->$this->setStdResult ( $resultResult);
    		}
    		    	    		}
 
   
}
?>