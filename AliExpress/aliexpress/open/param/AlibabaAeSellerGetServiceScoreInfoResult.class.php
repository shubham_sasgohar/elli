<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');
include_once ('aliexpress/open/param/AlibabaAeSellerServicescoreAeStoreServiceScoreInfoResponse.class.php');

class AlibabaAeSellerGetServiceScoreInfoResult {

        	
    private $response;
    
        /**
    * @return 
    */
        public function getResponse() {
        return $this->response;
    }
    
    /**
     * 设置     
     * @param AlibabaAeSellerServicescoreAeStoreServiceScoreInfoResponse $response     
          
     * 此参数必填     */
    public function setResponse(AlibabaAeSellerServicescoreAeStoreServiceScoreInfoResponse $response) {
        $this->response = $response;
    }
    
    	
	private $stdResult;
	
	public function setStdResult($stdResult) {
		$this->stdResult = $stdResult;
					    			    			if (array_key_exists ( "response", $this->stdResult )) {
    				$responseResult=$this->stdResult->{"response"};
    				$this->response = new AlibabaAeSellerServicescoreAeStoreServiceScoreInfoResponse();
    				$this->response->setStdResult ( $responseResult);
    			}
    			    		    		}
	
	private $arrayResult;
	public function setArrayResult($arrayResult) {
		$this->arrayResult = $arrayResult;
				    		    		if (array_key_exists ( "response", $this->arrayResult )) {
    		$responseResult=$arrayResult['response'];
    			    			$this->response = new AlibabaAeSellerServicescoreAeStoreServiceScoreInfoResponse();
    			    			$this->response->setStdResult ( $responseResult);
    		}
    		    	    		}

}
?>