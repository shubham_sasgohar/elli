<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');

class AlibabaAeLogisticsAeopShipmentDTO extends SDKDomain {

       	
    private $errorCode;
    
        /**
    * @return 错误码
    */
        public function getErrorCode() {
        return $this->errorCode;
    }
    
    /**
     * 设置错误码     
     * @param String $errorCode     
     * 参数示例：<pre>-100</pre>     
     * 此参数必填     */
    public function setErrorCode( $errorCode) {
        $this->errorCode = $errorCode;
    }
    
        	
    private $errorMsg;
    
        /**
    * @return 错误提示
    */
        public function getErrorMsg() {
        return $this->errorMsg;
    }
    
    /**
     * 设置错误提示     
     * @param String $errorMsg     
     * 参数示例：<pre>system error</pre>     
     * 此参数必填     */
    public function setErrorMsg( $errorMsg) {
        $this->errorMsg = $errorMsg;
    }
    
        	
    private $logisticsNo;
    
        /**
    * @return 运单号
    */
        public function getLogisticsNo() {
        return $this->logisticsNo;
    }
    
    /**
     * 设置运单号     
     * @param String $logisticsNo     
     * 参数示例：<pre>LT123456789CN</pre>     
     * 此参数必填     */
    public function setLogisticsNo( $logisticsNo) {
        $this->logisticsNo = $logisticsNo;
    }
    
        	
    private $serviceName;
    
        /**
    * @return 物流公司名称
    */
        public function getServiceName() {
        return $this->serviceName;
    }
    
    /**
     * 设置物流公司名称     
     * @param String $serviceName     
     * 参数示例：<pre>CAINIAO_STANDARD</pre>     
     * 此参数必填     */
    public function setServiceName( $serviceName) {
        $this->serviceName = $serviceName;
    }
    
        	
    private $trackingWebSite;
    
        /**
    * @return 追踪网址
    */
        public function getTrackingWebSite() {
        return $this->trackingWebSite;
    }
    
    /**
     * 设置追踪网址     
     * @param String $trackingWebSite     
     * 参数示例：<pre>www.17track.com</pre>     
     * 此参数必填     */
    public function setTrackingWebSite( $trackingWebSite) {
        $this->trackingWebSite = $trackingWebSite;
    }
    
    	
	private $stdResult;
	
	public function setStdResult($stdResult) {
		$this->stdResult = $stdResult;
					    			    			if (array_key_exists ( "errorCode", $this->stdResult )) {
    				$this->errorCode = $this->stdResult->{"errorCode"};
    			}
    			    		    				    			    			if (array_key_exists ( "errorMsg", $this->stdResult )) {
    				$this->errorMsg = $this->stdResult->{"errorMsg"};
    			}
    			    		    				    			    			if (array_key_exists ( "logisticsNo", $this->stdResult )) {
    				$this->logisticsNo = $this->stdResult->{"logisticsNo"};
    			}
    			    		    				    			    			if (array_key_exists ( "serviceName", $this->stdResult )) {
    				$this->serviceName = $this->stdResult->{"serviceName"};
    			}
    			    		    				    			    			if (array_key_exists ( "trackingWebSite", $this->stdResult )) {
    				$this->trackingWebSite = $this->stdResult->{"trackingWebSite"};
    			}
    			    		    		}
	
	private $arrayResult;
	public function setArrayResult($arrayResult) {
		$this->arrayResult = $arrayResult;
				    		    			if (array_key_exists ( "errorCode", $this->arrayResult )) {
    			$this->errorCode = $arrayResult['errorCode'];
    			}
    		    	    			    		    			if (array_key_exists ( "errorMsg", $this->arrayResult )) {
    			$this->errorMsg = $arrayResult['errorMsg'];
    			}
    		    	    			    		    			if (array_key_exists ( "logisticsNo", $this->arrayResult )) {
    			$this->logisticsNo = $arrayResult['logisticsNo'];
    			}
    		    	    			    		    			if (array_key_exists ( "serviceName", $this->arrayResult )) {
    			$this->serviceName = $arrayResult['serviceName'];
    			}
    		    	    			    		    			if (array_key_exists ( "trackingWebSite", $this->arrayResult )) {
    			$this->trackingWebSite = $arrayResult['trackingWebSite'];
    			}
    		    	    		}
 
   
}
?>