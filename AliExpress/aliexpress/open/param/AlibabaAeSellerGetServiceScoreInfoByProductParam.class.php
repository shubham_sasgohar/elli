<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');
include_once ('aliexpress/open/param/AlibabaAeSellerServicescoreAeProductServiceScoreQueryRequest.class.php');

class AlibabaAeSellerGetServiceScoreInfoByProductParam {

        
        /**
    * @return 
    */
        public function getQuery() {
        $tempResult = $this->sdkStdResult["query"];
        return $tempResult;
    }
    
    /**
     * 设置     
     * @param AlibabaAeSellerServicescoreAeProductServiceScoreQueryRequest $query     
     * 参数示例：<pre>查询参数</pre>     
     * 此参数必填     */
    public function setQuery(AlibabaAeSellerServicescoreAeProductServiceScoreQueryRequest $query) {
        $this->sdkStdResult["query"] = $query;
    }
    
        
    private $sdkStdResult=array();
    
    public function getSdkStdResult(){
    	return $this->sdkStdResult;
    }

}
?>