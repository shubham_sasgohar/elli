<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');

class AlibabaAeSellerServicescoreAeServiceIndexDTO extends SDKDomain {

       	
    private $buyNotSelRate;
    
        /**
    * @return 拍而不卖率
    */
        public function getBuyNotSelRate() {
        return $this->buyNotSelRate;
    }
    
    /**
     * 设置拍而不卖率     
     * @param String $buyNotSelRate     
     * 参数示例：<pre>0.39%</pre>     
     * 此参数必填     */
    public function setBuyNotSelRate( $buyNotSelRate) {
        $this->buyNotSelRate = $buyNotSelRate;
    }
    
        	
    private $nrIssueMordCnt;
    
        /**
    * @return 免责前未收到货纠纷发起率分子父订单数
    */
        public function getNrIssueMordCnt() {
        return $this->nrIssueMordCnt;
    }
    
    /**
     * 设置免责前未收到货纠纷发起率分子父订单数     
     * @param Long $nrIssueMordCnt     
     * 参数示例：<pre>66</pre>     
     * 此参数必填     */
    public function setNrIssueMordCnt( $nrIssueMordCnt) {
        $this->nrIssueMordCnt = $nrIssueMordCnt;
    }
    
        	
    private $nrDisclaimerIssueMordCnt;
    
        /**
    * @return 免责后未收到货纠纷发起率分子父订单数
    */
        public function getNrDisclaimerIssueMordCnt() {
        return $this->nrDisclaimerIssueMordCnt;
    }
    
    /**
     * 设置免责后未收到货纠纷发起率分子父订单数     
     * @param Long $nrDisclaimerIssueMordCnt     
     * 参数示例：<pre>41</pre>     
     * 此参数必填     */
    public function setNrDisclaimerIssueMordCnt( $nrDisclaimerIssueMordCnt) {
        $this->nrDisclaimerIssueMordCnt = $nrDisclaimerIssueMordCnt;
    }
    
        	
    private $nrDisclaimerIssueRate;
    
        /**
    * @return 免责后未收到货纠纷发起率
    */
        public function getNrDisclaimerIssueRate() {
        return $this->nrDisclaimerIssueRate;
    }
    
    /**
     * 设置免责后未收到货纠纷发起率     
     * @param String $nrDisclaimerIssueRate     
     * 参数示例：<pre>4.6%</pre>     
     * 此参数必填     */
    public function setNrDisclaimerIssueRate( $nrDisclaimerIssueRate) {
        $this->nrDisclaimerIssueRate = $nrDisclaimerIssueRate;
    }
    
        	
    private $snadIssueMordCnt;
    
        /**
    * @return 免责前货不对版纠纷发起率分子父订单数
    */
        public function getSnadIssueMordCnt() {
        return $this->snadIssueMordCnt;
    }
    
    /**
     * 设置免责前货不对版纠纷发起率分子父订单数     
     * @param Long $snadIssueMordCnt     
     * 参数示例：<pre>2</pre>     
     * 此参数必填     */
    public function setSnadIssueMordCnt( $snadIssueMordCnt) {
        $this->snadIssueMordCnt = $snadIssueMordCnt;
    }
    
        	
    private $snadDisclaimerIssueMordCnt;
    
        /**
    * @return 免责后货不对版纠纷发起率分子父订单数
    */
        public function getSnadDisclaimerIssueMordCnt() {
        return $this->snadDisclaimerIssueMordCnt;
    }
    
    /**
     * 设置免责后货不对版纠纷发起率分子父订单数     
     * @param Long $snadDisclaimerIssueMordCnt     
     * 参数示例：<pre>0</pre>     
     * 此参数必填     */
    public function setSnadDisclaimerIssueMordCnt( $snadDisclaimerIssueMordCnt) {
        $this->snadDisclaimerIssueMordCnt = $snadDisclaimerIssueMordCnt;
    }
    
        	
    private $snadDisclaimerIssueRate;
    
        /**
    * @return 免责后货不对版纠纷发起率
    */
        public function getSnadDisclaimerIssueRate() {
        return $this->snadDisclaimerIssueRate;
    }
    
    /**
     * 设置免责后货不对版纠纷发起率     
     * @param String $snadDisclaimerIssueRate     
     * 参数示例：<pre>0.37%</pre>     
     * 此参数必填     */
    public function setSnadDisclaimerIssueRate( $snadDisclaimerIssueRate) {
        $this->snadDisclaimerIssueRate = $snadDisclaimerIssueRate;
    }
    
        	
    private $dsrProdScore;
    
        /**
    * @return DSR商品评价综合评分
    */
        public function getDsrProdScore() {
        return $this->dsrProdScore;
    }
    
    /**
     * 设置DSR商品评价综合评分     
     * @param String $dsrProdScore     
     * 参数示例：<pre>4.41</pre>     
     * 此参数必填     */
    public function setDsrProdScore( $dsrProdScore) {
        $this->dsrProdScore = $dsrProdScore;
    }
    
        	
    private $dsrCommunicateScore;
    
        /**
    * @return DSR卖家服务评价综合评分
    */
        public function getDsrCommunicateScore() {
        return $this->dsrCommunicateScore;
    }
    
    /**
     * 设置DSR卖家服务评价综合评分     
     * @param String $dsrCommunicateScore     
     * 参数示例：<pre>4.55</pre>     
     * 此参数必填     */
    public function setDsrCommunicateScore( $dsrCommunicateScore) {
        $this->dsrCommunicateScore = $dsrCommunicateScore;
    }
    
        	
    private $dsrLogisScoreAftDisclaim;
    
        /**
    * @return DSR物流服务评价综合评分（免责后）
    */
        public function getDsrLogisScoreAftDisclaim() {
        return $this->dsrLogisScoreAftDisclaim;
    }
    
    /**
     * 设置DSR物流服务评价综合评分（免责后）     
     * @param String $dsrLogisScoreAftDisclaim     
     * 参数示例：<pre>4.7</pre>     
     * 此参数必填     */
    public function setDsrLogisScoreAftDisclaim( $dsrLogisScoreAftDisclaim) {
        $this->dsrLogisScoreAftDisclaim = $dsrLogisScoreAftDisclaim;
    }
    
        	
    private $logis48hSendGoodsRate;
    
        /**
    * @return 48小时发货率
    */
        public function getLogis48hSendGoodsRate() {
        return $this->logis48hSendGoodsRate;
    }
    
    /**
     * 设置48小时发货率     
     * @param String $logis48hSendGoodsRate     
     * 参数示例：<pre>6.91%</pre>     
     * 此参数必填     */
    public function setLogis48hSendGoodsRate( $logis48hSendGoodsRate) {
        $this->logis48hSendGoodsRate = $logis48hSendGoodsRate;
    }
    
    	
	private $stdResult;
	
	public function setStdResult($stdResult) {
		$this->stdResult = $stdResult;
					    			    			if (array_key_exists ( "buyNotSelRate", $this->stdResult )) {
    				$this->buyNotSelRate = $this->stdResult->{"buyNotSelRate"};
    			}
    			    		    				    			    			if (array_key_exists ( "nrIssueMordCnt", $this->stdResult )) {
    				$this->nrIssueMordCnt = $this->stdResult->{"nrIssueMordCnt"};
    			}
    			    		    				    			    			if (array_key_exists ( "nrDisclaimerIssueMordCnt", $this->stdResult )) {
    				$this->nrDisclaimerIssueMordCnt = $this->stdResult->{"nrDisclaimerIssueMordCnt"};
    			}
    			    		    				    			    			if (array_key_exists ( "nrDisclaimerIssueRate", $this->stdResult )) {
    				$this->nrDisclaimerIssueRate = $this->stdResult->{"nrDisclaimerIssueRate"};
    			}
    			    		    				    			    			if (array_key_exists ( "snadIssueMordCnt", $this->stdResult )) {
    				$this->snadIssueMordCnt = $this->stdResult->{"snadIssueMordCnt"};
    			}
    			    		    				    			    			if (array_key_exists ( "snadDisclaimerIssueMordCnt", $this->stdResult )) {
    				$this->snadDisclaimerIssueMordCnt = $this->stdResult->{"snadDisclaimerIssueMordCnt"};
    			}
    			    		    				    			    			if (array_key_exists ( "snadDisclaimerIssueRate", $this->stdResult )) {
    				$this->snadDisclaimerIssueRate = $this->stdResult->{"snadDisclaimerIssueRate"};
    			}
    			    		    				    			    			if (array_key_exists ( "dsrProdScore", $this->stdResult )) {
    				$this->dsrProdScore = $this->stdResult->{"dsrProdScore"};
    			}
    			    		    				    			    			if (array_key_exists ( "dsrCommunicateScore", $this->stdResult )) {
    				$this->dsrCommunicateScore = $this->stdResult->{"dsrCommunicateScore"};
    			}
    			    		    				    			    			if (array_key_exists ( "dsrLogisScoreAftDisclaim", $this->stdResult )) {
    				$this->dsrLogisScoreAftDisclaim = $this->stdResult->{"dsrLogisScoreAftDisclaim"};
    			}
    			    		    				    			    			if (array_key_exists ( "logis48hSendGoodsRate", $this->stdResult )) {
    				$this->logis48hSendGoodsRate = $this->stdResult->{"logis48hSendGoodsRate"};
    			}
    			    		    		}
	
	private $arrayResult;
	public function setArrayResult($arrayResult) {
		$this->arrayResult = $arrayResult;
				    		    			if (array_key_exists ( "buyNotSelRate", $this->arrayResult )) {
    			$this->buyNotSelRate = $arrayResult['buyNotSelRate'];
    			}
    		    	    			    		    			if (array_key_exists ( "nrIssueMordCnt", $this->arrayResult )) {
    			$this->nrIssueMordCnt = $arrayResult['nrIssueMordCnt'];
    			}
    		    	    			    		    			if (array_key_exists ( "nrDisclaimerIssueMordCnt", $this->arrayResult )) {
    			$this->nrDisclaimerIssueMordCnt = $arrayResult['nrDisclaimerIssueMordCnt'];
    			}
    		    	    			    		    			if (array_key_exists ( "nrDisclaimerIssueRate", $this->arrayResult )) {
    			$this->nrDisclaimerIssueRate = $arrayResult['nrDisclaimerIssueRate'];
    			}
    		    	    			    		    			if (array_key_exists ( "snadIssueMordCnt", $this->arrayResult )) {
    			$this->snadIssueMordCnt = $arrayResult['snadIssueMordCnt'];
    			}
    		    	    			    		    			if (array_key_exists ( "snadDisclaimerIssueMordCnt", $this->arrayResult )) {
    			$this->snadDisclaimerIssueMordCnt = $arrayResult['snadDisclaimerIssueMordCnt'];
    			}
    		    	    			    		    			if (array_key_exists ( "snadDisclaimerIssueRate", $this->arrayResult )) {
    			$this->snadDisclaimerIssueRate = $arrayResult['snadDisclaimerIssueRate'];
    			}
    		    	    			    		    			if (array_key_exists ( "dsrProdScore", $this->arrayResult )) {
    			$this->dsrProdScore = $arrayResult['dsrProdScore'];
    			}
    		    	    			    		    			if (array_key_exists ( "dsrCommunicateScore", $this->arrayResult )) {
    			$this->dsrCommunicateScore = $arrayResult['dsrCommunicateScore'];
    			}
    		    	    			    		    			if (array_key_exists ( "dsrLogisScoreAftDisclaim", $this->arrayResult )) {
    			$this->dsrLogisScoreAftDisclaim = $arrayResult['dsrLogisScoreAftDisclaim'];
    			}
    		    	    			    		    			if (array_key_exists ( "logis48hSendGoodsRate", $this->arrayResult )) {
    			$this->logis48hSendGoodsRate = $arrayResult['logis48hSendGoodsRate'];
    			}
    		    	    		}
 
   
}
?>