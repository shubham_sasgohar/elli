<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');

class AlibabaAeSellerServicescoreAeServiceScoreDTO extends SDKDomain {

       	
    private $totalScore;
    
        /**
    * @return 服务总得分
    */
        public function getTotalScore() {
        return $this->totalScore;
    }
    
    /**
     * 设置服务总得分     
     * @param String $totalScore     
     * 参数示例：<pre>75.61</pre>     
     * 此参数必填     */
    public function setTotalScore( $totalScore) {
        $this->totalScore = $totalScore;
    }
    
        	
    private $buyNotSelScore;
    
        /**
    * @return 拍而不卖得分
    */
        public function getBuyNotSelScore() {
        return $this->buyNotSelScore;
    }
    
    /**
     * 设置拍而不卖得分     
     * @param String $buyNotSelScore     
     * 参数示例：<pre>4.52</pre>     
     * 此参数必填     */
    public function setBuyNotSelScore( $buyNotSelScore) {
        $this->buyNotSelScore = $buyNotSelScore;
    }
    
        	
    private $nrIssueScore;
    
        /**
    * @return 未收到货纠纷得分
    */
        public function getNrIssueScore() {
        return $this->nrIssueScore;
    }
    
    /**
     * 设置未收到货纠纷得分     
     * @param String $nrIssueScore     
     * 参数示例：<pre>3.73</pre>     
     * 此参数必填     */
    public function setNrIssueScore( $nrIssueScore) {
        $this->nrIssueScore = $nrIssueScore;
    }
    
        	
    private $snadIssueScore;
    
        /**
    * @return 货不对版纠纷得分
    */
        public function getSnadIssueScore() {
        return $this->snadIssueScore;
    }
    
    /**
     * 设置货不对版纠纷得分     
     * @param String $snadIssueScore     
     * 参数示例：<pre>10.0</pre>     
     * 此参数必填     */
    public function setSnadIssueScore( $snadIssueScore) {
        $this->snadIssueScore = $snadIssueScore;
    }
    
        	
    private $dsrProdScore;
    
        /**
    * @return DSR商品描述得分
    */
        public function getDsrProdScore() {
        return $this->dsrProdScore;
    }
    
    /**
     * 设置DSR商品描述得分     
     * @param String $dsrProdScore     
     * 参数示例：<pre>15.3</pre>     
     * 此参数必填     */
    public function setDsrProdScore( $dsrProdScore) {
        $this->dsrProdScore = $dsrProdScore;
    }
    
        	
    private $dsrCommunicateScore;
    
        /**
    * @return DSR卖家服务得分
    */
        public function getDsrCommunicateScore() {
        return $this->dsrCommunicateScore;
    }
    
    /**
     * 设置DSR卖家服务得分     
     * @param String $dsrCommunicateScore     
     * 参数示例：<pre>9.56</pre>     
     * 此参数必填     */
    public function setDsrCommunicateScore( $dsrCommunicateScore) {
        $this->dsrCommunicateScore = $dsrCommunicateScore;
    }
    
        	
    private $dsrLogisScore;
    
        /**
    * @return DSR物流得分
    */
        public function getDsrLogisScore() {
        return $this->dsrLogisScore;
    }
    
    /**
     * 设置DSR物流得分     
     * @param String $dsrLogisScore     
     * 参数示例：<pre>7.5</pre>     
     * 此参数必填     */
    public function setDsrLogisScore( $dsrLogisScore) {
        $this->dsrLogisScore = $dsrLogisScore;
    }
    
    	
	private $stdResult;
	
	public function setStdResult($stdResult) {
		$this->stdResult = $stdResult;
					    			    			if (array_key_exists ( "totalScore", $this->stdResult )) {
    				$this->totalScore = $this->stdResult->{"totalScore"};
    			}
    			    		    				    			    			if (array_key_exists ( "buyNotSelScore", $this->stdResult )) {
    				$this->buyNotSelScore = $this->stdResult->{"buyNotSelScore"};
    			}
    			    		    				    			    			if (array_key_exists ( "nrIssueScore", $this->stdResult )) {
    				$this->nrIssueScore = $this->stdResult->{"nrIssueScore"};
    			}
    			    		    				    			    			if (array_key_exists ( "snadIssueScore", $this->stdResult )) {
    				$this->snadIssueScore = $this->stdResult->{"snadIssueScore"};
    			}
    			    		    				    			    			if (array_key_exists ( "dsrProdScore", $this->stdResult )) {
    				$this->dsrProdScore = $this->stdResult->{"dsrProdScore"};
    			}
    			    		    				    			    			if (array_key_exists ( "dsrCommunicateScore", $this->stdResult )) {
    				$this->dsrCommunicateScore = $this->stdResult->{"dsrCommunicateScore"};
    			}
    			    		    				    			    			if (array_key_exists ( "dsrLogisScore", $this->stdResult )) {
    				$this->dsrLogisScore = $this->stdResult->{"dsrLogisScore"};
    			}
    			    		    		}
	
	private $arrayResult;
	public function setArrayResult($arrayResult) {
		$this->arrayResult = $arrayResult;
				    		    			if (array_key_exists ( "totalScore", $this->arrayResult )) {
    			$this->totalScore = $arrayResult['totalScore'];
    			}
    		    	    			    		    			if (array_key_exists ( "buyNotSelScore", $this->arrayResult )) {
    			$this->buyNotSelScore = $arrayResult['buyNotSelScore'];
    			}
    		    	    			    		    			if (array_key_exists ( "nrIssueScore", $this->arrayResult )) {
    			$this->nrIssueScore = $arrayResult['nrIssueScore'];
    			}
    		    	    			    		    			if (array_key_exists ( "snadIssueScore", $this->arrayResult )) {
    			$this->snadIssueScore = $arrayResult['snadIssueScore'];
    			}
    		    	    			    		    			if (array_key_exists ( "dsrProdScore", $this->arrayResult )) {
    			$this->dsrProdScore = $arrayResult['dsrProdScore'];
    			}
    		    	    			    		    			if (array_key_exists ( "dsrCommunicateScore", $this->arrayResult )) {
    			$this->dsrCommunicateScore = $arrayResult['dsrCommunicateScore'];
    			}
    		    	    			    		    			if (array_key_exists ( "dsrLogisScore", $this->arrayResult )) {
    			$this->dsrLogisScore = $arrayResult['dsrLogisScore'];
    			}
    		    	    		}
 
   
}
?>