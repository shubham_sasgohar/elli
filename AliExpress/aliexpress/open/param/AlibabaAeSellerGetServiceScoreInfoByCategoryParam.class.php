<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');
include_once ('aliexpress/open/param/AlibabaAeSellerServicescoreAeCategoryServiceScoreQueryRequest.class.php');

class AlibabaAeSellerGetServiceScoreInfoByCategoryParam {

        
        /**
    * @return 查询参数
    */
        public function getQuery() {
        $tempResult = $this->sdkStdResult["query"];
        return $tempResult;
    }
    
    /**
     * 设置查询参数     
     * @param AlibabaAeSellerServicescoreAeCategoryServiceScoreQueryRequest $query     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setQuery(AlibabaAeSellerServicescoreAeCategoryServiceScoreQueryRequest $query) {
        $this->sdkStdResult["query"] = $query;
    }
    
        
    private $sdkStdResult=array();
    
    public function getSdkStdResult(){
    	return $this->sdkStdResult;
    }

}
?>