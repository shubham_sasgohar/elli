<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');

class AlibabaAeSellerServicescoreAeProductServiceScoreQueryRequest extends SDKDomain {

       	
    private $loginId;
    
        /**
    * @return 卖家loginId，需要与授权用户相同
    */
        public function getLoginId() {
        return $this->loginId;
    }
    
    /**
     * 设置卖家loginId，需要与授权用户相同     
     * @param String $loginId     
     * 参数示例：<pre>cn1231231</pre>     
     * 此参数必填     */
    public function setLoginId( $loginId) {
        $this->loginId = $loginId;
    }
    
        	
    private $itemId;
    
        /**
    * @return 商品ID
    */
        public function getItemId() {
        return $this->itemId;
    }
    
    /**
     * 设置商品ID     
     * @param Long $itemId     
     * 参数示例：<pre>32792839700</pre>     
     * 此参数必填     */
    public function setItemId( $itemId) {
        $this->itemId = $itemId;
    }
    
        	
    private $localeStr;
    
        /**
    * @return 语言环境： e.g:zh_CN 简体中文, zh_TW 繁体中文, en_US 英文
    */
        public function getLocaleStr() {
        return $this->localeStr;
    }
    
    /**
     * 设置语言环境： e.g:zh_CN 简体中文, zh_TW 繁体中文, en_US 英文     
     * @param String $localeStr     
     * 参数示例：<pre>zh_CN</pre>     
     * 此参数必填     */
    public function setLocaleStr( $localeStr) {
        $this->localeStr = $localeStr;
    }
    
    	
	private $stdResult;
	
	public function setStdResult($stdResult) {
		$this->stdResult = $stdResult;
					    			    			if (array_key_exists ( "loginId", $this->stdResult )) {
    				$this->loginId = $this->stdResult->{"loginId"};
    			}
    			    		    				    			    			if (array_key_exists ( "itemId", $this->stdResult )) {
    				$this->itemId = $this->stdResult->{"itemId"};
    			}
    			    		    				    			    			if (array_key_exists ( "localeStr", $this->stdResult )) {
    				$this->localeStr = $this->stdResult->{"localeStr"};
    			}
    			    		    		}
	
	private $arrayResult;
	public function setArrayResult($arrayResult) {
		$this->arrayResult = $arrayResult;
				    		    			if (array_key_exists ( "loginId", $this->arrayResult )) {
    			$this->loginId = $arrayResult['loginId'];
    			}
    		    	    			    		    			if (array_key_exists ( "itemId", $this->arrayResult )) {
    			$this->itemId = $arrayResult['itemId'];
    			}
    		    	    			    		    			if (array_key_exists ( "localeStr", $this->arrayResult )) {
    			$this->localeStr = $arrayResult['localeStr'];
    			}
    		    	    		}
 
   
}
?>