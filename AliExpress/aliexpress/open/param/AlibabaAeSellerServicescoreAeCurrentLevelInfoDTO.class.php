<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');

class AlibabaAeSellerServicescoreAeCurrentLevelInfoDTO extends SDKDomain {

       	
    private $appraisePeriod;
    
        /**
    * @return 当月考核周期
    */
        public function getAppraisePeriod() {
        return $this->appraisePeriod;
    }
    
    /**
     * 设置当月考核周期     
     * @param Integer $appraisePeriod     
     * 参数示例：<pre>30或者90</pre>     
     * 此参数必填     */
    public function setAppraisePeriod( $appraisePeriod) {
        $this->appraisePeriod = $appraisePeriod;
    }
    
        	
    private $level;
    
        /**
    * @return 当月服务等级
(1-不考核
2-不及格
3-及格
4-良好
5-优秀)
    */
        public function getLevel() {
        return $this->level;
    }
    
    /**
     * 设置当月服务等级
(1-不考核
2-不及格
3-及格
4-良好
5-优秀)     
     * @param Integer $level     
     * 参数示例：<pre>4</pre>     
     * 此参数必填     */
    public function setLevel( $level) {
        $this->level = $level;
    }
    
        	
    private $checkMOrderCount;
    
        /**
    * @return 考核订单量
    */
        public function getCheckMOrderCount() {
        return $this->checkMOrderCount;
    }
    
    /**
     * 设置考核订单量     
     * @param Long $checkMOrderCount     
     * 参数示例：<pre>200</pre>     
     * 此参数必填     */
    public function setCheckMOrderCount( $checkMOrderCount) {
        $this->checkMOrderCount = $checkMOrderCount;
    }
    
        	
    private $avgScore;
    
        /**
    * @return 上月每日服务得分均值
    */
        public function getAvgScore() {
        return $this->avgScore;
    }
    
    /**
     * 设置上月每日服务得分均值     
     * @param String $avgScore     
     * 参数示例：<pre>98.21</pre>     
     * 此参数必填     */
    public function setAvgScore( $avgScore) {
        $this->avgScore = $avgScore;
    }
    
        	
    private $startDate;
    
        /**
    * @return 当月服务等级计算起始时间
    */
        public function getStartDate() {
        return $this->startDate;
    }
    
    /**
     * 设置当月服务等级计算起始时间     
     * @param String $startDate     
     * 参数示例：<pre>20160729</pre>     
     * 此参数必填     */
    public function setStartDate( $startDate) {
        $this->startDate = $startDate;
    }
    
        	
    private $endDate;
    
        /**
    * @return 当月服务等级计算截止时间
    */
        public function getEndDate() {
        return $this->endDate;
    }
    
    /**
     * 设置当月服务等级计算截止时间     
     * @param String $endDate     
     * 参数示例：<pre>20160828</pre>     
     * 此参数必填     */
    public function setEndDate( $endDate) {
        $this->endDate = $endDate;
    }
    
        	
    private $predictLevel;
    
        /**
    * @return 下月预估等级
(1-不考核
2-不及格
3-及格
4-良好
5-优秀)
    */
        public function getPredictLevel() {
        return $this->predictLevel;
    }
    
    /**
     * 设置下月预估等级
(1-不考核
2-不及格
3-及格
4-良好
5-优秀)     
     * @param Integer $predictLevel     
     * 参数示例：<pre>4</pre>     
     * 此参数必填     */
    public function setPredictLevel( $predictLevel) {
        $this->predictLevel = $predictLevel;
    }
    
        	
    private $predictAvgScore;
    
        /**
    * @return 预估服务分得分均值
    */
        public function getPredictAvgScore() {
        return $this->predictAvgScore;
    }
    
    /**
     * 设置预估服务分得分均值     
     * @param String $predictAvgScore     
     * 参数示例：<pre>96.21</pre>     
     * 此参数必填     */
    public function setPredictAvgScore( $predictAvgScore) {
        $this->predictAvgScore = $predictAvgScore;
    }
    
        	
    private $predictStartDate;
    
        /**
    * @return 下月等级预估计算起始时间
    */
        public function getPredictStartDate() {
        return $this->predictStartDate;
    }
    
    /**
     * 设置下月等级预估计算起始时间     
     * @param String $predictStartDate     
     * 参数示例：<pre>20160829</pre>     
     * 此参数必填     */
    public function setPredictStartDate( $predictStartDate) {
        $this->predictStartDate = $predictStartDate;
    }
    
        	
    private $predictEndDate;
    
        /**
    * @return 下月等级预估计算截止时间
    */
        public function getPredictEndDate() {
        return $this->predictEndDate;
    }
    
    /**
     * 设置下月等级预估计算截止时间     
     * @param String $predictEndDate     
     * 参数示例：<pre>20160917</pre>     
     * 此参数必填     */
    public function setPredictEndDate( $predictEndDate) {
        $this->predictEndDate = $predictEndDate;
    }
    
    	
	private $stdResult;
	
	public function setStdResult($stdResult) {
		$this->stdResult = $stdResult;
					    			    			if (array_key_exists ( "appraisePeriod", $this->stdResult )) {
    				$this->appraisePeriod = $this->stdResult->{"appraisePeriod"};
    			}
    			    		    				    			    			if (array_key_exists ( "level", $this->stdResult )) {
    				$this->level = $this->stdResult->{"level"};
    			}
    			    		    				    			    			if (array_key_exists ( "checkMOrderCount", $this->stdResult )) {
    				$this->checkMOrderCount = $this->stdResult->{"checkMOrderCount"};
    			}
    			    		    				    			    			if (array_key_exists ( "avgScore", $this->stdResult )) {
    				$this->avgScore = $this->stdResult->{"avgScore"};
    			}
    			    		    				    			    			if (array_key_exists ( "startDate", $this->stdResult )) {
    				$this->startDate = $this->stdResult->{"startDate"};
    			}
    			    		    				    			    			if (array_key_exists ( "endDate", $this->stdResult )) {
    				$this->endDate = $this->stdResult->{"endDate"};
    			}
    			    		    				    			    			if (array_key_exists ( "predictLevel", $this->stdResult )) {
    				$this->predictLevel = $this->stdResult->{"predictLevel"};
    			}
    			    		    				    			    			if (array_key_exists ( "predictAvgScore", $this->stdResult )) {
    				$this->predictAvgScore = $this->stdResult->{"predictAvgScore"};
    			}
    			    		    				    			    			if (array_key_exists ( "predictStartDate", $this->stdResult )) {
    				$this->predictStartDate = $this->stdResult->{"predictStartDate"};
    			}
    			    		    				    			    			if (array_key_exists ( "predictEndDate", $this->stdResult )) {
    				$this->predictEndDate = $this->stdResult->{"predictEndDate"};
    			}
    			    		    		}
	
	private $arrayResult;
	public function setArrayResult($arrayResult) {
		$this->arrayResult = $arrayResult;
				    		    			if (array_key_exists ( "appraisePeriod", $this->arrayResult )) {
    			$this->appraisePeriod = $arrayResult['appraisePeriod'];
    			}
    		    	    			    		    			if (array_key_exists ( "level", $this->arrayResult )) {
    			$this->level = $arrayResult['level'];
    			}
    		    	    			    		    			if (array_key_exists ( "checkMOrderCount", $this->arrayResult )) {
    			$this->checkMOrderCount = $arrayResult['checkMOrderCount'];
    			}
    		    	    			    		    			if (array_key_exists ( "avgScore", $this->arrayResult )) {
    			$this->avgScore = $arrayResult['avgScore'];
    			}
    		    	    			    		    			if (array_key_exists ( "startDate", $this->arrayResult )) {
    			$this->startDate = $arrayResult['startDate'];
    			}
    		    	    			    		    			if (array_key_exists ( "endDate", $this->arrayResult )) {
    			$this->endDate = $arrayResult['endDate'];
    			}
    		    	    			    		    			if (array_key_exists ( "predictLevel", $this->arrayResult )) {
    			$this->predictLevel = $arrayResult['predictLevel'];
    			}
    		    	    			    		    			if (array_key_exists ( "predictAvgScore", $this->arrayResult )) {
    			$this->predictAvgScore = $arrayResult['predictAvgScore'];
    			}
    		    	    			    		    			if (array_key_exists ( "predictStartDate", $this->arrayResult )) {
    			$this->predictStartDate = $arrayResult['predictStartDate'];
    			}
    		    	    			    		    			if (array_key_exists ( "predictEndDate", $this->arrayResult )) {
    			$this->predictEndDate = $arrayResult['predictEndDate'];
    			}
    		    	    		}
 
   
}
?>