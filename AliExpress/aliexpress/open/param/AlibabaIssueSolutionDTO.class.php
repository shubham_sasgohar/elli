<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');
include_once ('aliexpress/open/param/IssueMoney.class.php');
include_once ('aliexpress/open/param/IssueMoney.class.php');

class AlibabaIssueSolutionDTO extends SDKDomain {

       	
    private $id;
    
        /**
    * @return 方案id
    */
        public function getId() {
        return $this->id;
    }
    
    /**
     * 设置方案id     
     * @param Long $id     
     * 参数示例：<pre>1234567890</pre>     
     * 此参数必填     */
    public function setId( $id) {
        $this->id = $id;
    }
    
        	
    private $gmtCreate;
    
        /**
    * @return 方案创建时间
    */
        public function getGmtCreate() {
        return $this->gmtCreate;
    }
    
    /**
     * 设置方案创建时间     
     * @param Date $gmtCreate     
     * 参数示例：<pre>2016-08-31 23:23:23</pre>     
     * 此参数必填     */
    public function setGmtCreate( $gmtCreate) {
        $this->gmtCreate = $gmtCreate;
    }
    
        	
    private $gmtModified;
    
        /**
    * @return 方案修改时间
    */
        public function getGmtModified() {
        return $this->gmtModified;
    }
    
    /**
     * 设置方案修改时间     
     * @param Date $gmtModified     
     * 参数示例：<pre>2016-08-31 23:23:23</pre>     
     * 此参数必填     */
    public function setGmtModified( $gmtModified) {
        $this->gmtModified = $gmtModified;
    }
    
        	
    private $version;
    
        /**
    * @return 版本号
    */
        public function getVersion() {
        return $this->version;
    }
    
    /**
     * 设置版本号     
     * @param Integer $version     
     * 参数示例：<pre>1</pre>     
     * 此参数必填     */
    public function setVersion( $version) {
        $this->version = $version;
    }
    
        	
    private $buyerAliid;
    
        /**
    * @return 买家Aliid
    */
        public function getBuyerAliid() {
        return $this->buyerAliid;
    }
    
    /**
     * 设置买家Aliid     
     * @param Long $buyerAliid     
     * 参数示例：<pre>12345678</pre>     
     * 此参数必填     */
    public function setBuyerAliid( $buyerAliid) {
        $this->buyerAliid = $buyerAliid;
    }
    
        	
    private $sellerAliid;
    
        /**
    * @return 卖家Aliid
    */
        public function getSellerAliid() {
        return $this->sellerAliid;
    }
    
    /**
     * 设置卖家Aliid     
     * @param Long $sellerAliid     
     * 参数示例：<pre>12345678</pre>     
     * 此参数必填     */
    public function setSellerAliid( $sellerAliid) {
        $this->sellerAliid = $sellerAliid;
    }
    
        	
    private $orderId;
    
        /**
    * @return 子订单号
    */
        public function getOrderId() {
        return $this->orderId;
    }
    
    /**
     * 设置子订单号     
     * @param Long $orderId     
     * 参数示例：<pre>7647012345678</pre>     
     * 此参数必填     */
    public function setOrderId( $orderId) {
        $this->orderId = $orderId;
    }
    
        	
    private $issueId;
    
        /**
    * @return 纠纷id
    */
        public function getIssueId() {
        return $this->issueId;
    }
    
    /**
     * 设置纠纷id     
     * @param Long $issueId     
     * 参数示例：<pre>40001234567856</pre>     
     * 此参数必填     */
    public function setIssueId( $issueId) {
        $this->issueId = $issueId;
    }
    
        	
    private $refundMoney;
    
        /**
    * @return 退款金额本币
    */
        public function getRefundMoney() {
        return $this->refundMoney;
    }
    
    /**
     * 设置退款金额本币     
     * @param IssueMoney $refundMoney     
     * 参数示例：<pre>23.5RUB</pre>     
     * 此参数必填     */
    public function setRefundMoney(IssueMoney $refundMoney) {
        $this->refundMoney = $refundMoney;
    }
    
        	
    private $refundMoneyPost;
    
        /**
    * @return 退款金额美金
    */
        public function getRefundMoneyPost() {
        return $this->refundMoneyPost;
    }
    
    /**
     * 设置退款金额美金     
     * @param IssueMoney $refundMoneyPost     
     * 参数示例：<pre>10.12USD</pre>     
     * 此参数必填     */
    public function setRefundMoneyPost(IssueMoney $refundMoneyPost) {
        $this->refundMoneyPost = $refundMoneyPost;
    }
    
        	
    private $isDefault;
    
        /**
    * @return 是否是默认方案
    */
        public function getIsDefault() {
        return $this->isDefault;
    }
    
    /**
     * 设置是否是默认方案     
     * @param Boolean $isDefault     
     * 参数示例：<pre>true</pre>     
     * 此参数必填     */
    public function setIsDefault( $isDefault) {
        $this->isDefault = $isDefault;
    }
    
        	
    private $solutionOwner;
    
        /**
    * @return 方案提出者
    */
        public function getSolutionOwner() {
        return $this->solutionOwner;
    }
    
    /**
     * 设置方案提出者     
     * @param String $solutionOwner     
     * 参数示例：<pre>seller、buyer或platform</pre>     
     * 此参数必填     */
    public function setSolutionOwner( $solutionOwner) {
        $this->solutionOwner = $solutionOwner;
    }
    
        	
    private $content;
    
        /**
    * @return 方案内容
    */
        public function getContent() {
        return $this->content;
    }
    
    /**
     * 设置方案内容     
     * @param String $content     
     * 参数示例：<pre>方案说明，或者你为什么选择此方案</pre>     
     * 此参数必填     */
    public function setContent( $content) {
        $this->content = $content;
    }
    
        	
    private $logisticsFeeBearRole;
    
        /**
    * @return 运费承担方
    */
        public function getLogisticsFeeBearRole() {
        return $this->logisticsFeeBearRole;
    }
    
    /**
     * 设置运费承担方     
     * @param String $logisticsFeeBearRole     
     * 参数示例：<pre>seller、buyer或platform</pre>     
     * 此参数必填     */
    public function setLogisticsFeeBearRole( $logisticsFeeBearRole) {
        $this->logisticsFeeBearRole = $logisticsFeeBearRole;
    }
    
        	
    private $solutionType;
    
        /**
    * @return 方案类型
    */
        public function getSolutionType() {
        return $this->solutionType;
    }
    
    /**
     * 设置方案类型     
     * @param String $solutionType     
     * 参数示例：<pre>refund or return_and_refund</pre>     
     * 此参数必填     */
    public function setSolutionType( $solutionType) {
        $this->solutionType = $solutionType;
    }
    
        	
    private $reachedTime;
    
        /**
    * @return 达成时间
    */
        public function getReachedTime() {
        return $this->reachedTime;
    }
    
    /**
     * 设置达成时间     
     * @param Date $reachedTime     
     * 参数示例：<pre>2016-08-31 23:23:23</pre>     
     * 此参数必填     */
    public function setReachedTime( $reachedTime) {
        $this->reachedTime = $reachedTime;
    }
    
        	
    private $status;
    
        /**
    * @return 方案状态
    */
        public function getStatus() {
        return $this->status;
    }
    
    /**
     * 设置方案状态     
     * @param String $status     
     * 参数示例：<pre>reached</pre>     
     * 此参数必填     */
    public function setStatus( $status) {
        $this->status = $status;
    }
    
        	
    private $reachedType;
    
        /**
    * @return 方案达成类型
    */
        public function getReachedType() {
        return $this->reachedType;
    }
    
    /**
     * 设置方案达成类型     
     * @param String $reachedType     
     * 参数示例：<pre>negotiation_consensus</pre>     
     * 此参数必填     */
    public function setReachedType( $reachedType) {
        $this->reachedType = $reachedType;
    }
    
        	
    private $buyerAcceptTime;
    
        /**
    * @return 买家接受时间
    */
        public function getBuyerAcceptTime() {
        return $this->buyerAcceptTime;
    }
    
    /**
     * 设置买家接受时间     
     * @param Date $buyerAcceptTime     
     * 参数示例：<pre>2016-08-31 23:23:23</pre>     
     * 此参数必填     */
    public function setBuyerAcceptTime( $buyerAcceptTime) {
        $this->buyerAcceptTime = $buyerAcceptTime;
    }
    
        	
    private $sellerAcceptTime;
    
        /**
    * @return 卖家接受时间
    */
        public function getSellerAcceptTime() {
        return $this->sellerAcceptTime;
    }
    
    /**
     * 设置卖家接受时间     
     * @param Date $sellerAcceptTime     
     * 参数示例：<pre>2016-08-31 23:23:23</pre>     
     * 此参数必填     */
    public function setSellerAcceptTime( $sellerAcceptTime) {
        $this->sellerAcceptTime = $sellerAcceptTime;
    }
    
        	
    private $logisticsFeeAmount;
    
        /**
    * @return 退货运费金额(分)
    */
        public function getLogisticsFeeAmount() {
        return $this->logisticsFeeAmount;
    }
    
    /**
     * 设置退货运费金额(分)     
     * @param Long $logisticsFeeAmount     
     * 参数示例：<pre>12</pre>     
     * 此参数必填     */
    public function setLogisticsFeeAmount( $logisticsFeeAmount) {
        $this->logisticsFeeAmount = $logisticsFeeAmount;
    }
    
        	
    private $logisticsFeeAmountCurrency;
    
        /**
    * @return 退货运费币种
    */
        public function getLogisticsFeeAmountCurrency() {
        return $this->logisticsFeeAmountCurrency;
    }
    
    /**
     * 设置退货运费币种     
     * @param String $logisticsFeeAmountCurrency     
     * 参数示例：<pre>USD</pre>     
     * 此参数必填     */
    public function setLogisticsFeeAmountCurrency( $logisticsFeeAmountCurrency) {
        $this->logisticsFeeAmountCurrency = $logisticsFeeAmountCurrency;
    }
    
    	
	private $stdResult;
	
	public function setStdResult($stdResult) {
		$this->stdResult = $stdResult;
					    			    			if (array_key_exists ( "id", $this->stdResult )) {
    				$this->id = $this->stdResult->{"id"};
    			}
    			    		    				    			    			if (array_key_exists ( "gmtCreate", $this->stdResult )) {
    				$this->gmtCreate = $this->stdResult->{"gmtCreate"};
    			}
    			    		    				    			    			if (array_key_exists ( "gmtModified", $this->stdResult )) {
    				$this->gmtModified = $this->stdResult->{"gmtModified"};
    			}
    			    		    				    			    			if (array_key_exists ( "version", $this->stdResult )) {
    				$this->version = $this->stdResult->{"version"};
    			}
    			    		    				    			    			if (array_key_exists ( "buyerAliid", $this->stdResult )) {
    				$this->buyerAliid = $this->stdResult->{"buyerAliid"};
    			}
    			    		    				    			    			if (array_key_exists ( "sellerAliid", $this->stdResult )) {
    				$this->sellerAliid = $this->stdResult->{"sellerAliid"};
    			}
    			    		    				    			    			if (array_key_exists ( "orderId", $this->stdResult )) {
    				$this->orderId = $this->stdResult->{"orderId"};
    			}
    			    		    				    			    			if (array_key_exists ( "issueId", $this->stdResult )) {
    				$this->issueId = $this->stdResult->{"issueId"};
    			}
    			    		    				    			    			if (array_key_exists ( "refundMoney", $this->stdResult )) {
    				$refundMoneyResult=$this->stdResult->{"refundMoney"};
    				$this->refundMoney = new IssueMoney();
    				$this->refundMoney->setStdResult ( $refundMoneyResult);
    			}
    			    		    				    			    			if (array_key_exists ( "refundMoneyPost", $this->stdResult )) {
    				$refundMoneyPostResult=$this->stdResult->{"refundMoneyPost"};
    				$this->refundMoneyPost = new IssueMoney();
    				$this->refundMoneyPost->setStdResult ( $refundMoneyPostResult);
    			}
    			    		    				    			    			if (array_key_exists ( "isDefault", $this->stdResult )) {
    				$this->isDefault = $this->stdResult->{"isDefault"};
    			}
    			    		    				    			    			if (array_key_exists ( "solutionOwner", $this->stdResult )) {
    				$this->solutionOwner = $this->stdResult->{"solutionOwner"};
    			}
    			    		    				    			    			if (array_key_exists ( "content", $this->stdResult )) {
    				$this->content = $this->stdResult->{"content"};
    			}
    			    		    				    			    			if (array_key_exists ( "logisticsFeeBearRole", $this->stdResult )) {
    				$this->logisticsFeeBearRole = $this->stdResult->{"logisticsFeeBearRole"};
    			}
    			    		    				    			    			if (array_key_exists ( "solutionType", $this->stdResult )) {
    				$this->solutionType = $this->stdResult->{"solutionType"};
    			}
    			    		    				    			    			if (array_key_exists ( "reachedTime", $this->stdResult )) {
    				$this->reachedTime = $this->stdResult->{"reachedTime"};
    			}
    			    		    				    			    			if (array_key_exists ( "status", $this->stdResult )) {
    				$this->status = $this->stdResult->{"status"};
    			}
    			    		    				    			    			if (array_key_exists ( "reachedType", $this->stdResult )) {
    				$this->reachedType = $this->stdResult->{"reachedType"};
    			}
    			    		    				    			    			if (array_key_exists ( "buyerAcceptTime", $this->stdResult )) {
    				$this->buyerAcceptTime = $this->stdResult->{"buyerAcceptTime"};
    			}
    			    		    				    			    			if (array_key_exists ( "sellerAcceptTime", $this->stdResult )) {
    				$this->sellerAcceptTime = $this->stdResult->{"sellerAcceptTime"};
    			}
    			    		    				    			    			if (array_key_exists ( "logisticsFeeAmount", $this->stdResult )) {
    				$this->logisticsFeeAmount = $this->stdResult->{"logisticsFeeAmount"};
    			}
    			    		    				    			    			if (array_key_exists ( "logisticsFeeAmountCurrency", $this->stdResult )) {
    				$this->logisticsFeeAmountCurrency = $this->stdResult->{"logisticsFeeAmountCurrency"};
    			}
    			    		    		}
	
	private $arrayResult;
	public function setArrayResult($arrayResult) {
		$this->arrayResult = $arrayResult;
				    		    			if (array_key_exists ( "id", $this->arrayResult )) {
    			$this->id = $arrayResult['id'];
    			}
    		    	    			    		    			if (array_key_exists ( "gmtCreate", $this->arrayResult )) {
    			$this->gmtCreate = $arrayResult['gmtCreate'];
    			}
    		    	    			    		    			if (array_key_exists ( "gmtModified", $this->arrayResult )) {
    			$this->gmtModified = $arrayResult['gmtModified'];
    			}
    		    	    			    		    			if (array_key_exists ( "version", $this->arrayResult )) {
    			$this->version = $arrayResult['version'];
    			}
    		    	    			    		    			if (array_key_exists ( "buyerAliid", $this->arrayResult )) {
    			$this->buyerAliid = $arrayResult['buyerAliid'];
    			}
    		    	    			    		    			if (array_key_exists ( "sellerAliid", $this->arrayResult )) {
    			$this->sellerAliid = $arrayResult['sellerAliid'];
    			}
    		    	    			    		    			if (array_key_exists ( "orderId", $this->arrayResult )) {
    			$this->orderId = $arrayResult['orderId'];
    			}
    		    	    			    		    			if (array_key_exists ( "issueId", $this->arrayResult )) {
    			$this->issueId = $arrayResult['issueId'];
    			}
    		    	    			    		    		if (array_key_exists ( "refundMoney", $this->arrayResult )) {
    		$refundMoneyResult=$arrayResult['refundMoney'];
    			    			$this->refundMoney = new IssueMoney();
    			    			$this->refundMoney->$this->setStdResult ( $refundMoneyResult);
    		}
    		    	    			    		    		if (array_key_exists ( "refundMoneyPost", $this->arrayResult )) {
    		$refundMoneyPostResult=$arrayResult['refundMoneyPost'];
    			    			$this->refundMoneyPost = new IssueMoney();
    			    			$this->refundMoneyPost->$this->setStdResult ( $refundMoneyPostResult);
    		}
    		    	    			    		    			if (array_key_exists ( "isDefault", $this->arrayResult )) {
    			$this->isDefault = $arrayResult['isDefault'];
    			}
    		    	    			    		    			if (array_key_exists ( "solutionOwner", $this->arrayResult )) {
    			$this->solutionOwner = $arrayResult['solutionOwner'];
    			}
    		    	    			    		    			if (array_key_exists ( "content", $this->arrayResult )) {
    			$this->content = $arrayResult['content'];
    			}
    		    	    			    		    			if (array_key_exists ( "logisticsFeeBearRole", $this->arrayResult )) {
    			$this->logisticsFeeBearRole = $arrayResult['logisticsFeeBearRole'];
    			}
    		    	    			    		    			if (array_key_exists ( "solutionType", $this->arrayResult )) {
    			$this->solutionType = $arrayResult['solutionType'];
    			}
    		    	    			    		    			if (array_key_exists ( "reachedTime", $this->arrayResult )) {
    			$this->reachedTime = $arrayResult['reachedTime'];
    			}
    		    	    			    		    			if (array_key_exists ( "status", $this->arrayResult )) {
    			$this->status = $arrayResult['status'];
    			}
    		    	    			    		    			if (array_key_exists ( "reachedType", $this->arrayResult )) {
    			$this->reachedType = $arrayResult['reachedType'];
    			}
    		    	    			    		    			if (array_key_exists ( "buyerAcceptTime", $this->arrayResult )) {
    			$this->buyerAcceptTime = $arrayResult['buyerAcceptTime'];
    			}
    		    	    			    		    			if (array_key_exists ( "sellerAcceptTime", $this->arrayResult )) {
    			$this->sellerAcceptTime = $arrayResult['sellerAcceptTime'];
    			}
    		    	    			    		    			if (array_key_exists ( "logisticsFeeAmount", $this->arrayResult )) {
    			$this->logisticsFeeAmount = $arrayResult['logisticsFeeAmount'];
    			}
    		    	    			    		    			if (array_key_exists ( "logisticsFeeAmountCurrency", $this->arrayResult )) {
    			$this->logisticsFeeAmountCurrency = $arrayResult['logisticsFeeAmountCurrency'];
    			}
    		    	    		}
 
   
}
?>