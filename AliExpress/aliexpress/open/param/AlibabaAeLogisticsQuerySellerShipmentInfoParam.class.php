<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');

class AlibabaAeLogisticsQuerySellerShipmentInfoParam {

        
        /**
    * @return 交易订单号
    */
        public function getTradeOrderId() {
        $tempResult = $this->sdkStdResult["tradeOrderId"];
        return $tempResult;
    }
    
    /**
     * 设置交易订单号     
     * @param Long $tradeOrderId     
     * 参数示例：<pre>8899213123123</pre>     
     * 此参数必填     */
    public function setTradeOrderId( $tradeOrderId) {
        $this->sdkStdResult["tradeOrderId"] = $tradeOrderId;
    }
    
        
        /**
    * @return 子订单序号
    */
        public function getSubTradeOrderIndex() {
        $tempResult = $this->sdkStdResult["subTradeOrderIndex"];
        return $tempResult;
    }
    
    /**
     * 设置子订单序号     
     * @param Integer $subTradeOrderIndex     
     * 参数示例：<pre>子订单需要从1开始</pre>     
     * 此参数必填     */
    public function setSubTradeOrderIndex( $subTradeOrderIndex) {
        $this->sdkStdResult["subTradeOrderIndex"] = $subTradeOrderIndex;
    }
    
        
        /**
    * @return 用户选择的实际发货物流服务（物流服务key：该接口根据api.listLogisticsService列出平台所支持的物流服务 进行获取目前所支持的物流。平台支持物流服务详细一览表详见论坛链接
    */
        public function getServiceName() {
        $tempResult = $this->sdkStdResult["serviceName"];
        return $tempResult;
    }
    
    /**
     * 设置用户选择的实际发货物流服务（物流服务key：该接口根据api.listLogisticsService列出平台所支持的物流服务 进行获取目前所支持的物流。平台支持物流服务详细一览表详见论坛链接     
     * @param String $serviceName     
     * 参数示例：<pre>CAINIAO_STANDARD</pre>     
     * 此参数必填     */
    public function setServiceName( $serviceName) {
        $this->sdkStdResult["serviceName"] = $serviceName;
    }
    
        
        /**
    * @return 运单号
    */
        public function getLogisticsNo() {
        $tempResult = $this->sdkStdResult["logisticsNo"];
        return $tempResult;
    }
    
    /**
     * 设置运单号     
     * @param String $logisticsNo     
     * 参数示例：<pre>LT123456789CN</pre>     
     * 此参数必填     */
    public function setLogisticsNo( $logisticsNo) {
        $this->sdkStdResult["logisticsNo"] = $logisticsNo;
    }
    
        
    private $sdkStdResult=array();
    
    public function getSdkStdResult(){
    	return $this->sdkStdResult;
    }

}
?>