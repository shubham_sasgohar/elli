<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');

class AeopWarehouseOrderQueryPdfRequest extends SDKDomain {

       	
    private $id;
    
        /**
    * @return 物流订单号
    */
        public function getId() {
        return $this->id;
    }
    
    /**
     * 设置物流订单号     
     * @param Long $id     
     * 参数示例：<pre>3000001</pre>     
     * 此参数必填     */
    public function setId( $id) {
        $this->id = $id;
    }
    
        	
    private $internationalLogisticsId;
    
        /**
    * @return 国际运单号
    */
        public function getInternationalLogisticsId() {
        return $this->internationalLogisticsId;
    }
    
    /**
     * 设置国际运单号     
     * @param String $internationalLogisticsId     
     * 参数示例：<pre>UR837927903YP</pre>     
     * 此参数必填     */
    public function setInternationalLogisticsId( $internationalLogisticsId) {
        $this->internationalLogisticsId = $internationalLogisticsId;
    }
    
    	
	private $stdResult;
	
	public function setStdResult($stdResult) {
		$this->stdResult = $stdResult;
					    			    			if (array_key_exists ( "id", $this->stdResult )) {
    				$this->id = $this->stdResult->{"id"};
    			}
    			    		    				    			    			if (array_key_exists ( "internationalLogisticsId", $this->stdResult )) {
    				$this->internationalLogisticsId = $this->stdResult->{"internationalLogisticsId"};
    			}
    			    		    		}
	
	private $arrayResult;
	public function setArrayResult($arrayResult) {
		$this->arrayResult = $arrayResult;
				    		    			if (array_key_exists ( "id", $this->arrayResult )) {
    			$this->id = $arrayResult['id'];
    			}
    		    	    			    		    			if (array_key_exists ( "internationalLogisticsId", $this->arrayResult )) {
    			$this->internationalLogisticsId = $arrayResult['internationalLogisticsId'];
    			}
    		    	    		}
 
   
}
?>