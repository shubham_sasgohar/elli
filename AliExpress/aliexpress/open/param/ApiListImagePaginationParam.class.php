<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');

class ApiListImagePaginationParam {

        
        /**
    * @return 图片存放位置.可选值:allGroup(所有分组), temp(被禁用的图片), subGroup(某一分组), unGroup(非分组). 如果locationType参数值为allGroup,temp,unGroup时，将忽略groupId参数。 如果locationType的参数值为subGroup,须指定groupId参数。
    */
        public function getLocationType() {
        $tempResult = $this->sdkStdResult["locationType"];
        return $tempResult;
    }
    
    /**
     * 设置图片存放位置.可选值:allGroup(所有分组), temp(被禁用的图片), subGroup(某一分组), unGroup(非分组). 如果locationType参数值为allGroup,temp,unGroup时，将忽略groupId参数。 如果locationType的参数值为subGroup,须指定groupId参数。     
     * @param String $locationType     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setLocationType( $locationType) {
        $this->sdkStdResult["locationType"] = $locationType;
    }
    
        
        /**
    * @return 图片组id
    */
        public function getGroupId() {
        $tempResult = $this->sdkStdResult["groupId"];
        return $tempResult;
    }
    
    /**
     * 设置图片组id     
     * @param String $groupId     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setGroupId( $groupId) {
        $this->sdkStdResult["groupId"] = $groupId;
    }
    
        
        /**
    * @return 当前页码
    */
        public function getCurrentPage() {
        $tempResult = $this->sdkStdResult["currentPage"];
        return $tempResult;
    }
    
    /**
     * 设置当前页码     
     * @param Integer $currentPage     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setCurrentPage( $currentPage) {
        $this->sdkStdResult["currentPage"] = $currentPage;
    }
    
        
        /**
    * @return 默认18个，最大值 50
    */
        public function getPageSize() {
        $tempResult = $this->sdkStdResult["pageSize"];
        return $tempResult;
    }
    
    /**
     * 设置默认18个，最大值 50     
     * @param Integer $pageSize     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setPageSize( $pageSize) {
        $this->sdkStdResult["pageSize"] = $pageSize;
    }
    
        
    private $sdkStdResult=array();
    
    public function getSdkStdResult(){
    	return $this->sdkStdResult;
    }

}
?>