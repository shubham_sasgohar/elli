<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');
include_once ('aliexpress/open/param/IssueMoney.class.php');
include_once ('aliexpress/open/param/IssueMoney.class.php');
include_once ('aliexpress/open/param/IssueRefundSuggestionDTO.class.php');

class IssueAPIIssueProcessDTO extends SDKDomain {

       	
    private $id;
    
        /**
    * @return processId
    */
        public function getId() {
        return $this->id;
    }
    
    /**
     * 设置processId     
     * @param Long $id     
     * 参数示例：<pre>680*************804</pre>     
     * 此参数必填     */
    public function setId( $id) {
        $this->id = $id;
    }
    
        	
    private $gmtCreate;
    
        /**
    * @return Process creation time
    */
        public function getGmtCreate() {
        return $this->gmtCreate;
    }
    
    /**
     * 设置Process creation time     
     * @param Date $gmtCreate     
     * 参数示例：<pre>20150714020749000-0700</pre>     
     * 此参数必填     */
    public function setGmtCreate( $gmtCreate) {
        $this->gmtCreate = $gmtCreate;
    }
    
        	
    private $gmtModified;
    
        /**
    * @return Process modified time
    */
        public function getGmtModified() {
        return $this->gmtModified;
    }
    
    /**
     * 设置Process modified time     
     * @param Date $gmtModified     
     * 参数示例：<pre>20150714020749000-0700</pre>     
     * 此参数必填     */
    public function setGmtModified( $gmtModified) {
        $this->gmtModified = $gmtModified;
    }
    
        	
    private $issueId;
    
        /**
    * @return issueId
    */
        public function getIssueId() {
        return $this->issueId;
    }
    
    /**
     * 设置issueId     
     * @param Long $issueId     
     * 参数示例：<pre>680*************804</pre>     
     * 此参数必填     */
    public function setIssueId( $issueId) {
        $this->issueId = $issueId;
    }
    
        	
    private $reason;
    
        /**
    * @return Issue reason
    */
        public function getReason() {
        return $this->reason;
    }
    
    /**
     * 设置Issue reason     
     * @param String $reason     
     * 参数示例：<pre>Color_not_as_described@3rdIssueReason</pre>     
     * 此参数必填     */
    public function setReason( $reason) {
        $this->reason = $reason;
    }
    
        	
    private $content;
    
        /**
    * @return Issue description
    */
        public function getContent() {
        return $this->content;
    }
    
    /**
     * 设置Issue description     
     * @param String $content     
     * 参数示例：<pre>The produit don't turn one.</pre>     
     * 此参数必填     */
    public function setContent( $content) {
        $this->content = $content;
    }
    
        	
    private $refundAmount;
    
        /**
    * @return Refund amount currency
    */
        public function getRefundAmount() {
        return $this->refundAmount;
    }
    
    /**
     * 设置Refund amount currency     
     * @param IssueMoney $refundAmount     
     * 参数示例：<pre>{"amount":0.1,"cent":10,"currencyCode":"RUB","centFactor":100,"currency":{"defaultFractionDigits":2,"currencyCode":"RUB","symbol":"RUB"}</pre>     
     * 此参数必填     */
    public function setRefundAmount(IssueMoney $refundAmount) {
        $this->refundAmount = $refundAmount;
    }
    
        	
    private $refundConfirmAmount;
    
        /**
    * @return Refund amount USD
    */
        public function getRefundConfirmAmount() {
        return $this->refundConfirmAmount;
    }
    
    /**
     * 设置Refund amount USD     
     * @param IssueMoney $refundConfirmAmount     
     * 参数示例：<pre>{"amount":0.01,"cent":1,"currencyCode":"USD","centFactor":100,"currency":{"defaultFractionDigits":2,"currencyCode":"USD","symbol":"$"}</pre>     
     * 此参数必填     */
    public function setRefundConfirmAmount(IssueMoney $refundConfirmAmount) {
        $this->refundConfirmAmount = $refundConfirmAmount;
    }
    
        	
    private $actionType;
    
        /**
    * @return actionType
    */
        public function getActionType() {
        return $this->actionType;
    }
    
    /**
     * 设置actionType     
     * @param String $actionType     
     * 参数示例：<pre>seller_accept</pre>     
     * 此参数必填     */
    public function setActionType( $actionType) {
        $this->actionType = $actionType;
    }
    
        	
    private $submitMemberType;
    
        /**
    * @return submitMemberType
(seller ;
buyer;
system )
    */
        public function getSubmitMemberType() {
        return $this->submitMemberType;
    }
    
    /**
     * 设置submitMemberType
(seller ;
buyer;
system )     
     * @param String $submitMemberType     
     * 参数示例：<pre>seller</pre>     
     * 此参数必填     */
    public function setSubmitMemberType( $submitMemberType) {
        $this->submitMemberType = $submitMemberType;
    }
    
        	
    private $attachments;
    
        /**
    * @return attachments
    */
        public function getAttachments() {
        return $this->attachments;
    }
    
    /**
     * 设置attachments     
     * @param array include @see String[] $attachments     
     * 参数示例：<pre>["http://g02.a.alicdn.com/kf/UT8B.pjXtxbXXcUQpbXm.png"]}]</pre>     
     * 此参数必填     */
    public function setAttachments( $attachments) {
        $this->attachments = $attachments;
    }
    
        	
    private $isReceivedGoods;
    
        /**
    * @return isReceivedGoods
Y
N
    */
        public function getIsReceivedGoods() {
        return $this->isReceivedGoods;
    }
    
    /**
     * 设置isReceivedGoods
Y
N     
     * @param String $isReceivedGoods     
     * 参数示例：<pre>Y</pre>     
     * 此参数必填     */
    public function setIsReceivedGoods( $isReceivedGoods) {
        $this->isReceivedGoods = $isReceivedGoods;
    }
    
        	
    private $videos;
    
        /**
    * @return videos
    */
        public function getVideos() {
        return $this->videos;
    }
    
    /**
     * 设置videos     
     * @param array include @see String[] $videos     
     * 参数示例：<pre>["http://cloud.video.taobao.com/play/u/133146836577/p/1/e/1/t/1/d/hd/fv/27046845.swf"]}]</pre>     
     * 此参数必填     */
    public function setVideos( $videos) {
        $this->videos = $videos;
    }
    
        	
    private $issueRefundSuggestionList;
    
        /**
    * @return issueRefundSuggestionList
    */
        public function getIssueRefundSuggestionList() {
        return $this->issueRefundSuggestionList;
    }
    
    /**
     * 设置issueRefundSuggestionList     
     * @param array include @see IssueRefundSuggestionDTO[] $issueRefundSuggestionList     
     * 参数示例：<pre>{"isDefault":true,"issueMoney":{"amount":74.47,"cent":7447,"centFactor":100,"currency":{"currencyCode":"RUB","symbol":"RUB"},"currencyCode":"RUB"},"issueMoneyPost":{"amount":1.42,"cent":142,"centFactor":100,"currency":{"currencyCode":"USD","symbol":"$"},"currencyCode":"USD"},"issueRefundType":"full_amount_refund","issueReturnGoods":false}</pre>     
     * 此参数必填     */
    public function setIssueRefundSuggestionList(IssueRefundSuggestionDTO $issueRefundSuggestionList) {
        $this->issueRefundSuggestionList = $issueRefundSuggestionList;
    }
    
    	
	private $stdResult;
	
	public function setStdResult($stdResult) {
		$this->stdResult = $stdResult;
					    			    			if (array_key_exists ( "id", $this->stdResult )) {
    				$this->id = $this->stdResult->{"id"};
    			}
    			    		    				    			    			if (array_key_exists ( "gmtCreate", $this->stdResult )) {
    				$this->gmtCreate = $this->stdResult->{"gmtCreate"};
    			}
    			    		    				    			    			if (array_key_exists ( "gmtModified", $this->stdResult )) {
    				$this->gmtModified = $this->stdResult->{"gmtModified"};
    			}
    			    		    				    			    			if (array_key_exists ( "issueId", $this->stdResult )) {
    				$this->issueId = $this->stdResult->{"issueId"};
    			}
    			    		    				    			    			if (array_key_exists ( "reason", $this->stdResult )) {
    				$this->reason = $this->stdResult->{"reason"};
    			}
    			    		    				    			    			if (array_key_exists ( "content", $this->stdResult )) {
    				$this->content = $this->stdResult->{"content"};
    			}
    			    		    				    			    			if (array_key_exists ( "refundAmount", $this->stdResult )) {
    				$refundAmountResult=$this->stdResult->{"refundAmount"};
    				$this->refundAmount = new IssueMoney();
    				$this->refundAmount->setStdResult ( $refundAmountResult);
    			}
    			    		    				    			    			if (array_key_exists ( "refundConfirmAmount", $this->stdResult )) {
    				$refundConfirmAmountResult=$this->stdResult->{"refundConfirmAmount"};
    				$this->refundConfirmAmount = new IssueMoney();
    				$this->refundConfirmAmount->setStdResult ( $refundConfirmAmountResult);
    			}
    			    		    				    			    			if (array_key_exists ( "actionType", $this->stdResult )) {
    				$this->actionType = $this->stdResult->{"actionType"};
    			}
    			    		    				    			    			if (array_key_exists ( "submitMemberType", $this->stdResult )) {
    				$this->submitMemberType = $this->stdResult->{"submitMemberType"};
    			}
    			    		    				    			    			if (array_key_exists ( "attachments", $this->stdResult )) {
    				$this->attachments = $this->stdResult->{"attachments"};
    			}
    			    		    				    			    			if (array_key_exists ( "isReceivedGoods", $this->stdResult )) {
    				$this->isReceivedGoods = $this->stdResult->{"isReceivedGoods"};
    			}
    			    		    				    			    			if (array_key_exists ( "videos", $this->stdResult )) {
    				$this->videos = $this->stdResult->{"videos"};
    			}
    			    		    				    			    			if (array_key_exists ( "issueRefundSuggestionList", $this->stdResult )) {
    			$issueRefundSuggestionListResult=$this->stdResult->{"issueRefundSuggestionList"};
    				$object = json_decode ( json_encode ( $issueRefundSuggestionListResult ), true );
					$this->issueRefundSuggestionList = array ();
					for($i = 0; $i < count ( $object ); $i ++) {
						$arrayobject = new ArrayObject ( $object [$i] );
						$IssueRefundSuggestionDTOResult=new IssueRefundSuggestionDTO();
						$IssueRefundSuggestionDTOResult->setArrayResult($arrayobject );
						$this->issueRefundSuggestionList [$i] = $IssueRefundSuggestionDTOResult;
					}
    			}
    			    		    		}
	
	private $arrayResult;
	public function setArrayResult($arrayResult) {
		$this->arrayResult = $arrayResult;
				    		    			if (array_key_exists ( "id", $this->arrayResult )) {
    			$this->id = $arrayResult['id'];
    			}
    		    	    			    		    			if (array_key_exists ( "gmtCreate", $this->arrayResult )) {
    			$this->gmtCreate = $arrayResult['gmtCreate'];
    			}
    		    	    			    		    			if (array_key_exists ( "gmtModified", $this->arrayResult )) {
    			$this->gmtModified = $arrayResult['gmtModified'];
    			}
    		    	    			    		    			if (array_key_exists ( "issueId", $this->arrayResult )) {
    			$this->issueId = $arrayResult['issueId'];
    			}
    		    	    			    		    			if (array_key_exists ( "reason", $this->arrayResult )) {
    			$this->reason = $arrayResult['reason'];
    			}
    		    	    			    		    			if (array_key_exists ( "content", $this->arrayResult )) {
    			$this->content = $arrayResult['content'];
    			}
    		    	    			    		    		if (array_key_exists ( "refundAmount", $this->arrayResult )) {
    		$refundAmountResult=$arrayResult['refundAmount'];
    			    			$this->refundAmount = new IssueMoney();
    			    			$this->refundAmount->$this->setStdResult ( $refundAmountResult);
    		}
    		    	    			    		    		if (array_key_exists ( "refundConfirmAmount", $this->arrayResult )) {
    		$refundConfirmAmountResult=$arrayResult['refundConfirmAmount'];
    			    			$this->refundConfirmAmount = new IssueMoney();
    			    			$this->refundConfirmAmount->$this->setStdResult ( $refundConfirmAmountResult);
    		}
    		    	    			    		    			if (array_key_exists ( "actionType", $this->arrayResult )) {
    			$this->actionType = $arrayResult['actionType'];
    			}
    		    	    			    		    			if (array_key_exists ( "submitMemberType", $this->arrayResult )) {
    			$this->submitMemberType = $arrayResult['submitMemberType'];
    			}
    		    	    			    		    			if (array_key_exists ( "attachments", $this->arrayResult )) {
    			$this->attachments = $arrayResult['attachments'];
    			}
    		    	    			    		    			if (array_key_exists ( "isReceivedGoods", $this->arrayResult )) {
    			$this->isReceivedGoods = $arrayResult['isReceivedGoods'];
    			}
    		    	    			    		    			if (array_key_exists ( "videos", $this->arrayResult )) {
    			$this->videos = $arrayResult['videos'];
    			}
    		    	    			    		    		if (array_key_exists ( "issueRefundSuggestionList", $this->arrayResult )) {
    		$issueRefundSuggestionListResult=$arrayResult['issueRefundSuggestionList'];
    			$this->issueRefundSuggestionList = IssueRefundSuggestionDTO();
    			$this->issueRefundSuggestionList->$this->setStdResult ( $issueRefundSuggestionListResult);
    		}
    		    	    		}
 
   
}
?>