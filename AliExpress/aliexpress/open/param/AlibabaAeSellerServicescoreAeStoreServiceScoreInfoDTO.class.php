<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');
include_once ('aliexpress/open/param/AlibabaAeSellerServicescoreAeServiceScoreInfoDTO.class.php');

class AlibabaAeSellerServicescoreAeStoreServiceScoreInfoDTO extends SDKDomain {

       	
    private $primOprPcateLv2Id;
    
        /**
    * @return 主赚二级类目id
    */
        public function getPrimOprPcateLv2Id() {
        return $this->primOprPcateLv2Id;
    }
    
    /**
     * 设置主赚二级类目id     
     * @param Long $primOprPcateLv2Id     
     * 参数示例：<pre>311</pre>     
     * 此参数必填     */
    public function setPrimOprPcateLv2Id( $primOprPcateLv2Id) {
        $this->primOprPcateLv2Id = $primOprPcateLv2Id;
    }
    
        	
    private $primOprPcateLv2Name;
    
        /**
    * @return 主赚二级类目名称
    */
        public function getPrimOprPcateLv2Name() {
        return $this->primOprPcateLv2Name;
    }
    
    /**
     * 设置主赚二级类目名称     
     * @param String $primOprPcateLv2Name     
     * 参数示例：<pre>儿童服装（2岁以上）</pre>     
     * 此参数必填     */
    public function setPrimOprPcateLv2Name( $primOprPcateLv2Name) {
        $this->primOprPcateLv2Name = $primOprPcateLv2Name;
    }
    
        	
    private $serviceScoreInfoDTO;
    
        /**
    * @return 
    */
        public function getServiceScoreInfoDTO() {
        return $this->serviceScoreInfoDTO;
    }
    
    /**
     * 设置     
     * @param AlibabaAeSellerServicescoreAeServiceScoreInfoDTO $serviceScoreInfoDTO     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setServiceScoreInfoDTO(AlibabaAeSellerServicescoreAeServiceScoreInfoDTO $serviceScoreInfoDTO) {
        $this->serviceScoreInfoDTO = $serviceScoreInfoDTO;
    }
    
    	
	private $stdResult;
	
	public function setStdResult($stdResult) {
		$this->stdResult = $stdResult;
					    			    			if (array_key_exists ( "primOprPcateLv2Id", $this->stdResult )) {
    				$this->primOprPcateLv2Id = $this->stdResult->{"primOprPcateLv2Id"};
    			}
    			    		    				    			    			if (array_key_exists ( "primOprPcateLv2Name", $this->stdResult )) {
    				$this->primOprPcateLv2Name = $this->stdResult->{"primOprPcateLv2Name"};
    			}
    			    		    				    			    			if (array_key_exists ( "serviceScoreInfoDTO", $this->stdResult )) {
    				$serviceScoreInfoDTOResult=$this->stdResult->{"serviceScoreInfoDTO"};
    				$this->serviceScoreInfoDTO = new AlibabaAeSellerServicescoreAeServiceScoreInfoDTO();
    				$this->serviceScoreInfoDTO->setStdResult ( $serviceScoreInfoDTOResult);
    			}
    			    		    		}
	
	private $arrayResult;
	public function setArrayResult($arrayResult) {
		$this->arrayResult = $arrayResult;
				    		    			if (array_key_exists ( "primOprPcateLv2Id", $this->arrayResult )) {
    			$this->primOprPcateLv2Id = $arrayResult['primOprPcateLv2Id'];
    			}
    		    	    			    		    			if (array_key_exists ( "primOprPcateLv2Name", $this->arrayResult )) {
    			$this->primOprPcateLv2Name = $arrayResult['primOprPcateLv2Name'];
    			}
    		    	    			    		    		if (array_key_exists ( "serviceScoreInfoDTO", $this->arrayResult )) {
    		$serviceScoreInfoDTOResult=$arrayResult['serviceScoreInfoDTO'];
    			    			$this->serviceScoreInfoDTO = new AlibabaAeSellerServicescoreAeServiceScoreInfoDTO();
    			    			$this->serviceScoreInfoDTO->$this->setStdResult ( $serviceScoreInfoDTOResult);
    		}
    		    	    		}
 
   
}
?>