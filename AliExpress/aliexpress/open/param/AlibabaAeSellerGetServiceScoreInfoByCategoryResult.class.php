<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');
include_once ('aliexpress/open/param/AlibabaAeSellerServicescoreAeCategoryServiceScoreQueryResponse.class.php');

class AlibabaAeSellerGetServiceScoreInfoByCategoryResult {

        	
    private $response;
    
        /**
    * @return 
    */
        public function getResponse() {
        return $this->response;
    }
    
    /**
     * 设置     
     * @param AlibabaAeSellerServicescoreAeCategoryServiceScoreQueryResponse $response     
          
     * 此参数必填     */
    public function setResponse(AlibabaAeSellerServicescoreAeCategoryServiceScoreQueryResponse $response) {
        $this->response = $response;
    }
    
    	
	private $stdResult;
	
	public function setStdResult($stdResult) {
		$this->stdResult = $stdResult;
					    			    			if (array_key_exists ( "response", $this->stdResult )) {
    				$responseResult=$this->stdResult->{"response"};
    				$this->response = new AlibabaAeSellerServicescoreAeCategoryServiceScoreQueryResponse();
    				$this->response->setStdResult ( $responseResult);
    			}
    			    		    		}
	
	private $arrayResult;
	public function setArrayResult($arrayResult) {
		$this->arrayResult = $arrayResult;
				    		    		if (array_key_exists ( "response", $this->arrayResult )) {
    		$responseResult=$arrayResult['response'];
    			    			$this->response = new AlibabaAeSellerServicescoreAeCategoryServiceScoreQueryResponse();
    			    			$this->response->setStdResult ( $responseResult);
    		}
    		    	    		}

}
?>