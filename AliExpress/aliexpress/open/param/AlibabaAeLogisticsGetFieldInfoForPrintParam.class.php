<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');
include_once ('aliexpress/open/param/AeopWarehouseOrderQueryPdfRequest.class.php');

class AlibabaAeLogisticsGetFieldInfoForPrintParam {

        
        /**
    * @return 运单号面单PDF打印查询参数
    */
        public function getRequest() {
        $tempResult = $this->sdkStdResult["request"];
        return $tempResult;
    }
    
    /**
     * 设置运单号面单PDF打印查询参数     
     * @param AeopWarehouseOrderQueryPdfRequest $request     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setRequest(AeopWarehouseOrderQueryPdfRequest $request) {
        $this->sdkStdResult["request"] = $request;
    }
    
        
    private $sdkStdResult=array();
    
    public function getSdkStdResult(){
    	return $this->sdkStdResult;
    }

}
?>