<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');

class AlibabaAeSellerTradeSellerSendGoodsFinishParam {

        
        /**
    * @return 
    */
        public function getOrderId() {
        $tempResult = $this->sdkStdResult["orderId"];
        return $tempResult;
    }
    
    /**
     * 设置     
     * @param Long $orderId     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setOrderId( $orderId) {
        $this->sdkStdResult["orderId"] = $orderId;
    }
    
        
        /**
    * @return 
    */
        public function getSellerOperationAliid() {
        $tempResult = $this->sdkStdResult["sellerOperationAliid"];
        return $tempResult;
    }
    
    /**
     * 设置     
     * @param Long $sellerOperationAliid     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setSellerOperationAliid( $sellerOperationAliid) {
        $this->sdkStdResult["sellerOperationAliid"] = $sellerOperationAliid;
    }
    
        
    private $sdkStdResult=array();
    
    public function getSdkStdResult(){
    	return $this->sdkStdResult;
    }

}
?>