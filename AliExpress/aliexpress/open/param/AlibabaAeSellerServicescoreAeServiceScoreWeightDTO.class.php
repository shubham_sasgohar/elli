<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');

class AlibabaAeSellerServicescoreAeServiceScoreWeightDTO extends SDKDomain {

       	
    private $notSellWeight;
    
        /**
    * @return 拍而不卖率权重
    */
        public function getNotSellWeight() {
        return $this->notSellWeight;
    }
    
    /**
     * 设置拍而不卖率权重     
     * @param Integer $notSellWeight     
     * 参数示例：<pre>10</pre>     
     * 此参数必填     */
    public function setNotSellWeight( $notSellWeight) {
        $this->notSellWeight = $notSellWeight;
    }
    
        	
    private $nrIssueWeight;
    
        /**
    * @return NR纠纷提起率权重
    */
        public function getNrIssueWeight() {
        return $this->nrIssueWeight;
    }
    
    /**
     * 设置NR纠纷提起率权重     
     * @param Integer $nrIssueWeight     
     * 参数示例：<pre>15</pre>     
     * 此参数必填     */
    public function setNrIssueWeight( $nrIssueWeight) {
        $this->nrIssueWeight = $nrIssueWeight;
    }
    
        	
    private $snadIssueWeight;
    
        /**
    * @return SNAD纠纷提起率权重
    */
        public function getSnadIssueWeight() {
        return $this->snadIssueWeight;
    }
    
    /**
     * 设置SNAD纠纷提起率权重     
     * @param Integer $snadIssueWeight     
     * 参数示例：<pre>15</pre>     
     * 此参数必填     */
    public function setSnadIssueWeight( $snadIssueWeight) {
        $this->snadIssueWeight = $snadIssueWeight;
    }
    
        	
    private $dsrGoodDescriptionWeight;
    
        /**
    * @return DSR商品描述权重
    */
        public function getDsrGoodDescriptionWeight() {
        return $this->dsrGoodDescriptionWeight;
    }
    
    /**
     * 设置DSR商品描述权重     
     * @param Integer $dsrGoodDescriptionWeight     
     * 参数示例：<pre>30</pre>     
     * 此参数必填     */
    public function setDsrGoodDescriptionWeight( $dsrGoodDescriptionWeight) {
        $this->dsrGoodDescriptionWeight = $dsrGoodDescriptionWeight;
    }
    
        	
    private $dsrCommunicatWeight;
    
        /**
    * @return DSR卖家服务权重
    */
        public function getDsrCommunicatWeight() {
        return $this->dsrCommunicatWeight;
    }
    
    /**
     * 设置DSR卖家服务权重     
     * @param Integer $dsrCommunicatWeight     
     * 参数示例：<pre>15</pre>     
     * 此参数必填     */
    public function setDsrCommunicatWeight( $dsrCommunicatWeight) {
        $this->dsrCommunicatWeight = $dsrCommunicatWeight;
    }
    
        	
    private $dsrLogisticsWeight;
    
        /**
    * @return DSR物流权重
    */
        public function getDsrLogisticsWeight() {
        return $this->dsrLogisticsWeight;
    }
    
    /**
     * 设置DSR物流权重     
     * @param Integer $dsrLogisticsWeight     
     * 参数示例：<pre>15</pre>     
     * 此参数必填     */
    public function setDsrLogisticsWeight( $dsrLogisticsWeight) {
        $this->dsrLogisticsWeight = $dsrLogisticsWeight;
    }
    
    	
	private $stdResult;
	
	public function setStdResult($stdResult) {
		$this->stdResult = $stdResult;
					    			    			if (array_key_exists ( "notSellWeight", $this->stdResult )) {
    				$this->notSellWeight = $this->stdResult->{"notSellWeight"};
    			}
    			    		    				    			    			if (array_key_exists ( "nrIssueWeight", $this->stdResult )) {
    				$this->nrIssueWeight = $this->stdResult->{"nrIssueWeight"};
    			}
    			    		    				    			    			if (array_key_exists ( "snadIssueWeight", $this->stdResult )) {
    				$this->snadIssueWeight = $this->stdResult->{"snadIssueWeight"};
    			}
    			    		    				    			    			if (array_key_exists ( "dsrGoodDescriptionWeight", $this->stdResult )) {
    				$this->dsrGoodDescriptionWeight = $this->stdResult->{"dsrGoodDescriptionWeight"};
    			}
    			    		    				    			    			if (array_key_exists ( "dsrCommunicatWeight", $this->stdResult )) {
    				$this->dsrCommunicatWeight = $this->stdResult->{"dsrCommunicatWeight"};
    			}
    			    		    				    			    			if (array_key_exists ( "dsrLogisticsWeight", $this->stdResult )) {
    				$this->dsrLogisticsWeight = $this->stdResult->{"dsrLogisticsWeight"};
    			}
    			    		    		}
	
	private $arrayResult;
	public function setArrayResult($arrayResult) {
		$this->arrayResult = $arrayResult;
				    		    			if (array_key_exists ( "notSellWeight", $this->arrayResult )) {
    			$this->notSellWeight = $arrayResult['notSellWeight'];
    			}
    		    	    			    		    			if (array_key_exists ( "nrIssueWeight", $this->arrayResult )) {
    			$this->nrIssueWeight = $arrayResult['nrIssueWeight'];
    			}
    		    	    			    		    			if (array_key_exists ( "snadIssueWeight", $this->arrayResult )) {
    			$this->snadIssueWeight = $arrayResult['snadIssueWeight'];
    			}
    		    	    			    		    			if (array_key_exists ( "dsrGoodDescriptionWeight", $this->arrayResult )) {
    			$this->dsrGoodDescriptionWeight = $arrayResult['dsrGoodDescriptionWeight'];
    			}
    		    	    			    		    			if (array_key_exists ( "dsrCommunicatWeight", $this->arrayResult )) {
    			$this->dsrCommunicatWeight = $arrayResult['dsrCommunicatWeight'];
    			}
    		    	    			    		    			if (array_key_exists ( "dsrLogisticsWeight", $this->arrayResult )) {
    			$this->dsrLogisticsWeight = $arrayResult['dsrLogisticsWeight'];
    			}
    		    	    		}
 
   
}
?>