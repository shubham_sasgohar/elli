<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');
include_once ('aliexpress/open/param/IssueAPIIssueProcessDTO.class.php');
include_once ('aliexpress/open/param/IssueMoney.class.php');
include_once ('aliexpress/open/param/IssueMoney.class.php');

class IssueAPIIssueDTO extends SDKDomain {

       	
    private $id;
    
        /**
    * @return IssueID
    */
        public function getId() {
        return $this->id;
    }
    
    /**
     * 设置IssueID     
     * @param Long $id     
     * 参数示例：<pre>680*************804</pre>     
     * 此参数必填     */
    public function setId( $id) {
        $this->id = $id;
    }
    
        	
    private $gmtCreate;
    
        /**
    * @return Create Issue time
    */
        public function getGmtCreate() {
        return $this->gmtCreate;
    }
    
    /**
     * 设置Create Issue time     
     * @param Date $gmtCreate     
     * 参数示例：<pre>20150714020749000-0700</pre>     
     * 此参数必填     */
    public function setGmtCreate( $gmtCreate) {
        $this->gmtCreate = $gmtCreate;
    }
    
        	
    private $gmtModified;
    
        /**
    * @return Modify Issue time
    */
        public function getGmtModified() {
        return $this->gmtModified;
    }
    
    /**
     * 设置Modify Issue time     
     * @param Date $gmtModified     
     * 参数示例：<pre>20150714021033000-0700</pre>     
     * 此参数必填     */
    public function setGmtModified( $gmtModified) {
        $this->gmtModified = $gmtModified;
    }
    
        	
    private $orderId;
    
        /**
    * @return orderId
    */
        public function getOrderId() {
        return $this->orderId;
    }
    
    /**
     * 设置orderId     
     * @param Long $orderId     
     * 参数示例：<pre>680*************804</pre>     
     * 此参数必填     */
    public function setOrderId( $orderId) {
        $this->orderId = $orderId;
    }
    
        	
    private $parentOrderId;
    
        /**
    * @return parentOrderId
    */
        public function getParentOrderId() {
        return $this->parentOrderId;
    }
    
    /**
     * 设置parentOrderId     
     * @param Long $parentOrderId     
     * 参数示例：<pre>0</pre>     
     * 此参数必填     */
    public function setParentOrderId( $parentOrderId) {
        $this->parentOrderId = $parentOrderId;
    }
    
        	
    private $issueStatus;
    
        /**
    * @return IssueStatus
(WAIT_SELLER_CONFIRM_REFUND  ;
SELLER_REFUSE_REFUND  ;
ACCEPTISSUE  ;
WAIT_BUYER_SEND_GOODS  ;
WAIT_SELLER_RECEIVE_GOODS  ;
ARBITRATING  ;
SELLER_RESPONSE_ISSUE_TIMEOUT  )
    */
        public function getIssueStatus() {
        return $this->issueStatus;
    }
    
    /**
     * 设置IssueStatus
(WAIT_SELLER_CONFIRM_REFUND  ;
SELLER_REFUSE_REFUND  ;
ACCEPTISSUE  ;
WAIT_BUYER_SEND_GOODS  ;
WAIT_SELLER_RECEIVE_GOODS  ;
ARBITRATING  ;
SELLER_RESPONSE_ISSUE_TIMEOUT  )     
     * @param String $issueStatus     
     * 参数示例：<pre>WAIT_SELLER_CONFIRM_REFUND</pre>     
     * 此参数必填     */
    public function setIssueStatus( $issueStatus) {
        $this->issueStatus = $issueStatus;
    }
    
        	
    private $issueProcessDTOs;
    
        /**
    * @return Dispute resolution process, only detail interface display.
    */
        public function getIssueProcessDTOs() {
        return $this->issueProcessDTOs;
    }
    
    /**
     * 设置Dispute resolution process, only detail interface display.     
     * @param array include @see IssueAPIIssueProcessDTO[] $issueProcessDTOs     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setIssueProcessDTOs(IssueAPIIssueProcessDTO $issueProcessDTOs) {
        $this->issueProcessDTOs = $issueProcessDTOs;
    }
    
        	
    private $limitRefundAmount;
    
        /**
    * @return Maximum refund amount USD.
    */
        public function getLimitRefundAmount() {
        return $this->limitRefundAmount;
    }
    
    /**
     * 设置Maximum refund amount USD.     
     * @param IssueMoney $limitRefundAmount     
     * 参数示例：<pre>{"amount":0.01,"cent":1,"currencyCode":"USD","centFactor":100,"currency":{"defaultFractionDigits":2,"currencyCode":"USD","symbol":"$"}</pre>     
     * 此参数必填     */
    public function setLimitRefundAmount(IssueMoney $limitRefundAmount) {
        $this->limitRefundAmount = $limitRefundAmount;
    }
    
        	
    private $limitRefundLocalAmount;
    
        /**
    * @return Maximum refund amount in local currency
    */
        public function getLimitRefundLocalAmount() {
        return $this->limitRefundLocalAmount;
    }
    
    /**
     * 设置Maximum refund amount in local currency     
     * @param IssueMoney $limitRefundLocalAmount     
     * 参数示例：<pre>{"amount":0.1,"cent":10,"currencyCode":"RUB","centFactor":100,"currency":{"defaultFractionDigits":2,"currencyCode":"RUB","symbol":"RUB"}</pre>     
     * 此参数必填     */
    public function setLimitRefundLocalAmount(IssueMoney $limitRefundLocalAmount) {
        $this->limitRefundLocalAmount = $limitRefundLocalAmount;
    }
    
        	
    private $reasonChinese;
    
        /**
    * @return Issue reason Chinese description
    */
        public function getReasonChinese() {
        return $this->reasonChinese;
    }
    
    /**
     * 设置Issue reason Chinese description     
     * @param String $reasonChinese     
     * 参数示例：<pre>T
he amount ordered doesn't match</pre>     
     * 此参数必填     */
    public function setReasonChinese( $reasonChinese) {
        $this->reasonChinese = $reasonChinese;
    }
    
        	
    private $reasonEnglish;
    
        /**
    * @return Issue reason description.
    */
        public function getReasonEnglish() {
        return $this->reasonEnglish;
    }
    
    /**
     * 设置Issue reason description.     
     * @param String $reasonEnglish     
     * 参数示例：<pre>Quantity shortage</pre>     
     * 此参数必填     */
    public function setReasonEnglish( $reasonEnglish) {
        $this->reasonEnglish = $reasonEnglish;
    }
    
    	
	private $stdResult;
	
	public function setStdResult($stdResult) {
		$this->stdResult = $stdResult;
					    			    			if (array_key_exists ( "id", $this->stdResult )) {
    				$this->id = $this->stdResult->{"id"};
    			}
    			    		    				    			    			if (array_key_exists ( "gmtCreate", $this->stdResult )) {
    				$this->gmtCreate = $this->stdResult->{"gmtCreate"};
    			}
    			    		    				    			    			if (array_key_exists ( "gmtModified", $this->stdResult )) {
    				$this->gmtModified = $this->stdResult->{"gmtModified"};
    			}
    			    		    				    			    			if (array_key_exists ( "orderId", $this->stdResult )) {
    				$this->orderId = $this->stdResult->{"orderId"};
    			}
    			    		    				    			    			if (array_key_exists ( "parentOrderId", $this->stdResult )) {
    				$this->parentOrderId = $this->stdResult->{"parentOrderId"};
    			}
    			    		    				    			    			if (array_key_exists ( "issueStatus", $this->stdResult )) {
    				$this->issueStatus = $this->stdResult->{"issueStatus"};
    			}
    			    		    				    			    			if (array_key_exists ( "issueProcessDTOs", $this->stdResult )) {
    			$issueProcessDTOsResult=$this->stdResult->{"issueProcessDTOs"};
    				$object = json_decode ( json_encode ( $issueProcessDTOsResult ), true );
					$this->issueProcessDTOs = array ();
					for($i = 0; $i < count ( $object ); $i ++) {
						$arrayobject = new ArrayObject ( $object [$i] );
						$IssueAPIIssueProcessDTOResult=new IssueAPIIssueProcessDTO();
						$IssueAPIIssueProcessDTOResult->setArrayResult($arrayobject );
						$this->issueProcessDTOs [$i] = $IssueAPIIssueProcessDTOResult;
					}
    			}
    			    		    				    			    			if (array_key_exists ( "limitRefundAmount", $this->stdResult )) {
    				$limitRefundAmountResult=$this->stdResult->{"limitRefundAmount"};
    				$this->limitRefundAmount = new IssueMoney();
    				$this->limitRefundAmount->setStdResult ( $limitRefundAmountResult);
    			}
    			    		    				    			    			if (array_key_exists ( "limitRefundLocalAmount", $this->stdResult )) {
    				$limitRefundLocalAmountResult=$this->stdResult->{"limitRefundLocalAmount"};
    				$this->limitRefundLocalAmount = new IssueMoney();
    				$this->limitRefundLocalAmount->setStdResult ( $limitRefundLocalAmountResult);
    			}
    			    		    				    			    			if (array_key_exists ( "reasonChinese", $this->stdResult )) {
    				$this->reasonChinese = $this->stdResult->{"reasonChinese"};
    			}
    			    		    				    			    			if (array_key_exists ( "reasonEnglish", $this->stdResult )) {
    				$this->reasonEnglish = $this->stdResult->{"reasonEnglish"};
    			}
    			    		    		}
	
	private $arrayResult;
	public function setArrayResult($arrayResult) {
		$this->arrayResult = $arrayResult;
				    		    			if (array_key_exists ( "id", $this->arrayResult )) {
    			$this->id = $arrayResult['id'];
    			}
    		    	    			    		    			if (array_key_exists ( "gmtCreate", $this->arrayResult )) {
    			$this->gmtCreate = $arrayResult['gmtCreate'];
    			}
    		    	    			    		    			if (array_key_exists ( "gmtModified", $this->arrayResult )) {
    			$this->gmtModified = $arrayResult['gmtModified'];
    			}
    		    	    			    		    			if (array_key_exists ( "orderId", $this->arrayResult )) {
    			$this->orderId = $arrayResult['orderId'];
    			}
    		    	    			    		    			if (array_key_exists ( "parentOrderId", $this->arrayResult )) {
    			$this->parentOrderId = $arrayResult['parentOrderId'];
    			}
    		    	    			    		    			if (array_key_exists ( "issueStatus", $this->arrayResult )) {
    			$this->issueStatus = $arrayResult['issueStatus'];
    			}
    		    	    			    		    		if (array_key_exists ( "issueProcessDTOs", $this->arrayResult )) {
    		$issueProcessDTOsResult=$arrayResult['issueProcessDTOs'];
    			$this->issueProcessDTOs = IssueAPIIssueProcessDTO();
    			$this->issueProcessDTOs->$this->setStdResult ( $issueProcessDTOsResult);
    		}
    		    	    			    		    		if (array_key_exists ( "limitRefundAmount", $this->arrayResult )) {
    		$limitRefundAmountResult=$arrayResult['limitRefundAmount'];
    			    			$this->limitRefundAmount = new IssueMoney();
    			    			$this->limitRefundAmount->$this->setStdResult ( $limitRefundAmountResult);
    		}
    		    	    			    		    		if (array_key_exists ( "limitRefundLocalAmount", $this->arrayResult )) {
    		$limitRefundLocalAmountResult=$arrayResult['limitRefundLocalAmount'];
    			    			$this->limitRefundLocalAmount = new IssueMoney();
    			    			$this->limitRefundLocalAmount->$this->setStdResult ( $limitRefundLocalAmountResult);
    		}
    		    	    			    		    			if (array_key_exists ( "reasonChinese", $this->arrayResult )) {
    			$this->reasonChinese = $arrayResult['reasonChinese'];
    			}
    		    	    			    		    			if (array_key_exists ( "reasonEnglish", $this->arrayResult )) {
    			$this->reasonEnglish = $arrayResult['reasonEnglish'];
    			}
    		    	    		}
 
   
}
?>