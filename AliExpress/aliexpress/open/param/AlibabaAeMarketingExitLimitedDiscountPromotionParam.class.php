<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');

class AlibabaAeMarketingExitLimitedDiscountPromotionParam {

        
        /**
    * @return ae promotion id
    */
        public function getPromotionId() {
        $tempResult = $this->sdkStdResult["promotionId"];
        return $tempResult;
    }
    
    /**
     * 设置ae promotion id     
     * @param Long $promotionId     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setPromotionId( $promotionId) {
        $this->sdkStdResult["promotionId"] = $promotionId;
    }
    
        
        /**
    * @return product id
    */
        public function getProductId() {
        $tempResult = $this->sdkStdResult["productId"];
        return $tempResult;
    }
    
    /**
     * 设置product id     
     * @param Long $productId     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setProductId( $productId) {
        $this->sdkStdResult["productId"] = $productId;
    }
    
        
    private $sdkStdResult=array();
    
    public function getSdkStdResult(){
    	return $this->sdkStdResult;
    }

}
?>