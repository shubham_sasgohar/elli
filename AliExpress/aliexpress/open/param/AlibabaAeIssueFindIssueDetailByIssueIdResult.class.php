<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');
include_once ('aliexpress/open/param/AlibabaIssueIssueDetailOpenApiDTO.class.php');

class AlibabaAeIssueFindIssueDetailByIssueIdResult {

        	
    private $resultObject;
    
        /**
    * @return 案件详细信息
    */
        public function getResultObject() {
        return $this->resultObject;
    }
    
    /**
     * 设置案件详细信息     
     * @param AlibabaIssueIssueDetailOpenApiDTO $resultObject     
          
     * 此参数必填     */
    public function setResultObject(AlibabaIssueIssueDetailOpenApiDTO $resultObject) {
        $this->resultObject = $resultObject;
    }
    
        	
    private $success;
    
        /**
    * @return 调用过程正确否
    */
        public function getSuccess() {
        return $this->success;
    }
    
    /**
     * 设置调用过程正确否     
     * @param Boolean $success     
          
     * 此参数必填     */
    public function setSuccess( $success) {
        $this->success = $success;
    }
    
        	
    private $resultCode;
    
        /**
    * @return 调用返回状态码
    */
        public function getResultCode() {
        return $this->resultCode;
    }
    
    /**
     * 设置调用返回状态码     
     * @param Integer $resultCode     
          
     * 此参数必填     */
    public function setResultCode( $resultCode) {
        $this->resultCode = $resultCode;
    }
    
        	
    private $resultMemo;
    
        /**
    * @return 调用返回说明
    */
        public function getResultMemo() {
        return $this->resultMemo;
    }
    
    /**
     * 设置调用返回说明     
     * @param String $resultMemo     
          
     * 此参数必填     */
    public function setResultMemo( $resultMemo) {
        $this->resultMemo = $resultMemo;
    }
    
    	
	private $stdResult;
	
	public function setStdResult($stdResult) {
		$this->stdResult = $stdResult;
					    			    			if (array_key_exists ( "resultObject", $this->stdResult )) {
    				$resultObjectResult=$this->stdResult->{"resultObject"};
    				$this->resultObject = new AlibabaIssueIssueDetailOpenApiDTO();
    				$this->resultObject->setStdResult ( $resultObjectResult);
    			}
    			    		    				    			    			if (array_key_exists ( "success", $this->stdResult )) {
    				$this->success = $this->stdResult->{"success"};
    			}
    			    		    				    			    			if (array_key_exists ( "resultCode", $this->stdResult )) {
    				$this->resultCode = $this->stdResult->{"resultCode"};
    			}
    			    		    				    			    			if (array_key_exists ( "resultMemo", $this->stdResult )) {
    				$this->resultMemo = $this->stdResult->{"resultMemo"};
    			}
    			    		    		}
	
	private $arrayResult;
	public function setArrayResult($arrayResult) {
		$this->arrayResult = $arrayResult;
				    		    		if (array_key_exists ( "resultObject", $this->arrayResult )) {
    		$resultObjectResult=$arrayResult['resultObject'];
    			    			$this->resultObject = new AlibabaIssueIssueDetailOpenApiDTO();
    			    			$this->resultObject->setStdResult ( $resultObjectResult);
    		}
    		    	    			    		    			if (array_key_exists ( "success", $this->arrayResult )) {
    			$this->success = $arrayResult['success'];
    			}
    		    	    			    		    			if (array_key_exists ( "resultCode", $this->arrayResult )) {
    			$this->resultCode = $arrayResult['resultCode'];
    			}
    		    	    			    		    			if (array_key_exists ( "resultMemo", $this->arrayResult )) {
    			$this->resultMemo = $arrayResult['resultMemo'];
    			}
    		    	    		}

}
?>