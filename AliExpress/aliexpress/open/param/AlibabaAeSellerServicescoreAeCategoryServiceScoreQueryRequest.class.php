<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');

class AlibabaAeSellerServicescoreAeCategoryServiceScoreQueryRequest extends SDKDomain {

       	
    private $loginId;
    
        /**
    * @return 卖家loginId，需要与授权用户相同
    */
        public function getLoginId() {
        return $this->loginId;
    }
    
    /**
     * 设置卖家loginId，需要与授权用户相同     
     * @param String $loginId     
     * 参数示例：<pre>cn1231231</pre>     
     * 此参数必填     */
    public function setLoginId( $loginId) {
        $this->loginId = $loginId;
    }
    
        	
    private $localeStr;
    
        /**
    * @return 语言环境： e.g:zh_CN 简体中文, zh_TW 繁体中文, en_US 英文
    */
        public function getLocaleStr() {
        return $this->localeStr;
    }
    
    /**
     * 设置语言环境： e.g:zh_CN 简体中文, zh_TW 繁体中文, en_US 英文     
     * @param String $localeStr     
     * 参数示例：<pre>zh_CN</pre>     
     * 此参数必填     */
    public function setLocaleStr( $localeStr) {
        $this->localeStr = $localeStr;
    }
    
        	
    private $pcateId;
    
        /**
    * @return 类目ID
    */
        public function getPcateId() {
        return $this->pcateId;
    }
    
    /**
     * 设置类目ID     
     * @param Long $pcateId     
     * 参数示例：<pre>200003274</pre>     
     * 此参数必填     */
    public function setPcateId( $pcateId) {
        $this->pcateId = $pcateId;
    }
    
        	
    private $pcateFlag;
    
        /**
    * @return 类目层级，目前只支持二级和三级类目
    */
        public function getPcateFlag() {
        return $this->pcateFlag;
    }
    
    /**
     * 设置类目层级，目前只支持二级和三级类目     
     * @param Long $pcateFlag     
     * 参数示例：<pre>2,3</pre>     
     * 此参数必填     */
    public function setPcateFlag( $pcateFlag) {
        $this->pcateFlag = $pcateFlag;
    }
    
    	
	private $stdResult;
	
	public function setStdResult($stdResult) {
		$this->stdResult = $stdResult;
					    			    			if (array_key_exists ( "loginId", $this->stdResult )) {
    				$this->loginId = $this->stdResult->{"loginId"};
    			}
    			    		    				    			    			if (array_key_exists ( "localeStr", $this->stdResult )) {
    				$this->localeStr = $this->stdResult->{"localeStr"};
    			}
    			    		    				    			    			if (array_key_exists ( "pcateId", $this->stdResult )) {
    				$this->pcateId = $this->stdResult->{"pcateId"};
    			}
    			    		    				    			    			if (array_key_exists ( "pcateFlag", $this->stdResult )) {
    				$this->pcateFlag = $this->stdResult->{"pcateFlag"};
    			}
    			    		    		}
	
	private $arrayResult;
	public function setArrayResult($arrayResult) {
		$this->arrayResult = $arrayResult;
				    		    			if (array_key_exists ( "loginId", $this->arrayResult )) {
    			$this->loginId = $arrayResult['loginId'];
    			}
    		    	    			    		    			if (array_key_exists ( "localeStr", $this->arrayResult )) {
    			$this->localeStr = $arrayResult['localeStr'];
    			}
    		    	    			    		    			if (array_key_exists ( "pcateId", $this->arrayResult )) {
    			$this->pcateId = $arrayResult['pcateId'];
    			}
    		    	    			    		    			if (array_key_exists ( "pcateFlag", $this->arrayResult )) {
    			$this->pcateFlag = $arrayResult['pcateFlag'];
    			}
    		    	    		}
 
   
}
?>