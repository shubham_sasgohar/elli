<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');
include_once ('aliexpress/open/param/AlibabaAeLogisticsAeopShipmentRequset.class.php');

class AlibabaAeLogisticsAeopSellerShipmentSubTradeOrderRequest extends SDKDomain {

       	
    private $subTradeOrderIndex;
    
        /**
    * @return 子订单序号，从1开始
    */
        public function getSubTradeOrderIndex() {
        return $this->subTradeOrderIndex;
    }
    
    /**
     * 设置子订单序号，从1开始     
     * @param Integer $subTradeOrderIndex     
     * 参数示例：<pre>1</pre>     
     * 此参数必填     */
    public function setSubTradeOrderIndex( $subTradeOrderIndex) {
        $this->subTradeOrderIndex = $subTradeOrderIndex;
    }
    
        	
    private $shipmentList;
    
        /**
    * @return 包裹声明发货信息
    */
        public function getShipmentList() {
        return $this->shipmentList;
    }
    
    /**
     * 设置包裹声明发货信息     
     * @param array include @see AlibabaAeLogisticsAeopShipmentRequset[] $shipmentList     
     * 参数示例：<pre>{
						"logisticsNo":"123456", //必填
						"serviceName":"CAINIAO_STANDARD", //必填
						"trackingWebSite":"www.baidu.com" //other时必填
					}</pre>     
     * 此参数必填     */
    public function setShipmentList(AlibabaAeLogisticsAeopShipmentRequset $shipmentList) {
        $this->shipmentList = $shipmentList;
    }
    
        	
    private $sendType;
    
        /**
    * @return 声明发货类型：part(部分发货)，all(全部发货)
    */
        public function getSendType() {
        return $this->sendType;
    }
    
    /**
     * 设置声明发货类型：part(部分发货)，all(全部发货)     
     * @param String $sendType     
     * 参数示例：<pre>part/all</pre>     
     * 此参数必填     */
    public function setSendType( $sendType) {
        $this->sendType = $sendType;
    }
    
    	
	private $stdResult;
	
	public function setStdResult($stdResult) {
		$this->stdResult = $stdResult;
					    			    			if (array_key_exists ( "subTradeOrderIndex", $this->stdResult )) {
    				$this->subTradeOrderIndex = $this->stdResult->{"subTradeOrderIndex"};
    			}
    			    		    				    			    			if (array_key_exists ( "shipmentList", $this->stdResult )) {
    			$shipmentListResult=$this->stdResult->{"shipmentList"};
    				$object = json_decode ( json_encode ( $shipmentListResult ), true );
					$this->shipmentList = array ();
					for($i = 0; $i < count ( $object ); $i ++) {
						$arrayobject = new ArrayObject ( $object [$i] );
						$AlibabaAeLogisticsAeopShipmentRequsetResult=new AlibabaAeLogisticsAeopShipmentRequset();
						$AlibabaAeLogisticsAeopShipmentRequsetResult->setArrayResult($arrayobject );
						$this->shipmentList [$i] = $AlibabaAeLogisticsAeopShipmentRequsetResult;
					}
    			}
    			    		    				    			    			if (array_key_exists ( "sendType", $this->stdResult )) {
    				$this->sendType = $this->stdResult->{"sendType"};
    			}
    			    		    		}
	
	private $arrayResult;
	public function setArrayResult($arrayResult) {
		$this->arrayResult = $arrayResult;
				    		    			if (array_key_exists ( "subTradeOrderIndex", $this->arrayResult )) {
    			$this->subTradeOrderIndex = $arrayResult['subTradeOrderIndex'];
    			}
    		    	    			    		    		if (array_key_exists ( "shipmentList", $this->arrayResult )) {
    		$shipmentListResult=$arrayResult['shipmentList'];
    			$this->shipmentList = AlibabaAeLogisticsAeopShipmentRequset();
    			$this->shipmentList->$this->setStdResult ( $shipmentListResult);
    		}
    		    	    			    		    			if (array_key_exists ( "sendType", $this->arrayResult )) {
    			$this->sendType = $arrayResult['sendType'];
    			}
    		    	    		}
 
   
}
?>