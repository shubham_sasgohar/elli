<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');

class ApiEvaluationSaveSellerFeedbackParam {

        
        /**
    * @return 订单编号
    */
        public function getOrderId() {
        $tempResult = $this->sdkStdResult["orderId"];
        return $tempResult;
    }
    
    /**
     * 设置订单编号     
     * @param Long $orderId     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setOrderId( $orderId) {
        $this->sdkStdResult["orderId"] = $orderId;
    }
    
        
        /**
    * @return 用户对主订单的打分
    */
        public function getScore() {
        $tempResult = $this->sdkStdResult["score"];
        return $tempResult;
    }
    
    /**
     * 设置用户对主订单的打分     
     * @param Integer $score     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setScore( $score) {
        $this->sdkStdResult["score"] = $score;
    }
    
        
        /**
    * @return 卖家对未评价的订单进行评价(Max 1,000 characters. Please do not use HTML codes or Chinese characters.同时包括中文标点也不支持）
    */
        public function getFeedbackContent() {
        $tempResult = $this->sdkStdResult["feedbackContent"];
        return $tempResult;
    }
    
    /**
     * 设置卖家对未评价的订单进行评价(Max 1,000 characters. Please do not use HTML codes or Chinese characters.同时包括中文标点也不支持）     
     * @param String $feedbackContent     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setFeedbackContent( $feedbackContent) {
        $this->sdkStdResult["feedbackContent"] = $feedbackContent;
    }
    
        
    private $sdkStdResult=array();
    
    public function getSdkStdResult(){
    	return $this->sdkStdResult;
    }

}
?>