<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');
include_once ('aliexpress/open/param/AlibabaProductPromiseTemplate.class.php');

class ApiQueryPromiseTemplateByIdResult {

        	
    private $templateList;
    
        /**
    * @return 
    */
        public function getTemplateList() {
        return $this->templateList;
    }
    
    /**
     * 设置     
     * @param array include @see AlibabaProductPromiseTemplate[] $templateList     
          
     * 此参数必填     */
    public function setTemplateList(AlibabaProductPromiseTemplate $templateList) {
        $this->templateList = $templateList;
    }
    
    	
	private $stdResult;
	
	public function setStdResult($stdResult) {
		$this->stdResult = $stdResult;
					    			    			if (array_key_exists ( "templateList", $this->stdResult )) {
    			$templateListResult=$this->stdResult->{"templateList"};
    				$object = json_decode ( json_encode ( $templateListResult ), true );
					$this->templateList = array ();
					for($i = 0; $i < count ( $object ); $i ++) {
						$arrayobject = new ArrayObject ( $object [$i] );
						$AlibabaProductPromiseTemplateResult=new AlibabaProductPromiseTemplate();
						$AlibabaProductPromiseTemplateResult->setArrayResult($arrayobject );
						$this->templateList [$i] = $AlibabaProductPromiseTemplateResult;
					}
    			}
    			    		    		}
	
	private $arrayResult;
	public function setArrayResult($arrayResult) {
		$this->arrayResult = $arrayResult;
				    		    		if (array_key_exists ( "templateList", $this->arrayResult )) {
    		$templateListResult=$arrayResult['templateList'];
    			$this->templateList = new AlibabaProductPromiseTemplate();
    			$this->templateList->setStdResult ( $templateListResult);
    		}
    		    	    		}

}
?>