<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');

class AlibabaProductNationalQuoteConfiguration extends SDKDomain {

       	
    private $configurationType;
    
        /**
    * @return 分国家定价规则类型[percentage：基于基准价格按比例配置]
    */
        public function getConfigurationType() {
        return $this->configurationType;
    }
    
    /**
     * 设置分国家定价规则类型[percentage：基于基准价格按比例配置]     
     * @param String $configurationType     
     * 参数示例：<pre>percentage</pre>     
     * 此参数必填     */
    public function setConfigurationType( $configurationType) {
        $this->configurationType = $configurationType;
    }
    
        	
    private $configurationData;
    
        /**
    * @return jsonArray格式的分国家定价规则数据。
1)基于基准价格按比例配置的数据格式：[{"shiptoCountry":"US","percentage":"5"},{"shiptoCountry":"RU","percentage":"-2"}]
其中shiptoCountry：ISO两位的国家编码（目前支持11个国家：RU,US,CA,ES,FR,UK,NL,IL,BR,CL,AU），
percentage：相对于基准价的调价比例（百分比整数，支持负数，当前限制>=-30 && <=100）
    */
        public function getConfigurationData() {
        return $this->configurationData;
    }
    
    /**
     * 设置jsonArray格式的分国家定价规则数据。
1)基于基准价格按比例配置的数据格式：[{"shiptoCountry":"US","percentage":"5"},{"shiptoCountry":"RU","percentage":"-2"}]
其中shiptoCountry：ISO两位的国家编码（目前支持11个国家：RU,US,CA,ES,FR,UK,NL,IL,BR,CL,AU），
percentage：相对于基准价的调价比例（百分比整数，支持负数，当前限制>=-30 && <=100）     
     * @param String $configurationData     
     * 参数示例：<pre>[{"shiptoCountry":"US","percentage":"5"},{"shiptoCountry":"RU","percentage":"-2"}]</pre>     
     * 此参数必填     */
    public function setConfigurationData( $configurationData) {
        $this->configurationData = $configurationData;
    }
    
    	
	private $stdResult;
	
	public function setStdResult($stdResult) {
		$this->stdResult = $stdResult;
					    			    			if (array_key_exists ( "configurationType", $this->stdResult )) {
    				$this->configurationType = $this->stdResult->{"configurationType"};
    			}
    			    		    				    			    			if (array_key_exists ( "configurationData", $this->stdResult )) {
    				$this->configurationData = $this->stdResult->{"configurationData"};
    			}
    			    		    		}
	
	private $arrayResult;
	public function setArrayResult($arrayResult) {
		$this->arrayResult = $arrayResult;
				    		    			if (array_key_exists ( "configurationType", $this->arrayResult )) {
    			$this->configurationType = $arrayResult['configurationType'];
    			}
    		    	    			    		    			if (array_key_exists ( "configurationData", $this->arrayResult )) {
    			$this->configurationData = $arrayResult['configurationData'];
    			}
    		    	    		}
 
   
}
?>