<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');

class AlibabaAeLogisticsAeopShipmentRequset extends SDKDomain {

       	
    private $logisticsNo;
    
        /**
    * @return 运单号
    */
        public function getLogisticsNo() {
        return $this->logisticsNo;
    }
    
    /**
     * 设置运单号     
     * @param String $logisticsNo     
     * 参数示例：<pre>LT123456789CN</pre>     
     * 此参数必填     */
    public function setLogisticsNo( $logisticsNo) {
        $this->logisticsNo = $logisticsNo;
    }
    
        	
    private $serviceName;
    
        /**
    * @return 用户选择的实际发货物流服务（物流服务key：该接口根据api.listLogisticsService列出平台所支持的物流服务 进行获取目前所支持的物流。平台支持物流服务详细一览表详见论坛链接
（http://sale.aliexpress.com/seller/shipping_methods_list.htm）
    */
        public function getServiceName() {
        return $this->serviceName;
    }
    
    /**
     * 设置用户选择的实际发货物流服务（物流服务key：该接口根据api.listLogisticsService列出平台所支持的物流服务 进行获取目前所支持的物流。平台支持物流服务详细一览表详见论坛链接
（http://sale.aliexpress.com/seller/shipping_methods_list.htm）     
     * @param String $serviceName     
     * 参数示例：<pre>CAINIAO_STANDARD</pre>     
     * 此参数必填     */
    public function setServiceName( $serviceName) {
        $this->serviceName = $serviceName;
    }
    
        	
    private $trackingWebSite;
    
        /**
    * @return 追踪网址
    */
        public function getTrackingWebSite() {
        return $this->trackingWebSite;
    }
    
    /**
     * 设置追踪网址     
     * @param String $trackingWebSite     
     * 参数示例：<pre>www.17track.com</pre>     
     * 此参数必填     */
    public function setTrackingWebSite( $trackingWebSite) {
        $this->trackingWebSite = $trackingWebSite;
    }
    
    	
	private $stdResult;
	
	public function setStdResult($stdResult) {
		$this->stdResult = $stdResult;
					    			    			if (array_key_exists ( "logisticsNo", $this->stdResult )) {
    				$this->logisticsNo = $this->stdResult->{"logisticsNo"};
    			}
    			    		    				    			    			if (array_key_exists ( "serviceName", $this->stdResult )) {
    				$this->serviceName = $this->stdResult->{"serviceName"};
    			}
    			    		    				    			    			if (array_key_exists ( "trackingWebSite", $this->stdResult )) {
    				$this->trackingWebSite = $this->stdResult->{"trackingWebSite"};
    			}
    			    		    		}
	
	private $arrayResult;
	public function setArrayResult($arrayResult) {
		$this->arrayResult = $arrayResult;
				    		    			if (array_key_exists ( "logisticsNo", $this->arrayResult )) {
    			$this->logisticsNo = $arrayResult['logisticsNo'];
    			}
    		    	    			    		    			if (array_key_exists ( "serviceName", $this->arrayResult )) {
    			$this->serviceName = $arrayResult['serviceName'];
    			}
    		    	    			    		    			if (array_key_exists ( "trackingWebSite", $this->arrayResult )) {
    			$this->trackingWebSite = $arrayResult['trackingWebSite'];
    			}
    		    	    		}
 
   
}
?>