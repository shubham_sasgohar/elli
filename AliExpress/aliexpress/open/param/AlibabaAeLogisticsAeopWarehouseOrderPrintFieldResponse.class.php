<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');

class AlibabaAeLogisticsAeopWarehouseOrderPrintFieldResponse extends SDKDomain {

       	
    private $success;
    
        /**
    * @return 是否调用成功
    */
        public function getSuccess() {
        return $this->success;
    }
    
    /**
     * 设置是否调用成功     
     * @param Boolean $success     
     * 参数示例：<pre>true</pre>     
     * 此参数必填     */
    public function setSuccess( $success) {
        $this->success = $success;
    }
    
        	
    private $errorDesc;
    
        /**
    * @return 调用错误描述信息
    */
        public function getErrorDesc() {
        return $this->errorDesc;
    }
    
    /**
     * 设置调用错误描述信息     
     * @param String $errorDesc     
     * 参数示例：<pre></pre>     
     * 此参数必填     */
    public function setErrorDesc( $errorDesc) {
        $this->errorDesc = $errorDesc;
    }
    
        	
    private $tradeOrderId;
    
        /**
    * @return 交易订单号
    */
        public function getTradeOrderId() {
        return $this->tradeOrderId;
    }
    
    /**
     * 设置交易订单号     
     * @param Long $tradeOrderId     
     * 参数示例：<pre>3400339439434</pre>     
     * 此参数必填     */
    public function setTradeOrderId( $tradeOrderId) {
        $this->tradeOrderId = $tradeOrderId;
    }
    
        	
    private $printPresorting;
    
        /**
    * @return 是否打印分拣码
    */
        public function getPrintPresorting() {
        return $this->printPresorting;
    }
    
    /**
     * 设置是否打印分拣码     
     * @param Boolean $printPresorting     
     * 参数示例：<pre>true</pre>     
     * 此参数必填     */
    public function setPrintPresorting( $printPresorting) {
        $this->printPresorting = $printPresorting;
    }
    
        	
    private $presortingCode;
    
        /**
    * @return presortingCode
    */
        public function getPresortingCode() {
        return $this->presortingCode;
    }
    
    /**
     * 设置presortingCode     
     * @param String $presortingCode     
     * 参数示例：<pre>2032390060</pre>     
     * 此参数必填     */
    public function setPresortingCode( $presortingCode) {
        $this->presortingCode = $presortingCode;
    }
    
        	
    private $zoningCode;
    
        /**
    * @return 分区码
    */
        public function getZoningCode() {
        return $this->zoningCode;
    }
    
    /**
     * 设置分区码     
     * @param String $zoningCode     
     * 参数示例：<pre>R1</pre>     
     * 此参数必填     */
    public function setZoningCode( $zoningCode) {
        $this->zoningCode = $zoningCode;
    }
    
    	
	private $stdResult;
	
	public function setStdResult($stdResult) {
		$this->stdResult = $stdResult;
					    			    			if (array_key_exists ( "success", $this->stdResult )) {
    				$this->success = $this->stdResult->{"success"};
    			}
    			    		    				    			    			if (array_key_exists ( "errorDesc", $this->stdResult )) {
    				$this->errorDesc = $this->stdResult->{"errorDesc"};
    			}
    			    		    				    			    			if (array_key_exists ( "tradeOrderId", $this->stdResult )) {
    				$this->tradeOrderId = $this->stdResult->{"tradeOrderId"};
    			}
    			    		    				    			    			if (array_key_exists ( "printPresorting", $this->stdResult )) {
    				$this->printPresorting = $this->stdResult->{"printPresorting"};
    			}
    			    		    				    			    			if (array_key_exists ( "presortingCode", $this->stdResult )) {
    				$this->presortingCode = $this->stdResult->{"presortingCode"};
    			}
    			    		    				    			    			if (array_key_exists ( "zoningCode", $this->stdResult )) {
    				$this->zoningCode = $this->stdResult->{"zoningCode"};
    			}
    			    		    		}
	
	private $arrayResult;
	public function setArrayResult($arrayResult) {
		$this->arrayResult = $arrayResult;
				    		    			if (array_key_exists ( "success", $this->arrayResult )) {
    			$this->success = $arrayResult['success'];
    			}
    		    	    			    		    			if (array_key_exists ( "errorDesc", $this->arrayResult )) {
    			$this->errorDesc = $arrayResult['errorDesc'];
    			}
    		    	    			    		    			if (array_key_exists ( "tradeOrderId", $this->arrayResult )) {
    			$this->tradeOrderId = $arrayResult['tradeOrderId'];
    			}
    		    	    			    		    			if (array_key_exists ( "printPresorting", $this->arrayResult )) {
    			$this->printPresorting = $arrayResult['printPresorting'];
    			}
    		    	    			    		    			if (array_key_exists ( "presortingCode", $this->arrayResult )) {
    			$this->presortingCode = $arrayResult['presortingCode'];
    			}
    		    	    			    		    			if (array_key_exists ( "zoningCode", $this->arrayResult )) {
    			$this->zoningCode = $arrayResult['zoningCode'];
    			}
    		    	    		}
 
   
}
?>