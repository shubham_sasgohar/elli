<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');
include_once ('aliexpress/open/param/AlibabaAeLogisticsAeopSellerShipmentSubTradeOrderRequest.class.php');

class AlibabaAeLogisticsSellerModifiedShipmentSupportSubTradeOrderParam {

        
        /**
    * @return 交易订单号
    */
        public function getTradeOrderId() {
        $tempResult = $this->sdkStdResult["tradeOrderId"];
        return $tempResult;
    }
    
    /**
     * 设置交易订单号     
     * @param Long $tradeOrderId     
     * 参数示例：<pre>888887777766</pre>     
     * 此参数必填     */
    public function setTradeOrderId( $tradeOrderId) {
        $this->sdkStdResult["tradeOrderId"] = $tradeOrderId;
    }
    
        
        /**
    * @return 旧的运单号
    */
        public function getOldLogisticsNo() {
        $tempResult = $this->sdkStdResult["oldLogisticsNo"];
        return $tempResult;
    }
    
    /**
     * 设置旧的运单号     
     * @param String $oldLogisticsNo     
     * 参数示例：<pre>LV87654321CN</pre>     
     * 此参数必填     */
    public function setOldLogisticsNo( $oldLogisticsNo) {
        $this->sdkStdResult["oldLogisticsNo"] = $oldLogisticsNo;
    }
    
        
        /**
    * @return 旧的物流服务名称
    */
        public function getOldServiceName() {
        $tempResult = $this->sdkStdResult["oldServiceName"];
        return $tempResult;
    }
    
    /**
     * 设置旧的物流服务名称     
     * @param String $oldServiceName     
     * 参数示例：<pre>CAINIAO_STANDARD</pre>     
     * 此参数必填     */
    public function setOldServiceName( $oldServiceName) {
        $this->sdkStdResult["oldServiceName"] = $oldServiceName;
    }
    
        
        /**
    * @return 新的声明发货信息
    */
        public function getSubTradeOrderList() {
        $tempResult = $this->sdkStdResult["subTradeOrderList"];
        return $tempResult;
    }
    
    /**
     * 设置新的声明发货信息     
     * @param array include @see AlibabaAeLogisticsAeopSellerShipmentSubTradeOrderRequest[] $subTradeOrderList     
     * 参数示例：<pre>[
			{
				"subTradeOrderIndex":"1",
				"shipmentList":[
					{
						"newLogisticsNo":"123456",
						"newServiceName":"CAINIAO_STANDARD",
						"trackingWebSite":"www.baidu.com"
					}
				]
			}
		]</pre>     
     * 此参数必填     */
    public function setSubTradeOrderList(AlibabaAeLogisticsAeopSellerShipmentSubTradeOrderRequest $subTradeOrderList) {
        $this->sdkStdResult["subTradeOrderList"] = $subTradeOrderList;
    }
    
        
    private $sdkStdResult=array();
    
    public function getSdkStdResult(){
    	return $this->sdkStdResult;
    }

}
?>