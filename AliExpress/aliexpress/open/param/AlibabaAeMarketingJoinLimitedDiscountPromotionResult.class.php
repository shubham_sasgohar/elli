<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');

class AlibabaAeMarketingJoinLimitedDiscountPromotionResult {

        	
    private $success;
    
        /**
    * @return the result for join promotion operation
    */
        public function getSuccess() {
        return $this->success;
    }
    
    /**
     * 设置the result for join promotion operation     
     * @param Boolean $success     
          
     * 此参数必填     */
    public function setSuccess( $success) {
        $this->success = $success;
    }
    
        	
    private $errorMsg;
    
        /**
    * @return error message
    */
        public function getErrorMsg() {
        return $this->errorMsg;
    }
    
    /**
     * 设置error message     
     * @param String $errorMsg     
          
     * 此参数必填     */
    public function setErrorMsg( $errorMsg) {
        $this->errorMsg = $errorMsg;
    }
    
        	
    private $errCode;
    
        /**
    * @return error code
    */
        public function getErrCode() {
        return $this->errCode;
    }
    
    /**
     * 设置error code     
     * @param Integer $errCode     
          
     * 此参数必填     */
    public function setErrCode( $errCode) {
        $this->errCode = $errCode;
    }
    
    	
	private $stdResult;
	
	public function setStdResult($stdResult) {
		$this->stdResult = $stdResult;
					    			    			if (array_key_exists ( "success", $this->stdResult )) {
    				$this->success = $this->stdResult->{"success"};
    			}
    			    		    				    			    			if (array_key_exists ( "errorMsg", $this->stdResult )) {
    				$this->errorMsg = $this->stdResult->{"errorMsg"};
    			}
    			    		    				    			    			if (array_key_exists ( "errCode", $this->stdResult )) {
    				$this->errCode = $this->stdResult->{"errCode"};
    			}
    			    		    		}
	
	private $arrayResult;
	public function setArrayResult($arrayResult) {
		$this->arrayResult = $arrayResult;
				    		    			if (array_key_exists ( "success", $this->arrayResult )) {
    			$this->success = $arrayResult['success'];
    			}
    		    	    			    		    			if (array_key_exists ( "errorMsg", $this->arrayResult )) {
    			$this->errorMsg = $arrayResult['errorMsg'];
    			}
    		    	    			    		    			if (array_key_exists ( "errCode", $this->arrayResult )) {
    			$this->errCode = $arrayResult['errCode'];
    			}
    		    	    		}

}
?>