<?php

include_once ('com/alibaba/openapi/client/entity/SDKDomain.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');
include_once ('aliexpress/open/param/AlibabaAeLogisticsAeopSellerShipmentSubTradeOrderResponse.class.php');

class AlibabaAeLogisticsQuerySellerShipmentInfoResult {

        	
    private $subTradeOrderList;
    
        /**
    * @return 
    */
        public function getSubTradeOrderList() {
        return $this->subTradeOrderList;
    }
    
    /**
     * 设置     
     * @param array include @see AlibabaAeLogisticsAeopSellerShipmentSubTradeOrderResponse[] $subTradeOrderList     
          
     * 此参数必填     */
    public function setSubTradeOrderList(AlibabaAeLogisticsAeopSellerShipmentSubTradeOrderResponse $subTradeOrderList) {
        $this->subTradeOrderList = $subTradeOrderList;
    }
    
        	
    private $success;
    
        /**
    * @return 是否成功
    */
        public function getSuccess() {
        return $this->success;
    }
    
    /**
     * 设置是否成功     
     * @param Boolean $success     
          
     * 此参数必填     */
    public function setSuccess( $success) {
        $this->success = $success;
    }
    
        	
    private $errorMsg;
    
        /**
    * @return 错误信息
    */
        public function getErrorMsg() {
        return $this->errorMsg;
    }
    
    /**
     * 设置错误信息     
     * @param String $errorMsg     
          
     * 此参数必填     */
    public function setErrorMsg( $errorMsg) {
        $this->errorMsg = $errorMsg;
    }
    
        	
    private $errorCode;
    
        /**
    * @return 错误编码
    */
        public function getErrorCode() {
        return $this->errorCode;
    }
    
    /**
     * 设置错误编码     
     * @param String $errorCode     
          
     * 此参数必填     */
    public function setErrorCode( $errorCode) {
        $this->errorCode = $errorCode;
    }
    
        	
    private $tradeOrderId;
    
        /**
    * @return 交易订单号
    */
        public function getTradeOrderId() {
        return $this->tradeOrderId;
    }
    
    /**
     * 设置交易订单号     
     * @param Long $tradeOrderId     
          
     * 此参数必填     */
    public function setTradeOrderId( $tradeOrderId) {
        $this->tradeOrderId = $tradeOrderId;
    }
    
    	
	private $stdResult;
	
	public function setStdResult($stdResult) {
		$this->stdResult = $stdResult;
					    			    			if (array_key_exists ( "subTradeOrderList", $this->stdResult )) {
    			$subTradeOrderListResult=$this->stdResult->{"subTradeOrderList"};
    				$object = json_decode ( json_encode ( $subTradeOrderListResult ), true );
					$this->subTradeOrderList = array ();
					for($i = 0; $i < count ( $object ); $i ++) {
						$arrayobject = new ArrayObject ( $object [$i] );
						$AlibabaAeLogisticsAeopSellerShipmentSubTradeOrderResponseResult=new AlibabaAeLogisticsAeopSellerShipmentSubTradeOrderResponse();
						$AlibabaAeLogisticsAeopSellerShipmentSubTradeOrderResponseResult->setArrayResult($arrayobject );
						$this->subTradeOrderList [$i] = $AlibabaAeLogisticsAeopSellerShipmentSubTradeOrderResponseResult;
					}
    			}
    			    		    				    			    			if (array_key_exists ( "success", $this->stdResult )) {
    				$this->success = $this->stdResult->{"success"};
    			}
    			    		    				    			    			if (array_key_exists ( "errorMsg", $this->stdResult )) {
    				$this->errorMsg = $this->stdResult->{"errorMsg"};
    			}
    			    		    				    			    			if (array_key_exists ( "errorCode", $this->stdResult )) {
    				$this->errorCode = $this->stdResult->{"errorCode"};
    			}
    			    		    				    			    			if (array_key_exists ( "tradeOrderId", $this->stdResult )) {
    				$this->tradeOrderId = $this->stdResult->{"tradeOrderId"};
    			}
    			    		    		}
	
	private $arrayResult;
	public function setArrayResult($arrayResult) {
		$this->arrayResult = $arrayResult;
				    		    		if (array_key_exists ( "subTradeOrderList", $this->arrayResult )) {
    		$subTradeOrderListResult=$arrayResult['subTradeOrderList'];
    			$this->subTradeOrderList = new AlibabaAeLogisticsAeopSellerShipmentSubTradeOrderResponse();
    			$this->subTradeOrderList->setStdResult ( $subTradeOrderListResult);
    		}
    		    	    			    		    			if (array_key_exists ( "success", $this->arrayResult )) {
    			$this->success = $arrayResult['success'];
    			}
    		    	    			    		    			if (array_key_exists ( "errorMsg", $this->arrayResult )) {
    			$this->errorMsg = $arrayResult['errorMsg'];
    			}
    		    	    			    		    			if (array_key_exists ( "errorCode", $this->arrayResult )) {
    			$this->errorCode = $arrayResult['errorCode'];
    			}
    		    	    			    		    			if (array_key_exists ( "tradeOrderId", $this->arrayResult )) {
    			$this->tradeOrderId = $arrayResult['tradeOrderId'];
    			}
    		    	    		}

}
?>