<?php
namespace common\models;

use Yii;
use yii\base\Model;
use common\models\LoginForm;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            ['username', 'required', 'message' => 'Email cannot be blank'],
            ['username', 'email' ,  'message' => 'Invalid email address'],
            ['username', 'checkDomain'],

            ['password', 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    
    
    public function checkDomain($attribute, $params)
    {
        
            $curr_url = "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
            $path = explode('.',$curr_url);
            $domain_name = $path[0];
            $dir_name = Yii::getAlias('@basedir').'/subdomains/'.$domain_name;
         
            
           if(is_dir($dir_name)):
                $email= $this->username;
                $admin_data=User::find()->Where(['email'=>$email,'role'=>'superadmin'])->one();
                //case 1
                if(empty($admin_data)):

                    $explode_new_user_email= explode(".",explode("@",$email)[1]);
                    $match_new_user_email=$explode_new_user_email[0];
                       if($match_new_user_email!=$domain_name) :

                           $this->addError('username','Invalid domain');   
                       endif;

                endif;
            endif;
            


            
//            if($dir_name) :
//            $domain_name1 = $path[0].'_elliot';
//            echo $domain_name1.'<br>';
//            $db = Yii::$app->getDb();
//            $db_name_explode=explode('dbname=', $db->dsn);
//           echo  $db_name=$db_name_explode[1];
//            
//            if($db_name!=$domain_name1) {
//                    // no real check at the moment to be sure that the error is triggered
//                 $this->addError('username','Invalid domain');                
//                 // $this->addError($attribute, Yii::t('user', 'You entered an invalid date format.')); 
//            }
//            endif;
          
 
    }
    
    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect Email or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        
        if ($this->validate()) {
            
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
            
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        
        if ($this->_user === null) {
            $this->_user = User::findByUsername($this->username);
        }
        
        return $this->_user;
    }
}
