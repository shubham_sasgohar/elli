<?php
namespace backend\models;

use yii\base\Model;
use common\models\User;
use common\models\CustomFunction;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $company_name;
    public $email;
    public $password;
    public $confirm_password;
 
    public $remember;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'checkUsername'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            
            ['company_name', 'trim'],
            ['company_name', 'required'],
            ['company_name', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This Company has already been taken.'],
            ['company_name', 'checkFolder'],
            ['company_name', 'string', 'min' => 2, 'max' => 255],
            

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],
            ['email', 'checkEmail'],
         

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            
            ['confirm_password', 'required'],
            ['confirm_password', 'string', 'min' => 6],
            ['confirm_password', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],
            
            array('remember', 'compare', 'compareValue' => 1, 'message' => 'You should accept term to use our service'),
        ];
    }
    
 
    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->company_name = $this->company_name;
        $user->email = $this->email;
        $domain =CustomFunction::getemaildomain($this->email);
        $user->domain_name =$domain;
        $user->setPassword($this->password);
        $user->generateAuthKey();
  

        return $user->save() ? $user : null;
    }
    
    public function checkEmail($attribute, $params)
    {
            $new_user_email=$this->email;
            /*explode a user new email*/
            $explode_new_user_email=explode("@",$new_user_email);
            $new_user_email=$explode_new_user_email[1];
            $match_new_user_email='@'.$new_user_email;
            $users_data = User::find()->where('email LIKE :query') ->addParams([':query'=>'%'.$match_new_user_email])->andWhere(['role'=>'merchant'])->all();
            if(!empty($users_data)) {
                // no real check at the moment to be sure that the error is triggered
                $this->addError('email','This Email Domain has been already exist');                
                 // $this->addError($attribute, Yii::t('user', 'You entered an invalid date format.'));
            }
    }
    /*Check If Comapny folder name already exist or Not*/
    public function checkFolder($attribute, $params)
    {
            $basedir = Yii::getAlias('@basedir');
            $company_name=$this->company_name;
            $domain_name = $company_name;
            $domain_folder_path = $basedir . '/subdomains' . '/' . $domain_name;
        
            if (is_dir($domain_folder_path)) {
            // is_dir - tells whether the filename is a directory
              $this->addError('company_name','This Company has already been taken');  
            // echo "This Domain is already exist";
            }
     
    }
    

    
    
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
          return Yii::$app->security->validatePassword($password, $this->password_hash);
    }
    
        public function checkUsername($attribute, $params)
    {
            
            $new_user_username=$this->username;
            
            $validate_username = strpos($new_user_username, '.');
            if($validate_username == true)
            {
                $this->addError('username','Invalid Username'); 
            }
           

    }
    
    
}
