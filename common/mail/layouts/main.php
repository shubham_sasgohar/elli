<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'), require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'), require(__DIR__ . '/params-local.php')
);

return [
  'id' => 'app-backend',
  'basePath' => dirname(__DIR__),
  'controllerNamespace' => 'backend\controllers',
  'bootstrap' => ['log'],
  'modules' => [],
  'components' => [
     'mailer' => [
                'class' => 'yii\swiftmailer\Mailer',
                'viewPath' => '@common/mail',
                'transport' => [
                    'class' => 'Swift_MailTransport',
                ],
                'useFileTransport' => false,
            ],
    'request' => [
      'csrfParam' => '_csrf-backend',
    ],
    'user' => [
      'identityClass' => 'common\models\User',
      //auth time is used inactivity on dashboard they will automatically logout//
//      'authTimeout'=>900,
     // 'enableAutoLogin' => true,
      'identityCookie' => [
        'name' => '_identity-backend',
        'httpOnly' => true,
        'path' => '/',
        'domain' => ".s86.co"
      ],
    ],
    'session' => [
      // this is the name of the session cookie used for login on the backend
      'name' => 'advanced-backend',
      'cookieParams' => [
        'path' => '/',
        'domain' => ".s86.co",
      ],
    ],
    'request' => [
      // ...
      'csrfCookie' => [
        'name' => '_csrf',
        'path' => '/',
        'domain' => ".s86.co",
      ],
    ],
    'log' => [
      'traceLevel' => YII_DEBUG ? 3 : 0,
      'targets' => [
        [
          'class' => 'yii\log\FileTarget',
          'levels' => ['error', 'warning'],
        ],
      ],
    ],
    'errorHandler' => [
      'errorAction' => 'site/error',
    ],
    'urlManager' => [
      'enablePrettyUrl' => true,
      'showScriptName' => false,
      'rules' => [
        //'<action:(.*)>' => 'site/<action>',
        //for site controller actions//
        'login'                         => 'site/login',
        'index'                         => 'site/index',
        'profile'                       => 'site/profile',
        'signup'                        => 'site/signup',
        'logout'                        => 'site/logout',
        'user-subscription'             => 'site/user-subscription',
        'subscriptionpayment'           => 'site/subscriptionpayment',
        'sessiondestroy'                => 'site/sessiondestroy',
        'order'                         => 'site/order',
        'cron-account-status'           => 'site/cron-account-status',
        'profile'                       => 'site/profile', 
        'saveprofile'                   => 'site/saveprofile', 
        'delete'                        => 'site/delete',
        'export-user'                   => 'site/export-user',
        'general'                       => 'site/general', 
        'get-countries'                 => 'site/get-countries', 
        'get-states'                    => 'site/get-states', 
        'get-cities'                    => 'site/get-cities', 
        'save-cover-image'              => 'site/save-cover-image', 
        'save-profile-image'            => 'site/save-profile-image',
        'save-general-info'             => 'site/save-general-info',
        'get-languages'                 => 'site/get-languages',
        'get-timezone'                  => 'site/get-timezone',
        'translations'                  => 'site/translations',
        'request-password-reset'        => 'site/request-password-reset',
         'reset-password'               => 'site/reset-password',
        'request-password-reset-token'  => 'site/request-password-reset-token',
  
        '<controller:\w+>/<id:\d+>' => '<controller>/view',
        '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
        '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
      ],
    ],
  ],
  'params' => $params,
];
